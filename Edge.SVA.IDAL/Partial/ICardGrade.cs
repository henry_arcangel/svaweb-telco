﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface ICardGrade
    {
        //Add by Alex 2014-06-09 ++
        SVA.Model.CardGrade GetImportCardGrade(string cardGradeCode);
        //Add by Alex 2014-06-09 --
    }
}
