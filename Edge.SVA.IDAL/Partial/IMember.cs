﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IMember
    {
        List<int> GetMembers(string mobileNumber);
        DataSet GetMembersID(string memberMobilePhone, string memberEmail);
    }
}
