﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_CardReceive_D
    {
        bool DeleteByOrder(string cardReceiveNumber);
    }
}
