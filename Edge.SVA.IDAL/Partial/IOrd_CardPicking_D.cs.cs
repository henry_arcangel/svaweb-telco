﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_CardPicking_D
    {
        DataSet GetListGroupByCardGrade(string strWhere);
        Dictionary<int, int> GetCardGradeIndex(string CardPickingNumber);
        bool DeleteByOrder(string CardPickingNumber);
    }

  
}
