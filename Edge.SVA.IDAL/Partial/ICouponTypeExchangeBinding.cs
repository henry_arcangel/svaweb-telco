﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface ICouponTypeExchangeBinding
    {
        DataSet FetchFields(string fields, string strWhere, params System.Data.SqlClient.SqlParameter[] parameters);
    }
}
