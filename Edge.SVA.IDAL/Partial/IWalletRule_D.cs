﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IWalletRule_D
    {
        bool DeleteByCode(string walletRuleCode);
    }
}
