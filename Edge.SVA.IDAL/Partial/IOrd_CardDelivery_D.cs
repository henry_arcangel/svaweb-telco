﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_CardDelivery_D
    {
        Dictionary<int, int> GetCardGradeIndex(string CardDeliveryNumber);
    }
}
