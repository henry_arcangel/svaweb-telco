﻿using System;
using System.Data;
using System.Collections.Generic;
namespace Edge.SVA.IDAL
{
	/// <summary>
	/// 接口层优惠劵的店铺条件
	/// </summary>
	public partial interface ICouponTypeStoreCondition
	{
        int AddList(List<Edge.SVA.Model.CouponTypeStoreCondition> modelList);

        int UpdateList(List<Edge.SVA.Model.CouponTypeStoreCondition> insertList, List<Edge.SVA.Model.CouponTypeStoreCondition> deleteList);
	} 
}
