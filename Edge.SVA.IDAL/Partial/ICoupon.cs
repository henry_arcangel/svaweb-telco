﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface ICoupon
    {

       
        DataSet GetListWithBatch(int Top, string strWhere, string filedOrder);
        DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times);
        DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times);
        int GetCount(string strWhere, int times);
        int GetCountWithBatch(string strWhere);
        DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder);
        bool ExsitCoupon(Model.Ord_ImportCouponUID_H model);
        bool ExsitCoupon(Model.Ord_CouponAdjust_H model);
        bool ExsitCoupon(string couponNumber);
        DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder);
        bool ValidCouponStauts(Model.Ord_CouponAdjust_H model,params int[] CouponStatus);
        Edge.SVA.Model.Coupon GetModelByUID(string couponUID);
    }
}
