﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_OrderToSupplier_Card_D
    {
        DataSet GetListGroupByCardType(string strWhere);
        bool DeleteByOrder(string orderSupplierNumber);
    }
}
