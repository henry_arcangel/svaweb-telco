﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IDALCRUD
    {
        DataSet Query(string sql);
        int ExecuteSql(string sql);
        int ExecuteSqlList(List<string> list);
    }
}
