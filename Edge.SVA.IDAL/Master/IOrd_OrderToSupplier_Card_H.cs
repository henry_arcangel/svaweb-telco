﻿using System;
using System.Data;
namespace Edge.SVA.IDAL
{
    /// <summary>
    /// 接口层面向供应商的订货单（订货实体Card）注：工作流程：总部向供应商下订单时
    /// </summary>
    public interface IOrd_OrderToSupplier_Card_H
    {
        #region  成员方法
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        bool Exists(string OrderSupplierNumber);
        /// <summary>
        /// 增加一条数据
        /// </summary>
        bool Add(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model);
        /// <summary>
        /// 更新一条数据
        /// </summary>
        bool Update(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model);
        /// <summary>
        /// 删除一条数据
        /// </summary>
        bool Delete(string OrderSupplierNumber);
        bool DeleteList(string OrderSupplierNumberlist);
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        Edge.SVA.Model.Ord_OrderToSupplier_Card_H GetModel(string OrderSupplierNumber);
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(string strWhere);
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        DataSet GetList(int Top, string strWhere, string filedOrder);
        int GetRecordCount(string strWhere);
        DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex);
        /// <summary>
        /// 根据分页获得数据列表
        /// </summary>
        //DataSet GetList(int PageSize,int PageIndex,string strWhere);
        #endregion  成员方法
    }
}

