﻿using System;
using System.Data;
namespace Edge.SVA.IDAL
{
    /// <summary>
    /// 接口层转赠单据表. 主表
    //@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D
    /// </summary>
    public partial interface IOrd_CardTransfer_H
    {
        #region  成员方法
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        bool Exists(string CardTransferNumber);
        /// <summary>
        /// 增加一条数据
        /// </summary>
        bool Add(Edge.SVA.Model.Ord_CardTransfer_H model);
        /// <summary>
        /// 更新一条数据
        /// </summary>
        bool Update(Edge.SVA.Model.Ord_CardTransfer_H model);
        /// <summary>
        /// 删除一条数据
        /// </summary>
        bool Delete(string CardTransferNumber);
        bool DeleteList(string CardTransferNumberlist);
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        Edge.SVA.Model.Ord_CardTransfer_H GetModel(string CardTransferNumber);
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(string strWhere);
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        DataSet GetList(int Top, string strWhere, string filedOrder);
        int GetRecordCount(string strWhere);
        DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex);
        /// <summary>
        /// 根据分页获得数据列表
        /// </summary>
        //DataSet GetList(int PageSize,int PageIndex,string strWhere);
        #endregion  成员方法
    }
}
