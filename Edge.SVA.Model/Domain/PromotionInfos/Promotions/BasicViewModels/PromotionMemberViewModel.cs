﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels
{
    public class PromotionMemberViewModel : Promotion_Member
    {
        private string objectKey = Guid.NewGuid().ToString();

        public string ObjectKey
        {
            get { return objectKey; }
            set { objectKey = value; }
        }

        private string _loyaltytypeName;

        public string LoyaltyTypeName
        {
            get { return _loyaltytypeName; }
            set { _loyaltytypeName = value; }
        }
        private string _loyaltyvalueName;

        public string LoyaltyValueName
        {
            get { return _loyaltyvalueName; }
            set { _loyaltyvalueName = value; }
        }
        private string oprFlag;

        public string OprFlag
        {
            get { return oprFlag; }
            set { oprFlag = value; }
        }

        //private string loyaltyCardTypeName;

        //public string LoyaltyCardTypeName
        //{
        //    get { return loyaltyCardTypeName; }
        //    set { loyaltyCardTypeName = value; }
        //}

        //private string loyaltyCardGradeName;

        //public string LoyaltyCardGradeName
        //{
        //    get { return loyaltyCardGradeName; }
        //    set { loyaltyCardGradeName = value; }
        //}

        private string loyaltyBirthdayFlagName;

        public string LoyaltyBirthdayFlagName
        {
            get { return loyaltyBirthdayFlagName; }
            set { loyaltyBirthdayFlagName = value; }
        }

        private string loyaltyPromoScopeName;
        public string LoyaltyPromoScopeName
        {
            get { return loyaltyPromoScopeName; }
            set { loyaltyPromoScopeName = value; }
        }
    }
}
