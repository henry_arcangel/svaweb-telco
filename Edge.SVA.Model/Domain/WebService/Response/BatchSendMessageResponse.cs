﻿using System;
using System.Collections.Generic;

namespace Edge.SVA.Model.Domain.WebService.Response
{

    public class BatchSendMessageResponse
    {

        private string _action;
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
            }
        }

        private int _responseCode;
        public int ResponseCode
        {
            get
            {
                return _responseCode;
            }
            set
            {
                _responseCode = value;
            }
        }

        private ResultInfo[] _resultInfo;
        public ResultInfo[] ResultInfo
        {
            get
            {
                return _resultInfo;
            }
            set
            {
                _resultInfo = value;
            }
        }

    }
}
