﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.WebService.Response
{
    public class NewMemberAccountResponse:WebServiceResponseBase
    {
        private string _memberID;
        public string MemberID
        {
            get
            {
                return _memberID;
            }
            set
            {
                _memberID = value;
            }
        }

        private string _memberPassword;
        public string MemberPassword
        {
            get
            {
                return _memberPassword;
            }
            set
            {
                _memberPassword = value;
            }
        }

        private string _cardNumber;
        public string CardNumber
        {
            get
            {
                return _cardNumber;
            }
            set
            {
                _cardNumber = value;
            }
        }

        private string _cardPassword;
        public string CardPassword
        {
            get
            {
                return _cardPassword;
            }
            set
            {
                _cardPassword = value;
            }
        }
    }
}
