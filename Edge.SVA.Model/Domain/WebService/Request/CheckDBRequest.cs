﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.WebService.Request
{
    public class CheckDBRequest
    {
        private string _action;
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
            }
        }

        private string _dBURL;

        public string DBURL
        {
            get { return _dBURL; }
            set { _dBURL = value; }
        }

        private string _dataBaseName;

        public string DataBaseName
        {
            get { return _dataBaseName; }
            set { _dataBaseName = value; }
        }

    }
}
