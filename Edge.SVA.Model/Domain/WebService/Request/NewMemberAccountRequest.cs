﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.WebService.Request
{
    public class NewMemberAccountRequest:WebServiceRequestBase
    {
        private string _memberRegisterMobile;
        public string MemberRegisterMobile
        {
            get
            {
                return _memberRegisterMobile;
            }
            set
            {
                _memberRegisterMobile = value;
            }
        }

        private string _memberMobilePhone;
        public string MemberMobilePhone
        {
            get
            {
                return _memberMobilePhone;
            }
            set
            {
                _memberMobilePhone = value;
            }
        }

        private string _countryCode;
        public string CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                _countryCode = value;
            }
        }

        private string _cardTypeCode;
        public string CardTypeCode
        {
            get
            {
                return _cardTypeCode;
            }
            set
            {
                _cardTypeCode = value;
            }
        }

        private string _cardNumber;
        public string CardNumber
        {
            get
            {
                return _cardNumber;
            }
            set
            {
                _cardNumber = value;
            }
        }

        private string _bindCardTypeID;
        public string BindCardTypeID
        {
            get
            {
                return _bindCardTypeID;
            }
            set
            {
                _bindCardTypeID = value;
            }
        }

        private string _memberDateOfBirth;
        public string MemberDateOfBirth
        {
            get
            {
                return _memberDateOfBirth;
            }
            set
            {
                _memberDateOfBirth = value;
            }
        }

        private string _memberSex;
        public string MemberSex
        {
            get
            {
                return _memberSex;
            }
            set
            {
                _memberSex = value;
            }
        }

        private string _memberEngFamilyName;
        public string MemberEngFamilyName
        {
            get
            {
                return _memberEngFamilyName;
            }
            set
            {
                _memberEngFamilyName = value;
            }
        }

        private string _memberEngGivenName;
        public string MemberEngGivenName
        {
            get
            {
                return _memberEngGivenName;
            }
            set
            {
                _memberEngGivenName = value;
            }
        }

        private string _memberChiFamilyName;
        public string MemberChiFamilyName
        {
            get
            {
                return _memberChiFamilyName;
            }
            set
            {
                _memberChiFamilyName = value;
            }
        }

        private string _memberChiGivenName;
        public string MemberChiGivenName
        {
            get
            {
                return _memberChiGivenName;
            }
            set
            {
                _memberChiGivenName = value;
            }
        }

        private string _autoBindCard;
        public string AutoBindCard
        {
            get
            {
                return _autoBindCard;
            }
            set
            {
                _autoBindCard = value;
            }
        }

        private string _referCardNumber;
        public string ReferCardNumber
        {
            get
            {
                return _referCardNumber;
            }
            set
            {
                _referCardNumber = value;
            }
        }
    }
}
