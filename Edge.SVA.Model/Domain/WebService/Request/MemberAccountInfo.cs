﻿using System;
using System.Collections.Generic;

namespace Edge.SVA.Model.Domain.WebService.Request
{

    public class MemberAccountInfo
    {

        private string _mPASSWORD;
        public string MPASSWORD
        {
            get
            {
                return _mPASSWORD;
            }
            set
            {
                _mPASSWORD = value;
            }
        }

        private string _mEMBERID;
        public string MEMBERID
        {
            get
            {
                return _mEMBERID;
            }
            set
            {
                _mEMBERID = value;
            }
        }

        private string _mSGACCOUNT;
        public string MSGACCOUNT
        {
            get
            {
                return _mSGACCOUNT;
            }
            set
            {
                _mSGACCOUNT = value;
            }
        }

        private string _mSGACCOUNTTYPE;
        public string MSGACCOUNTTYPE
        {
            get
            {
                return _mSGACCOUNTTYPE;
            }
            set
            {
                _mSGACCOUNTTYPE = value;
            }
        }

    }
}
