﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain
{
    public class InventoryReplenishRuleListViewModel
    {
        private List<InventoryReplenishRuleViewModel> inventoryReplenishRuleViewModelList = new List<InventoryReplenishRuleViewModel>();

        public List<InventoryReplenishRuleViewModel> InventoryReplenishRuleViewModelList
        {
            get { return inventoryReplenishRuleViewModelList; }
        }
    }
}
