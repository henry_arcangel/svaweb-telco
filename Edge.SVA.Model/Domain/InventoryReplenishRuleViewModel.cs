﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain
{
    public class InventoryReplenishRuleViewModel
    {
        string _orderTargetName = "";
        string _storeName = "";
        InventoryReplenishRule_D mainTable = new InventoryReplenishRule_D();

        public InventoryReplenishRule_D MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        public string StoreName
        {
            set { _storeName = value; }
            get { return _storeName; }
        }

        public string OrderTargetName
        {
            set { _orderTargetName = value; }
            get { return _orderTargetName; }
        }
    }
}
