﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain
{
    public class CouponReplenishRuleViewModel
    {
        string _orderTargetName = "";
        string _storeName = "";
        CouponReplenishRule_D mainTable = new CouponReplenishRule_D();

        public CouponReplenishRule_D MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        public string StoreName
        {
            set { _storeName = value; }
            get { return _storeName; }
        }

        public string OrderTargetName
        {
            set { _orderTargetName = value; }
            get { return _orderTargetName; }
        }
    }
}
