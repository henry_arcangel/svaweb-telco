﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.Operation
{
    public class CardTransferViewModel
    {
        Ord_CardTransfer_H mainTable = new Ord_CardTransfer_H();

        public Ord_CardTransfer_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        Ord_CardTransfer_D detailTable = new Ord_CardTransfer_D();

        public Ord_CardTransfer_D DetailTable
        {
            get { return detailTable; }
            set { detailTable = value; }
        }

    }
}
