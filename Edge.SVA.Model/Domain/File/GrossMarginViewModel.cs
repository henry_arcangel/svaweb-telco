﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class GrossMarginViewModel
    {
        GrossMargin mainTable = new GrossMargin();
        List<Edge.SVA.Model.GrossMargin_MobileNo> detailTable = new List<Edge.SVA.Model.GrossMargin_MobileNo>();

        public GrossMargin MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
        public List<Edge.SVA.Model.GrossMargin_MobileNo> DetailTable
        {
            get { return detailTable; }
            set { detailTable = value; }
        }
    }
}
