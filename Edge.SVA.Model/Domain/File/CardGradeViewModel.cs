﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.File.BasicViewModel;

namespace Edge.SVA.Model.Domain.File
{
    public class CardGradeViewModel
    {
        private CardGrade mainTable = new CardGrade();

        public CardGrade MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
        private MemberClause memberClause = new MemberClause();

        public MemberClause MemberClause
        {
            get { return memberClause; }
            set { memberClause = value; }
        }

        private List<CardGradeHoldCouponRuleViewModel> canHoldCouponGradeList = new List<CardGradeHoldCouponRuleViewModel>();
        /// <summary>
        /// 可以获取的coupongrade
        /// </summary>
        public List<CardGradeHoldCouponRuleViewModel> CanHoldCouponGradeList
        {
            get { return canHoldCouponGradeList; }
        }
        private List<CardGradeHoldCouponRuleViewModel> getCouponGradeList = new List<CardGradeHoldCouponRuleViewModel>();
        /// <summary>
        /// 升级后要得到的coupongrade
        /// </summary>
        public List<CardGradeHoldCouponRuleViewModel> GetCouponGradeList
        {
            get { return getCouponGradeList; }
        }
        private List<CardGradeHoldCouponRuleViewModel> canHoldCouponGradeListAdd = new List<CardGradeHoldCouponRuleViewModel>();
        /// <summary>
        /// 可以获取的coupongrade
        /// </summary>
        public List<CardGradeHoldCouponRuleViewModel> CardGradeHoldCouponRuleListAdd
        {
            get { return canHoldCouponGradeListAdd; }
        }

        private List<CardGradeHoldCouponRuleViewModel> canHoldCouponGradeListDelete = new List<CardGradeHoldCouponRuleViewModel>();
        /// <summary>
        /// 可以获取的coupongrade
        /// </summary>
        public List<CardGradeHoldCouponRuleViewModel> CardGradeHoldCouponRuleListDelete
        {
            get { return canHoldCouponGradeListDelete; }
        }

        public bool ValidateCanHoldCouponCount()
        {

            //todo:
            //int count = 0;
            //foreach (var item in canHoldCouponGradeList)
            //{
            //    if (item.MainTable.HoldCount.HasValue)
            //    {
            //        count += item.MainTable.HoldCount.Value;
            //    }
            //}
            //if (count >= 1)
            //{
            //    if (mainTable.HoldCouponCount.HasValue)
            //    {
            //        if (mainTable.HoldCouponCount.Value < count)
            //        {
            //            return false;
            //        }
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            return true;
        }
    }
}
