﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class CompanyViewModel
    {
        Company mainTable = new Company();

        public Company MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
