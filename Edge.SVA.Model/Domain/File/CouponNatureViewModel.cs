﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class CouponNatureViewModel
    {
        CouponNature mainTable = new CouponNature();

        public CouponNature MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
