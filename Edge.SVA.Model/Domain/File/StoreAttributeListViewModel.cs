﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class StoreAttributeListViewModel
    {
        StoreAttributeList mainTable = new StoreAttributeList();

        public StoreAttributeList MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
