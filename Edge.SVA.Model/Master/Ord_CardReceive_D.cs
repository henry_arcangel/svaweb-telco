﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 收货确认单子表
	/// </summary>
	[Serializable]
	public partial class Ord_CardReceive_D
	{
		public Ord_CardReceive_D()
		{}
		#region Model
		private int _keyid;
		private string _cardreceivenumber;
		private int? _cardtypeid;
		private int? _cardgradeid;
		private string _description;
		private int? _orderqty;
		private int? _actualqty;
		private string _firstcardnumber;
		private string _endcardnumber;
		private string _batchcardcode;
		private int? _cardstockstatus=2;
		private DateTime? _receivedatetime= DateTime.Now;
        private int? _orderamount;
        private int? _actualamount;
        private int? _orderpoint;
        private int? _actualpoint;
		/// <summary>
		/// 
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 订单编号，主键
		/// </summary>
		public string CardReceiveNumber
		{
			set{ _cardreceivenumber=value;}
			get{return _cardreceivenumber;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡级别ID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 描述。（例如改为损坏状态时填写的具体描述）
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 订货的订单数量
		/// </summary>
		public int? OrderQTY
		{
			set{ _orderqty=value;}
			get{return _orderqty;}
		}
		/// <summary>
		/// 实际收到的数量
		/// </summary>
		public int? ActualQTY
		{
			set{ _actualqty=value;}
			get{return _actualqty;}
		}
		/// <summary>
		/// 实际收货批次的首Card号
		/// </summary>
		public string FirstCardNumber
		{
			set{ _firstcardnumber=value;}
			get{return _firstcardnumber;}
		}
		/// <summary>
		/// 实际收货批次的末Card号
		/// </summary>
		public string EndCardNumber
		{
			set{ _endcardnumber=value;}
			get{return _endcardnumber;}
		}
		/// <summary>
		/// FirstCardNumber的批次号编码
		/// </summary>
		public string BatchCardCode
		{
			set{ _batchcardcode=value;}
			get{return _batchcardcode;}
		}
		/// <summary>
		/// 收到的Card状况。默认 2 2. Good For Release (總部確認收貨) 3. Damaged (總部發現有優惠劵損壞)
		/// </summary>
		public int? CardStockStatus
		{
			set{ _cardstockstatus=value;}
			get{return _cardstockstatus;}
		}
		/// <summary>
		/// 创建此明细的时间
		/// </summary>
		public DateTime? ReceiveDateTime
		{
			set{ _receivedatetime=value;}
			get{return _receivedatetime;}
		}
        /// <summary>
        /// 订单金额
        /// </summary>
        public int? OrderAmount
        {
            set { _orderamount = value; }
            get { return _orderamount; }
        }
        /// <summary>
        /// 实际收货金额
        /// </summary>
        public int? ActualAmount
        {
            set { _actualamount = value; }
            get { return _actualamount; }
        }
        /// <summary>
        /// 订单积分
        /// </summary>
        public int? OrderPoint
        {
            set { _orderpoint = value; }
            get { return _orderpoint; }
        }
        /// <summary>
        /// 实际收货积分
        /// </summary>
        public int? ActualPoint
        {
            set { _actualpoint = value; }
            get { return _actualpoint; }
        }
		#endregion Model

	}
}

