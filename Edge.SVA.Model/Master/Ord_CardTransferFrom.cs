﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Ord_CardTransferFrom:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Ord_CardTransferFrom
	{
		public Ord_CardTransferFrom()
		{}
		#region Model
		private string _cardtransfernumber;
		private string _cardnumber;
		private int? _cardtypeid;
		private DateTime _cardissuedate;
		private DateTime _cardexpirydate;
		private int _cardgradeid;
		private int? _memberid;
		private int? _batchcardid;
		private int _status=0;
		private int _usedcount=0;
		private string _cardpassword;
		private int? _totalpoints=0;
		private decimal? _totalamount=0M;
		private string _parentcardnumber;
		private int? _cardforfeitpoints=0;
		private decimal? _cardforfeitamount=0M;
		private int? _resetpassword=0;
		private DateTime? _cardamountexpirydate;
		private DateTime? _cardpointexpirydate;
		private int? _cumulativeearnpoints;
		private decimal? _cumulativeconsumptionamt;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private DateTime? _passwordexpirydate;
		private int? _pwdexpirypromptdays;
		private string _approvedcode;
		private int? _stockstatus;
		private int? _issuestoreid;
		private int? _activestoreid;
		private DateTime? _activedate;
		private DateTime? _pickupflag;
		/// <summary>
		///11 
		/// </summary>
		public string CardTransferNumber
		{
			set{ _cardtransfernumber=value;}
			get{return _cardtransfernumber;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime CardIssueDate
		{
			set{ _cardissuedate=value;}
			get{return _cardissuedate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime CardExpiryDate
		{
			set{ _cardexpirydate=value;}
			get{return _cardexpirydate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? MemberID
		{
			set{ _memberid=value;}
			get{return _memberid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? BatchCardID
		{
			set{ _batchcardid=value;}
			get{return _batchcardid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int UsedCount
		{
			set{ _usedcount=value;}
			get{return _usedcount;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string CardPassword
		{
			set{ _cardpassword=value;}
			get{return _cardpassword;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? TotalPoints
		{
			set{ _totalpoints=value;}
			get{return _totalpoints;}
		}
		/// <summary>
		///11 
		/// </summary>
		public decimal? TotalAmount
		{
			set{ _totalamount=value;}
			get{return _totalamount;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string ParentCardNumber
		{
			set{ _parentcardnumber=value;}
			get{return _parentcardnumber;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CardForfeitPoints
		{
			set{ _cardforfeitpoints=value;}
			get{return _cardforfeitpoints;}
		}
		/// <summary>
		///11 
		/// </summary>
		public decimal? CardForfeitAmount
		{
			set{ _cardforfeitamount=value;}
			get{return _cardforfeitamount;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? ResetPassword
		{
			set{ _resetpassword=value;}
			get{return _resetpassword;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CardAmountExpiryDate
		{
			set{ _cardamountexpirydate=value;}
			get{return _cardamountexpirydate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CardPointExpiryDate
		{
			set{ _cardpointexpirydate=value;}
			get{return _cardpointexpirydate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CumulativeEarnPoints
		{
			set{ _cumulativeearnpoints=value;}
			get{return _cumulativeearnpoints;}
		}
		/// <summary>
		///11 
		/// </summary>
		public decimal? CumulativeConsumptionAmt
		{
			set{ _cumulativeconsumptionamt=value;}
			get{return _cumulativeconsumptionamt;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? PasswordExpiryDate
		{
			set{ _passwordexpirydate=value;}
			get{return _passwordexpirydate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? PWDExpiryPromptDays
		{
			set{ _pwdexpirypromptdays=value;}
			get{return _pwdexpirypromptdays;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string ApprovedCode
		{
			set{ _approvedcode=value;}
			get{return _approvedcode;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? StockStatus
		{
			set{ _stockstatus=value;}
			get{return _stockstatus;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? IssueStoreID
		{
			set{ _issuestoreid=value;}
			get{return _issuestoreid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? ActiveStoreID
		{
			set{ _activestoreid=value;}
			get{return _activestoreid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? ActiveDate
		{
			set{ _activedate=value;}
			get{return _activedate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? PickupFlag
		{
			set{ _pickupflag=value;}
			get{return _pickupflag;}
		}
		#endregion Model

	}
}

