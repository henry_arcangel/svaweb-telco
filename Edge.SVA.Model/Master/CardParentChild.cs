﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡父子关系表。 
	///（目前为方便svaweb的设置UI，可能取代card表中的parentcardnumber字段）
	/// </summary>
	[Serializable]
	public partial class CardParentChild
	{
		public CardParentChild()
		{}
		#region Model
		private string _parentcardnumber;
		private string _childcardnumber;
		/// <summary>
		/// 主卡
		/// </summary>
		public string ParentCardNumber
		{
			set{ _parentcardnumber=value;}
			get{return _parentcardnumber;}
		}
		/// <summary>
		/// 子卡
		/// </summary>
		public string ChildCardNumber
		{
			set{ _childcardnumber=value;}
			get{return _childcardnumber;}
		}
		#endregion Model

	}
}

