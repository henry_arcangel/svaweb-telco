﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 面向供应商的订货单（订货实体Coupon）
    ///注：工作流程：总部向供应商下订单时，提供需要的Coupon号码（首号码 和 数量）。供应商根据提供的号码 提供Coupon。
    ///修改内容： @2014-06-04：
    ///CouponTypeID和QTY 移到 H 表，一个订单只能下一个CouponType。多个CouponType需要另外开单。 Detail表中的CouponType作为冗余字段。
    ///
    /// </summary>
    [Serializable]
    public partial class Ord_OrderToSupplier_H
    {
        public Ord_OrderToSupplier_H()
        { }
        #region Model
        private string _ordersuppliernumber;
        private int? _supplierid;
        private string _ordersupplierdesc;
        private int? _ordertype = 0;
        private string _referenceno;
        private int? _sendmethod;
        private string _sendaddress;
        private string _supplieraddress;
        private string _suppliertcontactname;
        private string _supplierphone;
        private string _supplieremail;
        private string _suppliermobile;
        private int _storeid;
        private string _storecontactname;
        private string _storephone;
        private string _storeemail;
        private string _storemobile;
        private string _remark;
        private int? _isprovidenumber = 0;
        private DateTime? _createdbusdate;
        private DateTime? _approvebusdate;
        private string _approvalcode;
        private string _approvestatus;
        private DateTime? _approveon;
        private int? _approveby;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
        private int _coupontypeid;
        private int _couponqty;
        private int? _companyid;
        private int? _packageqty = 1;
        private string _subject;
        /// <summary>
        /// 提交供应商订单单号，主键
        /// </summary>
        public string OrderSupplierNumber
        {
            set { _ordersuppliernumber = value; }
            get { return _ordersuppliernumber; }
        }
        /// <summary>
        /// 供应商主键，外键
        /// </summary>
        public int? SupplierID
        {
            set { _supplierid = value; }
            get { return _supplierid; }
        }
        /// <summary>
        /// 订单供应商描述
        /// </summary>
        public string OrderSupplierDesc
        {
            set { _ordersupplierdesc = value; }
            get { return _ordersupplierdesc; }
        }
        /// <summary>
        /// 订单类型。 0：手动。1：自动。 默认：1
        /// </summary>
        public int? OrderType
        {
            set { _ordertype = value; }
            get { return _ordertype; }
        }
        /// <summary>
        /// 参考编号。如果是由其他单据产生的，就填此单据号
        /// </summary>
        public string ReferenceNo
        {
            set { _referenceno = value; }
            get { return _referenceno; }
        }
        /// <summary>
        /// 送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。
        /// </summary>
        public int? SendMethod
        {
            set { _sendmethod = value; }
            get { return _sendmethod; }
        }
        /// <summary>
        /// 送货地址。（总部地址）
        /// </summary>
        public string SendAddress
        {
            set { _sendaddress = value; }
            get { return _sendaddress; }
        }
        /// <summary>
        /// 供应商地址
        /// </summary>
        public string SupplierAddress
        {
            set { _supplieraddress = value; }
            get { return _supplieraddress; }
        }
        /// <summary>
        /// 供应商联系人
        /// </summary>
        public string SuppliertContactName
        {
            set { _suppliertcontactname = value; }
            get { return _suppliertcontactname; }
        }
        /// <summary>
        /// 供应商联系电话
        /// </summary>
        public string SupplierPhone
        {
            set { _supplierphone = value; }
            get { return _supplierphone; }
        }
        /// <summary>
        /// 供应商联系邮箱
        /// </summary>
        public string SupplierEmail
        {
            set { _supplieremail = value; }
            get { return _supplieremail; }
        }
        /// <summary>
        /// 供应商联系手机
        /// </summary>
        public string SupplierMobile
        {
            set { _suppliermobile = value; }
            get { return _suppliermobile; }
        }
        /// <summary>
        /// 店铺ID （总部）
        /// </summary>
        public int StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        /// <summary>
        /// 总部联系人
        /// </summary>
        public string StoreContactName
        {
            set { _storecontactname = value; }
            get { return _storecontactname; }
        }
        /// <summary>
        /// 总部联系电话
        /// </summary>
        public string StorePhone
        {
            set { _storephone = value; }
            get { return _storephone; }
        }
        /// <summary>
        /// 总部联系邮箱
        /// </summary>
        public string StoreEmail
        {
            set { _storeemail = value; }
            get { return _storeemail; }
        }
        /// <summary>
        /// 总部联系手机
        /// </summary>
        public string StoreMobile
        {
            set { _storemobile = value; }
            get { return _storemobile; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }

        /// <summary>
        /// 标题用于RRG PR
        /// </summary>
        public string Subject
        {
            set { _subject = value; }
            get { return _subject; }
        }

        /// <summary>
        /// 是否需要向供应商提供号码。（供应商根据提供的号码打印）。 默认0。  0：不提供。 1：提供
        /// </summary>
        public int? IsProvideNumber
        {
            set { _isprovidenumber = value; }
            get { return _isprovidenumber; }
        }
        /// <summary>
        /// 单据创建时的busdate
        /// </summary>
        public DateTime? CreatedBusDate
        {
            set { _createdbusdate = value; }
            get { return _createdbusdate; }
        }
        /// <summary>
        /// 单据批核时的busdate
        /// </summary>
        public DateTime? ApproveBusDate
        {
            set { _approvebusdate = value; }
            get { return _approvebusdate; }
        }
        /// <summary>
        /// 批核时产生授权号，并通知前台
        /// </summary>
        public string ApprovalCode
        {
            set { _approvalcode = value; }
            get { return _approvalcode; }
        }
        /// <summary>
        /// 单据状态。状态： P：prepare。  A:Approve 。 V：Void
        /// </summary>
        public string ApproveStatus
        {
            set { _approvestatus = value; }
            get { return _approvestatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? ApproveOn
        {
            set { _approveon = value; }
            get { return _approveon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ApproveBy
        {
            set { _approveby = value; }
            get { return _approveby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// <summary>
        /// 优惠劵类型ID
        /// </summary>
        public int CouponTypeID
        {
            set { _coupontypeid = value; }
            get { return _coupontypeid; }
        }
        /// <summary>
        /// 订货数量
        /// </summary>
        public int CouponQty
        {
            set { _couponqty = value; }
            get { return _couponqty; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CompanyID
        {
            set { _companyid = value; }
            get { return _companyid; }
        }
        /// <summary>
        /// RRG需要增加，目前不用
        /// </summary>
        public int? PackageQty
        {
            set { _packageqty = value; }
            get { return _packageqty; }
        }
        #endregion Model

    }
}

