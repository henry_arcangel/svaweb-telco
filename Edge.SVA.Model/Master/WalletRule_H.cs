﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 学校表
	/// </summary>
	[Serializable]
	public partial class WalletRule_H
	{
        public WalletRule_H()
		{}
		#region Model
		private string _walletrulecode;
		private string _description;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;


		/// <summary>
        /// WalletRuleCode
		/// </summary>
		public string WalletRuleCode
		{
            set { _walletrulecode = value; }
            get { return _walletrulecode; }
		}
		/// <summary>
        /// Description
		/// </summary>
		public string Description
		{
            set { _description = value; }
            get { return _description; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

