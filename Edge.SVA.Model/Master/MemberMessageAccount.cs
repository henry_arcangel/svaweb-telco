﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 会员消息服务账号
    /// </summary>
    [Serializable]
    public partial class MemberMessageAccount
    {
        public MemberMessageAccount()
        { }
        #region Model
        private int _messageaccountid;
        private int _memberid;
        private int _messageservicetypeid;
        private string _accountnumber;
        private int _status = 1;
        private string _note;
        private int? _isprefer = 0;
        private string _tokenuid;
        private string _tokenstr;
        private DateTime? _tokenupdatedate;
        private int? _promotionflag = 0;
        private int? _verifyflag = 0;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
        /// <summary>
        /// 账号主键ID
        /// </summary>
        public int MessageAccountID
        {
            set { _messageaccountid = value; }
            get { return _messageaccountid; }
        }
        /// <summary>
        /// 会员ID，外键
        /// </summary>
        public int MemberID
        {
            set { _memberid = value; }
            get { return _memberid; }
        }
        /// <summary>
        /// MessageServiceType 表外键
        /// </summary>
        public int MessageServiceTypeID
        {
            set { _messageservicetypeid = value; }
            get { return _messageservicetypeid; }
        }
        /// <summary>
        /// 账号
        /// </summary>
        public string AccountNumber
        {
            set { _accountnumber = value; }
            get { return _accountnumber; }
        }
        /// <summary>
        /// 状态。0：无效。 1：生效。

        /// </summary>
        public int Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            set { _note = value; }
            get { return _note; }
        }
        /// <summary>
        /// 金额积分变动时，是否使用此账号发送消息。0: 不使用。 1：使用。 默认0。 一个会员只能有一个 IsPrefer=1的账号。  可以全部为0.
        /// </summary>
        public int? IsPrefer
        {
            set { _isprefer = value; }
            get { return _isprefer; }
        }
        /// <summary>
        /// 登录网站的UID
        /// </summary>
        public string TokenUID
        {
            set { _tokenuid = value; }
            get { return _tokenuid; }
        }
        /// <summary>
        /// 登录网站的Token
        /// </summary>
        public string TokenStr
        {
            set { _tokenstr = value; }
            get { return _tokenstr; }
        }
        /// <summary>
        /// 登录网站的Token的更新
        /// </summary>
        public DateTime? TokenUpdateDate
        {
            set { _tokenupdatedate = value; }
            get { return _tokenupdatedate; }
        }
        /// <summary>
        /// 此账号是否接收优惠消息。默认0，不接收。 （这个功能当前只是做记录。不做实际的业务逻辑操作）
        /// </summary>
        public int? PromotionFlag
        {
            set { _promotionflag = value; }
            get { return _promotionflag; }
        }
        /// <summary>
        /// 是否经过验证。 0：没有。 1：已经验证。  默认为0
        /// </summary>
        public int? VerifyFlag
        {
            set { _verifyflag = value; }
            get { return _verifyflag; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        #endregion Model

    }
}

