﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 系统用户消息通知设定。子表
    /// </summary>
    [Serializable]
    public partial class UserMessageSetting_D
    {
        public UserMessageSetting_D()
        { }
        #region Model
        private int _keyid;
        private string _usermessagecode;
        private int? _receiveuserid;
        private int _messageservicetypeid;
        private string _accountnumber;
        /// <summary>
        /// 自增长主键
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 通知编号
        /// </summary>
        public string UserMessageCode
        {
            set { _usermessagecode = value; }
            get { return _usermessagecode; }
        }
        /// <summary>
        /// 接收人员ID
        /// </summary>
        public int? ReceiveUserID
        {
            set { _receiveuserid = value; }
            get { return _receiveuserid; }
        }
        /// <summary>
        /// 消息发送方式 外键
        /// </summary>
        public int MessageServiceTypeID
        {
            set { _messageservicetypeid = value; }
            get { return _messageservicetypeid; }
        }
        /// <summary>
        /// 账号。
        /// </summary>
        public string AccountNumber
        {
            set { _accountnumber = value; }
            get { return _accountnumber; }
        }
        #endregion Model

    }
}

