﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 创建Member记录的子表
	/// </summary>
	[Serializable]
	public partial class Ord_CreateMember_D
	{
		public Ord_CreateMember_D()
		{}
		#region Model
		private int _keyid;
		private string _createmembernumber;
		private string _countrycode;
		private string _mobilenumber;
		private string _engfamilyname;
		private string _enggivenname;
		private string _chifamilyname;
		private string _chigivenname;
		private DateTime? _birthday;
		private int? _gender=0;
		private string _homeaddress;
		private string _email;
		private string _facebook;
		private string _qq;
		private string _msn;
		private string _weibo;
		private string _othercontact;
		/// <summary>
		///11 主键，自增长
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		///11 订单号，主键
		/// </summary>
		public string CreateMemberNumber
		{
			set{ _createmembernumber=value;}
			get{return _createmembernumber;}
		}
		/// <summary>
		///11 国家Code
		/// </summary>
		public string CountryCode
		{
			set{ _countrycode=value;}
			get{return _countrycode;}
		}
		/// <summary>
		///11 手机号码（CountryCode+MobileNumber= 注册号）
		/// </summary>
		public string MobileNumber
		{
			set{ _mobilenumber=value;}
			get{return _mobilenumber;}
		}
		/// <summary>
		///11 会员英文名（姓）
		/// </summary>
		public string EngFamilyName
		{
			set{ _engfamilyname=value;}
			get{return _engfamilyname;}
		}
		/// <summary>
		///11 会员英文名（名）
		/// </summary>
		public string EngGivenName
		{
			set{ _enggivenname=value;}
			get{return _enggivenname;}
		}
		/// <summary>
		///11 会员中文名（姓）
		/// </summary>
		public string ChiFamilyName
		{
			set{ _chifamilyname=value;}
			get{return _chifamilyname;}
		}
		/// <summary>
		///11 会员中文名（名）
		/// </summary>
		public string ChiGivenName
		{
			set{ _chigivenname=value;}
			get{return _chigivenname;}
		}
		/// <summary>
		///11 会员生日（出生日期）
		/// </summary>
		public DateTime? Birthday
		{
			set{ _birthday=value;}
			get{return _birthday;}
		}
		/// <summary>
		///11 会员性别
		///null或0：保密。1：男性。 2：女性
		/// </summary>
		public int? Gender
		{
			set{ _gender=value;}
			get{return _gender;}
		}
		/// <summary>
		///11 家庭地址
		/// </summary>
		public string HomeAddress
		{
			set{ _homeaddress=value;}
			get{return _homeaddress;}
		}
		/// <summary>
		///11 邮箱
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		///11 FaceBook账号
		/// </summary>
		public string Facebook
		{
			set{ _facebook=value;}
			get{return _facebook;}
		}
		/// <summary>
		///11 QQ号码
		/// </summary>
		public string QQ
		{
			set{ _qq=value;}
			get{return _qq;}
		}
		/// <summary>
		///11 MSN账号
		/// </summary>
		public string MSN
		{
			set{ _msn=value;}
			get{return _msn;}
		}
		/// <summary>
		///11 微博账号
		/// </summary>
		public string Weibo
		{
			set{ _weibo=value;}
			get{return _weibo;}
		}
		/// <summary>
		///11 其他联系方式
		/// </summary>
		public string OtherContact
		{
			set{ _othercontact=value;}
			get{return _othercontact;}
		}
		#endregion Model

	}
}

