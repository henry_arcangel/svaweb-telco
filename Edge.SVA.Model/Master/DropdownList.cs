﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// DropdownList:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class DropdownList
	{
		public DropdownList()
		{}
		#region Model
		private string _typecode;
		private string _description;
		private string _code;
		private string _text;
		private int? _orderid;
		private string _lan;
		/// <summary>
		/// 类型编码
		/// </summary>
		public string TypeCode
		{
			set{ _typecode=value;}
			get{return _typecode;}
		}
		/// <summary>
		/// 说明
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 页面的dropdownlist的value值
		/// </summary>
		public string Code
		{
			set{ _code=value;}
			get{return _code;}
		}
		/// <summary>
		/// 页面的dropdownlist的Text值
		/// </summary>
		public string Text
		{
			set{ _text=value;}
			get{return _text;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OrderID
		{
			set{ _orderid=value;}
			get{return _orderid;}
		}
		/// <summary>
		/// en-us  zh-cn  zh-hk
		/// </summary>
		public string Lan
		{
			set{ _lan=value;}
			get{return _lan;}
		}
		#endregion Model

	}
}

