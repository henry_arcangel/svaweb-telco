﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 促销命中表的指定货品列表
	/// </summary>
	[Serializable]
	public partial class Promotion_Hit_PLU
	{
		public Promotion_Hit_PLU()
		{}
		#region Model
		private int _keyid;
		private string _promotioncode;
		private int _hitseq;
		private int? _hitpluseq=0;
		private string _entitynum;
		private int? _entitytype=0;
		private int? _hitsign=1;
		private int? _pluhittype;
		/// <summary>
		///11 用户自己填，会按照这个来排序
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		///11 促销编码
		/// </summary>
		public string PromotionCode
		{
			set{ _promotioncode=value;}
			get{return _promotioncode;}
		}
		/// <summary>
		///11 Promotion_Hit表序号
		/// </summary>
		public int HitSeq
		{
			set{ _hitseq=value;}
			get{return _hitseq;}
		}
		/// <summary>
		///11 序号。将根据序号排序。默认0.
		/// </summary>
		public int? HitPLUSeq
		{
			set{ _hitpluseq=value;}
			get{return _hitpluseq;}
		}
		/// <summary>
		///11 号码
		/// </summary>
		public string EntityNum
		{
			set{ _entitynum=value;}
			get{return _entitynum;}
		}
		/// <summary>
		///11 决定EntityNum中内容的含义。默认0
		///0：所有货品。EntityNum中为空
		///1：EntityNum内容为 prodcode。 销售EntityNum中指定货品时，可以生效
		///2：EntityNum内容为 DepartCode。。 销售EntityNum中指定部门的货品时，可以生效
		///3：EntityNum内容为 TenderCode。 使用EntityNum中指定货币支付时，可以生效
		/// </summary>
		public int? EntityType
		{
			set{ _entitytype=value;}
			get{return _entitytype;}
		}
		/// <summary>
		///11 命中的符号。默认1
		///1：此记录为命中条件。 
		///0：此记录为非命中条件。（即指定的货品或部门或tender等不参与促销，除此都参与）
		///不参加促销时，不考虑hit条件
		/// </summary>
		public int? HitSign
		{
			set{ _hitsign=value;}
			get{return _hitsign;}
		}
		/// <summary>
		///11 列表中的货品或者部门的属性。
		///1： 命中条件。 （和Promotion_Hit 中的条件有关。比如hit条件是命中数量，那么就是指这里填写的货品的数量，其他货品不计算）
		///2： 过滤条件。（此时HitSign 属性有效，指参与的货品是否参与促销或排除在外。会产生PromotionHitPLUList表数据）
		///3： 命中条件。 （和1 的区别在于，和Promotion_Hit 中的条件无关。 只有销售清单中包含这些货品，促销才成立。）
		///
		///同类型货品列表，互相之间的关系
		///1：根据 Pormotion_Hit的设置，决定or， and的关系
		///2：根据列表的输入次序，过滤条件逐条累加，后面的可以覆盖前面的。
		///3：or 的关系。 销售清单中包含任何列表中的货品即可能命中。
		/// </summary>
		public int? PLUHitType
		{
			set{ _pluhittype=value;}
			get{return _pluhittype;}
		}
		#endregion Model

	}
}

