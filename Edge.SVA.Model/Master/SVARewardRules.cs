﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 会员推荐新会员奖励规则
	/// </summary>
	[Serializable]
	public partial class SVARewardRules
	{
		public SVARewardRules()
		{}
		#region Model
		private int _svarewardrulesid;
		private int _cardgradeid;
		private int? _svarewardtype=0;
		private string _svarewardrulescode;
		private decimal? _rewardamount;
		private int? _rewardpoint;
		private int? _rewardcoupontypeid;
		private int? _rewardcouponcount;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private int _status=1;
		private string _note;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		/// <summary>
		///11 主键
		/// </summary>
		public int SVARewardRulesID
		{
			set{ _svarewardrulesid=value;}
			get{return _svarewardrulesid;}
		}
		/// <summary>
		///11 卡级别ID
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		///11 奖励类型，默认0：
		///0： 一般
		///1：推荐新会员奖励。
		///2：新会员奖励
		///3：会员share to facebook奖励
		/// </summary>
		public int? SVARewardType
		{
			set{ _svarewardtype=value;}
			get{return _svarewardtype;}
		}
		/// <summary>
		///11 编号
		/// </summary>
		public string SVARewardRulesCode
		{
			set{ _svarewardrulescode=value;}
			get{return _svarewardrulescode;}
		}
		/// <summary>
		///11 奖励金额
		/// </summary>
		public decimal? RewardAmount
		{
			set{ _rewardamount=value;}
			get{return _rewardamount;}
		}
		/// <summary>
		///11 奖励积分
		/// </summary>
		public int? RewardPoint
		{
			set{ _rewardpoint=value;}
			get{return _rewardpoint;}
		}
		/// <summary>
		///11 奖励优惠劵类型
		/// </summary>
		public int? RewardCouponTypeID
		{
			set{ _rewardcoupontypeid=value;}
			get{return _rewardcoupontypeid;}
		}
		/// <summary>
		///11 奖励优惠劵数量
		/// </summary>
		public int? RewardCouponCount
		{
			set{ _rewardcouponcount=value;}
			get{return _rewardcouponcount;}
		}
		/// <summary>
		///11 生效日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		///11 结束日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		///11 状态： 0：无效。 1：有效。默认1.
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		///11 备注
		/// </summary>
		public string Note
		{
			set{ _note=value;}
			get{return _note;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

