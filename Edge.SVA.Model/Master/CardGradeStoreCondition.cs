﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡级别的店铺条件
	/// </summary>
	[Serializable]
	public partial class CardGradeStoreCondition
	{
		public CardGradeStoreCondition()
		{}
		#region Model
		private int _cardgradestoreconditionid;
		private int _cardgradeid;
		private int? _storeconditiontype=1;
		private int? _conditiontype=1;
		private int? _conditionid;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CardGradeStoreConditionID
		{
			set{ _cardgradestoreconditionid=value;}
			get{return _cardgradestoreconditionid;}
		}
		/// <summary>
		/// CardGrade ID
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 条件类型。 1：发布店铺。2：使用店铺
		/// </summary>
		public int? StoreConditionType
		{
			set{ _storeconditiontype=value;}
			get{return _storeconditiontype;}
		}
		/// <summary>
		/// 输入的条件类型。1：brand。2：Location。3：store
		/// </summary>
		public int? ConditionType
		{
			set{ _conditiontype=value;}
			get{return _conditiontype;}
		}
		/// <summary>
		/// 条件具体的ID。 例如：ConditionType=1时，输入的为brandID
		/// </summary>
		public int? ConditionID
		{
			set{ _conditionid=value;}
			get{return _conditionid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

