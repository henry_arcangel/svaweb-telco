﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 学校表
	/// </summary>
	[Serializable]
	public partial class School
	{
		public School()
		{}
		#region Model
		private int _schoolid;
		private string _schoolcode;
		private string _schoolname1;
		private string _schoolname2;
		private string _schoolname3;
		private string _schoolarea1;
		private string _schoolarea2;
		private string _schoolarea3;
		private string _schooldistrict1;
		private string _schooldistrict2;
		private string _schooldistrict3;
		private string _schoolnote;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int SchoolID
		{
			set{ _schoolid=value;}
			get{return _schoolid;}
		}
		/// <summary>
		/// 学校Code
		/// </summary>
		public string SchoolCode
		{
			set{ _schoolcode=value;}
			get{return _schoolcode;}
		}
		/// <summary>
		/// 学校名称1
		/// </summary>
		public string SchoolName1
		{
			set{ _schoolname1=value;}
			get{return _schoolname1;}
		}
		/// <summary>
		/// 学校名称2
		/// </summary>
		public string SchoolName2
		{
			set{ _schoolname2=value;}
			get{return _schoolname2;}
		}
		/// <summary>
		/// 学校名称3
		/// </summary>
		public string SchoolName3
		{
			set{ _schoolname3=value;}
			get{return _schoolname3;}
		}
		/// <summary>
		/// 学校所属地区1
		/// </summary>
		public string SchoolArea1
		{
			set{ _schoolarea1=value;}
			get{return _schoolarea1;}
		}
		/// <summary>
		/// 学校所属地区2
		/// </summary>
		public string SchoolArea2
		{
			set{ _schoolarea2=value;}
			get{return _schoolarea2;}
		}
		/// <summary>
		/// 学校所属地区3
		/// </summary>
		public string SchoolArea3
		{
			set{ _schoolarea3=value;}
			get{return _schoolarea3;}
		}
		/// <summary>
		/// 学校所属区域1
		/// </summary>
		public string SchoolDistrict1
		{
			set{ _schooldistrict1=value;}
			get{return _schooldistrict1;}
		}
		/// <summary>
		/// 学校所属区域2
		/// </summary>
		public string SchoolDistrict2
		{
			set{ _schooldistrict2=value;}
			get{return _schooldistrict2;}
		}
		/// <summary>
		/// 学校所属区域3
		/// </summary>
		public string SchoolDistrict3
		{
			set{ _schooldistrict3=value;}
			get{return _schooldistrict3;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string SchoolNote
		{
			set{ _schoolnote=value;}
			get{return _schoolnote;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

