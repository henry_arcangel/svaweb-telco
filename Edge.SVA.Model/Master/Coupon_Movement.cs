﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵操作流水记录表
	/// </summary>
	[Serializable]
	public partial class Coupon_Movement
	{
		public Coupon_Movement()
		{}
		#region Model
		private int _keyid;
		private int _oprid;
		private string _cardnumber;
		private int _cardtypeid;
		private string _couponnumber;
		private int _coupontypeid;
		private int? _refkeyid;
		private int? _refreceivekeyid;
		private string _reftxnno;
		private decimal? _openbal;
		private decimal? _amount;
		private decimal? _closebal;
		private DateTime? _busdate;
		private DateTime? _txndate;
		private string _tendercode;
		private decimal? _tenderrate=1M;
		private string _remark;
		private string _securitycode;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		/// <summary>
		/// 自增长主键ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 操作类型ID：
//101：会员获取Coupon（购买，消费赠送...）
//102：会员获得转赠。
//103：会员转赠他人。
//104：过期作废。
//105：会员使用coupon。
//106：void操作。
		/// </summary>
		public int OprID
		{
			set{ _oprid=value;}
			get{return _oprid;}
		}
		/// <summary>
		/// 卡号
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 优惠劵号码
		/// </summary>
		public string CouponNumber
		{
			set{ _couponnumber=value;}
			get{return _couponnumber;}
		}
		/// <summary>
		/// 优惠劵类型
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 有关联的movement记录的KeyID。
		/// </summary>
		public int? RefKeyID
		{
			set{ _refkeyid=value;}
			get{return _refkeyid;}
		}
		/// <summary>
		/// 如果有ReceiveTxn记录触发，则记录ReceiveTxn的KeyID
		/// </summary>
		public int? RefReceiveKeyID
		{
			set{ _refreceivekeyid=value;}
			get{return _refreceivekeyid;}
		}
		/// <summary>
		/// 如果有ReceiveTxn记录触发，则记录ReceiveTxn的TxnNo
		/// </summary>
		public string RefTxnNo
		{
			set{ _reftxnno=value;}
			get{return _reftxnno;}
		}
		/// <summary>
		/// 操作之前的金额值。（现金卷时有效。）
		/// </summary>
		public decimal? OpenBal
		{
			set{ _openbal=value;}
			get{return _openbal;}
		}
		/// <summary>
		/// 操作的金额值。（现金卷时有效。）
		/// </summary>
		public decimal? Amount
		{
			set{ _amount=value;}
			get{return _amount;}
		}
		/// <summary>
		/// 操作之后的金额值。（现金卷时有效。）
		/// </summary>
		public decimal? CloseBal
		{
			set{ _closebal=value;}
			get{return _closebal;}
		}
		/// <summary>
		/// 交易日期
		/// </summary>
		public DateTime? BusDate
		{
			set{ _busdate=value;}
			get{return _busdate;}
		}
		/// <summary>
		/// 交易时间
		/// </summary>
		public DateTime? Txndate
		{
			set{ _txndate=value;}
			get{return _txndate;}
		}
		/// <summary>
		/// 支付编码
		/// </summary>
		public string TenderCode
		{
			set{ _tendercode=value;}
			get{return _tendercode;}
		}
		/// <summary>
		/// 支付类型的汇率
		/// </summary>
		public decimal? TenderRate
		{
			set{ _tenderrate=value;}
			get{return _tenderrate;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 签名字段。
		/// </summary>
		public string SecurityCode
		{
			set{ _securitycode=value;}
			get{return _securitycode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		#endregion Model

	}
}

