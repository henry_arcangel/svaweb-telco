﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡等级表
	/// </summary>
	[Serializable]
	public partial class CardGrade
	{
		public CardGrade()
		{}
		#region Model
		private int _cardgradeid;
		private string _cardgradecode;
		private int _cardtypeid;
		private string _cardgradename1;
		private string _cardgradename2;
		private string _cardgradename3;
		private int _cardgraderank=0;
		private int? _cardgradeupdmethod=0;
		private int? _cardgradeupdthreshold;
		private decimal? _cardgradediscceiling;
		private decimal? _cardgrademaxamount=0M;
		private int? _cardtypeinitpoints=0;
		private decimal? _cardtypeinitamount=0M;
		private int? _cardconsumebasepoint=0;
		private decimal? _cardpointtoamountrate=0M;
		private decimal? _cardamounttopointrate=0M;
		private int? _forfeitafterexpired=0;
		private int? _isallowstorevalue=1;
		private int _cardpointtransfer=0;
		private int? _cardamounttransfer=0;
		private int? _cardvalidityduration;
		private int? _cardvalidityunit=0;
		private string _cardgradelayoutfile;
		private string _cardgradepicfile;
		private string _cardgradenotes;
		private decimal? _minamountpreadd;
		private decimal? _maxamountpreadd;
		private int? _activeresetexpirydate=0;
		private decimal? _minamountpretransfer;
		private decimal? _maxamountpretransfer;
		private int? _maxpointpretransfer;
		private decimal? _daymaxamounttransfer;
		private int? _daymaxpointtransfer;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private string _cardgradestatementfile;
		private string _cardnummask;
		private string _cardnumpattern;
		private int? _cardcheckdigit;
		private int? _checkdigitmodeid;
		private int? _passwordruleid;
		private int? _campaignid;
		private decimal? _minbalanceamount;
		private int? _minbalancepoint;
		private decimal? _minconsumeamount;
		private int? _graceperiodvalue;
		private int? _graceperiodunit;
		private int? _forfeitamountafterexpired=0;
		private int? _forfeitpointafterexpired=0;
		private int? _minpointpretransfer;
		private int? _isallowconsumptionpoint;
		private int? _isconsecutiveuid=1;
		private int? _uidtocardnumber=0;
		private int? _isimportuidnumber=0;
		private int? _uidcheckdigit=0;
		private int? _cardnumbertouid=0;
		private int? _minpointpreadd=0;
		private int? _maxpointpreadd=0;
		private int? _cardgrademaxpoint=0;
		private string _cardgradeupdhitplu;
		private int? _holdcouponcount;
		private int? _cardgradeupdcoupontypeid;
		private int? _mobileprotectperiodvalue;
		private int? _emailvalidatedperiodvalue;
		private int? _nonvalidatedperiodvalue;
		private int? _sessiontimeoutvalue;
		private int? _loginfailurecount;
		private int? _qrcodeperiodvalue;
		private int? _allowofflineqrcode;
		private string _qrcodeprefix;
		private int? _numberoftransdisplay;
		private int? _numberofcoupondisplay;
		private int? _numberofnewsdisplay;
		private int? _trainingmode;
		/// <summary>
		/// 卡等级ID
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 卡级别编号
		/// </summary>
		public string CardGradeCode
		{
			set{ _cardgradecode=value;}
			get{return _cardgradecode;}
		}
		/// <summary>
		/// 卡等级所属卡种类
		/// </summary>
		public int CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡等级描述1
		/// </summary>
		public string CardGradeName1
		{
			set{ _cardgradename1=value;}
			get{return _cardgradename1;}
		}
		/// <summary>
		/// 卡等级描述2
		/// </summary>
		public string CardGradeName2
		{
			set{ _cardgradename2=value;}
			get{return _cardgradename2;}
		}
		/// <summary>
		/// 卡等级描述3
		/// </summary>
		public string CardGradeName3
		{
			set{ _cardgradename3=value;}
			get{return _cardgradename3;}
		}
		/// <summary>
		/// 等级次序。（升序）

		/// </summary>
		public int CardGradeRank
		{
			set{ _cardgraderank=value;}
			get{return _cardgraderank;}
		}
		/// <summary>
		/// 卡等级升级规则： Null/0: 无。  1：每年消费金额到指定值。 2：每年获取积分到指定值。3：单笔消费金额到达指定值
		/// </summary>
		public int? CardGradeUpdMethod
		{
			set{ _cardgradeupdmethod=value;}
			get{return _cardgradeupdmethod;}
		}
		/// <summary>
		/// 升级界限值
		/// </summary>
		public int? CardGradeUpdThreshold
		{
			set{ _cardgradeupdthreshold=value;}
			get{return _cardgradeupdthreshold;}
		}
		/// <summary>
		/// 等级折扣上限值。记录为扣减掉的折扣。 例如：上限8折，记录为 0.2。  Null/0：没有折扣。
		/// </summary>
		public decimal? CardGradeDiscCeiling
		{
			set{ _cardgradediscceiling=value;}
			get{return _cardgradediscceiling;}
		}
		/// <summary>
		/// 最大存储金额。 默认0   Null/0： 不设上限。
		/// </summary>
		public decimal? CardGradeMaxAmount
		{
			set{ _cardgrademaxamount=value;}
			get{return _cardgrademaxamount;}
		}
		/// <summary>
		/// 卡创建时的初始积分。默认0。  （卡创建时，默认按照此设置，但可在创建时更改）
		/// </summary>
		public int? CardTypeInitPoints
		{
			set{ _cardtypeinitpoints=value;}
			get{return _cardtypeinitpoints;}
		}
		/// <summary>
		/// 卡创建时的初始金额。默认0。  （卡创建时，默认按照此设置，但可在创建时更改）
		/// </summary>
		public decimal? CardTypeInitAmount
		{
			set{ _cardtypeinitamount=value;}
			get{return _cardtypeinitamount;}
		}
		/// <summary>
		/// 消费积分的最低值。默认0  Null/0: 不限最低值。  
		/// </summary>
		public int? CardConsumeBasePoint
		{
			set{ _cardconsumebasepoint=value;}
			get{return _cardconsumebasepoint;}
		}
		/// <summary>
		/// 卡积分兑换金额设置值（例如，设置为10，表示10个积分兑换1元，10为最低兑换值）。默认0    Null/0: 不允许兑换。 

		/// </summary>
		public decimal? CardPointToAmountRate
		{
			set{ _cardpointtoamountrate=value;}
			get{return _cardpointtoamountrate;}
		}
		/// <summary>
		/// 卡金额兑换为积分（例如：设置为10， 表示10元兑换1个积分，10元为最低兑换值）。默认0   Null/0：不允许兑换。

		/// </summary>
		public decimal? CardAmountToPointRate
		{
			set{ _cardamounttopointrate=value;}
			get{return _cardamounttopointrate;}
		}
		/// <summary>
		/// 卡过期时金额和积分是否清0。 默认0.  0： 不清零。 1：清零
		/// </summary>
		public int? ForfeitAfterExpired
		{
			set{ _forfeitafterexpired=value;}
			get{return _forfeitafterexpired;}
		}
		/// <summary>
		/// 是否允许储值。 0: 不允许储值（即不允许增值）。 1：允许。  默认1
		/// </summary>
		public int? IsAllowStoreValue
		{
			set{ _isallowstorevalue=value;}
			get{return _isallowstorevalue;}
		}
		/// <summary>
		/// 是否允许转移积分，默认0   0：不允許； 1：允許同卡级别 2：同卡類型轉贈；3：允許同品牌轉贈；
		/// </summary>
		public int CardPointTransfer
		{
			set{ _cardpointtransfer=value;}
			get{return _cardpointtransfer;}
		}
		/// <summary>
		/// 是否允许转赠金额。默认0.  0：不允許； 1：允許同卡级别  2：同卡類型轉贈； 3：允許同品牌轉贈；
		/// </summary>
		public int? CardAmountTransfer
		{
			set{ _cardamounttransfer=value;}
			get{return _cardamounttransfer;}
		}
		/// <summary>
		/// 卡有效期持续时间长度。
		/// </summary>
		public int? CardValidityDuration
		{
			set{ _cardvalidityduration=value;}
			get{return _cardvalidityduration;}
		}
		/// <summary>
		/// 卡有效期持续时间长度 的单位：默认0，不需要检查CardValidityDuration值  0：永久。 1：年。 2：月。 3：星期。 4：天。5: 有效期不变更。
		/// </summary>
		public int? CardValidityUnit
		{
			set{ _cardvalidityunit=value;}
			get{return _cardvalidityunit;}
		}
		/// <summary>
		/// 可存放卡封面的设计模板，图片文件的相对路径名字
		/// </summary>
		public string CardGradeLayoutFile
		{
			set{ _cardgradelayoutfile=value;}
			get{return _cardgradelayoutfile;}
		}
		/// <summary>
		/// 可存放卡的图片
		/// </summary>
		public string CardGradePicFile
		{
			set{ _cardgradepicfile=value;}
			get{return _cardgradepicfile;}
		}
		/// <summary>
		/// 条例说明
		/// </summary>
		public string CardGradeNotes
		{
			set{ _cardgradenotes=value;}
			get{return _cardgradenotes;}
		}
		/// <summary>
		/// 单笔最小增值金额
		/// </summary>
		public decimal? MinAmountPreAdd
		{
			set{ _minamountpreadd=value;}
			get{return _minamountpreadd;}
		}
		/// <summary>
		/// 单笔最大增值金额
		/// </summary>
		public decimal? MaxAmountPreAdd
		{
			set{ _maxamountpreadd=value;}
			get{return _maxamountpreadd;}
		}
		/// <summary>
		/// 激活时，是否重置有效期。  0：不重置。 1：重置。  默认0；
		/// </summary>
		public int? ActiveResetExpiryDate
		{
			set{ _activeresetexpirydate=value;}
			get{return _activeresetexpirydate;}
		}
		/// <summary>
		/// 单笔最小转赠金额
		/// </summary>
		public decimal? MinAmountPreTransfer
		{
			set{ _minamountpretransfer=value;}
			get{return _minamountpretransfer;}
		}
		/// <summary>
		/// 单笔最大转赠金额
		/// </summary>
		public decimal? MaxAmountPreTransfer
		{
			set{ _maxamountpretransfer=value;}
			get{return _maxamountpretransfer;}
		}
		/// <summary>
		/// 单笔最大转赠积分
		/// </summary>
		public int? MaxPointPreTransfer
		{
			set{ _maxpointpretransfer=value;}
			get{return _maxpointpretransfer;}
		}
		/// <summary>
		/// 每天最大转赠金额
		/// </summary>
		public decimal? DayMaxAmountTransfer
		{
			set{ _daymaxamounttransfer=value;}
			get{return _daymaxamounttransfer;}
		}
		/// <summary>
		/// 每天最大转赠积分
		/// </summary>
		public int? DayMaxPointTransfer
		{
			set{ _daymaxpointtransfer=value;}
			get{return _daymaxpointtransfer;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardGradeStatementFile
		{
			set{ _cardgradestatementfile=value;}
			get{return _cardgradestatementfile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardNumMask
		{
			set{ _cardnummask=value;}
			get{return _cardnummask;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardNumPattern
		{
			set{ _cardnumpattern=value;}
			get{return _cardnumpattern;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardCheckdigit
		{
			set{ _cardcheckdigit=value;}
			get{return _cardcheckdigit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CheckDigitModeID
		{
			set{ _checkdigitmodeid=value;}
			get{return _checkdigitmodeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PasswordRuleID
		{
			set{ _passwordruleid=value;}
			get{return _passwordruleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CampaignID
		{
			set{ _campaignid=value;}
			get{return _campaignid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? MinBalanceAmount
		{
			set{ _minbalanceamount=value;}
			get{return _minbalanceamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MinBalancePoint
		{
			set{ _minbalancepoint=value;}
			get{return _minbalancepoint;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? MinConsumeAmount
		{
			set{ _minconsumeamount=value;}
			get{return _minconsumeamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? GracePeriodValue
		{
			set{ _graceperiodvalue=value;}
			get{return _graceperiodvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? GracePeriodUnit
		{
			set{ _graceperiodunit=value;}
			get{return _graceperiodunit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ForfeitAmountAfterExpired
		{
			set{ _forfeitamountafterexpired=value;}
			get{return _forfeitamountafterexpired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ForfeitPointAfterExpired
		{
			set{ _forfeitpointafterexpired=value;}
			get{return _forfeitpointafterexpired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MinPointPreTransfer
		{
			set{ _minpointpretransfer=value;}
			get{return _minpointpretransfer;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsAllowConsumptionPoint
		{
			set{ _isallowconsumptionpoint=value;}
			get{return _isallowconsumptionpoint;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsConsecutiveUID
		{
			set{ _isconsecutiveuid=value;}
			get{return _isconsecutiveuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDToCardNumber
		{
			set{ _uidtocardnumber=value;}
			get{return _uidtocardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsImportUIDNumber
		{
			set{ _isimportuidnumber=value;}
			get{return _isimportuidnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDCheckDigit
		{
			set{ _uidcheckdigit=value;}
			get{return _uidcheckdigit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardNumberToUID
		{
			set{ _cardnumbertouid=value;}
			get{return _cardnumbertouid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MinPointPreAdd
		{
			set{ _minpointpreadd=value;}
			get{return _minpointpreadd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MaxPointPreAdd
		{
			set{ _maxpointpreadd=value;}
			get{return _maxpointpreadd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardGradeMaxPoint
		{
			set{ _cardgrademaxpoint=value;}
			get{return _cardgrademaxpoint;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardGradeUpdHitPLU
		{
			set{ _cardgradeupdhitplu=value;}
			get{return _cardgradeupdhitplu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HoldCouponCount
		{
			set{ _holdcouponcount=value;}
			get{return _holdcouponcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardGradeUpdCouponTypeID
		{
			set{ _cardgradeupdcoupontypeid=value;}
			get{return _cardgradeupdcoupontypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MobileProtectPeriodValue
		{
			set{ _mobileprotectperiodvalue=value;}
			get{return _mobileprotectperiodvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? EmailValidatedPeriodValue
		{
			set{ _emailvalidatedperiodvalue=value;}
			get{return _emailvalidatedperiodvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NonValidatedPeriodValue
		{
			set{ _nonvalidatedperiodvalue=value;}
			get{return _nonvalidatedperiodvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SessionTimeoutValue
		{
			set{ _sessiontimeoutvalue=value;}
			get{return _sessiontimeoutvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LoginFailureCount
		{
			set{ _loginfailurecount=value;}
			get{return _loginfailurecount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? QRCodePeriodValue
		{
			set{ _qrcodeperiodvalue=value;}
			get{return _qrcodeperiodvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AllowOfflineQRCode
		{
			set{ _allowofflineqrcode=value;}
			get{return _allowofflineqrcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string QRCodePrefix
		{
			set{ _qrcodeprefix=value;}
			get{return _qrcodeprefix;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NumberOfTransDisplay
		{
			set{ _numberoftransdisplay=value;}
			get{return _numberoftransdisplay;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NumberOfCouponDisplay
		{
			set{ _numberofcoupondisplay=value;}
			get{return _numberofcoupondisplay;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NumberOfNewsDisplay
		{
			set{ _numberofnewsdisplay=value;}
			get{return _numberofnewsdisplay;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TrainingMode
		{
			set{ _trainingmode=value;}
			get{return _trainingmode;}
		}
		#endregion Model

	}
}

