﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
    /// Gross Margin
	/// </summary>
	[Serializable]
	public partial class GrossMargin
	{
        public GrossMargin()
		{}
		#region Model
        private string _grossmargincode;
        private string _description1;
        private string _description2;
        private string _description3;
        private string _prodcode;
        private int _vendertype;
        private int _venderid;
        private int _buyertype;
        private int _buyerid;
        private int _cardtypeid;
        private int _cardgradeid;
        private decimal? _vat;
        private string _report;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;

        public string GrossMarginCode
		{
            set { _grossmargincode = value; }
            get { return _grossmargincode; }
		}
        public string Description1
		{
            set { _description1 = value; }
            get { return _description1; }
		}
        public string Description2
        {
            set { _description2 = value; }
            get { return _description2; }
        }
        public string Description3
        {
            set { _description3 = value; }
            get { return _description3; }
        }
        public string ProdCode
        {
            set { _prodcode = value; }
            get { return _prodcode; }
        }
        public int VenderType
        {
            set { _vendertype = value; }
            get { return _vendertype; }
        }
        public int VenderID
        {
            set { _venderid = value; }
            get { return _venderid; }
        }
        public int BuyerType
        {
            set { _buyertype = value; }
            get { return _buyertype; }
        }
        public int BuyerID
        {
            set { _buyerid = value; }
            get { return _buyerid; }
        }
        public int CardTypeID
        {
            set { _cardtypeid = value; }
            get { return _cardtypeid; }
        }
        public int CardGradeID
        {
            set { _cardgradeid = value; }
            get { return _cardgradeid; }
        }
        public decimal? VAT
        {
            set { _vat = value; }
            get { return _vat; }
        }
        public string Report
        {
            set { _report = value; }
            get { return _report; }
        }
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}

		#endregion Model

	}
}

