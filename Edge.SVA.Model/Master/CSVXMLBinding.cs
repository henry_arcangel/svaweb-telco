﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// CSV 和 XML 自定义字段绑定
	/// </summary>
	[Serializable]
	public partial class CSVXMLBinding
	{
        public CSVXMLBinding()
		{}
		#region Model
        private string _BindingCode;
        private string _Description;
        private string _Func;
        private string _CSVColumnName;
        private string _XMLFieldName;
        private int _DataType;
        private string _FixedValue;
        private int _Length;
        private int _SaveToDB;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
		/// <summary>
		/// 
		/// </summary>
        public string BindingCode
		{
            set { _BindingCode = value; }
            get { return _BindingCode; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string Description
		{
            set { _Description = value; }
            get { return _Description; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string Func
		{
            set { _Func = value; }
            get { return _Func; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string CSVColumnName
		{
            set { _CSVColumnName = value; }
            get { return _CSVColumnName; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string XMLFieldName
		{
            set { _XMLFieldName = value; }
            get { return _XMLFieldName; }
		}
		/// <summary>
		/// 
		/// </summary>
        public int DataType
		{
            set { _DataType = value; }
            get { return _DataType; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string FixedValue
		{
            set { _FixedValue = value; }
            get { return _FixedValue; }
		}
        /// <summary>
        /// 
        /// </summary>
        public int Length
        {
            set { _Length = value; }
            get { return _Length; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SaveToDB
        {
            set { _SaveToDB = value; }
            get { return _SaveToDB; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
		#endregion Model

	}
}

