﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 学校表
	/// </summary>
	[Serializable]
	public partial class WalletRule_D
	{
        public WalletRule_D()
		{}
		#region Model
        private int _KeyID;
        private string _WalletRuleCode;
        private int? _BrandID;
        private int? _CardTypeID;
        private int? _CardGradeID;
        private int? _StoreID;

		/// <summary>
		/// 主键ID
		/// </summary>
        public int KeyID
		{
            set { _KeyID = value; }
            get { return _KeyID; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string WalletRuleCode
		{
            set { _WalletRuleCode = value; }
            get { return _WalletRuleCode; }
		}
		/// <summary>
		/// 
		/// </summary>
        public int? BrandID
		{
            set { _BrandID = value; }
            get { return _BrandID; }
		}
		/// <summary>
		/// 
		/// </summary>
        public int? CardTypeID
		{
            set { _CardTypeID = value; }
            get { return _CardTypeID; }
		}
		/// <summary>
		///
		/// </summary>
        public int? CardGradeID
		{
            set { _CardGradeID = value; }
            get { return _CardGradeID; }
		}
		/// <summary>
		/// 
		/// </summary>
        public int? StoreID
		{
            set { _StoreID = value; }
            get { return _StoreID; }
		}

		#endregion Model

	}
}

