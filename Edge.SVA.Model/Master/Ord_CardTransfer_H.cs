﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 转赠单据表. 主表
	///@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D， 加上Ord_CardTransfer_H表。组成主从表。
	///注：转移的表结构上不加限制， 由填写者自行定义业务逻辑。
	/// </summary>
	[Serializable]
	public partial class Ord_CardTransfer_H
	{
		public Ord_CardTransfer_H()
		{}
		#region Model
		private string _cardtransfernumber;
		private int? _ordertype=0;
		private string _description;
		private int? _reasonid;
		private string _remark;
		private int? _copycardflag;
		private string _approvalcode;
		private string _approvestatus;
		private DateTime? _createdbusdate;
		private DateTime? _approvebusdate;
		private DateTime? _approveon;
		private int? _approveby;
		private int? _createdby;
		private DateTime? _createdon= DateTime.Now;
		private int? _updatedby;
		private DateTime? _updatedon= DateTime.Now;
        private int? _purchasetype;
		/// <summary>
		///11 主键单号
		/// </summary>
		public string CardTransferNumber
		{
			set{ _cardtransfernumber=value;}
			get{return _cardtransfernumber;}
		}
		/// <summary>
		///11 订单类型。 默认0.  0：普通。
		/// </summary>
		public int? OrderType
		{
			set{ _ordertype=value;}
			get{return _ordertype;}
		}
		/// <summary>
		///11 描述
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		///11 原因ID
		/// </summary>
		public int? ReasonID
		{
			set{ _reasonid=value;}
			get{return _reasonid;}
		}
		/// <summary>
		///11 备注
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		///11 是否复制旧卡到新卡。null/0：只是从一个卡中转移指定值到另外一个卡。 1：旧卡换新卡。（所有数据复制到新卡），旧卡注销。
		/// </summary>
		public int? CopyCardFlag
		{
			set{ _copycardflag=value;}
			get{return _copycardflag;}
		}
		/// <summary>
		///11 批核时产生授权号，并通知前台
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		///11 单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void
		/// </summary>
		public string ApproveStatus
		{
			set{ _approvestatus=value;}
			get{return _approvestatus;}
		}
		/// <summary>
		///11 单据创建时的busdate
		/// </summary>
		public DateTime? CreatedBusDate
		{
			set{ _createdbusdate=value;}
			get{return _createdbusdate;}
		}
		/// <summary>
		///11 单据批核时的busdate
		/// </summary>
		public DateTime? ApproveBusDate
		{
			set{ _approvebusdate=value;}
			get{return _approvebusdate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? ApproveOn
		{
			set{ _approveon=value;}
			get{return _approveon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? ApproveBy
		{
			set{ _approveby=value;}
			get{return _approveby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}

        public int? PurchaseType
        {
            set { _purchasetype = value; }
            get { return _purchasetype; }
        }
		#endregion Model

	}
}

