﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon订单（店铺给总部的）自动产生pickup单的规则设置表。
    ///
    /// </summary>
    [Serializable]
    public partial class CouponAutoPickingRule_H
    {
        public CouponAutoPickingRule_H()
        { }
        #region Model
        private string _couponautopickingrulecode;
        private string _description;
        private DateTime? _startdate;
        private DateTime? _enddate;
        private int? _status = 1;
        private int? _dayflagid;
        private int? _monthflagid;
        private int? _weekflagid;
        private DateTime? _createdon = DateTime.Now;
        private DateTime? _updatedon = DateTime.Now;
        private int? _createdby;
        private int? _updatedby;
        /// <summary>
        /// 优惠劵订单自动提货规则编码
        /// </summary>
        public string CouponAutoPickingRuleCode
        {
            set { _couponautopickingrulecode = value; }
            get { return _couponautopickingrulecode; }
        }
        /// <summary>
        /// 规则描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime? StartDate
        {
            set { _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? EndDate
        {
            set { _enddate = value; }
            get { return _enddate; }
        }
        /// <summary>
        /// 是否生效。 0：无效，1：生效。 默认生效
        /// </summary>
        public int? Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 生效天的设置ID
        /// </summary>
        public int? DayFlagID
        {
            set { _dayflagid = value; }
            get { return _dayflagid; }
        }
        /// <summary>
        /// 生效月的设置ID
        /// </summary>
        public int? MonthFlagID
        {
            set { _monthflagid = value; }
            get { return _monthflagid; }
        }
        /// <summary>
        /// 生效周的设置ID
        /// </summary>
        public int? WeekFlagID
        {
            set { _weekflagid = value; }
            get { return _weekflagid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        #endregion Model

    }
}

