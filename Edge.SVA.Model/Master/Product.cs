﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 货品表
	/// </summary>
	[Serializable]
	public partial class Product
	{
		public Product()
		{}
		#region Model
		private string _prodcode;
		private string _prodname1;
		private string _prodname2;
		private string _prodname3;
		private string _departcode;
		private int? _productbrandid;
		private int? _nonsale=1;
		private string _prodpicfile;
		private int? _prodtype;
		private string _prodnote;
		private int? _packqty;
		private int? _newflag=0;
		private int? _hotsaleflag=0;
		private int? _flag1=0;
		private int? _flag2=0;
		private int? _flag3=0;
		private int? _flag4=0;
		private int? _flag5=0;
		private int? _originid;
		private int? _colorid=0;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		private int? _productsizeid=0;
		/// <summary>
		///11 货品编码，主键
		/// </summary>
		public string ProdCode
		{
			set{ _prodcode=value;}
			get{return _prodcode;}
		}
		/// <summary>
		///11 货品名称1
		/// </summary>
		public string ProdName1
		{
			set{ _prodname1=value;}
			get{return _prodname1;}
		}
		/// <summary>
		///11 货品名称2
		/// </summary>
		public string ProdName2
		{
			set{ _prodname2=value;}
			get{return _prodname2;}
		}
		/// <summary>
		///11 货品名称3
		/// </summary>
		public string ProdName3
		{
			set{ _prodname3=value;}
			get{return _prodname3;}
		}
		/// <summary>
		///11 货品部门编码
		/// </summary>
		public string DepartCode
		{
			set{ _departcode=value;}
			get{return _departcode;}
		}
		/// <summary>
		///11 货品品牌ID
		/// </summary>
		public int? ProductBrandID
		{
			set{ _productbrandid=value;}
			get{return _productbrandid;}
		}
		/// <summary>
		///11 是否允许销售。0：不允许。1：允许
		/// </summary>
		public int? NonSale
		{
			set{ _nonsale=value;}
			get{return _nonsale;}
		}
		/// <summary>
		///11 货品图片文件
		/// </summary>
		public string ProdPicFile
		{
			set{ _prodpicfile=value;}
			get{return _prodpicfile;}
		}
		/// <summary>
		///11 货品类型
		/// </summary>
		public int? ProdType
		{
			set{ _prodtype=value;}
			get{return _prodtype;}
		}
		/// <summary>
		///11 货品备注
		/// </summary>
		public string ProdNote
		{
			set{ _prodnote=value;}
			get{return _prodnote;}
		}
		/// <summary>
		///11 销售包装单位数量 (eg. 100)
		/// </summary>
		public int? PackQty
		{
			set{ _packqty=value;}
			get{return _packqty;}
		}
		/// <summary>
		///11 新品标志。 0：非新品。1：新品
		/// </summary>
		public int? NewFlag
		{
			set{ _newflag=value;}
			get{return _newflag;}
		}
		/// <summary>
		///11 热销标志。0：非热销。1：热销
		/// </summary>
		public int? HotSaleFlag
		{
			set{ _hotsaleflag=value;}
			get{return _hotsaleflag;}
		}
		/// <summary>
		///11 货品扩展标志1
		/// </summary>
		public int? Flag1
		{
			set{ _flag1=value;}
			get{return _flag1;}
		}
		/// <summary>
		///11 货品扩展标志2
		/// </summary>
		public int? Flag2
		{
			set{ _flag2=value;}
			get{return _flag2;}
		}
		/// <summary>
		///11 货品扩展标志3
		/// </summary>
		public int? Flag3
		{
			set{ _flag3=value;}
			get{return _flag3;}
		}
		/// <summary>
		///11 货品扩展标志4
		/// </summary>
		public int? Flag4
		{
			set{ _flag4=value;}
			get{return _flag4;}
		}
		/// <summary>
		///11 货品扩展标志5
		/// </summary>
		public int? Flag5
		{
			set{ _flag5=value;}
			get{return _flag5;}
		}
		/// <summary>
		///11 货品原产地ID。（目前内容定义为NationID）
		/// </summary>
		public int? OriginID
		{
			set{ _originid=value;}
			get{return _originid;}
		}
		/// <summary>
		///11 颜色ID。默认0:没有颜色可选
		/// </summary>
		public int? ColorID
		{
			set{ _colorid=value;}
			get{return _colorid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? ProductSizeID
		{
			set{ _productsizeid=value;}
			get{return _productsizeid;}
		}
		#endregion Model

	}
}

