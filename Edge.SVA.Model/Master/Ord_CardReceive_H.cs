﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 收货确认单。主表 （后台对供应商收货确认）（copy来源Ord_CouponReceive_H @2014-09-17）
    ///
    /// </summary>
    [Serializable]
    public partial class Ord_CardReceive_H
    {
        public Ord_CardReceive_H()
        { }
        #region Model
        private string _cardreceivenumber;
        private string _referenceno;
        private int? _storeid;
        private int? _supplierid;
        private string _storeraddress;
        private string _supplieraddress;
        private string _suppliertcontactname;
        private string _supplierphone;
        private string _supplieremail;
        private string _suppliermobile;
        private string _storecontactname;
        private string _storephone;
        private string _storeemail;
        private string _storemobile;
        private string _remark;
        private int? _ordertype = 0;
        private int? _receivetype;
        private string _remark1;
        private string _remark2;
        private int? _companyid;
        private int? _purchasetype=1;
        private DateTime? _createdbusdate;
        private DateTime? _approvebusdate;
        private string _approvalcode;
        private string _approvestatus;
        private DateTime? _approveon;
        private int? _approveby;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
        /// <summary>
        /// 收货确认单单号，主键
        /// </summary>
        public string CardReceiveNumber
        {
            set { _cardreceivenumber = value; }
            get { return _cardreceivenumber; }
        }
        /// <summary>
        /// 参考编号。指coupon订单号
        /// </summary>
        public string ReferenceNo
        {
            set { _referenceno = value; }
            get { return _referenceno; }
        }
        /// <summary>
        /// 收到货的店铺（后台）主键
        /// </summary>
        public int? StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        /// <summary>
        /// 发出或的供应商ID， 外键
        /// </summary>
        public int? SupplierID
        {
            set { _supplierid = value; }
            get { return _supplierid; }
        }
        /// <summary>
        /// 送货地址。（总部地址）
        /// </summary>
        public string StorerAddress
        {
            set { _storeraddress = value; }
            get { return _storeraddress; }
        }
        /// <summary>
        /// 供应商地址
        /// </summary>
        public string SupplierAddress
        {
            set { _supplieraddress = value; }
            get { return _supplieraddress; }
        }
        /// <summary>
        /// 供应商联系人
        /// </summary>
        public string SuppliertContactName
        {
            set { _suppliertcontactname = value; }
            get { return _suppliertcontactname; }
        }
        /// <summary>
        /// 供应商联系电话
        /// </summary>
        public string SupplierPhone
        {
            set { _supplierphone = value; }
            get { return _supplierphone; }
        }
        /// <summary>
        /// 供应商联系邮箱
        /// </summary>
        public string SupplierEmail
        {
            set { _supplieremail = value; }
            get { return _supplieremail; }
        }
        /// <summary>
        /// 供应商联系手机
        /// </summary>
        public string SupplierMobile
        {
            set { _suppliermobile = value; }
            get { return _suppliermobile; }
        }
        /// <summary>
        /// 总部联系人
        /// </summary>
        public string StoreContactName
        {
            set { _storecontactname = value; }
            get { return _storecontactname; }
        }
        /// <summary>
        /// 总部联系电话
        /// </summary>
        public string StorePhone
        {
            set { _storephone = value; }
            get { return _storephone; }
        }
        /// <summary>
        /// 总部联系邮箱
        /// </summary>
        public string StoreEmail
        {
            set { _storeemail = value; }
            get { return _storeemail; }
        }
        /// <summary>
        /// 总部联系手机
        /// </summary>
        public string StoreMobile
        {
            set { _storemobile = value; }
            get { return _storemobile; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 订单类型。 0：手动。1：自动。 默认：1
        /// </summary>
        public int? OrderType
        {
            set { _ordertype = value; }
            get { return _ordertype; }
        }
        /// <summary>
        /// 收货来源。 1：正常的订单收货。2：退货单的收货（只有店铺的退货单）
        /// </summary>
        public int? ReceiveType
        {
            set { _receivetype = value; }
            get { return _receivetype; }
        }
        /// <summary>
        /// 备注1
        /// </summary>
        public string Remark1
        {
            set { _remark1 = value; }
            get { return _remark1; }
        }
        /// <summary>
        /// 备注2
        /// </summary>
        public string Remark2
        {
            set { _remark2 = value; }
            get { return _remark2; }
        }
        /// <summary>
        /// 公司ID
        /// </summary>
        public int? CompanyID
        {
            set { _companyid = value; }
            get { return _companyid; }
        }
        /// <summary>
        /// 单据创建时的busdate
        /// </summary>
        public DateTime? CreatedBusDate
        {
            set { _createdbusdate = value; }
            get { return _createdbusdate; }
        }
        /// <summary>
        /// 单据批核时的busdate
        /// </summary>
        public DateTime? ApproveBusDate
        {
            set { _approvebusdate = value; }
            get { return _approvebusdate; }
        }
        /// <summary>
        /// 批核时产生授权号，并通知前台
        /// </summary>
        public string ApprovalCode
        {
            set { _approvalcode = value; }
            get { return _approvalcode; }
        }
        /// <summary>
        /// 单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void
        /// </summary>
        public string ApproveStatus
        {
            set { _approvestatus = value; }
            get { return _approvestatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? ApproveOn
        {
            set { _approveon = value; }
            get { return _approveon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ApproveBy
        {
            set { _approveby = value; }
            get { return _approveby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// <summary>
        /// 订货类型: 1：实体卡订货。 2： 指定卡购买金额或者积分
        /// </summary>
        public int? PurchaseType
        {
            set { _purchasetype = value; }
            get { return _purchasetype; }
        }

        #endregion Model

    }
}

