﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 手工交易单。 子表。
	/// </summary>
	[Serializable]
	public partial class Ord_TradeManually_D
	{
		public Ord_TradeManually_D()
		{}
		#region Model
		private int _keyid;
		private string _trademanuallycode;
		private int? _storeid;
		private decimal? _traderamount;
		private int? _earnpoint;
		private string _referenceno;
		/// <summary>
		/// 主键，自增长
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 单据号码
		/// </summary>
		public string TradeManuallyCode
		{
			set{ _trademanuallycode=value;}
			get{return _trademanuallycode;}
		}
		/// <summary>
		/// 店铺ID
		/// </summary>
		public int? StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		/// 交易金额
		/// </summary>
		public decimal? TraderAmount
		{
			set{ _traderamount=value;}
			get{return _traderamount;}
		}
		/// <summary>
		/// 获得的积分。（根据积分规则计算获得）
		/// </summary>
		public int? EarnPoint
		{
			set{ _earnpoint=value;}
			get{return _earnpoint;}
		}
		/// <summary>
		/// 参考编号。
		/// </summary>
		public string ReferenceNo
		{
			set{ _referenceno=value;}
			get{return _referenceno;}
		}
		#endregion Model

	}
}

