﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// CSV 和 XML 自定义字段绑定
	/// </summary>
	[Serializable]
	public partial class CSVXMLMointoringRule
	{
		public CSVXMLMointoringRule()
		{}
		#region Model
        private string _RuleName;
        private string _Description;
        private DateTime? _StartDate;
        private DateTime? _EndDate;
        private int _Status;
        private string _Func;
        private string _DownloadPath;
        private int _DayFlagID;
        private int _MonthFlagID;
        private int _WeekFlagID;
        private DateTime? _ActiveTime;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;

        public string RuleName
		{
            set { _RuleName = value; }
            get { return _RuleName; }
		}
        public string Description
        {
            set { _Description = value; }
            get { return _Description; }
        }
        public DateTime? StartDate
        {
            set { _StartDate = value; }
            get { return _StartDate; }
        }
        public DateTime? EndDate
        {
            set { _EndDate = value; }
            get { return _EndDate; }
        }
        public int Status
        {
            set { _Status = value; }
            get { return _Status; }
        }
        public string Func
        {
            set { _Func = value; }
            get { return _Func; }
        }
        public string DownloadPath
        {
            set { _DownloadPath = value; }
            get { return _DownloadPath; }
        }
        public int DayFlagID
        {
            set { _DayFlagID = value; }
            get { return _DayFlagID; }
        }
        public int MonthFlagID
        {
            set { _MonthFlagID = value; }
            get { return _MonthFlagID; }
        }
        public int WeekFlagID
        {
            set { _WeekFlagID = value; }
            get { return _WeekFlagID; }
        }
        public DateTime? ActiveTime
		{
            set { _ActiveTime = value; }
            get { return _ActiveTime; }
		}

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
		#endregion Model

	}
}

