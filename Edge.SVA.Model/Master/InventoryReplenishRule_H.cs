﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon自动补货规则设置表. 
    ///根据设置，自动创建order单。 （可能直接批核，可能不是。）
    ///需要设定storetypeid， 根据storetypeid， 子表中的Store 必须根据此storetype 来过滤。
    ///注：自动补货规则包含两种，根据发起的店铺类型不同，产生不同的order。   店铺的只能给后台， 后台的只能给供应商。 两种不同的订单。
    ///@2014-09-17: 增加Card的补库规则设置。因此更改表名CouponReplenishRule_H 为 InventoryReplenishRule_H
    /// </summary>
    [Serializable]
    public partial class InventoryReplenishRule_H
    {
        public InventoryReplenishRule_H()
        { }
        #region Model
        private string _inventoryreplenishcode;
        private string _description;
        private DateTime? _startdate;
        private DateTime? _enddate;
        private int? _status = 1;
        private int _mediatype = 1;
        private int? _brandid;
        private int _coupontypeid;
        private int _cardtypeid;
        private int _cardgradeid;
        private int _storetypeid;
        private int? _autocreateorder = 1;
        private int? _autoapproveorder = 0;
        private DateTime? _createdon = DateTime.Now;
        private DateTime? _updatedon = DateTime.Now;
        private int? _createdby;
        private int? _updatedby;
        private int? _dayflagid;
        private int? _monthflagid;
        private int? _weekflagid;
        private DateTime? _activetime;
        private int? _purchasetype = 1;
        /// <summary>
        /// 优惠劵自动补货规则编码
        /// </summary>
        public string InventoryReplenishCode
        {
            set { _inventoryreplenishcode = value; }
            get { return _inventoryreplenishcode; }
        }
        /// <summary>
        /// 规则描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime? StartDate
        {
            set { _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? EndDate
        {
            set { _enddate = value; }
            get { return _enddate; }
        }
        /// <summary>
        /// 是否生效。 0：无效，1：生效。 默认生效
        /// </summary>
        public int? Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 补货规则针对对象的类型： 1： Coupon。 2：Card
        /// </summary>
        public int MediaType
        {
            set { _mediatype = value; }
            get { return _mediatype; }
        }
        /// <summary>
        /// 品牌ID （coupon的或者是Card的）
        /// </summary>
        public int? BrandID
        {
            set { _brandid = value; }
            get { return _brandid; }
        }
        /// <summary>
        /// 根据brandid选择 CouponType ID （MediaType=1有效）
        /// </summary>
        public int CouponTypeID
        {
            set { _coupontypeid = value; }
            get { return _coupontypeid; }
        }
        /// <summary>
        /// 根据brandid选择 CardType ID （MediaType=2有效）
        /// </summary>
        public int CardTypeID
        {
            set { _cardtypeid = value; }
            get { return _cardtypeid; }
        }
        /// <summary>
        /// 根据brandid选择 CardGradeID （MediaType=2有效）
        /// </summary>
        public int CardGradeID
        {
            set { _cardgradeid = value; }
            get { return _cardgradeid; }
        }
        /// <summary>
        /// 店铺类型ID。1：总部。 2：店铺

        /// </summary>
        public int StoreTypeID
        {
            set { _storetypeid = value; }
            get { return _storetypeid; }
        }
        /// <summary>
        /// 是否自动创建订单
        /// </summary>
        public int? AutoCreateOrder
        {
            set { _autocreateorder = value; }
            get { return _autocreateorder; }
        }
        /// <summary>
        /// 自动创建的订单，是否直接批核
        /// </summary>
        public int? AutoApproveOrder
        {
            set { _autoapproveorder = value; }
            get { return _autoapproveorder; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// <summary>
        /// 生效天的设置ID
        /// </summary>
        public int? DayFlagID
        {
            set { _dayflagid = value; }
            get { return _dayflagid; }
        }
        /// <summary>
        /// 生效月的设置ID
        /// </summary>
        public int? MonthFlagID
        {
            set { _monthflagid = value; }
            get { return _monthflagid; }
        }
        /// <summary>
        /// 生效周的设置ID
        /// </summary>
        public int? WeekFlagID
        {
            set { _weekflagid = value; }
            get { return _weekflagid; }
        }
        /// <summary>
        /// 生效时间
        /// </summary>
        public DateTime? ActiveTime
        {
            set { _activetime = value; }
            get { return _activetime; }
        }

        /// <summary>
        /// 订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分
        /// </summary>
        public int? PurchaseType
        {
            set { _purchasetype = value; }
            get { return _purchasetype; }
        }
        
        #endregion Model

    }
}

