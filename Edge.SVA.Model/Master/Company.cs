﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
    /// 公司表
	/// </summary>
	[Serializable]
	public partial class Company
	{
		public Company()
		{}
		#region Model
        private int _companyid;
        private string _companycode;
        private string _companyname;
        private string _companyaddress;
        private string _companytelnum;
		private string _companyfaxnum;
        private int? _isdefault;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID 
		/// </summary>
		public int CompanyID
		{
			set{ _companyid=value;}
			get{return _companyid;}
		}
		/// <summary>
		/// 公司编码
		/// </summary>
		public string CompanyCode
		{
			set{ _companycode=value;}
			get{return _companycode;}
		}
		/// <summary>
		/// 公司名称
		/// </summary>
		public string CompanyName
		{
			set{ _companyname=value;}
			get{return _companyname;}
		}
		/// <summary>
		/// 地址ID
		/// </summary>
		public string CompanyAddress
		{
			set{ _companyaddress=value;}
			get{return _companyaddress;}
		}
		/// <summary>
		/// 公司电话
		/// </summary>
		public string CompanyTelNum
		{
			set{ _companytelnum=value;}
			get{return _companytelnum;}
		}
		/// <summary>
		/// 公司传真
		/// </summary>
		public string CompanyFaxNum
		{
			set{ _companyfaxnum=value;}
			get{return _companyfaxnum;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
        /// <summary>
        /// 
        /// </summary>
        public int? isDefault
		{
			set{ _isdefault=value;}
			get{return _isdefault;}
		}
		#endregion Model

	}
}

