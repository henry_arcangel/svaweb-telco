﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 促销消息图片表
	/// </summary>
	[Serializable]
	public partial class PromotionMsg_Pic
	{
		public PromotionMsg_Pic()
		{}
		#region Model
		private int _keyid;
		private string _promotionmsgcode;
		private string _extrapic;
		private string _picremark;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 优惠消息Code
		/// </summary>
		public string PromotionMsgCode
		{
			set{ _promotionmsgcode=value;}
			get{return _promotionmsgcode;}
		}
		/// <summary>
		/// 图片路径
		/// </summary>
		public string ExtraPic
		{
			set{ _extrapic=value;}
			get{return _extrapic;}
		}
		/// <summary>
		/// 图片备注
		/// </summary>
		public string PicRemark
		{
			set{ _picremark=value;}
			get{return _picremark;}
		}
		#endregion Model

	}
}

