﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 店铺类型表。固定数据：
	///1：member card。 2：staff card。 3：store value card。 4：cash card（含有初始金额，不可充值）
	/// </summary>
	[Serializable]
	public partial class CardTypeNature
	{
		public CardTypeNature()
		{}
		#region Model
		private int _cardtypenatureid;
		private string _cardtypenaturename1;
		private string _cardtypenaturename2;
		private string _cardtypenaturename3;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CardTypeNatureID
		{
			set{ _cardtypenatureid=value;}
			get{return _cardtypenatureid;}
		}
		/// <summary>
		/// 卡类型种类名称1
		/// </summary>
		public string CardTypeNatureName1
		{
			set{ _cardtypenaturename1=value;}
			get{return _cardtypenaturename1;}
		}
		/// <summary>
		/// 卡类型种类名称2
		/// </summary>
		public string CardTypeNatureName2
		{
			set{ _cardtypenaturename2=value;}
			get{return _cardtypenaturename2;}
		}
		/// <summary>
		/// 卡类型种类名称3
		/// </summary>
		public string CardTypeNatureName3
		{
			set{ _cardtypenaturename3=value;}
			get{return _cardtypenaturename3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

