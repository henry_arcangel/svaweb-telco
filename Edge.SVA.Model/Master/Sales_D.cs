﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// ?úê?μ￥×ó±í￡¨???·??μ￥±í￡?
	/// </summary>
	[Serializable]
	public partial class Sales_D
	{
		public Sales_D()
		{}
		#region Model
		private int _keyid;
		private string _txnno;
		private int _seqno;
		private string _prodcode;
		private string _proddesc;
		private string _serialno;
		private int? _collected;
		private decimal? _retailprice;
		private decimal? _netprice;
		private int? _qty;
		private decimal? _netamount;
		private string _additional;
		private decimal? _poprice=0M;
		private int? _poreasonid;
		private int? _discounttype=1;
		private decimal? _discountoffvalue=0M;
		private decimal? _discountoffprice=0M;
		private int? _discountreasonid;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		private DateTime? _reserveddate;
		private DateTime? _pickupdate;
		/// <summary>
		///11 ×???3¤?÷?ü
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		///11 ??ò×μ￥o?￡¨?÷?ü￡?
		/// </summary>
		public string TxnNo
		{
			set{ _txnno=value;}
			get{return _txnno;}
		}
		/// <summary>
		///11 ??ò×μ￥???·Dòo?
		/// </summary>
		public int SeqNo
		{
			set{ _seqno=value;}
			get{return _seqno;}
		}
		/// <summary>
		///11 ???·±à??
		/// </summary>
		public string ProdCode
		{
			set{ _prodcode=value;}
			get{return _prodcode;}
		}
		/// <summary>
		///11 ???·?èê?
		/// </summary>
		public string ProdDesc
		{
			set{ _proddesc=value;}
			get{return _proddesc;}
		}
		/// <summary>
		///11 ???·DòáDo?
		/// </summary>
		public string Serialno
		{
			set{ _serialno=value;}
			get{return _serialno;}
		}
		/// <summary>
		///11 ???·ìá??×′ì?￡¨?a′?￡?
		/// </summary>
		public int? Collected
		{
			set{ _collected=value;}
			get{return _collected;}
		}
		/// <summary>
		///11 ???·á?ê?????
		/// </summary>
		public decimal? RetailPrice
		{
			set{ _retailprice=value;}
			get{return _retailprice;}
		}
		/// <summary>
		///11 ???·êμ?ê?úê?????
		/// </summary>
		public decimal? NetPrice
		{
			set{ _netprice=value;}
			get{return _netprice;}
		}
		/// <summary>
		///11 ???·êyá?
		/// </summary>
		public int? Qty
		{
			set{ _qty=value;}
			get{return _qty;}
		}
		/// <summary>
		///11 ???·?e??￡¨NetPrice*Qty￡?
		/// </summary>
		public decimal? NetAmount
		{
			set{ _netamount=value;}
			get{return _netamount;}
		}
		/// <summary>
		///11 ???óD??￠
		/// </summary>
		public string Additional
		{
			set{ _additional=value;}
			get{return _additional;}
		}
		/// <summary>
		///11 ????????￡¨RetailPrice - POPrice = NetPrice￡?
		/// </summary>
		public decimal? POPrice
		{
			set{ _poprice=value;}
			get{return _poprice;}
		}
		/// <summary>
		///11 ?????-òòID￡¨Reason±í￡?
		/// </summary>
		public int? POReasonID
		{
			set{ _poreasonid=value;}
			get{return _poreasonid;}
		}
		/// <summary>
		///11 ????ààDí?￡1￡o?e???????￡2￡o±èày????
		/// </summary>
		public int? DiscountType
		{
			set{ _discounttype=value;}
			get{return _discounttype;}
		}
		/// <summary>
		///11 ????OFF ?μ
		/// </summary>
		public decimal? DiscountOffValue
		{
			set{ _discountoffvalue=value;}
			get{return _discountoffvalue;}
		}
		/// <summary>
		///11 ????OFF μ?????￡¨RetailPrice - DiscountOffPrice = NetPrice￡?
		/// </summary>
		public decimal? DiscountOffPrice
		{
			set{ _discountoffprice=value;}
			get{return _discountoffprice;}
		}
		/// <summary>
		///11 ?????-òòID￡¨Reason±í￡?
		/// </summary>
		public int? DiscountReasonID
		{
			set{ _discountreasonid=value;}
			get{return _discountreasonid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? ReservedDate
		{
			set{ _reserveddate=value;}
			get{return _reserveddate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? PickupDate
		{
			set{ _pickupdate=value;}
			get{return _pickupdate;}
		}
		#endregion Model

	}
}

