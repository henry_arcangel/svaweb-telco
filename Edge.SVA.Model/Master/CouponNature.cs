﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵类型级别
	/// </summary>
	[Serializable]
	public partial class CouponNature
	{
		public CouponNature()
		{}
		#region Model
		private int _couponnatureid;
		private string _couponnaturecode;
		private string _couponnaturename1;
		private string _couponnaturename2;
		private string _couponnaturename3;
		private int _brandid;
        private int _parentid;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int CouponNatureID
		{
			set{ _couponnatureid=value;}
			get{return _couponnatureid;}
		}
		/// <summary>
		/// 编码
		/// </summary>
		public string CouponNatureCode
		{
			set{ _couponnaturecode=value;}
			get{return _couponnaturecode;}
		}
		/// <summary>
		/// 优惠劵类型级别名称1
		/// </summary>
		public string CouponNatureName1
		{
			set{ _couponnaturename1=value;}
			get{return _couponnaturename1;}
		}
		/// <summary>
		/// 优惠劵类型级别名称2
		/// </summary>
		public string CouponNatureName2
		{
			set{ _couponnaturename2=value;}
			get{return _couponnaturename2;}
		}
		/// <summary>
		/// 优惠劵类型级别名称3
		/// </summary>
		public string CouponNatureName3
		{
			set{ _couponnaturename3=value;}
			get{return _couponnaturename3;}
		}
		/// <summary>
		/// 品牌ID
		/// </summary>
		public int BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
        /// <summary>
        /// Parent优惠券类型级别ID
        /// </summary>
        public int ParentID
        {
            set { _parentid = value; }
            get { return _parentid; }
        }
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

