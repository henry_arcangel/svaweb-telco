﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 卡送货单主表。
    ///（从批核后的拣货单复制过来，过滤了拣货数量为0的记录。 头表状态略有不同）
    /// </summary>
    [Serializable]
    public partial class Ord_CardDelivery_H
    {
        public Ord_CardDelivery_H()
        { }
        #region Model
        private string _carddeliverynumber;
        private string _referenceno;
        private int? _brandid;
        private int? _fromstoreid;
        private int? _storeid;
        private int? _customertype;
        private int? _customerid;
        private int? _sendmethod;
        private string _sendaddress;
        private string _fromaddress;
        private string _storecontactname;
        private string _storecontactphone;
        private string _storecontactemail;
        private string _storemobile;
        private string _fromcontactname;
        private string _fromcontactnumber;
        private string _fromemail;
        private string _frommobile;
        private int? _needactive = 0;
        private string _remark;
        private string _remark1;
        private DateTime? _createdbusdate;
        private DateTime? _approvebusdate;
        private string _approvalcode;
        private string _approvestatus;
        private DateTime? _approveon;
        private int? _approveby;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
        /// <summary>
        /// 送货单单号，主键
        /// </summary>
        public string CardDeliveryNumber
        {
            set { _carddeliverynumber = value; }
            get { return _carddeliverynumber; }
        }
        /// <summary>
        /// 参考编号。指coupon拣货单号
        /// </summary>
        public string ReferenceNo
        {
            set { _referenceno = value; }
            get { return _referenceno; }
        }
        /// <summary>
        /// 品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType
        /// </summary>
        public int? BrandID
        {
            set { _brandid = value; }
            get { return _brandid; }
        }
        /// <summary>
        /// 货源店铺，一般可能是总部
        /// </summary>
        public int? FromStoreID
        {
            set { _fromstoreid = value; }
            get { return _fromstoreid; }
        }
        /// <summary>
        /// 送达店铺主键
        /// </summary>
        public int? StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        /// <summary>
        /// 客户类型。1：客戶訂貨。 2：店鋪訂貨
        /// </summary>
        public int? CustomerType
        {
            set { _customertype = value; }
            get { return _customertype; }
        }
        /// <summary>
        /// customer表 主键， 外键
        /// </summary>
        public int? CustomerID
        {
            set { _customerid = value; }
            get { return _customerid; }
        }
        /// <summary>
        /// 送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network
        /// </summary>
        public int? SendMethod
        {
            set { _sendmethod = value; }
            get { return _sendmethod; }
        }
        /// <summary>
        /// 送货地址
        /// </summary>
        public string SendAddress
        {
            set { _sendaddress = value; }
            get { return _sendaddress; }
        }
        /// <summary>
        /// 发货地址（总部地址）
        /// </summary>
        public string FromAddress
        {
            set { _fromaddress = value; }
            get { return _fromaddress; }
        }
        /// <summary>
        /// 店铺联系人
        /// </summary>
        public string StoreContactName
        {
            set { _storecontactname = value; }
            get { return _storecontactname; }
        }
        /// <summary>
        /// 店铺联系电话
        /// </summary>
        public string StoreContactPhone
        {
            set { _storecontactphone = value; }
            get { return _storecontactphone; }
        }
        /// <summary>
        /// 店铺邮箱
        /// </summary>
        public string StoreContactEmail
        {
            set { _storecontactemail = value; }
            get { return _storecontactemail; }
        }
        /// <summary>
        /// 店铺手机
        /// </summary>
        public string StoreMobile
        {
            set { _storemobile = value; }
            get { return _storemobile; }
        }
        /// <summary>
        /// 发货总部联系人
        /// </summary>
        public string FromContactName
        {
            set { _fromcontactname = value; }
            get { return _fromcontactname; }
        }
        /// <summary>
        /// 发货总部联系电话
        /// </summary>
        public string FromContactNumber
        {
            set { _fromcontactnumber = value; }
            get { return _fromcontactnumber; }
        }
        /// <summary>
        /// 发货总部邮箱
        /// </summary>
        public string FromEmail
        {
            set { _fromemail = value; }
            get { return _fromemail; }
        }
        /// <summary>
        /// 发货总部联系手机
        /// </summary>
        public string FromMobile
        {
            set { _frommobile = value; }
            get { return _frommobile; }
        }
        /// <summary>
        /// 客户收货确认后是否激活。0：不激活。1：激活。默认0
        /// </summary>
        public int? NeedActive
        {
            set { _needactive = value; }
            get { return _needactive; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark1
        {
            set { _remark1 = value; }
            get { return _remark1; }
        }
        /// <summary>
        /// 单据创建时的busdate
        /// </summary>
        public DateTime? CreatedBusDate
        {
            set { _createdbusdate = value; }
            get { return _createdbusdate; }
        }
        /// <summary>
        /// 单据批核时的busdate
        /// </summary>
        public DateTime? ApproveBusDate
        {
            set { _approvebusdate = value; }
            get { return _approvebusdate; }
        }
        /// <summary>
        /// 批核时产生授权号，并通知前台
        /// </summary>
        public string ApprovalCode
        {
            set { _approvalcode = value; }
            get { return _approvalcode; }
        }
        /// <summary>
        /// 单据状态。状态： P：prepare（送货中）。  A:Approve（送货完成，顾客签收） 。 V：Void（顾客拒收，card回到dormant状态）
        /// </summary>
        public string ApproveStatus
        {
            set { _approvestatus = value; }
            get { return _approvestatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? ApproveOn
        {
            set { _approveon = value; }
            get { return _approveon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ApproveBy
        {
            set { _approveby = value; }
            get { return _approveby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        #endregion Model

    }
}

