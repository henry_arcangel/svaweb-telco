﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 赞助商
	/// </summary>
	[Serializable]
	public partial class Sponsor
	{
		public Sponsor()
		{}
		#region Model
		private int _sponsorid;
		private string _sponsorcode;
		private string _sponsorname1;
		private string _sponsorname2;
		private string _sponsorname3;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int SponsorID
		{
			set{ _sponsorid=value;}
			get{return _sponsorid;}
		}
		/// <summary>
		/// 赞助商编码
		/// </summary>
		public string SponsorCode
		{
			set{ _sponsorcode=value;}
			get{return _sponsorcode;}
		}
		/// <summary>
		/// 赞助商名称1
		/// </summary>
		public string SponsorName1
		{
			set{ _sponsorname1=value;}
			get{return _sponsorname1;}
		}
		/// <summary>
		/// 赞助商名称2
		/// </summary>
		public string SponsorName2
		{
			set{ _sponsorname2=value;}
			get{return _sponsorname2;}
		}
		/// <summary>
		/// 赞助商名称3
		/// </summary>
		public string SponsorName3
		{
			set{ _sponsorname3=value;}
			get{return _sponsorname3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

