﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Address:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Address
	{
		public Address()
		{}
		#region Model
		private string _addressid;
		private string _addressunit;
		private int? _addressfloor;
		private string _addressblock;
		private string _addressbuilding;
		private string _addressstreetno;
		private string _addressstreet;
		private string _addressdistrict;
		private string _addresscity;
		private string _addressprovince;
		private string _addresscountry;
		private string _addresszipcode;
		private int _addresslanguage=1;
		private DateTime? _createdon;
		private DateTime? _updatedon;
		private string _updatedby;
		private string _addr1;
		private string _addr2;
		private string _addr3;
		private string _addr4;
		/// <summary>
		/// 
		/// </summary>
		public string AddressID
		{
			set{ _addressid=value;}
			get{return _addressid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressUnit
		{
			set{ _addressunit=value;}
			get{return _addressunit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AddressFloor
		{
			set{ _addressfloor=value;}
			get{return _addressfloor;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressBlock
		{
			set{ _addressblock=value;}
			get{return _addressblock;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressBuilding
		{
			set{ _addressbuilding=value;}
			get{return _addressbuilding;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressStreetNo
		{
			set{ _addressstreetno=value;}
			get{return _addressstreetno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressStreet
		{
			set{ _addressstreet=value;}
			get{return _addressstreet;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressDistrict
		{
			set{ _addressdistrict=value;}
			get{return _addressdistrict;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressCity
		{
			set{ _addresscity=value;}
			get{return _addresscity;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressProvince
		{
			set{ _addressprovince=value;}
			get{return _addressprovince;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressCountry
		{
			set{ _addresscountry=value;}
			get{return _addresscountry;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddressZipCode
		{
			set{ _addresszipcode=value;}
			get{return _addresszipcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AddressLanguage
		{
			set{ _addresslanguage=value;}
			get{return _addresslanguage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Addr1
		{
			set{ _addr1=value;}
			get{return _addr1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Addr2
		{
			set{ _addr2=value;}
			get{return _addr2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Addr3
		{
			set{ _addr3=value;}
			get{return _addr3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Addr4
		{
			set{ _addr4=value;}
			get{return _addr4;}
		}
		#endregion Model

	}
}

