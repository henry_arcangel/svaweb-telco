﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon，Card自动补货设置子表。 
    /// </summary>
    [Serializable]
    public partial class InventoryReplenishRule_D
    {
        public InventoryReplenishRule_D()
        { }
        #region Model
        private int _keyid;
        private string _inventoryreplenishcode;
        private int _storeid;
        private int? _ordertargetid;
        private int? _minstockqty=0;
        private int? _runningstockqty=0;
        private int? _orderroundupqty=0;
        private int? _priority = 0;
        private int? _replenishtype = 1;
        private decimal? _minamtbalance=0;
        private decimal? _runningamtbalance=0;
        private int? _minpointbalance=0;
        private int? _runningpointbalance=0;
        /// <summary>
        /// 自增长ID
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 主表主键
        /// </summary>
        public string InventoryReplenishCode
        {
            set { _inventoryreplenishcode = value; }
            get { return _inventoryreplenishcode; }
        }
        /// <summary>
        /// 根据StoreTypeID选择店铺ID
        /// </summary>
        public int StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        /// <summary>
        /// 订货目标ID。（如果是店铺订货，则填写总部的ID。 如果总部订货，则填写供应商ID）
        /// </summary>
        public int? OrderTargetID
        {
            set { _ordertargetid = value; }
            get { return _ordertargetid; }
        }
        /// <summary>
        /// 最小允许库存。（低于此将可能触发自动补货）
        /// </summary>
        public int? MinStockQty
        {
            set { _minstockqty = value; }
            get { return _minstockqty; }
        }
        /// <summary>
        /// 正常库存数量。（补货数量为： 正常库存数量 - 当前库存数量）注：如果有补货的倍数要求，按照倍数要求计算。比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100
        /// </summary>
        public int? RunningStockQty
        {
            set { _runningstockqty = value; }
            get { return _runningstockqty; }
        }
        /// <summary>
        /// 订货数量的倍数（包装数量）：Sample:max=300 current=97max-current=203203 will be rounded up to 250 since it is the next number divisible by 50.

        /// </summary>
        public int? OrderRoundUpQty
        {
            set { _orderroundupqty = value; }
            get { return _orderroundupqty; }
        }
        /// <summary>
        /// 优先级。 （库存不足情况下。按 从小到大 优先分配。）
        /// </summary>
        public int? Priority
        {
            set { _priority = value; }
            get { return _priority; }
        }

        public int? ReplenishType
        {
            set { _replenishtype = value; }
            get { return _replenishtype; }
        }
        public decimal? MinAmtBalance
        {
            set { _minamtbalance = value; }
            get { return _minamtbalance; }
        }
        public decimal? RunningAmtBalance
        {
            set { _runningamtbalance = value; }
            get { return _runningamtbalance; }
        }
        public int? MinPointBalance
        {
            set { _minpointbalance = value; }
            get { return _minpointbalance; }
        }
        public int? RunningPointBalance
        {
            set { _runningpointbalance = value; }
            get { return _runningpointbalance; }
        }

        #endregion Model

    }
}

