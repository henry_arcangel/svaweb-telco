﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡消费赚积分规则表。（只根据金额计算积分）
	///一次性奖励积分，在SVAReward中设置。 
	/// </summary>
	[Serializable]
	public partial class PointRule
	{
		public PointRule()
		{}
		#region Model
		private int _pointruleid;
		private int? _cardtypeid;
		private int? _cardgradeid;
		private int? _pointruletype=0;
		private int? _pointruleseqno=0;
		private int _pointruleoper;
		private decimal _pointruleamount;
		private int _pointrulepoints;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private int? _pointrulelevel=0;
		private int? _memberdatetype=0;
		private int? _effectivedatetype=0;
		private int? _effectivedate=0;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private int? _status=1;
		private string _pointrulecode="";
		private string _pointruledesc1;
		private string _pointruledesc2;
		private string _pointruledesc3;
		private int? _hititem=0;
		private string _dayflagcode;
		private string _weekflagcode;
		private string _monthflagcode;
		/// <summary>
		/// 设置规则主键
		/// </summary>
		public int PointRuleID
		{
			set{ _pointruleid=value;}
			get{return _pointruleid;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡等级ID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 计算规则类型，默认0：
        //0：参与计算的金额类型为：消费金额
        //1：参与计算的金额类型为：使用SVA卡支付的金额
        //2：参与计算的金额类型为：SVA卡充值的金额
		/// </summary>
		public int? PointRuleType
		{
			set{ _pointruletype=value;}
			get{return _pointruletype;}
		}
		/// <summary>
		/// 记录序号
		/// </summary>
		public int? PointRuleSeqNo
		{
			set{ _pointruleseqno=value;}
			get{return _pointruleseqno;}
		}
		/// <summary>
		/// 计算方式设置：
        //0：每当消费PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分。  如：PointRuleAmount=100，PointRulePoints=10， 消费2000， 就可以获得 （2000 /100 ）* 10 = 200
        //1：当消费金额大于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分
        //2：当消费金额小于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分

		/// </summary>
		public int PointRuleOper
		{
			set{ _pointruleoper=value;}
			get{return _pointruleoper;}
		}
		/// <summary>
		/// 设定的积分规则金额
		/// </summary>
		public decimal PointRuleAmount
		{
			set{ _pointruleamount=value;}
			get{return _pointruleamount;}
		}
		/// <summary>
		/// 设定的积分规则积分
		/// </summary>
		public int PointRulePoints
		{
			set{ _pointrulepoints=value;}
			get{return _pointrulepoints;}
		}
		/// <summary>
		/// 开始日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		/// 结束日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		/// 规则级别。 默认0.  数字越大级别越高。 高级别优先。
		/// </summary>
		public int? PointRuleLevel
		{
			set{ _pointrulelevel=value;}
			get{return _pointrulelevel;}
		}
		/// <summary>
		/// 会员范围类型。 
        //0：不起作用。
        //1：生日当月。 
        //2：生日当周。 
        //3：生日当日。
        //4：卡激活当月。 
        //5：卡激活当周。 
        //6：卡激活当日。
		/// </summary>
		public int? MemberDateType
		{
			set{ _memberdatetype=value;}
			get{return _memberdatetype;}
		}
		/// <summary>
		/// 生效日期类型。 0：不做要求。 1：月。2：周。3：日
		/// </summary>
		public int? EffectiveDateType
		{
			set{ _effectivedatetype=value;}
			get{return _effectivedatetype;}
		}
		/// <summary>
		/// 设置月中哪一天生效。（根据EffectiveDateType判断含义）。 默认0：不起作用。
		/// </summary>
		public int? EffectiveDate
		{
			set{ _effectivedate=value;}
			get{return _effectivedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointRuleCode
		{
			set{ _pointrulecode=value;}
			get{return _pointrulecode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointRuleDesc1
		{
			set{ _pointruledesc1=value;}
			get{return _pointruledesc1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointRuleDesc2
		{
			set{ _pointruledesc2=value;}
			get{return _pointruledesc2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointRuleDesc3
		{
			set{ _pointruledesc3=value;}
			get{return _pointruledesc3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HitItem
		{
			set{ _hititem=value;}
			get{return _hititem;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DayFlagCode
		{
			set{ _dayflagcode=value;}
			get{return _dayflagcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string WeekFlagCode
		{
			set{ _weekflagcode=value;}
			get{return _weekflagcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MonthFlagCode
		{
			set{ _monthflagcode=value;}
			get{return _monthflagcode;}
		}
		#endregion Model

	}
}

