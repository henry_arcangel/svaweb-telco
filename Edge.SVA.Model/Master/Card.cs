﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 会员卡表
	/// </summary>
	[Serializable]
	public partial class Card
	{
		public Card()
		{}
		#region Model
		private string _cardnumber;
		private int? _cardtypeid;
		private DateTime _cardissuedate;
		private DateTime _cardexpirydate;
		private int _cardgradeid;
		private int? _memberid;
		private int? _batchcardid;
		private int _status=0;
		private int _usedcount=0;
		private string _cardpassword;
		private int? _totalpoints=0;
		private decimal? _totalamount=0M;
		private string _parentcardnumber;
		private int? _cardforfeitpoints=0;
		private decimal? _cardforfeitamount=0M;
		private int? _resetpassword=0;
		private DateTime? _cardamountexpirydate;
		private DateTime? _cardpointexpirydate;
		private int? _cumulativeearnpoints;
		private decimal? _cumulativeconsumptionamt;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private DateTime? _passwordexpirydate;
		private int? _pwdexpirypromptdays;
		private string _approvedcode;
		private int? _stockstatus;
		private int? _issuestoreid;
		private int? _activestoreid;
		private DateTime? _activedate;
		private DateTime? _pickupflag;
		/// <summary>
		/// 卡号。 作为唯一主键。 查询时，只输入卡号就可以唯一定位
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡发行日期
		/// </summary>
		public DateTime CardIssueDate
		{
			set{ _cardissuedate=value;}
			get{return _cardissuedate;}
		}
		/// <summary>
		/// 卡过期日期
		/// </summary>
		public DateTime CardExpiryDate
		{
			set{ _cardexpirydate=value;}
			get{return _cardexpirydate;}
		}
		/// <summary>
		/// 卡级别（CardGrad表）
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 会员表主键
		/// </summary>
		public int? MemberID
		{
			set{ _memberid=value;}
			get{return _memberid;}
		}
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int? BatchCardID
		{
			set{ _batchcardid=value;}
			get{return _batchcardid;}
		}
		/// <summary>
		/// 卡状态：
        ///0:	Dormant (need active) 		                
        ///1:	Active (can be used) 		               
        ///2:	already redeem  (can be used)                   
        ///3:	already void			                
        ///4:	already expiry	 		                
        ///5:	issued （need active）			        
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 使用次数
		/// </summary>
		public int UsedCount
		{
			set{ _usedcount=value;}
			get{return _usedcount;}
		}
		/// <summary>
		/// 卡密码。 存放的密码是 MD5的密文。 初始密码为 123456 （MD5 加密后存放）
		/// </summary>
		public string CardPassword
		{
			set{ _cardpassword=value;}
			get{return _cardpassword;}
		}
		/// <summary>
		/// 积分余额
		/// </summary>
		public int? TotalPoints
		{
			set{ _totalpoints=value;}
			get{return _totalpoints;}
		}
		/// <summary>
		/// 金额余额
		/// </summary>
		public decimal? TotalAmount
		{
			set{ _totalamount=value;}
			get{return _totalamount;}
		}
		/// <summary>
		/// 主卡号。 主卡必须相同cardtype
		/// </summary>
		public string ParentCardNumber
		{
			set{ _parentcardnumber=value;}
			get{return _parentcardnumber;}
		}
		/// <summary>
		/// 失效的积分余额。
		/// </summary>
		public int? CardForfeitPoints
		{
			set{ _cardforfeitpoints=value;}
			get{return _cardforfeitpoints;}
		}
		/// <summary>
		/// 失效的金额余额。
		/// </summary>
		public decimal? CardForfeitAmount
		{
			set{ _cardforfeitamount=value;}
			get{return _cardforfeitamount;}
		}
		/// <summary>
		/// 是否需要重置密码。默认 0 0：不需要。  1：需要重置。2：密码过期
		/// </summary>
		public int? ResetPassword
		{
			set{ _resetpassword=value;}
			get{return _resetpassword;}
		}
		/// <summary>
		/// 卡中金额有效期。 如果是多有效期， 这里取最大有效期。
		/// </summary>
		public DateTime? CardAmountExpiryDate
		{
			set{ _cardamountexpirydate=value;}
			get{return _cardamountexpirydate;}
		}
		/// <summary>
		/// 卡中积分有效期。 如果是多有效期， 这里取最大有效期。
		/// </summary>
		public DateTime? CardPointExpiryDate
		{
			set{ _cardpointexpirydate=value;}
			get{return _cardpointexpirydate;}
		}
		/// <summary>
		/// 累计获得的积分
		/// </summary>
		public int? CumulativeEarnPoints
		{
			set{ _cumulativeearnpoints=value;}
			get{return _cumulativeearnpoints;}
		}
		/// <summary>
		/// 累计消费金额
		/// </summary>
		public decimal? CumulativeConsumptionAmt
		{
			set{ _cumulativeconsumptionamt=value;}
			get{return _cumulativeconsumptionamt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? PasswordExpiryDate
		{
			set{ _passwordexpirydate=value;}
			get{return _passwordexpirydate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PWDExpiryPromptDays
		{
			set{ _pwdexpirypromptdays=value;}
			get{return _pwdexpirypromptdays;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ApprovedCode
		{
			set{ _approvedcode=value;}
			get{return _approvedcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? StockStatus
		{
			set{ _stockstatus=value;}
			get{return _stockstatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IssueStoreID
		{
			set{ _issuestoreid=value;}
			get{return _issuestoreid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ActiveStoreID
		{
			set{ _activestoreid=value;}
			get{return _activestoreid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ActiveDate
		{
			set{ _activedate=value;}
			get{return _activedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? PickupFlag
		{
			set{ _pickupflag=value;}
			get{return _pickupflag;}
		}
		#endregion Model

	}
}

