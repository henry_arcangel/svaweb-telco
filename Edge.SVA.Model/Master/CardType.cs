﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡类型表
	/// </summary>
	[Serializable]
	public partial class CardType
	{
		public CardType()
		{}
		#region Model
		private int _cardtypeid;
		private string _cardtypecode;
		private string _cardtypename1;
		private string _cardtypename2;
		private string _cardtypename3;
		private int _brandid;
		private string _cardtypenotes;
		private string _cardnummask;
		private string _cardnumpattern;
		private int? _cardmusthasowner=1;
		private int? _cardverifymethod=0;
		private DateTime? _cardtypestartdate;
		private DateTime? _cardtypeenddate;
		private int? _cardtypenatureid=1;
		private int _status=1;
		private string _cardextendcode;
		private int? _cardcheckdigit=0;
		private int? _checkdigitmodeid;
		private int? _cashexpiredate=1;
		private int? _pointexpiredate=1;
		private int? _currencyid;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private int? _passwordruleid;
		private int? _isphysicalcard;
		private int? _isimportuidnumber=0;
		private int? _isconsecutiveuid=1;
		private int? _uidtocardnumber=0;
		private int? _uidcheckdigit=0;
		private int? _cardnumbertouid=0;
        private int? _isdumpcard = 0; //Add By Robin 2015-04-08 for RRG Telco
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡类型编码
		/// </summary>
		public string CardTypeCode
		{
			set{ _cardtypecode=value;}
			get{return _cardtypecode;}
		}
		/// <summary>
		/// 卡类型名称1
		/// </summary>
		public string CardTypeName1
		{
			set{ _cardtypename1=value;}
			get{return _cardtypename1;}
		}
		/// <summary>
		/// 卡类型名称2
		/// </summary>
		public string CardTypeName2
		{
			set{ _cardtypename2=value;}
			get{return _cardtypename2;}
		}
		/// <summary>
		/// 卡类型名称3
		/// </summary>
		public string CardTypeName3
		{
			set{ _cardtypename3=value;}
			get{return _cardtypename3;}
		}
		/// <summary>
		/// 卡所属品牌
		/// </summary>
		public int BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
		/// <summary>
		/// 卡类型备注
		/// </summary>
		public string CardTypeNotes
		{
			set{ _cardtypenotes=value;}
			get{return _cardtypenotes;}
		}
		/// <summary>
		/// 卡号码规则
		/// </summary>
		public string CardNumMask
		{
			set{ _cardnummask=value;}
			get{return _cardnummask;}
		}
		/// <summary>
		/// 按照卡号码规则的初始值
		/// </summary>
		public string CardNumPattern
		{
			set{ _cardnumpattern=value;}
			get{return _cardnumpattern;}
		}
		/// <summary>
		/// 激活时是否必须要绑定member。0：不需要绑定。 1：需要绑定。 默认1。

		/// </summary>
		public int? CardMustHasOwner
		{
			set{ _cardmusthasowner=value;}
			get{return _cardmusthasowner;}
		}
		/// <summary>
		/// 卡验证方式。NULL或0：不验证。 1：Visual Verify。 2：Online Verify。3：Visual Verify & Negative file
		/// </summary>
		public int? CardVerifyMethod
		{
			set{ _cardverifymethod=value;}
			get{return _cardverifymethod;}
		}
		/// <summary>
		/// 卡类型生效日期
		/// </summary>
		public DateTime? CardTypeStartDate
		{
			set { _cardtypestartdate=value;}
			get{return _cardtypestartdate;}
		}
		/// <summary>
		/// 卡类型失效日期
		/// </summary>
		public DateTime? CardTypeEndDate
		{
			set{ _cardtypeenddate=value;}
			get{return _cardtypeenddate;}
		}
		/// <summary>
		/// 卡类型种类。外键ID

		/// </summary>
		public int? CardTypeNatureID
		{
			set{ _cardtypenatureid=value;}
			get{return _cardtypenatureid;}
		}
		/// <summary>
		/// 卡类型状态。0：无效。 1：有效。
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 与其他系统关联的code
		/// </summary>
		public string CardExtendCode
		{
			set{ _cardextendcode=value;}
			get{return _cardextendcode;}
		}
		/// <summary>
		/// 卡号是否包含校验位。默认0。0：没有。 1：有校验位。

		/// </summary>
		public int? CardCheckdigit
		{
			set{ _cardcheckdigit=value;}
			get{return _cardcheckdigit;}
		}
		/// <summary>
		/// 校验位产生逻辑表的外键（预留）。 CardCheckdigit设置为1时生效。 

		/// </summary>
		public int? CheckDigitModeID
		{
			set{ _checkdigitmodeid=value;}
			get{return _checkdigitmodeid;}
		}
		/// <summary>
		/// 金额是否有有效期。默认1。0：没有（只按照卡的有效期）。 1：有多有效期（每一笔充值金额都有独立的有效期）
		/// </summary>
		public int? CashExpiredate
		{
			set{ _cashexpiredate=value;}
			get{return _cashexpiredate;}
		}
		/// <summary>
		/// 积分是否有有效期。默认1。0：没有（只按照卡的有效期）。 1：有多有效期（每一笔充值金额都有独立的有效期）
		/// </summary>
		public int? PointExpiredate
		{
			set{ _pointexpiredate=value;}
			get{return _pointexpiredate;}
		}
		/// <summary>
		/// 币种ID，外键

		/// </summary>
		public int? CurrencyID
		{
			set{ _currencyid=value;}
			get{return _currencyid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PasswordRuleID
		{
			set{ _passwordruleid=value;}
			get{return _passwordruleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsPhysicalCard
		{
			set{ _isphysicalcard=value;}
			get{return _isphysicalcard;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsImportUIDNumber
		{
			set{ _isimportuidnumber=value;}
			get{return _isimportuidnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsConsecutiveUID
		{
			set{ _isconsecutiveuid=value;}
			get{return _isconsecutiveuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDToCardNumber
		{
			set{ _uidtocardnumber=value;}
			get{return _uidtocardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDCheckDigit
		{
			set{ _uidcheckdigit=value;}
			get{return _uidcheckdigit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardNumberToUID
		{
			set{ _cardnumbertouid=value;}
			get{return _cardnumbertouid;}
		}
        /// <summary>
		/// 判断是否是虚拟卡为了RRG的Telco等店铺/总部绑定充值卡
		/// </summary>
        public int? IsDumpCard
		{
			set{ _isdumpcard=value;}
			get{return _isdumpcard;}
		}
        
		#endregion Model

	}
}

