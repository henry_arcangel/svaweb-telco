﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 批次表
	/// </summary>
	[Serializable]
	public partial class BatchCard
	{
		public BatchCard()
		{}
		#region Model
		private int _batchcardid;
		private string _batchcardcode;
		private int _qty=1;
		private int _cardgradeid;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private long? _seqfrom;
		private long? _seqto;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int BatchCardID
		{
			set{ _batchcardid=value;}
			get{return _batchcardid;}
		}
		/// <summary>
		/// 批次ID。 组成规则： CardTypeID（或CouponTypeID） + 序号
		/// </summary>
		public string BatchCardCode
		{
			set{ _batchcardcode=value;}
			get{return _batchcardcode;}
		}
		/// <summary>
		/// 批次创建数量
		/// </summary>
		public int Qty
		{
			set{ _qty=value;}
			get{return _qty;}
		}
		/// <summary>
		/// 卡等级ID， 外键
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public long? SeqFrom
		{
			set{ _seqfrom=value;}
			get{return _seqfrom;}
		}
		/// <summary>
		/// 
		/// </summary>
		public long? SeqTo
		{
			set{ _seqto=value;}
			get{return _seqto;}
		}
		#endregion Model

	}
}

