﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Bank:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Bank
	{
		public Bank()
		{}
		#region Model
		private int _bankid;
		private string _bankcode;
		private string _bankname1;
		private string _bankname2;
		private string _bankname3;
		private DateTime? _createdon;
		private int? _createdby;
		private DateTime? _updatedon;
		private int? _updatedby;
		/// <summary>
		/// 主键
		/// </summary>
		public int BankID
		{
			set{ _bankid=value;}
			get{return _bankid;}
		}
		/// <summary>
		/// 银行代码
		/// </summary>
		public string BankCode
		{
			set{ _bankcode=value;}
			get{return _bankcode;}
		}
		/// <summary>
		/// 银行名称1
		/// </summary>
		public string BankName1
		{
			set{ _bankname1=value;}
			get{return _bankname1;}
		}
		/// <summary>
		/// 银行名称2
		/// </summary>
		public string BankName2
		{
			set{ _bankname2=value;}
			get{return _bankname2;}
		}
		/// <summary>
		/// 银行名称3
		/// </summary>
		public string BankName3
		{
			set{ _bankname3=value;}
			get{return _bankname3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

