﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 卡拣货单子表
    ///表中brandID，cardtypeid，cardgradeid 关系不做校验，由UI在创建单据时做校验。
    ///存储过程实际操作时，只按照CardGradeID来做。
    ///（实际拣货首号码必须填，尾号码可以根据数量来计算，具体根据业务需求来做）
    /// </summary>
    [Serializable]
    public partial class Ord_CardPicking_D
    {
        public Ord_CardPicking_D()
        { }
        #region Model
        private int _keyid;
        private string _cardpickingnumber;
        private int? _cardtypeid;
        private int? _cardgradeid;
        private string _description;
        private int? _orderqty;
        private int? _pickqty;
        private int? _actualqty;
        private string _firstcardnumber;
        private string _endcardnumber;
        private string _batchcardcode;
        private DateTime? _pickupdatetime = DateTime.Now;
        private int? _orderamount;
        private int? _pickamount;
        private int? _actualamount;
        private int? _orderpoint;
        private int? _pickpoint;
        private int? _actualpoint;
        /// <summary>
        /// 
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 订单编号，主键
        /// </summary>
        public string CardPickingNumber
        {
            set { _cardpickingnumber = value; }
            get { return _cardpickingnumber; }
        }
        /// <summary>
        /// 卡类型ID
        /// </summary>
        public int? CardTypeID
        {
            set { _cardtypeid = value; }
            get { return _cardtypeid; }
        }
        /// <summary>
        /// 卡级别ID
        /// </summary>
        public int? CardGradeID
        {
            set { _cardgradeid = value; }
            get { return _cardgradeid; }
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int? OrderQTY
        {
            set { _orderqty = value; }
            get { return _orderqty; }
        }
        /// <summary>
        /// 要求拣货数量
        /// </summary>
        public int? PickQTY
        {
            set { _pickqty = value; }
            get { return _pickqty; }
        }
        /// <summary>
        /// 实际拣货数量
        /// </summary>
        public int? ActualQTY
        {
            set { _actualqty = value; }
            get { return _actualqty; }
        }
        /// <summary>
        /// 实际拣货批次的首卡号
        /// </summary>
        public string FirstCardNumber
        {
            set { _firstcardnumber = value; }
            get { return _firstcardnumber; }
        }
        /// <summary>
        /// 实际拣货批次的末卡号
        /// </summary>
        public string EndCardNumber
        {
            set { _endcardnumber = value; }
            get { return _endcardnumber; }
        }
        /// <summary>
        /// FirstCardNumber的批次号编码
        /// </summary>
        public string BatchCardCode
        {
            set { _batchcardcode = value; }
            get { return _batchcardcode; }
        }
        /// <summary>
        /// 创建此明细的时间
        /// </summary>
        public DateTime? PickupDateTime
        {
            set { _pickupdatetime = value; }
            get { return _pickupdatetime; }
        }
        /// <summary>
        /// 订单金额
        /// </summary>
        public int? OrderAmount
        {
            set { _orderamount = value; }
            get { return _orderamount; }
        }
        /// <summary>
        /// 拣货金额
        /// </summary>
        public int? PickAmount
        {
            set { _pickamount = value; }
            get { return _pickamount; }
        }
        /// <summary>
        /// 实际拣货金额
        /// </summary>
        public int? ActualAmount
        {
            set { _actualamount = value; }
            get { return _actualamount; }
        }
        /// <summary>
        /// 订单积分
        /// </summary>
        public int? OrderPoint
        {
            set { _orderpoint = value; }
            get { return _orderpoint; }
        }
        /// <summary>
        /// 拣货积分
        /// </summary>
        public int? PickPoint
        {
            set { _pickpoint = value; }
            get { return _pickpoint; }
        }
        /// <summary>
        /// 实际拣货积分
        /// </summary>
        public int? ActualPoint
        {
            set { _actualpoint = value; }
            get { return _actualpoint; }
        }
        #endregion Model

    }
}

