﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon自动补货设置子表。 通知邮箱设置表。
    /// </summary>
    [Serializable]
    public partial class CouponReplenishRule_EMail
    {
        public CouponReplenishRule_EMail()
        { }
        #region Model
        private int _keyid;
        private string _couponreplenishcode;
        private string _email;
        /// <summary>
        /// 自增长ID
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 主表主键
        /// </summary>
        public string CouponReplenishCode
        {
            set { _couponreplenishcode = value; }
            get { return _couponreplenishcode; }
        }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string EMail
        {
            set { _email = value; }
            get { return _email; }
        }
        #endregion Model

    }
}

