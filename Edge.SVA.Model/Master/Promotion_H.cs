﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 促销头表。 （包含所有被动促销设置.  促销会根据具体的销售单，顾客情况，自动匹配合适的促销方案）
	/// </summary>
	[Serializable]
	public partial class Promotion_H
	{
		public Promotion_H()
		{}
		#region Model
		private string _promotioncode;
		private string _promotiondesc1;
		private string _promotiondesc2;
		private string _promotiondesc3;
		private string _promotionnote;
		private int? _storeid;
		private int? _storegroupid;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private DateTime? _starttime= DateTime.Now;
		private DateTime? _endtime= DateTime.Now;
		private int? _dayflagid;
		private int? _weekflagid;
		private int? _monthflagid;
		private int? _loyaltyflag=0;
		private int? _priority;
		private int? _toplimit=1;
		private int? _mutexflag;
		private int? _hitrelation=1;
		private DateTime? _createdbusdate;
		private DateTime? _approvebusdate;
		private string _approvalcode;
		private string _approvestatus;
		private DateTime? _approveon;
		private int? _approveby;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		///11 促销编码
		/// </summary>
		public string PromotionCode
		{
			set{ _promotioncode=value;}
			get{return _promotioncode;}
		}
		/// <summary>
		///11 促销描述1
		/// </summary>
		public string PromotionDesc1
		{
			set{ _promotiondesc1=value;}
			get{return _promotiondesc1;}
		}
		/// <summary>
		///11 促销描述2
		/// </summary>
		public string PromotionDesc2
		{
			set{ _promotiondesc2=value;}
			get{return _promotiondesc2;}
		}
		/// <summary>
		///11 促销描述3
		/// </summary>
		public string PromotionDesc3
		{
			set{ _promotiondesc3=value;}
			get{return _promotiondesc3;}
		}
		/// <summary>
		///11 备注
		/// </summary>
		public string PromotionNote
		{
			set{ _promotionnote=value;}
			get{return _promotionnote;}
		}
		/// <summary>
		///11 适用的店铺ID。0/NULL： 表示所有。
		/// </summary>
		public int? StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		///11 这组价格针对的店铺组ID。0/NULL： 表示所有
		/// </summary>
		public int? StoreGroupID
		{
			set{ _storegroupid=value;}
			get{return _storegroupid;}
		}
		/// <summary>
		///11 生效日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		///11 失效日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		///11 促销生效时间（例如 上午9:00），默认0表示全天有效
		/// </summary>
		public DateTime? StartTime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		///11 促销失效时间（例如 下午20:00），默认0表示全天有效
		/// </summary>
		public DateTime? EndTime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		///11 一月中促销生效日 （Buy_DayFlag表Code）
		/// </summary>
		public int? DayFlagID
		{
			set{ _dayflagid=value;}
			get{return _dayflagid;}
		}
		/// <summary>
		///11 一周中促销生效日 （Buy_WeekFlag表Code）
		/// </summary>
		public int? WeekFlagID
		{
			set{ _weekflagid=value;}
			get{return _weekflagid;}
		}
		/// <summary>
		///11 促销生效月 （Buy_MonthFlag表Code）
		/// </summary>
		public int? MonthFlagID
		{
			set{ _monthflagid=value;}
			get{return _monthflagid;}
		}
		/// <summary>
		///11 是否仅会员促销。0：不是。1：是的
		/// </summary>
		public int? LoyaltyFlag
		{
			set{ _loyaltyflag=value;}
			get{return _loyaltyflag;}
		}
		/// <summary>
		///11 优先级
		/// </summary>
		public int? Priority
		{
			set{ _priority=value;}
			get{return _priority;}
		}
		/// <summary>
		///11 上限数量。  一单中此促销允许重复的上限。默认1.  只允许出现一次。
		/// </summary>
		public int? TopLimit
		{
			set{ _toplimit=value;}
			get{return _toplimit;}
		}
		/// <summary>
		///11 互斥标志。 0 : 可以和其他促销共同出现。 1：只允许这一条出现。
		/// </summary>
		public int? MutexFlag
		{
			set{ _mutexflag=value;}
			get{return _mutexflag;}
		}
		/// <summary>
		///11 hit条件之间的逻辑关系。 1： or 。 2：and   默认1
		/// </summary>
		public int? HitRelation
		{
			set{ _hitrelation=value;}
			get{return _hitrelation;}
		}
		/// <summary>
		///11 促销创建时的busdate
		/// </summary>
		public DateTime? CreatedBusDate
		{
			set{ _createdbusdate=value;}
			get{return _createdbusdate;}
		}
		/// <summary>
		///11 促销批核时的busdate
		/// </summary>
		public DateTime? ApproveBusDate
		{
			set{ _approvebusdate=value;}
			get{return _approvebusdate;}
		}
		/// <summary>
		///11 批核时产生授权号，并通知前台
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		///11 单据状态。状态： P：Pending。  A:Approve 。 V：Void
		/// </summary>
		public string ApproveStatus
		{
			set{ _approvestatus=value;}
			get{return _approvestatus;}
		}
		/// <summary>
		///11 批核时间
		/// </summary>
		public DateTime? ApproveOn
		{
			set{ _approveon=value;}
			get{return _approveon;}
		}
		/// <summary>
		///11 批核人
		/// </summary>
		public int? ApproveBy
		{
			set{ _approveby=value;}
			get{return _approveby;}
		}
		/// <summary>
		///11 创建时间
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 创建人
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 修改时间
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 修改人
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

