﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠券类别列表 （CouponType 子表）
	/// </summary>
	[Serializable]
	public partial class CouponNatureList
	{
		public CouponNatureList()
		{}
		#region Model
		private int _keyid;
		private int _coupontypeid;
		private int _couponnatureid;
		/// <summary>
		/// 自增长ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 优惠券类型主键
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 优惠券类别主键
		/// </summary>
		public int CouponNatureID
		{
			set{ _couponnatureid=value;}
			get{return _couponnatureid;}
		}
		#endregion Model

	}
}

