﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// t退货单子表
    ///
    /// </summary>
    [Serializable]
    public partial class Ord_CardReturn_D
    {
        public Ord_CardReturn_D()
        { }
        #region Model
        private int _keyid;
        private string _cardreturnnumber;
        private int? _cardtypeid;
        private int? _cardgradeid;
        private string _description;
        private int? _orderqty;
        private int? _actualqty;
        private string _firstcardnumber;
        private string _endcardnumber;
        private string _batchcardcode;
        /// <summary>
        /// 
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 订单编号，主键
        /// </summary>
        public string CardReturnNumber
        {
            set { _cardreturnnumber = value; }
            get { return _cardreturnnumber; }
        }
        /// <summary>
        /// 卡类型ID
        /// </summary>
        public int? CardTypeID
        {
            set { _cardtypeid = value; }
            get { return _cardtypeid; }
        }
        /// <summary>
        /// 卡级别ID
        /// </summary>
        public int? CardGradeID
        {
            set { _cardgradeid = value; }
            get { return _cardgradeid; }
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 订货的订单数量
        /// </summary>
        public int? OrderQTY
        {
            set { _orderqty = value; }
            get { return _orderqty; }
        }
        /// <summary>
        /// 实际收到的数量
        /// </summary>
        public int? ActualQTY
        {
            set { _actualqty = value; }
            get { return _actualqty; }
        }
        /// <summary>
        /// 实际收货批次的首Card号
        /// </summary>
        public string FirstCardNumber
        {
            set { _firstcardnumber = value; }
            get { return _firstcardnumber; }
        }
        /// <summary>
        /// 实际收货批次的末Card号
        /// </summary>
        public string EndCardNumber
        {
            set { _endcardnumber = value; }
            get { return _endcardnumber; }
        }
        /// <summary>
        /// FirstCardNumber的批次号编码
        /// </summary>
        public string BatchCardCode
        {
            set { _batchcardcode = value; }
            get { return _batchcardcode; }
        }
        #endregion Model

    }
}

