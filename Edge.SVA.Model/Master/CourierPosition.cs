﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 送货员位置表
	/// </summary>
	[Serializable]
	public partial class CourierPosition
	{
		public CourierPosition()
		{}
		#region Model
		private string _couriercode;
		private string _couriername;
		private int? _status=1;
		private string _longitude;
		private string _latitude;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		/// <summary>
		/// 送货员编号
		/// </summary>
		public string CourierCode
		{
			set{ _couriercode=value;}
			get{return _couriercode;}
		}
		/// <summary>
		/// 送货员名称
		/// </summary>
		public string CourierName
		{
			set{ _couriername=value;}
			get{return _couriername;}
		}
		/// <summary>
		/// 送货员状态：
//0：未工作。 1：工作状态
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 送货员当前经度
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 送货员当前纬度
		/// </summary>
		public string latitude
		{
			set{ _latitude=value;}
			get{return _latitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

