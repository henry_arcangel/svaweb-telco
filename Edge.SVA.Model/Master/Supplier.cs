﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 供货商
    /// </summary>
    [Serializable]
    public partial class Supplier
    {
        public Supplier()
        { }
        #region Model
        private int _supplierid;
        private string _suppliercode;
        private string _supplierdesc1;
        private string _supplierdesc2;
        private string _supplierdesc3;
        private string _supplieraddress;
        private string _otheraddress1;
        private string _otheraddress2;
        private string _supplieremail;
        private string _contact;
        private string _contactphone;
        private string _contactemail;
        private string _contactmobile;
        private string _remark;
        private DateTime? _createdon = DateTime.Now;
        private int? _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private int? _updatedby;
        /// <summary>
        /// 自增长主键
        /// </summary>
        public int SupplierID
        {
            set { _supplierid = value; }
            get { return _supplierid; }
        }
        /// <summary>
        /// 供货商编码
        /// </summary>
        public string SupplierCode
        {
            set { _suppliercode = value; }
            get { return _suppliercode; }
        }
        /// <summary>
        /// 供货商描述1
        /// </summary>
        public string SupplierDesc1
        {
            set { _supplierdesc1 = value; }
            get { return _supplierdesc1; }
        }
        /// <summary>
        /// 供货商描述2
        /// </summary>
        public string SupplierDesc2
        {
            set { _supplierdesc2 = value; }
            get { return _supplierdesc2; }
        }
        /// <summary>
        /// 供货商描述3
        /// </summary>
        public string SupplierDesc3
        {
            set { _supplierdesc3 = value; }
            get { return _supplierdesc3; }
        }
        /// <summary>
        /// 供货商主地址
        /// </summary>
        public string SupplierAddress
        {
            set { _supplieraddress = value; }
            get { return _supplieraddress; }
        }
        /// <summary>
        /// 供货商地址1
        /// </summary>
        public string OtherAddress1
        {
            set { _otheraddress1 = value; }
            get { return _otheraddress1; }
        }
        /// <summary>
        /// 供货商地址2
        /// </summary>
        public string OtherAddress2
        {
            set { _otheraddress2 = value; }
            get { return _otheraddress2; }
        }
        /// <summary>
        /// 供货商Email
        /// </summary>
        public string SupplierEmail
        {
            set { _supplieremail = value; }
            get { return _supplieremail; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact
        {
            set { _contact = value; }
            get { return _contact; }
        }
        /// <summary>
        /// 联系人电话
        /// </summary>
        public string ContactPhone
        {
            set { _contactphone = value; }
            get { return _contactphone; }
        }
        /// <summary>
        /// 联系人Email
        /// </summary>
        public string ContactEmail
        {
            set { _contactemail = value; }
            get { return _contactemail; }
        }
        /// <summary>
        /// 联系人手机
        /// </summary>
        public string ContactMobile
        {
            set { _contactmobile = value; }
            get { return _contactmobile; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        #endregion Model

    }
}
