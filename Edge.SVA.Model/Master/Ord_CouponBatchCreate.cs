﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// ó??Y???úá?′′?¨±í
	/// </summary>
	[Serializable]
	public partial class Ord_CouponBatchCreate
	{
		public Ord_CouponBatchCreate()
		{}
		#region Model
		private string _couponcreatenumber;
		private int _coupontypeid;
		private int _couponcount;
		private string _note;
		private DateTime? _issueddate;
		private DateTime? _expirydate;
		private decimal? _initamount=0M;
		private int? _initpoints=0;
		private int? _randompwd=0;
		private string _initpassword;
		private string _approvalcode;
		private string _approvestatus="P";
		private int? _batchcouponid;
		private DateTime? _approveon;
		private int? _approveby;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		private DateTime? _createdbusdate;
		private DateTime? _approvebusdate;
		private int? _initstatus=0;
		/// <summary>
		///11 ?¨?¨μ￥o???￡? ?÷?ü
		/// </summary>
		public string CouponCreateNumber
		{
			set{ _couponcreatenumber=value;}
			get{return _couponcreatenumber;}
		}
		/// <summary>
		///11 ?¨μè??ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		///11 ?¨êyá?
		/// </summary>
		public int CouponCount
		{
			set{ _couponcount=value;}
			get{return _couponcount;}
		}
		/// <summary>
		///11 ±?×￠
		/// </summary>
		public string Note
		{
			set{ _note=value;}
			get{return _note;}
		}
		/// <summary>
		///11 ·￠DDè??ú
		/// </summary>
		public DateTime? IssuedDate
		{
			set{ _issueddate=value;}
			get{return _issueddate;}
		}
		/// <summary>
		///11 óDD§?ú
		/// </summary>
		public DateTime? ExpiryDate
		{
			set{ _expirydate=value;}
			get{return _expirydate;}
		}
		/// <summary>
		///11 3?ê??e??
		/// </summary>
		public decimal? InitAmount
		{
			set{ _initamount=value;}
			get{return _initamount;}
		}
		/// <summary>
		///11 3?ê??y·?
		/// </summary>
		public int? InitPoints
		{
			set{ _initpoints=value;}
			get{return _initpoints;}
		}
		/// <summary>
		///11 ê?·?3?ê????ú?ü???￡0￡o2?ê??￡1:ê?μ??￡ ??è?0
		/// </summary>
		public int? RandomPWD
		{
			set{ _randompwd=value;}
			get{return _randompwd;}
		}
		/// <summary>
		///11 3?ê??ü???￡RandomPWD=1ê±￡?2?Dèòaì?
		/// </summary>
		public string InitPassword
		{
			set{ _initpassword=value;}
			get{return _initpassword;}
		}
		/// <summary>
		///11 ?úo?ê±2úéúêúè¨o?￡?2￠í¨?a?°ì¨
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		///11 ×′ì?￡o P￡oprepare?￡  A:Approve ?￡ V￡oVoid
		/// </summary>
		public string ApproveStatus
		{
			set{ _approvestatus=value;}
			get{return _approvestatus;}
		}
		/// <summary>
		///11 ?ú′?o?
		/// </summary>
		public int? BatchCouponID
		{
			set{ _batchcouponid=value;}
			get{return _batchcouponid;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? ApproveOn
		{
			set{ _approveon=value;}
			get{return _approveon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? ApproveBy
		{
			set{ _approveby=value;}
			get{return _approveby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		///11 μ￥?Y′′?¨ê±μ?busdate
		/// </summary>
		public DateTime? CreatedBusDate
		{
			set{ _createdbusdate=value;}
			get{return _createdbusdate;}
		}
		/// <summary>
		///11 μ￥?Y?úo?ê±μ?busdate
		/// </summary>
		public DateTime? ApproveBusDate
		{
			set{ _approvebusdate=value;}
			get{return _approvebusdate;}
		}
		/// <summary>
		///11 
		/// </summary>
		public int? InitStatus
		{
			set{ _initstatus=value;}
			get{return _initstatus;}
		}
		#endregion Model

	}
}

