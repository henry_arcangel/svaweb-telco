﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 面向供应商的订货单，明细表
	///如果要求向供应商提供CardNumber。  并且要求每个Number都记录下来。 那么OrderQty=1， FirstCardNumber=EndCardNumber
	/// </summary>
	[Serializable]
	public partial class Ord_OrderToSupplier_Card_D
	{
		public Ord_OrderToSupplier_Card_D()
		{}
		#region Model
		private int _keyid;
		private string _ordersuppliernumber;
		private int? _cardtypeid;
		private int? _cardgradeid;
		private int? _orderqty=1;
		private string _firstcardnumber;
		private string _endcardnumber;
		private string _batchcardcode;
		private int? _packageqty=1;
		private int? _orderroundupqty=1;
        private decimal? _orderamount = 0;
        private int? _orderpoint = 0;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 提交供应商订单单号，外键
		/// </summary>
		public string OrderSupplierNumber
		{
			set{ _ordersuppliernumber=value;}
			get{return _ordersuppliernumber;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡级别ID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 订货数量
		/// </summary>
		public int? OrderQty
		{
			set{ _orderqty=value;}
			get{return _orderqty;}
		}
		/// <summary>
		/// 提供给供应商的首card号。（如果IsProvideNumber=1，那么需要提供Card号码）
		/// </summary>
		public string FirstCardNumber
		{
			set{ _firstcardnumber=value;}
			get{return _firstcardnumber;}
		}
		/// <summary>
		/// 提供给供应商的末card号
		/// </summary>
		public string EndCardNumber
		{
			set{ _endcardnumber=value;}
			get{return _endcardnumber;}
		}
		/// <summary>
		/// FirstcardNumber的批次号编码
		/// </summary>
		public string BatchCardCode
		{
			set{ _batchcardcode=value;}
			get{return _batchcardcode;}
		}
		/// <summary>
		/// 包装数量。默认为1. 不得为0 或者 null @2014-08-28：由每一包中的Coupon数量， 改为 包裹的数量。 
		/// </summary>
		public int? PackageQty
		{
			set{ _packageqty=value;}
			get{return _packageqty;}
		}
		/// <summary>
		/// 每一包中的Card数量，来源于规则表中的 OrderRoundUpQty。 
		/// </summary>
		public int? OrderRoundUpQty
		{
			set{ _orderroundupqty=value;}
			get{return _orderroundupqty;}
		}
        /// <summary>
        /// 订货充值金额
        /// </summary>
        public decimal? OrderAmount
        {
            set { _orderamount = value; }
            get { return _orderamount; }
        }
        /// <summary>
        /// 订货积分数量
        /// </summary>
        public int? OrderPoint
        {
            set { _orderpoint = value; }
            get { return _orderpoint; }
        }
		#endregion Model

	}
}

