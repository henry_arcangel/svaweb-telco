﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Coupon自动补货设置子表。 
	/// </summary>
	[Serializable]
	public partial class GrossMargin_MobileNo
	{
        public GrossMargin_MobileNo()
		{}
		#region Model
		private int _keyid;
        private string _grossmargincode;
        private string _mobilenopattern;
        private int _patternstart;
        private int _patternlen;
		/// <summary>
		/// 自增长ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 主表主键
		/// </summary>
        public string MobileNoPattern
		{
            set { _mobilenopattern = value; }
            get { return _mobilenopattern; }
		}

        public string GrossMarginCode
        {
            set { _grossmargincode = value; }
            get { return _grossmargincode; }
        }

        public int PatternStart
		{
            set { _patternstart = value; }
            get { return _patternstart; }
		}

        public int PatternLen
        {
            set { _patternlen = value; }
            get { return _patternlen; }
        }

		#endregion Model

	}
}

