﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵号码与UID对照表。
	///（UID：客户提供的优惠劵号码）
	/// </summary>
	[Serializable]
	public partial class CouponUIDMap
	{
		public CouponUIDMap()
		{}
		#region Model
		private string _couponuid;
		private string _importcouponnumber;
		private int? _batchcouponid;
		private int? _coupontypeid;
		private string _couponnumber;
		private int? _status;
		private DateTime? _createdon;
		private int? _createdby;
		/// <summary>
		/// 优惠劵导入号码，主键
		/// </summary>
		public string CouponUID
		{
			set{ _couponuid=value;}
			get{return _couponuid;}
		}
		/// <summary>
		/// 单据号码
		/// </summary>
		public string ImportCouponNumber
		{
			set{ _importcouponnumber=value;}
			get{return _importcouponnumber;}
		}
		/// <summary>
		/// coupon 的 batchID
		/// </summary>
		public int? BatchCouponID
		{
			set{ _batchcouponid=value;}
			get{return _batchcouponid;}
		}
		/// <summary>
		/// 指定的CouponTypeID
		/// </summary>
		public int? CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 绑定的CouponNumber
		/// </summary>
		public string CouponNumber
		{
			set{ _couponnumber=value;}
			get{return _couponnumber;}
		}
		/// <summary>
		/// 状态。0：无效。 1：有效
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		#endregion Model

	}
}

