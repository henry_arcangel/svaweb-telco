﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵类型表
	/// </summary>
	[Serializable]
	public partial class CouponType
	{
		public CouponType()
		{}
		#region Model
		private int _coupontypeid;
		private int _coupontypenatureid=1;
		private string _coupontypecode;
		private string _coupontypename1;
		private string _coupontypename2;
		private string _coupontypename3;
		private int _brandid;
		private string _coupontypenotes;
		private string _couponnummask;
		private string _couponnumpattern;
		private int _couponvalidityduration;
		private int _couponvalidityunit=0;
		private int? _couponverifymethod=0;
		private int? _currencyid;
		private DateTime? _coupontypestartdate;
		private DateTime? _coupontypeenddate;
		private decimal _coupontypeamount;
		private int _coupontypepoint;
		private decimal _coupontypediscount=1M;
		private int _status=0;
		private string _coupontypelayoutfile;
		private string _coupontypepicfile;
		private bool _couponcheckdigit= false;
		private int? _checkdigitmodeid;
		private int? _campaignid;
		private int? _locatestore=0;
		private int? _ismemberbind=1;
		private int? _activeresetexpirydate=0;
		private int? _coupontypetransfer;
		private int? _passwordruleid;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private int? _coupontypeunvalued;
		private int? _coupontypefixedamount=0;
		private int? _isimportcouponnumber;
		private int? _couponforfeitcontrol;
		private int? _uidtocouponnumber=0;
		private DateTime? _couponspecifyexpirydate;
		private int? _effectscheduleid;
		private int? _uidcheckdigit=0;
		private int? _couponnumbertouid=0;
		private int? _isconsecutiveuid=1;
		private int? _couponnatureid;
		private int? _coupontyperank=0;
		private int? _autoreplenish=0;
		private DateTime? _startdatetime;
		private DateTime? _enddatetime;
		private int? _coupontyperedeemcount=0;
		private int? _sponsorid;
		private decimal? _sponsoredvalue;
		private int? _supplierid;
		private int? _allowdeletecoupon;
		private int? _allowsharecoupon;
		private string _qrcodeprefix;
		private int? _maxdownloadcoupons;
		private int? _couponreturnvalue;
		private int? _trainingmode;
		private int? _unlimitedusage;
		/// <summary>
		/// 优惠劵类型主键ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 优惠劵类型种类ID，外键
		/// </summary>
		public int CouponTypeNatureID
		{
			set{ _coupontypenatureid=value;}
			get{return _coupontypenatureid;}
		}
		/// <summary>
		/// 优惠券编码
		/// </summary>
		public string CouponTypeCode
		{
			set{ _coupontypecode=value;}
			get{return _coupontypecode;}
		}
		/// <summary>
		/// 优惠劵类型名称1
		/// </summary>
		public string CouponTypeName1
		{
			set{ _coupontypename1=value;}
			get{return _coupontypename1;}
		}
		/// <summary>
		/// 优惠劵类型名称2
		/// </summary>
		public string CouponTypeName2
		{
			set{ _coupontypename2=value;}
			get{return _coupontypename2;}
		}
		/// <summary>
		/// 优惠劵类型名称3
		/// </summary>
		public string CouponTypeName3
		{
			set{ _coupontypename3=value;}
			get{return _coupontypename3;}
		}
		/// <summary>
		/// 品牌ID
		/// </summary>
		public int BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
		/// <summary>
		/// 备注说明
		/// </summary>
		public string CouponTypeNotes
		{
			set{ _coupontypenotes=value;}
			get{return _coupontypenotes;}
		}
		/// <summary>
		/// 优惠劵号码规则
		/// </summary>
		public string CouponNumMask
		{
			set{ _couponnummask=value;}
			get{return _couponnummask;}
		}
		/// <summary>
		/// 优惠劵号码规则的初始值
		/// </summary>
		public string CouponNumPattern
		{
			set{ _couponnumpattern=value;}
			get{return _couponnumpattern;}
		}
		/// <summary>
		/// 优惠劵有效期值。
		/// </summary>
		public int CouponValidityDuration
		{
			set{ _couponvalidityduration=value;}
			get{return _couponvalidityduration;}
		}
		/// <summary>
		/// 卡有效期持续时间长度 的单位：默认0，不需要检查CouponValidityDuration值 0：永久。 1：年。 2：月。 3：星期。 4：天。 5: 有效期不变更。
		/// </summary>
		public int CouponValidityUnit
		{
			set{ _couponvalidityunit=value;}
			get{return _couponvalidityunit;}
		}
		/// <summary>
		/// 卡验证方式。NULL或0：不验证。 1：Visual Verify。 2：Online Verify。3：Visual Verify & Negative file
		/// </summary>
		public int? CouponVerifyMethod
		{
			set{ _couponverifymethod=value;}
			get{return _couponverifymethod;}
		}
		/// <summary>
		/// 币种ID，外键

		/// </summary>
		public int? CurrencyID
		{
			set{ _currencyid=value;}
			get{return _currencyid;}
		}
		/// <summary>
		/// 优惠劵类型开始生效日期
		/// </summary>
		public DateTime? CouponTypeStartDate
		{
			set{ _coupontypestartdate=value;}
			get{return _coupontypestartdate;}
		}
		/// <summary>
		/// 优惠劵类型失效日期
		/// </summary>
		public DateTime? CouponTypeEndDate
		{
			set{ _coupontypeenddate=value;}
			get{return _coupontypeenddate;}
		}
		/// <summary>
		/// 优惠劵价值，金额
		/// </summary>
		public decimal CouponTypeAmount
		{
			set{ _coupontypeamount=value;}
			get{return _coupontypeamount;}
		}
		/// <summary>
		/// 优惠劵价值，积分
		/// </summary>
		public int CouponTypePoint
		{
			set{ _coupontypepoint=value;}
			get{return _coupontypepoint;}
		}
		/// <summary>
		/// 优惠劵价值，现金折扣值（7折，记录为: 0.7）默认1
		/// </summary>
		public decimal CouponTypeDiscount
		{
			set{ _coupontypediscount=value;}
			get{return _coupontypediscount;}
		}
		/// <summary>
		/// 优惠劵种类状态。0：失效。 1：生效。
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 可存放优惠劵封面的打印模板文件 
		/// </summary>
		public string CouponTypeLayoutFile
		{
			set{ _coupontypelayoutfile=value;}
			get{return _coupontypelayoutfile;}
		}
		/// <summary>
		/// 可存放优惠劵封面的图片相对路径名称。 
		/// </summary>
		public string CouponTypePicFile
		{
			set{ _coupontypepicfile=value;}
			get{return _coupontypepicfile;}
		}
		/// <summary>
		/// 优惠劵号是否包含校验位。默认0 0：没有。 1：有校验位。
		/// </summary>
		public bool CouponCheckdigit
		{
			set{ _couponcheckdigit=value;}
			get{return _couponcheckdigit;}
		}
		/// <summary>
		/// 校验位产生逻辑表的外键（预留）。 CardCheckdigit设置为1时生效。 

		/// </summary>
		public int? CheckDigitModeID
		{
			set{ _checkdigitmodeid=value;}
			get{return _checkdigitmodeid;}
		}
		/// <summary>
		/// 类似store group 的 附加字段。 作为coupontype分类使用。
		/// </summary>
		public int? CampaignID
		{
			set{ _campaignid=value;}
			get{return _campaignid;}
		}
		/// <summary>
		/// 是否领取店铺本店使用。0：不是。1：是。 默认0
		/// </summary>
		public int? LocateStore
		{
			set{ _locatestore=value;}
			get{return _locatestore;}
		}
		/// <summary>
		/// 是否要绑定会员。0：不是，1：是的。 默认1。  如果要绑定会员，必须绑定会员后激活。
		/// </summary>
		public int? IsMemberBind
		{
			set{ _ismemberbind=value;}
			get{return _ismemberbind;}
		}
		/// <summary>
		/// 激活时是否要重置有效期. 0：不重置。 1：重置，默认0
		/// </summary>
		public int? ActiveResetExpiryDate
		{
			set{ _activeresetexpirydate=value;}
			get{return _activeresetexpirydate;}
		}
		/// <summary>
		/// 优惠劵转赠：0：不允许。1：同品牌转赠。2：同CardGrade。3：同CardType。 4：任意转赠
		/// </summary>
		public int? CouponTypeTransfer
		{
			set{ _coupontypetransfer=value;}
			get{return _coupontypetransfer;}
		}
		/// <summary>
		/// 密码规则表ID
		/// </summary>
		public int? PasswordRuleID
		{
			set{ _passwordruleid=value;}
			get{return _passwordruleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponTypeUnvalued
		{
			set{ _coupontypeunvalued=value;}
			get{return _coupontypeunvalued;}
		}
		/// <summary>
		/// 定额优惠劵。0：不定值。1：普通优惠劵。
		/// </summary>
		public int? CoupontypeFixedAmount
		{
			set{ _coupontypefixedamount=value;}
			get{return _coupontypefixedamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsImportCouponNumber
		{
			set{ _isimportcouponnumber=value;}
			get{return _isimportcouponnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponforfeitControl
		{
			set{ _couponforfeitcontrol=value;}
			get{return _couponforfeitcontrol;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDToCouponNumber
		{
			set{ _uidtocouponnumber=value;}
			get{return _uidtocouponnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CouponSpecifyExpiryDate
		{
			set{ _couponspecifyexpirydate=value;}
			get{return _couponspecifyexpirydate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? EffectScheduleID
		{
			set{ _effectscheduleid=value;}
			get{return _effectscheduleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UIDCheckDigit
		{
			set{ _uidcheckdigit=value;}
			get{return _uidcheckdigit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponNumberToUID
		{
			set{ _couponnumbertouid=value;}
			get{return _couponnumbertouid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsConsecutiveUID
		{
			set{ _isconsecutiveuid=value;}
			get{return _isconsecutiveuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponNatureID
		{
			set{ _couponnatureid=value;}
			get{return _couponnatureid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponTypeRank
		{
			set{ _coupontyperank=value;}
			get{return _coupontyperank;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AutoReplenish
		{
			set{ _autoreplenish=value;}
			get{return _autoreplenish;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? StartDateTime
		{
			set{ _startdatetime=value;}
			get{return _startdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? EndDateTime
		{
			set{ _enddatetime=value;}
			get{return _enddatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponTypeRedeemCount
		{
			set{ _coupontyperedeemcount=value;}
			get{return _coupontyperedeemcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SponsorID
		{
			set{ _sponsorid=value;}
			get{return _sponsorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SponsoredValue
		{
			set{ _sponsoredvalue=value;}
			get{return _sponsoredvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SupplierID
		{
			set{ _supplierid=value;}
			get{return _supplierid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AllowDeleteCoupon
		{
			set{ _allowdeletecoupon=value;}
			get{return _allowdeletecoupon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AllowShareCoupon
		{
			set{ _allowsharecoupon=value;}
			get{return _allowsharecoupon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string QRCodePrefix
		{
			set{ _qrcodeprefix=value;}
			get{return _qrcodeprefix;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MaxDownloadCoupons
		{
			set{ _maxdownloadcoupons=value;}
			get{return _maxdownloadcoupons;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CouponReturnValue
		{
			set{ _couponreturnvalue=value;}
			get{return _couponreturnvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TrainingMode
		{
			set{ _trainingmode=value;}
			get{return _trainingmode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnlimitedUsage
		{
			set{ _unlimitedusage=value;}
			get{return _unlimitedusage;}
		}
		#endregion Model

	}
}

