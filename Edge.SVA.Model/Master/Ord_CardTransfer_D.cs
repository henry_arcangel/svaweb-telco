﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 转赠单据表. 子表
	///@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D， 加上Ord_CardTransfer_H表。组成主从表。
	/// </summary>
	[Serializable]
	public partial class Ord_CardTransfer_D
	{
		public Ord_CardTransfer_D()
		{}
		#region Model
		private int _keyid;
		private string _cardtransfernumber;
		private int? _sourcecardtypeid;
		private int? _sourcecardgradeid;
		private string _sourcecardnumber;
		private int? _destcardtypeid;
		private int? _destcardgradeid;
		private string _destcardnumber;
		private string _originaltxnno;
		private DateTime? _txndate;
		private string _storecode;
		private string _servercode;
		private string _registercode;
		private string _brandcode;
		private int? _reasonid;
		private string _note;
		private decimal? _actamount;
		private int? _actpoints;
		private string _actcouponnumbers;
		private int? _copycardflag;
		/// <summary>
		///11 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		///11 单据号码，主键
		/// </summary>
		public string CardTransferNumber
		{
			set{ _cardtransfernumber=value;}
			get{return _cardtransfernumber;}
		}
		/// <summary>
		///11 转出的卡类型ID
		/// </summary>
		public int? SourceCardTypeID
		{
			set{ _sourcecardtypeid=value;}
			get{return _sourcecardtypeid;}
		}
		/// <summary>
		///11 转出的卡级别ID
		/// </summary>
		public int? SourceCardGradeID
		{
			set{ _sourcecardgradeid=value;}
			get{return _sourcecardgradeid;}
		}
		/// <summary>
		///11 转出的卡号
		/// </summary>
		public string SourceCardNumber
		{
			set{ _sourcecardnumber=value;}
			get{return _sourcecardnumber;}
		}
		/// <summary>
		///11 转入的卡类型ID
		/// </summary>
		public int? DestCardTypeID
		{
			set{ _destcardtypeid=value;}
			get{return _destcardtypeid;}
		}
		/// <summary>
		///11 转入的卡级别ID
		/// </summary>
		public int? DestCardGradeID
		{
			set{ _destcardgradeid=value;}
			get{return _destcardgradeid;}
		}
		/// <summary>
		///11 转入的卡号
		/// </summary>
		public string DestCardNumber
		{
			set{ _destcardnumber=value;}
			get{return _destcardnumber;}
		}
		/// <summary>
		///11 关联的原始单据号
		/// </summary>
		public string OriginalTxnNo
		{
			set{ _originaltxnno=value;}
			get{return _originaltxnno;}
		}
		/// <summary>
		///11 交易日期
		/// </summary>
		public DateTime? TxnDate
		{
			set{ _txndate=value;}
			get{return _txndate;}
		}
		/// <summary>
		///11 店铺编码
		/// </summary>
		public string StoreCode
		{
			set{ _storecode=value;}
			get{return _storecode;}
		}
		/// <summary>
		///11 服务器编号
		/// </summary>
		public string ServerCode
		{
			set{ _servercode=value;}
			get{return _servercode;}
		}
		/// <summary>
		///11 终端编号
		/// </summary>
		public string RegisterCode
		{
			set{ _registercode=value;}
			get{return _registercode;}
		}
		/// <summary>
		///11 品牌编码
		/// </summary>
		public string BrandCode
		{
			set{ _brandcode=value;}
			get{return _brandcode;}
		}
		/// <summary>
		///11 转移原因
		/// </summary>
		public int? ReasonID
		{
			set{ _reasonid=value;}
			get{return _reasonid;}
		}
		/// <summary>
		///11 备注
		/// </summary>
		public string Note
		{
			set{ _note=value;}
			get{return _note;}
		}
		/// <summary>
		///11 转移金额
		/// </summary>
		public decimal? ActAmount
		{
			set{ _actamount=value;}
			get{return _actamount;}
		}
		/// <summary>
		///11 转移积分
		/// </summary>
		public int? ActPoints
		{
			set{ _actpoints=value;}
			get{return _actpoints;}
		}
		/// <summary>
		///11 coupon列表。 例如： ‘0010001’，‘0020003’
		/// </summary>
		public string ActCouponNumbers
		{
			set{ _actcouponnumbers=value;}
			get{return _actcouponnumbers;}
		}
		/// <summary>
		///11 是否复制旧卡到新卡。null/0：只是从一个卡中转移指定值到另外一个卡。 1：旧卡换新卡。（所有数据复制到新卡），旧卡注销。
		/// </summary>
		public int? CopyCardFlag
		{
			set{ _copycardflag=value;}
			get{return _copycardflag;}
		}
		#endregion Model

	}
}

