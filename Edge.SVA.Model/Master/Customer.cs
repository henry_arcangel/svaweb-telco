﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 客户表
	/// </summary>
	[Serializable]
	public partial class Customer
	{
		public Customer()
		{}
		#region Model
		private int _customerid;
		private string _customercode;
		private string _customerdesc1;
		private string _customerdesc2;
		private string _customerdesc3;
		private string _customeraddress;
		private string _contact;
		private string _contactphone;
		private string _remark;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 顾客自增长主键
		/// </summary>
		public int CustomerID
		{
			set{ _customerid=value;}
			get{return _customerid;}
		}
		/// <summary>
		/// 顾客编码
		/// </summary>
		public string CustomerCode
		{
			set{ _customercode=value;}
			get{return _customercode;}
		}
		/// <summary>
		/// 顾客描述1
		/// </summary>
		public string CustomerDesc1
		{
			set{ _customerdesc1=value;}
			get{return _customerdesc1;}
		}
		/// <summary>
		/// 顾客描述2
		/// </summary>
		public string CustomerDesc2
		{
			set{ _customerdesc2=value;}
			get{return _customerdesc2;}
		}
		/// <summary>
		/// 顾客描述3
		/// </summary>
		public string CustomerDesc3
		{
			set{ _customerdesc3=value;}
			get{return _customerdesc3;}
		}
		/// <summary>
		/// 顾客地址
		/// </summary>
		public string CustomerAddress
		{
			set{ _customeraddress=value;}
			get{return _customeraddress;}
		}
		/// <summary>
		/// 联系人
		/// </summary>
		public string Contact
		{
			set{ _contact=value;}
			get{return _contact;}
		}
		/// <summary>
		/// 联系电话
		/// </summary>
		public string ContactPhone
		{
			set{ _contactphone=value;}
			get{return _contactphone;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

