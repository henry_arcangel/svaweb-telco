﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// GM导入表
	/// </summary>
	[Serializable]
	public partial class ImportGM
	{
        public ImportGM()
		{}
		#region Model
		private string _SKU;
		private string _UPC;
		private string _SKUDesc;
		private decimal? _SKUUnitAmount;
        private string _prodcode;
        private string _BU;
        private decimal? _GMValue;
        private string _CardTypeCode;
        private string _CardGradeCode;
        private string _NumberMask;
        private string _NumberPrefix;
		private DateTime? _createdon;
        private string _CreatedBy;
		private DateTime? _updatedon;
		private string _updatedby;
        private string _TransactionType;
        private string _Export;
        private string _CardGradeName;

        public string CardGradeName
        { set { _CardGradeName = value; }
            get { return _CardGradeName; }
        }

		/// <summary>
		/// 
		/// </summary>
        public string SKU
		{
            set { _SKU = value; }
            get { return _SKU; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string UPC
		{
            set { _UPC = value; }
            get { return _UPC; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string SKUDesc
		{
            set { _SKUDesc = value; }
            get { return _SKUDesc; }
		}
		/// <summary>
		/// 
		/// </summary>
        public decimal? SKUUnitAmount
		{
            set { _SKUUnitAmount = value; }
            get { return _SKUUnitAmount; }
		}
        /// <summary>
        /// 
        /// </summary>
        public string ProdCode
        {
            set { _prodcode = value; }
            get { return _prodcode; }
        }
		/// <summary>
		/// 
		/// </summary>
        public string BU
		{
            set { _BU = value; }
            get { return _BU; }
		}
		/// <summary>
		/// 
		/// </summary>
        public decimal? GMValue
		{
            set { _GMValue = value; }
            get { return _GMValue; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string CardTypeCode
		{
            set { _CardTypeCode = value; }
            get { return _CardTypeCode; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string CardGradeCode
		{
            set { _CardGradeCode = value; }
            get { return _CardGradeCode; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string NumberMask
		{
            set { _NumberMask = value; }
            get { return _NumberMask; }
		}
		/// <summary>
		/// 
		/// </summary>
        public string NumberPrefix
		{
            set { _NumberPrefix = value; }
            get { return _NumberPrefix; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string CreatedBy
		{
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}

        public string TransactionType
        {
            get
            {
                return _TransactionType;
            }
            set
            {
                _TransactionType = value;
            }
        }

        public string Export
        {
            get
            {
                return _Export;
            }
            set
            {
                _Export = value;
            }
        }
		#endregion Model

    

	}
}

