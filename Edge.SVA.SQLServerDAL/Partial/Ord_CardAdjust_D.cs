﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using System.Data;
using Edge.DBUtility;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardAdjust_D : IOrd_CardAdjust_D
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListWithCard(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select Ord_CardAdjust_D.KeyID,Ord_CardAdjust_D.CardAdjustNumber,Ord_CardAdjust_D.CardNumber ,");
            strSql.Append(" Card.CardGradeID,Card.CardIssueDate,Card.CardExpiryDate,Card.CardExpiryDate as OrgExpiryDate,");
            strSql.Append(" Card.BatchCardID,Card.Status,Card.CardPassword,Card.CardNumber,Card.CreatedOn,Card.UpdatedOn,Card.CreatedBy,");
            strSql.Append(" Card.UpdatedBy,Card.TotalAmount");
            strSql.Append("  FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());

        }


        public DataSet GetPageListWithCard_Movement1(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            int startNum = pageSize * currentPage + 1;
            int endNum = pageSize * (currentPage + 1);

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select E.* from(
select D.*,ROW_NUMBER() over(order by D.CardNumber asc) as rowRank,A.Additional1,A.CardAdjustNumber, A.ActAmount as TotalAmount from Ord_CardAdjust_D A inner join
(
select B.*,T.CardTypeID, T.CardGradeID, T.BatchCardID, T.CardExpiryDate
from Card_Movement B left join Card T on B.CardNumber = T.CardNumber
where B.KeyID=(select max(KeyID) from Card_Movement C where B.CardNumber=C.CardNumber)
) D on A.CardNumber=D.CardNumber where " + strWhere + @") E
where E.rowRank between " + startNum + " and " + endNum);
            return DbHelperSQL.Query(strSql.ToString());

        }
        public int GetCountWithCard_Movement1(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select count(1) from Ord_CardAdjust_D A where " + strWhere);
            return DbHelperSQL.GetCount(strSql.ToString());

        }


        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithCard(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetPageListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            int topNum = pageSize * currentPage;
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select top " + pageSize + " Ord_CardAdjust_D.KeyID,Ord_CardAdjust_D.CardAdjustNumber,Ord_CardAdjust_D.CardNumber ,");
            strSql.Append(" Card.CardTypeID,Card.CardIssueDate,Card.CardExpiryDate,Card.CardExpiryDate as OrgExpiryDate,");
            strSql.Append(" Card.BatchCardID,Card.Status,Card.CardPassword,Card.CardNumber,Card.CreatedOn,Card.UpdatedOn,Card.CreatedBy,");
            strSql.Append(" Card.UpdatedBy,Ord_CardAdjust_D.OrderAmount as OrdCardAmount,Ord_CardAdjust_D.OrderAmount,Ord_CardAdjust_D.OrderAmount as TotalAmount,Card.CardTypeID,Card.CardGradeID,Ord_CardAdjust_D.OrderPoints,Card.TotalPoints,Ord_CardAdjust_D.Additional1 ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (currentPage > 0)
            {
                strSql.Append(" where Ord_CardAdjust_D.CardNumber not in(select top " + topNum + " Ord_CardAdjust_D.CardNumber FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by " + filedOrder + ")");
            }
            if (strWhere.Trim() != "")
            {
                if (currentPage > 0)
                {
                    strSql.Append(" and " + strWhere);
                }
                else
                {
                    strSql.Append(" where " + strWhere);
                }
            }
            strSql.Append(" order by " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithCard_Movement(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Card_Movement ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetPageListWithCard_Movement(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            //SqlParameter[] parameters = {
            //        new SqlParameter("@tblName", SqlDbType.VarChar, 255),
            //        new SqlParameter("@fldName", SqlDbType.VarChar, 255),
            //        new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
            //        new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
            //        new SqlParameter("@PageSize", SqlDbType.Int),
            //        new SqlParameter("@PageIndex", SqlDbType.Int),
            //        new SqlParameter("@IsReCount", SqlDbType.Bit),
            //        new SqlParameter("@OrderType", SqlDbType.Bit),
            //        new SqlParameter("@strWhere", SqlDbType.NText),
            //        };
            //parameters[0].Value = "Coupon_Movement";
            //parameters[1].Value = "CouponNumber,CreatedOn,OrgExpiryDate,NewExpiryDate as CouponExpiryDate,OrgStatus,NewStatus as Status,OpenBal,Amount,CloseBal,CloseBal as CouponAmount";
            //parameters[2].Value = filedOrder;
            //parameters[3].Value = "";
            //parameters[4].Value = PageSize;
            //parameters[5].Value = PageIndex;
            //parameters[6].Value = 0;
            //parameters[7].Value = 0;
            //parameters[8].Value = strWhere;
            //return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");


            int topNum = pageSize * currentPage;
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select top " + pageSize + " Card.CreatedOn,");
            strSql.Append(" Card_Movement.CardNumber,Card_Movement.OrgExpiryDate,Card_Movement.NewExpiryDate as CardExpiryDate,Card.CardTypeID,Card.CardGradeID,Card.BatchCardID, ");
            //strSql.Append(" Card_Movement.OrgStatus,Card_Movement.NewStatus as Status,Card_Movement.OpenBal,Card_Movement.Amount,Card_Movement.CloseBal,Card_Movement.OpenBal as CardAmount,Card_Movement.Amount as TotalAmount ");
            strSql.Append(" Card_Movement.OrgStatus,Card_Movement.NewStatus as Status,Card_Movement.OpenBal,Card_Movement.Amount,Card_Movement.CloseBal TotalAmount,Card_Movement.OpenBal as CardAmount,Card_Movement.Amount as OrderAmount,Card_Movement.ClosePoint,Card_Movement.OpenPoint,Card_Movement.Points as OrderPoints ");
            //strSql.Append("Coupon.CouponTypeID,Coupon.CouponIssueDate,Coupon.CouponExpiryDate,Coupon.CouponActiveDate,Coupon.StoreID,");
            //strSql.Append("Coupon.BatchCouponID,Coupon.Status,Coupon.CouponPassword,Coupon.CardNumber,Coupon.CreatedOn,Coupon.UpdatedOn,Coupon.CreatedBy,");
            //strSql.Append("Coupon.UpdatedBy,Ord_CouponAdjust_D.CouponAmount,Coupon.RedeemStoreID");
            strSql.Append(" FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber ");
            if (currentPage > 0)
            {
                strSql.Append(" where Card_Movement.CardNumber not in(select top " + topNum + " Card_Movement.CardNumber FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by " + filedOrder + ")");
            }
            if (strWhere.Trim() != "")
            {
                if (currentPage > 0)
                {
                    strSql.Append(" and " + strWhere);
                }
                else
                {
                    strSql.Append(" where " + strWhere);
                }
            }
            strSql.Append(" order by " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }

        public decimal GetAllDenominationWithCard(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Card.TotalAmount) as Denomination ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["Denomination"];

                if (result == DBNull.Value) return 0.0m;

                decimal denomination = 0.0m;

                return decimal.TryParse(result.ToString(), out denomination) ? denomination : 0.0m;
            }
        }

        public decimal GetAllDenominationWithOrd_CardAdjust_D(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Ord_CardAdjust_D.OrderAmount) as Denomination ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["Denomination"];

                if (result == DBNull.Value) return 0.0m;

                decimal denomination = 0.0m;

                return decimal.TryParse(result.ToString(), out denomination) ? denomination : 0.0m;
            }
        }

        public decimal GetAllDenominationWithCard_Movement(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

//            strSql.Append("select sum(Card_Movement.CloseBal) as Denomination ");
//            strSql.Append(" FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber ");
            strSql.Append("select sum(Ord_CardAdjust_D.ActAmount) as Denomination ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["Denomination"];

                if (result == DBNull.Value) return 0.0m;

                decimal denomination = 0.0m;

                return decimal.TryParse(result.ToString(), out denomination) ? denomination : 0.0m;
            }

        }

        public void GetAllDenominationWithCard_Movement(string strWhere, out decimal openBal, out decimal amount, out decimal closeBal)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Card_Movement.OpenBal) as openBal,sum(Card_Movement.Amount) as amount,sum(Card_Movement.CloseBal) as closeBal ");
            strSql.Append(" FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                //object objOpen = reader["openBal"];
                //object objAmount = reader["amount"];
                //object objClose = reader["closeBal"];
                object objOpen = reader["openBal"];
                object objAmount = reader["closeBal"];
                object objClose = reader["amount"];

                //if (objOpen == DBNull.Value) openBal = 0.0m;
                //if (objAmount == DBNull.Value) amount = 0.0m;
                //if (objClose == DBNull.Value) closeBal = 0.0m;

                decimal denomination = 0.0m;

                openBal = decimal.TryParse(objOpen.ToString(), out denomination) ? denomination : 0.0m;
                amount = decimal.TryParse(objAmount.ToString(), out denomination) ? denomination : 0.0m;
                closeBal = decimal.TryParse(objClose.ToString(), out denomination) ? denomination : 0.0m;
            }

        }

    }
}
