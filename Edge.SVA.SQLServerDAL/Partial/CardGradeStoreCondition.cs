﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;
using System.Collections.Generic;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardGradeStoreCondition
	/// </summary>
	public partial class CardGradeStoreCondition:ICardGradeStoreCondition
	{
        /// <summary>
        /// 批量增加数据
        /// </summary>
        public int AddList(List<Edge.SVA.Model.CardGradeStoreCondition> modelList)
        {
            List<CommandInfo> infoList = new List<CommandInfo>();
            foreach (Edge.SVA.Model.CardGradeStoreCondition item in modelList)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into CardGradeStoreCondition(");
                strSql.Append("CardGradeID,StoreConditionType,ConditionType,ConditionID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
                strSql.Append(" values (");
                strSql.Append("@CardGradeID,@StoreConditionType,@ConditionType,@ConditionID,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
                strSql.Append(";select @@IDENTITY");
                SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
                parameters[0].Value = item.CardGradeID;
                parameters[1].Value = item.StoreConditionType;
                parameters[2].Value = item.ConditionType;
                parameters[3].Value = item.ConditionID;
                parameters[4].Value = item.CreatedOn;
                parameters[5].Value = item.CreatedBy;
                parameters[6].Value = item.UpdatedOn;
                parameters[7].Value = item.UpdatedBy;

                CommandInfo info = new CommandInfo();
                info.CommandText = strSql.ToString();
                info.Parameters = parameters;
                infoList.Add(info);
            }

            return DbHelperSQL.ExecuteSqlTran(infoList);
        }


        /// <summary>
        /// 批量更新数据
        /// </summary>
        public int UpdateList(List<Edge.SVA.Model.CardGradeStoreCondition> insertList, List<Edge.SVA.Model.CardGradeStoreCondition> deleteList)
        {
            List<CommandInfo> infoList = new List<CommandInfo>();

            foreach (Edge.SVA.Model.CardGradeStoreCondition item in deleteList)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("delete from CardGradeStoreCondition ");
                strSql.Append("where CardGradeID=@CardGradeID and ");
                strSql.Append("StoreConditionType=@StoreConditionType and ");
                strSql.Append("ConditionType=@ConditionType and ");
                strSql.Append("ConditionID=@ConditionID ");
                SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime)};
                parameters[0].Value = item.CardGradeID;
                parameters[1].Value = item.StoreConditionType;
                parameters[2].Value = item.ConditionType;
                parameters[3].Value = item.ConditionID;

                CommandInfo info = new CommandInfo();
                info.CommandText = strSql.ToString();
                info.Parameters = parameters;
                infoList.Add(info);
            }
            foreach (Edge.SVA.Model.CardGradeStoreCondition item in insertList)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into CardGradeStoreCondition(");
                strSql.Append("CardGradeID,StoreConditionType,ConditionType,ConditionID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
                strSql.Append(" values (");
                strSql.Append("@CardGradeID,@StoreConditionType,@ConditionType,@ConditionID,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
                strSql.Append(";select @@IDENTITY");
                SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionType", SqlDbType.Int,4),
					new SqlParameter("@ConditionID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
                parameters[0].Value = item.CardGradeID;
                parameters[1].Value = item.StoreConditionType;
                parameters[2].Value = item.ConditionType;
                parameters[3].Value = item.ConditionID;
                parameters[4].Value = item.CreatedOn;
                parameters[5].Value = item.CreatedBy;
                parameters[6].Value = item.UpdatedOn;
                parameters[7].Value = item.UpdatedBy;

                CommandInfo info = new CommandInfo();
                info.CommandText = strSql.ToString();
                info.Parameters = parameters;
                infoList.Add(info);
            }

            return DbHelperSQL.ExecuteSqlTran(infoList);
        }

	}
}

