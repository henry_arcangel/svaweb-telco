﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Edge.DBUtility;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_OrderToSupplier_Card_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_OrderToSupplier_Card_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_OrderToSupplier_Card_D.KeyID";
        }
        public bool DeleteByOrder(string orderSupplierNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_OrderToSupplier_Card_D where OrderSupplierNumber = @OrderSupplierNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@OrderSupplierNumber",SqlDbType.VarChar,64){ Value = orderSupplierNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListGroupByCardType(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT CardTypeID,CardGradeID, sum(OrderQty) OrderQty, max(PackageQty) PackageQty FROM [Ord_OrderToSupplier_Card_D] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" group by CardTypeID,CardGradeID");
            return DbHelperSQL.Query(strSql.ToString());
        }
    }
}
