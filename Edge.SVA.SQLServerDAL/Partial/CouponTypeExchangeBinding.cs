﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CouponTypeExchangeBinding
    {
        public DataSet FetchFields(string fields, string strWhere, params System.Data.SqlClient.SqlParameter[] parameters)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.AppendFormat("select {0} from CouponTypeExchangeBinding ", fields);
            if (!string.IsNullOrEmpty(strWhere))
            {
                sql.AppendFormat("where {0} ",strWhere);
            }
            return DBUtility.DbHelperSQL.Query(sql.ToString(), parameters);
        }
    }
}
