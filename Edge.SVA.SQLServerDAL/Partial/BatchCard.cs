﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using Edge.DBUtility;
using System.Data;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class BatchCard : IBatchCard
    {

            public Dictionary<int, string> GetBatchID(int top)
            {
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("select top {0} BatchCardID,BatchCardCode from BatchCard order by BatchCardID desc", top);

                Dictionary<int, string> batchList = new Dictionary<int, string>();

                using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString()))
                {
                    int batchCouponID = 0;
                    while (reader.Read())
                    {
                        if (int.TryParse(reader["BatchCardID"].ToString(), out batchCouponID))
                        {
                            batchList.Add(batchCouponID, reader["BatchCardCode"].ToString().Trim());
                        }
                    }
                }
                return batchList;

            }

            public Dictionary<int, string> GetBatchID(int top, string partialBatchCode)
            {
                StringBuilder sql = new StringBuilder(150);

                sql.AppendFormat("select  top {0} BatchCardID,BatchCardCode from BatchCard ", top);
                sql.Append("where BatchCardCode like '%'+@BatchCardCode+'%' order by BatchCardID");


                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PartialBatchCode", partialBatchCode) };

                Dictionary<int, string> batchList = new Dictionary<int, string>();
                using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
                {
                    int batchCouponID = 0;
                    while (reader.Read())
                    {
                        if (int.TryParse(reader["BatchCardID"].ToString(), out batchCouponID))
                        {
                            batchList.Add(batchCouponID, reader["BatchCardCode"].ToString());
                        }
                    }
                }
                return batchList;
            }

            public Dictionary<int, string> GetBatchID(int top, int cardGradeID)
            {
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("select top {0} BatchCardID ,BatchCardCode from BatchCard where CardGradeID = @CardGradeID order by BatchCardID desc", top);

                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@CardGradeID", cardGradeID) };

                Dictionary<int, string> batchList = new Dictionary<int, string>();

                using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
                {
                    int batchCouponID = 0;

                    while (reader.Read())
                    {
                        if (int.TryParse(reader["BatchCardID"].ToString(), out batchCouponID))
                        {
                            batchList.Add(batchCouponID, reader["BatchCardCode"].ToString());
                        }
                    }
                }
                return batchList;
            }

            public Dictionary<int, string> GetBatchID(int top, string partialBatchCode, int cardGradeID)
            {

                StringBuilder sql = new StringBuilder(150);

                sql.AppendFormat("select  top {0} BatchCardID,BatchCardCode from BatchCard ", top);
                sql.Append("where BatchCardCode like '%'+@PartialBatchCode+'%' ");
                sql.Append("and CardGradeID = @CardGradeID ");
                sql.Append("order by BatchCardID desc");

                SqlParameter[] parameters = new SqlParameter[] { 
                                            new SqlParameter("@PartialBatchCode", partialBatchCode),
                                            new SqlParameter("@CardGradeID",cardGradeID)
             };

                Dictionary<int, string> batchList = new Dictionary<int, string>();
                using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
                {
                    int batchCouponID = 0;
                    while (reader.Read())
                    {

                        if (int.TryParse(reader["BatchCardID"].ToString(), out batchCouponID))
                        {
                            batchList.Add(batchCouponID, reader["BatchCardCode"].ToString());
                        }
                    }
                }
                return batchList;
            }

            // Add by Alex 2014-06-09 ++
            public bool ExistBatchCode(string batchCardCode)
            {
                StringBuilder sql = new StringBuilder(150);

                sql.Append("select count(1) from BatchCard ");
                sql.Append("where BatchCardCode = @BatchCardCode ");

                return DbHelperSQL.Exists(sql.ToString(), new SqlParameter[] { new SqlParameter("@BatchCardCode", batchCardCode) });
            }
            //add by Alex 2014-06-09 --
    }
}
