﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardTransfer : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardTransfer"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "Ord_CardTransfer.CardTransferNumber";
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_CardTransfer model, int times)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_CardTransfer set ");
            strSql.Append("SourceCardNumber=@SourceCardNumber,");
            strSql.Append("DestCardNumber=@DestCardNumber,");
            strSql.Append("OriginalTxnNo=@OriginalTxnNo,");
            strSql.Append("TxnDate=@TxnDate,");
            strSql.Append("StoreCode=@StoreCode,");
            strSql.Append("ServerCode=@ServerCode,");
            strSql.Append("RegisterCode=@RegisterCode,");
            strSql.Append("ReasonID=@ReasonID,");
            strSql.Append("Note=@Note,");
            strSql.Append("ActAmount=@ActAmount,");
            strSql.Append("ActPoints=@ActPoints,");
            strSql.Append("ActCouponNumbers=@ActCouponNumbers,");
            strSql.Append("ApproveStatus=@ApproveStatus,");
            strSql.Append("ApproveOn=@ApproveOn,");
            strSql.Append("ApproveBy=@ApproveBy,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("CreatedBusDate=@CreatedBusDate,");
            strSql.Append("ApproveBusDate=@ApproveBusDate,");
            strSql.Append("ApprovalCode=@ApprovalCode,");
            strSql.Append("BrandCode=@BrandCode");
            strSql.Append(" where CardTransferNumber=@CardTransferNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@SourceCardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@DestCardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@OriginalTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,512),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@ActAmount", SqlDbType.Money,8),
					new SqlParameter("@ActPoints", SqlDbType.Int,4),
					new SqlParameter("@ActCouponNumbers", SqlDbType.VarChar),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,512),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,512),
					new SqlParameter("@CardTransferNumber", SqlDbType.VarChar,512)};
            parameters[0].Value = model.SourceCardNumber;
            parameters[1].Value = model.DestCardNumber;
            parameters[2].Value = model.OriginalTxnNo;
            parameters[3].Value = model.TxnDate;
            parameters[4].Value = model.StoreCode;
            parameters[5].Value = model.ServerCode;
            parameters[6].Value = model.RegisterCode;
            parameters[7].Value = model.ReasonID;
            parameters[8].Value = model.Note;
            parameters[9].Value = model.ActAmount;
            parameters[10].Value = model.ActPoints;
            parameters[11].Value = model.ActCouponNumbers;
            parameters[12].Value = model.ApproveStatus;
            parameters[13].Value = model.ApproveOn;
            parameters[14].Value = model.ApproveBy;
            parameters[15].Value = model.CreatedOn;
            parameters[16].Value = model.CreatedBy;
            parameters[17].Value = model.UpdatedOn;
            parameters[18].Value = model.UpdatedBy;
            parameters[19].Value = model.CreatedBusDate;
            parameters[20].Value = model.ApproveBusDate;
            parameters[21].Value = model.ApprovalCode;
            parameters[22].Value = model.BrandCode;
            parameters[23].Value = model.CardTransferNumber;

            int rows = DbHelperSQL.ExecuteSqlByTime(strSql.ToString(), times, parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
