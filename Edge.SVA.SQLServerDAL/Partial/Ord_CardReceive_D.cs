﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardReceive_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardReceive_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_CardReceive_D.KeyID";
        }

        public bool DeleteByOrder(string cardReceiveNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_CardReceive_D where CardReceiveNumber = @CardReceiveNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CardReceiveNumber",SqlDbType.VarChar,64){ Value = cardReceiveNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
