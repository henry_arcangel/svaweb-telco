﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardOrderForm_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardOrderForm_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_CardOrderForm_D.KeyID";
        }

        public bool DeleteByOrder(string CardOrderFormNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_CardOrderForm_D where CardOrderFormNumber = @CardOrderFormNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CardOrderFormNumber",SqlDbType.VarChar,64){ Value = CardOrderFormNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
