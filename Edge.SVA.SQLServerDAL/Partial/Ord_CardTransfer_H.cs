﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Edge.DBUtility;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardTransfer_H : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardTransfer_H"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "Ord_CardTransfer_H.CardTransferNumber";
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_CardTransfer_H model,int seconds)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_CardTransfer_H set ");
            strSql.Append("OrderType=@OrderType,");
            strSql.Append("Description=@Description,");
            strSql.Append("ReasonID=@ReasonID,");
            strSql.Append("Remark=@Remark,");
            strSql.Append("CopyCardFlag=@CopyCardFlag,");
            strSql.Append("ApprovalCode=@ApprovalCode,");
            strSql.Append("ApproveStatus=@ApproveStatus,");
            strSql.Append("CreatedBusDate=@CreatedBusDate,");
            strSql.Append("ApproveBusDate=@ApproveBusDate,");
            strSql.Append("ApproveOn=@ApproveOn,");
            strSql.Append("ApproveBy=@ApproveBy,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn");
            strSql.Append(" where CardTransferNumber=@CardTransferNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderType", SqlDbType.Int,4),
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@CopyCardFlag", SqlDbType.Int,4),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CardTransferNumber", SqlDbType.VarChar,64)};
            parameters[0].Value = model.OrderType;
            parameters[1].Value = model.Description;
            parameters[2].Value = model.ReasonID;
            parameters[3].Value = model.Remark;
            parameters[4].Value = model.CopyCardFlag;
            parameters[5].Value = model.ApprovalCode;
            parameters[6].Value = model.ApproveStatus;
            parameters[7].Value = model.CreatedBusDate;
            parameters[8].Value = model.ApproveBusDate;
            parameters[9].Value = model.ApproveOn;
            parameters[10].Value = model.ApproveBy;
            parameters[11].Value = model.CreatedBy;
            parameters[12].Value = model.CreatedOn;
            parameters[13].Value = model.UpdatedBy;
            parameters[14].Value = model.UpdatedOn;
            parameters[15].Value = model.CardTransferNumber;

            int rows = DbHelperSQL.ExecuteSqlByTime(strSql.ToString(),seconds, parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
