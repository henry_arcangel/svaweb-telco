﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.DBUtility;
using Edge.SVA.IDAL;

namespace Edge.SVA.SQLServerDAL
{
    partial class DALCRUD : IDALCRUD
    {
        public System.Data.DataSet Query(string sql)
        {
            return DbHelperSQL.Query(sql);
        }

        public int ExecuteSql(string sql)
        {
            return DbHelperSQL.ExecuteSql(sql);
        }

        public int ExecuteSqlList(List<string> list)
        {
            return DbHelperSQL.ExecuteSqlTran(list);
        }
    }
}
