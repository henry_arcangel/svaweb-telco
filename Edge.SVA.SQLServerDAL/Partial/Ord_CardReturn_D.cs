﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardReturn_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardReturn_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_CardReturn_D.KeyID";
        }

        public bool DeleteByOrder(string CardReturnNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_CardReturn_D where CardReturnNumber = @CardReturnNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CardReturnNumber",SqlDbType.VarChar,64){ Value = CardReturnNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
