﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using System.Data.SqlClient;
using Edge.DBUtility;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Coupon_Movement : ICoupon_Movement
    {
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder,string fields)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            //parameters[0].Value = "Coupon_Movement";
            parameters[0].Value = "view_Coupon_Movement"; //Modified By Robin 2014-11-03 to fix RRG performance issue (no issue transaction in coupon_movement)
            parameters[1].Value = fields;
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        public string GetTxnType(int oprID)
        {
            Dictionary<int,string> txnMap = System.Web.HttpContext.Current.Session["TxnTypeMapping"] as Dictionary<int,string>;
            if (txnMap == null)
            {
                txnMap = new Dictionary<int, string>();
            }
            System.Data.IDataReader reader = null;
            object sync = typeof(Coupon_Movement);
            System.Threading.Monitor.Enter(sync);
            try
            {
                if (txnMap.ContainsKey(oprID)) return txnMap[oprID];

                StringBuilder sql = new StringBuilder();
                sql.Append("select OprIDDesc1 from OperationTable where OprID = @OprID");

                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@OprID",oprID)
                };
                reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                if (reader.Read())
                {
                    txnMap.Add(oprID, reader["OprIDDesc1"].ToString());
                    System.Web.HttpContext.Current.Session["TxnTypeMapping"] = txnMap;
                    return txnMap[oprID];
                }
                return "";
            }
            finally
            {
                System.Threading.Monitor.Exit(sync);
                if (reader != null) reader.Close();
            }
        }
    }
}
