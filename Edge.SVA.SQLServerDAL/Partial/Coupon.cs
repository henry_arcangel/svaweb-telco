﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.DBUtility;
using Edge.SVA.IDAL;
using System.Data.SqlClient;
using System.Data;//Please add references

namespace Edge.SVA.SQLServerDAL
{
    public partial class Coupon : ICoupon
    {
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetListWithBatch(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" Coupon.CouponNumber,Coupon.CouponTypeID,Coupon.CouponIssueDate,Coupon.CouponExpiryDate,Coupon.CouponActiveDate,Coupon.StoreID,Coupon.BatchCouponID,Coupon.Status,Coupon.CouponPassword,Coupon.CardNumber,Coupon.CouponAmount,Coupon.CreatedOn,Coupon.UpdatedOn,Coupon.CreatedBy,Coupon.UpdatedBy,Coupon.RedeemStoreID,BatchCoupon.BatchCouponCode ");
            strSql.Append(" FROM Coupon left join BatchCoupon on BatchCoupon.BatchCouponID=Coupon.BatchCouponID ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithBatch(string strWhere)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select count(*) FROM Coupon left join BatchCoupon on BatchCoupon.BatchCouponID=Coupon.BatchCouponID ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            int topNum = pageSize * currentPage;
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select top " + pageSize + " Coupon.CouponNumber,Coupon.CouponTypeID,Coupon.CouponIssueDate,Coupon.CouponExpiryDate,Coupon.CouponActiveDate,Coupon.StoreID,Coupon.BatchCouponID,Coupon.Status,Coupon.CouponPassword,Coupon.CardNumber,Coupon.CouponAmount,Coupon.CreatedOn,Coupon.UpdatedOn,Coupon.CreatedBy,Coupon.UpdatedBy,Coupon.RedeemStoreID,BatchCoupon.BatchCouponCode");
            strSql.Append(" FROM Coupon left join BatchCoupon on BatchCoupon.BatchCouponID=Coupon.BatchCouponID ");
            if (currentPage > 0)
            {
                strSql.Append(" where Coupon.CouponNumber not in(select top " + topNum + " Coupon.CouponNumber FROM Coupon left join BatchCoupon on BatchCoupon.BatchCouponID=Coupon.BatchCouponID");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by " + filedOrder + ")");
            }
            if (strWhere.Trim() != "")
            {
                if (currentPage > 0)
                {
                    strSql.Append(" and " + strWhere);
                }
                else
                {
                    strSql.Append(" where " + strWhere);
                }
            }
            strSql.Append(" order by " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }

        public bool ExsitCoupon(Model.Ord_ImportCouponUID_H model)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select count(1) from CouponUIDMap ");
            sql.Append("where CouponUID in  ");
            sql.Append("(select Ord_ImportCouponUID_D.CouponUID from Ord_ImportCouponUID_D  where ImportCouponNumber = @ImportCouponNumber) ");

            SqlParameter[] parameters = { new SqlParameter("@ImportCouponNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = model.ImportCouponNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ExsitCoupon(Model.Ord_CouponAdjust_H model)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CouponNumber) from Coupon ");
            sql.Append("where CouponNumber in ");
            sql.Append("(select CouponNumber from Ord_CouponAdjust_D where CouponAdjustNumber = @CouponAdjustNumber) ");

            SqlParameter[] parameters = { new SqlParameter("@CouponAdjustNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = model.CouponAdjustNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ExsitCoupon(string couponNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CouponNumber) from Coupon ");
            sql.Append("where CouponNumber = @CouponNumber ");

            SqlParameter[] parameters = { new SqlParameter("@CouponNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = couponNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ValidCouponStauts(Model.Ord_CouponAdjust_H model, params int[] CouponStatus)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CouponNumber) from Coupon ");
            sql.Append("where CouponNumber in ");
            sql.Append("(select CouponNumber from Ord_CouponAdjust_D where CouponAdjustNumber = @CouponAdjustNumber) ");

            if (CouponStatus.Length <= 0) return false;
            if (CouponStatus.Length == 1)
            {
                sql.AppendFormat("and  [Status] <> {0}", CouponStatus[0]);
            }
            else
            {
                string ids = "";
                for (int i = 0; i < CouponStatus.Length; i++)
                {
                    ids += string.Format("{0},", CouponStatus[i]);
                }

                sql.AppendFormat("and  [Status] not in ({0})", ids.Substring(0, ids.Length - 1));
            }
            SqlParameter[] parameters = { 
                                            new SqlParameter("@CouponAdjustNumber", SqlDbType.VarChar, 512),
                                        };

            parameters[0].Value = model.CouponAdjustNumber;


            return !DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        ///// <summary>
        ///// 获得前几行数据Coupon数据for 批量issue,激活等等
        ///// </summary>
        //public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select ");
        //    if (Top > 0)
        //    {
        //        strSql.Append(" top " + Top.ToString());
        //    }
        //    strSql.Append(" CouponNumber,CouponAmount,CouponExpiryDate,CreatedOn,Status,CouponTypeID,BatchCouponID ");
        //    strSql.Append(" FROM Coupon ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    strSql.Append(" order by " + filedOrder);
        //    return DbHelperSQL.Query(strSql.ToString());
        //}

        /// <summary>
        /// 获得前几行数据Coupon数据for 批量issue,激活等等
        /// </summary>
        public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" Coupon.CouponNumber,Coupon.CouponAmount,Coupon.CouponExpiryDate,Coupon.CreatedOn,Coupon.Status,Coupon.CouponTypeID,Coupon.BatchCouponID,BatchCoupon.BatchCouponCode as BatchCode,CouponUIDMap.CouponUID ");
            strSql.Append(" FROM Coupon ");
            strSql.Append(" left join CouponUIDMap on Coupon.CouponNumber=CouponUIDMap.CouponNumber ");
            strSql.Append(" left join BatchCoupon on Coupon.BatchCouponID=BatchCoupon.BatchCouponID ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields,int times)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Coupon";
            parameters[1].Value = fields;
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 1;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds",times);
        }

        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Coupon";
            parameters[1].Value = fields;
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds", times);
        }

        public Edge.SVA.Model.Coupon GetModelByUID(string couponUID)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select c.Status,c.CouponTypeID,c.BatchCouponID,c.CouponExpiryDate,c.CouponAmount,c.CouponNumber ");
            sql.Append("from Coupon as c inner join CouponUIDMap as m on c.CouponNumber = m.CouponNumber ");
            sql.Append("where m.CouponUID = @CouponUID");

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@CouponUID",SqlDbType.VarChar) { Value = couponUID }
            };
            System.Data.IDataReader reader = null;
            Edge.SVA.Model.Coupon coupon = null;
            try
            {
                reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                int i = 0;
                if (reader != null && reader.Read())
                {
                    coupon = new Model.Coupon();
                    coupon.Status = int.TryParse(reader["Status"].ToString(), out i) ? i : 0;
                    coupon.CouponTypeID = int.TryParse(reader["CouponTypeID"].ToString(), out i) ? i : 0;
                    coupon.BatchCouponID = int.TryParse(reader["BatchCouponID"].ToString(), out i) ? i : 0;
                    coupon.CouponNumber = reader["CouponNumber"].ToString();

                    if (reader["CouponExpiryDate"] != null && !string.IsNullOrEmpty(reader["CouponExpiryDate"].ToString()))
                    {
                        coupon.CouponExpiryDate = DateTime.Parse(reader["CouponExpiryDate"].ToString());
                    }

                    if (reader["CouponAmount"] != null && !string.IsNullOrEmpty(reader["CouponAmount"].ToString()))
                    {
                        coupon.CouponAmount = decimal.Parse(reader["CouponAmount"].ToString());
                    }


                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return coupon;
        }

        public int GetCount(string strWhere, int times)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Coupon ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            object result = DbHelperSQL.GetSingle(sql.ToString(), times);

            if (result != null && result is int) return (int)result;

            return -1;
        }
    }
}
