﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CouponAutoPickingRule_H : BaseDAL
    {
        protected override string TableName
        {
            get { return "CouponAutoPickingRule_H"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "CouponAutoPickingRule_H.CouponAutoPickingRuleCode";
        }
    }
}
