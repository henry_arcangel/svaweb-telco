﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL.Partial
{
    public partial class Ord_CouponReceive_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CouponReceive_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_CouponReceive_D.KeyID";
        }
    }
}
