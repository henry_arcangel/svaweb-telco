﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Card_Movement
    /// </summary>
    public partial class Card_Movement : ICard_Movement
    {

        #region ICard_Movement Members


        /// <summary>
        /// 获得总条数.Card_Move_Movement和Card内联查询
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public int GetCountWithCard(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Card_Movement inner join Card on Card.CardNumber=Card_Movement.CardNumber");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

        /// <summary>
        /// 获得查询分页数据.Card_Move_Movement和Card内联查询
        /// </summary>
        public DataSet GetListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            int topSize = pageSize * currentPage;
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select top " + pageSize + " Card_Movement.KeyID,Card_Movement.OprID,Card_Movement.CardNumber,Card_Movement.RefKeyID, ");
            strSql.Append("Card_Movement.RefReceiveKeyID,Card_Movement.RefTxnNo,Card_Movement.OpenBal,Card_Movement.Amount,Card_Movement.CloseBal, ");
            strSql.Append("Card_Movement.Points,Card_Movement.BusDate,Card_Movement.Txndate,Card_Movement.OrgExpiryDate,Card_Movement.NewExpiryDate, ");
            strSql.Append("Card_Movement.CardCashDetailID,Card_Movement.CardPointDetailID,Card_Movement.TenderID,Card_Movement.Additional,Card_Movement.Remark, ");
            strSql.Append("Card_Movement.SecurityCode,Card_Movement.CreatedOn,Card_Movement.CreatedBy from Card_Movement inner join Card on Card.CardNumber=Card_Movement.CardNumber");
            if (currentPage > 0)
            {
                strSql.Append(" where KeyID not in(select top " + topSize + " KeyID from Card_Movement inner join Card on Card.CardNumber=Card_Movement.CardNumber");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by " + filedOrder + ")");
            }
            if (strWhere.Trim() != "")
            {
                if (currentPage > 0)
                {
                    strSql.Append(" and " + strWhere);
                }
                else
                {
                    strSql.Append(" where " + strWhere);
                }
            }
            strSql.Append(" order by " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion
    }
}