﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Edge.DBUtility;
using System.Data;

namespace Edge.SVA.SQLServerDAL.Partial
{
    public partial class DistributeTemplate
    {		
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int DistributionID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from DistributeTemplate");
            strSql.Append(" where DistributionID=@DistributionID ");
            SqlParameter[] parameters = {
					new SqlParameter("@DistributionCode", SqlDbType.VarChar,512)};
            parameters[0].Value = DistributionID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
    }
}
