﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.SQLServerDAL
{
    public partial class InventoryReplenishRule_H : BaseDAL
    {
        protected override string TableName
        {
            get { return "InventoryReplenishRule_H"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "InventoryReplenishRule_H.InventoryReplenishCode";
        }
    }
}
