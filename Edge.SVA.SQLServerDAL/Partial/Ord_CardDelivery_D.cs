﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardDelivery_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardDelivery_D"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "Ord_CardDelivery_D.KeyID";
        }

        public Dictionary<int, int> GetCardGradeIndex(string CardDeliveryNumber)
        {
            Dictionary<int, int> dic = new Dictionary<int, int>();
            StringBuilder sql = new StringBuilder();
            sql.Append("select CardGradeID from Ord_CardDelivery_D ");
            sql.Append("where CardDeliveryNumber = @CardDeliveryNumber group by CardGradeID order by CardGradeID");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new System.Data.SqlClient.SqlParameter("@CardDeliveryNumber",System.Data.SqlDbType.NVarChar,64) { Value = CardDeliveryNumber }

            };
            System.Data.IDataReader reader = null;
            try
            {
                reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                int i = 0;
                while (reader.Read())
                {
                    dic.Add(int.Parse(reader["CardGradeID"].ToString()), ++i);
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return dic;

        }
    }
}
