﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Edge.DBUtility;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CardPicking_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CardPicking_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "Ord_CardPicking_D.KeyID";
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListGroupByCardGrade(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT CardGradeID,SUM(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY,SUM(OrderAmount) as OrderAmount,Sum(PickAmount) as PickAmount,SUM(OrderPoint) as OrderPoint,Sum(PickPoint) as PickPoint FROM [Ord_CardPicking_D] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" group by CardGradeID");
            return DbHelperSQL.Query(strSql.ToString());
        }

        public Dictionary<int, int> GetCardGradeIndex(string CardPickingNumber)
        {
            Dictionary<int, int> dic = new Dictionary<int, int>();
            StringBuilder sql = new StringBuilder();
            sql.Append("select CardGradeID from Ord_CardPicking_D ");
            sql.Append("where CardPickingNumber = @CardPickingNumber group by CardGradeID order by CardGradeID");
            
            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new System.Data.SqlClient.SqlParameter("@CardPickingNumber",SqlDbType.NVarChar,64) { Value = CardPickingNumber }

            };
            System.Data.IDataReader reader = null;
            try
            {
                reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(),parameters);
                int i = 0;
                while (reader.Read())
                {
                    dic.Add(int.Parse(reader["CardGradeID"].ToString()), ++i);
                }
            }
            finally
            {
                if(reader != null) reader.Close();
            }

            return dic;

        }

        public bool DeleteByOrder(string CardPickingNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_CardPicking_D where CardPickingNumber = @CardPickingNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CardPickingNumber",SqlDbType.VarChar,64){ Value = CardPickingNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
