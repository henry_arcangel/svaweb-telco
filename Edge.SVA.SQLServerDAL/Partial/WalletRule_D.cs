﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Edge.DBUtility;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class WalletRule_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "WalletRule_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "WalletRule_D.KeyID";
        }

        public bool DeleteByCode(string walletRuleCode)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from WalletRule_D where WalletRuleCode = @WalletRuleCode");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@WalletRuleCode",SqlDbType.VarChar,64){ Value = walletRuleCode }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
