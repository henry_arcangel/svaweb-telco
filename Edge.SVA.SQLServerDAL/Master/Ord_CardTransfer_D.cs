﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_CardTransfer_D
	/// </summary>
	public partial class Ord_CardTransfer_D:IOrd_CardTransfer_D
	{
		public Ord_CardTransfer_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Ord_CardTransfer_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_CardTransfer_D");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Ord_CardTransfer_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_CardTransfer_D(");
			strSql.Append("CardTransferNumber,SourceCardTypeID,SourceCardGradeID,SourceCardNumber,DestCardTypeID,DestCardGradeID,DestCardNumber,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,BrandCode,ReasonID,Note,ActAmount,ActPoints,ActCouponNumbers,CopyCardFlag)");
			strSql.Append(" values (");
			strSql.Append("@CardTransferNumber,@SourceCardTypeID,@SourceCardGradeID,@SourceCardNumber,@DestCardTypeID,@DestCardGradeID,@DestCardNumber,@OriginalTxnNo,@TxnDate,@StoreCode,@ServerCode,@RegisterCode,@BrandCode,@ReasonID,@Note,@ActAmount,@ActPoints,@ActCouponNumbers,@CopyCardFlag)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTransferNumber", SqlDbType.VarChar,64),
					new SqlParameter("@SourceCardTypeID", SqlDbType.Int,4),
					new SqlParameter("@SourceCardGradeID", SqlDbType.Int,4),
					new SqlParameter("@SourceCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@DestCardTypeID", SqlDbType.Int,4),
					new SqlParameter("@DestCardGradeID", SqlDbType.Int,4),
					new SqlParameter("@DestCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OriginalTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,64),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,64),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,64),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,64),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@ActAmount", SqlDbType.Money,8),
					new SqlParameter("@ActPoints", SqlDbType.Int,4),
					new SqlParameter("@ActCouponNumbers", SqlDbType.VarChar),
					new SqlParameter("@CopyCardFlag", SqlDbType.Int,4)};
			parameters[0].Value = model.CardTransferNumber;
			parameters[1].Value = model.SourceCardTypeID;
			parameters[2].Value = model.SourceCardGradeID;
			parameters[3].Value = model.SourceCardNumber;
			parameters[4].Value = model.DestCardTypeID;
			parameters[5].Value = model.DestCardGradeID;
			parameters[6].Value = model.DestCardNumber;
			parameters[7].Value = model.OriginalTxnNo;
			parameters[8].Value = model.TxnDate;
			parameters[9].Value = model.StoreCode;
			parameters[10].Value = model.ServerCode;
			parameters[11].Value = model.RegisterCode;
			parameters[12].Value = model.BrandCode;
			parameters[13].Value = model.ReasonID;
			parameters[14].Value = model.Note;
			parameters[15].Value = model.ActAmount;
			parameters[16].Value = model.ActPoints;
			parameters[17].Value = model.ActCouponNumbers;
			parameters[18].Value = model.CopyCardFlag;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CardTransfer_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_CardTransfer_D set ");
			strSql.Append("CardTransferNumber=@CardTransferNumber,");
			strSql.Append("SourceCardTypeID=@SourceCardTypeID,");
			strSql.Append("SourceCardGradeID=@SourceCardGradeID,");
			strSql.Append("SourceCardNumber=@SourceCardNumber,");
			strSql.Append("DestCardTypeID=@DestCardTypeID,");
			strSql.Append("DestCardGradeID=@DestCardGradeID,");
			strSql.Append("DestCardNumber=@DestCardNumber,");
			strSql.Append("OriginalTxnNo=@OriginalTxnNo,");
			strSql.Append("TxnDate=@TxnDate,");
			strSql.Append("StoreCode=@StoreCode,");
			strSql.Append("ServerCode=@ServerCode,");
			strSql.Append("RegisterCode=@RegisterCode,");
			strSql.Append("BrandCode=@BrandCode,");
			strSql.Append("ReasonID=@ReasonID,");
			strSql.Append("Note=@Note,");
			strSql.Append("ActAmount=@ActAmount,");
			strSql.Append("ActPoints=@ActPoints,");
			strSql.Append("ActCouponNumbers=@ActCouponNumbers,");
			strSql.Append("CopyCardFlag=@CopyCardFlag");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTransferNumber", SqlDbType.VarChar,64),
					new SqlParameter("@SourceCardTypeID", SqlDbType.Int,4),
					new SqlParameter("@SourceCardGradeID", SqlDbType.Int,4),
					new SqlParameter("@SourceCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@DestCardTypeID", SqlDbType.Int,4),
					new SqlParameter("@DestCardGradeID", SqlDbType.Int,4),
					new SqlParameter("@DestCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OriginalTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,64),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,64),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,64),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,64),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@ActAmount", SqlDbType.Money,8),
					new SqlParameter("@ActPoints", SqlDbType.Int,4),
					new SqlParameter("@ActCouponNumbers", SqlDbType.VarChar),
					new SqlParameter("@CopyCardFlag", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.CardTransferNumber;
			parameters[1].Value = model.SourceCardTypeID;
			parameters[2].Value = model.SourceCardGradeID;
			parameters[3].Value = model.SourceCardNumber;
			parameters[4].Value = model.DestCardTypeID;
			parameters[5].Value = model.DestCardGradeID;
			parameters[6].Value = model.DestCardNumber;
			parameters[7].Value = model.OriginalTxnNo;
			parameters[8].Value = model.TxnDate;
			parameters[9].Value = model.StoreCode;
			parameters[10].Value = model.ServerCode;
			parameters[11].Value = model.RegisterCode;
			parameters[12].Value = model.BrandCode;
			parameters[13].Value = model.ReasonID;
			parameters[14].Value = model.Note;
			parameters[15].Value = model.ActAmount;
			parameters[16].Value = model.ActPoints;
			parameters[17].Value = model.ActCouponNumbers;
			parameters[18].Value = model.CopyCardFlag;
			parameters[19].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CardTransfer_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CardTransfer_D ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardTransfer_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,CardTransferNumber,SourceCardTypeID,SourceCardGradeID,SourceCardNumber,DestCardTypeID,DestCardGradeID,DestCardNumber,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,BrandCode,ReasonID,Note,ActAmount,ActPoints,ActCouponNumbers,CopyCardFlag from Ord_CardTransfer_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Ord_CardTransfer_D model=new Edge.SVA.Model.Ord_CardTransfer_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardTransferNumber"]!=null && ds.Tables[0].Rows[0]["CardTransferNumber"].ToString()!="")
				{
					model.CardTransferNumber=ds.Tables[0].Rows[0]["CardTransferNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SourceCardTypeID"]!=null && ds.Tables[0].Rows[0]["SourceCardTypeID"].ToString()!="")
				{
					model.SourceCardTypeID=int.Parse(ds.Tables[0].Rows[0]["SourceCardTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SourceCardGradeID"]!=null && ds.Tables[0].Rows[0]["SourceCardGradeID"].ToString()!="")
				{
					model.SourceCardGradeID=int.Parse(ds.Tables[0].Rows[0]["SourceCardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SourceCardNumber"]!=null && ds.Tables[0].Rows[0]["SourceCardNumber"].ToString()!="")
				{
					model.SourceCardNumber=ds.Tables[0].Rows[0]["SourceCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DestCardTypeID"]!=null && ds.Tables[0].Rows[0]["DestCardTypeID"].ToString()!="")
				{
					model.DestCardTypeID=int.Parse(ds.Tables[0].Rows[0]["DestCardTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DestCardGradeID"]!=null && ds.Tables[0].Rows[0]["DestCardGradeID"].ToString()!="")
				{
					model.DestCardGradeID=int.Parse(ds.Tables[0].Rows[0]["DestCardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DestCardNumber"]!=null && ds.Tables[0].Rows[0]["DestCardNumber"].ToString()!="")
				{
					model.DestCardNumber=ds.Tables[0].Rows[0]["DestCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OriginalTxnNo"]!=null && ds.Tables[0].Rows[0]["OriginalTxnNo"].ToString()!="")
				{
					model.OriginalTxnNo=ds.Tables[0].Rows[0]["OriginalTxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TxnDate"]!=null && ds.Tables[0].Rows[0]["TxnDate"].ToString()!="")
				{
					model.TxnDate=DateTime.Parse(ds.Tables[0].Rows[0]["TxnDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StoreCode"]!=null && ds.Tables[0].Rows[0]["StoreCode"].ToString()!="")
				{
					model.StoreCode=ds.Tables[0].Rows[0]["StoreCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ServerCode"]!=null && ds.Tables[0].Rows[0]["ServerCode"].ToString()!="")
				{
					model.ServerCode=ds.Tables[0].Rows[0]["ServerCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RegisterCode"]!=null && ds.Tables[0].Rows[0]["RegisterCode"].ToString()!="")
				{
					model.RegisterCode=ds.Tables[0].Rows[0]["RegisterCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BrandCode"]!=null && ds.Tables[0].Rows[0]["BrandCode"].ToString()!="")
				{
					model.BrandCode=ds.Tables[0].Rows[0]["BrandCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ReasonID"]!=null && ds.Tables[0].Rows[0]["ReasonID"].ToString()!="")
				{
					model.ReasonID=int.Parse(ds.Tables[0].Rows[0]["ReasonID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Note"]!=null && ds.Tables[0].Rows[0]["Note"].ToString()!="")
				{
					model.Note=ds.Tables[0].Rows[0]["Note"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ActAmount"]!=null && ds.Tables[0].Rows[0]["ActAmount"].ToString()!="")
				{
					model.ActAmount=decimal.Parse(ds.Tables[0].Rows[0]["ActAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActPoints"]!=null && ds.Tables[0].Rows[0]["ActPoints"].ToString()!="")
				{
					model.ActPoints=int.Parse(ds.Tables[0].Rows[0]["ActPoints"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActCouponNumbers"]!=null && ds.Tables[0].Rows[0]["ActCouponNumbers"].ToString()!="")
				{
					model.ActCouponNumbers=ds.Tables[0].Rows[0]["ActCouponNumbers"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CopyCardFlag"]!=null && ds.Tables[0].Rows[0]["CopyCardFlag"].ToString()!="")
				{
					model.CopyCardFlag=int.Parse(ds.Tables[0].Rows[0]["CopyCardFlag"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,CardTransferNumber,SourceCardTypeID,SourceCardGradeID,SourceCardNumber,DestCardTypeID,DestCardGradeID,DestCardNumber,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,BrandCode,ReasonID,Note,ActAmount,ActPoints,ActCouponNumbers,CopyCardFlag ");
			strSql.Append(" FROM Ord_CardTransfer_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,CardTransferNumber,SourceCardTypeID,SourceCardGradeID,SourceCardNumber,DestCardTypeID,DestCardGradeID,DestCardNumber,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,BrandCode,ReasonID,Note,ActAmount,ActPoints,ActCouponNumbers,CopyCardFlag ");
			strSql.Append(" FROM Ord_CardTransfer_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_CardTransfer_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_CardTransfer_D T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_CardTransfer_D";
			parameters[1].Value = "KeyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

