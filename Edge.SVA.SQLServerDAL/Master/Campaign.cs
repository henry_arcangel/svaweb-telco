﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Campaign
	/// </summary>
	public partial class Campaign:ICampaign
	{
		public Campaign()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CampaignID", "Campaign"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CampaignCode,int CampaignID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Campaign");
			strSql.Append(" where CampaignCode=@CampaignCode and CampaignID=@CampaignID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512),
					new SqlParameter("@CampaignID", SqlDbType.Int,4)};
			parameters[0].Value = CampaignCode;
			parameters[1].Value = CampaignID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Campaign model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Campaign(");
			strSql.Append("CampaignCode,CampaignName1,CampaignName2,CampaignName3,CampaignDetail1,CampaignDetail3,CampaignDetail2,CampaignType,CampaignPicFile,Status,BrandID,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CampaignCode,@CampaignName1,@CampaignName2,@CampaignName3,@CampaignDetail1,@CampaignDetail3,@CampaignDetail2,@CampaignType,@CampaignPicFile,@Status,@BrandID,@StartDate,@EndDate,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512),
					new SqlParameter("@CampaignName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignDetail1", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail3", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail2", SqlDbType.NVarChar),
					new SqlParameter("@CampaignType", SqlDbType.Int,4),
					new SqlParameter("@CampaignPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CampaignCode;
			parameters[1].Value = model.CampaignName1;
			parameters[2].Value = model.CampaignName2;
			parameters[3].Value = model.CampaignName3;
			parameters[4].Value = model.CampaignDetail1;
			parameters[5].Value = model.CampaignDetail3;
			parameters[6].Value = model.CampaignDetail2;
			parameters[7].Value = model.CampaignType;
			parameters[8].Value = model.CampaignPicFile;
			parameters[9].Value = model.Status;
			parameters[10].Value = model.BrandID;
			parameters[11].Value = model.StartDate;
			parameters[12].Value = model.EndDate;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.CreatedBy;
			parameters[15].Value = model.UpdatedOn;
			parameters[16].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Campaign model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Campaign set ");
			strSql.Append("CampaignName1=@CampaignName1,");
			strSql.Append("CampaignName2=@CampaignName2,");
			strSql.Append("CampaignName3=@CampaignName3,");
			strSql.Append("CampaignDetail1=@CampaignDetail1,");
			strSql.Append("CampaignDetail3=@CampaignDetail3,");
			strSql.Append("CampaignDetail2=@CampaignDetail2,");
			strSql.Append("CampaignType=@CampaignType,");
			strSql.Append("CampaignPicFile=@CampaignPicFile,");
			strSql.Append("Status=@Status,");
			strSql.Append("BrandID=@BrandID,");
			strSql.Append("StartDate=@StartDate,");
			strSql.Append("EndDate=@EndDate,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("CampaignCode=@CampaignCode");
			strSql.Append(" where CampaignID=@CampaignID");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignDetail1", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail3", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail2", SqlDbType.NVarChar),
					new SqlParameter("@CampaignType", SqlDbType.Int,4),
					new SqlParameter("@CampaignPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.CampaignName1;
			parameters[1].Value = model.CampaignName2;
			parameters[2].Value = model.CampaignName3;
			parameters[3].Value = model.CampaignDetail1;
			parameters[4].Value = model.CampaignDetail3;
			parameters[5].Value = model.CampaignDetail2;
			parameters[6].Value = model.CampaignType;
			parameters[7].Value = model.CampaignPicFile;
			parameters[8].Value = model.Status;
			parameters[9].Value = model.BrandID;
			parameters[10].Value = model.StartDate;
			parameters[11].Value = model.EndDate;
			parameters[12].Value = model.CreatedOn;
			parameters[13].Value = model.CreatedBy;
			parameters[14].Value = model.UpdatedOn;
			parameters[15].Value = model.UpdatedBy;
			parameters[16].Value = model.CampaignID;
			parameters[17].Value = model.CampaignCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CampaignID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Campaign ");
			strSql.Append(" where CampaignID=@CampaignID");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4)
};
			parameters[0].Value = CampaignID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CampaignCode,int CampaignID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Campaign ");
			strSql.Append(" where CampaignCode=@CampaignCode and CampaignID=@CampaignID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512),
					new SqlParameter("@CampaignID", SqlDbType.Int,4)};
			parameters[0].Value = CampaignCode;
			parameters[1].Value = CampaignID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CampaignIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Campaign ");
			strSql.Append(" where CampaignID in ("+CampaignIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Campaign GetModel(int CampaignID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CampaignID,CampaignCode,CampaignName1,CampaignName2,CampaignName3,CampaignDetail1,CampaignDetail3,CampaignDetail2,CampaignType,CampaignPicFile,Status,BrandID,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Campaign ");
			strSql.Append(" where CampaignID=@CampaignID");
			SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4)
};
			parameters[0].Value = CampaignID;

			Edge.SVA.Model.Campaign model=new Edge.SVA.Model.Campaign();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CampaignID"]!=null && ds.Tables[0].Rows[0]["CampaignID"].ToString()!="")
				{
					model.CampaignID=int.Parse(ds.Tables[0].Rows[0]["CampaignID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CampaignCode"]!=null && ds.Tables[0].Rows[0]["CampaignCode"].ToString()!="")
				{
					model.CampaignCode=ds.Tables[0].Rows[0]["CampaignCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignName1"]!=null && ds.Tables[0].Rows[0]["CampaignName1"].ToString()!="")
				{
					model.CampaignName1=ds.Tables[0].Rows[0]["CampaignName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignName2"]!=null && ds.Tables[0].Rows[0]["CampaignName2"].ToString()!="")
				{
					model.CampaignName2=ds.Tables[0].Rows[0]["CampaignName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignName3"]!=null && ds.Tables[0].Rows[0]["CampaignName3"].ToString()!="")
				{
					model.CampaignName3=ds.Tables[0].Rows[0]["CampaignName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignDetail1"]!=null && ds.Tables[0].Rows[0]["CampaignDetail1"].ToString()!="")
				{
					model.CampaignDetail1=ds.Tables[0].Rows[0]["CampaignDetail1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignDetail3"]!=null && ds.Tables[0].Rows[0]["CampaignDetail3"].ToString()!="")
				{
					model.CampaignDetail3=ds.Tables[0].Rows[0]["CampaignDetail3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignDetail2"]!=null && ds.Tables[0].Rows[0]["CampaignDetail2"].ToString()!="")
				{
					model.CampaignDetail2=ds.Tables[0].Rows[0]["CampaignDetail2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CampaignType"]!=null && ds.Tables[0].Rows[0]["CampaignType"].ToString()!="")
				{
					model.CampaignType=int.Parse(ds.Tables[0].Rows[0]["CampaignType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CampaignPicFile"]!=null && ds.Tables[0].Rows[0]["CampaignPicFile"].ToString()!="")
				{
					model.CampaignPicFile=ds.Tables[0].Rows[0]["CampaignPicFile"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BrandID"]!=null && ds.Tables[0].Rows[0]["BrandID"].ToString()!="")
				{
					model.BrandID=int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartDate"]!=null && ds.Tables[0].Rows[0]["StartDate"].ToString()!="")
				{
					model.StartDate=DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndDate"]!=null && ds.Tables[0].Rows[0]["EndDate"].ToString()!="")
				{
					model.EndDate=DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CampaignID,CampaignCode,CampaignName1,CampaignName2,CampaignName3,CampaignDetail1,CampaignDetail3,CampaignDetail2,CampaignType,CampaignPicFile,Status,BrandID,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Campaign ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CampaignID,CampaignCode,CampaignName1,CampaignName2,CampaignName3,CampaignDetail1,CampaignDetail3,CampaignDetail2,CampaignType,CampaignPicFile,Status,BrandID,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Campaign ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            int OrderType = 0;
            string OrderField = filedOrder;
            if (filedOrder.ToLower().EndsWith(" desc"))
            {
                OrderType = 1;
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" desc"));
            }
            else if (filedOrder.ToLower().EndsWith(" asc"))
            {
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" asc"));
            }
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Campaign";
            parameters[1].Value = "*";
            parameters[2].Value = OrderField;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = OrderType;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Campaign ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

