﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:MemberMessageAccount
    /// </summary>
    public partial class MemberMessageAccount : IMemberMessageAccount
    {
        public MemberMessageAccount()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("MessageAccountID", "MemberMessageAccount");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int MessageAccountID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from MemberMessageAccount");
            strSql.Append(" where MessageAccountID=@MessageAccountID");
            SqlParameter[] parameters = {
					new SqlParameter("@MessageAccountID", SqlDbType.Int,4)
			};
            parameters[0].Value = MessageAccountID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.MemberMessageAccount model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MemberMessageAccount(");
            strSql.Append("MemberID,MessageServiceTypeID,AccountNumber,Status,Note,IsPrefer,TokenUID,TokenStr,TokenUpdateDate,PromotionFlag,VerifyFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@MemberID,@MessageServiceTypeID,@AccountNumber,@Status,@Note,@IsPrefer,@TokenUID,@TokenStr,@TokenUpdateDate,@PromotionFlag,@VerifyFlag,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@MessageServiceTypeID", SqlDbType.Int,4),
					new SqlParameter("@AccountNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@IsPrefer", SqlDbType.Int,4),
					new SqlParameter("@TokenUID", SqlDbType.VarChar,64),
					new SqlParameter("@TokenStr", SqlDbType.VarChar,512),
					new SqlParameter("@TokenUpdateDate", SqlDbType.DateTime),
					new SqlParameter("@PromotionFlag", SqlDbType.Int,4),
					new SqlParameter("@VerifyFlag", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.MemberID;
            parameters[1].Value = model.MessageServiceTypeID;
            parameters[2].Value = model.AccountNumber;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.Note;
            parameters[5].Value = model.IsPrefer;
            parameters[6].Value = model.TokenUID;
            parameters[7].Value = model.TokenStr;
            parameters[8].Value = model.TokenUpdateDate;
            parameters[9].Value = model.PromotionFlag;
            parameters[10].Value = model.VerifyFlag;
            parameters[11].Value = model.CreatedOn;
            parameters[12].Value = model.CreatedBy;
            parameters[13].Value = model.UpdatedOn;
            parameters[14].Value = model.UpdatedBy;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.MemberMessageAccount model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MemberMessageAccount set ");
            strSql.Append("MemberID=@MemberID,");
            strSql.Append("MessageServiceTypeID=@MessageServiceTypeID,");
            strSql.Append("AccountNumber=@AccountNumber,");
            strSql.Append("Status=@Status,");
            strSql.Append("Note=@Note,");
            strSql.Append("IsPrefer=@IsPrefer,");
            strSql.Append("TokenUID=@TokenUID,");
            strSql.Append("TokenStr=@TokenStr,");
            strSql.Append("TokenUpdateDate=@TokenUpdateDate,");
            strSql.Append("PromotionFlag=@PromotionFlag,");
            strSql.Append("VerifyFlag=@VerifyFlag,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where MessageAccountID=@MessageAccountID");
            SqlParameter[] parameters = {
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@MessageServiceTypeID", SqlDbType.Int,4),
					new SqlParameter("@AccountNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@IsPrefer", SqlDbType.Int,4),
					new SqlParameter("@TokenUID", SqlDbType.VarChar,64),
					new SqlParameter("@TokenStr", SqlDbType.VarChar,512),
					new SqlParameter("@TokenUpdateDate", SqlDbType.DateTime),
					new SqlParameter("@PromotionFlag", SqlDbType.Int,4),
					new SqlParameter("@VerifyFlag", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@MessageAccountID", SqlDbType.Int,4)};
            parameters[0].Value = model.MemberID;
            parameters[1].Value = model.MessageServiceTypeID;
            parameters[2].Value = model.AccountNumber;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.Note;
            parameters[5].Value = model.IsPrefer;
            parameters[6].Value = model.TokenUID;
            parameters[7].Value = model.TokenStr;
            parameters[8].Value = model.TokenUpdateDate;
            parameters[9].Value = model.PromotionFlag;
            parameters[10].Value = model.VerifyFlag;
            parameters[11].Value = model.CreatedOn;
            parameters[12].Value = model.CreatedBy;
            parameters[13].Value = model.UpdatedOn;
            parameters[14].Value = model.UpdatedBy;
            parameters[15].Value = model.MessageAccountID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int MessageAccountID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from MemberMessageAccount ");
            strSql.Append(" where MessageAccountID=@MessageAccountID");
            SqlParameter[] parameters = {
					new SqlParameter("@MessageAccountID", SqlDbType.Int,4)
			};
            parameters[0].Value = MessageAccountID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string MessageAccountIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from MemberMessageAccount ");
            strSql.Append(" where MessageAccountID in (" + MessageAccountIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.MemberMessageAccount GetModel(int MessageAccountID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 MessageAccountID,MemberID,MessageServiceTypeID,AccountNumber,Status,Note,IsPrefer,TokenUID,TokenStr,TokenUpdateDate,PromotionFlag,VerifyFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from MemberMessageAccount ");
            strSql.Append(" where MessageAccountID=@MessageAccountID");
            SqlParameter[] parameters = {
					new SqlParameter("@MessageAccountID", SqlDbType.Int,4)
			};
            parameters[0].Value = MessageAccountID;

            Edge.SVA.Model.MemberMessageAccount model = new Edge.SVA.Model.MemberMessageAccount();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.MemberMessageAccount DataRowToModel(DataRow row)
        {
            Edge.SVA.Model.MemberMessageAccount model = new Edge.SVA.Model.MemberMessageAccount();
            if (row != null)
            {
                if (row["MessageAccountID"] != null && row["MessageAccountID"].ToString() != "")
                {
                    model.MessageAccountID = int.Parse(row["MessageAccountID"].ToString());
                }
                if (row["MemberID"] != null && row["MemberID"].ToString() != "")
                {
                    model.MemberID = int.Parse(row["MemberID"].ToString());
                }
                if (row["MessageServiceTypeID"] != null && row["MessageServiceTypeID"].ToString() != "")
                {
                    model.MessageServiceTypeID = int.Parse(row["MessageServiceTypeID"].ToString());
                }
                if (row["AccountNumber"] != null)
                {
                    model.AccountNumber = row["AccountNumber"].ToString();
                }
                if (row["Status"] != null && row["Status"].ToString() != "")
                {
                    model.Status = int.Parse(row["Status"].ToString());
                }
                if (row["Note"] != null)
                {
                    model.Note = row["Note"].ToString();
                }
                if (row["IsPrefer"] != null && row["IsPrefer"].ToString() != "")
                {
                    model.IsPrefer = int.Parse(row["IsPrefer"].ToString());
                }
                if (row["TokenUID"] != null)
                {
                    model.TokenUID = row["TokenUID"].ToString();
                }
                if (row["TokenStr"] != null)
                {
                    model.TokenStr = row["TokenStr"].ToString();
                }
                if (row["TokenUpdateDate"] != null && row["TokenUpdateDate"].ToString() != "")
                {
                    model.TokenUpdateDate = DateTime.Parse(row["TokenUpdateDate"].ToString());
                }
                if (row["PromotionFlag"] != null && row["PromotionFlag"].ToString() != "")
                {
                    model.PromotionFlag = int.Parse(row["PromotionFlag"].ToString());
                }
                if (row["VerifyFlag"] != null && row["VerifyFlag"].ToString() != "")
                {
                    model.VerifyFlag = int.Parse(row["VerifyFlag"].ToString());
                }
                if (row["CreatedOn"] != null && row["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(row["CreatedOn"].ToString());
                }
                if (row["CreatedBy"] != null && row["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(row["CreatedBy"].ToString());
                }
                if (row["UpdatedOn"] != null && row["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(row["UpdatedOn"].ToString());
                }
                if (row["UpdatedBy"] != null && row["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(row["UpdatedBy"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select MessageAccountID,MemberID,MessageServiceTypeID,AccountNumber,Status,Note,IsPrefer,TokenUID,TokenStr,TokenUpdateDate,PromotionFlag,VerifyFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM MemberMessageAccount ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" MessageAccountID,MemberID,MessageServiceTypeID,AccountNumber,Status,Note,IsPrefer,TokenUID,TokenStr,TokenUpdateDate,PromotionFlag,VerifyFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM MemberMessageAccount ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM MemberMessageAccount ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.MessageAccountID desc");
            }
            strSql.Append(")AS Row, T.*  from MemberMessageAccount T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "MemberMessageAccount";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from MemberMessageAccount ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "MemberMessageAccount";
            parameters[1].Value = "MessageAccountID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

