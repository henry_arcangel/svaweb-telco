﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Member
	/// </summary>
	public partial class Member:IMember
	{
		public Member()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("MemberID", "Member"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string MemberRegisterMobile,int MemberID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Member");
			strSql.Append(" where MemberRegisterMobile=@MemberRegisterMobile and MemberID=@MemberID ");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberRegisterMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberID", SqlDbType.Int,4)			};
			parameters[0].Value = MemberRegisterMobile;
			parameters[1].Value = MemberID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Member model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Member(");
			strSql.Append("MemberPassword,MemberIdentityType,MemberIdentityRef,MemberRegisterMobile,MemberMobilePhone,MemberEmail,MemberAppellation,MemberEngFamilyName,MemberEngGivenName,MemberChiFamilyName,MemberChiGivenName,MemberSex,MemberDateOfBirth,MemberDayOfBirth,MemberMonthOfBirth,MemberYearOfBirth,MemberMarital,HomeAddress,HomeTelNum,HomeFaxNum,OfficeAddress,OfficeTelNum,OfficeFaxNum,EducationID,ProfessionID,MemberPosition,NationID,CompanyDesc,SpRemark,OtherContact,Hobbies,Status,MemberDefLanguage,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,CountryCode,NickName,MemberPictureFile,MemberPicture,ReadReguFlag,ReferCardNumber,ViewHistoryTranDays,ResetPWDCount,AcceptPhoneAdvertising,ReceiveAllAdvertising,TransPersonalInfo)");
			strSql.Append(" values (");
			strSql.Append("@MemberPassword,@MemberIdentityType,@MemberIdentityRef,@MemberRegisterMobile,@MemberMobilePhone,@MemberEmail,@MemberAppellation,@MemberEngFamilyName,@MemberEngGivenName,@MemberChiFamilyName,@MemberChiGivenName,@MemberSex,@MemberDateOfBirth,@MemberDayOfBirth,@MemberMonthOfBirth,@MemberYearOfBirth,@MemberMarital,@HomeAddress,@HomeTelNum,@HomeFaxNum,@OfficeAddress,@OfficeTelNum,@OfficeFaxNum,@EducationID,@ProfessionID,@MemberPosition,@NationID,@CompanyDesc,@SpRemark,@OtherContact,@Hobbies,@Status,@MemberDefLanguage,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@PasswordExpiryDate,@PWDExpiryPromptDays,@CountryCode,@NickName,@MemberPictureFile,@MemberPicture,@ReadReguFlag,@ReferCardNumber,@ViewHistoryTranDays,@ResetPWDCount,@AcceptPhoneAdvertising,@ReceiveAllAdvertising,@TransPersonalInfo)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberPassword", SqlDbType.VarChar,512),
					new SqlParameter("@MemberIdentityType", SqlDbType.Int,4),
					new SqlParameter("@MemberIdentityRef", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberRegisterMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberMobilePhone", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberAppellation", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEngFamilyName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEngGivenName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberChiFamilyName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberChiGivenName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberSex", SqlDbType.Int,4),
					new SqlParameter("@MemberDateOfBirth", SqlDbType.DateTime),
					new SqlParameter("@MemberDayOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberMonthOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberYearOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberMarital", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@HomeTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@HomeFaxNum", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeFaxNum", SqlDbType.NVarChar,512),
					new SqlParameter("@EducationID", SqlDbType.Int,4),
					new SqlParameter("@ProfessionID", SqlDbType.Int,4),
					new SqlParameter("@MemberPosition", SqlDbType.NVarChar,512),
					new SqlParameter("@NationID", SqlDbType.Int,4),
					new SqlParameter("@CompanyDesc", SqlDbType.NVarChar,512),
					new SqlParameter("@SpRemark", SqlDbType.NVarChar,512),
					new SqlParameter("@OtherContact", SqlDbType.NVarChar,512),
					new SqlParameter("@Hobbies", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MemberDefLanguage", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@PWDExpiryPromptDays", SqlDbType.Int,4),
					new SqlParameter("@CountryCode", SqlDbType.VarChar,512),
					new SqlParameter("@NickName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberPictureFile", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberPicture", SqlDbType.VarBinary,-1),
					new SqlParameter("@ReadReguFlag", SqlDbType.Int,4),
					new SqlParameter("@ReferCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@ViewHistoryTranDays", SqlDbType.Int,4),
					new SqlParameter("@ResetPWDCount", SqlDbType.Int,4),
					new SqlParameter("@AcceptPhoneAdvertising", SqlDbType.Int,4),
					new SqlParameter("@ReceiveAllAdvertising", SqlDbType.Int,4),
					new SqlParameter("@TransPersonalInfo", SqlDbType.Int,4)};
			parameters[0].Value = model.MemberPassword;
			parameters[1].Value = model.MemberIdentityType;
			parameters[2].Value = model.MemberIdentityRef;
			parameters[3].Value = model.MemberRegisterMobile;
			parameters[4].Value = model.MemberMobilePhone;
			parameters[5].Value = model.MemberEmail;
			parameters[6].Value = model.MemberAppellation;
			parameters[7].Value = model.MemberEngFamilyName;
			parameters[8].Value = model.MemberEngGivenName;
			parameters[9].Value = model.MemberChiFamilyName;
			parameters[10].Value = model.MemberChiGivenName;
			parameters[11].Value = model.MemberSex;
			parameters[12].Value = model.MemberDateOfBirth;
			parameters[13].Value = model.MemberDayOfBirth;
			parameters[14].Value = model.MemberMonthOfBirth;
			parameters[15].Value = model.MemberYearOfBirth;
			parameters[16].Value = model.MemberMarital;
			parameters[17].Value = model.HomeAddress;
			parameters[18].Value = model.HomeTelNum;
			parameters[19].Value = model.HomeFaxNum;
			parameters[20].Value = model.OfficeAddress;
			parameters[21].Value = model.OfficeTelNum;
			parameters[22].Value = model.OfficeFaxNum;
			parameters[23].Value = model.EducationID;
			parameters[24].Value = model.ProfessionID;
			parameters[25].Value = model.MemberPosition;
			parameters[26].Value = model.NationID;
			parameters[27].Value = model.CompanyDesc;
			parameters[28].Value = model.SpRemark;
			parameters[29].Value = model.OtherContact;
			parameters[30].Value = model.Hobbies;
			parameters[31].Value = model.Status;
			parameters[32].Value = model.MemberDefLanguage;
			parameters[33].Value = model.CreatedOn;
			parameters[34].Value = model.UpdatedOn;
			parameters[35].Value = model.CreatedBy;
			parameters[36].Value = model.UpdatedBy;
			parameters[37].Value = model.PasswordExpiryDate;
			parameters[38].Value = model.PWDExpiryPromptDays;
			parameters[39].Value = model.CountryCode;
			parameters[40].Value = model.NickName;
			parameters[41].Value = model.MemberPictureFile;
			parameters[42].Value = model.MemberPicture;
			parameters[43].Value = model.ReadReguFlag;
			parameters[44].Value = model.ReferCardNumber;
			parameters[45].Value = model.ViewHistoryTranDays;
			parameters[46].Value = model.ResetPWDCount;
			parameters[47].Value = model.AcceptPhoneAdvertising;
			parameters[48].Value = model.ReceiveAllAdvertising;
			parameters[49].Value = model.TransPersonalInfo;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Member model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Member set ");
			strSql.Append("MemberPassword=@MemberPassword,");
			strSql.Append("MemberIdentityType=@MemberIdentityType,");
			strSql.Append("MemberIdentityRef=@MemberIdentityRef,");
			strSql.Append("MemberMobilePhone=@MemberMobilePhone,");
			strSql.Append("MemberEmail=@MemberEmail,");
			strSql.Append("MemberAppellation=@MemberAppellation,");
			strSql.Append("MemberEngFamilyName=@MemberEngFamilyName,");
			strSql.Append("MemberEngGivenName=@MemberEngGivenName,");
			strSql.Append("MemberChiFamilyName=@MemberChiFamilyName,");
			strSql.Append("MemberChiGivenName=@MemberChiGivenName,");
			strSql.Append("MemberSex=@MemberSex,");
			strSql.Append("MemberDateOfBirth=@MemberDateOfBirth,");
			strSql.Append("MemberDayOfBirth=@MemberDayOfBirth,");
			strSql.Append("MemberMonthOfBirth=@MemberMonthOfBirth,");
			strSql.Append("MemberYearOfBirth=@MemberYearOfBirth,");
			strSql.Append("MemberMarital=@MemberMarital,");
			strSql.Append("HomeAddress=@HomeAddress,");
			strSql.Append("HomeTelNum=@HomeTelNum,");
			strSql.Append("HomeFaxNum=@HomeFaxNum,");
			strSql.Append("OfficeAddress=@OfficeAddress,");
			strSql.Append("OfficeTelNum=@OfficeTelNum,");
			strSql.Append("OfficeFaxNum=@OfficeFaxNum,");
			strSql.Append("EducationID=@EducationID,");
			strSql.Append("ProfessionID=@ProfessionID,");
			strSql.Append("MemberPosition=@MemberPosition,");
			strSql.Append("NationID=@NationID,");
			strSql.Append("CompanyDesc=@CompanyDesc,");
			strSql.Append("SpRemark=@SpRemark,");
			strSql.Append("OtherContact=@OtherContact,");
			strSql.Append("Hobbies=@Hobbies,");
			strSql.Append("Status=@Status,");
			strSql.Append("MemberDefLanguage=@MemberDefLanguage,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("PasswordExpiryDate=@PasswordExpiryDate,");
			strSql.Append("PWDExpiryPromptDays=@PWDExpiryPromptDays,");
			strSql.Append("CountryCode=@CountryCode,");
			strSql.Append("NickName=@NickName,");
			strSql.Append("MemberPictureFile=@MemberPictureFile,");
			strSql.Append("MemberPicture=@MemberPicture,");
			strSql.Append("ReadReguFlag=@ReadReguFlag,");
			strSql.Append("ReferCardNumber=@ReferCardNumber,");
			strSql.Append("ViewHistoryTranDays=@ViewHistoryTranDays,");
			strSql.Append("ResetPWDCount=@ResetPWDCount,");
			strSql.Append("AcceptPhoneAdvertising=@AcceptPhoneAdvertising,");
			strSql.Append("ReceiveAllAdvertising=@ReceiveAllAdvertising,");
			strSql.Append("TransPersonalInfo=@TransPersonalInfo");
			strSql.Append(" where MemberID=@MemberID");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberPassword", SqlDbType.VarChar,512),
					new SqlParameter("@MemberIdentityType", SqlDbType.Int,4),
					new SqlParameter("@MemberIdentityRef", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberMobilePhone", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberAppellation", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEngFamilyName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberEngGivenName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberChiFamilyName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberChiGivenName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberSex", SqlDbType.Int,4),
					new SqlParameter("@MemberDateOfBirth", SqlDbType.DateTime),
					new SqlParameter("@MemberDayOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberMonthOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberYearOfBirth", SqlDbType.Int,4),
					new SqlParameter("@MemberMarital", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@HomeTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@HomeFaxNum", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@OfficeFaxNum", SqlDbType.NVarChar,512),
					new SqlParameter("@EducationID", SqlDbType.Int,4),
					new SqlParameter("@ProfessionID", SqlDbType.Int,4),
					new SqlParameter("@MemberPosition", SqlDbType.NVarChar,512),
					new SqlParameter("@NationID", SqlDbType.Int,4),
					new SqlParameter("@CompanyDesc", SqlDbType.NVarChar,512),
					new SqlParameter("@SpRemark", SqlDbType.NVarChar,512),
					new SqlParameter("@OtherContact", SqlDbType.NVarChar,512),
					new SqlParameter("@Hobbies", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MemberDefLanguage", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@PWDExpiryPromptDays", SqlDbType.Int,4),
					new SqlParameter("@CountryCode", SqlDbType.VarChar,512),
					new SqlParameter("@NickName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberPictureFile", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberPicture", SqlDbType.VarBinary,-1),
					new SqlParameter("@ReadReguFlag", SqlDbType.Int,4),
					new SqlParameter("@ReferCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@ViewHistoryTranDays", SqlDbType.Int,4),
					new SqlParameter("@ResetPWDCount", SqlDbType.Int,4),
					new SqlParameter("@AcceptPhoneAdvertising", SqlDbType.Int,4),
					new SqlParameter("@ReceiveAllAdvertising", SqlDbType.Int,4),
					new SqlParameter("@TransPersonalInfo", SqlDbType.Int,4),
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@MemberRegisterMobile", SqlDbType.NVarChar,512)};
			parameters[0].Value = model.MemberPassword;
			parameters[1].Value = model.MemberIdentityType;
			parameters[2].Value = model.MemberIdentityRef;
			parameters[3].Value = model.MemberMobilePhone;
			parameters[4].Value = model.MemberEmail;
			parameters[5].Value = model.MemberAppellation;
			parameters[6].Value = model.MemberEngFamilyName;
			parameters[7].Value = model.MemberEngGivenName;
			parameters[8].Value = model.MemberChiFamilyName;
			parameters[9].Value = model.MemberChiGivenName;
			parameters[10].Value = model.MemberSex;
			parameters[11].Value = model.MemberDateOfBirth;
			parameters[12].Value = model.MemberDayOfBirth;
			parameters[13].Value = model.MemberMonthOfBirth;
			parameters[14].Value = model.MemberYearOfBirth;
			parameters[15].Value = model.MemberMarital;
			parameters[16].Value = model.HomeAddress;
			parameters[17].Value = model.HomeTelNum;
			parameters[18].Value = model.HomeFaxNum;
			parameters[19].Value = model.OfficeAddress;
			parameters[20].Value = model.OfficeTelNum;
			parameters[21].Value = model.OfficeFaxNum;
			parameters[22].Value = model.EducationID;
			parameters[23].Value = model.ProfessionID;
			parameters[24].Value = model.MemberPosition;
			parameters[25].Value = model.NationID;
			parameters[26].Value = model.CompanyDesc;
			parameters[27].Value = model.SpRemark;
			parameters[28].Value = model.OtherContact;
			parameters[29].Value = model.Hobbies;
			parameters[30].Value = model.Status;
			parameters[31].Value = model.MemberDefLanguage;
			parameters[32].Value = model.CreatedOn;
			parameters[33].Value = model.UpdatedOn;
			parameters[34].Value = model.CreatedBy;
			parameters[35].Value = model.UpdatedBy;
			parameters[36].Value = model.PasswordExpiryDate;
			parameters[37].Value = model.PWDExpiryPromptDays;
			parameters[38].Value = model.CountryCode;
			parameters[39].Value = model.NickName;
			parameters[40].Value = model.MemberPictureFile;
			parameters[41].Value = model.MemberPicture;
			parameters[42].Value = model.ReadReguFlag;
			parameters[43].Value = model.ReferCardNumber;
			parameters[44].Value = model.ViewHistoryTranDays;
			parameters[45].Value = model.ResetPWDCount;
			parameters[46].Value = model.AcceptPhoneAdvertising;
			parameters[47].Value = model.ReceiveAllAdvertising;
			parameters[48].Value = model.TransPersonalInfo;
			parameters[49].Value = model.MemberID;
			parameters[50].Value = model.MemberRegisterMobile;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int MemberID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Member ");
			strSql.Append(" where MemberID=@MemberID");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberID", SqlDbType.Int,4)
			};
			parameters[0].Value = MemberID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string MemberRegisterMobile,int MemberID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Member ");
			strSql.Append(" where MemberRegisterMobile=@MemberRegisterMobile and MemberID=@MemberID ");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberRegisterMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberID", SqlDbType.Int,4)			};
			parameters[0].Value = MemberRegisterMobile;
			parameters[1].Value = MemberID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string MemberIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Member ");
			strSql.Append(" where MemberID in ("+MemberIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Member GetModel(int MemberID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 MemberID,MemberPassword,MemberIdentityType,MemberIdentityRef,MemberRegisterMobile,MemberMobilePhone,MemberEmail,MemberAppellation,MemberEngFamilyName,MemberEngGivenName,MemberChiFamilyName,MemberChiGivenName,MemberSex,MemberDateOfBirth,MemberDayOfBirth,MemberMonthOfBirth,MemberYearOfBirth,MemberMarital,HomeAddress,HomeTelNum,HomeFaxNum,OfficeAddress,OfficeTelNum,OfficeFaxNum,EducationID,ProfessionID,MemberPosition,NationID,CompanyDesc,SpRemark,OtherContact,Hobbies,Status,MemberDefLanguage,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,CountryCode,NickName,MemberPictureFile,MemberPicture,ReadReguFlag,ReferCardNumber,ViewHistoryTranDays,ResetPWDCount,AcceptPhoneAdvertising,ReceiveAllAdvertising,TransPersonalInfo from Member ");
			strSql.Append(" where MemberID=@MemberID");
			SqlParameter[] parameters = {
					new SqlParameter("@MemberID", SqlDbType.Int,4)
			};
			parameters[0].Value = MemberID;

			Edge.SVA.Model.Member model=new Edge.SVA.Model.Member();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Member DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.Member model=new Edge.SVA.Model.Member();
			if (row != null)
			{
				if(row["MemberID"]!=null && row["MemberID"].ToString()!="")
				{
					model.MemberID=int.Parse(row["MemberID"].ToString());
				}
				if(row["MemberPassword"]!=null)
				{
					model.MemberPassword=row["MemberPassword"].ToString();
				}
				if(row["MemberIdentityType"]!=null && row["MemberIdentityType"].ToString()!="")
				{
					model.MemberIdentityType=int.Parse(row["MemberIdentityType"].ToString());
				}
				if(row["MemberIdentityRef"]!=null)
				{
					model.MemberIdentityRef=row["MemberIdentityRef"].ToString();
				}
				if(row["MemberRegisterMobile"]!=null)
				{
					model.MemberRegisterMobile=row["MemberRegisterMobile"].ToString();
				}
				if(row["MemberMobilePhone"]!=null)
				{
					model.MemberMobilePhone=row["MemberMobilePhone"].ToString();
				}
				if(row["MemberEmail"]!=null)
				{
					model.MemberEmail=row["MemberEmail"].ToString();
				}
				if(row["MemberAppellation"]!=null)
				{
					model.MemberAppellation=row["MemberAppellation"].ToString();
				}
				if(row["MemberEngFamilyName"]!=null)
				{
					model.MemberEngFamilyName=row["MemberEngFamilyName"].ToString();
				}
				if(row["MemberEngGivenName"]!=null)
				{
					model.MemberEngGivenName=row["MemberEngGivenName"].ToString();
				}
				if(row["MemberChiFamilyName"]!=null)
				{
					model.MemberChiFamilyName=row["MemberChiFamilyName"].ToString();
				}
				if(row["MemberChiGivenName"]!=null)
				{
					model.MemberChiGivenName=row["MemberChiGivenName"].ToString();
				}
				if(row["MemberSex"]!=null && row["MemberSex"].ToString()!="")
				{
					model.MemberSex=int.Parse(row["MemberSex"].ToString());
				}
				if(row["MemberDateOfBirth"]!=null && row["MemberDateOfBirth"].ToString()!="")
				{
					model.MemberDateOfBirth=DateTime.Parse(row["MemberDateOfBirth"].ToString());
				}
				if(row["MemberDayOfBirth"]!=null && row["MemberDayOfBirth"].ToString()!="")
				{
					model.MemberDayOfBirth=int.Parse(row["MemberDayOfBirth"].ToString());
				}
				if(row["MemberMonthOfBirth"]!=null && row["MemberMonthOfBirth"].ToString()!="")
				{
					model.MemberMonthOfBirth=int.Parse(row["MemberMonthOfBirth"].ToString());
				}
				if(row["MemberYearOfBirth"]!=null && row["MemberYearOfBirth"].ToString()!="")
				{
					model.MemberYearOfBirth=int.Parse(row["MemberYearOfBirth"].ToString());
				}
				if(row["MemberMarital"]!=null && row["MemberMarital"].ToString()!="")
				{
					model.MemberMarital=int.Parse(row["MemberMarital"].ToString());
				}
				if(row["HomeAddress"]!=null)
				{
					model.HomeAddress=row["HomeAddress"].ToString();
				}
				if(row["HomeTelNum"]!=null)
				{
					model.HomeTelNum=row["HomeTelNum"].ToString();
				}
				if(row["HomeFaxNum"]!=null)
				{
					model.HomeFaxNum=row["HomeFaxNum"].ToString();
				}
				if(row["OfficeAddress"]!=null)
				{
					model.OfficeAddress=row["OfficeAddress"].ToString();
				}
				if(row["OfficeTelNum"]!=null)
				{
					model.OfficeTelNum=row["OfficeTelNum"].ToString();
				}
				if(row["OfficeFaxNum"]!=null)
				{
					model.OfficeFaxNum=row["OfficeFaxNum"].ToString();
				}
				if(row["EducationID"]!=null && row["EducationID"].ToString()!="")
				{
					model.EducationID=int.Parse(row["EducationID"].ToString());
				}
				if(row["ProfessionID"]!=null && row["ProfessionID"].ToString()!="")
				{
					model.ProfessionID=int.Parse(row["ProfessionID"].ToString());
				}
				if(row["MemberPosition"]!=null)
				{
					model.MemberPosition=row["MemberPosition"].ToString();
				}
				if(row["NationID"]!=null && row["NationID"].ToString()!="")
				{
					model.NationID=int.Parse(row["NationID"].ToString());
				}
				if(row["CompanyDesc"]!=null)
				{
					model.CompanyDesc=row["CompanyDesc"].ToString();
				}
				if(row["SpRemark"]!=null)
				{
					model.SpRemark=row["SpRemark"].ToString();
				}
				if(row["OtherContact"]!=null)
				{
					model.OtherContact=row["OtherContact"].ToString();
				}
				if(row["Hobbies"]!=null)
				{
					model.Hobbies=row["Hobbies"].ToString();
				}
				if(row["Status"]!=null && row["Status"].ToString()!="")
				{
					model.Status=int.Parse(row["Status"].ToString());
				}
				if(row["MemberDefLanguage"]!=null && row["MemberDefLanguage"].ToString()!="")
				{
					model.MemberDefLanguage=int.Parse(row["MemberDefLanguage"].ToString());
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["PasswordExpiryDate"]!=null && row["PasswordExpiryDate"].ToString()!="")
				{
					model.PasswordExpiryDate=DateTime.Parse(row["PasswordExpiryDate"].ToString());
				}
				if(row["PWDExpiryPromptDays"]!=null && row["PWDExpiryPromptDays"].ToString()!="")
				{
					model.PWDExpiryPromptDays=int.Parse(row["PWDExpiryPromptDays"].ToString());
				}
				if(row["CountryCode"]!=null)
				{
					model.CountryCode=row["CountryCode"].ToString();
				}
				if(row["NickName"]!=null)
				{
					model.NickName=row["NickName"].ToString();
				}
				if(row["MemberPictureFile"]!=null)
				{
					model.MemberPictureFile=row["MemberPictureFile"].ToString();
				}
				if(row["MemberPicture"]!=null && row["MemberPicture"].ToString()!="")
				{
					model.MemberPicture=(byte[])row["MemberPicture"];
				}
				if(row["ReadReguFlag"]!=null && row["ReadReguFlag"].ToString()!="")
				{
					model.ReadReguFlag=int.Parse(row["ReadReguFlag"].ToString());
				}
				if(row["ReferCardNumber"]!=null)
				{
					model.ReferCardNumber=row["ReferCardNumber"].ToString();
				}
				if(row["ViewHistoryTranDays"]!=null && row["ViewHistoryTranDays"].ToString()!="")
				{
					model.ViewHistoryTranDays=int.Parse(row["ViewHistoryTranDays"].ToString());
				}
				if(row["ResetPWDCount"]!=null && row["ResetPWDCount"].ToString()!="")
				{
					model.ResetPWDCount=int.Parse(row["ResetPWDCount"].ToString());
				}
				if(row["AcceptPhoneAdvertising"]!=null && row["AcceptPhoneAdvertising"].ToString()!="")
				{
					model.AcceptPhoneAdvertising=int.Parse(row["AcceptPhoneAdvertising"].ToString());
				}
				if(row["ReceiveAllAdvertising"]!=null && row["ReceiveAllAdvertising"].ToString()!="")
				{
					model.ReceiveAllAdvertising=int.Parse(row["ReceiveAllAdvertising"].ToString());
				}
				if(row["TransPersonalInfo"]!=null && row["TransPersonalInfo"].ToString()!="")
				{
					model.TransPersonalInfo=int.Parse(row["TransPersonalInfo"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select MemberID,MemberPassword,MemberIdentityType,MemberIdentityRef,MemberRegisterMobile,MemberMobilePhone,MemberEmail,MemberAppellation,MemberEngFamilyName,MemberEngGivenName,MemberChiFamilyName,MemberChiGivenName,MemberSex,MemberDateOfBirth,MemberDayOfBirth,MemberMonthOfBirth,MemberYearOfBirth,MemberMarital,HomeAddress,HomeTelNum,HomeFaxNum,OfficeAddress,OfficeTelNum,OfficeFaxNum,EducationID,ProfessionID,MemberPosition,NationID,CompanyDesc,SpRemark,OtherContact,Hobbies,Status,MemberDefLanguage,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,CountryCode,NickName,MemberPictureFile,MemberPicture,ReadReguFlag,ReferCardNumber,ViewHistoryTranDays,ResetPWDCount,AcceptPhoneAdvertising,ReceiveAllAdvertising,TransPersonalInfo ");
			strSql.Append(" FROM Member ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" MemberID,MemberPassword,MemberIdentityType,MemberIdentityRef,MemberRegisterMobile,MemberMobilePhone,MemberEmail,MemberAppellation,MemberEngFamilyName,MemberEngGivenName,MemberChiFamilyName,MemberChiGivenName,MemberSex,MemberDateOfBirth,MemberDayOfBirth,MemberMonthOfBirth,MemberYearOfBirth,MemberMarital,HomeAddress,HomeTelNum,HomeFaxNum,OfficeAddress,OfficeTelNum,OfficeFaxNum,EducationID,ProfessionID,MemberPosition,NationID,CompanyDesc,SpRemark,OtherContact,Hobbies,Status,MemberDefLanguage,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,CountryCode,NickName,MemberPictureFile,MemberPicture,ReadReguFlag,ReferCardNumber,ViewHistoryTranDays,ResetPWDCount,AcceptPhoneAdvertising,ReceiveAllAdvertising,TransPersonalInfo ");
			strSql.Append(" FROM Member ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Member ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.MemberID desc");
			}
			strSql.Append(")AS Row, T.*  from Member T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}



        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Member";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Member ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Member";
			parameters[1].Value = "MemberID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

