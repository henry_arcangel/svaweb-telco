﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:InventoryReplenishRule_H
    /// </summary>
    public partial class InventoryReplenishRule_H : IInventoryReplenishRule_H
    {
        public InventoryReplenishRule_H()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string InventoryReplenishCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from InventoryReplenishRule_H");
            strSql.Append(" where InventoryReplenishCode=@InventoryReplenishCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = InventoryReplenishCode;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.InventoryReplenishRule_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into InventoryReplenishRule_H(");
            strSql.Append("InventoryReplenishCode,Description,StartDate,EndDate,Status,MediaType,BrandID,CouponTypeID,CardTypeID,CardGradeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,PurchaseType)");
            strSql.Append(" values (");
            strSql.Append("@InventoryReplenishCode,@Description,@StartDate,@EndDate,@Status,@MediaType,@BrandID,@CouponTypeID,@CardTypeID,@CardGradeID,@StoreTypeID,@AutoCreateOrder,@AutoApproveOrder,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@DayFlagID,@MonthFlagID,@WeekFlagID,@ActiveTime,@PurchaseType)");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64),
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MediaType", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreTypeID", SqlDbType.Int,4),
					new SqlParameter("@AutoCreateOrder", SqlDbType.Int,4),
					new SqlParameter("@AutoApproveOrder", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@DayFlagID", SqlDbType.Int,4),
					new SqlParameter("@MonthFlagID", SqlDbType.Int,4),
					new SqlParameter("@WeekFlagID", SqlDbType.Int,4),
					new SqlParameter("@ActiveTime", SqlDbType.DateTime),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4)};
            parameters[0].Value = model.InventoryReplenishCode;
            parameters[1].Value = model.Description;
            parameters[2].Value = model.StartDate;
            parameters[3].Value = model.EndDate;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.MediaType;
            parameters[6].Value = model.BrandID;
            parameters[7].Value = model.CouponTypeID;
            parameters[8].Value = model.CardTypeID;
            parameters[9].Value = model.CardGradeID;
            parameters[10].Value = model.StoreTypeID;
            parameters[11].Value = model.AutoCreateOrder;
            parameters[12].Value = model.AutoApproveOrder;
            parameters[13].Value = model.CreatedOn;
            parameters[14].Value = model.UpdatedOn;
            parameters[15].Value = model.CreatedBy;
            parameters[16].Value = model.UpdatedBy;
            parameters[17].Value = model.DayFlagID;
            parameters[18].Value = model.MonthFlagID;
            parameters[19].Value = model.WeekFlagID;
            parameters[20].Value = model.ActiveTime;
            parameters[21].Value = model.PurchaseType;
 
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.InventoryReplenishRule_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update InventoryReplenishRule_H set ");
            strSql.Append("Description=@Description,");
            strSql.Append("StartDate=@StartDate,");
            strSql.Append("EndDate=@EndDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("MediaType=@MediaType,");
            strSql.Append("BrandID=@BrandID,");
            strSql.Append("CouponTypeID=@CouponTypeID,");
            strSql.Append("CardTypeID=@CardTypeID,");
            strSql.Append("CardGradeID=@CardGradeID,");
            strSql.Append("StoreTypeID=@StoreTypeID,");
            strSql.Append("AutoCreateOrder=@AutoCreateOrder,");
            strSql.Append("AutoApproveOrder=@AutoApproveOrder,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("DayFlagID=@DayFlagID,");
            strSql.Append("MonthFlagID=@MonthFlagID,");
            strSql.Append("WeekFlagID=@WeekFlagID,");
            strSql.Append("ActiveTime=@ActiveTime,");
            strSql.Append("PurchaseType=@PurchaseType");
            strSql.Append(" where InventoryReplenishCode=@InventoryReplenishCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MediaType", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreTypeID", SqlDbType.Int,4),
					new SqlParameter("@AutoCreateOrder", SqlDbType.Int,4),
					new SqlParameter("@AutoApproveOrder", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@DayFlagID", SqlDbType.Int,4),
					new SqlParameter("@MonthFlagID", SqlDbType.Int,4),
					new SqlParameter("@WeekFlagID", SqlDbType.Int,4),
					new SqlParameter("@ActiveTime", SqlDbType.DateTime),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4),
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64)};
            parameters[0].Value = model.Description;
            parameters[1].Value = model.StartDate;
            parameters[2].Value = model.EndDate;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.MediaType;
            parameters[5].Value = model.BrandID;
            parameters[6].Value = model.CouponTypeID;
            parameters[7].Value = model.CardTypeID;
            parameters[8].Value = model.CardGradeID;
            parameters[9].Value = model.StoreTypeID;
            parameters[10].Value = model.AutoCreateOrder;
            parameters[11].Value = model.AutoApproveOrder;
            parameters[12].Value = model.CreatedOn;
            parameters[13].Value = model.UpdatedOn;
            parameters[14].Value = model.CreatedBy;
            parameters[15].Value = model.UpdatedBy;
            parameters[16].Value = model.DayFlagID;
            parameters[17].Value = model.MonthFlagID;
            parameters[18].Value = model.WeekFlagID;
            parameters[19].Value = model.ActiveTime;
            parameters[20].Value = model.PurchaseType;
            parameters[21].Value = model.InventoryReplenishCode;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string InventoryReplenishCode)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InventoryReplenishRule_H ");
            strSql.Append(" where InventoryReplenishCode=@InventoryReplenishCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = InventoryReplenishCode;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string InventoryReplenishCodelist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InventoryReplenishRule_H ");
            strSql.Append(" where InventoryReplenishCode in (" + InventoryReplenishCodelist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.InventoryReplenishRule_H GetModel(string InventoryReplenishCode)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 InventoryReplenishCode,Description,StartDate,EndDate,Status,MediaType,BrandID,CouponTypeID,CardTypeID,CardGradeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,PurchaseType from InventoryReplenishRule_H ");
            strSql.Append(" where InventoryReplenishCode=@InventoryReplenishCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = InventoryReplenishCode;

            Edge.SVA.Model.InventoryReplenishRule_H model = new Edge.SVA.Model.InventoryReplenishRule_H();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["InventoryReplenishCode"] != null && ds.Tables[0].Rows[0]["InventoryReplenishCode"].ToString() != "")
                {
                    model.InventoryReplenishCode = ds.Tables[0].Rows[0]["InventoryReplenishCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Description"] != null && ds.Tables[0].Rows[0]["Description"].ToString() != "")
                {
                    model.Description = ds.Tables[0].Rows[0]["Description"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StartDate"] != null && ds.Tables[0].Rows[0]["StartDate"].ToString() != "")
                {
                    model.StartDate = DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["EndDate"] != null && ds.Tables[0].Rows[0]["EndDate"].ToString() != "")
                {
                    model.EndDate = DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MediaType"] != null && ds.Tables[0].Rows[0]["MediaType"].ToString() != "")
                {
                    model.MediaType = int.Parse(ds.Tables[0].Rows[0]["MediaType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BrandID"] != null && ds.Tables[0].Rows[0]["BrandID"].ToString() != "")
                {
                    model.BrandID = int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponTypeID"] != null && ds.Tables[0].Rows[0]["CouponTypeID"].ToString() != "")
                {
                    model.CouponTypeID = int.Parse(ds.Tables[0].Rows[0]["CouponTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StoreTypeID"] != null && ds.Tables[0].Rows[0]["StoreTypeID"].ToString() != "")
                {
                    model.StoreTypeID = int.Parse(ds.Tables[0].Rows[0]["StoreTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AutoCreateOrder"] != null && ds.Tables[0].Rows[0]["AutoCreateOrder"].ToString() != "")
                {
                    model.AutoCreateOrder = int.Parse(ds.Tables[0].Rows[0]["AutoCreateOrder"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AutoApproveOrder"] != null && ds.Tables[0].Rows[0]["AutoApproveOrder"].ToString() != "")
                {
                    model.AutoApproveOrder = int.Parse(ds.Tables[0].Rows[0]["AutoApproveOrder"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DayFlagID"] != null && ds.Tables[0].Rows[0]["DayFlagID"].ToString() != "")
                {
                    model.DayFlagID = int.Parse(ds.Tables[0].Rows[0]["DayFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MonthFlagID"] != null && ds.Tables[0].Rows[0]["MonthFlagID"].ToString() != "")
                {
                    model.MonthFlagID = int.Parse(ds.Tables[0].Rows[0]["MonthFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WeekFlagID"] != null && ds.Tables[0].Rows[0]["WeekFlagID"].ToString() != "")
                {
                    model.WeekFlagID = int.Parse(ds.Tables[0].Rows[0]["WeekFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActiveTime"] != null && ds.Tables[0].Rows[0]["ActiveTime"].ToString() != "")
                {
                    model.ActiveTime = DateTime.Parse(ds.Tables[0].Rows[0]["ActiveTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PurchaseType"] != null && ds.Tables[0].Rows[0]["PurchaseType"].ToString() != "")
                {
                    model.PurchaseType = int.Parse(ds.Tables[0].Rows[0]["PurchaseType"].ToString());
                }
                
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select InventoryReplenishCode,Description,StartDate,EndDate,Status,MediaType,BrandID,CouponTypeID,CardTypeID,CardGradeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,PurchaseType ");
            strSql.Append(" FROM InventoryReplenishRule_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" InventoryReplenishCode,Description,StartDate,EndDate,Status,MediaType,BrandID,CouponTypeID,CardTypeID,CardGradeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,PurchaseType ");
            strSql.Append(" FROM InventoryReplenishRule_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM InventoryReplenishRule_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.InventoryReplenishCode desc");
            }
            strSql.Append(")AS Row, T.*  from InventoryReplenishRule_H T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "InventoryReplenishRule_H";
            parameters[1].Value = "InventoryReplenishCode";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

