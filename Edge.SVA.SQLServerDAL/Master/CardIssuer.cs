﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardIssuer
	/// </summary>
	public partial class CardIssuer:ICardIssuer
	{
		public CardIssuer()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CardIssuerID", "CardIssuer"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CardIssuerID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardIssuer");
			strSql.Append(" where CardIssuerID=@CardIssuerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssuerID", SqlDbType.Int,4)
			};
			parameters[0].Value = CardIssuerID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.CardIssuer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardIssuer(");
			strSql.Append("CardIssuerName1,CardIssuerName2,CardIssuerName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CardIssuerDesc1,CardIssuerDesc2,CardIssuerDesc3,DomesticCurrencyID)");
			strSql.Append(" values (");
			strSql.Append("@CardIssuerName1,@CardIssuerName2,@CardIssuerName3,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@CardIssuerDesc1,@CardIssuerDesc2,@CardIssuerDesc3,@DomesticCurrencyID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssuerName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardIssuerName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardIssuerName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardIssuerDesc1", SqlDbType.NVarChar),
					new SqlParameter("@CardIssuerDesc2", SqlDbType.NVarChar),
					new SqlParameter("@CardIssuerDesc3", SqlDbType.NVarChar),
					new SqlParameter("@DomesticCurrencyID", SqlDbType.Int,4)};
			parameters[0].Value = model.CardIssuerName1;
			parameters[1].Value = model.CardIssuerName2;
			parameters[2].Value = model.CardIssuerName3;
			parameters[3].Value = model.CreatedOn;
			parameters[4].Value = model.CreatedBy;
			parameters[5].Value = model.UpdatedOn;
			parameters[6].Value = model.UpdatedBy;
			parameters[7].Value = model.CardIssuerDesc1;
			parameters[8].Value = model.CardIssuerDesc2;
			parameters[9].Value = model.CardIssuerDesc3;
			parameters[10].Value = model.DomesticCurrencyID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardIssuer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardIssuer set ");
			strSql.Append("CardIssuerName1=@CardIssuerName1,");
			strSql.Append("CardIssuerName2=@CardIssuerName2,");
			strSql.Append("CardIssuerName3=@CardIssuerName3,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CardIssuerDesc1=@CardIssuerDesc1,");
			strSql.Append("CardIssuerDesc2=@CardIssuerDesc2,");
			strSql.Append("CardIssuerDesc3=@CardIssuerDesc3,");
			strSql.Append("DomesticCurrencyID=@DomesticCurrencyID");
			strSql.Append(" where CardIssuerID=@CardIssuerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssuerName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardIssuerName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardIssuerName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardIssuerDesc1", SqlDbType.NVarChar),
					new SqlParameter("@CardIssuerDesc2", SqlDbType.NVarChar),
					new SqlParameter("@CardIssuerDesc3", SqlDbType.NVarChar),
					new SqlParameter("@DomesticCurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CardIssuerID", SqlDbType.Int,4)};
			parameters[0].Value = model.CardIssuerName1;
			parameters[1].Value = model.CardIssuerName2;
			parameters[2].Value = model.CardIssuerName3;
			parameters[3].Value = model.CreatedOn;
			parameters[4].Value = model.CreatedBy;
			parameters[5].Value = model.UpdatedOn;
			parameters[6].Value = model.UpdatedBy;
			parameters[7].Value = model.CardIssuerDesc1;
			parameters[8].Value = model.CardIssuerDesc2;
			parameters[9].Value = model.CardIssuerDesc3;
			parameters[10].Value = model.DomesticCurrencyID;
			parameters[11].Value = model.CardIssuerID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardIssuerID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardIssuer ");
			strSql.Append(" where CardIssuerID=@CardIssuerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssuerID", SqlDbType.Int,4)
			};
			parameters[0].Value = CardIssuerID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardIssuerIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardIssuer ");
			strSql.Append(" where CardIssuerID in ("+CardIssuerIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardIssuer GetModel(int CardIssuerID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CardIssuerID,CardIssuerName1,CardIssuerName2,CardIssuerName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CardIssuerDesc1,CardIssuerDesc2,CardIssuerDesc3,DomesticCurrencyID from CardIssuer ");
			strSql.Append(" where CardIssuerID=@CardIssuerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssuerID", SqlDbType.Int,4)
			};
			parameters[0].Value = CardIssuerID;

			Edge.SVA.Model.CardIssuer model=new Edge.SVA.Model.CardIssuer();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CardIssuerID"]!=null && ds.Tables[0].Rows[0]["CardIssuerID"].ToString()!="")
				{
					model.CardIssuerID=int.Parse(ds.Tables[0].Rows[0]["CardIssuerID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardIssuerName1"]!=null && ds.Tables[0].Rows[0]["CardIssuerName1"].ToString()!="")
				{
					model.CardIssuerName1=ds.Tables[0].Rows[0]["CardIssuerName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardIssuerName2"]!=null && ds.Tables[0].Rows[0]["CardIssuerName2"].ToString()!="")
				{
					model.CardIssuerName2=ds.Tables[0].Rows[0]["CardIssuerName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardIssuerName3"]!=null && ds.Tables[0].Rows[0]["CardIssuerName3"].ToString()!="")
				{
					model.CardIssuerName3=ds.Tables[0].Rows[0]["CardIssuerName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardIssuerDesc1"]!=null && ds.Tables[0].Rows[0]["CardIssuerDesc1"].ToString()!="")
				{
					model.CardIssuerDesc1=ds.Tables[0].Rows[0]["CardIssuerDesc1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardIssuerDesc2"]!=null && ds.Tables[0].Rows[0]["CardIssuerDesc2"].ToString()!="")
				{
					model.CardIssuerDesc2=ds.Tables[0].Rows[0]["CardIssuerDesc2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardIssuerDesc3"]!=null && ds.Tables[0].Rows[0]["CardIssuerDesc3"].ToString()!="")
				{
					model.CardIssuerDesc3=ds.Tables[0].Rows[0]["CardIssuerDesc3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DomesticCurrencyID"]!=null && ds.Tables[0].Rows[0]["DomesticCurrencyID"].ToString()!="")
				{
					model.DomesticCurrencyID=int.Parse(ds.Tables[0].Rows[0]["DomesticCurrencyID"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CardIssuerID,CardIssuerName1,CardIssuerName2,CardIssuerName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CardIssuerDesc1,CardIssuerDesc2,CardIssuerDesc3,DomesticCurrencyID ");
			strSql.Append(" FROM CardIssuer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CardIssuerID,CardIssuerName1,CardIssuerName2,CardIssuerName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CardIssuerDesc1,CardIssuerDesc2,CardIssuerDesc3,DomesticCurrencyID ");
			strSql.Append(" FROM CardIssuer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM CardIssuer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CardIssuerID desc");
			}
			strSql.Append(")AS Row, T.*  from CardIssuer T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "CardIssuer";
			parameters[1].Value = "CardIssuerID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

