﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Ord_CardReceive_H
    /// </summary>
    public partial class Ord_CardReceive_H : IOrd_CardReceive_H
    {
        public Ord_CardReceive_H()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CardReceiveNumber)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Ord_CardReceive_H");
            strSql.Append(" where CardReceiveNumber=@CardReceiveNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardReceiveNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CardReceiveNumber;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Ord_CardReceive_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Ord_CardReceive_H(");
            strSql.Append("CardReceiveNumber,ReferenceNo,StoreID,SupplierID,StorerAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,OrderType,ReceiveType,Remark1,Remark2,CompanyID,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@CardReceiveNumber,@ReferenceNo,@StoreID,@SupplierID,@StorerAddress,@SupplierAddress,@SuppliertContactName,@SupplierPhone,@SupplierEmail,@SupplierMobile,@StoreContactName,@StorePhone,@StoreEmail,@StoreMobile,@Remark,@OrderType,@ReceiveType,@Remark1,@Remark2,@CompanyID,@PurchaseType,@CreatedBusDate,@ApproveBusDate,@ApprovalCode,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            SqlParameter[] parameters = {
					new SqlParameter("@CardReceiveNumber", SqlDbType.VarChar,64),
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@StorerAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SupplierAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SuppliertContactName", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierPhone", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierMobile", SqlDbType.VarChar,512),
					new SqlParameter("@StoreContactName", SqlDbType.VarChar,512),
					new SqlParameter("@StorePhone", SqlDbType.VarChar,512),
					new SqlParameter("@StoreEmail", SqlDbType.VarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@OrderType", SqlDbType.Int,4),
					new SqlParameter("@ReceiveType", SqlDbType.Int,4),
					new SqlParameter("@Remark1", SqlDbType.VarChar,512),
					new SqlParameter("@Remark2", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyID", SqlDbType.Int,4),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.CardReceiveNumber;
            parameters[1].Value = model.ReferenceNo;
            parameters[2].Value = model.StoreID;
            parameters[3].Value = model.SupplierID;
            parameters[4].Value = model.StorerAddress;
            parameters[5].Value = model.SupplierAddress;
            parameters[6].Value = model.SuppliertContactName;
            parameters[7].Value = model.SupplierPhone;
            parameters[8].Value = model.SupplierEmail;
            parameters[9].Value = model.SupplierMobile;
            parameters[10].Value = model.StoreContactName;
            parameters[11].Value = model.StorePhone;
            parameters[12].Value = model.StoreEmail;
            parameters[13].Value = model.StoreMobile;
            parameters[14].Value = model.Remark;
            parameters[15].Value = model.OrderType;
            parameters[16].Value = model.ReceiveType;
            parameters[17].Value = model.Remark1;
            parameters[18].Value = model.Remark2;
            parameters[19].Value = model.CompanyID;
            parameters[20].Value = model.PurchaseType;
            parameters[21].Value = model.CreatedBusDate;
            parameters[22].Value = model.ApproveBusDate;
            parameters[23].Value = model.ApprovalCode;
            parameters[24].Value = model.ApproveStatus;
            parameters[25].Value = model.ApproveOn;
            parameters[26].Value = model.ApproveBy;
            parameters[27].Value = model.CreatedOn;
            parameters[28].Value = model.CreatedBy;
            parameters[29].Value = model.UpdatedOn;
            parameters[30].Value = model.UpdatedBy;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_CardReceive_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_CardReceive_H set ");
            strSql.Append("ReferenceNo=@ReferenceNo,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("SupplierID=@SupplierID,");
            strSql.Append("StorerAddress=@StorerAddress,");
            strSql.Append("SupplierAddress=@SupplierAddress,");
            strSql.Append("SuppliertContactName=@SuppliertContactName,");
            strSql.Append("SupplierPhone=@SupplierPhone,");
            strSql.Append("SupplierEmail=@SupplierEmail,");
            strSql.Append("SupplierMobile=@SupplierMobile,");
            strSql.Append("StoreContactName=@StoreContactName,");
            strSql.Append("StorePhone=@StorePhone,");
            strSql.Append("StoreEmail=@StoreEmail,");
            strSql.Append("StoreMobile=@StoreMobile,");
            strSql.Append("Remark=@Remark,");
            strSql.Append("OrderType=@OrderType,");
            strSql.Append("ReceiveType=@ReceiveType,");
            strSql.Append("Remark1=@Remark1,");
            strSql.Append("Remark2=@Remark2,");
            strSql.Append("CompanyID=@CompanyID,");
            strSql.Append("PurchaseType=@PurchaseType,");
            strSql.Append("CreatedBusDate=@CreatedBusDate,");
            strSql.Append("ApproveBusDate=@ApproveBusDate,");
            strSql.Append("ApprovalCode=@ApprovalCode,");
            strSql.Append("ApproveStatus=@ApproveStatus,");
            strSql.Append("ApproveOn=@ApproveOn,");
            strSql.Append("ApproveBy=@ApproveBy,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where CardReceiveNumber=@CardReceiveNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@StorerAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SupplierAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SuppliertContactName", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierPhone", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierMobile", SqlDbType.VarChar,512),
					new SqlParameter("@StoreContactName", SqlDbType.VarChar,512),
					new SqlParameter("@StorePhone", SqlDbType.VarChar,512),
					new SqlParameter("@StoreEmail", SqlDbType.VarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@OrderType", SqlDbType.Int,4),
					new SqlParameter("@ReceiveType", SqlDbType.Int,4),
					new SqlParameter("@Remark1", SqlDbType.VarChar,512),
					new SqlParameter("@Remark2", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyID", SqlDbType.Int,4),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardReceiveNumber", SqlDbType.VarChar,64)};
            parameters[0].Value = model.ReferenceNo;
            parameters[1].Value = model.StoreID;
            parameters[2].Value = model.SupplierID;
            parameters[3].Value = model.StorerAddress;
            parameters[4].Value = model.SupplierAddress;
            parameters[5].Value = model.SuppliertContactName;
            parameters[6].Value = model.SupplierPhone;
            parameters[7].Value = model.SupplierEmail;
            parameters[8].Value = model.SupplierMobile;
            parameters[9].Value = model.StoreContactName;
            parameters[10].Value = model.StorePhone;
            parameters[11].Value = model.StoreEmail;
            parameters[12].Value = model.StoreMobile;
            parameters[13].Value = model.Remark;
            parameters[14].Value = model.OrderType;
            parameters[15].Value = model.ReceiveType;
            parameters[16].Value = model.Remark1;
            parameters[17].Value = model.Remark2;
            parameters[18].Value = model.CompanyID;
            parameters[19].Value = model.PurchaseType;
            parameters[20].Value = model.CreatedBusDate;
            parameters[21].Value = model.ApproveBusDate;
            parameters[22].Value = model.ApprovalCode;
            parameters[23].Value = model.ApproveStatus;
            parameters[24].Value = model.ApproveOn;
            parameters[25].Value = model.ApproveBy;
            parameters[26].Value = model.CreatedOn;
            parameters[27].Value = model.CreatedBy;
            parameters[28].Value = model.UpdatedOn;
            parameters[29].Value = model.UpdatedBy;
            parameters[30].Value = model.CardReceiveNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CardReceiveNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_CardReceive_H ");
            strSql.Append(" where CardReceiveNumber=@CardReceiveNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardReceiveNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CardReceiveNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CardReceiveNumberlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_CardReceive_H ");
            strSql.Append(" where CardReceiveNumber in (" + CardReceiveNumberlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Ord_CardReceive_H GetModel(string CardReceiveNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CardReceiveNumber,ReferenceNo,StoreID,SupplierID,StorerAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,OrderType,ReceiveType,Remark1,Remark2,CompanyID,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Ord_CardReceive_H ");
            strSql.Append(" where CardReceiveNumber=@CardReceiveNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardReceiveNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CardReceiveNumber;

            Edge.SVA.Model.Ord_CardReceive_H model = new Edge.SVA.Model.Ord_CardReceive_H();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CardReceiveNumber"] != null && ds.Tables[0].Rows[0]["CardReceiveNumber"].ToString() != "")
                {
                    model.CardReceiveNumber = ds.Tables[0].Rows[0]["CardReceiveNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ReferenceNo"] != null && ds.Tables[0].Rows[0]["ReferenceNo"].ToString() != "")
                {
                    model.ReferenceNo = ds.Tables[0].Rows[0]["ReferenceNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SupplierID"] != null && ds.Tables[0].Rows[0]["SupplierID"].ToString() != "")
                {
                    model.SupplierID = int.Parse(ds.Tables[0].Rows[0]["SupplierID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StorerAddress"] != null && ds.Tables[0].Rows[0]["StorerAddress"].ToString() != "")
                {
                    model.StorerAddress = ds.Tables[0].Rows[0]["StorerAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierAddress"] != null && ds.Tables[0].Rows[0]["SupplierAddress"].ToString() != "")
                {
                    model.SupplierAddress = ds.Tables[0].Rows[0]["SupplierAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SuppliertContactName"] != null && ds.Tables[0].Rows[0]["SuppliertContactName"].ToString() != "")
                {
                    model.SuppliertContactName = ds.Tables[0].Rows[0]["SuppliertContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierPhone"] != null && ds.Tables[0].Rows[0]["SupplierPhone"].ToString() != "")
                {
                    model.SupplierPhone = ds.Tables[0].Rows[0]["SupplierPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierEmail"] != null && ds.Tables[0].Rows[0]["SupplierEmail"].ToString() != "")
                {
                    model.SupplierEmail = ds.Tables[0].Rows[0]["SupplierEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierMobile"] != null && ds.Tables[0].Rows[0]["SupplierMobile"].ToString() != "")
                {
                    model.SupplierMobile = ds.Tables[0].Rows[0]["SupplierMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreContactName"] != null && ds.Tables[0].Rows[0]["StoreContactName"].ToString() != "")
                {
                    model.StoreContactName = ds.Tables[0].Rows[0]["StoreContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StorePhone"] != null && ds.Tables[0].Rows[0]["StorePhone"].ToString() != "")
                {
                    model.StorePhone = ds.Tables[0].Rows[0]["StorePhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreEmail"] != null && ds.Tables[0].Rows[0]["StoreEmail"].ToString() != "")
                {
                    model.StoreEmail = ds.Tables[0].Rows[0]["StoreEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreMobile"] != null && ds.Tables[0].Rows[0]["StoreMobile"].ToString() != "")
                {
                    model.StoreMobile = ds.Tables[0].Rows[0]["StoreMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OrderType"] != null && ds.Tables[0].Rows[0]["OrderType"].ToString() != "")
                {
                    model.OrderType = int.Parse(ds.Tables[0].Rows[0]["OrderType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ReceiveType"] != null && ds.Tables[0].Rows[0]["ReceiveType"].ToString() != "")
                {
                    model.ReceiveType = int.Parse(ds.Tables[0].Rows[0]["ReceiveType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Remark1"] != null && ds.Tables[0].Rows[0]["Remark1"].ToString() != "")
                {
                    model.Remark1 = ds.Tables[0].Rows[0]["Remark1"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark2"] != null && ds.Tables[0].Rows[0]["Remark2"].ToString() != "")
                {
                    model.Remark2 = ds.Tables[0].Rows[0]["Remark2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CompanyID"] != null && ds.Tables[0].Rows[0]["CompanyID"].ToString() != "")
                {
                    model.CompanyID = int.Parse(ds.Tables[0].Rows[0]["CompanyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PurchaseType"] != null && ds.Tables[0].Rows[0]["PurchaseType"].ToString() != "")
                {
                    model.PurchaseType = int.Parse(ds.Tables[0].Rows[0]["PurchaseType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBusDate"] != null && ds.Tables[0].Rows[0]["CreatedBusDate"].ToString() != "")
                {
                    model.CreatedBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBusDate"] != null && ds.Tables[0].Rows[0]["ApproveBusDate"].ToString() != "")
                {
                    model.ApproveBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApprovalCode"] != null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString() != "")
                {
                    model.ApprovalCode = ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveStatus"] != null && ds.Tables[0].Rows[0]["ApproveStatus"].ToString() != "")
                {
                    model.ApproveStatus = ds.Tables[0].Rows[0]["ApproveStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveOn"] != null && ds.Tables[0].Rows[0]["ApproveOn"].ToString() != "")
                {
                    model.ApproveOn = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBy"] != null && ds.Tables[0].Rows[0]["ApproveBy"].ToString() != "")
                {
                    model.ApproveBy = int.Parse(ds.Tables[0].Rows[0]["ApproveBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CardReceiveNumber,ReferenceNo,StoreID,SupplierID,StorerAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,OrderType,ReceiveType,Remark1,Remark2,CompanyID,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_CardReceive_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CardReceiveNumber,ReferenceNo,StoreID,SupplierID,StorerAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,OrderType,ReceiveType,Remark1,Remark2,CompanyID,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_CardReceive_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Ord_CardReceive_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CardReceiveNumber desc");
            }
            strSql.Append(")AS Row, T.*  from Ord_CardReceive_H T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Ord_CardReceive_H";
            parameters[1].Value = "CardReceiveNumber";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

