﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:GrossMargin_MobileNo
    /// </summary>
    public partial class GrossMargin_MobileNo : IGrossMargin_MobileNo
    {
        public GrossMargin_MobileNo()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from GrossMargin_MobileNo");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.GrossMargin_MobileNo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into GrossMargin_MobileNo(");
            strSql.Append("GrossMarginCode, MobileNoPattern, PatternStart, PatternLen)");
            strSql.Append(" values (");
            strSql.Append("@GrossMarginCode, @MobileNoPattern, @PatternStart, @PatternLen)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64),
					new SqlParameter("@MobileNoPattern", SqlDbType.VarChar,64),
					new SqlParameter("@PatternStart", SqlDbType.Int,4),
					new SqlParameter("@PatternLen", SqlDbType.Int,4)};
            parameters[0].Value = model.GrossMarginCode;
            parameters[1].Value = model.MobileNoPattern;
            parameters[2].Value = model.PatternStart;
            parameters[3].Value = model.PatternLen;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.GrossMargin_MobileNo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update GrossMargin_MobileNo set ");
            strSql.Append("GrossMarginCode=@GrossMarginCode,");
            strSql.Append("MobileNoPattern=@MobileNoPattern,");
            strSql.Append("PatternStart=@PatternStart,");
            strSql.Append("PatternLen=@PatternLen");   
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64),
                    new SqlParameter("@MobileNoPattern", SqlDbType.VarChar,64),
					new SqlParameter("@PatternStart", SqlDbType.Int,4),
					new SqlParameter("@PatternLen", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
            parameters[0].Value = model.GrossMarginCode;
            parameters[1].Value = model.MobileNoPattern;
            parameters[2].Value = model.PatternStart;
            parameters[3].Value = model.PatternLen;
            parameters[4].Value = model.KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from GrossMargin_MobileNo ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from GrossMargin_MobileNo ");
            strSql.Append(" where KeyID in (" + KeyIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.GrossMargin_MobileNo GetModel(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 KeyID,GrossMarginCode, MobileNoPattern, PatternStart, PatternLen from GrossMargin_MobileNo ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            Edge.SVA.Model.GrossMargin_MobileNo model = new Edge.SVA.Model.GrossMargin_MobileNo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["KeyID"] != null && ds.Tables[0].Rows[0]["KeyID"].ToString() != "")
                {
                    model.KeyID = int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["GrossMarginCode"] != null && ds.Tables[0].Rows[0]["GrossMarginCode"].ToString() != "")
                {
                    model.GrossMarginCode = ds.Tables[0].Rows[0]["GrossMarginCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MobileNoPattern"] != null && ds.Tables[0].Rows[0]["MobileNoPattern"].ToString() != "")
                {
                    model.MobileNoPattern = ds.Tables[0].Rows[0]["MobileNoPattern"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PatternStart"] != null && ds.Tables[0].Rows[0]["PatternStart"].ToString() != "")
                {
                    model.PatternStart = int.Parse(ds.Tables[0].Rows[0]["PatternStart"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PatternLen"] != null && ds.Tables[0].Rows[0]["PatternLen"].ToString() != "")
                {
                    model.PatternLen = int.Parse(ds.Tables[0].Rows[0]["PatternLen"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select KeyID,GrossMarginCode, MobileNoPattern, PatternStart, PatternLen ");
            strSql.Append(" FROM GrossMargin_MobileNo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" KeyID,GrossMarginCode, MobileNoPattern, PatternStart, PatternLen ");
            strSql.Append(" FROM GrossMargin_MobileNo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM GrossMargin_MobileNo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.KeyID desc");
            }
            strSql.Append(")AS Row, T.*  from GrossMargin_MobileNo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method

        public bool DeleteByGrossMarginCode(string GrossMarginCode)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from GrossMargin_MobileNo where GrossMarginCode = @GrossMarginCode");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@GrossMarginCode",SqlDbType.VarChar,64){ Value = GrossMarginCode }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}

