﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_ImportCardUID_D
	/// </summary>
	public partial class Ord_ImportCardUID_D:IOrd_ImportCardUID_D
	{
		public Ord_ImportCardUID_D()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Ord_ImportCardUID_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_ImportCardUID_D");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Ord_ImportCardUID_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_ImportCardUID_D(");
			strSql.Append("ImportCardNumber,CardGradeID,CardUID,ExpiryDate,BatchCode,Denomination)");
			strSql.Append(" values (");
			strSql.Append("@ImportCardNumber,@CardGradeID,@CardUID,@ExpiryDate,@BatchCode,@Denomination)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@CardUID", SqlDbType.VarChar,512),
					new SqlParameter("@ExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512),
					new SqlParameter("@Denomination", SqlDbType.Money,8)};
			parameters[0].Value = model.ImportCardNumber;
			parameters[1].Value = model.CardGradeID;
			parameters[2].Value = model.CardUID;
			parameters[3].Value = model.ExpiryDate;
			parameters[4].Value = model.BatchCode;
			parameters[5].Value = model.Denomination;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_ImportCardUID_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_ImportCardUID_D set ");
			strSql.Append("ImportCardNumber=@ImportCardNumber,");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("CardUID=@CardUID,");
			strSql.Append("ExpiryDate=@ExpiryDate,");
			strSql.Append("BatchCode=@BatchCode,");
			strSql.Append("Denomination=@Denomination");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@CardUID", SqlDbType.VarChar,512),
					new SqlParameter("@ExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512),
					new SqlParameter("@Denomination", SqlDbType.Money,8),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.ImportCardNumber;
			parameters[1].Value = model.CardGradeID;
			parameters[2].Value = model.CardUID;
			parameters[3].Value = model.ExpiryDate;
			parameters[4].Value = model.BatchCode;
			parameters[5].Value = model.Denomination;
			parameters[6].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportCardUID_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportCardUID_D ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportCardUID_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,ImportCardNumber,CardGradeID,CardUID,ExpiryDate,BatchCode,Denomination from Ord_ImportCardUID_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Ord_ImportCardUID_D model=new Edge.SVA.Model.Ord_ImportCardUID_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportCardUID_D DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.Ord_ImportCardUID_D model=new Edge.SVA.Model.Ord_ImportCardUID_D();
			if (row != null)
			{
				if(row["KeyID"]!=null && row["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(row["KeyID"].ToString());
				}
				if(row["ImportCardNumber"]!=null)
				{
					model.ImportCardNumber=row["ImportCardNumber"].ToString();
				}
				if(row["CardGradeID"]!=null && row["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(row["CardGradeID"].ToString());
				}
				if(row["CardUID"]!=null)
				{
					model.CardUID=row["CardUID"].ToString();
				}
				if(row["ExpiryDate"]!=null && row["ExpiryDate"].ToString()!="")
				{
					model.ExpiryDate=DateTime.Parse(row["ExpiryDate"].ToString());
				}
				if(row["BatchCode"]!=null)
				{
					model.BatchCode=row["BatchCode"].ToString();
				}
				if(row["Denomination"]!=null && row["Denomination"].ToString()!="")
				{
					model.Denomination=decimal.Parse(row["Denomination"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,ImportCardNumber,CardGradeID,CardUID,ExpiryDate,BatchCode,Denomination ");
			strSql.Append(" FROM Ord_ImportCardUID_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,ImportCardNumber,CardGradeID,CardUID,ExpiryDate,BatchCode,Denomination ");
			strSql.Append(" FROM Ord_ImportCardUID_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_ImportCardUID_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_ImportCardUID_D T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_ImportCardUID_D";
			parameters[1].Value = "KeyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

