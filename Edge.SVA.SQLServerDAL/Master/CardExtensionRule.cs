﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardExtensionRule
	/// </summary>
	public partial class CardExtensionRule:ICardExtensionRule
	{
		public CardExtensionRule()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ExtensionRuleID", "CardExtensionRule"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ExtensionRuleID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardExtensionRule");
			strSql.Append(" where ExtensionRuleID=@ExtensionRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@ExtensionRuleID", SqlDbType.Int,4)
};
			parameters[0].Value = ExtensionRuleID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.CardExtensionRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardExtensionRule(");
            strSql.Append("RuleType,CardTypeID,CardGradeID,ExtensionRuleSeqNo,MaxLimit,RuleAmount,Extension,ExtensionUnit,ExtendType,SpecifyExpiryDate, StartDate,EndDate,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@RuleType,@CardTypeID,@CardGradeID,@ExtensionRuleSeqNo,@MaxLimit,@RuleAmount,@Extension,@ExtensionUnit,@ExtendType,@SpecifyExpiryDate,@StartDate,@EndDate,@Status,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RuleType", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@ExtensionRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@MaxLimit", SqlDbType.Int,4),
					new SqlParameter("@RuleAmount", SqlDbType.Money,8),
					new SqlParameter("@Extension", SqlDbType.Int,4),
					new SqlParameter("@ExtensionUnit", SqlDbType.Int,4),
					new SqlParameter("@ExtendType", SqlDbType.Int,4),
                    new SqlParameter("@SpecifyExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.RuleType;
			parameters[1].Value = model.CardTypeID;
			parameters[2].Value = model.CardGradeID;
			parameters[3].Value = model.ExtensionRuleSeqNo;
			parameters[4].Value = model.MaxLimit;
			parameters[5].Value = model.RuleAmount;
			parameters[6].Value = model.Extension;
			parameters[7].Value = model.ExtensionUnit;
			parameters[8].Value = model.ExtendType;
            parameters[9].Value = model.SpecifyExpiryDate;
			parameters[10].Value = model.StartDate;
			parameters[11].Value = model.EndDate;
			parameters[12].Value = model.Status;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.CreatedBy;
			parameters[15].Value = model.UpdatedOn;
			parameters[16].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardExtensionRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardExtensionRule set ");
			strSql.Append("RuleType=@RuleType,");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("ExtensionRuleSeqNo=@ExtensionRuleSeqNo,");
			strSql.Append("MaxLimit=@MaxLimit,");
			strSql.Append("RuleAmount=@RuleAmount,");
			strSql.Append("Extension=@Extension,");
			strSql.Append("ExtensionUnit=@ExtensionUnit,");
			strSql.Append("ExtendType=@ExtendType,");
            strSql.Append("SpecifyExpiryDate=@SpecifyExpiryDate,");
			strSql.Append("StartDate=@StartDate,");
			strSql.Append("EndDate=@EndDate,");
			strSql.Append("Status=@Status,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where ExtensionRuleID=@ExtensionRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@RuleType", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@ExtensionRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@MaxLimit", SqlDbType.Int,4),
					new SqlParameter("@RuleAmount", SqlDbType.Money,8),
					new SqlParameter("@Extension", SqlDbType.Int,4),
					new SqlParameter("@ExtensionUnit", SqlDbType.Int,4),
					new SqlParameter("@ExtendType", SqlDbType.Int,4),
                    new SqlParameter("@SpecifyExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@ExtensionRuleID", SqlDbType.Int,4)};
			parameters[0].Value = model.RuleType;
			parameters[1].Value = model.CardTypeID;
			parameters[2].Value = model.CardGradeID;
			parameters[3].Value = model.ExtensionRuleSeqNo;
			parameters[4].Value = model.MaxLimit;
			parameters[5].Value = model.RuleAmount;
			parameters[6].Value = model.Extension;
			parameters[7].Value = model.ExtensionUnit;
			parameters[8].Value = model.ExtendType;
            parameters[9].Value = model.SpecifyExpiryDate;
			parameters[10].Value = model.StartDate;
			parameters[11].Value = model.EndDate;
			parameters[12].Value = model.Status;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.CreatedBy;
			parameters[15].Value = model.UpdatedOn;
			parameters[16].Value = model.UpdatedBy;
			parameters[17].Value = model.ExtensionRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ExtensionRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardExtensionRule ");
			strSql.Append(" where ExtensionRuleID=@ExtensionRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@ExtensionRuleID", SqlDbType.Int,4)
};
			parameters[0].Value = ExtensionRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ExtensionRuleIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardExtensionRule ");
			strSql.Append(" where ExtensionRuleID in ("+ExtensionRuleIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardExtensionRule GetModel(int ExtensionRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 ExtensionRuleID,RuleType,CardTypeID,CardGradeID,ExtensionRuleSeqNo,MaxLimit,RuleAmount,Extension,ExtensionUnit,ExtendType,SpecifyExpiryDate,StartDate,EndDate,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from CardExtensionRule ");
			strSql.Append(" where ExtensionRuleID=@ExtensionRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@ExtensionRuleID", SqlDbType.Int,4)
};
			parameters[0].Value = ExtensionRuleID;

			Edge.SVA.Model.CardExtensionRule model=new Edge.SVA.Model.CardExtensionRule();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ExtensionRuleID"]!=null && ds.Tables[0].Rows[0]["ExtensionRuleID"].ToString()!="")
				{
					model.ExtensionRuleID=int.Parse(ds.Tables[0].Rows[0]["ExtensionRuleID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RuleType"]!=null && ds.Tables[0].Rows[0]["RuleType"].ToString()!="")
				{
					model.RuleType=int.Parse(ds.Tables[0].Rows[0]["RuleType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardTypeID"]!=null && ds.Tables[0].Rows[0]["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardGradeID"]!=null && ds.Tables[0].Rows[0]["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExtensionRuleSeqNo"]!=null && ds.Tables[0].Rows[0]["ExtensionRuleSeqNo"].ToString()!="")
				{
					model.ExtensionRuleSeqNo=int.Parse(ds.Tables[0].Rows[0]["ExtensionRuleSeqNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MaxLimit"]!=null && ds.Tables[0].Rows[0]["MaxLimit"].ToString()!="")
				{
					model.MaxLimit=int.Parse(ds.Tables[0].Rows[0]["MaxLimit"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RuleAmount"]!=null && ds.Tables[0].Rows[0]["RuleAmount"].ToString()!="")
				{
					model.RuleAmount=decimal.Parse(ds.Tables[0].Rows[0]["RuleAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Extension"]!=null && ds.Tables[0].Rows[0]["Extension"].ToString()!="")
				{
					model.Extension=int.Parse(ds.Tables[0].Rows[0]["Extension"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExtensionUnit"]!=null && ds.Tables[0].Rows[0]["ExtensionUnit"].ToString()!="")
				{
					model.ExtensionUnit=int.Parse(ds.Tables[0].Rows[0]["ExtensionUnit"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExtendType"]!=null && ds.Tables[0].Rows[0]["ExtendType"].ToString()!="")
				{
					model.ExtendType=int.Parse(ds.Tables[0].Rows[0]["ExtendType"].ToString());
				}
                if (ds.Tables[0].Rows[0]["SpecifyExpiryDate"] != null && ds.Tables[0].Rows[0]["SpecifyExpiryDate"].ToString() != "")
				{
                    model.SpecifyExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["SpecifyExpiryDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartDate"]!=null && ds.Tables[0].Rows[0]["StartDate"].ToString()!="")
				{
					model.StartDate=DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndDate"]!=null && ds.Tables[0].Rows[0]["EndDate"].ToString()!="")
				{
					model.EndDate=DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ExtensionRuleID,RuleType,CardTypeID,CardGradeID,ExtensionRuleSeqNo,MaxLimit,RuleAmount,Extension,ExtensionUnit,ExtendType,StartDate,EndDate,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CardExtensionRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ExtensionRuleID,RuleType,CardTypeID,CardGradeID,ExtensionRuleSeqNo,MaxLimit,RuleAmount,Extension,ExtensionUnit,ExtendType,SpecifyExpiryDate,StartDate,EndDate,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CardExtensionRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CardExtensionRule";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardExtensionRule ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

