﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
    /// 数据访问类:WalletRule_D
	/// </summary>
    public partial class WalletRule_D : IWalletRule_D
	{
        public WalletRule_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
            return DbHelperSQL.GetMaxID("KeyID", "WalletRule_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) from WalletRule_D");
            strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.WalletRule_D model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into WalletRule_D(");
            strSql.Append("WalletRuleCode,BrandID,CardTypeID,CardGradeID,StoreID)");
			strSql.Append(" values (");
            strSql.Append("@WalletRuleCode,@BrandID,@CardTypeID,@CardGradeID,@StoreID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
                    new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4)};
            parameters[0].Value = model.WalletRuleCode;
            parameters[1].Value = model.BrandID;
            parameters[2].Value = model.CardTypeID;
            parameters[3].Value = model.CardGradeID;
            parameters[4].Value = model.StoreID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.WalletRule_D model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update WalletRule_D set ");
            strSql.Append("WalletRuleCode=@WalletRuleCode,");
            strSql.Append("BrandID=@BrandID,");
            strSql.Append("CardTypeID=@CardTypeID,");
            strSql.Append("CardGradeID=@CardGradeID,");
            strSql.Append("StoreID=@StoreID ");
            strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
                    new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
            parameters[0].Value = model.WalletRuleCode;
            parameters[1].Value = model.BrandID;
            parameters[2].Value = model.CardTypeID;
            parameters[3].Value = model.CardGradeID;
            parameters[4].Value = model.StoreID;
            parameters[5].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from WalletRule_D ");
            strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from WalletRule_D ");
            strSql.Append(" where KeyID in (" + KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.WalletRule_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 KeyID, WalletRuleCode,BrandID,CardTypeID,CardGradeID,StoreID from WalletRule_D ");
            strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            Edge.SVA.Model.WalletRule_D model = new Edge.SVA.Model.WalletRule_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["KeyID"] != null && ds.Tables[0].Rows[0]["KeyID"].ToString() != "")
				{
                    model.KeyID = int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
                if (ds.Tables[0].Rows[0]["WalletRuleCode"] != null && ds.Tables[0].Rows[0]["WalletRuleCode"].ToString() != "")
				{
                    model.WalletRuleCode = ds.Tables[0].Rows[0]["WalletRuleCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["BrandID"] != null && ds.Tables[0].Rows[0]["BrandID"].ToString() != "")
                {
                    model.BrandID = int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select KeyID, WalletRuleCode,BrandID,CardTypeID,CardGradeID,StoreID  ");
            strSql.Append(" FROM WalletRule_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" KeyID, WalletRuleCode,BrandID,CardTypeID,CardGradeID,StoreID ");
            strSql.Append(" FROM WalletRule_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) FROM WalletRule_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}


		#endregion  Method
	}
}

