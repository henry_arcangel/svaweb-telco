﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
    /// 数据访问类:ImportGM
	/// </summary>
	public partial class ImportGM:IImportGM
	{
        public ImportGM()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.ImportGM model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into ImportGM(");
			strSql.Append("SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,Export,TransactionType)");
			strSql.Append(" values (");
            strSql.Append("@SKU,@UPC,@SKUDesc,@SKUUnitAmount,@ProdCode,@BU,@GMValue,@CardTypeCode,@CardGradeCode,@NumberMask,@NumberPrefix,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@export,@transactiontype)");
			SqlParameter[] parameters = {
					new SqlParameter("@SKU", SqlDbType.VarChar,64),
                    new SqlParameter("@UPC", SqlDbType.VarChar,64),
					new SqlParameter("@SKUDesc", SqlDbType.VarChar,64),
					new SqlParameter("@SKUUnitAmount", SqlDbType.Float,8),
					new SqlParameter("@ProdCode", SqlDbType.VarChar,64),
					new SqlParameter("@BU", SqlDbType.VarChar,64),
					new SqlParameter("@GMValue", SqlDbType.Decimal,9),
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,64),
					new SqlParameter("@CardGradeCode", SqlDbType.VarChar,64),
					new SqlParameter("@NumberMask", SqlDbType.VarChar,64),
					new SqlParameter("@NumberPrefix", SqlDbType.VarChar,64),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Char,8),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,30),
                         new SqlParameter("@export", SqlDbType.VarChar,64),
        new SqlParameter("@transactiontype", SqlDbType.VarChar,64)};
			parameters[0].Value = Convert.ToString("0000000")  +  model.UPC;
			parameters[1].Value = model.UPC;
            parameters[2].Value = model.SKUDesc;
            parameters[3].Value = model.SKUUnitAmount;
            parameters[4].Value = model.ProdCode;
            parameters[5].Value = model.BU;
            parameters[6].Value = model.GMValue;
            parameters[7].Value = model.CardTypeCode;
            parameters[8].Value = model.CardGradeCode;
            parameters[9].Value = model.NumberMask;
            parameters[10].Value = model.NumberPrefix;
			parameters[11].Value = model.CreatedOn;
            parameters[12].Value = model.CreatedBy;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.UpdatedBy;
            parameters[15].Value= model.Export;
            parameters[16].Value= model.TransactionType;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.ImportGM model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update ImportGM set ");
            strSql.Append("SKUDesc=@SKUDesc,");
            strSql.Append("SKUUnitAmount=@SKUUnitAmount,");
            strSql.Append("BU=@BU,");
            strSql.Append("GMValue=@GMValue,");
            strSql.Append("CardTypeCode=@CardTypeCode,");
            strSql.Append("CardGradeCode=@CardGradeCode,");
            strSql.Append("NumberMask=@NumberMask,");
            strSql.Append("NumberPrefix=@NumberPrefix,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("TransactionType=@TransactionType,");
            strSql.Append("Export=@Export,");
            strSql.Append("ProdCode=@ProdCode");
            strSql.Append(" where UPC = @UPC ");
			SqlParameter[] parameters = {
					new SqlParameter("@SKU", SqlDbType.VarChar,164),
                    new SqlParameter("@UPC", SqlDbType.VarChar,164),
					new SqlParameter("@SKUDesc", SqlDbType.VarChar,164),
					new SqlParameter("@SKUUnitAmount", SqlDbType.Float,8),
					new SqlParameter("@BU", SqlDbType.VarChar,164),
					new SqlParameter("@GMValue", SqlDbType.Decimal,9),
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,164),
					new SqlParameter("@CardGradeCode", SqlDbType.VarChar,164),
					new SqlParameter("@NumberMask", SqlDbType.VarChar,164),
					new SqlParameter("@NumberPrefix", SqlDbType.VarChar,164),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,64),
                    new SqlParameter("@ProdCode", SqlDbType.VarChar,64),
                       new SqlParameter("@Export", SqlDbType.VarChar,64),
                       new SqlParameter("@TransactionType", SqlDbType.VarChar,614)                   
                                        };
            parameters[0].Value = model.SKU;
            parameters[1].Value = model.UPC;
            parameters[2].Value = model.SKUDesc;
            parameters[3].Value = model.SKUUnitAmount;           
            parameters[4].Value = model.BU;
            parameters[5].Value = model.GMValue;
            parameters[6].Value = model.CardTypeCode;
            parameters[7].Value = model.CardGradeCode;
            parameters[8].Value = model.NumberMask;
            parameters[9].Value = model.NumberPrefix;
            parameters[10].Value = model.UpdatedOn;
            parameters[11].Value = model.UpdatedBy;
            parameters[12].Value = model.ProdCode;
            parameters[13].Value = model.Export;
            parameters[14].Value = model.TransactionType;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ImportGM ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        public bool Delete(int SKU)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from IMPORTGM ");
            strSql.Append(" where UPC=@SKU");
            SqlParameter[] parameters = {
					new SqlParameter("@SKU", SqlDbType.Int,45)
			};
            parameters[0].Value = SKU;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


     
        public Edge.SVA.Model.ImportGM GetModelSKU(int upc)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            //removed by jay 1/22/2016
            //strSql.Append("select  top 1 SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,export,transactiontype from ImportGM ");
            strSql.Append("select  top 1 * from viewImportGM ");
            strSql.Append(" where UPC='" + upc + "'");
            SqlParameter[] parameters = {
};

            Edge.SVA.Model.ImportGM model = new Edge.SVA.Model.ImportGM();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SKU"] != null && ds.Tables[0].Rows[0]["SKU"].ToString() != "")
                {
                    model.SKU = ds.Tables[0].Rows[0]["SKU"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UPC"] != null && ds.Tables[0].Rows[0]["UPC"].ToString() != "")
                {
                    model.UPC = ds.Tables[0].Rows[0]["UPC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SKUDesc"] != null && ds.Tables[0].Rows[0]["SKUDesc"].ToString() != "")
                {
                    model.SKUDesc = ds.Tables[0].Rows[0]["SKUDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SKUUnitAmount"] != null && ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString() != "")
                {
                    model.SKUUnitAmount = decimal.Parse(ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString());
                   // model.SKUUnitAmount = Convert.ToInt32(model.SKUUnitAmount.Value.ToString("#####"));
                }
                if (ds.Tables[0].Rows[0]["ProdCode"] != null && ds.Tables[0].Rows[0]["ProdCode"].ToString() != "")
                {
                    model.ProdCode = ds.Tables[0].Rows[0]["ProdCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BU"] != null && ds.Tables[0].Rows[0]["BU"].ToString() != "")
                {
                    model.BU = ds.Tables[0].Rows[0]["BU"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GMValue"] != null && ds.Tables[0].Rows[0]["GMValue"].ToString() != "")
                {
                    model.GMValue = decimal.Parse(ds.Tables[0].Rows[0]["GMValue"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeCode"] != null && ds.Tables[0].Rows[0]["CardTypeCode"].ToString() != "")
                {
                    model.CardTypeCode = ds.Tables[0].Rows[0]["CardTypeCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardGradeCode"] != null && ds.Tables[0].Rows[0]["CardGradeCode"].ToString() != "")
                {
                    model.CardGradeCode = ds.Tables[0].Rows[0]["CardGradeCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NumberMask"] != null && ds.Tables[0].Rows[0]["NumberMask"].ToString() != "")
                {
                    model.NumberMask = ds.Tables[0].Rows[0]["NumberMask"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NumberPrefix"] != null && ds.Tables[0].Rows[0]["NumberPrefix"].ToString() != "")
                {
                    model.NumberPrefix = ds.Tables[0].Rows[0]["NumberPrefix"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
                }

                if (ds.Tables[0].Rows[0]["Export"] != null && ds.Tables[0].Rows[0]["Export"].ToString() != "")
                {
                    model.Export = ds.Tables[0].Rows[0]["Export"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TransactionType"] != null && ds.Tables[0].Rows[0]["TransactionType"].ToString() != "")
                {
                    model.TransactionType = ds.Tables[0].Rows[0]["TransactionType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardGradeName1"] != null && ds.Tables[0].Rows[0]["CardGradeName1"].ToString() != "")
                {
                    model.CardGradeName = ds.Tables[0].Rows[0]["CardGradeCode"].ToString() + " - " + ds.Tables[0].Rows[0]["CardGradeName1"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        public Edge.SVA.Model.ImportGM GetModelProd(string prodcode)
    
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select  top 1 SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,export,transactiontype from ImportGM ");

            strSql.Append(" where prodcode='" + prodcode + "'");
            SqlParameter[] parameters = {
};

            Edge.SVA.Model.ImportGM model = new Edge.SVA.Model.ImportGM();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SKU"] != null && ds.Tables[0].Rows[0]["SKU"].ToString() != "")
                {
                    model.SKU = ds.Tables[0].Rows[0]["SKU"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UPC"] != null && ds.Tables[0].Rows[0]["UPC"].ToString() != "")
                {
                    model.UPC = ds.Tables[0].Rows[0]["UPC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SKUDesc"] != null && ds.Tables[0].Rows[0]["SKUDesc"].ToString() != "")
                {
                    model.SKUDesc = ds.Tables[0].Rows[0]["SKUDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SKUUnitAmount"] != null && ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString() != "")
                {
                    model.SKUUnitAmount = decimal.Parse(ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProdCode"] != null && ds.Tables[0].Rows[0]["ProdCode"].ToString() != "")
                {
                    model.ProdCode = ds.Tables[0].Rows[0]["ProdCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BU"] != null && ds.Tables[0].Rows[0]["BU"].ToString() != "")
                {
                    model.BU = ds.Tables[0].Rows[0]["BU"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GMValue"] != null && ds.Tables[0].Rows[0]["GMValue"].ToString() != "")
                {
                    model.GMValue = decimal.Parse(ds.Tables[0].Rows[0]["GMValue"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeCode"] != null && ds.Tables[0].Rows[0]["CardTypeCode"].ToString() != "")
                {
                    model.CardTypeCode = ds.Tables[0].Rows[0]["CardTypeCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardGradeCode"] != null && ds.Tables[0].Rows[0]["CardGradeCode"].ToString() != "")
                {
                    model.CardGradeCode = ds.Tables[0].Rows[0]["CardGradeCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NumberMask"] != null && ds.Tables[0].Rows[0]["NumberMask"].ToString() != "")
                {
                    model.NumberMask = ds.Tables[0].Rows[0]["NumberMask"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NumberPrefix"] != null && ds.Tables[0].Rows[0]["NumberPrefix"].ToString() != "")
                {
                    model.NumberPrefix = ds.Tables[0].Rows[0]["NumberPrefix"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
                }

                if (ds.Tables[0].Rows[0]["Export"] != null && ds.Tables[0].Rows[0]["Export"].ToString() != "")
                {
                    model.Export = ds.Tables[0].Rows[0]["Export"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TransactionType"] != null && ds.Tables[0].Rows[0]["TransactionType"].ToString() != "")
                {
                    model.TransactionType = ds.Tables[0].Rows[0]["TransactionType"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.ImportGM GetModel(string prodcode)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            
           strSql.Append("select  top 1 SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,export,transactiontype from ImportGM ");
           
            strSql.Append(" where prodcode='" + prodcode + "'");
			SqlParameter[] parameters = {
};

            Edge.SVA.Model.ImportGM model = new Edge.SVA.Model.ImportGM();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["SKU"] != null && ds.Tables[0].Rows[0]["SKU"].ToString() != "")
				{
                    model.SKU = ds.Tables[0].Rows[0]["SKU"].ToString();
				}
                if (ds.Tables[0].Rows[0]["UPC"] != null && ds.Tables[0].Rows[0]["UPC"].ToString() != "")
				{
                    model.UPC = ds.Tables[0].Rows[0]["UPC"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SKUDesc"] != null && ds.Tables[0].Rows[0]["SKUDesc"].ToString() != "")
				{
                    model.SKUDesc = ds.Tables[0].Rows[0]["SKUDesc"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SKUUnitAmount"] != null && ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString() != "")
				{
                    model.SKUUnitAmount = decimal.Parse(ds.Tables[0].Rows[0]["SKUUnitAmount"].ToString());
				}
                if (ds.Tables[0].Rows[0]["ProdCode"] != null && ds.Tables[0].Rows[0]["ProdCode"].ToString() != "")
				{
                    model.ProdCode = ds.Tables[0].Rows[0]["ProdCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["BU"] != null && ds.Tables[0].Rows[0]["BU"].ToString() != "")
				{
                    model.BU = ds.Tables[0].Rows[0]["BU"].ToString();
				}
                if (ds.Tables[0].Rows[0]["GMValue"] != null && ds.Tables[0].Rows[0]["GMValue"].ToString() != "")
				{
                    model.GMValue = decimal.Parse(ds.Tables[0].Rows[0]["GMValue"].ToString());
				}
                if (ds.Tables[0].Rows[0]["CardTypeCode"] != null && ds.Tables[0].Rows[0]["CardTypeCode"].ToString() != "")
				{
                    model.CardTypeCode = ds.Tables[0].Rows[0]["CardTypeCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["CardGradeCode"] != null && ds.Tables[0].Rows[0]["CardGradeCode"].ToString() != "")
				{
                    model.CardGradeCode = ds.Tables[0].Rows[0]["CardGradeCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["NumberMask"] != null && ds.Tables[0].Rows[0]["NumberMask"].ToString() != "")
				{
                    model.NumberMask = ds.Tables[0].Rows[0]["NumberMask"].ToString();
				}
                if (ds.Tables[0].Rows[0]["NumberPrefix"] != null && ds.Tables[0].Rows[0]["NumberPrefix"].ToString() != "")
				{
                    model.NumberPrefix = ds.Tables[0].Rows[0]["NumberPrefix"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
				{
                    model.CreatedBy = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}

                if (ds.Tables[0].Rows[0]["Export"] != null && ds.Tables[0].Rows[0]["Export"].ToString() != "")
                {
                    model.Export = ds.Tables[0].Rows[0]["Export"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TransactionType"] != null && ds.Tables[0].Rows[0]["TransactionType"].ToString() != "")
                {
                    model.TransactionType = ds.Tables[0].Rows[0]["TransactionType"].ToString();
                }
         
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM ImportGM ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
        //public DataSet GetListView(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
        //    strSql.Append(" FROM ImportGM ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperSQL.Query(strSql.ToString());
        //}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" SKU,UPC,SKUDesc,SKUUnitAmount,ProdCode,BU,GMValue,CardTypeCode,CardGradeCode,NumberMask,NumberPrefix,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM ImportGM ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// Get record from View
		/// </summary>
        public DataSet GetListViews(int PageSize, int PageIndex, string strWhere, string filedOrder, int order)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "viewImportGM";//"ImportGM";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = order;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds"); //Changed by Roche from sp_GetRecordByPageOrder2 
        }
        /// <summary>
        /// Get record from View
        /// </summary>
        public DataSet GetListViews(int PageSize, int PageIndex, string strWhere, string filedOrder )
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "viewImportGM";//"ImportGM";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "viewImportGM";//"ImportGM";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from ImportGM ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

