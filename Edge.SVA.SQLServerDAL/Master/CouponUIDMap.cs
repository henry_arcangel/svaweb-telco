﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CouponUIDMap
	/// </summary>
	public partial class CouponUIDMap:ICouponUIDMap
	{
		public CouponUIDMap()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CouponUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CouponUIDMap");
			strSql.Append(" where CouponUID=@CouponUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CouponUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CouponUIDMap model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CouponUIDMap(");
			strSql.Append("CouponUID,ImportCouponNumber,BatchCouponID,CouponTypeID,CouponNumber,Status,CreatedOn,CreatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CouponUID,@ImportCouponNumber,@BatchCouponID,@CouponTypeID,@CouponNumber,@Status,@CreatedOn,@CreatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponUID", SqlDbType.VarChar,512),
					new SqlParameter("@ImportCouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CouponUID;
			parameters[1].Value = model.ImportCouponNumber;
			parameters[2].Value = model.BatchCouponID;
			parameters[3].Value = model.CouponTypeID;
			parameters[4].Value = model.CouponNumber;
			parameters[5].Value = model.Status;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponUIDMap model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CouponUIDMap set ");
			strSql.Append("ImportCouponNumber=@ImportCouponNumber,");
			strSql.Append("BatchCouponID=@BatchCouponID,");
			strSql.Append("CouponTypeID=@CouponTypeID,");
			strSql.Append("CouponNumber=@CouponNumber,");
			strSql.Append("Status=@Status,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy");
			strSql.Append(" where CouponUID=@CouponUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponUID", SqlDbType.VarChar,512)};
			parameters[0].Value = model.ImportCouponNumber;
			parameters[1].Value = model.BatchCouponID;
			parameters[2].Value = model.CouponTypeID;
			parameters[3].Value = model.CouponNumber;
			parameters[4].Value = model.Status;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.CouponUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CouponUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponUIDMap ");
			strSql.Append(" where CouponUID=@CouponUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CouponUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CouponUIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponUIDMap ");
			strSql.Append(" where CouponUID in ("+CouponUIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponUIDMap GetModel(string CouponUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CouponUID,ImportCouponNumber,BatchCouponID,CouponTypeID,CouponNumber,Status,CreatedOn,CreatedBy from CouponUIDMap ");
			strSql.Append(" where CouponUID=@CouponUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CouponUID;

			Edge.SVA.Model.CouponUIDMap model=new Edge.SVA.Model.CouponUIDMap();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CouponUID"]!=null && ds.Tables[0].Rows[0]["CouponUID"].ToString()!="")
				{
					model.CouponUID=ds.Tables[0].Rows[0]["CouponUID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ImportCouponNumber"]!=null && ds.Tables[0].Rows[0]["ImportCouponNumber"].ToString()!="")
				{
					model.ImportCouponNumber=ds.Tables[0].Rows[0]["ImportCouponNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BatchCouponID"]!=null && ds.Tables[0].Rows[0]["BatchCouponID"].ToString()!="")
				{
					model.BatchCouponID=int.Parse(ds.Tables[0].Rows[0]["BatchCouponID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CouponTypeID"]!=null && ds.Tables[0].Rows[0]["CouponTypeID"].ToString()!="")
				{
					model.CouponTypeID=int.Parse(ds.Tables[0].Rows[0]["CouponTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CouponNumber"]!=null && ds.Tables[0].Rows[0]["CouponNumber"].ToString()!="")
				{
					model.CouponNumber=ds.Tables[0].Rows[0]["CouponNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CouponUID,ImportCouponNumber,BatchCouponID,CouponTypeID,CouponNumber,Status,CreatedOn,CreatedBy ");
			strSql.Append(" FROM CouponUIDMap ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CouponUID,ImportCouponNumber,BatchCouponID,CouponTypeID,CouponNumber,Status,CreatedOn,CreatedBy ");
			strSql.Append(" FROM CouponUIDMap ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CouponUIDMap";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CouponUIDMap ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

