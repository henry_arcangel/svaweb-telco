﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Store_Attribute
	/// </summary>
	public partial class Store_Attribute:IStore_Attribute
	{
		public Store_Attribute()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SAID", "Store_Attribute"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SAID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Store_Attribute");
			strSql.Append(" where SAID=@SAID");
			SqlParameter[] parameters = {
					new SqlParameter("@SAID", SqlDbType.Int,4)
			};
			parameters[0].Value = SAID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Store_Attribute model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Store_Attribute(");
			strSql.Append("SACode,SADesc1,SADesc2,SADesc3,SAPic)");
			strSql.Append(" values (");
			strSql.Append("@SACode,@SADesc1,@SADesc2,@SADesc3,@SAPic)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SACode", SqlDbType.VarChar,64),
					new SqlParameter("@SADesc1", SqlDbType.VarChar,512),
					new SqlParameter("@SADesc2", SqlDbType.VarChar,512),
					new SqlParameter("@SADesc3", SqlDbType.VarChar,512),
					new SqlParameter("@SAPic", SqlDbType.VarChar,512)};
			parameters[0].Value = model.SACode;
			parameters[1].Value = model.SADesc1;
			parameters[2].Value = model.SADesc2;
			parameters[3].Value = model.SADesc3;
			parameters[4].Value = model.SAPic;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Store_Attribute model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Store_Attribute set ");
			strSql.Append("SACode=@SACode,");
			strSql.Append("SADesc1=@SADesc1,");
			strSql.Append("SADesc2=@SADesc2,");
			strSql.Append("SADesc3=@SADesc3,");
			strSql.Append("SAPic=@SAPic");
			strSql.Append(" where SAID=@SAID");
			SqlParameter[] parameters = {
					new SqlParameter("@SACode", SqlDbType.VarChar,64),
					new SqlParameter("@SADesc1", SqlDbType.VarChar,512),
					new SqlParameter("@SADesc2", SqlDbType.VarChar,512),
					new SqlParameter("@SADesc3", SqlDbType.VarChar,512),
					new SqlParameter("@SAPic", SqlDbType.VarChar,512),
					new SqlParameter("@SAID", SqlDbType.Int,4)};
			parameters[0].Value = model.SACode;
			parameters[1].Value = model.SADesc1;
			parameters[2].Value = model.SADesc2;
			parameters[3].Value = model.SADesc3;
			parameters[4].Value = model.SAPic;
			parameters[5].Value = model.SAID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SAID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Store_Attribute ");
			strSql.Append(" where SAID=@SAID");
			SqlParameter[] parameters = {
					new SqlParameter("@SAID", SqlDbType.Int,4)
			};
			parameters[0].Value = SAID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SAIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Store_Attribute ");
			strSql.Append(" where SAID in ("+SAIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Store_Attribute GetModel(int SAID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SAID,SACode,SADesc1,SADesc2,SADesc3,SAPic from Store_Attribute ");
			strSql.Append(" where SAID=@SAID");
			SqlParameter[] parameters = {
					new SqlParameter("@SAID", SqlDbType.Int,4)
			};
			parameters[0].Value = SAID;

			Edge.SVA.Model.Store_Attribute model=new Edge.SVA.Model.Store_Attribute();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Store_Attribute DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.Store_Attribute model=new Edge.SVA.Model.Store_Attribute();
			if (row != null)
			{
				if(row["SAID"]!=null && row["SAID"].ToString()!="")
				{
					model.SAID=int.Parse(row["SAID"].ToString());
				}
				if(row["SACode"]!=null)
				{
					model.SACode=row["SACode"].ToString();
				}
				if(row["SADesc1"]!=null)
				{
					model.SADesc1=row["SADesc1"].ToString();
				}
				if(row["SADesc2"]!=null)
				{
					model.SADesc2=row["SADesc2"].ToString();
				}
				if(row["SADesc3"]!=null)
				{
					model.SADesc3=row["SADesc3"].ToString();
				}
				if(row["SAPic"]!=null)
				{
					model.SAPic=row["SAPic"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SAID,SACode,SADesc1,SADesc2,SADesc3,SAPic ");
			strSql.Append(" FROM Store_Attribute ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SAID,SACode,SADesc1,SADesc2,SADesc3,SAPic ");
			strSql.Append(" FROM Store_Attribute ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Store_Attribute ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SAID desc");
			}
			strSql.Append(")AS Row, T.*  from Store_Attribute T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Store_Attribute";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Store_Attribute ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Store_Attribute";
			parameters[1].Value = "SAID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

