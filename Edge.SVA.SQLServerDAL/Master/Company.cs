﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Company
	/// </summary>
	public partial class Company:ICompany
	{
		public Company()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CompanyID", "Company"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CompanyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Company");
			strSql.Append(" where CompanyID=@CompanyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CompanyID", SqlDbType.Int,4)
};
			parameters[0].Value = CompanyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Company model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Company(");
			strSql.Append("CompanyCode,CompanyName,CompanyAddress,CompanyTelNum,CompanyFaxNum,isDefault,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CompanyCode,@CompanyName,@CompanyAddress,@CompanyTelNum,@CompanyFaxNum,@isDefault,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CompanyCode", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyName", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyFaxNum", SqlDbType.NVarChar,512),
                    new SqlParameter("@isDefault", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CompanyCode;
			parameters[1].Value = model.CompanyName;
			parameters[2].Value = model.CompanyAddress;
			parameters[3].Value = model.CompanyTelNum;
			parameters[4].Value = model.CompanyFaxNum;
            parameters[5].Value = model.isDefault;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;
			parameters[8].Value = model.UpdatedOn;
			parameters[9].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Company model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Company set ");
			strSql.Append("CompanyCode=@CompanyCode,");
			strSql.Append("CompanyName=@CompanyName,");
			strSql.Append("CompanyAddress=@CompanyAddress,");
			strSql.Append("CompanyTelNum=@CompanyTelNum,");
			strSql.Append("CompanyFaxNum=@CompanyFaxNum,");
            strSql.Append("isDefault=@isDefault,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where CompanyID=@CompanyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CompanyCode", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyName", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyTelNum", SqlDbType.NVarChar,512),
					new SqlParameter("@CompanyFaxNum", SqlDbType.NVarChar,512),
                    new SqlParameter("@isDefault", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CompanyID", SqlDbType.Int,4)};
			parameters[0].Value = model.CompanyCode;
			parameters[1].Value = model.CompanyName;
			parameters[2].Value = model.CompanyAddress;
			parameters[3].Value = model.CompanyTelNum;
			parameters[4].Value = model.CompanyFaxNum;
            parameters[5].Value = model.isDefault;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;
			parameters[8].Value = model.UpdatedOn;
			parameters[9].Value = model.UpdatedBy;
			parameters[10].Value = model.CompanyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CompanyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Company ");
			strSql.Append(" where CompanyID=@CompanyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CompanyID", SqlDbType.Int,4)
};
			parameters[0].Value = CompanyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CompanyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Company ");
			strSql.Append(" where CompanyID in ("+CompanyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Company GetModel(int CompanyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CompanyID,CompanyCode,CompanyName,CompanyAddress,CompanyTelNum,CompanyFaxNum,isDefault,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Company ");
			strSql.Append(" where CompanyID=@CompanyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CompanyID", SqlDbType.Int,4)
};
			parameters[0].Value = CompanyID;

			Edge.SVA.Model.Company model=new Edge.SVA.Model.Company();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CompanyID"]!=null && ds.Tables[0].Rows[0]["CompanyID"].ToString()!="")
				{
					model.CompanyID=int.Parse(ds.Tables[0].Rows[0]["CompanyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CompanyCode"]!=null && ds.Tables[0].Rows[0]["CompanyCode"].ToString()!="")
				{
					model.CompanyCode=ds.Tables[0].Rows[0]["CompanyCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CompanyName"]!=null && ds.Tables[0].Rows[0]["CompanyName"].ToString()!="")
				{
					model.CompanyName=ds.Tables[0].Rows[0]["CompanyName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CompanyAddress"]!=null && ds.Tables[0].Rows[0]["CompanyAddress"].ToString()!="")
				{
					model.CompanyAddress=ds.Tables[0].Rows[0]["CompanyAddress"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CompanyTelNum"]!=null && ds.Tables[0].Rows[0]["CompanyTelNum"].ToString()!="")
				{
					model.CompanyTelNum=ds.Tables[0].Rows[0]["CompanyTelNum"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CompanyFaxNum"]!=null && ds.Tables[0].Rows[0]["CompanyFaxNum"].ToString()!="")
				{
					model.CompanyFaxNum=ds.Tables[0].Rows[0]["CompanyFaxNum"].ToString();
				}
                if (ds.Tables[0].Rows[0]["isDefault"] != null && ds.Tables[0].Rows[0]["isDefault"].ToString() != "")
                {
                    model.isDefault = int.Parse(ds.Tables[0].Rows[0]["isDefault"].ToString());
                }
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CompanyID,CompanyCode,CompanyName,CompanyAddress,CompanyTelNum,CompanyFaxNum,isDefault,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Company ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CompanyID,CompanyCode,CompanyName,CompanyAddress,CompanyTelNum,CompanyFaxNum,isDefault,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Company ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Company ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CompanyID desc");
            }
            strSql.Append(")AS Row, T.*  from Company T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

		#endregion  Method
	}
}

