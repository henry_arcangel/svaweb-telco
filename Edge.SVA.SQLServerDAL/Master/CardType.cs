﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:CardType
    /// </summary>
    public partial class CardType : ICardType
    {
        public CardType()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("CardTypeID", "CardType");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CardTypeCode, int CardTypeID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from CardType");
            strSql.Append(" where CardTypeCode=@CardTypeCode and CardTypeID=@CardTypeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,64),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4)};
            parameters[0].Value = CardTypeCode;
            parameters[1].Value = CardTypeID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.CardType model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CardType(");
            strSql.Append("CardTypeCode,CardTypeName1,CardTypeName2,CardTypeName3,BrandID,CardTypeNotes,CardNumMask,CardNumPattern,CardMustHasOwner,CardVerifyMethod,CardTypeStartDate,CardTypeEndDate,CardTypeNatureID,Status,CardExtendCode,CardCheckdigit,CheckDigitModeID,CashExpiredate,PointExpiredate,CurrencyID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordRuleID,IsPhysicalCard,IsImportUIDNumber,IsConsecutiveUID,UIDToCardNumber,UIDCheckDigit,CardNumberToUID,IsDumpCard)");
            strSql.Append(" values (");
            strSql.Append("@CardTypeCode,@CardTypeName1,@CardTypeName2,@CardTypeName3,@BrandID,@CardTypeNotes,@CardNumMask,@CardNumPattern,@CardMustHasOwner,@CardVerifyMethod,@CardTypeStartDate,@CardTypeEndDate,@CardTypeNatureID,@Status,@CardExtendCode,@CardCheckdigit,@CheckDigitModeID,@CashExpiredate,@PointExpiredate,@CurrencyID,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@PasswordRuleID,@IsPhysicalCard,@IsImportUIDNumber,@IsConsecutiveUID,@UIDToCardNumber,@UIDCheckDigit,@CardNumberToUID,@IsDumpCard)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,64),
					new SqlParameter("@CardTypeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardTypeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardTypeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeNotes", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CardMustHasOwner", SqlDbType.Int,4),
					new SqlParameter("@CardVerifyMethod", SqlDbType.Int,4),
					new SqlParameter("@CardTypeStartDate", SqlDbType.DateTime),
					new SqlParameter("@CardTypeEndDate", SqlDbType.DateTime),
					new SqlParameter("@CardTypeNatureID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CardExtendCode", SqlDbType.VarChar,512),
					new SqlParameter("@CardCheckdigit", SqlDbType.Int,4),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@CashExpiredate", SqlDbType.Int,4),
					new SqlParameter("@PointExpiredate", SqlDbType.Int,4),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@IsPhysicalCard", SqlDbType.Int,4),
					new SqlParameter("@IsImportUIDNumber", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@UIDToCardNumber", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CardNumberToUID", SqlDbType.Int,4),
                    new SqlParameter("@IsDumpCard", SqlDbType.Int,4)};
            parameters[0].Value = model.CardTypeCode;
            parameters[1].Value = model.CardTypeName1;
            parameters[2].Value = model.CardTypeName2;
            parameters[3].Value = model.CardTypeName3;
            parameters[4].Value = model.BrandID;
            parameters[5].Value = model.CardTypeNotes;
            parameters[6].Value = model.CardNumMask;
            parameters[7].Value = model.CardNumPattern;
            parameters[8].Value = model.CardMustHasOwner;
            parameters[9].Value = model.CardVerifyMethod;
            parameters[10].Value = model.CardTypeStartDate;
            parameters[11].Value = model.CardTypeEndDate;
            parameters[12].Value = model.CardTypeNatureID;
            parameters[13].Value = model.Status;
            parameters[14].Value = model.CardExtendCode;
            parameters[15].Value = model.CardCheckdigit;
            parameters[16].Value = model.CheckDigitModeID;
            parameters[17].Value = model.CashExpiredate;
            parameters[18].Value = model.PointExpiredate;
            parameters[19].Value = model.CurrencyID;
            parameters[20].Value = model.CreatedOn;
            parameters[21].Value = model.UpdatedOn;
            parameters[22].Value = model.CreatedBy;
            parameters[23].Value = model.UpdatedBy;
            parameters[24].Value = model.PasswordRuleID;
            parameters[25].Value = model.IsPhysicalCard;
            parameters[26].Value = model.IsImportUIDNumber;
            parameters[27].Value = model.IsConsecutiveUID;
            parameters[28].Value = model.UIDToCardNumber;
            parameters[29].Value = model.UIDCheckDigit;
            parameters[30].Value = model.CardNumberToUID;
            parameters[31].Value = model.IsDumpCard;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.CardType model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CardType set ");
            strSql.Append("CardTypeName1=@CardTypeName1,");
            strSql.Append("CardTypeName2=@CardTypeName2,");
            strSql.Append("CardTypeName3=@CardTypeName3,");
            strSql.Append("BrandID=@BrandID,");
            strSql.Append("CardTypeNotes=@CardTypeNotes,");
            strSql.Append("CardNumMask=@CardNumMask,");
            strSql.Append("CardNumPattern=@CardNumPattern,");
            strSql.Append("CardMustHasOwner=@CardMustHasOwner,");
            strSql.Append("CardVerifyMethod=@CardVerifyMethod,");
            strSql.Append("CardTypeStartDate=@CardTypeStartDate,");
            strSql.Append("CardTypeEndDate=@CardTypeEndDate,");
            strSql.Append("CardTypeNatureID=@CardTypeNatureID,");
            strSql.Append("Status=@Status,");
            strSql.Append("CardExtendCode=@CardExtendCode,");
            strSql.Append("CardCheckdigit=@CardCheckdigit,");
            strSql.Append("CheckDigitModeID=@CheckDigitModeID,");
            strSql.Append("CashExpiredate=@CashExpiredate,");
            strSql.Append("PointExpiredate=@PointExpiredate,");
            strSql.Append("CurrencyID=@CurrencyID,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("PasswordRuleID=@PasswordRuleID,");
            strSql.Append("IsPhysicalCard=@IsPhysicalCard,");
            strSql.Append("IsImportUIDNumber=@IsImportUIDNumber,");
            strSql.Append("IsConsecutiveUID=@IsConsecutiveUID,");
            strSql.Append("UIDToCardNumber=@UIDToCardNumber,");
            strSql.Append("UIDCheckDigit=@UIDCheckDigit,");
            strSql.Append("CardNumberToUID=@CardNumberToUID,");
            strSql.Append("CardTypeCode=@CardTypeCode");
            strSql.Append("IsDumpCard=@IsDumpCard");
            strSql.Append(" where CardTypeID=@CardTypeID");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardTypeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardTypeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeNotes", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CardMustHasOwner", SqlDbType.Int,4),
					new SqlParameter("@CardVerifyMethod", SqlDbType.Int,4),
					new SqlParameter("@CardTypeStartDate", SqlDbType.DateTime),
					new SqlParameter("@CardTypeEndDate", SqlDbType.DateTime),
					new SqlParameter("@CardTypeNatureID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CardExtendCode", SqlDbType.VarChar,512),
					new SqlParameter("@CardCheckdigit", SqlDbType.Int,4),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@CashExpiredate", SqlDbType.Int,4),
					new SqlParameter("@PointExpiredate", SqlDbType.Int,4),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@IsPhysicalCard", SqlDbType.Int,4),
					new SqlParameter("@IsImportUIDNumber", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@UIDToCardNumber", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CardNumberToUID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,64),
                    new SqlParameter("@IsDumpCard", SqlDbType.Int,4)};
            parameters[0].Value = model.CardTypeName1;
            parameters[1].Value = model.CardTypeName2;
            parameters[2].Value = model.CardTypeName3;
            parameters[3].Value = model.BrandID;
            parameters[4].Value = model.CardTypeNotes;
            parameters[5].Value = model.CardNumMask;
            parameters[6].Value = model.CardNumPattern;
            parameters[7].Value = model.CardMustHasOwner;
            parameters[8].Value = model.CardVerifyMethod;
            parameters[9].Value = model.CardTypeStartDate;
            parameters[10].Value = model.CardTypeEndDate;
            parameters[11].Value = model.CardTypeNatureID;
            parameters[12].Value = model.Status;
            parameters[13].Value = model.CardExtendCode;
            parameters[14].Value = model.CardCheckdigit;
            parameters[15].Value = model.CheckDigitModeID;
            parameters[16].Value = model.CashExpiredate;
            parameters[17].Value = model.PointExpiredate;
            parameters[18].Value = model.CurrencyID;
            parameters[19].Value = model.CreatedOn;
            parameters[20].Value = model.UpdatedOn;
            parameters[21].Value = model.CreatedBy;
            parameters[22].Value = model.UpdatedBy;
            parameters[23].Value = model.PasswordRuleID;
            parameters[24].Value = model.IsPhysicalCard;
            parameters[25].Value = model.IsImportUIDNumber;
            parameters[26].Value = model.IsConsecutiveUID;
            parameters[27].Value = model.UIDToCardNumber;
            parameters[28].Value = model.UIDCheckDigit;
            parameters[29].Value = model.CardNumberToUID;
            parameters[30].Value = model.CardTypeID;
            parameters[31].Value = model.CardTypeCode;
            parameters[32].Value = model.IsDumpCard;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CardTypeID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CardType ");
            strSql.Append(" where CardTypeID=@CardTypeID");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4)
};
            parameters[0].Value = CardTypeID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CardTypeCode, int CardTypeID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CardType ");
            strSql.Append(" where CardTypeCode=@CardTypeCode and CardTypeID=@CardTypeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeCode", SqlDbType.VarChar,64),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4)};
            parameters[0].Value = CardTypeCode;
            parameters[1].Value = CardTypeID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CardTypeIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CardType ");
            strSql.Append(" where CardTypeID in (" + CardTypeIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.CardType GetModel(int CardTypeID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CardTypeID,CardTypeCode,CardTypeName1,CardTypeName2,CardTypeName3,BrandID,CardTypeNotes,CardNumMask,CardNumPattern,CardMustHasOwner,CardVerifyMethod,CardTypeStartDate,CardTypeEndDate,CardTypeNatureID,Status,CardExtendCode,CardCheckdigit,CheckDigitModeID,CashExpiredate,PointExpiredate,CurrencyID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordRuleID,IsPhysicalCard,IsImportUIDNumber,IsConsecutiveUID,UIDToCardNumber,UIDCheckDigit,CardNumberToUID,IsDumpCard from CardType ");
            strSql.Append(" where CardTypeID=@CardTypeID");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4)
};
            parameters[0].Value = CardTypeID;

            Edge.SVA.Model.CardType model = new Edge.SVA.Model.CardType();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeCode"] != null && ds.Tables[0].Rows[0]["CardTypeCode"].ToString() != "")
                {
                    model.CardTypeCode = ds.Tables[0].Rows[0]["CardTypeCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardTypeName1"] != null && ds.Tables[0].Rows[0]["CardTypeName1"].ToString() != "")
                {
                    model.CardTypeName1 = ds.Tables[0].Rows[0]["CardTypeName1"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardTypeName2"] != null && ds.Tables[0].Rows[0]["CardTypeName2"].ToString() != "")
                {
                    model.CardTypeName2 = ds.Tables[0].Rows[0]["CardTypeName2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardTypeName3"] != null && ds.Tables[0].Rows[0]["CardTypeName3"].ToString() != "")
                {
                    model.CardTypeName3 = ds.Tables[0].Rows[0]["CardTypeName3"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BrandID"] != null && ds.Tables[0].Rows[0]["BrandID"].ToString() != "")
                {
                    model.BrandID = int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeNotes"] != null && ds.Tables[0].Rows[0]["CardTypeNotes"].ToString() != "")
                {
                    model.CardTypeNotes = ds.Tables[0].Rows[0]["CardTypeNotes"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardNumMask"] != null && ds.Tables[0].Rows[0]["CardNumMask"].ToString() != "")
                {
                    model.CardNumMask = ds.Tables[0].Rows[0]["CardNumMask"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardNumPattern"] != null && ds.Tables[0].Rows[0]["CardNumPattern"].ToString() != "")
                {
                    model.CardNumPattern = ds.Tables[0].Rows[0]["CardNumPattern"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardMustHasOwner"] != null && ds.Tables[0].Rows[0]["CardMustHasOwner"].ToString() != "")
                {
                    model.CardMustHasOwner = int.Parse(ds.Tables[0].Rows[0]["CardMustHasOwner"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardVerifyMethod"] != null && ds.Tables[0].Rows[0]["CardVerifyMethod"].ToString() != "")
                {
                    model.CardVerifyMethod = int.Parse(ds.Tables[0].Rows[0]["CardVerifyMethod"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeStartDate"] != null && ds.Tables[0].Rows[0]["CardTypeStartDate"].ToString() != "")
                {
                    model.CardTypeStartDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardTypeStartDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeEndDate"] != null && ds.Tables[0].Rows[0]["CardTypeEndDate"].ToString() != "")
                {
                    model.CardTypeEndDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardTypeEndDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeNatureID"] != null && ds.Tables[0].Rows[0]["CardTypeNatureID"].ToString() != "")
                {
                    model.CardTypeNatureID = int.Parse(ds.Tables[0].Rows[0]["CardTypeNatureID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardExtendCode"] != null && ds.Tables[0].Rows[0]["CardExtendCode"].ToString() != "")
                {
                    model.CardExtendCode = ds.Tables[0].Rows[0]["CardExtendCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardCheckdigit"] != null && ds.Tables[0].Rows[0]["CardCheckdigit"].ToString() != "")
                {
                    model.CardCheckdigit = int.Parse(ds.Tables[0].Rows[0]["CardCheckdigit"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CheckDigitModeID"] != null && ds.Tables[0].Rows[0]["CheckDigitModeID"].ToString() != "")
                {
                    model.CheckDigitModeID = int.Parse(ds.Tables[0].Rows[0]["CheckDigitModeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CashExpiredate"] != null && ds.Tables[0].Rows[0]["CashExpiredate"].ToString() != "")
                {
                    model.CashExpiredate = int.Parse(ds.Tables[0].Rows[0]["CashExpiredate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PointExpiredate"] != null && ds.Tables[0].Rows[0]["PointExpiredate"].ToString() != "")
                {
                    model.PointExpiredate = int.Parse(ds.Tables[0].Rows[0]["PointExpiredate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CurrencyID"] != null && ds.Tables[0].Rows[0]["CurrencyID"].ToString() != "")
                {
                    model.CurrencyID = int.Parse(ds.Tables[0].Rows[0]["CurrencyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PasswordRuleID"] != null && ds.Tables[0].Rows[0]["PasswordRuleID"].ToString() != "")
                {
                    model.PasswordRuleID = int.Parse(ds.Tables[0].Rows[0]["PasswordRuleID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsPhysicalCard"] != null && ds.Tables[0].Rows[0]["IsPhysicalCard"].ToString() != "")
                {
                    model.IsPhysicalCard = int.Parse(ds.Tables[0].Rows[0]["IsPhysicalCard"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsImportUIDNumber"] != null && ds.Tables[0].Rows[0]["IsImportUIDNumber"].ToString() != "")
                {
                    model.IsImportUIDNumber = int.Parse(ds.Tables[0].Rows[0]["IsImportUIDNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsConsecutiveUID"] != null && ds.Tables[0].Rows[0]["IsConsecutiveUID"].ToString() != "")
                {
                    model.IsConsecutiveUID = int.Parse(ds.Tables[0].Rows[0]["IsConsecutiveUID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UIDToCardNumber"] != null && ds.Tables[0].Rows[0]["UIDToCardNumber"].ToString() != "")
                {
                    model.UIDToCardNumber = int.Parse(ds.Tables[0].Rows[0]["UIDToCardNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UIDCheckDigit"] != null && ds.Tables[0].Rows[0]["UIDCheckDigit"].ToString() != "")
                {
                    model.UIDCheckDigit = int.Parse(ds.Tables[0].Rows[0]["UIDCheckDigit"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardNumberToUID"] != null && ds.Tables[0].Rows[0]["CardNumberToUID"].ToString() != "")
                {
                    model.CardNumberToUID = int.Parse(ds.Tables[0].Rows[0]["CardNumberToUID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsDumpCard"] != null && ds.Tables[0].Rows[0]["IsDumpCard"].ToString() != "")
                {
                    model.IsDumpCard = int.Parse(ds.Tables[0].Rows[0]["IsDumpCard"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CardTypeID,CardTypeCode,CardTypeName1,CardTypeName2,CardTypeName3,BrandID,CardTypeNotes,CardNumMask,CardNumPattern,CardMustHasOwner,CardVerifyMethod,CardTypeStartDate,CardTypeEndDate,CardTypeNatureID,Status,CardExtendCode,CardCheckdigit,CheckDigitModeID,CashExpiredate,PointExpiredate,CurrencyID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordRuleID,IsPhysicalCard,IsImportUIDNumber,IsConsecutiveUID,UIDToCardNumber,UIDCheckDigit,CardNumberToUID,IsDumpCard ");
            strSql.Append(" FROM CardType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CardTypeID,CardTypeCode,CardTypeName1,CardTypeName2,CardTypeName3,BrandID,CardTypeNotes,CardNumMask,CardNumPattern,CardMustHasOwner,CardVerifyMethod,CardTypeStartDate,CardTypeEndDate,CardTypeNatureID,Status,CardExtendCode,CardCheckdigit,CheckDigitModeID,CashExpiredate,PointExpiredate,CurrencyID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordRuleID,IsPhysicalCard,IsImportUIDNumber,IsConsecutiveUID,UIDToCardNumber,UIDCheckDigit,CardNumberToUID,IsDumpCard ");
            strSql.Append(" FROM CardType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            int OrderType = 0;
            string OrderField = filedOrder;
            if (filedOrder.ToLower().EndsWith(" desc"))
            {
                OrderType = 1;
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" desc"));
            }
            else if (filedOrder.ToLower().EndsWith(" asc"))
            {
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" asc"));
            }
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CardType";
            parameters[1].Value = "*";
            parameters[2].Value = OrderField;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = OrderType;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardType ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

        #endregion  Method
    }
}

