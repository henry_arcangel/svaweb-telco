﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:UserMessageSetting_H
    /// </summary>
    public partial class UserMessageSetting_H : IUserMessageSetting_H
    {
        public UserMessageSetting_H()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string UserMessageCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from UserMessageSetting_H");
            strSql.Append(" where UserMessageCode=@UserMessageCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@UserMessageCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = UserMessageCode;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.UserMessageSetting_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into UserMessageSetting_H(");
            strSql.Append("UserMessageCode,UserMessageDesc,StartDate,EndDate,Status,SendUserID,UserMessageType,UserMessageTitle,UserMessageContent,TriggeType,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@UserMessageCode,@UserMessageDesc,@StartDate,@EndDate,@Status,@SendUserID,@UserMessageType,@UserMessageTitle,@UserMessageContent,@TriggeType,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            SqlParameter[] parameters = {
					new SqlParameter("@UserMessageCode", SqlDbType.VarChar,64),
					new SqlParameter("@UserMessageDesc", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@SendUserID", SqlDbType.Int,4),
					new SqlParameter("@UserMessageType", SqlDbType.Int,4),
					new SqlParameter("@UserMessageTitle", SqlDbType.VarChar,512),
					new SqlParameter("@UserMessageContent", SqlDbType.VarChar),
					new SqlParameter("@TriggeType", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.UserMessageCode;
            parameters[1].Value = model.UserMessageDesc;
            parameters[2].Value = model.StartDate;
            parameters[3].Value = model.EndDate;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.SendUserID;
            parameters[6].Value = model.UserMessageType;
            parameters[7].Value = model.UserMessageTitle;
            parameters[8].Value = model.UserMessageContent;
            parameters[9].Value = model.TriggeType;
            parameters[10].Value = model.CreatedOn;
            parameters[11].Value = model.CreatedBy;
            parameters[12].Value = model.UpdatedOn;
            parameters[13].Value = model.UpdatedBy;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.UserMessageSetting_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update UserMessageSetting_H set ");
            strSql.Append("UserMessageDesc=@UserMessageDesc,");
            strSql.Append("StartDate=@StartDate,");
            strSql.Append("EndDate=@EndDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("SendUserID=@SendUserID,");
            strSql.Append("UserMessageType=@UserMessageType,");
            strSql.Append("UserMessageTitle=@UserMessageTitle,");
            strSql.Append("UserMessageContent=@UserMessageContent,");
            strSql.Append("TriggeType=@TriggeType,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where UserMessageCode=@UserMessageCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@UserMessageDesc", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@SendUserID", SqlDbType.Int,4),
					new SqlParameter("@UserMessageType", SqlDbType.Int,4),
					new SqlParameter("@UserMessageTitle", SqlDbType.VarChar,512),
					new SqlParameter("@UserMessageContent", SqlDbType.VarChar),
					new SqlParameter("@TriggeType", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@UserMessageCode", SqlDbType.VarChar,64)};
            parameters[0].Value = model.UserMessageDesc;
            parameters[1].Value = model.StartDate;
            parameters[2].Value = model.EndDate;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.SendUserID;
            parameters[5].Value = model.UserMessageType;
            parameters[6].Value = model.UserMessageTitle;
            parameters[7].Value = model.UserMessageContent;
            parameters[8].Value = model.TriggeType;
            parameters[9].Value = model.CreatedOn;
            parameters[10].Value = model.CreatedBy;
            parameters[11].Value = model.UpdatedOn;
            parameters[12].Value = model.UpdatedBy;
            parameters[13].Value = model.UserMessageCode;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string UserMessageCode)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from UserMessageSetting_H ");
            strSql.Append(" where UserMessageCode=@UserMessageCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@UserMessageCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = UserMessageCode;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string UserMessageCodelist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from UserMessageSetting_H ");
            strSql.Append(" where UserMessageCode in (" + UserMessageCodelist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.UserMessageSetting_H GetModel(string UserMessageCode)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 UserMessageCode,UserMessageDesc,StartDate,EndDate,Status,SendUserID,UserMessageType,UserMessageTitle,UserMessageContent,TriggeType,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from UserMessageSetting_H ");
            strSql.Append(" where UserMessageCode=@UserMessageCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@UserMessageCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = UserMessageCode;

            Edge.SVA.Model.UserMessageSetting_H model = new Edge.SVA.Model.UserMessageSetting_H();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["UserMessageCode"] != null && ds.Tables[0].Rows[0]["UserMessageCode"].ToString() != "")
                {
                    model.UserMessageCode = ds.Tables[0].Rows[0]["UserMessageCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UserMessageDesc"] != null && ds.Tables[0].Rows[0]["UserMessageDesc"].ToString() != "")
                {
                    model.UserMessageDesc = ds.Tables[0].Rows[0]["UserMessageDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StartDate"] != null && ds.Tables[0].Rows[0]["StartDate"].ToString() != "")
                {
                    model.StartDate = DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["EndDate"] != null && ds.Tables[0].Rows[0]["EndDate"].ToString() != "")
                {
                    model.EndDate = DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SendUserID"] != null && ds.Tables[0].Rows[0]["SendUserID"].ToString() != "")
                {
                    model.SendUserID = int.Parse(ds.Tables[0].Rows[0]["SendUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UserMessageType"] != null && ds.Tables[0].Rows[0]["UserMessageType"].ToString() != "")
                {
                    model.UserMessageType = int.Parse(ds.Tables[0].Rows[0]["UserMessageType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UserMessageTitle"] != null && ds.Tables[0].Rows[0]["UserMessageTitle"].ToString() != "")
                {
                    model.UserMessageTitle = ds.Tables[0].Rows[0]["UserMessageTitle"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UserMessageContent"] != null && ds.Tables[0].Rows[0]["UserMessageContent"].ToString() != "")
                {
                    model.UserMessageContent = ds.Tables[0].Rows[0]["UserMessageContent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TriggeType"] != null && ds.Tables[0].Rows[0]["TriggeType"].ToString() != "")
                {
                    model.TriggeType = int.Parse(ds.Tables[0].Rows[0]["TriggeType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select UserMessageCode,UserMessageDesc,StartDate,EndDate,Status,SendUserID,UserMessageType,UserMessageTitle,UserMessageContent,TriggeType,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM UserMessageSetting_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" UserMessageCode,UserMessageDesc,StartDate,EndDate,Status,SendUserID,UserMessageType,UserMessageTitle,UserMessageContent,TriggeType,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM UserMessageSetting_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM UserMessageSetting_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.UserMessageCode desc");
            }
            strSql.Append(")AS Row, T.*  from UserMessageSetting_H T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "UserMessageSetting_H";
            parameters[1].Value = "UserMessageCode";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

