﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:GrossMarge
	/// </summary>
    public partial class GrossMargin : IGrossMargin
	{
        public GrossMargin()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
            return 0;
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(string GrossMarginCode)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) from GrossMargin");
            strSql.Append(" where GrossMarginCode=@GrossMarginCode");
			SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64)
};
            parameters[0].Value = GrossMarginCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
        public int Add(Edge.SVA.Model.GrossMargin model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into GrossMargin(");
            strSql.Append("GrossMarginCode,Description1,Description2,Description3,VenderType,VenderID,BuyerType,BuyerID,CardTypeID,CardGradeID,VAT,Report,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy, ProdCode)");
			strSql.Append(" values (");
            strSql.Append("@GrossMarginCode,@Description1,@Description2,@Description3,@VenderType,@VenderID,@BuyerType,@BuyerID,@CardTypeID,@CardGradeID,@VAT,@Report,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy, @ProdCode)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64),
					new SqlParameter("@Description1", SqlDbType.VarChar,512),
					new SqlParameter("@Description2", SqlDbType.VarChar,512),
					new SqlParameter("@Description3", SqlDbType.VarChar,512),
                    new SqlParameter("@VenderType", SqlDbType.Int,4),
                    new SqlParameter("@VenderID", SqlDbType.Int,4),
                    new SqlParameter("@BuyerType", SqlDbType.Int,4),
                    new SqlParameter("@BuyerID", SqlDbType.Int,4),
                    new SqlParameter("@CardTypeID", SqlDbType.Int,4),
                    new SqlParameter("@CardGradeID", SqlDbType.Int,4),
                    new SqlParameter("@VAT", SqlDbType.Decimal,9),
                    new SqlParameter("@Report", SqlDbType.VarChar,64),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
                    new SqlParameter("@ProdCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.GrossMarginCode;
			parameters[1].Value = model.Description1;
            parameters[2].Value = model.Description2;
            parameters[3].Value = model.Description3;
            parameters[4].Value = model.VenderType;
            parameters[5].Value = model.VenderID;
            parameters[6].Value = model.BuyerType;
            parameters[7].Value = model.BuyerID;
            parameters[8].Value = model.CardTypeID;
            parameters[9].Value = model.CardGradeID;
            parameters[10].Value = model.VAT;
            parameters[11].Value = model.Report;
			parameters[12].Value = model.CreatedOn;
			parameters[13].Value = model.CreatedBy;
			parameters[14].Value = model.UpdatedOn;
			parameters[15].Value = model.UpdatedBy;
            parameters[16].Value = model.ProdCode;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.GrossMargin model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update GrossMargin set ");
            strSql.Append("Description1=@Description1,");
            strSql.Append("Description2=@Description2,");
            strSql.Append("Description3=@Description3,");
            strSql.Append("VenderType=@VenderType,");
            strSql.Append("VenderID=@VenderID,");
            strSql.Append("BuyerType=@BuyerType,");
            strSql.Append("BuyerID=@BuyerID,");
            strSql.Append("CardTypeID=@CardTypeID,");
            strSql.Append("CardGradeID=@CardGradeID,");
            strSql.Append("VAT=@VAT,");
            strSql.Append("Report=@Report,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("ProdCode=@ProdCode");
            strSql.Append(" where GrossMarginCode=@GrossMarginCode");
			SqlParameter[] parameters = {
					new SqlParameter("@Description1", SqlDbType.VarChar,512),
					new SqlParameter("@Description2", SqlDbType.VarChar,512),
					new SqlParameter("@Description3", SqlDbType.VarChar,512),
                    new SqlParameter("@VenderType", SqlDbType.Int,4),
                    new SqlParameter("@VenderID", SqlDbType.Int,4),
                    new SqlParameter("@BuyerType", SqlDbType.Int,4),
                    new SqlParameter("@BuyerID", SqlDbType.Int,4),
                    new SqlParameter("@CardTypeID", SqlDbType.Int,4),
                    new SqlParameter("@CardGradeID", SqlDbType.Int,4),
                    new SqlParameter("@VAT", SqlDbType.Decimal,9),
                    new SqlParameter("@Report", SqlDbType.VarChar,64),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
                    new SqlParameter("@ProdCode", SqlDbType.VarChar,64),
                    new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64)};
            parameters[0].Value = model.Description1;
            parameters[1].Value = model.Description2;
            parameters[2].Value = model.Description3;
            parameters[3].Value = model.VenderType;
            parameters[4].Value = model.VenderID;
            parameters[5].Value = model.BuyerType;
            parameters[6].Value = model.BuyerID;
            parameters[7].Value = model.CardTypeID;
            parameters[8].Value = model.CardGradeID;
            parameters[9].Value = model.VAT;
            parameters[10].Value = model.Report;
            parameters[11].Value = model.CreatedOn;
            parameters[12].Value = model.CreatedBy;
            parameters[13].Value = model.UpdatedOn;
            parameters[14].Value = model.UpdatedBy;
            parameters[15].Value = model.ProdCode;
            parameters[16].Value = model.GrossMarginCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string GrossMarginCode)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from GrossMargin ");
            strSql.Append(" where GrossMarginCode=@GrossMarginCode");
			SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64)
};
            parameters[0].Value = GrossMarginCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
        public bool DeleteList(string GrossMarginCodelist)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from GrossMarginCode ");
            strSql.Append(" where GrossMarginCode in (" + GrossMarginCodelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.GrossMargin GetModel(string GrossMarginCode)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 GrossMarginCode,Description1,Description2,Description3,VenderType,VenderID,BuyerType,BuyerID,CardTypeID,CardGradeID,VAT,Report,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ProdCode from GrossMargin ");
            strSql.Append(" where GrossMarginCode=@GrossMarginCode");
			SqlParameter[] parameters = {
					new SqlParameter("@GrossMarginCode", SqlDbType.VarChar,64)
};
            parameters[0].Value = GrossMarginCode;

            Edge.SVA.Model.GrossMargin model = new Edge.SVA.Model.GrossMargin();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["GrossMarginCode"] != null && ds.Tables[0].Rows[0]["GrossMarginCode"].ToString() != "")
				{
                    model.GrossMarginCode = ds.Tables[0].Rows[0]["GrossMarginCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description1"] != null && ds.Tables[0].Rows[0]["Description1"].ToString() != "")
				{
                    model.Description1 = ds.Tables[0].Rows[0]["Description1"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description2"] != null && ds.Tables[0].Rows[0]["Description2"].ToString() != "")
				{
                    model.Description2 = ds.Tables[0].Rows[0]["Description2"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description3"] != null && ds.Tables[0].Rows[0]["Description3"].ToString() != "")
				{
                    model.Description3 = ds.Tables[0].Rows[0]["Description3"].ToString();
				}

                if (ds.Tables[0].Rows[0]["VenderType"] != null && ds.Tables[0].Rows[0]["VenderType"].ToString() != "")
                {
                    model.VenderType = int.Parse(ds.Tables[0].Rows[0]["VenderType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["VenderID"] != null && ds.Tables[0].Rows[0]["VenderID"].ToString() != "")
                {
                    model.VenderID = int.Parse(ds.Tables[0].Rows[0]["VenderID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuyerType"] != null && ds.Tables[0].Rows[0]["BuyerType"].ToString() != "")
                {
                    model.BuyerType = int.Parse(ds.Tables[0].Rows[0]["BuyerType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuyerID"] != null && ds.Tables[0].Rows[0]["BuyerID"].ToString() != "")
                {
                    model.BuyerID = int.Parse(ds.Tables[0].Rows[0]["BuyerID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["VAT"] != null && ds.Tables[0].Rows[0]["VAT"].ToString() != "")
                {
                    model.VAT = decimal.Parse(ds.Tables[0].Rows[0]["VAT"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Report"] != null && ds.Tables[0].Rows[0]["Report"].ToString() != "")
                {
                    model.Report = ds.Tables[0].Rows[0]["Report"].ToString();
                }
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
                if (ds.Tables[0].Rows[0]["ProdCode"] != null && ds.Tables[0].Rows[0]["ProdCode"].ToString() != "")
                {
                    model.ProdCode = ds.Tables[0].Rows[0]["ProdCode"].ToString();
                }
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select GrossMarginCode,Description1,Description2,Description3,VenderType,VenderID,BuyerType,BuyerID,CardTypeID,CardGradeID,VAT,Report,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ProdCode ");
			strSql.Append(" FROM GrossMargin ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" GrossMarginCode,Description1,Description2,Description3,VenderType,VenderID,BuyerType,BuyerID,CardTypeID,CardGradeID,VAT,Report,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ProdCode ");
			strSql.Append(" FROM GrossMargin ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM GrossMargin ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CrossMarginCode desc");
            }
            strSql.Append(")AS Row, T.*  from GrossMargin T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

		#endregion  Method
	}
}

