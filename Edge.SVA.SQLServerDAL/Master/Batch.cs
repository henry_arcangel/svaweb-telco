﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Batch
	/// </summary>
	public partial class Batch:IBatch
	{
		public Batch()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("BatchID", "Batch"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string BatchCode,int BatchID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Batch");
			strSql.Append(" where BatchCode=@BatchCode and BatchID=@BatchID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512),
					new SqlParameter("@BatchID", SqlDbType.Int,4)};
			parameters[0].Value = BatchCode;
			parameters[1].Value = BatchID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Batch model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Batch(");
			strSql.Append("BatchCode,SeqFrom,SeqTo,InitAmount,Qty,BatchType,TypeID,Active,Redeem,Expiry,Void,Abnormal,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@BatchCode,@SeqFrom,@SeqTo,@InitAmount,@Qty,@BatchType,@TypeID,@Active,@Redeem,@Expiry,@Void,@Abnormal,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512),
					new SqlParameter("@SeqFrom", SqlDbType.Int,4),
					new SqlParameter("@SeqTo", SqlDbType.Int,4),
					new SqlParameter("@InitAmount", SqlDbType.Money,8),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@BatchType", SqlDbType.Int,4),
					new SqlParameter("@TypeID", SqlDbType.NVarChar,512),
					new SqlParameter("@Active", SqlDbType.Int,4),
					new SqlParameter("@Redeem", SqlDbType.Int,4),
					new SqlParameter("@Expiry", SqlDbType.Int,4),
					new SqlParameter("@Void", SqlDbType.Int,4),
					new SqlParameter("@Abnormal", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.BatchCode;
			parameters[1].Value = model.SeqFrom;
			parameters[2].Value = model.SeqTo;
			parameters[3].Value = model.InitAmount;
			parameters[4].Value = model.Qty;
			parameters[5].Value = model.BatchType;
			parameters[6].Value = model.TypeID;
			parameters[7].Value = model.Active;
			parameters[8].Value = model.Redeem;
			parameters[9].Value = model.Expiry;
			parameters[10].Value = model.Void;
			parameters[11].Value = model.Abnormal;
			parameters[12].Value = model.CreatedOn;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.CreatedBy;
			parameters[15].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Batch model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Batch set ");
			strSql.Append("SeqFrom=@SeqFrom,");
			strSql.Append("SeqTo=@SeqTo,");
			strSql.Append("InitAmount=@InitAmount,");
			strSql.Append("Qty=@Qty,");
			strSql.Append("BatchType=@BatchType,");
			strSql.Append("TypeID=@TypeID,");
			strSql.Append("Active=@Active,");
			strSql.Append("Redeem=@Redeem,");
			strSql.Append("Expiry=@Expiry,");
			strSql.Append("Void=@Void,");
			strSql.Append("Abnormal=@Abnormal,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where BatchID=@BatchID");
			SqlParameter[] parameters = {
					new SqlParameter("@SeqFrom", SqlDbType.Int,4),
					new SqlParameter("@SeqTo", SqlDbType.Int,4),
					new SqlParameter("@InitAmount", SqlDbType.Money,8),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@BatchType", SqlDbType.Int,4),
					new SqlParameter("@TypeID", SqlDbType.NVarChar,512),
					new SqlParameter("@Active", SqlDbType.Int,4),
					new SqlParameter("@Redeem", SqlDbType.Int,4),
					new SqlParameter("@Expiry", SqlDbType.Int,4),
					new SqlParameter("@Void", SqlDbType.Int,4),
					new SqlParameter("@Abnormal", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@BatchID", SqlDbType.Int,4),
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.SeqFrom;
			parameters[1].Value = model.SeqTo;
			parameters[2].Value = model.InitAmount;
			parameters[3].Value = model.Qty;
			parameters[4].Value = model.BatchType;
			parameters[5].Value = model.TypeID;
			parameters[6].Value = model.Active;
			parameters[7].Value = model.Redeem;
			parameters[8].Value = model.Expiry;
			parameters[9].Value = model.Void;
			parameters[10].Value = model.Abnormal;
			parameters[11].Value = model.CreatedOn;
			parameters[12].Value = model.UpdatedOn;
			parameters[13].Value = model.CreatedBy;
			parameters[14].Value = model.UpdatedBy;
			parameters[15].Value = model.BatchID;
			parameters[16].Value = model.BatchCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int BatchID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Batch ");
			strSql.Append(" where BatchID=@BatchID");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchID", SqlDbType.Int,4)
};
			parameters[0].Value = BatchID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string BatchCode,int BatchID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Batch ");
			strSql.Append(" where BatchCode=@BatchCode and BatchID=@BatchID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCode", SqlDbType.VarChar,512),
					new SqlParameter("@BatchID", SqlDbType.Int,4)};
			parameters[0].Value = BatchCode;
			parameters[1].Value = BatchID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string BatchIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Batch ");
			strSql.Append(" where BatchID in ("+BatchIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Batch GetModel(int BatchID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 BatchID,BatchCode,SeqFrom,SeqTo,InitAmount,Qty,BatchType,TypeID,Active,Redeem,Expiry,Void,Abnormal,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy from Batch ");
			strSql.Append(" where BatchID=@BatchID");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchID", SqlDbType.Int,4)
};
			parameters[0].Value = BatchID;

			Edge.SVA.Model.Batch model=new Edge.SVA.Model.Batch();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["BatchID"]!=null && ds.Tables[0].Rows[0]["BatchID"].ToString()!="")
				{
					model.BatchID=int.Parse(ds.Tables[0].Rows[0]["BatchID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BatchCode"]!=null && ds.Tables[0].Rows[0]["BatchCode"].ToString()!="")
				{
					model.BatchCode=ds.Tables[0].Rows[0]["BatchCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SeqFrom"]!=null && ds.Tables[0].Rows[0]["SeqFrom"].ToString()!="")
				{
					model.SeqFrom=int.Parse(ds.Tables[0].Rows[0]["SeqFrom"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SeqTo"]!=null && ds.Tables[0].Rows[0]["SeqTo"].ToString()!="")
				{
					model.SeqTo=int.Parse(ds.Tables[0].Rows[0]["SeqTo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InitAmount"]!=null && ds.Tables[0].Rows[0]["InitAmount"].ToString()!="")
				{
					model.InitAmount=decimal.Parse(ds.Tables[0].Rows[0]["InitAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Qty"]!=null && ds.Tables[0].Rows[0]["Qty"].ToString()!="")
				{
					model.Qty=int.Parse(ds.Tables[0].Rows[0]["Qty"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BatchType"]!=null && ds.Tables[0].Rows[0]["BatchType"].ToString()!="")
				{
					model.BatchType=int.Parse(ds.Tables[0].Rows[0]["BatchType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TypeID"]!=null && ds.Tables[0].Rows[0]["TypeID"].ToString()!="")
				{
					model.TypeID=ds.Tables[0].Rows[0]["TypeID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Active"]!=null && ds.Tables[0].Rows[0]["Active"].ToString()!="")
				{
					model.Active=int.Parse(ds.Tables[0].Rows[0]["Active"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Redeem"]!=null && ds.Tables[0].Rows[0]["Redeem"].ToString()!="")
				{
					model.Redeem=int.Parse(ds.Tables[0].Rows[0]["Redeem"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Expiry"]!=null && ds.Tables[0].Rows[0]["Expiry"].ToString()!="")
				{
					model.Expiry=int.Parse(ds.Tables[0].Rows[0]["Expiry"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Void"]!=null && ds.Tables[0].Rows[0]["Void"].ToString()!="")
				{
					model.Void=int.Parse(ds.Tables[0].Rows[0]["Void"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Abnormal"]!=null && ds.Tables[0].Rows[0]["Abnormal"].ToString()!="")
				{
					model.Abnormal=int.Parse(ds.Tables[0].Rows[0]["Abnormal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select BatchID,BatchCode,SeqFrom,SeqTo,InitAmount,Qty,BatchType,TypeID,Active,Redeem,Expiry,Void,Abnormal,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM Batch ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" BatchID,BatchCode,SeqFrom,SeqTo,InitAmount,Qty,BatchType,TypeID,Active,Redeem,Expiry,Void,Abnormal,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM Batch ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			parameters[0].Value = "Batch";
			parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
		}

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Batch ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

