﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardGrade
	/// </summary>
	public partial class CardGrade:ICardGrade
	{
		public CardGrade()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CardGradeID", "CardGrade"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardGradeCode,int CardGradeID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardGrade");
			strSql.Append(" where CardGradeCode=@CardGradeCode and CardGradeID=@CardGradeID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeCode", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)			};
			parameters[0].Value = CardGradeCode;
			parameters[1].Value = CardGradeID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.CardGrade model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardGrade(");
			strSql.Append("CardGradeCode,CardTypeID,CardGradeName1,CardGradeName2,CardGradeName3,CardGradeRank,CardGradeUpdMethod,CardGradeUpdThreshold,CardGradeDiscCeiling,CardGradeMaxAmount,CardTypeInitPoints,CardTypeInitAmount,CardConsumeBasePoint,CardPointToAmountRate,CardAmountToPointRate,ForfeitAfterExpired,IsAllowStoreValue,CardPointTransfer,CardAmountTransfer,CardValidityDuration,CardValidityUnit,CardGradeLayoutFile,CardGradePicFile,CardGradeNotes,MinAmountPreAdd,MaxAmountPreAdd,ActiveResetExpiryDate,MinAmountPreTransfer,MaxAmountPreTransfer,MaxPointPreTransfer,DayMaxAmountTransfer,DayMaxPointTransfer,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CardGradeStatementFile,CardNumMask,CardNumPattern,CardCheckdigit,CheckDigitModeID,PasswordRuleID,CampaignID,MinBalanceAmount,MinBalancePoint,MinConsumeAmount,GracePeriodValue,GracePeriodUnit,ForfeitAmountAfterExpired,ForfeitPointAfterExpired,MinPointPreTransfer,IsAllowConsumptionPoint,IsConsecutiveUID,UIDToCardNumber,IsImportUIDNumber,UIDCheckDigit,CardNumberToUID,MinPointPreAdd,MaxPointPreAdd,CardGradeMaxPoint,CardGradeUpdHitPLU,HoldCouponCount,CardGradeUpdCouponTypeID,MobileProtectPeriodValue,EmailValidatedPeriodValue,NonValidatedPeriodValue,SessionTimeoutValue,LoginFailureCount,QRCodePeriodValue,AllowOfflineQRCode,QRCodePrefix,NumberOfTransDisplay,NumberOfCouponDisplay,NumberOfNewsDisplay,TrainingMode)");
			strSql.Append(" values (");
			strSql.Append("@CardGradeCode,@CardTypeID,@CardGradeName1,@CardGradeName2,@CardGradeName3,@CardGradeRank,@CardGradeUpdMethod,@CardGradeUpdThreshold,@CardGradeDiscCeiling,@CardGradeMaxAmount,@CardTypeInitPoints,@CardTypeInitAmount,@CardConsumeBasePoint,@CardPointToAmountRate,@CardAmountToPointRate,@ForfeitAfterExpired,@IsAllowStoreValue,@CardPointTransfer,@CardAmountTransfer,@CardValidityDuration,@CardValidityUnit,@CardGradeLayoutFile,@CardGradePicFile,@CardGradeNotes,@MinAmountPreAdd,@MaxAmountPreAdd,@ActiveResetExpiryDate,@MinAmountPreTransfer,@MaxAmountPreTransfer,@MaxPointPreTransfer,@DayMaxAmountTransfer,@DayMaxPointTransfer,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@CardGradeStatementFile,@CardNumMask,@CardNumPattern,@CardCheckdigit,@CheckDigitModeID,@PasswordRuleID,@CampaignID,@MinBalanceAmount,@MinBalancePoint,@MinConsumeAmount,@GracePeriodValue,@GracePeriodUnit,@ForfeitAmountAfterExpired,@ForfeitPointAfterExpired,@MinPointPreTransfer,@IsAllowConsumptionPoint,@IsConsecutiveUID,@UIDToCardNumber,@IsImportUIDNumber,@UIDCheckDigit,@CardNumberToUID,@MinPointPreAdd,@MaxPointPreAdd,@CardGradeMaxPoint,@CardGradeUpdHitPLU,@HoldCouponCount,@CardGradeUpdCouponTypeID,@MobileProtectPeriodValue,@EmailValidatedPeriodValue,@NonValidatedPeriodValue,@SessionTimeoutValue,@LoginFailureCount,@QRCodePeriodValue,@AllowOfflineQRCode,@QRCodePrefix,@NumberOfTransDisplay,@NumberOfCouponDisplay,@NumberOfNewsDisplay,@TrainingMode)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeCode", SqlDbType.NVarChar,512),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeRank", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdMethod", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdThreshold", SqlDbType.Int,4),
					new SqlParameter("@CardGradeDiscCeiling", SqlDbType.Decimal,9),
					new SqlParameter("@CardGradeMaxAmount", SqlDbType.Money,8),
					new SqlParameter("@CardTypeInitPoints", SqlDbType.Int,4),
					new SqlParameter("@CardTypeInitAmount", SqlDbType.Money,8),
					new SqlParameter("@CardConsumeBasePoint", SqlDbType.Int,4),
					new SqlParameter("@CardPointToAmountRate", SqlDbType.Decimal,9),
					new SqlParameter("@CardAmountToPointRate", SqlDbType.Decimal,9),
					new SqlParameter("@ForfeitAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@IsAllowStoreValue", SqlDbType.Int,4),
					new SqlParameter("@CardPointTransfer", SqlDbType.Int,4),
					new SqlParameter("@CardAmountTransfer", SqlDbType.Int,4),
					new SqlParameter("@CardValidityDuration", SqlDbType.Int,4),
					new SqlParameter("@CardValidityUnit", SqlDbType.Int,4),
					new SqlParameter("@CardGradeLayoutFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradePicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeNotes", SqlDbType.NVarChar,-1),
					new SqlParameter("@MinAmountPreAdd", SqlDbType.Money,8),
					new SqlParameter("@MaxAmountPreAdd", SqlDbType.Money,8),
					new SqlParameter("@ActiveResetExpiryDate", SqlDbType.Int,4),
					new SqlParameter("@MinAmountPreTransfer", SqlDbType.Money,8),
					new SqlParameter("@MaxAmountPreTransfer", SqlDbType.Money,8),
					new SqlParameter("@MaxPointPreTransfer", SqlDbType.Int,4),
					new SqlParameter("@DayMaxAmountTransfer", SqlDbType.Money,8),
					new SqlParameter("@DayMaxPointTransfer", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardGradeStatementFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CardCheckdigit", SqlDbType.Int,4),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@MinBalanceAmount", SqlDbType.Money,8),
					new SqlParameter("@MinBalancePoint", SqlDbType.Int,4),
					new SqlParameter("@MinConsumeAmount", SqlDbType.Money,8),
					new SqlParameter("@GracePeriodValue", SqlDbType.Int,4),
					new SqlParameter("@GracePeriodUnit", SqlDbType.Int,4),
					new SqlParameter("@ForfeitAmountAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@ForfeitPointAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@MinPointPreTransfer", SqlDbType.Int,4),
					new SqlParameter("@IsAllowConsumptionPoint", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@UIDToCardNumber", SqlDbType.Int,4),
					new SqlParameter("@IsImportUIDNumber", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CardNumberToUID", SqlDbType.Int,4),
					new SqlParameter("@MinPointPreAdd", SqlDbType.Int,4),
					new SqlParameter("@MaxPointPreAdd", SqlDbType.Int,4),
					new SqlParameter("@CardGradeMaxPoint", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdHitPLU", SqlDbType.VarChar,64),
					new SqlParameter("@HoldCouponCount", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdCouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@MobileProtectPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@EmailValidatedPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@NonValidatedPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@SessionTimeoutValue", SqlDbType.Int,4),
					new SqlParameter("@LoginFailureCount", SqlDbType.Int,4),
					new SqlParameter("@QRCodePeriodValue", SqlDbType.Int,4),
					new SqlParameter("@AllowOfflineQRCode", SqlDbType.Int,4),
					new SqlParameter("@QRCodePrefix", SqlDbType.VarChar,64),
					new SqlParameter("@NumberOfTransDisplay", SqlDbType.Int,4),
					new SqlParameter("@NumberOfCouponDisplay", SqlDbType.Int,4),
					new SqlParameter("@NumberOfNewsDisplay", SqlDbType.Int,4),
					new SqlParameter("@TrainingMode", SqlDbType.Int,4)};
			parameters[0].Value = model.CardGradeCode;
			parameters[1].Value = model.CardTypeID;
			parameters[2].Value = model.CardGradeName1;
			parameters[3].Value = model.CardGradeName2;
			parameters[4].Value = model.CardGradeName3;
			parameters[5].Value = model.CardGradeRank;
			parameters[6].Value = model.CardGradeUpdMethod;
			parameters[7].Value = model.CardGradeUpdThreshold;
			parameters[8].Value = model.CardGradeDiscCeiling;
			parameters[9].Value = model.CardGradeMaxAmount;
			parameters[10].Value = model.CardTypeInitPoints;
			parameters[11].Value = model.CardTypeInitAmount;
			parameters[12].Value = model.CardConsumeBasePoint;
			parameters[13].Value = model.CardPointToAmountRate;
			parameters[14].Value = model.CardAmountToPointRate;
			parameters[15].Value = model.ForfeitAfterExpired;
			parameters[16].Value = model.IsAllowStoreValue;
			parameters[17].Value = model.CardPointTransfer;
			parameters[18].Value = model.CardAmountTransfer;
			parameters[19].Value = model.CardValidityDuration;
			parameters[20].Value = model.CardValidityUnit;
			parameters[21].Value = model.CardGradeLayoutFile;
			parameters[22].Value = model.CardGradePicFile;
			parameters[23].Value = model.CardGradeNotes;
			parameters[24].Value = model.MinAmountPreAdd;
			parameters[25].Value = model.MaxAmountPreAdd;
			parameters[26].Value = model.ActiveResetExpiryDate;
			parameters[27].Value = model.MinAmountPreTransfer;
			parameters[28].Value = model.MaxAmountPreTransfer;
			parameters[29].Value = model.MaxPointPreTransfer;
			parameters[30].Value = model.DayMaxAmountTransfer;
			parameters[31].Value = model.DayMaxPointTransfer;
			parameters[32].Value = model.CreatedOn;
			parameters[33].Value = model.UpdatedOn;
			parameters[34].Value = model.CreatedBy;
			parameters[35].Value = model.UpdatedBy;
			parameters[36].Value = model.CardGradeStatementFile;
			parameters[37].Value = model.CardNumMask;
			parameters[38].Value = model.CardNumPattern;
			parameters[39].Value = model.CardCheckdigit;
			parameters[40].Value = model.CheckDigitModeID;
			parameters[41].Value = model.PasswordRuleID;
			parameters[42].Value = model.CampaignID;
			parameters[43].Value = model.MinBalanceAmount;
			parameters[44].Value = model.MinBalancePoint;
			parameters[45].Value = model.MinConsumeAmount;
			parameters[46].Value = model.GracePeriodValue;
			parameters[47].Value = model.GracePeriodUnit;
			parameters[48].Value = model.ForfeitAmountAfterExpired;
			parameters[49].Value = model.ForfeitPointAfterExpired;
			parameters[50].Value = model.MinPointPreTransfer;
			parameters[51].Value = model.IsAllowConsumptionPoint;
			parameters[52].Value = model.IsConsecutiveUID;
			parameters[53].Value = model.UIDToCardNumber;
			parameters[54].Value = model.IsImportUIDNumber;
			parameters[55].Value = model.UIDCheckDigit;
			parameters[56].Value = model.CardNumberToUID;
			parameters[57].Value = model.MinPointPreAdd;
			parameters[58].Value = model.MaxPointPreAdd;
			parameters[59].Value = model.CardGradeMaxPoint;
			parameters[60].Value = model.CardGradeUpdHitPLU;
			parameters[61].Value = model.HoldCouponCount;
			parameters[62].Value = model.CardGradeUpdCouponTypeID;
			parameters[63].Value = model.MobileProtectPeriodValue;
			parameters[64].Value = model.EmailValidatedPeriodValue;
			parameters[65].Value = model.NonValidatedPeriodValue;
			parameters[66].Value = model.SessionTimeoutValue;
			parameters[67].Value = model.LoginFailureCount;
			parameters[68].Value = model.QRCodePeriodValue;
			parameters[69].Value = model.AllowOfflineQRCode;
			parameters[70].Value = model.QRCodePrefix;
			parameters[71].Value = model.NumberOfTransDisplay;
			parameters[72].Value = model.NumberOfCouponDisplay;
			parameters[73].Value = model.NumberOfNewsDisplay;
			parameters[74].Value = model.TrainingMode;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardGrade model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardGrade set ");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CardGradeName1=@CardGradeName1,");
			strSql.Append("CardGradeName2=@CardGradeName2,");
			strSql.Append("CardGradeName3=@CardGradeName3,");
			strSql.Append("CardGradeRank=@CardGradeRank,");
			strSql.Append("CardGradeUpdMethod=@CardGradeUpdMethod,");
			strSql.Append("CardGradeUpdThreshold=@CardGradeUpdThreshold,");
			strSql.Append("CardGradeDiscCeiling=@CardGradeDiscCeiling,");
			strSql.Append("CardGradeMaxAmount=@CardGradeMaxAmount,");
			strSql.Append("CardTypeInitPoints=@CardTypeInitPoints,");
			strSql.Append("CardTypeInitAmount=@CardTypeInitAmount,");
			strSql.Append("CardConsumeBasePoint=@CardConsumeBasePoint,");
			strSql.Append("CardPointToAmountRate=@CardPointToAmountRate,");
			strSql.Append("CardAmountToPointRate=@CardAmountToPointRate,");
			strSql.Append("ForfeitAfterExpired=@ForfeitAfterExpired,");
			strSql.Append("IsAllowStoreValue=@IsAllowStoreValue,");
			strSql.Append("CardPointTransfer=@CardPointTransfer,");
			strSql.Append("CardAmountTransfer=@CardAmountTransfer,");
			strSql.Append("CardValidityDuration=@CardValidityDuration,");
			strSql.Append("CardValidityUnit=@CardValidityUnit,");
			strSql.Append("CardGradeLayoutFile=@CardGradeLayoutFile,");
			strSql.Append("CardGradePicFile=@CardGradePicFile,");
			strSql.Append("CardGradeNotes=@CardGradeNotes,");
			strSql.Append("MinAmountPreAdd=@MinAmountPreAdd,");
			strSql.Append("MaxAmountPreAdd=@MaxAmountPreAdd,");
			strSql.Append("ActiveResetExpiryDate=@ActiveResetExpiryDate,");
			strSql.Append("MinAmountPreTransfer=@MinAmountPreTransfer,");
			strSql.Append("MaxAmountPreTransfer=@MaxAmountPreTransfer,");
			strSql.Append("MaxPointPreTransfer=@MaxPointPreTransfer,");
			strSql.Append("DayMaxAmountTransfer=@DayMaxAmountTransfer,");
			strSql.Append("DayMaxPointTransfer=@DayMaxPointTransfer,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CardGradeStatementFile=@CardGradeStatementFile,");
			strSql.Append("CardNumMask=@CardNumMask,");
			strSql.Append("CardNumPattern=@CardNumPattern,");
			strSql.Append("CardCheckdigit=@CardCheckdigit,");
			strSql.Append("CheckDigitModeID=@CheckDigitModeID,");
			strSql.Append("PasswordRuleID=@PasswordRuleID,");
			strSql.Append("CampaignID=@CampaignID,");
			strSql.Append("MinBalanceAmount=@MinBalanceAmount,");
			strSql.Append("MinBalancePoint=@MinBalancePoint,");
			strSql.Append("MinConsumeAmount=@MinConsumeAmount,");
			strSql.Append("GracePeriodValue=@GracePeriodValue,");
			strSql.Append("GracePeriodUnit=@GracePeriodUnit,");
			strSql.Append("ForfeitAmountAfterExpired=@ForfeitAmountAfterExpired,");
			strSql.Append("ForfeitPointAfterExpired=@ForfeitPointAfterExpired,");
			strSql.Append("MinPointPreTransfer=@MinPointPreTransfer,");
			strSql.Append("IsAllowConsumptionPoint=@IsAllowConsumptionPoint,");
			strSql.Append("IsConsecutiveUID=@IsConsecutiveUID,");
			strSql.Append("UIDToCardNumber=@UIDToCardNumber,");
			strSql.Append("IsImportUIDNumber=@IsImportUIDNumber,");
			strSql.Append("UIDCheckDigit=@UIDCheckDigit,");
			strSql.Append("CardNumberToUID=@CardNumberToUID,");
			strSql.Append("MinPointPreAdd=@MinPointPreAdd,");
			strSql.Append("MaxPointPreAdd=@MaxPointPreAdd,");
			strSql.Append("CardGradeMaxPoint=@CardGradeMaxPoint,");
			strSql.Append("CardGradeUpdHitPLU=@CardGradeUpdHitPLU,");
			strSql.Append("HoldCouponCount=@HoldCouponCount,");
			strSql.Append("CardGradeUpdCouponTypeID=@CardGradeUpdCouponTypeID,");
			strSql.Append("MobileProtectPeriodValue=@MobileProtectPeriodValue,");
			strSql.Append("EmailValidatedPeriodValue=@EmailValidatedPeriodValue,");
			strSql.Append("NonValidatedPeriodValue=@NonValidatedPeriodValue,");
			strSql.Append("SessionTimeoutValue=@SessionTimeoutValue,");
			strSql.Append("LoginFailureCount=@LoginFailureCount,");
			strSql.Append("QRCodePeriodValue=@QRCodePeriodValue,");
			strSql.Append("AllowOfflineQRCode=@AllowOfflineQRCode,");
			strSql.Append("QRCodePrefix=@QRCodePrefix,");
			strSql.Append("NumberOfTransDisplay=@NumberOfTransDisplay,");
			strSql.Append("NumberOfCouponDisplay=@NumberOfCouponDisplay,");
			strSql.Append("NumberOfNewsDisplay=@NumberOfNewsDisplay,");
			strSql.Append("TrainingMode=@TrainingMode");
			strSql.Append(" where CardGradeID=@CardGradeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeRank", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdMethod", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdThreshold", SqlDbType.Int,4),
					new SqlParameter("@CardGradeDiscCeiling", SqlDbType.Decimal,9),
					new SqlParameter("@CardGradeMaxAmount", SqlDbType.Money,8),
					new SqlParameter("@CardTypeInitPoints", SqlDbType.Int,4),
					new SqlParameter("@CardTypeInitAmount", SqlDbType.Money,8),
					new SqlParameter("@CardConsumeBasePoint", SqlDbType.Int,4),
					new SqlParameter("@CardPointToAmountRate", SqlDbType.Decimal,9),
					new SqlParameter("@CardAmountToPointRate", SqlDbType.Decimal,9),
					new SqlParameter("@ForfeitAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@IsAllowStoreValue", SqlDbType.Int,4),
					new SqlParameter("@CardPointTransfer", SqlDbType.Int,4),
					new SqlParameter("@CardAmountTransfer", SqlDbType.Int,4),
					new SqlParameter("@CardValidityDuration", SqlDbType.Int,4),
					new SqlParameter("@CardValidityUnit", SqlDbType.Int,4),
					new SqlParameter("@CardGradeLayoutFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradePicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeNotes", SqlDbType.NVarChar,-1),
					new SqlParameter("@MinAmountPreAdd", SqlDbType.Money,8),
					new SqlParameter("@MaxAmountPreAdd", SqlDbType.Money,8),
					new SqlParameter("@ActiveResetExpiryDate", SqlDbType.Int,4),
					new SqlParameter("@MinAmountPreTransfer", SqlDbType.Money,8),
					new SqlParameter("@MaxAmountPreTransfer", SqlDbType.Money,8),
					new SqlParameter("@MaxPointPreTransfer", SqlDbType.Int,4),
					new SqlParameter("@DayMaxAmountTransfer", SqlDbType.Money,8),
					new SqlParameter("@DayMaxPointTransfer", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardGradeStatementFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CardNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CardCheckdigit", SqlDbType.Int,4),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@MinBalanceAmount", SqlDbType.Money,8),
					new SqlParameter("@MinBalancePoint", SqlDbType.Int,4),
					new SqlParameter("@MinConsumeAmount", SqlDbType.Money,8),
					new SqlParameter("@GracePeriodValue", SqlDbType.Int,4),
					new SqlParameter("@GracePeriodUnit", SqlDbType.Int,4),
					new SqlParameter("@ForfeitAmountAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@ForfeitPointAfterExpired", SqlDbType.Int,4),
					new SqlParameter("@MinPointPreTransfer", SqlDbType.Int,4),
					new SqlParameter("@IsAllowConsumptionPoint", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@UIDToCardNumber", SqlDbType.Int,4),
					new SqlParameter("@IsImportUIDNumber", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CardNumberToUID", SqlDbType.Int,4),
					new SqlParameter("@MinPointPreAdd", SqlDbType.Int,4),
					new SqlParameter("@MaxPointPreAdd", SqlDbType.Int,4),
					new SqlParameter("@CardGradeMaxPoint", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdHitPLU", SqlDbType.VarChar,64),
					new SqlParameter("@HoldCouponCount", SqlDbType.Int,4),
					new SqlParameter("@CardGradeUpdCouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@MobileProtectPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@EmailValidatedPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@NonValidatedPeriodValue", SqlDbType.Int,4),
					new SqlParameter("@SessionTimeoutValue", SqlDbType.Int,4),
					new SqlParameter("@LoginFailureCount", SqlDbType.Int,4),
					new SqlParameter("@QRCodePeriodValue", SqlDbType.Int,4),
					new SqlParameter("@AllowOfflineQRCode", SqlDbType.Int,4),
					new SqlParameter("@QRCodePrefix", SqlDbType.VarChar,64),
					new SqlParameter("@NumberOfTransDisplay", SqlDbType.Int,4),
					new SqlParameter("@NumberOfCouponDisplay", SqlDbType.Int,4),
					new SqlParameter("@NumberOfNewsDisplay", SqlDbType.Int,4),
					new SqlParameter("@TrainingMode", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeCode", SqlDbType.NVarChar,512)};
			parameters[0].Value = model.CardTypeID;
			parameters[1].Value = model.CardGradeName1;
			parameters[2].Value = model.CardGradeName2;
			parameters[3].Value = model.CardGradeName3;
			parameters[4].Value = model.CardGradeRank;
			parameters[5].Value = model.CardGradeUpdMethod;
			parameters[6].Value = model.CardGradeUpdThreshold;
			parameters[7].Value = model.CardGradeDiscCeiling;
			parameters[8].Value = model.CardGradeMaxAmount;
			parameters[9].Value = model.CardTypeInitPoints;
			parameters[10].Value = model.CardTypeInitAmount;
			parameters[11].Value = model.CardConsumeBasePoint;
			parameters[12].Value = model.CardPointToAmountRate;
			parameters[13].Value = model.CardAmountToPointRate;
			parameters[14].Value = model.ForfeitAfterExpired;
			parameters[15].Value = model.IsAllowStoreValue;
			parameters[16].Value = model.CardPointTransfer;
			parameters[17].Value = model.CardAmountTransfer;
			parameters[18].Value = model.CardValidityDuration;
			parameters[19].Value = model.CardValidityUnit;
			parameters[20].Value = model.CardGradeLayoutFile;
			parameters[21].Value = model.CardGradePicFile;
			parameters[22].Value = model.CardGradeNotes;
			parameters[23].Value = model.MinAmountPreAdd;
			parameters[24].Value = model.MaxAmountPreAdd;
			parameters[25].Value = model.ActiveResetExpiryDate;
			parameters[26].Value = model.MinAmountPreTransfer;
			parameters[27].Value = model.MaxAmountPreTransfer;
			parameters[28].Value = model.MaxPointPreTransfer;
			parameters[29].Value = model.DayMaxAmountTransfer;
			parameters[30].Value = model.DayMaxPointTransfer;
			parameters[31].Value = model.CreatedOn;
			parameters[32].Value = model.UpdatedOn;
			parameters[33].Value = model.CreatedBy;
			parameters[34].Value = model.UpdatedBy;
			parameters[35].Value = model.CardGradeStatementFile;
			parameters[36].Value = model.CardNumMask;
			parameters[37].Value = model.CardNumPattern;
			parameters[38].Value = model.CardCheckdigit;
			parameters[39].Value = model.CheckDigitModeID;
			parameters[40].Value = model.PasswordRuleID;
			parameters[41].Value = model.CampaignID;
			parameters[42].Value = model.MinBalanceAmount;
			parameters[43].Value = model.MinBalancePoint;
			parameters[44].Value = model.MinConsumeAmount;
			parameters[45].Value = model.GracePeriodValue;
			parameters[46].Value = model.GracePeriodUnit;
			parameters[47].Value = model.ForfeitAmountAfterExpired;
			parameters[48].Value = model.ForfeitPointAfterExpired;
			parameters[49].Value = model.MinPointPreTransfer;
			parameters[50].Value = model.IsAllowConsumptionPoint;
			parameters[51].Value = model.IsConsecutiveUID;
			parameters[52].Value = model.UIDToCardNumber;
			parameters[53].Value = model.IsImportUIDNumber;
			parameters[54].Value = model.UIDCheckDigit;
			parameters[55].Value = model.CardNumberToUID;
			parameters[56].Value = model.MinPointPreAdd;
			parameters[57].Value = model.MaxPointPreAdd;
			parameters[58].Value = model.CardGradeMaxPoint;
			parameters[59].Value = model.CardGradeUpdHitPLU;
			parameters[60].Value = model.HoldCouponCount;
			parameters[61].Value = model.CardGradeUpdCouponTypeID;
			parameters[62].Value = model.MobileProtectPeriodValue;
			parameters[63].Value = model.EmailValidatedPeriodValue;
			parameters[64].Value = model.NonValidatedPeriodValue;
			parameters[65].Value = model.SessionTimeoutValue;
			parameters[66].Value = model.LoginFailureCount;
			parameters[67].Value = model.QRCodePeriodValue;
			parameters[68].Value = model.AllowOfflineQRCode;
			parameters[69].Value = model.QRCodePrefix;
			parameters[70].Value = model.NumberOfTransDisplay;
			parameters[71].Value = model.NumberOfCouponDisplay;
			parameters[72].Value = model.NumberOfNewsDisplay;
			parameters[73].Value = model.TrainingMode;
			parameters[74].Value = model.CardGradeID;
			parameters[75].Value = model.CardGradeCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardGradeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardGrade ");
			strSql.Append(" where CardGradeID=@CardGradeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)
			};
			parameters[0].Value = CardGradeID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardGradeCode,int CardGradeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardGrade ");
			strSql.Append(" where CardGradeCode=@CardGradeCode and CardGradeID=@CardGradeID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeCode", SqlDbType.NVarChar,512),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)			};
			parameters[0].Value = CardGradeCode;
			parameters[1].Value = CardGradeID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardGradeIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardGrade ");
			strSql.Append(" where CardGradeID in ("+CardGradeIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardGrade GetModel(int CardGradeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CardGradeID,CardGradeCode,CardTypeID,CardGradeName1,CardGradeName2,CardGradeName3,CardGradeRank,CardGradeUpdMethod,CardGradeUpdThreshold,CardGradeDiscCeiling,CardGradeMaxAmount,CardTypeInitPoints,CardTypeInitAmount,CardConsumeBasePoint,CardPointToAmountRate,CardAmountToPointRate,ForfeitAfterExpired,IsAllowStoreValue,CardPointTransfer,CardAmountTransfer,CardValidityDuration,CardValidityUnit,CardGradeLayoutFile,CardGradePicFile,CardGradeNotes,MinAmountPreAdd,MaxAmountPreAdd,ActiveResetExpiryDate,MinAmountPreTransfer,MaxAmountPreTransfer,MaxPointPreTransfer,DayMaxAmountTransfer,DayMaxPointTransfer,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CardGradeStatementFile,CardNumMask,CardNumPattern,CardCheckdigit,CheckDigitModeID,PasswordRuleID,CampaignID,MinBalanceAmount,MinBalancePoint,MinConsumeAmount,GracePeriodValue,GracePeriodUnit,ForfeitAmountAfterExpired,ForfeitPointAfterExpired,MinPointPreTransfer,IsAllowConsumptionPoint,IsConsecutiveUID,UIDToCardNumber,IsImportUIDNumber,UIDCheckDigit,CardNumberToUID,MinPointPreAdd,MaxPointPreAdd,CardGradeMaxPoint,CardGradeUpdHitPLU,HoldCouponCount,CardGradeUpdCouponTypeID,MobileProtectPeriodValue,EmailValidatedPeriodValue,NonValidatedPeriodValue,SessionTimeoutValue,LoginFailureCount,QRCodePeriodValue,AllowOfflineQRCode,QRCodePrefix,NumberOfTransDisplay,NumberOfCouponDisplay,NumberOfNewsDisplay,TrainingMode from CardGrade ");
			strSql.Append(" where CardGradeID=@CardGradeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)
			};
			parameters[0].Value = CardGradeID;

			Edge.SVA.Model.CardGrade model=new Edge.SVA.Model.CardGrade();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardGrade DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.CardGrade model=new Edge.SVA.Model.CardGrade();
			if (row != null)
			{
				if(row["CardGradeID"]!=null && row["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(row["CardGradeID"].ToString());
				}
				if(row["CardGradeCode"]!=null)
				{
					model.CardGradeCode=row["CardGradeCode"].ToString();
				}
				if(row["CardTypeID"]!=null && row["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=int.Parse(row["CardTypeID"].ToString());
				}
				if(row["CardGradeName1"]!=null)
				{
					model.CardGradeName1=row["CardGradeName1"].ToString();
				}
				if(row["CardGradeName2"]!=null)
				{
					model.CardGradeName2=row["CardGradeName2"].ToString();
				}
				if(row["CardGradeName3"]!=null)
				{
					model.CardGradeName3=row["CardGradeName3"].ToString();
				}
				if(row["CardGradeRank"]!=null && row["CardGradeRank"].ToString()!="")
				{
					model.CardGradeRank=int.Parse(row["CardGradeRank"].ToString());
				}
				if(row["CardGradeUpdMethod"]!=null && row["CardGradeUpdMethod"].ToString()!="")
				{
					model.CardGradeUpdMethod=int.Parse(row["CardGradeUpdMethod"].ToString());
				}
				if(row["CardGradeUpdThreshold"]!=null && row["CardGradeUpdThreshold"].ToString()!="")
				{
					model.CardGradeUpdThreshold=int.Parse(row["CardGradeUpdThreshold"].ToString());
				}
				if(row["CardGradeDiscCeiling"]!=null && row["CardGradeDiscCeiling"].ToString()!="")
				{
					model.CardGradeDiscCeiling=decimal.Parse(row["CardGradeDiscCeiling"].ToString());
				}
				if(row["CardGradeMaxAmount"]!=null && row["CardGradeMaxAmount"].ToString()!="")
				{
					model.CardGradeMaxAmount=decimal.Parse(row["CardGradeMaxAmount"].ToString());
				}
				if(row["CardTypeInitPoints"]!=null && row["CardTypeInitPoints"].ToString()!="")
				{
					model.CardTypeInitPoints=int.Parse(row["CardTypeInitPoints"].ToString());
				}
				if(row["CardTypeInitAmount"]!=null && row["CardTypeInitAmount"].ToString()!="")
				{
					model.CardTypeInitAmount=decimal.Parse(row["CardTypeInitAmount"].ToString());
				}
				if(row["CardConsumeBasePoint"]!=null && row["CardConsumeBasePoint"].ToString()!="")
				{
					model.CardConsumeBasePoint=int.Parse(row["CardConsumeBasePoint"].ToString());
				}
				if(row["CardPointToAmountRate"]!=null && row["CardPointToAmountRate"].ToString()!="")
				{
					model.CardPointToAmountRate=decimal.Parse(row["CardPointToAmountRate"].ToString());
				}
				if(row["CardAmountToPointRate"]!=null && row["CardAmountToPointRate"].ToString()!="")
				{
					model.CardAmountToPointRate=decimal.Parse(row["CardAmountToPointRate"].ToString());
				}
				if(row["ForfeitAfterExpired"]!=null && row["ForfeitAfterExpired"].ToString()!="")
				{
					model.ForfeitAfterExpired=int.Parse(row["ForfeitAfterExpired"].ToString());
				}
				if(row["IsAllowStoreValue"]!=null && row["IsAllowStoreValue"].ToString()!="")
				{
					model.IsAllowStoreValue=int.Parse(row["IsAllowStoreValue"].ToString());
				}
				if(row["CardPointTransfer"]!=null && row["CardPointTransfer"].ToString()!="")
				{
					model.CardPointTransfer=int.Parse(row["CardPointTransfer"].ToString());
				}
				if(row["CardAmountTransfer"]!=null && row["CardAmountTransfer"].ToString()!="")
				{
					model.CardAmountTransfer=int.Parse(row["CardAmountTransfer"].ToString());
				}
				if(row["CardValidityDuration"]!=null && row["CardValidityDuration"].ToString()!="")
				{
					model.CardValidityDuration=int.Parse(row["CardValidityDuration"].ToString());
				}
				if(row["CardValidityUnit"]!=null && row["CardValidityUnit"].ToString()!="")
				{
					model.CardValidityUnit=int.Parse(row["CardValidityUnit"].ToString());
				}
				if(row["CardGradeLayoutFile"]!=null)
				{
					model.CardGradeLayoutFile=row["CardGradeLayoutFile"].ToString();
				}
				if(row["CardGradePicFile"]!=null)
				{
					model.CardGradePicFile=row["CardGradePicFile"].ToString();
				}
				if(row["CardGradeNotes"]!=null)
				{
					model.CardGradeNotes=row["CardGradeNotes"].ToString();
				}
				if(row["MinAmountPreAdd"]!=null && row["MinAmountPreAdd"].ToString()!="")
				{
					model.MinAmountPreAdd=decimal.Parse(row["MinAmountPreAdd"].ToString());
				}
				if(row["MaxAmountPreAdd"]!=null && row["MaxAmountPreAdd"].ToString()!="")
				{
					model.MaxAmountPreAdd=decimal.Parse(row["MaxAmountPreAdd"].ToString());
				}
				if(row["ActiveResetExpiryDate"]!=null && row["ActiveResetExpiryDate"].ToString()!="")
				{
					model.ActiveResetExpiryDate=int.Parse(row["ActiveResetExpiryDate"].ToString());
				}
				if(row["MinAmountPreTransfer"]!=null && row["MinAmountPreTransfer"].ToString()!="")
				{
					model.MinAmountPreTransfer=decimal.Parse(row["MinAmountPreTransfer"].ToString());
				}
				if(row["MaxAmountPreTransfer"]!=null && row["MaxAmountPreTransfer"].ToString()!="")
				{
					model.MaxAmountPreTransfer=decimal.Parse(row["MaxAmountPreTransfer"].ToString());
				}
				if(row["MaxPointPreTransfer"]!=null && row["MaxPointPreTransfer"].ToString()!="")
				{
					model.MaxPointPreTransfer=int.Parse(row["MaxPointPreTransfer"].ToString());
				}
				if(row["DayMaxAmountTransfer"]!=null && row["DayMaxAmountTransfer"].ToString()!="")
				{
					model.DayMaxAmountTransfer=decimal.Parse(row["DayMaxAmountTransfer"].ToString());
				}
				if(row["DayMaxPointTransfer"]!=null && row["DayMaxPointTransfer"].ToString()!="")
				{
					model.DayMaxPointTransfer=int.Parse(row["DayMaxPointTransfer"].ToString());
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["CardGradeStatementFile"]!=null)
				{
					model.CardGradeStatementFile=row["CardGradeStatementFile"].ToString();
				}
				if(row["CardNumMask"]!=null)
				{
					model.CardNumMask=row["CardNumMask"].ToString();
				}
				if(row["CardNumPattern"]!=null)
				{
					model.CardNumPattern=row["CardNumPattern"].ToString();
				}
				if(row["CardCheckdigit"]!=null && row["CardCheckdigit"].ToString()!="")
				{
					model.CardCheckdigit=int.Parse(row["CardCheckdigit"].ToString());
				}
				if(row["CheckDigitModeID"]!=null && row["CheckDigitModeID"].ToString()!="")
				{
					model.CheckDigitModeID=int.Parse(row["CheckDigitModeID"].ToString());
				}
				if(row["PasswordRuleID"]!=null && row["PasswordRuleID"].ToString()!="")
				{
					model.PasswordRuleID=int.Parse(row["PasswordRuleID"].ToString());
				}
				if(row["CampaignID"]!=null && row["CampaignID"].ToString()!="")
				{
					model.CampaignID=int.Parse(row["CampaignID"].ToString());
				}
				if(row["MinBalanceAmount"]!=null && row["MinBalanceAmount"].ToString()!="")
				{
					model.MinBalanceAmount=decimal.Parse(row["MinBalanceAmount"].ToString());
				}
				if(row["MinBalancePoint"]!=null && row["MinBalancePoint"].ToString()!="")
				{
					model.MinBalancePoint=int.Parse(row["MinBalancePoint"].ToString());
				}
				if(row["MinConsumeAmount"]!=null && row["MinConsumeAmount"].ToString()!="")
				{
					model.MinConsumeAmount=decimal.Parse(row["MinConsumeAmount"].ToString());
				}
				if(row["GracePeriodValue"]!=null && row["GracePeriodValue"].ToString()!="")
				{
					model.GracePeriodValue=int.Parse(row["GracePeriodValue"].ToString());
				}
				if(row["GracePeriodUnit"]!=null && row["GracePeriodUnit"].ToString()!="")
				{
					model.GracePeriodUnit=int.Parse(row["GracePeriodUnit"].ToString());
				}
				if(row["ForfeitAmountAfterExpired"]!=null && row["ForfeitAmountAfterExpired"].ToString()!="")
				{
					model.ForfeitAmountAfterExpired=int.Parse(row["ForfeitAmountAfterExpired"].ToString());
				}
				if(row["ForfeitPointAfterExpired"]!=null && row["ForfeitPointAfterExpired"].ToString()!="")
				{
					model.ForfeitPointAfterExpired=int.Parse(row["ForfeitPointAfterExpired"].ToString());
				}
				if(row["MinPointPreTransfer"]!=null && row["MinPointPreTransfer"].ToString()!="")
				{
					model.MinPointPreTransfer=int.Parse(row["MinPointPreTransfer"].ToString());
				}
				if(row["IsAllowConsumptionPoint"]!=null && row["IsAllowConsumptionPoint"].ToString()!="")
				{
					model.IsAllowConsumptionPoint=int.Parse(row["IsAllowConsumptionPoint"].ToString());
				}
				if(row["IsConsecutiveUID"]!=null && row["IsConsecutiveUID"].ToString()!="")
				{
					model.IsConsecutiveUID=int.Parse(row["IsConsecutiveUID"].ToString());
				}
				if(row["UIDToCardNumber"]!=null && row["UIDToCardNumber"].ToString()!="")
				{
					model.UIDToCardNumber=int.Parse(row["UIDToCardNumber"].ToString());
				}
				if(row["IsImportUIDNumber"]!=null && row["IsImportUIDNumber"].ToString()!="")
				{
					model.IsImportUIDNumber=int.Parse(row["IsImportUIDNumber"].ToString());
				}
				if(row["UIDCheckDigit"]!=null && row["UIDCheckDigit"].ToString()!="")
				{
					model.UIDCheckDigit=int.Parse(row["UIDCheckDigit"].ToString());
				}
				if(row["CardNumberToUID"]!=null && row["CardNumberToUID"].ToString()!="")
				{
					model.CardNumberToUID=int.Parse(row["CardNumberToUID"].ToString());
				}
				if(row["MinPointPreAdd"]!=null && row["MinPointPreAdd"].ToString()!="")
				{
					model.MinPointPreAdd=int.Parse(row["MinPointPreAdd"].ToString());
				}
				if(row["MaxPointPreAdd"]!=null && row["MaxPointPreAdd"].ToString()!="")
				{
					model.MaxPointPreAdd=int.Parse(row["MaxPointPreAdd"].ToString());
				}
				if(row["CardGradeMaxPoint"]!=null && row["CardGradeMaxPoint"].ToString()!="")
				{
					model.CardGradeMaxPoint=int.Parse(row["CardGradeMaxPoint"].ToString());
				}
				if(row["CardGradeUpdHitPLU"]!=null)
				{
					model.CardGradeUpdHitPLU=row["CardGradeUpdHitPLU"].ToString();
				}
				if(row["HoldCouponCount"]!=null && row["HoldCouponCount"].ToString()!="")
				{
					model.HoldCouponCount=int.Parse(row["HoldCouponCount"].ToString());
				}
				if(row["CardGradeUpdCouponTypeID"]!=null && row["CardGradeUpdCouponTypeID"].ToString()!="")
				{
					model.CardGradeUpdCouponTypeID=int.Parse(row["CardGradeUpdCouponTypeID"].ToString());
				}
				if(row["MobileProtectPeriodValue"]!=null && row["MobileProtectPeriodValue"].ToString()!="")
				{
					model.MobileProtectPeriodValue=int.Parse(row["MobileProtectPeriodValue"].ToString());
				}
				if(row["EmailValidatedPeriodValue"]!=null && row["EmailValidatedPeriodValue"].ToString()!="")
				{
					model.EmailValidatedPeriodValue=int.Parse(row["EmailValidatedPeriodValue"].ToString());
				}
				if(row["NonValidatedPeriodValue"]!=null && row["NonValidatedPeriodValue"].ToString()!="")
				{
					model.NonValidatedPeriodValue=int.Parse(row["NonValidatedPeriodValue"].ToString());
				}
				if(row["SessionTimeoutValue"]!=null && row["SessionTimeoutValue"].ToString()!="")
				{
					model.SessionTimeoutValue=int.Parse(row["SessionTimeoutValue"].ToString());
				}
				if(row["LoginFailureCount"]!=null && row["LoginFailureCount"].ToString()!="")
				{
					model.LoginFailureCount=int.Parse(row["LoginFailureCount"].ToString());
				}
				if(row["QRCodePeriodValue"]!=null && row["QRCodePeriodValue"].ToString()!="")
				{
					model.QRCodePeriodValue=int.Parse(row["QRCodePeriodValue"].ToString());
				}
				if(row["AllowOfflineQRCode"]!=null && row["AllowOfflineQRCode"].ToString()!="")
				{
					model.AllowOfflineQRCode=int.Parse(row["AllowOfflineQRCode"].ToString());
				}
				if(row["QRCodePrefix"]!=null)
				{
					model.QRCodePrefix=row["QRCodePrefix"].ToString();
				}
				if(row["NumberOfTransDisplay"]!=null && row["NumberOfTransDisplay"].ToString()!="")
				{
					model.NumberOfTransDisplay=int.Parse(row["NumberOfTransDisplay"].ToString());
				}
				if(row["NumberOfCouponDisplay"]!=null && row["NumberOfCouponDisplay"].ToString()!="")
				{
					model.NumberOfCouponDisplay=int.Parse(row["NumberOfCouponDisplay"].ToString());
				}
				if(row["NumberOfNewsDisplay"]!=null && row["NumberOfNewsDisplay"].ToString()!="")
				{
					model.NumberOfNewsDisplay=int.Parse(row["NumberOfNewsDisplay"].ToString());
				}
				if(row["TrainingMode"]!=null && row["TrainingMode"].ToString()!="")
				{
					model.TrainingMode=int.Parse(row["TrainingMode"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CardGradeID,CardGradeCode,CardTypeID,CardGradeName1,CardGradeName2,CardGradeName3,CardGradeRank,CardGradeUpdMethod,CardGradeUpdThreshold,CardGradeDiscCeiling,CardGradeMaxAmount,CardTypeInitPoints,CardTypeInitAmount,CardConsumeBasePoint,CardPointToAmountRate,CardAmountToPointRate,ForfeitAfterExpired,IsAllowStoreValue,CardPointTransfer,CardAmountTransfer,CardValidityDuration,CardValidityUnit,CardGradeLayoutFile,CardGradePicFile,CardGradeNotes,MinAmountPreAdd,MaxAmountPreAdd,ActiveResetExpiryDate,MinAmountPreTransfer,MaxAmountPreTransfer,MaxPointPreTransfer,DayMaxAmountTransfer,DayMaxPointTransfer,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CardGradeStatementFile,CardNumMask,CardNumPattern,CardCheckdigit,CheckDigitModeID,PasswordRuleID,CampaignID,MinBalanceAmount,MinBalancePoint,MinConsumeAmount,GracePeriodValue,GracePeriodUnit,ForfeitAmountAfterExpired,ForfeitPointAfterExpired,MinPointPreTransfer,IsAllowConsumptionPoint,IsConsecutiveUID,UIDToCardNumber,IsImportUIDNumber,UIDCheckDigit,CardNumberToUID,MinPointPreAdd,MaxPointPreAdd,CardGradeMaxPoint,CardGradeUpdHitPLU,HoldCouponCount,CardGradeUpdCouponTypeID,MobileProtectPeriodValue,EmailValidatedPeriodValue,NonValidatedPeriodValue,SessionTimeoutValue,LoginFailureCount,QRCodePeriodValue,AllowOfflineQRCode,QRCodePrefix,NumberOfTransDisplay,NumberOfCouponDisplay,NumberOfNewsDisplay,TrainingMode ");
			strSql.Append(" FROM CardGrade ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// Get list from ViewCardGrade Added By jay 1/21/2016
        /// </summary>
        public DataSet GetListView(string strWhere)

        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CardGradeID,CardGradeCode,CardTypeID,CardGradeName1,CardGradeName2,CardGradeName3,CardGradeRank,CardGradeUpdMethod,CardGradeUpdThreshold,CardGradeDiscCeiling,CardGradeMaxAmount,CardTypeInitPoints,CardTypeInitAmount,CardConsumeBasePoint,CardPointToAmountRate,CardAmountToPointRate,ForfeitAfterExpired,IsAllowStoreValue,CardPointTransfer,CardAmountTransfer,CardValidityDuration,CardValidityUnit,CardGradeLayoutFile,CardGradePicFile,CardGradeNotes,MinAmountPreAdd,MaxAmountPreAdd,ActiveResetExpiryDate,MinAmountPreTransfer,MaxAmountPreTransfer,MaxPointPreTransfer,DayMaxAmountTransfer,DayMaxPointTransfer,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CardGradeStatementFile,CardNumMask,CardNumPattern,CardCheckdigit,CheckDigitModeID,PasswordRuleID,CampaignID,MinBalanceAmount,MinBalancePoint,MinConsumeAmount,GracePeriodValue,GracePeriodUnit,ForfeitAmountAfterExpired,ForfeitPointAfterExpired,MinPointPreTransfer,IsAllowConsumptionPoint,IsConsecutiveUID,UIDToCardNumber,IsImportUIDNumber,UIDCheckDigit,CardNumberToUID,MinPointPreAdd,MaxPointPreAdd,CardGradeMaxPoint,CardGradeUpdHitPLU,HoldCouponCount,CardGradeUpdCouponTypeID,MobileProtectPeriodValue,EmailValidatedPeriodValue,NonValidatedPeriodValue,SessionTimeoutValue,LoginFailureCount,QRCodePeriodValue,AllowOfflineQRCode,QRCodePrefix,NumberOfTransDisplay,NumberOfCouponDisplay,NumberOfNewsDisplay,TrainingMode ");
            strSql.Append(" FROM viewCardGrade ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CardGradeID,CardGradeCode,CardTypeID,CardGradeName1,CardGradeName2,CardGradeName3,CardGradeRank,CardGradeUpdMethod,CardGradeUpdThreshold,CardGradeDiscCeiling,CardGradeMaxAmount,CardTypeInitPoints,CardTypeInitAmount,CardConsumeBasePoint,CardPointToAmountRate,CardAmountToPointRate,ForfeitAfterExpired,IsAllowStoreValue,CardPointTransfer,CardAmountTransfer,CardValidityDuration,CardValidityUnit,CardGradeLayoutFile,CardGradePicFile,CardGradeNotes,MinAmountPreAdd,MaxAmountPreAdd,ActiveResetExpiryDate,MinAmountPreTransfer,MaxAmountPreTransfer,MaxPointPreTransfer,DayMaxAmountTransfer,DayMaxPointTransfer,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CardGradeStatementFile,CardNumMask,CardNumPattern,CardCheckdigit,CheckDigitModeID,PasswordRuleID,CampaignID,MinBalanceAmount,MinBalancePoint,MinConsumeAmount,GracePeriodValue,GracePeriodUnit,ForfeitAmountAfterExpired,ForfeitPointAfterExpired,MinPointPreTransfer,IsAllowConsumptionPoint,IsConsecutiveUID,UIDToCardNumber,IsImportUIDNumber,UIDCheckDigit,CardNumberToUID,MinPointPreAdd,MaxPointPreAdd,CardGradeMaxPoint,CardGradeUpdHitPLU,HoldCouponCount,CardGradeUpdCouponTypeID,MobileProtectPeriodValue,EmailValidatedPeriodValue,NonValidatedPeriodValue,SessionTimeoutValue,LoginFailureCount,QRCodePeriodValue,AllowOfflineQRCode,QRCodePrefix,NumberOfTransDisplay,NumberOfCouponDisplay,NumberOfNewsDisplay,TrainingMode ");
			strSql.Append(" FROM CardGrade ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM CardGrade ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CardGradeID desc");
			}
			strSql.Append(")AS Row, T.*  from CardGrade T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            int OrderType = 0;
            string OrderField = filedOrder;
            if (filedOrder.ToLower().EndsWith(" desc"))
            {
                OrderType = 1;
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" desc"));
            }
            else if (filedOrder.ToLower().EndsWith(" asc"))
            {
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" asc"));
            }
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CardGrade";
            parameters[1].Value = "*";
            parameters[2].Value = OrderField;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = OrderType;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardGrade ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "CardGrade";
			parameters[1].Value = "CardGradeID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

