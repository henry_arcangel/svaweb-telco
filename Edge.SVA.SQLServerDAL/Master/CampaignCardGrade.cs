﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:CampaignCardGrade
    /// </summary>
    public partial class CampaignCardGrade : ICampaignCardGrade
    {
        public CampaignCardGrade()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("CampaignID", "CampaignCardGrade");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CampaignID, int CardGradeID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from CampaignCardGrade");
            strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = CampaignID;
            parameters[1].Value = CardGradeID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.CampaignCardGrade model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CampaignCardGrade(");
            strSql.Append("CampaignID,CardGradeID)");
            strSql.Append(" values (");
            strSql.Append("@CampaignID,@CardGradeID)");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CardGradeID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.CampaignCardGrade model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CampaignCardGrade set ");

            strSql.Append("CampaignID=@CampaignID,");
            strSql.Append("CardGradeID=@CardGradeID");
            strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CardGradeID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CampaignID, int CardGradeID)
        {
            if (CardGradeID == 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("delete from CampaignCardGrade ");
                strSql.Append(" where CampaignID=@CampaignID ");
                SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4)};
                parameters[0].Value = CampaignID;

                int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("delete from CampaignCardGrade ");
                strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
                SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
                parameters[0].Value = CampaignID;
                parameters[1].Value = CardGradeID;

                int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.CampaignCardGrade GetModel(int CampaignID, int CardGradeID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CampaignID,CardGradeID from CampaignCardGrade ");
            strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = CampaignID;
            parameters[1].Value = CardGradeID;

            Edge.SVA.Model.CampaignCardGrade model = new Edge.SVA.Model.CampaignCardGrade();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CampaignID"] != null && ds.Tables[0].Rows[0]["CampaignID"].ToString() != "")
                {
                    model.CampaignID = int.Parse(ds.Tables[0].Rows[0]["CampaignID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CampaignID,CardGradeID ");
            strSql.Append(" FROM CampaignCardGrade ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CampaignID,CardGradeID ");
            strSql.Append(" FROM CampaignCardGrade ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CampaignCardGrade";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CampaignCardGrade ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

        #endregion  Method
    }
}

