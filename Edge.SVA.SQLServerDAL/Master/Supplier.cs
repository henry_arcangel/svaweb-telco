﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Supplier
    /// </summary>
    public partial class Supplier:ISupplier
    {
        public Supplier()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(string SupplierCode)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) from Supplier");
            strSql.Append(" where SupplierCode=@SupplierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@SupplierCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = SupplierCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Supplier model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into Supplier(");
            strSql.Append("SupplierCode,SupplierDesc1,SupplierDesc2,SupplierDesc3,SupplierAddress,OtherAddress1,OtherAddress2,SupplierEmail,Contact,ContactPhone,ContactEmail,ContactMobile,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@SupplierCode,@SupplierDesc1,@SupplierDesc2,@SupplierDesc3,@SupplierAddress,@OtherAddress1,@OtherAddress2,@SupplierEmail,@Contact,@ContactPhone,@ContactEmail,@ContactMobile,@Remark,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SupplierCode", SqlDbType.VarChar,64),
					new SqlParameter("@SupplierDesc1", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierDesc2", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierDesc3", SqlDbType.VarChar,512),
                    new SqlParameter("@SupplierAddress", SqlDbType.VarChar,512),
					new SqlParameter("@OtherAddress1", SqlDbType.VarChar,512),
                    new SqlParameter("@OtherAddress2", SqlDbType.VarChar,512),
                    new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
                    new SqlParameter("@Contact", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactPhone", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactEmail", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactMobile", SqlDbType.VarChar,512),    
                    new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.SupplierCode;
			parameters[1].Value = model.SupplierDesc1;
            parameters[2].Value = model.SupplierDesc2;
            parameters[3].Value = model.SupplierDesc3;
            parameters[4].Value = model.SupplierAddress;
            parameters[5].Value = model.OtherAddress1;
            parameters[6].Value = model.OtherAddress2;
            parameters[7].Value = model.SupplierEmail;
            parameters[8].Value = model.Contact;
            parameters[9].Value = model.ContactPhone;
            parameters[10].Value = model.ContactEmail;
            parameters[11].Value = model.ContactMobile;
            parameters[12].Value = model.Remark;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.CreatedBy;
			parameters[15].Value = model.UpdatedOn;
			parameters[16].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Supplier model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update Supplier set ");
            strSql.Append("SupplierDesc1=@SupplierDesc1,");
            strSql.Append("SupplierDesc2=@SupplierDesc2,");
            strSql.Append("SupplierDesc3=@SupplierDesc3,");
            strSql.Append("SupplierAddress=@SupplierAddress,");
            strSql.Append("OtherAddress1=@OtherAddress1,");
            strSql.Append("OtherAddress2=@OtherAddress2,");
            strSql.Append("SupplierEmail=@SupplierEmail,");
            strSql.Append("Contact=@Contact,");
            strSql.Append("ContactPhone=@ContactPhone,");
            strSql.Append("ContactEmail=@ContactEmail,");
            strSql.Append("ContactMobile=@ContactMobile,");
            strSql.Append("Remark=@Remark,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where SupplierCode=@SupplierCode");
            SqlParameter[] parameters = {
					new SqlParameter("@SupplierCode", SqlDbType.VarChar,64),
					new SqlParameter("@SupplierDesc1", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierDesc2", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierDesc3", SqlDbType.VarChar,512),
                    new SqlParameter("@SupplierAddress", SqlDbType.VarChar,512),
					new SqlParameter("@OtherAddress1", SqlDbType.VarChar,512),
                    new SqlParameter("@OtherAddress2", SqlDbType.VarChar,512),
                    new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
                    new SqlParameter("@Contact", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactPhone", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactEmail", SqlDbType.VarChar,512),
                    new SqlParameter("@ContactMobile", SqlDbType.VarChar,512),    
                    new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.SupplierCode;
            parameters[1].Value = model.SupplierDesc1;
            parameters[2].Value = model.SupplierDesc2;
            parameters[3].Value = model.SupplierDesc3;
            parameters[4].Value = model.SupplierAddress;
            parameters[5].Value = model.OtherAddress1;
            parameters[6].Value = model.OtherAddress2;
            parameters[7].Value = model.SupplierEmail;
            parameters[8].Value = model.Contact;
            parameters[9].Value = model.ContactPhone;
            parameters[10].Value = model.ContactEmail;
            parameters[11].Value = model.ContactMobile;
            parameters[12].Value = model.Remark;
            parameters[13].Value = model.CreatedOn;
            parameters[14].Value = model.CreatedBy;
            parameters[15].Value = model.UpdatedOn;
            parameters[16].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SupplierID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from Supplier ");
            strSql.Append(" where SupplierID=@SupplierID");
			SqlParameter[] parameters = {
					new SqlParameter("@SupplierID", SqlDbType.Int,4)
			};
            parameters[0].Value = SupplierID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(string SupplierCode)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from Supplier ");
            strSql.Append(" where SupplierCode=@SupplierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@SupplierCode", SqlDbType.VarChar,64)			};
            parameters[0].Value = SupplierCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
        public bool DeleteList(string SupplierIDlist)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from Supplier ");
            strSql.Append(" where SupplierID in (" + SupplierIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.Supplier GetModel(int SupplierID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 SupplierID,SupplierCode,SupplierDesc1,SupplierDesc2,SupplierDesc3,SupplierAddress,OtherAddress1,OtherAddress2,SupplierEmail,Contact,ContactPhone,ContactEmail,ContactMobile,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Supplier ");
            strSql.Append(" where SupplierID=@SupplierID");
			SqlParameter[] parameters = {
					new SqlParameter("@SupplierID", SqlDbType.Int,4)
			};
            parameters[0].Value = SupplierID;

            Edge.SVA.Model.Supplier model = new Edge.SVA.Model.Supplier();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["SupplierID"] != null && ds.Tables[0].Rows[0]["SupplierID"].ToString() != "")
				{
                    model.SupplierID = int.Parse(ds.Tables[0].Rows[0]["SupplierID"].ToString());
				}
                if (ds.Tables[0].Rows[0]["SupplierCode"] != null && ds.Tables[0].Rows[0]["SupplierCode"].ToString() != "")
				{
                    model.SupplierCode = ds.Tables[0].Rows[0]["SupplierCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SupplierDesc1"] != null && ds.Tables[0].Rows[0]["SupplierDesc1"].ToString() != "")
				{
                    model.SupplierDesc1 = ds.Tables[0].Rows[0]["SupplierDesc1"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SupplierDesc2"] != null && ds.Tables[0].Rows[0]["SupplierDesc2"].ToString() != "")
				{
                    model.SupplierDesc2 = ds.Tables[0].Rows[0]["SupplierDesc2"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SupplierDesc3"] != null && ds.Tables[0].Rows[0]["SupplierDesc3"].ToString() != "")
				{
                    model.SupplierDesc3 = ds.Tables[0].Rows[0]["SupplierDesc3"].ToString();
				}
                if (ds.Tables[0].Rows[0]["SupplierAddress"] != null && ds.Tables[0].Rows[0]["SupplierAddress"].ToString() != "")
                {
                    model.SupplierAddress = ds.Tables[0].Rows[0]["SupplierAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OtherAddress1"] != null && ds.Tables[0].Rows[0]["OtherAddress1"].ToString() != "")
                {
                    model.OtherAddress1 = ds.Tables[0].Rows[0]["OtherAddress1"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OtherAddress2"] != null && ds.Tables[0].Rows[0]["OtherAddress2"].ToString() != "")
                {
                    model.OtherAddress2 = ds.Tables[0].Rows[0]["OtherAddress2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierEmail"] != null && ds.Tables[0].Rows[0]["SupplierEmail"].ToString() != "")
                {
                    model.SupplierEmail = ds.Tables[0].Rows[0]["SupplierEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Contact"] != null && ds.Tables[0].Rows[0]["Contact"].ToString() != "")
                {
                    model.Contact = ds.Tables[0].Rows[0]["Contact"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ContactPhone"] != null && ds.Tables[0].Rows[0]["ContactPhone"].ToString() != "")
                {
                    model.ContactPhone = ds.Tables[0].Rows[0]["ContactPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ContactEmail"] != null && ds.Tables[0].Rows[0]["ContactEmail"].ToString() != "")
                {
                    model.ContactEmail = ds.Tables[0].Rows[0]["ContactEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ContactMobile"] != null && ds.Tables[0].Rows[0]["ContactMobile"].ToString() != "")
                {
                    model.ContactMobile = ds.Tables[0].Rows[0]["ContactMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select SupplierID,SupplierCode,SupplierDesc1,SupplierDesc2,SupplierDesc3,SupplierAddress,OtherAddress1,OtherAddress2,SupplierEmail,Contact,ContactPhone,ContactEmail,ContactMobile,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Supplier ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" SupplierID,SupplierCode,SupplierDesc1,SupplierDesc2,SupplierDesc3,SupplierAddress,OtherAddress1,OtherAddress2,SupplierEmail,Contact,ContactPhone,ContactEmail,ContactMobile,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Supplier ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) FROM Supplier ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
                strSql.Append("order by T.SupplierID desc");
			}
            strSql.Append(")AS Row, T.*  from Supplier T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		#endregion  Method
	}
}
