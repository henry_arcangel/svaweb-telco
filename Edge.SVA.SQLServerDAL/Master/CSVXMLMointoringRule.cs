﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CSVXMLMointoringRule
	/// </summary>
    public partial class CSVXMLMointoringRule : ICSVXMLMointoringRule
	{
		public CSVXMLMointoringRule()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.CSVXMLMointoringRule model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into CSVXMLMointoringRule(");
            strSql.Append("RuleName,Description,StartDate,EndDate,Status,Func,DownloadPath,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@RuleName,@Description,@StartDate,@EndDate,@Status,@Func,@DownloadPath,@DayFlagID,@MonthFlagID,@WeekFlagID,@ActiveTime,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@RuleName", SqlDbType.VarChar,64),
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Func", SqlDbType.VarChar,64),
					new SqlParameter("@DownloadPath", SqlDbType.VarChar,512),
					new SqlParameter("@DayFlagID", SqlDbType.Int,4),
					new SqlParameter("@MonthFlagID", SqlDbType.Int,4),
					new SqlParameter("@WeekFlagID" ,SqlDbType.Int,4),
					new SqlParameter("@ActiveTime", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.RuleName;
            parameters[1].Value = model.Description;
            parameters[2].Value = model.StartDate;
            parameters[3].Value = model.EndDate;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.Func;
            parameters[6].Value = model.DownloadPath;
            parameters[7].Value = model.DayFlagID;
            parameters[8].Value = model.MonthFlagID;
            parameters[9].Value = model.WeekFlagID;
            parameters[10].Value = model.ActiveTime;
            parameters[11].Value = model.CreatedOn;
            parameters[12].Value = model.CreatedBy;
            parameters[13].Value = model.UpdatedOn;
            parameters[14].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.CSVXMLMointoringRule model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update CSVXMLMointoringRule set ");
            strSql.Append("Description=@Description,");
            strSql.Append("StartDate=@StartDate,");
            strSql.Append("EndDate=@EndDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("Func=@Func,");
            strSql.Append("DownloadPath=@DownloadPath,");
            strSql.Append("DayFlagID=@DayFlagID,");
            strSql.Append("MonthFlagID=@MonthFlagID,");
            strSql.Append("WeekFlagID=@WeekFlagID,");
            strSql.Append("ActiveTime=@ActiveTime,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where RuleName = @RuleName");
            SqlParameter[] parameters = {					
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Func", SqlDbType.VarChar,64),
					new SqlParameter("@DownloadPath", SqlDbType.VarChar,512),
					new SqlParameter("@DayFlagID", SqlDbType.Int,4),
					new SqlParameter("@MonthFlagID", SqlDbType.Int,4),
					new SqlParameter("@WeekFlagID" ,SqlDbType.Int,4),
					new SqlParameter("@ActiveTime", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
                    new SqlParameter("@RuleName", SqlDbType.VarChar,64)};            
            parameters[0].Value = model.Description;
            parameters[1].Value = model.StartDate;
            parameters[2].Value = model.EndDate;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.Func;
            parameters[5].Value = model.DownloadPath;
            parameters[6].Value = model.DayFlagID;
            parameters[7].Value = model.MonthFlagID;
            parameters[8].Value = model.WeekFlagID;
            parameters[9].Value = model.ActiveTime;
            parameters[10].Value = model.CreatedOn;
            parameters[11].Value = model.CreatedBy;
            parameters[12].Value = model.UpdatedOn;
            parameters[13].Value = model.UpdatedBy;
            parameters[14].Value = model.RuleName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from CSVXMLMointoringRule ");
            strSql.Append(" where RuleName=@RuleName  ");
            SqlParameter[] parameters = {
					new SqlParameter("@RuleName", SqlDbType.VarChar,64)};
            parameters[0].Value = code;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.CSVXMLMointoringRule GetModel(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 RuleName,Description,StartDate,EndDate,Status,Func,DownloadPath,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from CSVXMLMointoringRule ");
            strSql.Append(" where RuleName=@RuleName  ");
            SqlParameter[] parameters = {
					new SqlParameter("@RuleName", SqlDbType.VarChar,64)};
            parameters[0].Value = code;

            Edge.SVA.Model.CSVXMLMointoringRule model = new Edge.SVA.Model.CSVXMLMointoringRule();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["RuleName"] != null && ds.Tables[0].Rows[0]["RuleName"].ToString() != "")
				{
                    model.RuleName = ds.Tables[0].Rows[0]["RuleName"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description"] != null && ds.Tables[0].Rows[0]["Description"].ToString() != "")
				{
                    model.Description = ds.Tables[0].Rows[0]["Description"].ToString();
				}
                if (ds.Tables[0].Rows[0]["StartDate"] != null && ds.Tables[0].Rows[0]["StartDate"].ToString() != "")
				{
                    model.StartDate = DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
                if (ds.Tables[0].Rows[0]["EndDate"] != null && ds.Tables[0].Rows[0]["EndDate"].ToString() != "")
				{
                    model.EndDate = DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
				{
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
                if (ds.Tables[0].Rows[0]["Func"] != null && ds.Tables[0].Rows[0]["Func"].ToString() != "")
				{
                    model.Func = ds.Tables[0].Rows[0]["Func"].ToString();
				}
                if (ds.Tables[0].Rows[0]["DownloadPath"] != null && ds.Tables[0].Rows[0]["DownloadPath"].ToString() != "")
				{
                    model.DownloadPath = ds.Tables[0].Rows[0]["DownloadPath"].ToString();
				}

                if (ds.Tables[0].Rows[0]["DayFlagID"] != null && ds.Tables[0].Rows[0]["DayFlagID"].ToString() != "")
                {
                    model.DayFlagID = int.Parse(ds.Tables[0].Rows[0]["DayFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MonthFlagID"] != null && ds.Tables[0].Rows[0]["MonthFlagID"].ToString() != "")
                {
                    model.MonthFlagID = int.Parse(ds.Tables[0].Rows[0]["MonthFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WeekFlagID"] != null && ds.Tables[0].Rows[0]["WeekFlagID"].ToString() != "")
                {
                    model.WeekFlagID = int.Parse(ds.Tables[0].Rows[0]["WeekFlagID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActiveTime"] != null && ds.Tables[0].Rows[0]["ActiveTime"].ToString() != "")
                {
                    model.ActiveTime = DateTime.Parse(ds.Tables[0].Rows[0]["ActiveTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select RuleName,Description,StartDate,EndDate,Status,Func,DownloadPath,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy  ");
            strSql.Append(" FROM CSVXMLMointoringRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" RuleName,Description,StartDate,EndDate,Status,Func,DownloadPath,DayFlagID,MonthFlagID,WeekFlagID,ActiveTime,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM CSVXMLMointoringRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CSVXMLMointoringRule";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CSVXMLMointoringRule ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

