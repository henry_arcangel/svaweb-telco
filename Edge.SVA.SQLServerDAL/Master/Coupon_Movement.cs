﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Coupon_Movement
	/// </summary>
	public partial class Coupon_Movement:ICoupon_Movement
	{
		public Coupon_Movement()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Coupon_Movement"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Coupon_Movement");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Coupon_Movement model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Coupon_Movement(");
			strSql.Append("OprID,CardNumber,CardTypeID,CouponNumber,CouponTypeID,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,BusDate,Txndate,TenderCode,TenderRate,Remark,SecurityCode,CreatedOn,CreatedBy)");
			strSql.Append(" values (");
			strSql.Append("@OprID,@CardNumber,@CardTypeID,@CouponNumber,@CouponTypeID,@RefKeyID,@RefReceiveKeyID,@RefTxnNo,@OpenBal,@Amount,@CloseBal,@BusDate,@Txndate,@TenderCode,@TenderRate,@Remark,@SecurityCode,@CreatedOn,@CreatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@RefKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefReceiveKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@OpenBal", SqlDbType.Money,8),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@CloseBal", SqlDbType.Money,8),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@Txndate", SqlDbType.DateTime),
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512),
					new SqlParameter("@TenderRate", SqlDbType.Decimal,9),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@SecurityCode", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512)};
			parameters[0].Value = model.OprID;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.CardTypeID;
			parameters[3].Value = model.CouponNumber;
			parameters[4].Value = model.CouponTypeID;
			parameters[5].Value = model.RefKeyID;
			parameters[6].Value = model.RefReceiveKeyID;
			parameters[7].Value = model.RefTxnNo;
			parameters[8].Value = model.OpenBal;
			parameters[9].Value = model.Amount;
			parameters[10].Value = model.CloseBal;
			parameters[11].Value = model.BusDate;
			parameters[12].Value = model.Txndate;
			parameters[13].Value = model.TenderCode;
			parameters[14].Value = model.TenderRate;
			parameters[15].Value = model.Remark;
			parameters[16].Value = model.SecurityCode;
			parameters[17].Value = model.CreatedOn;
			parameters[18].Value = model.CreatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Coupon_Movement model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Coupon_Movement set ");
			strSql.Append("OprID=@OprID,");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CouponNumber=@CouponNumber,");
			strSql.Append("CouponTypeID=@CouponTypeID,");
			strSql.Append("RefKeyID=@RefKeyID,");
			strSql.Append("RefReceiveKeyID=@RefReceiveKeyID,");
			strSql.Append("RefTxnNo=@RefTxnNo,");
			strSql.Append("OpenBal=@OpenBal,");
			strSql.Append("Amount=@Amount,");
			strSql.Append("CloseBal=@CloseBal,");
			strSql.Append("BusDate=@BusDate,");
			strSql.Append("Txndate=@Txndate,");
			strSql.Append("TenderCode=@TenderCode,");
			strSql.Append("TenderRate=@TenderRate,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("SecurityCode=@SecurityCode,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@RefKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefReceiveKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@OpenBal", SqlDbType.Money,8),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@CloseBal", SqlDbType.Money,8),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@Txndate", SqlDbType.DateTime),
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512),
					new SqlParameter("@TenderRate", SqlDbType.Decimal,9),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@SecurityCode", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.OprID;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.CardTypeID;
			parameters[3].Value = model.CouponNumber;
			parameters[4].Value = model.CouponTypeID;
			parameters[5].Value = model.RefKeyID;
			parameters[6].Value = model.RefReceiveKeyID;
			parameters[7].Value = model.RefTxnNo;
			parameters[8].Value = model.OpenBal;
			parameters[9].Value = model.Amount;
			parameters[10].Value = model.CloseBal;
			parameters[11].Value = model.BusDate;
			parameters[12].Value = model.Txndate;
			parameters[13].Value = model.TenderCode;
			parameters[14].Value = model.TenderRate;
			parameters[15].Value = model.Remark;
			parameters[16].Value = model.SecurityCode;
			parameters[17].Value = model.CreatedOn;
			parameters[18].Value = model.CreatedBy;
			parameters[19].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Coupon_Movement ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Coupon_Movement ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Coupon_Movement GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,OprID,CardNumber,CardTypeID,CouponNumber,CouponTypeID,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,BusDate,Txndate,TenderCode,TenderRate,Remark,SecurityCode,CreatedOn,CreatedBy from Coupon_Movement ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Coupon_Movement model=new Edge.SVA.Model.Coupon_Movement();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OprID"]!=null && ds.Tables[0].Rows[0]["OprID"].ToString()!="")
				{
					model.OprID=int.Parse(ds.Tables[0].Rows[0]["OprID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardTypeID"]!=null && ds.Tables[0].Rows[0]["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CouponNumber"]!=null && ds.Tables[0].Rows[0]["CouponNumber"].ToString()!="")
				{
					model.CouponNumber=ds.Tables[0].Rows[0]["CouponNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CouponTypeID"]!=null && ds.Tables[0].Rows[0]["CouponTypeID"].ToString()!="")
				{
					model.CouponTypeID=int.Parse(ds.Tables[0].Rows[0]["CouponTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefKeyID"]!=null && ds.Tables[0].Rows[0]["RefKeyID"].ToString()!="")
				{
					model.RefKeyID=int.Parse(ds.Tables[0].Rows[0]["RefKeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefReceiveKeyID"]!=null && ds.Tables[0].Rows[0]["RefReceiveKeyID"].ToString()!="")
				{
					model.RefReceiveKeyID=int.Parse(ds.Tables[0].Rows[0]["RefReceiveKeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefTxnNo"]!=null && ds.Tables[0].Rows[0]["RefTxnNo"].ToString()!="")
				{
					model.RefTxnNo=ds.Tables[0].Rows[0]["RefTxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OpenBal"]!=null && ds.Tables[0].Rows[0]["OpenBal"].ToString()!="")
				{
					model.OpenBal=decimal.Parse(ds.Tables[0].Rows[0]["OpenBal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Amount"]!=null && ds.Tables[0].Rows[0]["Amount"].ToString()!="")
				{
					model.Amount=decimal.Parse(ds.Tables[0].Rows[0]["Amount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CloseBal"]!=null && ds.Tables[0].Rows[0]["CloseBal"].ToString()!="")
				{
					model.CloseBal=decimal.Parse(ds.Tables[0].Rows[0]["CloseBal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BusDate"]!=null && ds.Tables[0].Rows[0]["BusDate"].ToString()!="")
				{
					model.BusDate=DateTime.Parse(ds.Tables[0].Rows[0]["BusDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Txndate"]!=null && ds.Tables[0].Rows[0]["Txndate"].ToString()!="")
				{
					model.Txndate=DateTime.Parse(ds.Tables[0].Rows[0]["Txndate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenderCode"]!=null && ds.Tables[0].Rows[0]["TenderCode"].ToString()!="")
				{
					model.TenderCode=ds.Tables[0].Rows[0]["TenderCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TenderRate"]!=null && ds.Tables[0].Rows[0]["TenderRate"].ToString()!="")
				{
					model.TenderRate=decimal.Parse(ds.Tables[0].Rows[0]["TenderRate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SecurityCode"]!=null && ds.Tables[0].Rows[0]["SecurityCode"].ToString()!="")
				{
					model.SecurityCode=ds.Tables[0].Rows[0]["SecurityCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,OprID,CardNumber,CardTypeID,CouponNumber,CouponTypeID,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,BusDate,Txndate,TenderCode,TenderRate,Remark,SecurityCode,CreatedOn,CreatedBy ");
			strSql.Append(" FROM Coupon_Movement ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,OprID,CardNumber,CardTypeID,CouponNumber,CouponTypeID,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,BusDate,Txndate,TenderCode,TenderRate,Remark,SecurityCode,CreatedOn,CreatedBy ");
			strSql.Append(" FROM Coupon_Movement ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			//parameters[0].Value = "Coupon_Movement"; 
            parameters[0].Value = "view_Coupon_Movement"; //Modified By Robin 2014-11-03 to fix RRG performance issue (no issue transaction in coupon_movement)
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Coupon_Movement ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

