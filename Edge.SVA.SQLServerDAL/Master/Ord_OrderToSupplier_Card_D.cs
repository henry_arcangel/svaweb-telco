﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Ord_OrderToSupplier_Card_D
    /// </summary>
    public partial class Ord_OrderToSupplier_Card_D : IOrd_OrderToSupplier_Card_D
    {
        public Ord_OrderToSupplier_Card_D()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Ord_OrderToSupplier_Card_D");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.Ord_OrderToSupplier_Card_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Ord_OrderToSupplier_Card_D(");
            strSql.Append("OrderSupplierNumber,CardTypeID,CardGradeID,OrderQty,FirstCardNumber,EndCardNumber,BatchCardCode,PackageQty,OrderRoundUpQty,OrderAmount,OrderPoint)");
            strSql.Append(" values (");
            strSql.Append("@OrderSupplierNumber,@CardTypeID,@CardGradeID,@OrderQty,@FirstCardNumber,@EndCardNumber,@BatchCardCode,@PackageQty,@OrderRoundUpQty,@OrderAmount,@OrderPoint)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@OrderQty", SqlDbType.Int,4),
					new SqlParameter("@FirstCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@EndCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BatchCardCode", SqlDbType.VarChar,64),
					new SqlParameter("@PackageQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
                    new SqlParameter("@OrderAmount", SqlDbType.Decimal,4),
                    new SqlParameter("@OrderPoint", SqlDbType.Int,4)};
            parameters[0].Value = model.OrderSupplierNumber;
            parameters[1].Value = model.CardTypeID;
            parameters[2].Value = model.CardGradeID;
            parameters[3].Value = model.OrderQty;
            parameters[4].Value = model.FirstCardNumber;
            parameters[5].Value = model.EndCardNumber;
            parameters[6].Value = model.BatchCardCode;
            parameters[7].Value = model.PackageQty;
            parameters[8].Value = model.OrderRoundUpQty;
            parameters[9].Value = model.OrderAmount;
            parameters[10].Value = model.OrderPoint;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_OrderToSupplier_Card_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_OrderToSupplier_Card_D set ");
            strSql.Append("OrderSupplierNumber=@OrderSupplierNumber,");
            strSql.Append("CardTypeID=@CardTypeID,");
            strSql.Append("CardGradeID=@CardGradeID,");
            strSql.Append("OrderQty=@OrderQty,");
            strSql.Append("FirstCardNumber=@FirstCardNumber,");
            strSql.Append("EndCardNumber=@EndCardNumber,");
            strSql.Append("BatchCardCode=@BatchCardCode,");
            strSql.Append("PackageQty=@PackageQty,");
            strSql.Append("OrderRoundUpQty=@OrderRoundUpQty");
            strSql.Append("OrderAmount=@OrderAmount");
            strSql.Append("OrderPoint=@OrderPoint");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@OrderQty", SqlDbType.Int,4),
					new SqlParameter("@FirstCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@EndCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BatchCardCode", SqlDbType.VarChar,64),
					new SqlParameter("@PackageQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
                    new SqlParameter("@OrderAmount", SqlDbType.Decimal,4),
                    new SqlParameter("@OrderPoint", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
            parameters[0].Value = model.OrderSupplierNumber;
            parameters[1].Value = model.CardTypeID;
            parameters[2].Value = model.CardGradeID;
            parameters[3].Value = model.OrderQty;
            parameters[4].Value = model.FirstCardNumber;
            parameters[5].Value = model.EndCardNumber;
            parameters[6].Value = model.BatchCardCode;
            parameters[7].Value = model.PackageQty;
            parameters[8].Value = model.OrderRoundUpQty;
            parameters[9].Value = model.OrderAmount;
            parameters[10].Value = model.OrderPoint;
            parameters[11].Value = model.KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_OrderToSupplier_Card_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_OrderToSupplier_Card_D ");
            strSql.Append(" where KeyID in (" + KeyIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Ord_OrderToSupplier_Card_D GetModel(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 KeyID,OrderSupplierNumber,CardTypeID,CardGradeID,OrderQty,FirstCardNumber,EndCardNumber,BatchCardCode,PackageQty,OrderRoundUpQty,OrderAmount,OrderPoint from Ord_OrderToSupplier_Card_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            Edge.SVA.Model.Ord_OrderToSupplier_Card_D model = new Edge.SVA.Model.Ord_OrderToSupplier_Card_D();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["KeyID"] != null && ds.Tables[0].Rows[0]["KeyID"].ToString() != "")
                {
                    model.KeyID = int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderSupplierNumber"] != null && ds.Tables[0].Rows[0]["OrderSupplierNumber"].ToString() != "")
                {
                    model.OrderSupplierNumber = ds.Tables[0].Rows[0]["OrderSupplierNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderQty"] != null && ds.Tables[0].Rows[0]["OrderQty"].ToString() != "")
                {
                    model.OrderQty = int.Parse(ds.Tables[0].Rows[0]["OrderQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FirstCardNumber"] != null && ds.Tables[0].Rows[0]["FirstCardNumber"].ToString() != "")
                {
                    model.FirstCardNumber = ds.Tables[0].Rows[0]["FirstCardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EndCardNumber"] != null && ds.Tables[0].Rows[0]["EndCardNumber"].ToString() != "")
                {
                    model.EndCardNumber = ds.Tables[0].Rows[0]["EndCardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BatchCardCode"] != null && ds.Tables[0].Rows[0]["BatchCardCode"].ToString() != "")
                {
                    model.BatchCardCode = ds.Tables[0].Rows[0]["BatchCardCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PackageQty"] != null && ds.Tables[0].Rows[0]["PackageQty"].ToString() != "")
                {
                    model.PackageQty = int.Parse(ds.Tables[0].Rows[0]["PackageQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderRoundUpQty"] != null && ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString() != "")
                {
                    model.OrderRoundUpQty = int.Parse(ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderAmount"] != null && ds.Tables[0].Rows[0]["OrderAmount"].ToString() != "")
                {
                    model.OrderAmount = int.Parse(ds.Tables[0].Rows[0]["OrderAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderPoint"] != null && ds.Tables[0].Rows[0]["OrderPoint"].ToString() != "")
                {
                    model.OrderPoint = int.Parse(ds.Tables[0].Rows[0]["OrderPoint"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select KeyID,OrderSupplierNumber,CardTypeID,CardGradeID,OrderQty,FirstCardNumber,EndCardNumber,BatchCardCode,PackageQty,OrderRoundUpQty,OrderAmount,OrderPoint ");
            strSql.Append(" FROM Ord_OrderToSupplier_Card_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" KeyID,OrderSupplierNumber,CardTypeID,CardGradeID,OrderQty,FirstCardNumber,EndCardNumber,BatchCardCode,PackageQty,OrderRoundUpQty,OrderAmount,OrderPoint ");
            strSql.Append(" FROM Ord_OrderToSupplier_Card_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Ord_OrderToSupplier_Card_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.KeyID desc");
            }
            strSql.Append(")AS Row, T.*  from Ord_OrderToSupplier_Card_D T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Ord_OrderToSupplier_Card_D";
            parameters[1].Value = "KeyID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

