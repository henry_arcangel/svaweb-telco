﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Card
	/// </summary>
	public partial class Card:ICard
	{
		public Card()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardNumber)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Card");
			strSql.Append(" where CardNumber=@CardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512)};
			parameters[0].Value = CardNumber;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Card model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Card(");
            strSql.Append("CardNumber,CardTypeID,CardIssueDate,CardExpiryDate,CardGradeID,MemberID,BatchCardID,Status,UsedCount,CardPassword,TotalPoints,TotalAmount,ParentCardNumber,CardForfeitPoints,CardForfeitAmount,ResetPassword,CardAmountExpiryDate,CardPointExpiryDate,CumulativeEarnPoints,CumulativeConsumptionAmt,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,ApprovedCode,StockStatus,IssueStoreID,ActiveStoreID,ActiveDate,PickupFlag,StockStatus)");
            strSql.Append(" values (");
            strSql.Append("@CardNumber,@CardTypeID,@CardIssueDate,@CardExpiryDate,@CardGradeID,@MemberID,@BatchCardID,@Status,@UsedCount,@CardPassword,@TotalPoints,@TotalAmount,@ParentCardNumber,@CardForfeitPoints,@CardForfeitAmount,@ResetPassword,@CardAmountExpiryDate,@CardPointExpiryDate,@CumulativeEarnPoints,@CumulativeConsumptionAmt,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@PasswordExpiryDate,@PWDExpiryPromptDays,@ApprovedCode,@StockStatus,@IssueStoreID,@ActiveStoreID,@ActiveDate,@PickupFlag,@StockStatus)");
            SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardIssueDate", SqlDbType.DateTime),
					new SqlParameter("@CardExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@BatchCardID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@UsedCount", SqlDbType.Int,4),
					new SqlParameter("@CardPassword", SqlDbType.VarChar,512),
					new SqlParameter("@TotalPoints", SqlDbType.Int,4),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@ParentCardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CardForfeitPoints", SqlDbType.Int,4),
					new SqlParameter("@CardForfeitAmount", SqlDbType.Money,8),
					new SqlParameter("@ResetPassword", SqlDbType.Int,4),
					new SqlParameter("@CardAmountExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CardPointExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CumulativeEarnPoints", SqlDbType.Int,4),
					new SqlParameter("@CumulativeConsumptionAmt", SqlDbType.Money,8),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@PWDExpiryPromptDays", SqlDbType.Int,4),
					new SqlParameter("@ApprovedCode", SqlDbType.VarChar,512),
					new SqlParameter("@StockStatus", SqlDbType.Int,4),
					new SqlParameter("@IssueStoreID", SqlDbType.Int,4),
					new SqlParameter("@ActiveStoreID", SqlDbType.Int,4),
					new SqlParameter("@ActiveDate", SqlDbType.DateTime),
					new SqlParameter("@PickupFlag", SqlDbType.DateTime),
                    new SqlParameter("@StockStatus", SqlDbType.Int,4)};
            
            parameters[0].Value = model.CardNumber;
            parameters[1].Value = model.CardTypeID;
            parameters[2].Value = model.CardIssueDate;
            parameters[3].Value = model.CardExpiryDate;
            parameters[4].Value = model.CardGradeID;
            parameters[5].Value = model.MemberID;
            parameters[6].Value = model.BatchCardID;
            parameters[7].Value = model.Status;
            parameters[8].Value = model.UsedCount;
            parameters[9].Value = model.CardPassword;
            parameters[10].Value = model.TotalPoints;
            parameters[11].Value = model.TotalAmount;
            parameters[12].Value = model.ParentCardNumber;
            parameters[13].Value = model.CardForfeitPoints;
            parameters[14].Value = model.CardForfeitAmount;
            parameters[15].Value = model.ResetPassword;
            parameters[16].Value = model.CardAmountExpiryDate;
            parameters[17].Value = model.CardPointExpiryDate;
            parameters[18].Value = model.CumulativeEarnPoints;
            parameters[19].Value = model.CumulativeConsumptionAmt;
            parameters[20].Value = model.CreatedOn;
            parameters[21].Value = model.UpdatedOn;
            parameters[22].Value = model.CreatedBy;
            parameters[23].Value = model.UpdatedBy;
            parameters[24].Value = model.PasswordExpiryDate;
            parameters[25].Value = model.PWDExpiryPromptDays;
            parameters[26].Value = model.ApprovedCode;
            parameters[27].Value = model.StockStatus;
            parameters[28].Value = model.IssueStoreID;
            parameters[29].Value = model.ActiveStoreID;
            parameters[30].Value = model.ActiveDate;
            parameters[31].Value = model.PickupFlag;
            parameters[32].Value = model.StockStatus;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Card model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Card set ");
            strSql.Append("CardTypeID=@CardTypeID,");
            strSql.Append("CardIssueDate=@CardIssueDate,");
            strSql.Append("CardExpiryDate=@CardExpiryDate,");
            strSql.Append("CardGradeID=@CardGradeID,");
            strSql.Append("MemberID=@MemberID,");
            strSql.Append("BatchCardID=@BatchCardID,");
            strSql.Append("Status=@Status,");
            strSql.Append("UsedCount=@UsedCount,");
            strSql.Append("CardPassword=@CardPassword,");
            strSql.Append("TotalPoints=@TotalPoints,");
            strSql.Append("TotalAmount=@TotalAmount,");
            strSql.Append("ParentCardNumber=@ParentCardNumber,");
            strSql.Append("CardForfeitPoints=@CardForfeitPoints,");
            strSql.Append("CardForfeitAmount=@CardForfeitAmount,");
            strSql.Append("ResetPassword=@ResetPassword,");
            strSql.Append("CardAmountExpiryDate=@CardAmountExpiryDate,");
            strSql.Append("CardPointExpiryDate=@CardPointExpiryDate,");
            strSql.Append("CumulativeEarnPoints=@CumulativeEarnPoints,");
            strSql.Append("CumulativeConsumptionAmt=@CumulativeConsumptionAmt,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("PasswordExpiryDate=@PasswordExpiryDate,");
            strSql.Append("PWDExpiryPromptDays=@PWDExpiryPromptDays,");
            strSql.Append("ApprovedCode=@ApprovedCode,");
            strSql.Append("StockStatus=@StockStatus,");
            strSql.Append("IssueStoreID=@IssueStoreID,");
            strSql.Append("ActiveStoreID=@ActiveStoreID,");
            strSql.Append("ActiveDate=@ActiveDate,");
            strSql.Append("PickupFlag=@PickupFlag");
            strSql.Append("StockStatus=@StockStatus");
            strSql.Append(" where CardNumber=@CardNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardIssueDate", SqlDbType.DateTime),
					new SqlParameter("@CardExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@BatchCardID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@UsedCount", SqlDbType.Int,4),
					new SqlParameter("@CardPassword", SqlDbType.VarChar,512),
					new SqlParameter("@TotalPoints", SqlDbType.Int,4),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@ParentCardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CardForfeitPoints", SqlDbType.Int,4),
					new SqlParameter("@CardForfeitAmount", SqlDbType.Money,8),
					new SqlParameter("@ResetPassword", SqlDbType.Int,4),
					new SqlParameter("@CardAmountExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CardPointExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CumulativeEarnPoints", SqlDbType.Int,4),
					new SqlParameter("@CumulativeConsumptionAmt", SqlDbType.Money,8),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@PasswordExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@PWDExpiryPromptDays", SqlDbType.Int,4),
					new SqlParameter("@ApprovedCode", SqlDbType.VarChar,512),
					new SqlParameter("@StockStatus", SqlDbType.Int,4),
					new SqlParameter("@IssueStoreID", SqlDbType.Int,4),
					new SqlParameter("@ActiveStoreID", SqlDbType.Int,4),
					new SqlParameter("@ActiveDate", SqlDbType.DateTime),
					new SqlParameter("@PickupFlag", SqlDbType.DateTime),
                    new SqlParameter("@StockStatus", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512)};
            parameters[0].Value = model.CardTypeID;
            parameters[1].Value = model.CardIssueDate;
            parameters[2].Value = model.CardExpiryDate;
            parameters[3].Value = model.CardGradeID;
            parameters[4].Value = model.MemberID;
            parameters[5].Value = model.BatchCardID;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.UsedCount;
            parameters[8].Value = model.CardPassword;
            parameters[9].Value = model.TotalPoints;
            parameters[10].Value = model.TotalAmount;
            parameters[11].Value = model.ParentCardNumber;
            parameters[12].Value = model.CardForfeitPoints;
            parameters[13].Value = model.CardForfeitAmount;
            parameters[14].Value = model.ResetPassword;
            parameters[15].Value = model.CardAmountExpiryDate;
            parameters[16].Value = model.CardPointExpiryDate;
            parameters[17].Value = model.CumulativeEarnPoints;
            parameters[18].Value = model.CumulativeConsumptionAmt;
            parameters[19].Value = model.CreatedOn;
            parameters[20].Value = model.UpdatedOn;
            parameters[21].Value = model.CreatedBy;
            parameters[22].Value = model.UpdatedBy;
            parameters[23].Value = model.PasswordExpiryDate;
            parameters[24].Value = model.PWDExpiryPromptDays;
            parameters[25].Value = model.ApprovedCode;
            parameters[26].Value = model.StockStatus;
            parameters[27].Value = model.IssueStoreID;
            parameters[28].Value = model.ActiveStoreID;
            parameters[29].Value = model.ActiveDate;
            parameters[30].Value = model.PickupFlag;
            parameters[31].Value = model.StockStatus;
            parameters[32].Value = model.CardNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardNumber)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Card ");
			strSql.Append(" where CardNumber=@CardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512)};
			parameters[0].Value = CardNumber;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardNumberlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Card ");
			strSql.Append(" where CardNumber in ("+CardNumberlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Card GetModel(string CardNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CardNumber,CardTypeID,CardIssueDate,CardExpiryDate,CardGradeID,MemberID,BatchCardID,Status,UsedCount,CardPassword,TotalPoints,TotalAmount,ParentCardNumber,CardForfeitPoints,CardForfeitAmount,ResetPassword,CardAmountExpiryDate,CardPointExpiryDate,CumulativeEarnPoints,CumulativeConsumptionAmt,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,ApprovedCode,StockStatus,IssueStoreID,ActiveStoreID,ActiveDate,PickupFlag,StockStatus from Card ");
            strSql.Append(" where CardNumber=@CardNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512)			};
            parameters[0].Value = CardNumber;

            Edge.SVA.Model.Card model = new Edge.SVA.Model.Card();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CardNumber"] != null && ds.Tables[0].Rows[0]["CardNumber"].ToString() != "")
                {
                    model.CardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardTypeID"] != null && ds.Tables[0].Rows[0]["CardTypeID"].ToString() != "")
                {
                    model.CardTypeID = int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardIssueDate"] != null && ds.Tables[0].Rows[0]["CardIssueDate"].ToString() != "")
                {
                    model.CardIssueDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardIssueDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardExpiryDate"] != null && ds.Tables[0].Rows[0]["CardExpiryDate"].ToString() != "")
                {
                    model.CardExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardExpiryDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardGradeID"] != null && ds.Tables[0].Rows[0]["CardGradeID"].ToString() != "")
                {
                    model.CardGradeID = int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MemberID"] != null && ds.Tables[0].Rows[0]["MemberID"].ToString() != "")
                {
                    model.MemberID = int.Parse(ds.Tables[0].Rows[0]["MemberID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BatchCardID"] != null && ds.Tables[0].Rows[0]["BatchCardID"].ToString() != "")
                {
                    model.BatchCardID = int.Parse(ds.Tables[0].Rows[0]["BatchCardID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UsedCount"] != null && ds.Tables[0].Rows[0]["UsedCount"].ToString() != "")
                {
                    model.UsedCount = int.Parse(ds.Tables[0].Rows[0]["UsedCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardPassword"] != null && ds.Tables[0].Rows[0]["CardPassword"].ToString() != "")
                {
                    model.CardPassword = ds.Tables[0].Rows[0]["CardPassword"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TotalPoints"] != null && ds.Tables[0].Rows[0]["TotalPoints"].ToString() != "")
                {
                    model.TotalPoints = int.Parse(ds.Tables[0].Rows[0]["TotalPoints"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalAmount"] != null && ds.Tables[0].Rows[0]["TotalAmount"].ToString() != "")
                {
                    model.TotalAmount = decimal.Parse(ds.Tables[0].Rows[0]["TotalAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ParentCardNumber"] != null && ds.Tables[0].Rows[0]["ParentCardNumber"].ToString() != "")
                {
                    model.ParentCardNumber = ds.Tables[0].Rows[0]["ParentCardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardForfeitPoints"] != null && ds.Tables[0].Rows[0]["CardForfeitPoints"].ToString() != "")
                {
                    model.CardForfeitPoints = int.Parse(ds.Tables[0].Rows[0]["CardForfeitPoints"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardForfeitAmount"] != null && ds.Tables[0].Rows[0]["CardForfeitAmount"].ToString() != "")
                {
                    model.CardForfeitAmount = decimal.Parse(ds.Tables[0].Rows[0]["CardForfeitAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ResetPassword"] != null && ds.Tables[0].Rows[0]["ResetPassword"].ToString() != "")
                {
                    model.ResetPassword = int.Parse(ds.Tables[0].Rows[0]["ResetPassword"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardAmountExpiryDate"] != null && ds.Tables[0].Rows[0]["CardAmountExpiryDate"].ToString() != "")
                {
                    model.CardAmountExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardAmountExpiryDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CardPointExpiryDate"] != null && ds.Tables[0].Rows[0]["CardPointExpiryDate"].ToString() != "")
                {
                    model.CardPointExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["CardPointExpiryDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CumulativeEarnPoints"] != null && ds.Tables[0].Rows[0]["CumulativeEarnPoints"].ToString() != "")
                {
                    model.CumulativeEarnPoints = int.Parse(ds.Tables[0].Rows[0]["CumulativeEarnPoints"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CumulativeConsumptionAmt"] != null && ds.Tables[0].Rows[0]["CumulativeConsumptionAmt"].ToString() != "")
                {
                    model.CumulativeConsumptionAmt = decimal.Parse(ds.Tables[0].Rows[0]["CumulativeConsumptionAmt"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PasswordExpiryDate"] != null && ds.Tables[0].Rows[0]["PasswordExpiryDate"].ToString() != "")
                {
                    model.PasswordExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["PasswordExpiryDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PWDExpiryPromptDays"] != null && ds.Tables[0].Rows[0]["PWDExpiryPromptDays"].ToString() != "")
                {
                    model.PWDExpiryPromptDays = int.Parse(ds.Tables[0].Rows[0]["PWDExpiryPromptDays"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApprovedCode"] != null && ds.Tables[0].Rows[0]["ApprovedCode"].ToString() != "")
                {
                    model.ApprovedCode = ds.Tables[0].Rows[0]["ApprovedCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StockStatus"] != null && ds.Tables[0].Rows[0]["StockStatus"].ToString() != "")
                {
                    model.StockStatus = int.Parse(ds.Tables[0].Rows[0]["StockStatus"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IssueStoreID"] != null && ds.Tables[0].Rows[0]["IssueStoreID"].ToString() != "")
                {
                    model.IssueStoreID = int.Parse(ds.Tables[0].Rows[0]["IssueStoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActiveStoreID"] != null && ds.Tables[0].Rows[0]["ActiveStoreID"].ToString() != "")
                {
                    model.ActiveStoreID = int.Parse(ds.Tables[0].Rows[0]["ActiveStoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActiveDate"] != null && ds.Tables[0].Rows[0]["ActiveDate"].ToString() != "")
                {
                    model.ActiveDate = DateTime.Parse(ds.Tables[0].Rows[0]["ActiveDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PickupFlag"] != null && ds.Tables[0].Rows[0]["PickupFlag"].ToString() != "")
                {
                    model.PickupFlag = DateTime.Parse(ds.Tables[0].Rows[0]["PickupFlag"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StockStatus"] != null && ds.Tables[0].Rows[0]["StockStatus"].ToString() != "")
                {
                    model.StockStatus = int.Parse(ds.Tables[0].Rows[0]["StockStatus"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CardNumber,CardTypeID,CardIssueDate,CardExpiryDate,CardGradeID,MemberID,BatchCardID,Status,UsedCount,CardPassword,TotalPoints,TotalAmount,ParentCardNumber,CardForfeitPoints,CardForfeitAmount,ResetPassword,CardAmountExpiryDate,CardPointExpiryDate,CumulativeEarnPoints,CumulativeConsumptionAmt,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,ApprovedCode,StockStatus,IssueStoreID,ActiveStoreID,ActiveDate,PickupFlag,StockStatus ");
            strSql.Append(" FROM Card ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CardNumber,CardTypeID,CardIssueDate,CardExpiryDate,CardGradeID,MemberID,BatchCardID,Status,UsedCount,CardPassword,TotalPoints,TotalAmount,ParentCardNumber,CardForfeitPoints,CardForfeitAmount,ResetPassword,CardAmountExpiryDate,CardPointExpiryDate,CumulativeEarnPoints,CumulativeConsumptionAmt,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,PasswordExpiryDate,PWDExpiryPromptDays,ApprovedCode,StockStatus,IssueStoreID,ActiveStoreID,ActiveDate,PickupFlag,StockStatus ");
            strSql.Append(" FROM Card ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Card";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Card ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

