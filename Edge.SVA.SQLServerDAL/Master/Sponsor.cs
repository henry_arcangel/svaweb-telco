﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Sponsor
	/// </summary>
	public partial class Sponsor:ISponsor
	{
		public Sponsor()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string SponsorCode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Sponsor");
			strSql.Append(" where SponsorCode=@SponsorCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorCode", SqlDbType.VarChar,64)			};
			parameters[0].Value = SponsorCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Sponsor model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Sponsor(");
			strSql.Append("SponsorCode,SponsorName1,SponsorName2,SponsorName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@SponsorCode,@SponsorName1,@SponsorName2,@SponsorName3,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorCode", SqlDbType.VarChar,64),
					new SqlParameter("@SponsorName1", SqlDbType.VarChar,512),
					new SqlParameter("@SponsorName2", SqlDbType.VarChar,512),
					new SqlParameter("@SponsorName3", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.SponsorCode;
			parameters[1].Value = model.SponsorName1;
			parameters[2].Value = model.SponsorName2;
			parameters[3].Value = model.SponsorName3;
			parameters[4].Value = model.CreatedOn;
			parameters[5].Value = model.CreatedBy;
			parameters[6].Value = model.UpdatedOn;
			parameters[7].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sponsor model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Sponsor set ");
			strSql.Append("SponsorName1=@SponsorName1,");
			strSql.Append("SponsorName2=@SponsorName2,");
			strSql.Append("SponsorName3=@SponsorName3,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where SponsorID=@SponsorID");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorName1", SqlDbType.VarChar,512),
					new SqlParameter("@SponsorName2", SqlDbType.VarChar,512),
					new SqlParameter("@SponsorName3", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@SponsorID", SqlDbType.Int,4),
					new SqlParameter("@SponsorCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.SponsorName1;
			parameters[1].Value = model.SponsorName2;
			parameters[2].Value = model.SponsorName3;
			parameters[3].Value = model.CreatedOn;
			parameters[4].Value = model.CreatedBy;
			parameters[5].Value = model.UpdatedOn;
			parameters[6].Value = model.UpdatedBy;
			parameters[7].Value = model.SponsorID;
			parameters[8].Value = model.SponsorCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SponsorID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sponsor ");
			strSql.Append(" where SponsorID=@SponsorID");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorID", SqlDbType.Int,4)
			};
			parameters[0].Value = SponsorID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string SponsorCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sponsor ");
			strSql.Append(" where SponsorCode=@SponsorCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorCode", SqlDbType.VarChar,64)			};
			parameters[0].Value = SponsorCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SponsorIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sponsor ");
			strSql.Append(" where SponsorID in ("+SponsorIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sponsor GetModel(int SponsorID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SponsorID,SponsorCode,SponsorName1,SponsorName2,SponsorName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Sponsor ");
			strSql.Append(" where SponsorID=@SponsorID");
			SqlParameter[] parameters = {
					new SqlParameter("@SponsorID", SqlDbType.Int,4)
			};
			parameters[0].Value = SponsorID;

			Edge.SVA.Model.Sponsor model=new Edge.SVA.Model.Sponsor();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SponsorID"]!=null && ds.Tables[0].Rows[0]["SponsorID"].ToString()!="")
				{
					model.SponsorID=int.Parse(ds.Tables[0].Rows[0]["SponsorID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SponsorCode"]!=null && ds.Tables[0].Rows[0]["SponsorCode"].ToString()!="")
				{
					model.SponsorCode=ds.Tables[0].Rows[0]["SponsorCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SponsorName1"]!=null && ds.Tables[0].Rows[0]["SponsorName1"].ToString()!="")
				{
					model.SponsorName1=ds.Tables[0].Rows[0]["SponsorName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SponsorName2"]!=null && ds.Tables[0].Rows[0]["SponsorName2"].ToString()!="")
				{
					model.SponsorName2=ds.Tables[0].Rows[0]["SponsorName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SponsorName3"]!=null && ds.Tables[0].Rows[0]["SponsorName3"].ToString()!="")
				{
					model.SponsorName3=ds.Tables[0].Rows[0]["SponsorName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SponsorID,SponsorCode,SponsorName1,SponsorName2,SponsorName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Sponsor ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SponsorID,SponsorCode,SponsorName1,SponsorName2,SponsorName3,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Sponsor ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Sponsor ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SponsorID desc");
			}
			strSql.Append(")AS Row, T.*  from Sponsor T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Sponsor";
			parameters[1].Value = "SponsorID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

