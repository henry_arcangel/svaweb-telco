﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Address
	/// </summary>
	public partial class Address:IAddress
	{
		public Address()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string AddressID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Address");
			strSql.Append(" where AddressID=@AddressID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AddressID", SqlDbType.VarChar,36)};
			parameters[0].Value = AddressID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Address model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Address(");
			strSql.Append("AddressID,AddressUnit,AddressFloor,AddressBlock,AddressBuilding,AddressStreetNo,AddressStreet,AddressDistrict,AddressCity,AddressProvince,AddressCountry,AddressZipCode,AddressLanguage,CreatedOn,UpdatedOn,UpdatedBy,Addr1,Addr2,Addr3,Addr4)");
			strSql.Append(" values (");
			strSql.Append("@AddressID,@AddressUnit,@AddressFloor,@AddressBlock,@AddressBuilding,@AddressStreetNo,@AddressStreet,@AddressDistrict,@AddressCity,@AddressProvince,@AddressCountry,@AddressZipCode,@AddressLanguage,@CreatedOn,@UpdatedOn,@UpdatedBy,@Addr1,@Addr2,@Addr3,@Addr4)");
			SqlParameter[] parameters = {
					new SqlParameter("@AddressID", SqlDbType.VarChar,36),
					new SqlParameter("@AddressUnit", SqlDbType.NVarChar,4),
					new SqlParameter("@AddressFloor", SqlDbType.SmallInt,2),
					new SqlParameter("@AddressBlock", SqlDbType.NVarChar,2),
					new SqlParameter("@AddressBuilding", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressStreetNo", SqlDbType.NVarChar,10),
					new SqlParameter("@AddressStreet", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressDistrict", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressCity", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressProvince", SqlDbType.NVarChar,20),
					new SqlParameter("@AddressCountry", SqlDbType.NVarChar,20),
					new SqlParameter("@AddressZipCode", SqlDbType.NVarChar,10),
					new SqlParameter("@AddressLanguage", SqlDbType.TinyInt,1),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.NVarChar,30),
					new SqlParameter("@Addr1", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr2", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr3", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr4", SqlDbType.NVarChar,200)};
			parameters[0].Value = model.AddressID;
			parameters[1].Value = model.AddressUnit;
			parameters[2].Value = model.AddressFloor;
			parameters[3].Value = model.AddressBlock;
			parameters[4].Value = model.AddressBuilding;
			parameters[5].Value = model.AddressStreetNo;
			parameters[6].Value = model.AddressStreet;
			parameters[7].Value = model.AddressDistrict;
			parameters[8].Value = model.AddressCity;
			parameters[9].Value = model.AddressProvince;
			parameters[10].Value = model.AddressCountry;
			parameters[11].Value = model.AddressZipCode;
			parameters[12].Value = model.AddressLanguage;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.UpdatedOn;
			parameters[15].Value = model.UpdatedBy;
			parameters[16].Value = model.Addr1;
			parameters[17].Value = model.Addr2;
			parameters[18].Value = model.Addr3;
			parameters[19].Value = model.Addr4;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Address model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Address set ");
			strSql.Append("AddressUnit=@AddressUnit,");
			strSql.Append("AddressFloor=@AddressFloor,");
			strSql.Append("AddressBlock=@AddressBlock,");
			strSql.Append("AddressBuilding=@AddressBuilding,");
			strSql.Append("AddressStreetNo=@AddressStreetNo,");
			strSql.Append("AddressStreet=@AddressStreet,");
			strSql.Append("AddressDistrict=@AddressDistrict,");
			strSql.Append("AddressCity=@AddressCity,");
			strSql.Append("AddressProvince=@AddressProvince,");
			strSql.Append("AddressCountry=@AddressCountry,");
			strSql.Append("AddressZipCode=@AddressZipCode,");
			strSql.Append("AddressLanguage=@AddressLanguage,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("Addr1=@Addr1,");
			strSql.Append("Addr2=@Addr2,");
			strSql.Append("Addr3=@Addr3,");
			strSql.Append("Addr4=@Addr4");
			strSql.Append(" where AddressID=@AddressID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AddressUnit", SqlDbType.NVarChar,4),
					new SqlParameter("@AddressFloor", SqlDbType.SmallInt,2),
					new SqlParameter("@AddressBlock", SqlDbType.NVarChar,2),
					new SqlParameter("@AddressBuilding", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressStreetNo", SqlDbType.NVarChar,10),
					new SqlParameter("@AddressStreet", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressDistrict", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressCity", SqlDbType.NVarChar,30),
					new SqlParameter("@AddressProvince", SqlDbType.NVarChar,20),
					new SqlParameter("@AddressCountry", SqlDbType.NVarChar,20),
					new SqlParameter("@AddressZipCode", SqlDbType.NVarChar,10),
					new SqlParameter("@AddressLanguage", SqlDbType.TinyInt,1),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.NVarChar,30),
					new SqlParameter("@Addr1", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr2", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr3", SqlDbType.NVarChar,200),
					new SqlParameter("@Addr4", SqlDbType.NVarChar,200),
					new SqlParameter("@AddressID", SqlDbType.VarChar,36)};
			parameters[0].Value = model.AddressUnit;
			parameters[1].Value = model.AddressFloor;
			parameters[2].Value = model.AddressBlock;
			parameters[3].Value = model.AddressBuilding;
			parameters[4].Value = model.AddressStreetNo;
			parameters[5].Value = model.AddressStreet;
			parameters[6].Value = model.AddressDistrict;
			parameters[7].Value = model.AddressCity;
			parameters[8].Value = model.AddressProvince;
			parameters[9].Value = model.AddressCountry;
			parameters[10].Value = model.AddressZipCode;
			parameters[11].Value = model.AddressLanguage;
			parameters[12].Value = model.CreatedOn;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.UpdatedBy;
			parameters[15].Value = model.Addr1;
			parameters[16].Value = model.Addr2;
			parameters[17].Value = model.Addr3;
			parameters[18].Value = model.Addr4;
			parameters[19].Value = model.AddressID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string AddressID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Address ");
			strSql.Append(" where AddressID=@AddressID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AddressID", SqlDbType.VarChar,36)};
			parameters[0].Value = AddressID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string AddressIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Address ");
			strSql.Append(" where AddressID in ("+AddressIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Address GetModel(string AddressID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 AddressID,AddressUnit,AddressFloor,AddressBlock,AddressBuilding,AddressStreetNo,AddressStreet,AddressDistrict,AddressCity,AddressProvince,AddressCountry,AddressZipCode,AddressLanguage,CreatedOn,UpdatedOn,UpdatedBy,Addr1,Addr2,Addr3,Addr4 from Address ");
			strSql.Append(" where AddressID=@AddressID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AddressID", SqlDbType.VarChar,36)};
			parameters[0].Value = AddressID;

			Edge.SVA.Model.Address model=new Edge.SVA.Model.Address();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["AddressID"]!=null && ds.Tables[0].Rows[0]["AddressID"].ToString()!="")
				{
					model.AddressID=ds.Tables[0].Rows[0]["AddressID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressUnit"]!=null && ds.Tables[0].Rows[0]["AddressUnit"].ToString()!="")
				{
					model.AddressUnit=ds.Tables[0].Rows[0]["AddressUnit"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressFloor"]!=null && ds.Tables[0].Rows[0]["AddressFloor"].ToString()!="")
				{
					model.AddressFloor=int.Parse(ds.Tables[0].Rows[0]["AddressFloor"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AddressBlock"]!=null && ds.Tables[0].Rows[0]["AddressBlock"].ToString()!="")
				{
					model.AddressBlock=ds.Tables[0].Rows[0]["AddressBlock"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressBuilding"]!=null && ds.Tables[0].Rows[0]["AddressBuilding"].ToString()!="")
				{
					model.AddressBuilding=ds.Tables[0].Rows[0]["AddressBuilding"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressStreetNo"]!=null && ds.Tables[0].Rows[0]["AddressStreetNo"].ToString()!="")
				{
					model.AddressStreetNo=ds.Tables[0].Rows[0]["AddressStreetNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressStreet"]!=null && ds.Tables[0].Rows[0]["AddressStreet"].ToString()!="")
				{
					model.AddressStreet=ds.Tables[0].Rows[0]["AddressStreet"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressDistrict"]!=null && ds.Tables[0].Rows[0]["AddressDistrict"].ToString()!="")
				{
					model.AddressDistrict=ds.Tables[0].Rows[0]["AddressDistrict"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressCity"]!=null && ds.Tables[0].Rows[0]["AddressCity"].ToString()!="")
				{
					model.AddressCity=ds.Tables[0].Rows[0]["AddressCity"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressProvince"]!=null && ds.Tables[0].Rows[0]["AddressProvince"].ToString()!="")
				{
					model.AddressProvince=ds.Tables[0].Rows[0]["AddressProvince"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressCountry"]!=null && ds.Tables[0].Rows[0]["AddressCountry"].ToString()!="")
				{
					model.AddressCountry=ds.Tables[0].Rows[0]["AddressCountry"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressZipCode"]!=null && ds.Tables[0].Rows[0]["AddressZipCode"].ToString()!="")
				{
					model.AddressZipCode=ds.Tables[0].Rows[0]["AddressZipCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddressLanguage"]!=null && ds.Tables[0].Rows[0]["AddressLanguage"].ToString()!="")
				{
					model.AddressLanguage=int.Parse(ds.Tables[0].Rows[0]["AddressLanguage"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Addr1"]!=null && ds.Tables[0].Rows[0]["Addr1"].ToString()!="")
				{
					model.Addr1=ds.Tables[0].Rows[0]["Addr1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Addr2"]!=null && ds.Tables[0].Rows[0]["Addr2"].ToString()!="")
				{
					model.Addr2=ds.Tables[0].Rows[0]["Addr2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Addr3"]!=null && ds.Tables[0].Rows[0]["Addr3"].ToString()!="")
				{
					model.Addr3=ds.Tables[0].Rows[0]["Addr3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Addr4"]!=null && ds.Tables[0].Rows[0]["Addr4"].ToString()!="")
				{
					model.Addr4=ds.Tables[0].Rows[0]["Addr4"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select AddressID,AddressUnit,AddressFloor,AddressBlock,AddressBuilding,AddressStreetNo,AddressStreet,AddressDistrict,AddressCity,AddressProvince,AddressCountry,AddressZipCode,AddressLanguage,CreatedOn,UpdatedOn,UpdatedBy,Addr1,Addr2,Addr3,Addr4 ");
			strSql.Append(" FROM Address ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" AddressID,AddressUnit,AddressFloor,AddressBlock,AddressBuilding,AddressStreetNo,AddressStreet,AddressDistrict,AddressCity,AddressProvince,AddressCountry,AddressZipCode,AddressLanguage,CreatedOn,UpdatedOn,UpdatedBy,Addr1,Addr2,Addr3,Addr4 ");
			strSql.Append(" FROM Address ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


        ///<summary>
        ///分页获取数据列表
        ///</summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            /*
             *    @tblName varchar(255),   -- 表名 
                  @fldName varchar(255),   -- 显示字段名 
                  @OrderfldName varchar(255),  -- 排序字段名 
                  @StatfldName varchar(255),  -- 统计字段名 
                  @PageSize int = 10,   -- 页尺寸 
                  @PageIndex int = 1,   -- 页码 
                  @IsReCount bit = 0,   -- 返回记录总数, 非 0 值则返回 
                  @OrderType bit = 0,   -- 设置排序类型, 非 0 值则降序 
                  @strWhere varchar(1000) = ''  -- 查询条件 (注意: 不要加 where) */
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
                    };
            parameters[0].Value = "Address";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Address ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
        
		#endregion  Method
	}
}

