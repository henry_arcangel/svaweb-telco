﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:InventoryReplenishRule_D
    /// </summary>
    public partial class InventoryReplenishRule_D : IInventoryReplenishRule_D
    {
        public InventoryReplenishRule_D()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from InventoryReplenishRule_D");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.InventoryReplenishRule_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into InventoryReplenishRule_D(");
            strSql.Append("InventoryReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority, ReplenishType,MinAmtBalance,RunningAmtBalance,MinPointBalance,RunningPointBalance)");
            strSql.Append(" values (");
            strSql.Append("@InventoryReplenishCode,@StoreID,@OrderTargetID,@MinStockQty,@RunningStockQty,@OrderRoundUpQty,@Priority, @ReplenishType,@MinAmtBalance,@RunningAmtBalance,@MinPointBalance,@RunningPointBalance)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@OrderTargetID", SqlDbType.Int,4),
					new SqlParameter("@MinStockQty", SqlDbType.Int,4),
					new SqlParameter("@RunningStockQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
					new SqlParameter("@Priority", SqlDbType.Int,4),
					new SqlParameter("@ReplenishType", SqlDbType.Int,4),
					new SqlParameter("@MinAmtBalance", SqlDbType.Money,8),
					new SqlParameter("@RunningAmtBalance", SqlDbType.Money,8),
					new SqlParameter("@MinPointBalance", SqlDbType.Int,4),
					new SqlParameter("@RunningPointBalance", SqlDbType.Int,4)};
            parameters[0].Value = model.InventoryReplenishCode;
            parameters[1].Value = model.StoreID;
            parameters[2].Value = model.OrderTargetID;
            parameters[3].Value = model.MinStockQty;
            parameters[4].Value = model.RunningStockQty;
            parameters[5].Value = model.OrderRoundUpQty;
            parameters[6].Value = model.Priority;
            parameters[7].Value = model.ReplenishType;
            parameters[8].Value = model.MinAmtBalance;
            parameters[9].Value = model.RunningAmtBalance;
            parameters[10].Value = model.MinPointBalance;
            parameters[11].Value = model.RunningPointBalance;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.InventoryReplenishRule_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update InventoryReplenishRule_D set ");
            strSql.Append("InventoryReplenishCode=@InventoryReplenishCode,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("OrderTargetID=@OrderTargetID,");
            strSql.Append("MinStockQty=@MinStockQty,");
            strSql.Append("RunningStockQty=@RunningStockQty,");
            strSql.Append("OrderRoundUpQty=@OrderRoundUpQty,");
            strSql.Append("Priority=@Priority,");
            strSql.Append("ReplenishType=@ReplenishType,");
            strSql.Append("MinAmtBalance=@MinAmtBalance,");
            strSql.Append("RunningAmtBalance=@RunningAmtBalance,");
            strSql.Append("MinPointBalance=@MinPointBalance,");
            strSql.Append("RunningPointBalance=@RunningPointBalance");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@OrderTargetID", SqlDbType.Int,4),
					new SqlParameter("@MinStockQty", SqlDbType.Int,4),
					new SqlParameter("@RunningStockQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
					new SqlParameter("@Priority", SqlDbType.Int,4),
					new SqlParameter("@ReplenishType", SqlDbType.Int,4),
					new SqlParameter("@MinAmtBalance", SqlDbType.Money,8),
					new SqlParameter("@RunningAmtBalance", SqlDbType.Money,8),
					new SqlParameter("@MinPointBalance", SqlDbType.Int,4),
					new SqlParameter("@RunningPointBalance", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
            parameters[0].Value = model.InventoryReplenishCode;
            parameters[1].Value = model.StoreID;
            parameters[2].Value = model.OrderTargetID;
            parameters[3].Value = model.MinStockQty;
            parameters[4].Value = model.RunningStockQty;
            parameters[5].Value = model.OrderRoundUpQty;
            parameters[6].Value = model.Priority;
            parameters[7].Value = model.ReplenishType;
            parameters[8].Value = model.MinAmtBalance;
            parameters[9].Value = model.RunningAmtBalance;
            parameters[10].Value = model.MinPointBalance;
            parameters[11].Value = model.RunningPointBalance;
            parameters[12].Value = model.KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InventoryReplenishRule_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InventoryReplenishRule_D ");
            strSql.Append(" where KeyID in (" + KeyIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.InventoryReplenishRule_D GetModel(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 KeyID,InventoryReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority, ReplenishType,MinAmtBalance,RunningAmtBalance,MinPointBalance,RunningPointBalance from InventoryReplenishRule_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            Edge.SVA.Model.InventoryReplenishRule_D model = new Edge.SVA.Model.InventoryReplenishRule_D();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["KeyID"] != null && ds.Tables[0].Rows[0]["KeyID"].ToString() != "")
                {
                    model.KeyID = int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InventoryReplenishCode"] != null && ds.Tables[0].Rows[0]["InventoryReplenishCode"].ToString() != "")
                {
                    model.InventoryReplenishCode = ds.Tables[0].Rows[0]["InventoryReplenishCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderTargetID"] != null && ds.Tables[0].Rows[0]["OrderTargetID"].ToString() != "")
                {
                    model.OrderTargetID = int.Parse(ds.Tables[0].Rows[0]["OrderTargetID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MinStockQty"] != null && ds.Tables[0].Rows[0]["MinStockQty"].ToString() != "")
                {
                    model.MinStockQty = int.Parse(ds.Tables[0].Rows[0]["MinStockQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RunningStockQty"] != null && ds.Tables[0].Rows[0]["RunningStockQty"].ToString() != "")
                {
                    model.RunningStockQty = int.Parse(ds.Tables[0].Rows[0]["RunningStockQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderRoundUpQty"] != null && ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString() != "")
                {
                    model.OrderRoundUpQty = int.Parse(ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Priority"] != null && ds.Tables[0].Rows[0]["Priority"].ToString() != "")
                {
                    model.Priority = int.Parse(ds.Tables[0].Rows[0]["Priority"].ToString());
                }

                if (ds.Tables[0].Rows[0]["ReplenishType"] != null && ds.Tables[0].Rows[0]["ReplenishType"].ToString() != "")
                {
                    model.ReplenishType = int.Parse(ds.Tables[0].Rows[0]["ReplenishType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MinAmtBalance"] != null && ds.Tables[0].Rows[0]["MinAmtBalance"].ToString() != "")
                {
                    model.MinAmtBalance = decimal.Parse(ds.Tables[0].Rows[0]["MinAmtBalance"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RunningAmtBalance"] != null && ds.Tables[0].Rows[0]["RunningAmtBalance"].ToString() != "")
                {
                    model.RunningAmtBalance = decimal.Parse(ds.Tables[0].Rows[0]["RunningAmtBalance"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MinPointBalance"] != null && ds.Tables[0].Rows[0]["MinPointBalance"].ToString() != "")
                {
                    model.MinPointBalance = int.Parse(ds.Tables[0].Rows[0]["MinPointBalance"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RunningPointBalance"] != null && ds.Tables[0].Rows[0]["RunningPointBalance"].ToString() != "")
                {
                    model.RunningPointBalance = int.Parse(ds.Tables[0].Rows[0]["RunningPointBalance"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select KeyID,InventoryReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority, ReplenishType,MinAmtBalance,RunningAmtBalance,MinPointBalance,RunningPointBalance ");
            strSql.Append(" FROM InventoryReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" KeyID,InventoryReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority, ReplenishType,MinAmtBalance,RunningAmtBalance,MinPointBalance,RunningPointBalance ");
            strSql.Append(" FROM InventoryReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM InventoryReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.KeyID desc");
            }
            strSql.Append(")AS Row, T.*  from InventoryReplenishRule_D T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "InventoryReplenishRule_D";
            parameters[1].Value = "KeyID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method

        public bool DeleteByInventoryReplenishCode(string inventoryReplenishCode)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from InventoryReplenishRule_D where InventoryReplenishCode = @InventoryReplenishCode");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@InventoryReplenishCode",SqlDbType.VarChar,64){ Value = inventoryReplenishCode }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}

