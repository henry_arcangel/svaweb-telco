﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Sales_H
	/// </summary>
	public partial class Sales_H:ISales_H
	{
		public Sales_H()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string TxnNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Sales_H");
			strSql.Append(" where TxnNo=@TxnNo ");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512)			};
			parameters[0].Value = TxnNo;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Sales_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Sales_H(");
			strSql.Append("TxnNo,StoreCode,RegisterCode,ServerCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,Channel,TotalAmount,Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliverBy,DeliveryAddress,DeliverStartOn,DeliverEndOn,DeliveryLongitude,DeliveryLatitude,Contact,ContactPhone,Approvedby,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ApprovalCode,SalesReceipt,SalesReceiptBIN,BrandCode,SalesTag,Additional,LogisticsProviderID,PaymentDoneOn,DeliveryNumber,PickupType,PickupStoreCode,CODFlag)");
			strSql.Append(" values (");
			strSql.Append("@TxnNo,@StoreCode,@RegisterCode,@ServerCode,@BusDate,@TxnDate,@SalesType,@CashierID,@SalesManID,@Channel,@TotalAmount,@Status,@MemberID,@MemberName,@MemberMobilePhone,@CardNumber,@MemberAddress,@DeliverBy,@DeliveryAddress,@DeliverStartOn,@DeliverEndOn,@DeliveryLongitude,@DeliveryLatitude,@Contact,@ContactPhone,@Approvedby,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@ApprovalCode,@SalesReceipt,@SalesReceiptBIN,@BrandCode,@SalesTag,@Additional,@LogisticsProviderID,@PaymentDoneOn,@DeliveryNumber,@PickupType,@PickupStoreCode,@CODFlag)");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@SalesType", SqlDbType.Int,4),
					new SqlParameter("@CashierID", SqlDbType.VarChar,512),
					new SqlParameter("@SalesManID", SqlDbType.VarChar,512),
					new SqlParameter("@Channel", SqlDbType.VarChar,512),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@MemberName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberMobilePhone", SqlDbType.VarChar,512),
					new SqlParameter("@CardNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@DeliverBy", SqlDbType.VarChar,512),
					new SqlParameter("@DeliveryAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@DeliverStartOn", SqlDbType.DateTime),
					new SqlParameter("@DeliverEndOn", SqlDbType.DateTime),
					new SqlParameter("@DeliveryLongitude", SqlDbType.VarChar,512),
					new SqlParameter("@DeliveryLatitude", SqlDbType.VarChar,512),
					new SqlParameter("@Contact", SqlDbType.NVarChar,512),
					new SqlParameter("@ContactPhone", SqlDbType.VarChar,512),
					new SqlParameter("@Approvedby", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@SalesReceipt", SqlDbType.VarChar),
					new SqlParameter("@SalesReceiptBIN", SqlDbType.VarBinary),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,64),
					new SqlParameter("@SalesTag", SqlDbType.VarChar,512),
					new SqlParameter("@Additional", SqlDbType.NVarChar,512),
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4),
					new SqlParameter("@PaymentDoneOn", SqlDbType.DateTime),
					new SqlParameter("@DeliveryNumber", SqlDbType.VarChar,512),
					new SqlParameter("@PickupType", SqlDbType.Int,4),
					new SqlParameter("@PickupStoreCode", SqlDbType.VarChar,64),
					new SqlParameter("@CODFlag", SqlDbType.Int,4)};
			parameters[0].Value = model.TxnNo;
			parameters[1].Value = model.StoreCode;
			parameters[2].Value = model.RegisterCode;
			parameters[3].Value = model.ServerCode;
			parameters[4].Value = model.BusDate;
			parameters[5].Value = model.TxnDate;
			parameters[6].Value = model.SalesType;
			parameters[7].Value = model.CashierID;
			parameters[8].Value = model.SalesManID;
			parameters[9].Value = model.Channel;
			parameters[10].Value = model.TotalAmount;
			parameters[11].Value = model.Status;
			parameters[12].Value = model.MemberID;
			parameters[13].Value = model.MemberName;
			parameters[14].Value = model.MemberMobilePhone;
			parameters[15].Value = model.CardNumber;
			parameters[16].Value = model.MemberAddress;
			parameters[17].Value = model.DeliverBy;
			parameters[18].Value = model.DeliveryAddress;
			parameters[19].Value = model.DeliverStartOn;
			parameters[20].Value = model.DeliverEndOn;
			parameters[21].Value = model.DeliveryLongitude;
			parameters[22].Value = model.DeliveryLatitude;
			parameters[23].Value = model.Contact;
			parameters[24].Value = model.ContactPhone;
			parameters[25].Value = model.Approvedby;
			parameters[26].Value = model.CreatedOn;
			parameters[27].Value = model.CreatedBy;
			parameters[28].Value = model.UpdatedOn;
			parameters[29].Value = model.UpdatedBy;
			parameters[30].Value = model.ApprovalCode;
			parameters[31].Value = model.SalesReceipt;
			parameters[32].Value = model.SalesReceiptBIN;
			parameters[33].Value = model.BrandCode;
			parameters[34].Value = model.SalesTag;
			parameters[35].Value = model.Additional;
			parameters[36].Value = model.LogisticsProviderID;
			parameters[37].Value = model.PaymentDoneOn;
			parameters[38].Value = model.DeliveryNumber;
			parameters[39].Value = model.PickupType;
			parameters[40].Value = model.PickupStoreCode;
			parameters[41].Value = model.CODFlag;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sales_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Sales_H set ");
			strSql.Append("StoreCode=@StoreCode,");
			strSql.Append("RegisterCode=@RegisterCode,");
			strSql.Append("ServerCode=@ServerCode,");
			strSql.Append("BusDate=@BusDate,");
			strSql.Append("TxnDate=@TxnDate,");
			strSql.Append("SalesType=@SalesType,");
			strSql.Append("CashierID=@CashierID,");
			strSql.Append("SalesManID=@SalesManID,");
			strSql.Append("Channel=@Channel,");
			strSql.Append("TotalAmount=@TotalAmount,");
			strSql.Append("Status=@Status,");
			strSql.Append("MemberID=@MemberID,");
			strSql.Append("MemberName=@MemberName,");
			strSql.Append("MemberMobilePhone=@MemberMobilePhone,");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("MemberAddress=@MemberAddress,");
			strSql.Append("DeliverBy=@DeliverBy,");
			strSql.Append("DeliveryAddress=@DeliveryAddress,");
			strSql.Append("DeliverStartOn=@DeliverStartOn,");
			strSql.Append("DeliverEndOn=@DeliverEndOn,");
			strSql.Append("DeliveryLongitude=@DeliveryLongitude,");
			strSql.Append("DeliveryLatitude=@DeliveryLatitude,");
			strSql.Append("Contact=@Contact,");
			strSql.Append("ContactPhone=@ContactPhone,");
			strSql.Append("Approvedby=@Approvedby,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("ApprovalCode=@ApprovalCode,");
			strSql.Append("SalesReceipt=@SalesReceipt,");
			strSql.Append("SalesReceiptBIN=@SalesReceiptBIN,");
			strSql.Append("BrandCode=@BrandCode,");
			strSql.Append("SalesTag=@SalesTag,");
			strSql.Append("Additional=@Additional,");
			strSql.Append("LogisticsProviderID=@LogisticsProviderID,");
			strSql.Append("PaymentDoneOn=@PaymentDoneOn,");
			strSql.Append("DeliveryNumber=@DeliveryNumber,");
			strSql.Append("PickupType=@PickupType,");
			strSql.Append("PickupStoreCode=@PickupStoreCode,");
			strSql.Append("CODFlag=@CODFlag");
			strSql.Append(" where TxnNo=@TxnNo ");
			SqlParameter[] parameters = {
					new SqlParameter("@StoreCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@SalesType", SqlDbType.Int,4),
					new SqlParameter("@CashierID", SqlDbType.VarChar,512),
					new SqlParameter("@SalesManID", SqlDbType.VarChar,512),
					new SqlParameter("@Channel", SqlDbType.VarChar,512),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@MemberID", SqlDbType.Int,4),
					new SqlParameter("@MemberName", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberMobilePhone", SqlDbType.VarChar,512),
					new SqlParameter("@CardNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@MemberAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@DeliverBy", SqlDbType.VarChar,512),
					new SqlParameter("@DeliveryAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@DeliverStartOn", SqlDbType.DateTime),
					new SqlParameter("@DeliverEndOn", SqlDbType.DateTime),
					new SqlParameter("@DeliveryLongitude", SqlDbType.VarChar,512),
					new SqlParameter("@DeliveryLatitude", SqlDbType.VarChar,512),
					new SqlParameter("@Contact", SqlDbType.NVarChar,512),
					new SqlParameter("@ContactPhone", SqlDbType.VarChar,512),
					new SqlParameter("@Approvedby", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@SalesReceipt", SqlDbType.VarChar),
					new SqlParameter("@SalesReceiptBIN", SqlDbType.VarBinary),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,64),
					new SqlParameter("@SalesTag", SqlDbType.VarChar,512),
					new SqlParameter("@Additional", SqlDbType.NVarChar,512),
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4),
					new SqlParameter("@PaymentDoneOn", SqlDbType.DateTime),
					new SqlParameter("@DeliveryNumber", SqlDbType.VarChar,512),
					new SqlParameter("@PickupType", SqlDbType.Int,4),
					new SqlParameter("@PickupStoreCode", SqlDbType.VarChar,64),
					new SqlParameter("@CODFlag", SqlDbType.Int,4),
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512)};
			parameters[0].Value = model.StoreCode;
			parameters[1].Value = model.RegisterCode;
			parameters[2].Value = model.ServerCode;
			parameters[3].Value = model.BusDate;
			parameters[4].Value = model.TxnDate;
			parameters[5].Value = model.SalesType;
			parameters[6].Value = model.CashierID;
			parameters[7].Value = model.SalesManID;
			parameters[8].Value = model.Channel;
			parameters[9].Value = model.TotalAmount;
			parameters[10].Value = model.Status;
			parameters[11].Value = model.MemberID;
			parameters[12].Value = model.MemberName;
			parameters[13].Value = model.MemberMobilePhone;
			parameters[14].Value = model.CardNumber;
			parameters[15].Value = model.MemberAddress;
			parameters[16].Value = model.DeliverBy;
			parameters[17].Value = model.DeliveryAddress;
			parameters[18].Value = model.DeliverStartOn;
			parameters[19].Value = model.DeliverEndOn;
			parameters[20].Value = model.DeliveryLongitude;
			parameters[21].Value = model.DeliveryLatitude;
			parameters[22].Value = model.Contact;
			parameters[23].Value = model.ContactPhone;
			parameters[24].Value = model.Approvedby;
			parameters[25].Value = model.CreatedOn;
			parameters[26].Value = model.CreatedBy;
			parameters[27].Value = model.UpdatedOn;
			parameters[28].Value = model.UpdatedBy;
			parameters[29].Value = model.ApprovalCode;
			parameters[30].Value = model.SalesReceipt;
			parameters[31].Value = model.SalesReceiptBIN;
			parameters[32].Value = model.BrandCode;
			parameters[33].Value = model.SalesTag;
			parameters[34].Value = model.Additional;
			parameters[35].Value = model.LogisticsProviderID;
			parameters[36].Value = model.PaymentDoneOn;
			parameters[37].Value = model.DeliveryNumber;
			parameters[38].Value = model.PickupType;
			parameters[39].Value = model.PickupStoreCode;
			parameters[40].Value = model.CODFlag;
			parameters[41].Value = model.TxnNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TxnNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sales_H ");
			strSql.Append(" where TxnNo=@TxnNo ");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512)			};
			parameters[0].Value = TxnNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TxnNolist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sales_H ");
			strSql.Append(" where TxnNo in ("+TxnNolist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sales_H GetModel(string TxnNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TxnNo,StoreCode,RegisterCode,ServerCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,Channel,TotalAmount,Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliverBy,DeliveryAddress,DeliverStartOn,DeliverEndOn,DeliveryLongitude,DeliveryLatitude,Contact,ContactPhone,Approvedby,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ApprovalCode,SalesReceipt,SalesReceiptBIN,BrandCode,SalesTag,Additional,LogisticsProviderID,PaymentDoneOn,DeliveryNumber,PickupType,PickupStoreCode,CODFlag from Sales_H ");
			strSql.Append(" where TxnNo=@TxnNo ");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512)			};
			parameters[0].Value = TxnNo;

			Edge.SVA.Model.Sales_H model=new Edge.SVA.Model.Sales_H();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["TxnNo"]!=null && ds.Tables[0].Rows[0]["TxnNo"].ToString()!="")
				{
					model.TxnNo=ds.Tables[0].Rows[0]["TxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["StoreCode"]!=null && ds.Tables[0].Rows[0]["StoreCode"].ToString()!="")
				{
					model.StoreCode=ds.Tables[0].Rows[0]["StoreCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RegisterCode"]!=null && ds.Tables[0].Rows[0]["RegisterCode"].ToString()!="")
				{
					model.RegisterCode=ds.Tables[0].Rows[0]["RegisterCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ServerCode"]!=null && ds.Tables[0].Rows[0]["ServerCode"].ToString()!="")
				{
					model.ServerCode=ds.Tables[0].Rows[0]["ServerCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BusDate"]!=null && ds.Tables[0].Rows[0]["BusDate"].ToString()!="")
				{
					model.BusDate=DateTime.Parse(ds.Tables[0].Rows[0]["BusDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TxnDate"]!=null && ds.Tables[0].Rows[0]["TxnDate"].ToString()!="")
				{
					model.TxnDate=DateTime.Parse(ds.Tables[0].Rows[0]["TxnDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SalesType"]!=null && ds.Tables[0].Rows[0]["SalesType"].ToString()!="")
				{
					model.SalesType=int.Parse(ds.Tables[0].Rows[0]["SalesType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CashierID"]!=null && ds.Tables[0].Rows[0]["CashierID"].ToString()!="")
				{
					model.CashierID=ds.Tables[0].Rows[0]["CashierID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SalesManID"]!=null && ds.Tables[0].Rows[0]["SalesManID"].ToString()!="")
				{
					model.SalesManID=ds.Tables[0].Rows[0]["SalesManID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Channel"]!=null && ds.Tables[0].Rows[0]["Channel"].ToString()!="")
				{
					model.Channel=ds.Tables[0].Rows[0]["Channel"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TotalAmount"]!=null && ds.Tables[0].Rows[0]["TotalAmount"].ToString()!="")
				{
					model.TotalAmount=decimal.Parse(ds.Tables[0].Rows[0]["TotalAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MemberID"]!=null && ds.Tables[0].Rows[0]["MemberID"].ToString()!="")
				{
					model.MemberID=int.Parse(ds.Tables[0].Rows[0]["MemberID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MemberName"]!=null && ds.Tables[0].Rows[0]["MemberName"].ToString()!="")
				{
					model.MemberName=ds.Tables[0].Rows[0]["MemberName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MemberMobilePhone"]!=null && ds.Tables[0].Rows[0]["MemberMobilePhone"].ToString()!="")
				{
					model.MemberMobilePhone=ds.Tables[0].Rows[0]["MemberMobilePhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MemberAddress"]!=null && ds.Tables[0].Rows[0]["MemberAddress"].ToString()!="")
				{
					model.MemberAddress=ds.Tables[0].Rows[0]["MemberAddress"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DeliverBy"]!=null && ds.Tables[0].Rows[0]["DeliverBy"].ToString()!="")
				{
					model.DeliverBy=ds.Tables[0].Rows[0]["DeliverBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DeliveryAddress"]!=null && ds.Tables[0].Rows[0]["DeliveryAddress"].ToString()!="")
				{
					model.DeliveryAddress=ds.Tables[0].Rows[0]["DeliveryAddress"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DeliverStartOn"]!=null && ds.Tables[0].Rows[0]["DeliverStartOn"].ToString()!="")
				{
					model.DeliverStartOn=DateTime.Parse(ds.Tables[0].Rows[0]["DeliverStartOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DeliverEndOn"]!=null && ds.Tables[0].Rows[0]["DeliverEndOn"].ToString()!="")
				{
					model.DeliverEndOn=DateTime.Parse(ds.Tables[0].Rows[0]["DeliverEndOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DeliveryLongitude"]!=null && ds.Tables[0].Rows[0]["DeliveryLongitude"].ToString()!="")
				{
					model.DeliveryLongitude=ds.Tables[0].Rows[0]["DeliveryLongitude"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DeliveryLatitude"]!=null && ds.Tables[0].Rows[0]["DeliveryLatitude"].ToString()!="")
				{
					model.DeliveryLatitude=ds.Tables[0].Rows[0]["DeliveryLatitude"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Contact"]!=null && ds.Tables[0].Rows[0]["Contact"].ToString()!="")
				{
					model.Contact=ds.Tables[0].Rows[0]["Contact"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ContactPhone"]!=null && ds.Tables[0].Rows[0]["ContactPhone"].ToString()!="")
				{
					model.ContactPhone=ds.Tables[0].Rows[0]["ContactPhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Approvedby"]!=null && ds.Tables[0].Rows[0]["Approvedby"].ToString()!="")
				{
					model.Approvedby=ds.Tables[0].Rows[0]["Approvedby"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApprovalCode"]!=null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString()!="")
				{
					model.ApprovalCode=ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SalesReceipt"]!=null && ds.Tables[0].Rows[0]["SalesReceipt"].ToString()!="")
				{
					model.SalesReceipt=ds.Tables[0].Rows[0]["SalesReceipt"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SalesReceiptBIN"]!=null && ds.Tables[0].Rows[0]["SalesReceiptBIN"].ToString()!="")
				{
					model.SalesReceiptBIN=(byte[])ds.Tables[0].Rows[0]["SalesReceiptBIN"];
				}
				if(ds.Tables[0].Rows[0]["BrandCode"]!=null && ds.Tables[0].Rows[0]["BrandCode"].ToString()!="")
				{
					model.BrandCode=ds.Tables[0].Rows[0]["BrandCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SalesTag"]!=null && ds.Tables[0].Rows[0]["SalesTag"].ToString()!="")
				{
					model.SalesTag=ds.Tables[0].Rows[0]["SalesTag"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Additional"]!=null && ds.Tables[0].Rows[0]["Additional"].ToString()!="")
				{
					model.Additional=ds.Tables[0].Rows[0]["Additional"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LogisticsProviderID"]!=null && ds.Tables[0].Rows[0]["LogisticsProviderID"].ToString()!="")
				{
					model.LogisticsProviderID=int.Parse(ds.Tables[0].Rows[0]["LogisticsProviderID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PaymentDoneOn"]!=null && ds.Tables[0].Rows[0]["PaymentDoneOn"].ToString()!="")
				{
					model.PaymentDoneOn=DateTime.Parse(ds.Tables[0].Rows[0]["PaymentDoneOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DeliveryNumber"]!=null && ds.Tables[0].Rows[0]["DeliveryNumber"].ToString()!="")
				{
					model.DeliveryNumber=ds.Tables[0].Rows[0]["DeliveryNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PickupType"]!=null && ds.Tables[0].Rows[0]["PickupType"].ToString()!="")
				{
					model.PickupType=int.Parse(ds.Tables[0].Rows[0]["PickupType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PickupStoreCode"]!=null && ds.Tables[0].Rows[0]["PickupStoreCode"].ToString()!="")
				{
					model.PickupStoreCode=ds.Tables[0].Rows[0]["PickupStoreCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CODFlag"]!=null && ds.Tables[0].Rows[0]["CODFlag"].ToString()!="")
				{
					model.CODFlag=int.Parse(ds.Tables[0].Rows[0]["CODFlag"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TxnNo,StoreCode,RegisterCode,ServerCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,Channel,TotalAmount,Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliverBy,DeliveryAddress,DeliverStartOn,DeliverEndOn,DeliveryLongitude,DeliveryLatitude,Contact,ContactPhone,Approvedby,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ApprovalCode,SalesReceipt,SalesReceiptBIN,BrandCode,SalesTag,Additional,LogisticsProviderID,PaymentDoneOn,DeliveryNumber,PickupType,PickupStoreCode,CODFlag ");
			strSql.Append(" FROM Sales_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TxnNo,StoreCode,RegisterCode,ServerCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,Channel,TotalAmount,Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliverBy,DeliveryAddress,DeliverStartOn,DeliverEndOn,DeliveryLongitude,DeliveryLatitude,Contact,ContactPhone,Approvedby,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ApprovalCode,SalesReceipt,SalesReceiptBIN,BrandCode,SalesTag,Additional,LogisticsProviderID,PaymentDoneOn,DeliveryNumber,PickupType,PickupStoreCode,CODFlag ");
			strSql.Append(" FROM Sales_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Sales_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TxnNo desc");
			}
			strSql.Append(")AS Row, T.*  from Sales_H T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Sales_H";
			parameters[1].Value = "TxnNo";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

