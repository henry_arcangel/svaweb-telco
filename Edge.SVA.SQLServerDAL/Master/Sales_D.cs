﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Sales_D
	/// </summary>
	public partial class Sales_D:ISales_D
	{
		public Sales_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Sales_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Sales_D");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Sales_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Sales_D(");
			strSql.Append("TxnNo,SeqNo,ProdCode,ProdDesc,Serialno,Collected,RetailPrice,NetPrice,Qty,NetAmount,Additional,POPrice,POReasonID,DiscountType,DiscountOffValue,DiscountOffPrice,DiscountReasonID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ReservedDate,PickupDate)");
			strSql.Append(" values (");
			strSql.Append("@TxnNo,@SeqNo,@ProdCode,@ProdDesc,@Serialno,@Collected,@RetailPrice,@NetPrice,@Qty,@NetAmount,@Additional,@POPrice,@POReasonID,@DiscountType,@DiscountOffValue,@DiscountOffPrice,@DiscountReasonID,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@ReservedDate,@PickupDate)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@SeqNo", SqlDbType.Int,4),
					new SqlParameter("@ProdCode", SqlDbType.VarChar,64),
					new SqlParameter("@ProdDesc", SqlDbType.NVarChar,512),
					new SqlParameter("@Serialno", SqlDbType.VarChar,512),
					new SqlParameter("@Collected", SqlDbType.Int,4),
					new SqlParameter("@RetailPrice", SqlDbType.Money,8),
					new SqlParameter("@NetPrice", SqlDbType.Money,8),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@NetAmount", SqlDbType.Money,8),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@POPrice", SqlDbType.Money,8),
					new SqlParameter("@POReasonID", SqlDbType.Int,4),
					new SqlParameter("@DiscountType", SqlDbType.Int,4),
					new SqlParameter("@DiscountOffValue", SqlDbType.Decimal,9),
					new SqlParameter("@DiscountOffPrice", SqlDbType.Money,8),
					new SqlParameter("@DiscountReasonID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@ReservedDate", SqlDbType.DateTime),
					new SqlParameter("@PickupDate", SqlDbType.DateTime)};
			parameters[0].Value = model.TxnNo;
			parameters[1].Value = model.SeqNo;
			parameters[2].Value = model.ProdCode;
			parameters[3].Value = model.ProdDesc;
			parameters[4].Value = model.Serialno;
			parameters[5].Value = model.Collected;
			parameters[6].Value = model.RetailPrice;
			parameters[7].Value = model.NetPrice;
			parameters[8].Value = model.Qty;
			parameters[9].Value = model.NetAmount;
			parameters[10].Value = model.Additional;
			parameters[11].Value = model.POPrice;
			parameters[12].Value = model.POReasonID;
			parameters[13].Value = model.DiscountType;
			parameters[14].Value = model.DiscountOffValue;
			parameters[15].Value = model.DiscountOffPrice;
			parameters[16].Value = model.DiscountReasonID;
			parameters[17].Value = model.CreatedOn;
			parameters[18].Value = model.CreatedBy;
			parameters[19].Value = model.UpdatedOn;
			parameters[20].Value = model.UpdatedBy;
			parameters[21].Value = model.ReservedDate;
			parameters[22].Value = model.PickupDate;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sales_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Sales_D set ");
			strSql.Append("TxnNo=@TxnNo,");
			strSql.Append("SeqNo=@SeqNo,");
			strSql.Append("ProdCode=@ProdCode,");
			strSql.Append("ProdDesc=@ProdDesc,");
			strSql.Append("Serialno=@Serialno,");
			strSql.Append("Collected=@Collected,");
			strSql.Append("RetailPrice=@RetailPrice,");
			strSql.Append("NetPrice=@NetPrice,");
			strSql.Append("Qty=@Qty,");
			strSql.Append("NetAmount=@NetAmount,");
			strSql.Append("Additional=@Additional,");
			strSql.Append("POPrice=@POPrice,");
			strSql.Append("POReasonID=@POReasonID,");
			strSql.Append("DiscountType=@DiscountType,");
			strSql.Append("DiscountOffValue=@DiscountOffValue,");
			strSql.Append("DiscountOffPrice=@DiscountOffPrice,");
			strSql.Append("DiscountReasonID=@DiscountReasonID,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("ReservedDate=@ReservedDate,");
			strSql.Append("PickupDate=@PickupDate");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@SeqNo", SqlDbType.Int,4),
					new SqlParameter("@ProdCode", SqlDbType.VarChar,64),
					new SqlParameter("@ProdDesc", SqlDbType.NVarChar,512),
					new SqlParameter("@Serialno", SqlDbType.VarChar,512),
					new SqlParameter("@Collected", SqlDbType.Int,4),
					new SqlParameter("@RetailPrice", SqlDbType.Money,8),
					new SqlParameter("@NetPrice", SqlDbType.Money,8),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@NetAmount", SqlDbType.Money,8),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@POPrice", SqlDbType.Money,8),
					new SqlParameter("@POReasonID", SqlDbType.Int,4),
					new SqlParameter("@DiscountType", SqlDbType.Int,4),
					new SqlParameter("@DiscountOffValue", SqlDbType.Decimal,9),
					new SqlParameter("@DiscountOffPrice", SqlDbType.Money,8),
					new SqlParameter("@DiscountReasonID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@ReservedDate", SqlDbType.DateTime),
					new SqlParameter("@PickupDate", SqlDbType.DateTime),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.TxnNo;
			parameters[1].Value = model.SeqNo;
			parameters[2].Value = model.ProdCode;
			parameters[3].Value = model.ProdDesc;
			parameters[4].Value = model.Serialno;
			parameters[5].Value = model.Collected;
			parameters[6].Value = model.RetailPrice;
			parameters[7].Value = model.NetPrice;
			parameters[8].Value = model.Qty;
			parameters[9].Value = model.NetAmount;
			parameters[10].Value = model.Additional;
			parameters[11].Value = model.POPrice;
			parameters[12].Value = model.POReasonID;
			parameters[13].Value = model.DiscountType;
			parameters[14].Value = model.DiscountOffValue;
			parameters[15].Value = model.DiscountOffPrice;
			parameters[16].Value = model.DiscountReasonID;
			parameters[17].Value = model.CreatedOn;
			parameters[18].Value = model.CreatedBy;
			parameters[19].Value = model.UpdatedOn;
			parameters[20].Value = model.UpdatedBy;
			parameters[21].Value = model.ReservedDate;
			parameters[22].Value = model.PickupDate;
			parameters[23].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sales_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sales_D ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sales_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,TxnNo,SeqNo,ProdCode,ProdDesc,Serialno,Collected,RetailPrice,NetPrice,Qty,NetAmount,Additional,POPrice,POReasonID,DiscountType,DiscountOffValue,DiscountOffPrice,DiscountReasonID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ReservedDate,PickupDate from Sales_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Sales_D model=new Edge.SVA.Model.Sales_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TxnNo"]!=null && ds.Tables[0].Rows[0]["TxnNo"].ToString()!="")
				{
					model.TxnNo=ds.Tables[0].Rows[0]["TxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SeqNo"]!=null && ds.Tables[0].Rows[0]["SeqNo"].ToString()!="")
				{
					model.SeqNo=int.Parse(ds.Tables[0].Rows[0]["SeqNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProdCode"]!=null && ds.Tables[0].Rows[0]["ProdCode"].ToString()!="")
				{
					model.ProdCode=ds.Tables[0].Rows[0]["ProdCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProdDesc"]!=null && ds.Tables[0].Rows[0]["ProdDesc"].ToString()!="")
				{
					model.ProdDesc=ds.Tables[0].Rows[0]["ProdDesc"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Serialno"]!=null && ds.Tables[0].Rows[0]["Serialno"].ToString()!="")
				{
					model.Serialno=ds.Tables[0].Rows[0]["Serialno"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Collected"]!=null && ds.Tables[0].Rows[0]["Collected"].ToString()!="")
				{
					model.Collected=int.Parse(ds.Tables[0].Rows[0]["Collected"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RetailPrice"]!=null && ds.Tables[0].Rows[0]["RetailPrice"].ToString()!="")
				{
					model.RetailPrice=decimal.Parse(ds.Tables[0].Rows[0]["RetailPrice"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NetPrice"]!=null && ds.Tables[0].Rows[0]["NetPrice"].ToString()!="")
				{
					model.NetPrice=decimal.Parse(ds.Tables[0].Rows[0]["NetPrice"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Qty"]!=null && ds.Tables[0].Rows[0]["Qty"].ToString()!="")
				{
					model.Qty=int.Parse(ds.Tables[0].Rows[0]["Qty"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NetAmount"]!=null && ds.Tables[0].Rows[0]["NetAmount"].ToString()!="")
				{
					model.NetAmount=decimal.Parse(ds.Tables[0].Rows[0]["NetAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Additional"]!=null && ds.Tables[0].Rows[0]["Additional"].ToString()!="")
				{
					model.Additional=ds.Tables[0].Rows[0]["Additional"].ToString();
				}
				if(ds.Tables[0].Rows[0]["POPrice"]!=null && ds.Tables[0].Rows[0]["POPrice"].ToString()!="")
				{
					model.POPrice=decimal.Parse(ds.Tables[0].Rows[0]["POPrice"].ToString());
				}
				if(ds.Tables[0].Rows[0]["POReasonID"]!=null && ds.Tables[0].Rows[0]["POReasonID"].ToString()!="")
				{
					model.POReasonID=int.Parse(ds.Tables[0].Rows[0]["POReasonID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DiscountType"]!=null && ds.Tables[0].Rows[0]["DiscountType"].ToString()!="")
				{
					model.DiscountType=int.Parse(ds.Tables[0].Rows[0]["DiscountType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DiscountOffValue"]!=null && ds.Tables[0].Rows[0]["DiscountOffValue"].ToString()!="")
				{
					model.DiscountOffValue=decimal.Parse(ds.Tables[0].Rows[0]["DiscountOffValue"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DiscountOffPrice"]!=null && ds.Tables[0].Rows[0]["DiscountOffPrice"].ToString()!="")
				{
					model.DiscountOffPrice=decimal.Parse(ds.Tables[0].Rows[0]["DiscountOffPrice"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DiscountReasonID"]!=null && ds.Tables[0].Rows[0]["DiscountReasonID"].ToString()!="")
				{
					model.DiscountReasonID=int.Parse(ds.Tables[0].Rows[0]["DiscountReasonID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ReservedDate"]!=null && ds.Tables[0].Rows[0]["ReservedDate"].ToString()!="")
				{
					model.ReservedDate=DateTime.Parse(ds.Tables[0].Rows[0]["ReservedDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PickupDate"]!=null && ds.Tables[0].Rows[0]["PickupDate"].ToString()!="")
				{
					model.PickupDate=DateTime.Parse(ds.Tables[0].Rows[0]["PickupDate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,TxnNo,SeqNo,ProdCode,ProdDesc,Serialno,Collected,RetailPrice,NetPrice,Qty,NetAmount,Additional,POPrice,POReasonID,DiscountType,DiscountOffValue,DiscountOffPrice,DiscountReasonID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ReservedDate,PickupDate ");
			strSql.Append(" FROM Sales_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,TxnNo,SeqNo,ProdCode,ProdDesc,Serialno,Collected,RetailPrice,NetPrice,Qty,NetAmount,Additional,POPrice,POReasonID,DiscountType,DiscountOffValue,DiscountOffPrice,DiscountReasonID,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ReservedDate,PickupDate ");
			strSql.Append(" FROM Sales_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Sales_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			strSql.Append(")AS Row, T.*  from Sales_D T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Sales_D";
			parameters[1].Value = "KeyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

