﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Ord_OrderToSupplier_Card_H
    /// </summary>
    public partial class Ord_OrderToSupplier_Card_H : IOrd_OrderToSupplier_Card_H
    {
        public Ord_OrderToSupplier_Card_H()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string OrderSupplierNumber)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Ord_OrderToSupplier_Card_H");
            strSql.Append(" where OrderSupplierNumber=@OrderSupplierNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = OrderSupplierNumber;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Ord_OrderToSupplier_Card_H(");
            strSql.Append("OrderSupplierNumber,SupplierID,OrderSupplierDesc,OrderType,ReferenceNo,SendMethod,SendAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreID,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,Subject,CompanyID,PackageQty,IsProvideNumber,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@OrderSupplierNumber,@SupplierID,@OrderSupplierDesc,@OrderType,@ReferenceNo,@SendMethod,@SendAddress,@SupplierAddress,@SuppliertContactName,@SupplierPhone,@SupplierEmail,@SupplierMobile,@StoreID,@StoreContactName,@StorePhone,@StoreEmail,@StoreMobile,@Remark,@Subject,@CompanyID,@PackageQty,@IsProvideNumber,@PurchaseType,@CreatedBusDate,@ApproveBusDate,@ApprovalCode,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@OrderSupplierDesc", SqlDbType.VarChar,512),
					new SqlParameter("@OrderType", SqlDbType.Int,4),
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@SendMethod", SqlDbType.Int,4),
					new SqlParameter("@SendAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SupplierAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SuppliertContactName", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierPhone", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierMobile", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@StoreContactName", SqlDbType.VarChar,512),
					new SqlParameter("@StorePhone", SqlDbType.VarChar,512),
					new SqlParameter("@StoreEmail", SqlDbType.VarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@Subject", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyID", SqlDbType.Int,4),
					new SqlParameter("@PackageQty", SqlDbType.Int,4),
					new SqlParameter("@IsProvideNumber", SqlDbType.Int,4),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.OrderSupplierNumber;
            parameters[1].Value = model.SupplierID;
            parameters[2].Value = model.OrderSupplierDesc;
            parameters[3].Value = model.OrderType;
            parameters[4].Value = model.ReferenceNo;
            parameters[5].Value = model.SendMethod;
            parameters[6].Value = model.SendAddress;
            parameters[7].Value = model.SupplierAddress;
            parameters[8].Value = model.SuppliertContactName;
            parameters[9].Value = model.SupplierPhone;
            parameters[10].Value = model.SupplierEmail;
            parameters[11].Value = model.SupplierMobile;
            parameters[12].Value = model.StoreID;
            parameters[13].Value = model.StoreContactName;
            parameters[14].Value = model.StorePhone;
            parameters[15].Value = model.StoreEmail;
            parameters[16].Value = model.StoreMobile;
            parameters[17].Value = model.Remark;
            parameters[18].Value = model.Subject;
            parameters[19].Value = model.CompanyID;
            parameters[20].Value = model.PackageQty;
            parameters[21].Value = model.IsProvideNumber;
            parameters[22].Value = model.PurchaseType;
            parameters[23].Value = model.CreatedBusDate;
            parameters[24].Value = model.ApproveBusDate;
            parameters[25].Value = model.ApprovalCode;
            parameters[26].Value = model.ApproveStatus;
            parameters[27].Value = model.ApproveOn;
            parameters[28].Value = model.ApproveBy;
            parameters[29].Value = model.CreatedOn;
            parameters[30].Value = model.CreatedBy;
            parameters[31].Value = model.UpdatedOn;
            parameters[32].Value = model.UpdatedBy;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_OrderToSupplier_Card_H set ");
            strSql.Append("SupplierID=@SupplierID,");
            strSql.Append("OrderSupplierDesc=@OrderSupplierDesc,");
            strSql.Append("OrderType=@OrderType,");
            strSql.Append("ReferenceNo=@ReferenceNo,");
            strSql.Append("SendMethod=@SendMethod,");
            strSql.Append("SendAddress=@SendAddress,");
            strSql.Append("SupplierAddress=@SupplierAddress,");
            strSql.Append("SuppliertContactName=@SuppliertContactName,");
            strSql.Append("SupplierPhone=@SupplierPhone,");
            strSql.Append("SupplierEmail=@SupplierEmail,");
            strSql.Append("SupplierMobile=@SupplierMobile,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("StoreContactName=@StoreContactName,");
            strSql.Append("StorePhone=@StorePhone,");
            strSql.Append("StoreEmail=@StoreEmail,");
            strSql.Append("StoreMobile=@StoreMobile,");
            strSql.Append("Remark=@Remark,");
            strSql.Append("Subject=@Subject,");
            strSql.Append("CompanyID=@CompanyID,");
            strSql.Append("PackageQty=@PackageQty,");
            strSql.Append("IsProvideNumber=@IsProvideNumber,");
            strSql.Append("PurchaseType=@PurchaseType,");
            strSql.Append("CreatedBusDate=@CreatedBusDate,");
            strSql.Append("ApproveBusDate=@ApproveBusDate,");
            strSql.Append("ApprovalCode=@ApprovalCode,");
            strSql.Append("ApproveStatus=@ApproveStatus,");
            strSql.Append("ApproveOn=@ApproveOn,");
            strSql.Append("ApproveBy=@ApproveBy,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where OrderSupplierNumber=@OrderSupplierNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@OrderSupplierDesc", SqlDbType.VarChar,512),
					new SqlParameter("@OrderType", SqlDbType.Int,4),
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@SendMethod", SqlDbType.Int,4),
					new SqlParameter("@SendAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SupplierAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@SuppliertContactName", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierPhone", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierEmail", SqlDbType.VarChar,512),
					new SqlParameter("@SupplierMobile", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@StoreContactName", SqlDbType.VarChar,512),
					new SqlParameter("@StorePhone", SqlDbType.VarChar,512),
					new SqlParameter("@StoreEmail", SqlDbType.VarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@Subject", SqlDbType.VarChar,512),
					new SqlParameter("@CompanyID", SqlDbType.Int,4),
					new SqlParameter("@PackageQty", SqlDbType.Int,4),
					new SqlParameter("@IsProvideNumber", SqlDbType.Int,4),
                    new SqlParameter("@PurchaseType", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64)};
            parameters[0].Value = model.SupplierID;
            parameters[1].Value = model.OrderSupplierDesc;
            parameters[2].Value = model.OrderType;
            parameters[3].Value = model.ReferenceNo;
            parameters[4].Value = model.SendMethod;
            parameters[5].Value = model.SendAddress;
            parameters[6].Value = model.SupplierAddress;
            parameters[7].Value = model.SuppliertContactName;
            parameters[8].Value = model.SupplierPhone;
            parameters[9].Value = model.SupplierEmail;
            parameters[10].Value = model.SupplierMobile;
            parameters[11].Value = model.StoreID;
            parameters[12].Value = model.StoreContactName;
            parameters[13].Value = model.StorePhone;
            parameters[14].Value = model.StoreEmail;
            parameters[15].Value = model.StoreMobile;
            parameters[16].Value = model.Remark;
            parameters[17].Value = model.Subject;
            parameters[18].Value = model.CompanyID;
            parameters[19].Value = model.PackageQty;
            parameters[20].Value = model.IsProvideNumber;
            parameters[21].Value = model.PurchaseType;
            parameters[22].Value = model.CreatedBusDate;
            parameters[23].Value = model.ApproveBusDate;
            parameters[24].Value = model.ApprovalCode;
            parameters[25].Value = model.ApproveStatus;
            parameters[26].Value = model.ApproveOn;
            parameters[27].Value = model.ApproveBy;
            parameters[28].Value = model.CreatedOn;
            parameters[29].Value = model.CreatedBy;
            parameters[30].Value = model.UpdatedOn;
            parameters[31].Value = model.UpdatedBy;
            parameters[32].Value = model.OrderSupplierNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string OrderSupplierNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_OrderToSupplier_Card_H ");
            strSql.Append(" where OrderSupplierNumber=@OrderSupplierNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = OrderSupplierNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OrderSupplierNumberlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_OrderToSupplier_Card_H ");
            strSql.Append(" where OrderSupplierNumber in (" + OrderSupplierNumberlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Ord_OrderToSupplier_Card_H GetModel(string OrderSupplierNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OrderSupplierNumber,SupplierID,OrderSupplierDesc,OrderType,ReferenceNo,SendMethod,SendAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreID,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,Subject,CompanyID,PackageQty,IsProvideNumber,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Ord_OrderToSupplier_Card_H ");
            strSql.Append(" where OrderSupplierNumber=@OrderSupplierNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@OrderSupplierNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = OrderSupplierNumber;

            Edge.SVA.Model.Ord_OrderToSupplier_Card_H model = new Edge.SVA.Model.Ord_OrderToSupplier_Card_H();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OrderSupplierNumber"] != null && ds.Tables[0].Rows[0]["OrderSupplierNumber"].ToString() != "")
                {
                    model.OrderSupplierNumber = ds.Tables[0].Rows[0]["OrderSupplierNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierID"] != null && ds.Tables[0].Rows[0]["SupplierID"].ToString() != "")
                {
                    model.SupplierID = int.Parse(ds.Tables[0].Rows[0]["SupplierID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderSupplierDesc"] != null && ds.Tables[0].Rows[0]["OrderSupplierDesc"].ToString() != "")
                {
                    model.OrderSupplierDesc = ds.Tables[0].Rows[0]["OrderSupplierDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OrderType"] != null && ds.Tables[0].Rows[0]["OrderType"].ToString() != "")
                {
                    model.OrderType = int.Parse(ds.Tables[0].Rows[0]["OrderType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ReferenceNo"] != null && ds.Tables[0].Rows[0]["ReferenceNo"].ToString() != "")
                {
                    model.ReferenceNo = ds.Tables[0].Rows[0]["ReferenceNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SendMethod"] != null && ds.Tables[0].Rows[0]["SendMethod"].ToString() != "")
                {
                    model.SendMethod = int.Parse(ds.Tables[0].Rows[0]["SendMethod"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SendAddress"] != null && ds.Tables[0].Rows[0]["SendAddress"].ToString() != "")
                {
                    model.SendAddress = ds.Tables[0].Rows[0]["SendAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierAddress"] != null && ds.Tables[0].Rows[0]["SupplierAddress"].ToString() != "")
                {
                    model.SupplierAddress = ds.Tables[0].Rows[0]["SupplierAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SuppliertContactName"] != null && ds.Tables[0].Rows[0]["SuppliertContactName"].ToString() != "")
                {
                    model.SuppliertContactName = ds.Tables[0].Rows[0]["SuppliertContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierPhone"] != null && ds.Tables[0].Rows[0]["SupplierPhone"].ToString() != "")
                {
                    model.SupplierPhone = ds.Tables[0].Rows[0]["SupplierPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierEmail"] != null && ds.Tables[0].Rows[0]["SupplierEmail"].ToString() != "")
                {
                    model.SupplierEmail = ds.Tables[0].Rows[0]["SupplierEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SupplierMobile"] != null && ds.Tables[0].Rows[0]["SupplierMobile"].ToString() != "")
                {
                    model.SupplierMobile = ds.Tables[0].Rows[0]["SupplierMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StoreContactName"] != null && ds.Tables[0].Rows[0]["StoreContactName"].ToString() != "")
                {
                    model.StoreContactName = ds.Tables[0].Rows[0]["StoreContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StorePhone"] != null && ds.Tables[0].Rows[0]["StorePhone"].ToString() != "")
                {
                    model.StorePhone = ds.Tables[0].Rows[0]["StorePhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreEmail"] != null && ds.Tables[0].Rows[0]["StoreEmail"].ToString() != "")
                {
                    model.StoreEmail = ds.Tables[0].Rows[0]["StoreEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreMobile"] != null && ds.Tables[0].Rows[0]["StoreMobile"].ToString() != "")
                {
                    model.StoreMobile = ds.Tables[0].Rows[0]["StoreMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Subject"] != null && ds.Tables[0].Rows[0]["Subject"].ToString() != "")
                {
                    model.Subject = ds.Tables[0].Rows[0]["Subject"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CompanyID"] != null && ds.Tables[0].Rows[0]["CompanyID"].ToString() != "")
                {
                    model.CompanyID = int.Parse(ds.Tables[0].Rows[0]["CompanyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PackageQty"] != null && ds.Tables[0].Rows[0]["PackageQty"].ToString() != "")
                {
                    model.PackageQty = int.Parse(ds.Tables[0].Rows[0]["PackageQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsProvideNumber"] != null && ds.Tables[0].Rows[0]["IsProvideNumber"].ToString() != "")
                {
                    model.IsProvideNumber = int.Parse(ds.Tables[0].Rows[0]["IsProvideNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PurchaseType"] != null && ds.Tables[0].Rows[0]["PurchaseType"].ToString() != "")
                {
                    model.PurchaseType = int.Parse(ds.Tables[0].Rows[0]["PurchaseType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBusDate"] != null && ds.Tables[0].Rows[0]["CreatedBusDate"].ToString() != "")
                {
                    model.CreatedBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBusDate"] != null && ds.Tables[0].Rows[0]["ApproveBusDate"].ToString() != "")
                {
                    model.ApproveBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApprovalCode"] != null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString() != "")
                {
                    model.ApprovalCode = ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveStatus"] != null && ds.Tables[0].Rows[0]["ApproveStatus"].ToString() != "")
                {
                    model.ApproveStatus = ds.Tables[0].Rows[0]["ApproveStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveOn"] != null && ds.Tables[0].Rows[0]["ApproveOn"].ToString() != "")
                {
                    model.ApproveOn = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBy"] != null && ds.Tables[0].Rows[0]["ApproveBy"].ToString() != "")
                {
                    model.ApproveBy = int.Parse(ds.Tables[0].Rows[0]["ApproveBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OrderSupplierNumber,SupplierID,OrderSupplierDesc,OrderType,ReferenceNo,SendMethod,SendAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreID,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,Subject,CompanyID,PackageQty,IsProvideNumber,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_OrderToSupplier_Card_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OrderSupplierNumber,SupplierID,OrderSupplierDesc,OrderType,ReferenceNo,SendMethod,SendAddress,SupplierAddress,SuppliertContactName,SupplierPhone,SupplierEmail,SupplierMobile,StoreID,StoreContactName,StorePhone,StoreEmail,StoreMobile,Remark,Subject,CompanyID,PackageQty,IsProvideNumber,PurchaseType,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_OrderToSupplier_Card_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Ord_OrderToSupplier_Card_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OrderSupplierNumber desc");
            }
            strSql.Append(")AS Row, T.*  from Ord_OrderToSupplier_Card_H T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Ord_OrderToSupplier_Card_H";
            parameters[1].Value = "OrderSupplierNumber";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

