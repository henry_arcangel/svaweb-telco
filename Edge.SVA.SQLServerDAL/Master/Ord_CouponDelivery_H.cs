﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Ord_CouponDelivery_H
    /// </summary>
    public partial class Ord_CouponDelivery_H : IOrd_CouponDelivery_H
    {
        public Ord_CouponDelivery_H()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CouponDeliveryNumber)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Ord_CouponDelivery_H");
            strSql.Append(" where CouponDeliveryNumber=@CouponDeliveryNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponDeliveryNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CouponDeliveryNumber;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Ord_CouponDelivery_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Ord_CouponDelivery_H(");
            strSql.Append("CouponDeliveryNumber,ReferenceNo,BrandID,FromStoreID,StoreID,CustomerType,CustomerID,SendMethod,SendAddress,FromAddress,StoreContactName,StoreContactPhone,StoreContactEmail,StoreMobile,FromContactName,FromContactNumber,FromEmail,FromMobile,NeedActive,Remark,Remark1,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@CouponDeliveryNumber,@ReferenceNo,@BrandID,@FromStoreID,@StoreID,@CustomerType,@CustomerID,@SendMethod,@SendAddress,@FromAddress,@StoreContactName,@StoreContactPhone,@StoreContactEmail,@StoreMobile,@FromContactName,@FromContactNumber,@FromEmail,@FromMobile,@NeedActive,@Remark,@Remark1,@CreatedBusDate,@ApproveBusDate,@ApprovalCode,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponDeliveryNumber", SqlDbType.VarChar,64),
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@FromStoreID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@CustomerType", SqlDbType.Int,4),
					new SqlParameter("@CustomerID", SqlDbType.Int,4),
					new SqlParameter("@SendMethod", SqlDbType.Int,4),
					new SqlParameter("@SendAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@FromAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactName", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactPhone", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@FromContactName", SqlDbType.NVarChar,512),
					new SqlParameter("@FromContactNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@FromEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@FromMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@NeedActive", SqlDbType.Int,4),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
                    new SqlParameter("@Remark1", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.CouponDeliveryNumber;
            parameters[1].Value = model.ReferenceNo;
            parameters[2].Value = model.BrandID;
            parameters[3].Value = model.FromStoreID;
            parameters[4].Value = model.StoreID;
            parameters[5].Value = model.CustomerType;
            parameters[6].Value = model.CustomerID;
            parameters[7].Value = model.SendMethod;
            parameters[8].Value = model.SendAddress;
            parameters[9].Value = model.FromAddress;
            parameters[10].Value = model.StoreContactName;
            parameters[11].Value = model.StoreContactPhone;
            parameters[12].Value = model.StoreContactEmail;
            parameters[13].Value = model.StoreMobile;
            parameters[14].Value = model.FromContactName;
            parameters[15].Value = model.FromContactNumber;
            parameters[16].Value = model.FromEmail;
            parameters[17].Value = model.FromMobile;
            parameters[18].Value = model.NeedActive;
            parameters[19].Value = model.Remark;
            parameters[20].Value = model.Remark1;
            parameters[21].Value = model.CreatedBusDate;
            parameters[22].Value = model.ApproveBusDate;
            parameters[23].Value = model.ApprovalCode;
            parameters[24].Value = model.ApproveStatus;
            parameters[25].Value = model.ApproveOn;
            parameters[26].Value = model.ApproveBy;
            parameters[27].Value = model.CreatedOn;
            parameters[28].Value = model.CreatedBy;
            parameters[29].Value = model.UpdatedOn;
            parameters[30].Value = model.UpdatedBy;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Ord_CouponDelivery_H model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ord_CouponDelivery_H set ");
            strSql.Append("ReferenceNo=@ReferenceNo,");
            strSql.Append("BrandID=@BrandID,");
            strSql.Append("FromStoreID=@FromStoreID,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("CustomerType=@CustomerType,");
            strSql.Append("CustomerID=@CustomerID,");
            strSql.Append("SendMethod=@SendMethod,");
            strSql.Append("SendAddress=@SendAddress,");
            strSql.Append("FromAddress=@FromAddress,");
            strSql.Append("StoreContactName=@StoreContactName,");
            strSql.Append("StoreContactPhone=@StoreContactPhone,");
            strSql.Append("StoreContactEmail=@StoreContactEmail,");
            strSql.Append("StoreMobile=@StoreMobile,");
            strSql.Append("FromContactName=@FromContactName,");
            strSql.Append("FromContactNumber=@FromContactNumber,");
            strSql.Append("FromEmail=@FromEmail,");
            strSql.Append("FromMobile=@FromMobile,");
            strSql.Append("NeedActive=@NeedActive,");
            strSql.Append("Remark=@Remark,");
            strSql.Append("Remark1=@Remark1,");
            strSql.Append("CreatedBusDate=@CreatedBusDate,");
            strSql.Append("ApproveBusDate=@ApproveBusDate,");
            strSql.Append("ApprovalCode=@ApprovalCode,");
            strSql.Append("ApproveStatus=@ApproveStatus,");
            strSql.Append("ApproveOn=@ApproveOn,");
            strSql.Append("ApproveBy=@ApproveBy,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where CouponDeliveryNumber=@CouponDeliveryNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@FromStoreID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@CustomerType", SqlDbType.Int,4),
					new SqlParameter("@CustomerID", SqlDbType.Int,4),
					new SqlParameter("@SendMethod", SqlDbType.Int,4),
					new SqlParameter("@SendAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@FromAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactName", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactPhone", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreContactEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@StoreMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@FromContactName", SqlDbType.NVarChar,512),
					new SqlParameter("@FromContactNumber", SqlDbType.NVarChar,512),
					new SqlParameter("@FromEmail", SqlDbType.NVarChar,512),
					new SqlParameter("@FromMobile", SqlDbType.NVarChar,512),
					new SqlParameter("@NeedActive", SqlDbType.Int,4),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
                    new SqlParameter("@Remark1", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponDeliveryNumber", SqlDbType.VarChar,64)};
            parameters[0].Value = model.ReferenceNo;
            parameters[1].Value = model.BrandID;
            parameters[2].Value = model.FromStoreID;
            parameters[3].Value = model.StoreID;
            parameters[4].Value = model.CustomerType;
            parameters[5].Value = model.CustomerID;
            parameters[6].Value = model.SendMethod;
            parameters[7].Value = model.SendAddress;
            parameters[8].Value = model.FromAddress;
            parameters[9].Value = model.StoreContactName;
            parameters[10].Value = model.StoreContactPhone;
            parameters[11].Value = model.StoreContactEmail;
            parameters[12].Value = model.StoreMobile;
            parameters[13].Value = model.FromContactName;
            parameters[14].Value = model.FromContactNumber;
            parameters[15].Value = model.FromEmail;
            parameters[16].Value = model.FromMobile;
            parameters[17].Value = model.NeedActive;
            parameters[18].Value = model.Remark;
            parameters[19].Value = model.Remark1;
            parameters[20].Value = model.CreatedBusDate;
            parameters[21].Value = model.ApproveBusDate;
            parameters[22].Value = model.ApprovalCode;
            parameters[23].Value = model.ApproveStatus;
            parameters[24].Value = model.ApproveOn;
            parameters[25].Value = model.ApproveBy;
            parameters[26].Value = model.CreatedOn;
            parameters[27].Value = model.CreatedBy;
            parameters[28].Value = model.UpdatedOn;
            parameters[29].Value = model.UpdatedBy;
            parameters[30].Value = model.CouponDeliveryNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CouponDeliveryNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_CouponDelivery_H ");
            strSql.Append(" where CouponDeliveryNumber=@CouponDeliveryNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponDeliveryNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CouponDeliveryNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CouponDeliveryNumberlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ord_CouponDelivery_H ");
            strSql.Append(" where CouponDeliveryNumber in (" + CouponDeliveryNumberlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Ord_CouponDelivery_H GetModel(string CouponDeliveryNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CouponDeliveryNumber,ReferenceNo,BrandID,FromStoreID,StoreID,CustomerType,CustomerID,SendMethod,SendAddress,FromAddress,StoreContactName,StoreContactPhone,StoreContactEmail,StoreMobile,FromContactName,FromContactNumber,FromEmail,FromMobile,NeedActive,Remark,Remark1,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Ord_CouponDelivery_H ");
            strSql.Append(" where CouponDeliveryNumber=@CouponDeliveryNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponDeliveryNumber", SqlDbType.VarChar,64)			};
            parameters[0].Value = CouponDeliveryNumber;

            Edge.SVA.Model.Ord_CouponDelivery_H model = new Edge.SVA.Model.Ord_CouponDelivery_H();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CouponDeliveryNumber"] != null && ds.Tables[0].Rows[0]["CouponDeliveryNumber"].ToString() != "")
                {
                    model.CouponDeliveryNumber = ds.Tables[0].Rows[0]["CouponDeliveryNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ReferenceNo"] != null && ds.Tables[0].Rows[0]["ReferenceNo"].ToString() != "")
                {
                    model.ReferenceNo = ds.Tables[0].Rows[0]["ReferenceNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BrandID"] != null && ds.Tables[0].Rows[0]["BrandID"].ToString() != "")
                {
                    model.BrandID = int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FromStoreID"] != null && ds.Tables[0].Rows[0]["FromStoreID"].ToString() != "")
                {
                    model.FromStoreID = int.Parse(ds.Tables[0].Rows[0]["FromStoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CustomerType"] != null && ds.Tables[0].Rows[0]["CustomerType"].ToString() != "")
                {
                    model.CustomerType = int.Parse(ds.Tables[0].Rows[0]["CustomerType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CustomerID"] != null && ds.Tables[0].Rows[0]["CustomerID"].ToString() != "")
                {
                    model.CustomerID = int.Parse(ds.Tables[0].Rows[0]["CustomerID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SendMethod"] != null && ds.Tables[0].Rows[0]["SendMethod"].ToString() != "")
                {
                    model.SendMethod = int.Parse(ds.Tables[0].Rows[0]["SendMethod"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SendAddress"] != null && ds.Tables[0].Rows[0]["SendAddress"].ToString() != "")
                {
                    model.SendAddress = ds.Tables[0].Rows[0]["SendAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FromAddress"] != null && ds.Tables[0].Rows[0]["FromAddress"].ToString() != "")
                {
                    model.FromAddress = ds.Tables[0].Rows[0]["FromAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreContactName"] != null && ds.Tables[0].Rows[0]["StoreContactName"].ToString() != "")
                {
                    model.StoreContactName = ds.Tables[0].Rows[0]["StoreContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreContactPhone"] != null && ds.Tables[0].Rows[0]["StoreContactPhone"].ToString() != "")
                {
                    model.StoreContactPhone = ds.Tables[0].Rows[0]["StoreContactPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreContactEmail"] != null && ds.Tables[0].Rows[0]["StoreContactEmail"].ToString() != "")
                {
                    model.StoreContactEmail = ds.Tables[0].Rows[0]["StoreContactEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreMobile"] != null && ds.Tables[0].Rows[0]["StoreMobile"].ToString() != "")
                {
                    model.StoreMobile = ds.Tables[0].Rows[0]["StoreMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FromContactName"] != null && ds.Tables[0].Rows[0]["FromContactName"].ToString() != "")
                {
                    model.FromContactName = ds.Tables[0].Rows[0]["FromContactName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FromContactNumber"] != null && ds.Tables[0].Rows[0]["FromContactNumber"].ToString() != "")
                {
                    model.FromContactNumber = ds.Tables[0].Rows[0]["FromContactNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FromEmail"] != null && ds.Tables[0].Rows[0]["FromEmail"].ToString() != "")
                {
                    model.FromEmail = ds.Tables[0].Rows[0]["FromEmail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FromMobile"] != null && ds.Tables[0].Rows[0]["FromMobile"].ToString() != "")
                {
                    model.FromMobile = ds.Tables[0].Rows[0]["FromMobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NeedActive"] != null && ds.Tables[0].Rows[0]["NeedActive"].ToString() != "")
                {
                    model.NeedActive = int.Parse(ds.Tables[0].Rows[0]["NeedActive"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark1"] != null && ds.Tables[0].Rows[0]["Remark1"].ToString() != "")
                {
                    model.Remark1 = ds.Tables[0].Rows[0]["Remark1"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreatedBusDate"] != null && ds.Tables[0].Rows[0]["CreatedBusDate"].ToString() != "")
                {
                    model.CreatedBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBusDate"] != null && ds.Tables[0].Rows[0]["ApproveBusDate"].ToString() != "")
                {
                    model.ApproveBusDate = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveBusDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApprovalCode"] != null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString() != "")
                {
                    model.ApprovalCode = ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveStatus"] != null && ds.Tables[0].Rows[0]["ApproveStatus"].ToString() != "")
                {
                    model.ApproveStatus = ds.Tables[0].Rows[0]["ApproveStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproveOn"] != null && ds.Tables[0].Rows[0]["ApproveOn"].ToString() != "")
                {
                    model.ApproveOn = DateTime.Parse(ds.Tables[0].Rows[0]["ApproveOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproveBy"] != null && ds.Tables[0].Rows[0]["ApproveBy"].ToString() != "")
                {
                    model.ApproveBy = int.Parse(ds.Tables[0].Rows[0]["ApproveBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CouponDeliveryNumber,ReferenceNo,BrandID,FromStoreID,StoreID,CustomerType,CustomerID,SendMethod,SendAddress,FromAddress,StoreContactName,StoreContactPhone,StoreContactEmail,StoreMobile,FromContactName,FromContactNumber,FromEmail,FromMobile,NeedActive,Remark,Remark1,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_CouponDelivery_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CouponDeliveryNumber,ReferenceNo,BrandID,FromStoreID,StoreID,CustomerType,CustomerID,SendMethod,SendAddress,FromAddress,StoreContactName,StoreContactPhone,StoreContactEmail,StoreMobile,FromContactName,FromContactNumber,FromEmail,FromMobile,NeedActive,Remark,Remark1,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM Ord_CouponDelivery_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Ord_CouponDelivery_H ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CouponDeliveryNumber desc");
            }
            strSql.Append(")AS Row, T.*  from Ord_CouponDelivery_H T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Ord_CouponDelivery_H";
            parameters[1].Value = "CouponDeliveryNumber";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

