﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CouponNature
	/// </summary>
	public partial class CouponNature:ICouponNature
	{
		public CouponNature()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CouponNatureID", "CouponNature"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CouponNatureID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CouponNature");
			strSql.Append(" where CouponNatureID=@CouponNatureID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4)
};
			parameters[0].Value = CouponNatureID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.CouponNature model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CouponNature(");
            strSql.Append("CouponNatureCode,CouponNatureName1,CouponNatureName2,CouponNatureName3,BrandID,ParentID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@CouponNatureCode,@CouponNatureName1,@CouponNatureName2,@CouponNatureName3,@BrandID,@ParentID,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponNatureCode", SqlDbType.VarChar,64),
					new SqlParameter("@CouponNatureName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNatureName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNatureName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
                    new SqlParameter("@ParentID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CouponNatureCode;
			parameters[1].Value = model.CouponNatureName1;
			parameters[2].Value = model.CouponNatureName2;
			parameters[3].Value = model.CouponNatureName3;
			parameters[4].Value = model.BrandID;
            parameters[5].Value = model.ParentID;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.UpdatedOn;
			parameters[8].Value = model.CreatedBy;
			parameters[9].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponNature model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CouponNature set ");
			strSql.Append("CouponNatureCode=@CouponNatureCode,");
			strSql.Append("CouponNatureName1=@CouponNatureName1,");
			strSql.Append("CouponNatureName2=@CouponNatureName2,");
			strSql.Append("CouponNatureName3=@CouponNatureName3,");
			strSql.Append("BrandID=@BrandID,");
            strSql.Append("ParentID=@ParentID,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where CouponNatureID=@CouponNatureID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponNatureCode", SqlDbType.VarChar,64),
					new SqlParameter("@CouponNatureName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNatureName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNatureName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
                    new SqlParameter("@ParentID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4)};
			parameters[0].Value = model.CouponNatureCode;
			parameters[1].Value = model.CouponNatureName1;
			parameters[2].Value = model.CouponNatureName2;
			parameters[3].Value = model.CouponNatureName3;
            parameters[4].Value = model.BrandID;
			parameters[5].Value = model.ParentID;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.UpdatedOn;
			parameters[8].Value = model.CreatedBy;
			parameters[9].Value = model.UpdatedBy;
			parameters[10].Value = model.CouponNatureID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CouponNatureID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponNature ");
			strSql.Append(" where CouponNatureID=@CouponNatureID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4)
};
			parameters[0].Value = CouponNatureID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CouponNatureIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponNature ");
			strSql.Append(" where CouponNatureID in ("+CouponNatureIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponNature GetModel(int CouponNatureID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 CouponNatureID,CouponNatureCode,CouponNatureName1,CouponNatureName2,CouponNatureName3,BrandID,ParentID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy from CouponNature ");
			strSql.Append(" where CouponNatureID=@CouponNatureID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4)
};
			parameters[0].Value = CouponNatureID;

			Edge.SVA.Model.CouponNature model=new Edge.SVA.Model.CouponNature();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CouponNatureID"]!=null && ds.Tables[0].Rows[0]["CouponNatureID"].ToString()!="")
				{
					model.CouponNatureID=int.Parse(ds.Tables[0].Rows[0]["CouponNatureID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CouponNatureCode"]!=null && ds.Tables[0].Rows[0]["CouponNatureCode"].ToString()!="")
				{
					model.CouponNatureCode=ds.Tables[0].Rows[0]["CouponNatureCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CouponNatureName1"]!=null && ds.Tables[0].Rows[0]["CouponNatureName1"].ToString()!="")
				{
					model.CouponNatureName1=ds.Tables[0].Rows[0]["CouponNatureName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CouponNatureName2"]!=null && ds.Tables[0].Rows[0]["CouponNatureName2"].ToString()!="")
				{
					model.CouponNatureName2=ds.Tables[0].Rows[0]["CouponNatureName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CouponNatureName3"]!=null && ds.Tables[0].Rows[0]["CouponNatureName3"].ToString()!="")
				{
					model.CouponNatureName3=ds.Tables[0].Rows[0]["CouponNatureName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BrandID"]!=null && ds.Tables[0].Rows[0]["BrandID"].ToString()!="")
				{
					model.BrandID=int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
				}
                if (ds.Tables[0].Rows[0]["ParentID"] != null && ds.Tables[0].Rows[0]["ParentID"].ToString() != "")
                {
                    model.ParentID = int.Parse(ds.Tables[0].Rows[0]["ParentID"].ToString());
                }
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select CouponNatureID,CouponNatureCode,CouponNatureName1,CouponNatureName2,CouponNatureName3,BrandID,ParentID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM CouponNature ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" CouponNatureID,CouponNatureCode,CouponNatureName1,CouponNatureName2,CouponNatureName3,BrandID,ParentID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM CouponNature ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CouponNature";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CouponNature ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CouponNatureID desc");
            }
            strSql.Append(")AS Row, T.*  from CouponNature T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

		#endregion  Method
	}
}

