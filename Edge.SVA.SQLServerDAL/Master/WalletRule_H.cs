﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
    /// 数据访问类:WalletRule_H
	/// </summary>
    public partial class WalletRule_H : IWalletRule_H
	{
        public WalletRule_H()
		{}
		#region  Method


		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(string WalletRuleCode)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) from WalletRule_H");
            strSql.Append(" where WalletRuleCode=@WalletRuleCode");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64)
			};
            parameters[0].Value = WalletRuleCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.WalletRule_H model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into WalletRule_H(");
            strSql.Append("WalletRuleCode,Description,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@WalletRuleCode,@Description,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64),
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.WalletRuleCode;
            parameters[1].Value = model.Description;
			parameters[2].Value = model.CreatedOn;
			parameters[3].Value = model.CreatedBy;
			parameters[4].Value = model.UpdatedOn;
			parameters[5].Value = model.UpdatedBy;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.WalletRule_H model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update WalletRule_H set ");
            strSql.Append("Description=@Description,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where WalletRuleCode=@WalletRuleCode");
			SqlParameter[] parameters = {
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64)};
            parameters[0].Value = model.Description;
			parameters[1].Value = model.CreatedOn;
			parameters[2].Value = model.CreatedBy;
			parameters[3].Value = model.UpdatedOn;
			parameters[4].Value = model.UpdatedBy;
            parameters[5].Value = model.WalletRuleCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(string WalletRuleCode)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from WalletRule_H ");
            strSql.Append(" where WalletRuleCode=@WalletRuleCode");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64)
			};
            parameters[0].Value = WalletRuleCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
        public bool DeleteList(string WalletRuleCodeDlist)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from WalletRule_H ");
            strSql.Append(" where WalletRuleCode in ('" + WalletRuleCodeDlist + "')  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.WalletRule_H GetModel(string WalletRuleCode)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 WalletRuleCode, Description,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from WalletRule_H ");
            strSql.Append(" where WalletRuleCode=@WalletRuleCode");
			SqlParameter[] parameters = {
					new SqlParameter("@WalletRuleCode", SqlDbType.VarChar,64)
			};
            parameters[0].Value = WalletRuleCode;

            Edge.SVA.Model.WalletRule_H model = new Edge.SVA.Model.WalletRule_H();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["WalletRuleCode"] != null && ds.Tables[0].Rows[0]["WalletRuleCode"].ToString() != "")
				{
                    model.WalletRuleCode = ds.Tables[0].Rows[0]["WalletRuleCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description"] != null && ds.Tables[0].Rows[0]["Description"].ToString() != "")
				{
                    model.Description = ds.Tables[0].Rows[0]["Description"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select WalletRuleCode, Description,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM WalletRule_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" WalletRuleCode, Description, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM WalletRule_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select count(1) FROM WalletRule_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
                strSql.Append("order by T.WalletRuleCode desc");
			}
            strSql.Append(")AS Row, T.*  from WalletRule_H T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}


		#endregion  Method
	}
}

