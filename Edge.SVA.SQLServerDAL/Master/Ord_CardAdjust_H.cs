﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    //modify By Gavin @2015-03-02 add field: flag1
	/// <summary>
	/// 数据访问类:Ord_CardAdjust_H
	/// </summary>
	public partial class Ord_CardAdjust_H:IOrd_CardAdjust_H
	{
		public Ord_CardAdjust_H()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardAdjustNumber)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_CardAdjust_H");
			strSql.Append(" where CardAdjustNumber=@CardAdjustNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar,512)			};
			parameters[0].Value = CardAdjustNumber;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Ord_CardAdjust_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_CardAdjust_H(");
			strSql.Append("CardAdjustNumber,OprID,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,ReasonID,Note,ActAmount,ActPoints,ActExpireDate,CardCount,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate,ApprovalCode,BrandCode,StoreID,ActStatus,Flag1)");
			strSql.Append(" values (");
            strSql.Append("@CardAdjustNumber,@OprID,@OriginalTxnNo,@TxnDate,@StoreCode,@ServerCode,@RegisterCode,@ReasonID,@Note,@ActAmount,@ActPoints,@ActExpireDate,@CardCount,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@CreatedBusDate,@ApproveBusDate,@ApprovalCode,@BrandCode,@StoreID,@ActStatus,@Flag1)");
			SqlParameter[] parameters = {
					new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar,512),
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@OriginalTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,512),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.VarChar,512),
					new SqlParameter("@ActAmount", SqlDbType.Money,8),
					new SqlParameter("@ActPoints", SqlDbType.Int,4),
					new SqlParameter("@ActExpireDate", SqlDbType.DateTime),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,512),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@ActStatus", SqlDbType.Int,4),
                    new SqlParameter("@Flag1", SqlDbType.Int,4)};
			parameters[0].Value = model.CardAdjustNumber;
			parameters[1].Value = model.OprID;
			parameters[2].Value = model.OriginalTxnNo;
			parameters[3].Value = model.TxnDate;
			parameters[4].Value = model.StoreCode;
			parameters[5].Value = model.ServerCode;
			parameters[6].Value = model.RegisterCode;
			parameters[7].Value = model.ReasonID;
			parameters[8].Value = model.Note;
			parameters[9].Value = model.ActAmount;
			parameters[10].Value = model.ActPoints;
			parameters[11].Value = model.ActExpireDate;
			parameters[12].Value = model.CardCount;
			parameters[13].Value = model.ApproveStatus;
			parameters[14].Value = model.ApproveOn;
			parameters[15].Value = model.ApproveBy;
			parameters[16].Value = model.CreatedOn;
			parameters[17].Value = model.CreatedBy;
			parameters[18].Value = model.UpdatedOn;
			parameters[19].Value = model.UpdatedBy;
			parameters[20].Value = model.CreatedBusDate;
			parameters[21].Value = model.ApproveBusDate;
			parameters[22].Value = model.ApprovalCode;
			parameters[23].Value = model.BrandCode;
			parameters[24].Value = model.StoreID;
			parameters[25].Value = model.ActStatus;
            parameters[26].Value = model.Flag1;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CardAdjust_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_CardAdjust_H set ");
			strSql.Append("OprID=@OprID,");
			strSql.Append("OriginalTxnNo=@OriginalTxnNo,");
			strSql.Append("TxnDate=@TxnDate,");
			strSql.Append("StoreCode=@StoreCode,");
			strSql.Append("ServerCode=@ServerCode,");
			strSql.Append("RegisterCode=@RegisterCode,");
			strSql.Append("ReasonID=@ReasonID,");
			strSql.Append("Note=@Note,");
			strSql.Append("ActAmount=@ActAmount,");
			strSql.Append("ActPoints=@ActPoints,");
			strSql.Append("ActExpireDate=@ActExpireDate,");
			strSql.Append("CardCount=@CardCount,");
			strSql.Append("ApproveStatus=@ApproveStatus,");
			strSql.Append("ApproveOn=@ApproveOn,");
			strSql.Append("ApproveBy=@ApproveBy,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CreatedBusDate=@CreatedBusDate,");
			strSql.Append("ApproveBusDate=@ApproveBusDate,");
			strSql.Append("ApprovalCode=@ApprovalCode,");
			strSql.Append("BrandCode=@BrandCode,");
			strSql.Append("StoreID=@StoreID,");
			strSql.Append("ActStatus=@ActStatus,");
            strSql.Append("Flag1=@Flag1");
			strSql.Append(" where CardAdjustNumber=@CardAdjustNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@OriginalTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@StoreCode", SqlDbType.VarChar,512),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@ReasonID", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.VarChar,512),
					new SqlParameter("@ActAmount", SqlDbType.Money,8),
					new SqlParameter("@ActPoints", SqlDbType.Int,4),
					new SqlParameter("@ActExpireDate", SqlDbType.DateTime),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,512),
					new SqlParameter("@BrandCode", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@ActStatus", SqlDbType.Int,4),
                    new SqlParameter("@Flag1", SqlDbType.Int,4),
					new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar,512)};
			parameters[0].Value = model.OprID;
			parameters[1].Value = model.OriginalTxnNo;
			parameters[2].Value = model.TxnDate;
			parameters[3].Value = model.StoreCode;
			parameters[4].Value = model.ServerCode;
			parameters[5].Value = model.RegisterCode;
			parameters[6].Value = model.ReasonID;
			parameters[7].Value = model.Note;
			parameters[8].Value = model.ActAmount;
			parameters[9].Value = model.ActPoints;
			parameters[10].Value = model.ActExpireDate;
			parameters[11].Value = model.CardCount;
			parameters[12].Value = model.ApproveStatus;
			parameters[13].Value = model.ApproveOn;
			parameters[14].Value = model.ApproveBy;
			parameters[15].Value = model.CreatedOn;
			parameters[16].Value = model.CreatedBy;
			parameters[17].Value = model.UpdatedOn;
			parameters[18].Value = model.UpdatedBy;
			parameters[19].Value = model.CreatedBusDate;
			parameters[20].Value = model.ApproveBusDate;
			parameters[21].Value = model.ApprovalCode;
			parameters[22].Value = model.BrandCode;
			parameters[23].Value = model.StoreID;
			parameters[24].Value = model.ActStatus;
            parameters[25].Value = model.Flag1;
			parameters[26].Value = model.CardAdjustNumber;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardAdjustNumber)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CardAdjust_H ");
			strSql.Append(" where CardAdjustNumber=@CardAdjustNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar,512)			};
			parameters[0].Value = CardAdjustNumber;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardAdjustNumberlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CardAdjust_H ");
			strSql.Append(" where CardAdjustNumber in ("+CardAdjustNumberlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardAdjust_H GetModel(string CardAdjustNumber)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CardAdjustNumber,OprID,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,ReasonID,Note,ActAmount,ActPoints,ActExpireDate,CardCount,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate,ApprovalCode,BrandCode,StoreID,ActStatus,Flag1 from Ord_CardAdjust_H ");
			strSql.Append(" where CardAdjustNumber=@CardAdjustNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar,512)			};
			parameters[0].Value = CardAdjustNumber;

			Edge.SVA.Model.Ord_CardAdjust_H model=new Edge.SVA.Model.Ord_CardAdjust_H();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardAdjust_H DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.Ord_CardAdjust_H model=new Edge.SVA.Model.Ord_CardAdjust_H();
			if (row != null)
			{
				if(row["CardAdjustNumber"]!=null)
				{
					model.CardAdjustNumber=row["CardAdjustNumber"].ToString();
				}
				if(row["OprID"]!=null && row["OprID"].ToString()!="")
				{
					model.OprID=int.Parse(row["OprID"].ToString());
				}
				if(row["OriginalTxnNo"]!=null)
				{
					model.OriginalTxnNo=row["OriginalTxnNo"].ToString();
				}
				if(row["TxnDate"]!=null && row["TxnDate"].ToString()!="")
				{
					model.TxnDate=DateTime.Parse(row["TxnDate"].ToString());
				}
				if(row["StoreCode"]!=null)
				{
					model.StoreCode=row["StoreCode"].ToString();
				}
				if(row["ServerCode"]!=null)
				{
					model.ServerCode=row["ServerCode"].ToString();
				}
				if(row["RegisterCode"]!=null)
				{
					model.RegisterCode=row["RegisterCode"].ToString();
				}
				if(row["ReasonID"]!=null && row["ReasonID"].ToString()!="")
				{
					model.ReasonID=int.Parse(row["ReasonID"].ToString());
				}
				if(row["Note"]!=null)
				{
					model.Note=row["Note"].ToString();
				}
				if(row["ActAmount"]!=null && row["ActAmount"].ToString()!="")
				{
					model.ActAmount=decimal.Parse(row["ActAmount"].ToString());
				}
				if(row["ActPoints"]!=null && row["ActPoints"].ToString()!="")
				{
					model.ActPoints=int.Parse(row["ActPoints"].ToString());
				}
				if(row["ActExpireDate"]!=null && row["ActExpireDate"].ToString()!="")
				{
					model.ActExpireDate=DateTime.Parse(row["ActExpireDate"].ToString());
				}
				if(row["CardCount"]!=null && row["CardCount"].ToString()!="")
				{
					model.CardCount=int.Parse(row["CardCount"].ToString());
				}
				if(row["ApproveStatus"]!=null)
				{
					model.ApproveStatus=row["ApproveStatus"].ToString();
				}
				if(row["ApproveOn"]!=null && row["ApproveOn"].ToString()!="")
				{
					model.ApproveOn=DateTime.Parse(row["ApproveOn"].ToString());
				}
				if(row["ApproveBy"]!=null && row["ApproveBy"].ToString()!="")
				{
					model.ApproveBy=int.Parse(row["ApproveBy"].ToString());
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["CreatedBusDate"]!=null && row["CreatedBusDate"].ToString()!="")
				{
					model.CreatedBusDate=DateTime.Parse(row["CreatedBusDate"].ToString());
				}
				if(row["ApproveBusDate"]!=null && row["ApproveBusDate"].ToString()!="")
				{
					model.ApproveBusDate=DateTime.Parse(row["ApproveBusDate"].ToString());
				}
				if(row["ApprovalCode"]!=null)
				{
					model.ApprovalCode=row["ApprovalCode"].ToString();
				}
				if(row["BrandCode"]!=null)
				{
					model.BrandCode=row["BrandCode"].ToString();
				}
				if(row["StoreID"]!=null && row["StoreID"].ToString()!="")
				{
					model.StoreID=int.Parse(row["StoreID"].ToString());
				}
				if(row["ActStatus"]!=null && row["ActStatus"].ToString()!="")
				{
					model.ActStatus=int.Parse(row["ActStatus"].ToString());
				}
                if (row["Flag1"] != null && row["Flag1"].ToString() != "")
                {
                    model.Flag1 = int.Parse(row["Flag1"].ToString());
                }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CardAdjustNumber,OprID,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,ReasonID,Note,ActAmount,ActPoints,ActExpireDate,CardCount,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate,ApprovalCode,BrandCode,StoreID,ActStatus,Flag1 ");
			strSql.Append(" FROM Ord_CardAdjust_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CardAdjustNumber,OprID,OriginalTxnNo,TxnDate,StoreCode,ServerCode,RegisterCode,ReasonID,Note,ActAmount,ActPoints,ActExpireDate,CardCount,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate,ApprovalCode,BrandCode,StoreID,ActStatus, Flag1 ");
			strSql.Append(" FROM Ord_CardAdjust_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_CardAdjust_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CardAdjustNumber desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_CardAdjust_H T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Ord_CardAdjust_H";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 1;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Ord_CardAdjust_H ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_CardAdjust_H";
			parameters[1].Value = "CardAdjustNumber";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

