﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardAutoAddValueRule
	/// </summary>
	public partial class CardAutoAddValueRule:ICardAutoAddValueRule
	{
		public CardAutoAddValueRule()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid AutoAddRuleID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardAutoAddValueRule");
			strSql.Append(" where AutoAddRuleID=@AutoAddRuleID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AutoAddRuleID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = AutoAddRuleID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CardAutoAddValueRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardAutoAddValueRule(");
			strSql.Append("AutoAddRuleID,CardTypeID,CardGradeID,AutoAddRuleSeqNo,RuleDesc,AddAmount,StartDate,EndDate,Interval,IntervalUnit,DayOfInterval,LastExecDate,ExecutedCount,JustForBirth,UpdatedDate,CreatedDate,CreatedBy,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@AutoAddRuleID,@CardTypeID,@CardGradeID,@AutoAddRuleSeqNo,@RuleDesc,@AddAmount,@StartDate,@EndDate,@Interval,@IntervalUnit,@DayOfInterval,@LastExecDate,@ExecutedCount,@JustForBirth,@UpdatedDate,@CreatedDate,@CreatedBy,@UpdatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@AutoAddRuleID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CardTypeID", SqlDbType.NVarChar,20),
					new SqlParameter("@CardGradeID", SqlDbType.NVarChar,10),
					new SqlParameter("@AutoAddRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@RuleDesc", SqlDbType.VarChar,50),
					new SqlParameter("@AddAmount", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Interval", SqlDbType.Int,4),
					new SqlParameter("@IntervalUnit", SqlDbType.Int,4),
					new SqlParameter("@DayOfInterval", SqlDbType.Int,4),
					new SqlParameter("@LastExecDate", SqlDbType.DateTime),
					new SqlParameter("@ExecutedCount", SqlDbType.Int,4),
					new SqlParameter("@JustForBirth", SqlDbType.Int,4),
					new SqlParameter("@UpdatedDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,10),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,10)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = model.CardTypeID;
			parameters[2].Value = model.CardGradeID;
			parameters[3].Value = model.AutoAddRuleSeqNo;
			parameters[4].Value = model.RuleDesc;
			parameters[5].Value = model.AddAmount;
			parameters[6].Value = model.StartDate;
			parameters[7].Value = model.EndDate;
			parameters[8].Value = model.Interval;
			parameters[9].Value = model.IntervalUnit;
			parameters[10].Value = model.DayOfInterval;
			parameters[11].Value = model.LastExecDate;
			parameters[12].Value = model.ExecutedCount;
			parameters[13].Value = model.JustForBirth;
			parameters[14].Value = model.UpdatedDate;
			parameters[15].Value = model.CreatedDate;
			parameters[16].Value = model.CreatedBy;
			parameters[17].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardAutoAddValueRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardAutoAddValueRule set ");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("AutoAddRuleSeqNo=@AutoAddRuleSeqNo,");
			strSql.Append("RuleDesc=@RuleDesc,");
			strSql.Append("AddAmount=@AddAmount,");
			strSql.Append("StartDate=@StartDate,");
			strSql.Append("EndDate=@EndDate,");
			strSql.Append("Interval=@Interval,");
			strSql.Append("IntervalUnit=@IntervalUnit,");
			strSql.Append("DayOfInterval=@DayOfInterval,");
			strSql.Append("LastExecDate=@LastExecDate,");
			strSql.Append("ExecutedCount=@ExecutedCount,");
			strSql.Append("JustForBirth=@JustForBirth,");
			strSql.Append("UpdatedDate=@UpdatedDate,");
			strSql.Append("CreatedDate=@CreatedDate,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where AutoAddRuleID=@AutoAddRuleID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.NVarChar,20),
					new SqlParameter("@CardGradeID", SqlDbType.NVarChar,10),
					new SqlParameter("@AutoAddRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@RuleDesc", SqlDbType.VarChar,50),
					new SqlParameter("@AddAmount", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Interval", SqlDbType.Int,4),
					new SqlParameter("@IntervalUnit", SqlDbType.Int,4),
					new SqlParameter("@DayOfInterval", SqlDbType.Int,4),
					new SqlParameter("@LastExecDate", SqlDbType.DateTime),
					new SqlParameter("@ExecutedCount", SqlDbType.Int,4),
					new SqlParameter("@JustForBirth", SqlDbType.Int,4),
					new SqlParameter("@UpdatedDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,10),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,10),
					new SqlParameter("@AutoAddRuleID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.CardTypeID;
			parameters[1].Value = model.CardGradeID;
			parameters[2].Value = model.AutoAddRuleSeqNo;
			parameters[3].Value = model.RuleDesc;
			parameters[4].Value = model.AddAmount;
			parameters[5].Value = model.StartDate;
			parameters[6].Value = model.EndDate;
			parameters[7].Value = model.Interval;
			parameters[8].Value = model.IntervalUnit;
			parameters[9].Value = model.DayOfInterval;
			parameters[10].Value = model.LastExecDate;
			parameters[11].Value = model.ExecutedCount;
			parameters[12].Value = model.JustForBirth;
			parameters[13].Value = model.UpdatedDate;
			parameters[14].Value = model.CreatedDate;
			parameters[15].Value = model.CreatedBy;
			parameters[16].Value = model.UpdatedBy;
			parameters[17].Value = model.AutoAddRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid AutoAddRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardAutoAddValueRule ");
			strSql.Append(" where AutoAddRuleID=@AutoAddRuleID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AutoAddRuleID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = AutoAddRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string AutoAddRuleIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardAutoAddValueRule ");
			strSql.Append(" where AutoAddRuleID in ("+AutoAddRuleIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardAutoAddValueRule GetModel(Guid AutoAddRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 AutoAddRuleID,CardTypeID,CardGradeID,AutoAddRuleSeqNo,RuleDesc,AddAmount,StartDate,EndDate,Interval,IntervalUnit,DayOfInterval,LastExecDate,ExecutedCount,JustForBirth,UpdatedDate,CreatedDate,CreatedBy,UpdatedBy from CardAutoAddValueRule ");
			strSql.Append(" where AutoAddRuleID=@AutoAddRuleID ");
			SqlParameter[] parameters = {
					new SqlParameter("@AutoAddRuleID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = AutoAddRuleID;

			Edge.SVA.Model.CardAutoAddValueRule model=new Edge.SVA.Model.CardAutoAddValueRule();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["AutoAddRuleID"]!=null && ds.Tables[0].Rows[0]["AutoAddRuleID"].ToString()!="")
				{
					model.AutoAddRuleID= new Guid(ds.Tables[0].Rows[0]["AutoAddRuleID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardTypeID"]!=null && ds.Tables[0].Rows[0]["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=ds.Tables[0].Rows[0]["CardTypeID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardGradeID"]!=null && ds.Tables[0].Rows[0]["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=ds.Tables[0].Rows[0]["CardGradeID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AutoAddRuleSeqNo"]!=null && ds.Tables[0].Rows[0]["AutoAddRuleSeqNo"].ToString()!="")
				{
					model.AutoAddRuleSeqNo=int.Parse(ds.Tables[0].Rows[0]["AutoAddRuleSeqNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RuleDesc"]!=null && ds.Tables[0].Rows[0]["RuleDesc"].ToString()!="")
				{
					model.RuleDesc=ds.Tables[0].Rows[0]["RuleDesc"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AddAmount"]!=null && ds.Tables[0].Rows[0]["AddAmount"].ToString()!="")
				{
					model.AddAmount=int.Parse(ds.Tables[0].Rows[0]["AddAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartDate"]!=null && ds.Tables[0].Rows[0]["StartDate"].ToString()!="")
				{
					model.StartDate=DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndDate"]!=null && ds.Tables[0].Rows[0]["EndDate"].ToString()!="")
				{
					model.EndDate=DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Interval"]!=null && ds.Tables[0].Rows[0]["Interval"].ToString()!="")
				{
					model.Interval=int.Parse(ds.Tables[0].Rows[0]["Interval"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IntervalUnit"]!=null && ds.Tables[0].Rows[0]["IntervalUnit"].ToString()!="")
				{
					model.IntervalUnit=int.Parse(ds.Tables[0].Rows[0]["IntervalUnit"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DayOfInterval"]!=null && ds.Tables[0].Rows[0]["DayOfInterval"].ToString()!="")
				{
					model.DayOfInterval=int.Parse(ds.Tables[0].Rows[0]["DayOfInterval"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LastExecDate"]!=null && ds.Tables[0].Rows[0]["LastExecDate"].ToString()!="")
				{
					model.LastExecDate=DateTime.Parse(ds.Tables[0].Rows[0]["LastExecDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExecutedCount"]!=null && ds.Tables[0].Rows[0]["ExecutedCount"].ToString()!="")
				{
					model.ExecutedCount=int.Parse(ds.Tables[0].Rows[0]["ExecutedCount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["JustForBirth"]!=null && ds.Tables[0].Rows[0]["JustForBirth"].ToString()!="")
				{
					model.JustForBirth=int.Parse(ds.Tables[0].Rows[0]["JustForBirth"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedDate"]!=null && ds.Tables[0].Rows[0]["UpdatedDate"].ToString()!="")
				{
					model.UpdatedDate=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedDate"]!=null && ds.Tables[0].Rows[0]["CreatedDate"].ToString()!="")
				{
					model.CreatedDate=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select AutoAddRuleID,CardTypeID,CardGradeID,AutoAddRuleSeqNo,RuleDesc,AddAmount,StartDate,EndDate,Interval,IntervalUnit,DayOfInterval,LastExecDate,ExecutedCount,JustForBirth,UpdatedDate,CreatedDate,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM CardAutoAddValueRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" AutoAddRuleID,CardTypeID,CardGradeID,AutoAddRuleSeqNo,RuleDesc,AddAmount,StartDate,EndDate,Interval,IntervalUnit,DayOfInterval,LastExecDate,ExecutedCount,JustForBirth,UpdatedDate,CreatedDate,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM CardAutoAddValueRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			parameters[0].Value = "CardAutoAddValueRule";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardAutoAddValueRule ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

