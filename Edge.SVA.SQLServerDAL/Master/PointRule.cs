﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:PointRule
	/// </summary>
	public partial class PointRule:IPointRule
	{
		public PointRule()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("PointRuleID", "PointRule"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int PointRuleID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from PointRule");
			strSql.Append(" where PointRuleID=@PointRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@PointRuleID", SqlDbType.Int,4)
			};
			parameters[0].Value = PointRuleID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.PointRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into PointRule(");
			strSql.Append("CardTypeID,CardGradeID,PointRuleType,PointRuleSeqNo,PointRuleOper,PointRuleAmount,PointRulePoints,StartDate,EndDate,PointRuleLevel,MemberDateType,EffectiveDateType,EffectiveDate,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,Status,PointRuleCode,PointRuleDesc1,PointRuleDesc2,PointRuleDesc3,HitItem,DayFlagCode,WeekFlagCode,MonthFlagCode)");
			strSql.Append(" values (");
			strSql.Append("@CardTypeID,@CardGradeID,@PointRuleType,@PointRuleSeqNo,@PointRuleOper,@PointRuleAmount,@PointRulePoints,@StartDate,@EndDate,@PointRuleLevel,@MemberDateType,@EffectiveDateType,@EffectiveDate,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@Status,@PointRuleCode,@PointRuleDesc1,@PointRuleDesc2,@PointRuleDesc3,@HitItem,@DayFlagCode,@WeekFlagCode,@MonthFlagCode)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@PointRuleType", SqlDbType.Int,4),
					new SqlParameter("@PointRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@PointRuleOper", SqlDbType.Int,4),
					new SqlParameter("@PointRuleAmount", SqlDbType.Money,8),
					new SqlParameter("@PointRulePoints", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@PointRuleLevel", SqlDbType.Int,4),
					new SqlParameter("@MemberDateType", SqlDbType.Int,4),
					new SqlParameter("@EffectiveDateType", SqlDbType.Int,4),
					new SqlParameter("@EffectiveDate", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@PointRuleCode", SqlDbType.VarChar,64),
					new SqlParameter("@PointRuleDesc1", SqlDbType.VarChar,512),
					new SqlParameter("@PointRuleDesc2", SqlDbType.VarChar,512),
					new SqlParameter("@PointRuleDesc3", SqlDbType.VarChar,512),
					new SqlParameter("@HitItem", SqlDbType.Int,4),
					new SqlParameter("@DayFlagCode", SqlDbType.VarChar,64),
					new SqlParameter("@WeekFlagCode", SqlDbType.VarChar,64),
					new SqlParameter("@MonthFlagCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.CardTypeID;
			parameters[1].Value = model.CardGradeID;
			parameters[2].Value = model.PointRuleType;
			parameters[3].Value = model.PointRuleSeqNo;
			parameters[4].Value = model.PointRuleOper;
			parameters[5].Value = model.PointRuleAmount;
			parameters[6].Value = model.PointRulePoints;
			parameters[7].Value = model.StartDate;
			parameters[8].Value = model.EndDate;
			parameters[9].Value = model.PointRuleLevel;
			parameters[10].Value = model.MemberDateType;
			parameters[11].Value = model.EffectiveDateType;
			parameters[12].Value = model.EffectiveDate;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.UpdatedOn;
			parameters[15].Value = model.CreatedBy;
			parameters[16].Value = model.UpdatedBy;
			parameters[17].Value = model.Status;
			parameters[18].Value = model.PointRuleCode;
			parameters[19].Value = model.PointRuleDesc1;
			parameters[20].Value = model.PointRuleDesc2;
			parameters[21].Value = model.PointRuleDesc3;
			parameters[22].Value = model.HitItem;
			parameters[23].Value = model.DayFlagCode;
			parameters[24].Value = model.WeekFlagCode;
			parameters[25].Value = model.MonthFlagCode;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.PointRule model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update PointRule set ");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("PointRuleType=@PointRuleType,");
			strSql.Append("PointRuleSeqNo=@PointRuleSeqNo,");
			strSql.Append("PointRuleOper=@PointRuleOper,");
			strSql.Append("PointRuleAmount=@PointRuleAmount,");
			strSql.Append("PointRulePoints=@PointRulePoints,");
			strSql.Append("StartDate=@StartDate,");
			strSql.Append("EndDate=@EndDate,");
			strSql.Append("PointRuleLevel=@PointRuleLevel,");
			strSql.Append("MemberDateType=@MemberDateType,");
			strSql.Append("EffectiveDateType=@EffectiveDateType,");
			strSql.Append("EffectiveDate=@EffectiveDate,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("Status=@Status,");
			strSql.Append("PointRuleCode=@PointRuleCode,");
			strSql.Append("PointRuleDesc1=@PointRuleDesc1,");
			strSql.Append("PointRuleDesc2=@PointRuleDesc2,");
			strSql.Append("PointRuleDesc3=@PointRuleDesc3,");
			strSql.Append("HitItem=@HitItem,");
			strSql.Append("DayFlagCode=@DayFlagCode,");
			strSql.Append("WeekFlagCode=@WeekFlagCode,");
			strSql.Append("MonthFlagCode=@MonthFlagCode");
			strSql.Append(" where PointRuleID=@PointRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardTypeID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@PointRuleType", SqlDbType.Int,4),
					new SqlParameter("@PointRuleSeqNo", SqlDbType.Int,4),
					new SqlParameter("@PointRuleOper", SqlDbType.Int,4),
					new SqlParameter("@PointRuleAmount", SqlDbType.Money,8),
					new SqlParameter("@PointRulePoints", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@PointRuleLevel", SqlDbType.Int,4),
					new SqlParameter("@MemberDateType", SqlDbType.Int,4),
					new SqlParameter("@EffectiveDateType", SqlDbType.Int,4),
					new SqlParameter("@EffectiveDate", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@PointRuleCode", SqlDbType.VarChar,64),
					new SqlParameter("@PointRuleDesc1", SqlDbType.VarChar,512),
					new SqlParameter("@PointRuleDesc2", SqlDbType.VarChar,512),
					new SqlParameter("@PointRuleDesc3", SqlDbType.VarChar,512),
					new SqlParameter("@HitItem", SqlDbType.Int,4),
					new SqlParameter("@DayFlagCode", SqlDbType.VarChar,64),
					new SqlParameter("@WeekFlagCode", SqlDbType.VarChar,64),
					new SqlParameter("@MonthFlagCode", SqlDbType.VarChar,64),
					new SqlParameter("@PointRuleID", SqlDbType.Int,4)};
			parameters[0].Value = model.CardTypeID;
			parameters[1].Value = model.CardGradeID;
			parameters[2].Value = model.PointRuleType;
			parameters[3].Value = model.PointRuleSeqNo;
			parameters[4].Value = model.PointRuleOper;
			parameters[5].Value = model.PointRuleAmount;
			parameters[6].Value = model.PointRulePoints;
			parameters[7].Value = model.StartDate;
			parameters[8].Value = model.EndDate;
			parameters[9].Value = model.PointRuleLevel;
			parameters[10].Value = model.MemberDateType;
			parameters[11].Value = model.EffectiveDateType;
			parameters[12].Value = model.EffectiveDate;
			parameters[13].Value = model.CreatedOn;
			parameters[14].Value = model.UpdatedOn;
			parameters[15].Value = model.CreatedBy;
			parameters[16].Value = model.UpdatedBy;
			parameters[17].Value = model.Status;
			parameters[18].Value = model.PointRuleCode;
			parameters[19].Value = model.PointRuleDesc1;
			parameters[20].Value = model.PointRuleDesc2;
			parameters[21].Value = model.PointRuleDesc3;
			parameters[22].Value = model.HitItem;
			parameters[23].Value = model.DayFlagCode;
			parameters[24].Value = model.WeekFlagCode;
			parameters[25].Value = model.MonthFlagCode;
			parameters[26].Value = model.PointRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PointRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from PointRule ");
			strSql.Append(" where PointRuleID=@PointRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@PointRuleID", SqlDbType.Int,4)
			};
			parameters[0].Value = PointRuleID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string PointRuleIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from PointRule ");
			strSql.Append(" where PointRuleID in ("+PointRuleIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.PointRule GetModel(int PointRuleID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PointRuleID,CardTypeID,CardGradeID,PointRuleType,PointRuleSeqNo,PointRuleOper,PointRuleAmount,PointRulePoints,StartDate,EndDate,PointRuleLevel,MemberDateType,EffectiveDateType,EffectiveDate,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,Status,PointRuleCode,PointRuleDesc1,PointRuleDesc2,PointRuleDesc3,HitItem,DayFlagCode,WeekFlagCode,MonthFlagCode from PointRule ");
			strSql.Append(" where PointRuleID=@PointRuleID");
			SqlParameter[] parameters = {
					new SqlParameter("@PointRuleID", SqlDbType.Int,4)
			};
			parameters[0].Value = PointRuleID;

			Edge.SVA.Model.PointRule model=new Edge.SVA.Model.PointRule();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["PointRuleID"]!=null && ds.Tables[0].Rows[0]["PointRuleID"].ToString()!="")
				{
					model.PointRuleID=int.Parse(ds.Tables[0].Rows[0]["PointRuleID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardTypeID"]!=null && ds.Tables[0].Rows[0]["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=int.Parse(ds.Tables[0].Rows[0]["CardTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardGradeID"]!=null && ds.Tables[0].Rows[0]["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleType"]!=null && ds.Tables[0].Rows[0]["PointRuleType"].ToString()!="")
				{
					model.PointRuleType=int.Parse(ds.Tables[0].Rows[0]["PointRuleType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleSeqNo"]!=null && ds.Tables[0].Rows[0]["PointRuleSeqNo"].ToString()!="")
				{
					model.PointRuleSeqNo=int.Parse(ds.Tables[0].Rows[0]["PointRuleSeqNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleOper"]!=null && ds.Tables[0].Rows[0]["PointRuleOper"].ToString()!="")
				{
					model.PointRuleOper=int.Parse(ds.Tables[0].Rows[0]["PointRuleOper"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleAmount"]!=null && ds.Tables[0].Rows[0]["PointRuleAmount"].ToString()!="")
				{
					model.PointRuleAmount=decimal.Parse(ds.Tables[0].Rows[0]["PointRuleAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRulePoints"]!=null && ds.Tables[0].Rows[0]["PointRulePoints"].ToString()!="")
				{
					model.PointRulePoints=int.Parse(ds.Tables[0].Rows[0]["PointRulePoints"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartDate"]!=null && ds.Tables[0].Rows[0]["StartDate"].ToString()!="")
				{
					model.StartDate=DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndDate"]!=null && ds.Tables[0].Rows[0]["EndDate"].ToString()!="")
				{
					model.EndDate=DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleLevel"]!=null && ds.Tables[0].Rows[0]["PointRuleLevel"].ToString()!="")
				{
					model.PointRuleLevel=int.Parse(ds.Tables[0].Rows[0]["PointRuleLevel"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MemberDateType"]!=null && ds.Tables[0].Rows[0]["MemberDateType"].ToString()!="")
				{
					model.MemberDateType=int.Parse(ds.Tables[0].Rows[0]["MemberDateType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EffectiveDateType"]!=null && ds.Tables[0].Rows[0]["EffectiveDateType"].ToString()!="")
				{
					model.EffectiveDateType=int.Parse(ds.Tables[0].Rows[0]["EffectiveDateType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EffectiveDate"]!=null && ds.Tables[0].Rows[0]["EffectiveDate"].ToString()!="")
				{
					model.EffectiveDate=int.Parse(ds.Tables[0].Rows[0]["EffectiveDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PointRuleCode"]!=null && ds.Tables[0].Rows[0]["PointRuleCode"].ToString()!="")
				{
					model.PointRuleCode=ds.Tables[0].Rows[0]["PointRuleCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PointRuleDesc1"]!=null && ds.Tables[0].Rows[0]["PointRuleDesc1"].ToString()!="")
				{
					model.PointRuleDesc1=ds.Tables[0].Rows[0]["PointRuleDesc1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PointRuleDesc2"]!=null && ds.Tables[0].Rows[0]["PointRuleDesc2"].ToString()!="")
				{
					model.PointRuleDesc2=ds.Tables[0].Rows[0]["PointRuleDesc2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PointRuleDesc3"]!=null && ds.Tables[0].Rows[0]["PointRuleDesc3"].ToString()!="")
				{
					model.PointRuleDesc3=ds.Tables[0].Rows[0]["PointRuleDesc3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["HitItem"]!=null && ds.Tables[0].Rows[0]["HitItem"].ToString()!="")
				{
					model.HitItem=int.Parse(ds.Tables[0].Rows[0]["HitItem"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DayFlagCode"]!=null && ds.Tables[0].Rows[0]["DayFlagCode"].ToString()!="")
				{
					model.DayFlagCode=ds.Tables[0].Rows[0]["DayFlagCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["WeekFlagCode"]!=null && ds.Tables[0].Rows[0]["WeekFlagCode"].ToString()!="")
				{
					model.WeekFlagCode=ds.Tables[0].Rows[0]["WeekFlagCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MonthFlagCode"]!=null && ds.Tables[0].Rows[0]["MonthFlagCode"].ToString()!="")
				{
					model.MonthFlagCode=ds.Tables[0].Rows[0]["MonthFlagCode"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PointRuleID,CardTypeID,CardGradeID,PointRuleType,PointRuleSeqNo,PointRuleOper,PointRuleAmount,PointRulePoints,StartDate,EndDate,PointRuleLevel,MemberDateType,EffectiveDateType,EffectiveDate,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,Status,PointRuleCode,PointRuleDesc1,PointRuleDesc2,PointRuleDesc3,HitItem,DayFlagCode,WeekFlagCode,MonthFlagCode ");
			strSql.Append(" FROM PointRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" PointRuleID,CardTypeID,CardGradeID,PointRuleType,PointRuleSeqNo,PointRuleOper,PointRuleAmount,PointRulePoints,StartDate,EndDate,PointRuleLevel,MemberDateType,EffectiveDateType,EffectiveDate,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,Status,PointRuleCode,PointRuleDesc1,PointRuleDesc2,PointRuleDesc3,HitItem,DayFlagCode,WeekFlagCode,MonthFlagCode ");
			strSql.Append(" FROM PointRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM PointRule ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.PointRuleID desc");
			}
			strSql.Append(")AS Row, T.*  from PointRule T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "PointRule";
			parameters[1].Value = "PointRuleID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

