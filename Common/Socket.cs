using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Edge.Common
{
    public class GetSocket
    {
        private static Socket ConnectSocket(string server, int port)
        {
            Socket s = null;
            IPHostEntry hostEntry = null;

            // Get host related information.
            hostEntry = Dns.GetHostEntry(server);

            // Loop through the AddressList to obtain the supported AddressFamily. This is to avoid
            // an exception that occurs when the host IP Address is not compatible with the address family
            // (typical in the IPv6 case).
            foreach (IPAddress address in hostEntry.AddressList)
            {
                //IPEndPoint ipe = new IPEndPoint(address, port);
                IPEndPoint ipe = new IPEndPoint(IPAddress.Parse(server), port);
                Socket tempSocket =
                    new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                    s = tempSocket;
                    break;
                }
                else
                {
                    continue;
                }
            }
            return s;
        }

        // This method requests the home page content for the specified server.
        public static string SocketSendReceive(string server, int port, string request)
        {
            Byte[] bytesStr = Encoding.ASCII.GetBytes(request);
            int byteLen = bytesStr.Length;
            Byte[] bytesSent = new Byte[byteLen + 4];

            bytesSent[3] = (byte)((byteLen) & 0xFF);
            bytesSent[2] = (byte)((byteLen >> 8) & 0xFF);
            bytesSent[1] = (byte)((byteLen >> 16) & 0xFF);
            bytesSent[0] = (byte)((byteLen >> 24) & 0xFF);
            bytesStr.CopyTo(bytesSent, 4);

            // Create a socket connection with the specified server and port.
            Socket s = ConnectSocket(server, port);

            if (s == null)
                return ("Connection failed");

            s.NoDelay = true;
            s.ReceiveTimeout = 6000;
            // Send request to the server.
            s.Send(bytesSent, bytesSent.Length, 0);

            // Receive the server home page content.
            int bytes = 0;
            string response = "";
            int responselen = 4;
            int ReceiveNum = 0;
            Byte[] bytesBuf = new Byte[256];
            Byte[] bytesReceived = new Byte[2048];

            // The following will block until te page is transmitted.
            do            
            {
                bytes = s.Receive(bytesBuf, bytesBuf.Length, 0);
                bytesBuf.CopyTo(bytesReceived, ReceiveNum);
                ReceiveNum = ReceiveNum + bytes;
                if ((ReceiveNum > 3) && (responselen == 4))
                {
                    responselen = (int)bytesReceived[3] + (int)(bytesReceived[2] << 8) + (int)(bytesReceived[1] << 16) + (int)(bytesReceived[0] << 24);
                }
      
            }
            while (ReceiveNum < responselen);
            response = response + Encoding.ASCII.GetString(bytesReceived, 4, responselen);

            s.Disconnect(false);

//            response = "ReceiveNum:" + ReceiveNum.ToString() + "  responselen:" + responselen.ToString() + " response:" + response;
            return response;
        }

    }
}
