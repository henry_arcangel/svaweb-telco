﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;

namespace Edge.Common
{
    public class CFSXMLFormat
    {
        public string GetRequestStr(DataTable ndata, ref string ErrorMsg)
        {
            string reqstr = "";
            ErrorMsg = "";
            try
            {
                DataRow dr = ndata.Rows[0];
                XmlDocument doc = new XmlDocument();
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
                doc.AppendChild(dec);
                //创建一个根节点（一级）
                XmlElement root = doc.CreateElement("Interface");
                doc.AppendChild(root);
                root.SetAttribute("OpCode", dr["OpCode"].ToString());
                root.SetAttribute("businessType", dr["businessType"].ToString());
                root.SetAttribute("xmlns", dr["xmlns"].ToString());
                //创建节点（二级）
                XmlElement node = doc.CreateElement("Transaction");

                node.SetAttribute("StoreId", dr["StoreId"].ToString());
                node.SetAttribute("PosId", dr["PosId"].ToString());
                node.SetAttribute("TxnDate", dr["TxnDate"].ToString());
                node.SetAttribute("TxnNo", dr["TxnNo"].ToString());
                node.SetAttribute("SN", dr["SN"].ToString());
                node.SetAttribute("TargetSN", dr["TargetSN"].ToString());
                node.SetAttribute("Amount", dr["Amount"].ToString());
                node.SetAttribute("TotalAmount", dr["TotalAmount"].ToString());

                node.SetAttribute("ItemUID", dr["ItemUID"].ToString()); //ito naman, dating naka comment out. By Roche
                node.SetAttribute("TransactionType", dr["TransactionType"].ToString()); //dinagdag ko to. Kailangan daw eh. By Roche
                node.SetAttribute("ProdCode", dr["ProdCode"].ToString()); //dating dr["ItemUID"]. Ginawa kong ProdCode. By Roche

                node.SetAttribute("MobileNumber", dr["MobileNumber"].ToString());
                node.SetAttribute("CashierId", dr["CashierId"].ToString());
                node.SetAttribute("TxnType", dr["TxnType"].ToString());
                node.SetAttribute("BrandCode", dr["BrandCode"].ToString());
                node.SetAttribute("CardUID", dr["CardUID"].ToString());
                node.SetAttribute("SKU", dr["SKU"].ToString());
                root.AppendChild(node);
                reqstr = doc.OuterXml.ToString();
                ErrorMsg = "";
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message + "  Output String:" + reqstr;
            }
            return reqstr;
        }

        public DataTable AnalyResponseStr(string ResponseStr, ref string ErrorMsg)
        {
            ErrorMsg = "";
            DataTable dt = new DataTable();
            try
            {               
                dt.Columns.Add("OpCode", typeof(string));
                dt.Columns.Add("ResponseCode", typeof(string));
                dt.Columns.Add("ErrorMessage", typeof(string));

                dt.Columns.Add("businessType", typeof(string));
                dt.Columns.Add("StoreId", typeof(string));
                dt.Columns.Add("PosId", typeof(string));
                dt.Columns.Add("TxnDate", typeof(string));
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("SN", typeof(string));
                dt.Columns.Add("TargetSN", typeof(string));
                dt.Columns.Add("Amount", typeof(string));
                dt.Columns.Add("TotalAmount", typeof(string));

                dt.Columns.Add("ItemUID", typeof(string)); //ganun din naman. By Roche
                dt.Columns.Add("TransactionType", typeof(string)); //dinagdag ko, kailangan daw. By Roche
                dt.Columns.Add("ProdCode", typeof(string)); //para ito sa prodcode. By Roche

                dt.Columns.Add("MobileNumber", typeof(string));
                dt.Columns.Add("CashierId", typeof(string));
                dt.Columns.Add("TxnType", typeof(string));
                dt.Columns.Add("BrandCode", typeof(string));
                dt.Columns.Add("CardUID", typeof(string));

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ResponseStr);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(doc.NameTable); 
                nsMgr.AddNamespace("ns", "http://www.tap.org/gc/beans");

                XmlNode root = doc.SelectSingleNode("/ns:Interface", nsMgr);//查找<Interface>
                XmlNode tran = doc.SelectSingleNode("/ns:Interface/ns:Transaction", nsMgr);//查找<Interface>
                DataRow dr = dt.NewRow();

                dr["OpCode"] = root.Attributes["OpCode"].Value.ToString();
                dr["ResponseCode"] = root.Attributes["ResponseCode"].Value.ToString().Trim();                    
                dr["ErrorMessage"] = root.Attributes["ErrorMessage"].Value.ToString();
                dr["businessType"] = root.Attributes["businessType"].Value.ToString();
                dr["StoreId"] = tran.Attributes["StoreId"].Value.ToString();
                dr["PosId"] = tran.Attributes["PosId"].Value.ToString();
                dr["TxnDate"] = tran.Attributes["TxnDate"].Value.ToString();
                dr["TxnNo"] = tran.Attributes["TxnNo"].Value.ToString();
                dr["SN"] = tran.Attributes["SN"].Value.ToString();
                dr["TargetSN"] = tran.Attributes["TargetSN"].Value.ToString();
                dr["Amount"] = tran.Attributes["Amount"].Value.ToString();
                dr["TotalAmount"] = tran.Attributes["TotalAmount"].Value.ToString();

                dr["ItemUID"] = tran.Attributes["ItemUID"].Value.ToString(); //ganun din naman to. By Roche
                dr["TransactionType"] = tran.Attributes["TransactionType"].Value.ToString(); //dinagdag din, kailagan daw. By Roche
                dr["ProdCode"] = tran.Attributes["ProdCode"].Value.ToString(); //prodcode to. By Roche

                dr["MobileNumber"] = tran.Attributes["MobileNumber"].Value.ToString();
                dr["CashierId"] = tran.Attributes["CashierId"].Value.ToString();
                dr["TxnType"] = tran.Attributes["TxnType"].Value.ToString();
                dr["BrandCode"] = tran.Attributes["BrandCode"].Value.ToString();
                dr["CardUID"] = tran.Attributes["CardUID"].Value.ToString();
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message + "  Response String:" + ResponseStr;
            }

            return dt;
        }
    }
}
