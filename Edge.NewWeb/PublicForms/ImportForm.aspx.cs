﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Edge.Web.Controllers;
using Edge.Web.Tools;
using System.Data;
using System.Text.RegularExpressions;
using Edge.SVA.Model.Domain.Surpport;
using System.Data.SqlClient;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.PublicForms
{
    public partial class ImportForm : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterCloseEvent(btnClose);
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateFile())
            {
                if (Request["Menu"].ToLower() == "store")
                {
                    string pathFile = this.ImportFile.SaveToServer("Images/Store");
                    string path = Server.MapPath("~" + pathFile);
                    DataTable dt = ExcelTool.GetFirstSheet(path);
                    SVASessionInfo.ImportStorePath = path;
                }
                if (Request["Menu"].ToLower() == "memberorder")
                {
                    string pathFile = this.ImportFile.SaveAttachFileToServer("MemberInformation/OtherInformation/Orders");
                    string path = Server.MapPath("~" + pathFile);
                    DataTable dt = ExcelTool.GetFirstSheet(path);
                    SVASessionInfo.ImportStorePath = path;
                }
                //Add by Nathan 20140626++
                if (Request["Menu"].ToLower() == "tender")
                {
                    string pathFile = this.ImportFile.SaveAttachFileToServer("MemberInformation/OtherInformation/Tenders");
                    string path = Server.MapPath("~" + pathFile);
                    DataTable dt = ExcelTool.GetFirstSheet(path);
                    SVASessionInfo.ImportTenderPath = path;
                }
                //Add by Nathan 20140626--

                //Add by Gavin 20150401++
                if (Request["Menu"].ToLower() == "couponredeem")
                {
                    string pathFile = this.ImportFile.SaveAttachFileToServer("BatchImportCoupons");
                    string path = Server.MapPath("~" + pathFile);
                    DataTable dt = ExcelTool.GetFirstSheet(path);
                    SVASessionInfo.ImportCouponRedeemPath = path;
                }
                //Add by Gavin 20150401--
                CloseAndPostBack();
            }
        }
        protected bool ValidateFile()
        {
            if (Request["Menu"].ToLower() == "store")
            {
                if (string.IsNullOrEmpty(this.ImportFile.ShortFileName))
                {
                    ShowWarning(Resources.MessageTips.NoData);
                    return false;
                }
                string filename = Path.GetExtension(this.ImportFile.ShortFileName).TrimStart('.');
                if (filename.ToLower() != "xls")
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90446"));
                    return false;
                }
            }
            return true;
        }
    }
}