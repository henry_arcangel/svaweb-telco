﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.TelcoVariant
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.ImportGM, Edge.SVA.Model.ImportGM>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                Tools.ControlTool.BindCardTypeCode(CardTypeCode);

            }

        }

        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGradeCode(CardGradeCode,Convert.ToString(CardTypeCode.SelectedValue.ToString()));
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasSKUCode(this.UPC.Text.Trim(), 0))
            {
                this.ShowWarning(Resources.MessageTips.AlreadyExists);
                return;
            }

            if (this.ValidateInput())
            {

                Edge.SVA.Model.ImportGM item = this.GetAddObject();

                if (item != null)
                {
                    item.CreatedBy = Convert.ToString(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
                    item.CreatedOn = System.DateTime.Now;
                    item.UpdatedBy = Convert.ToString(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
                    item.UpdatedOn = System.DateTime.Now;
                    //   item.CardGradeCode = this.CardGradeCode.Text;
                    //   System.Windows.Forms.MessageBox.Show(this.CardGradeCode.Text);
                    //  item.CardTypeCode = this.CardTypeCode.Text;

                    //   item.SKU = item.SKU.ToUpper();
                }

                if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.ImportGM>(item) > 0)
                {
                    this.CloseAndRefresh();
                }
                else
                {
                    this.ShowAddFailed();
                }
            }
        }

        // added by Darwin Pasco: Input validation
        private bool ValidateInput()
        {
            bool functionReturnVaue = false;
            decimal num;

            if (string.IsNullOrEmpty(UPC.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + UPC.Label.ToString());
                return functionReturnVaue;
            }
            else if (UPC.Text.Substring(0, 1) == "0")
            {
                ShowWarning(Resources.MessageTips.SKUNumberCannotStartWithZero + ": " + this.UPC.Label.ToString());
            }
            else if (string.IsNullOrEmpty(SKUDesc.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.SKUDesc.Label.ToString());
                return functionReturnVaue;
            }            
            else if (string.IsNullOrEmpty(ProdCode.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.ProdCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (!decimal.TryParse(SKUUnitAmount.Text, out num))
            {
                ShowWarning(Resources.MessageTips.NotNumeric + ": " + this.SKUUnitAmount.Label.ToString());
                return functionReturnVaue;
            }
            else if (Convert.ToDecimal(SKUUnitAmount.Text) <= 0)
            {
                ShowWarning(Resources.MessageTips.CannotBeLessThanOrEqualToZero + ": " + this.SKUUnitAmount.Label.ToString());
                return functionReturnVaue;
            }
            else if (CardTypeCode.SelectedText == "----------")
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + CardTypeCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (CardGradeCode.SelectedText == "----------")
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.CardGradeCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (!decimal.TryParse(GMValue.Text, out num))
            {
                ShowWarning(Resources.MessageTips.NotNumeric + ": " + this.GMValue.Label.ToString());
                return functionReturnVaue;
            }
            else if (Convert.ToDecimal(GMValue.Text) < 0)
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.GMValue.Label.ToString());
                return functionReturnVaue;
            }
            else if (string.IsNullOrEmpty(NumberPrefix.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.NumberPrefix.Label.ToString());
                return functionReturnVaue;
            }
            else
            {
                functionReturnVaue = true;
            }
            return functionReturnVaue;
        }
    }
}
