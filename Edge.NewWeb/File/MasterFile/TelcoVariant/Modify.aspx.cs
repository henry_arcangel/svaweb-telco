﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Text;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.TelcoVariant
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.ImportGM, Edge.SVA.Model.ImportGM>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }


                Tools.ControlTool.BindCardTypeCode(CardTypeCode);
                Tools.ControlTool.BindCardGradeCode(CardGradeCode);

                RegisterCloseEvent(btnClose);
            }
        }
        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGradeCode(CardGradeCode, Convert.ToString(CardTypeCode.SelectedValue.ToString()));
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.SVA.Model.ImportGM GM = new Edge.SVA.BLL.ImportGM().GetModelSKU(Convert.ToInt32(Model.UPC));
    
                //this.CardGradeCode.Text = Model.CardGradeCode  + " - " + Model.CardGradeName;

               // Model.SKUUnitAmount = Convert.ToInt32(Model.SKUUnitAmount.Value.ToString("#####"));
          //      ViewState["UPC"] = Model.UPC;
              
                      //          Edge.SVA.Model.ImportGM GM = new Edge.SVA.BLL.ImportGM().GetModel(Model.UPC);
                               // string brandText = DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                               // string brandID = brand.BrandID.ToString();

                              //  this.SKUDesc.Text = GM.SKUDesc;
                /*           this.BrandID.Items.Add(new FineUI.ListItem() { Text = brandText, Value = brandID });

                         string msg = "";

                         if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                         {
                             this.CardNumMaskImport.Text = Model.CardNumMask;
                             this.CardNumPatternImport.Text = Model.CardNumPattern;
                         }
                         else if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                         {
                             this.CardNumMask.Text = Model.CardNumMask;
                             this.CardNumPattern.Text = Model.CardNumPattern;
                         }


                         //如果已经创建Card或者已创建CardGrade， 就不能修改其他规则
                         if ((DALTool.isCardTypeCreatedCard(Model.CardTypeID, ref msg)) || DALTool.isCardTypeCreatedCardGrade(Model.CardTypeID, ref msg))
                         {
                             if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                             {
                                 this.CardNumMask.Enabled = false;
                                 this.CardNumPattern.Enabled = false;
                                 this.CardCheckdigit.Enabled = false;
                                 this.CardNumberToUID.Enabled = false;
                                 this.CheckDigitModeID.Enabled = false;
                             }
                             else
                             {
                                 this.IsConsecutiveUID.Enabled = false;
                                 this.UIDCheckDigit.Enabled = false;
                                 this.UIDToCardNumber.Enabled = false;
                                 this.CardNumMaskImport.Enabled = false;
                                 this.CardNumPatternImport.Enabled = false;
                             }

                             this.CardTypeCode.Enabled = false;
                             this.CardMustHasOwner.Enabled = false;
                             this.CashExpiredate.Enabled = false;
                             this.PointExpiredate.Enabled = false;
                             this.IsPhysicalCard.Enabled = false;
                             this.IsImportUIDNumber.Enabled = false;
                             this.BrandID.Enabled = false;                 
                         }

                         //if (DALTool.isCardTypeCreatedCard(Model.CardTypeID, ref msg))//如果已经创建卡就不能修改其他规则
                         //{
                         //    if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                         //    {
                         //        this.CardNumMask.Enabled = false;
                         //        this.CardNumPattern.Enabled = false;
                         //        this.CardCheckdigit.Enabled = false;
                         //        this.CardNumberToUID.Enabled = false;
                         //        this.CheckDigitModeID.Enabled = false;
                         //    }
                         //    else
                         //    {
                         //        this.IsConsecutiveUID.Enabled = false;
                         //        this.UIDCheckDigit.Enabled = false;
                         //        this.UIDToCardNumber.Enabled = false;
                         //        this.CardNumMaskImport.Enabled = false;
                         //        this.CardNumPatternImport.Enabled = false;
                         //    }

                         //    this.CardTypeCode.Enabled = false;
                         //    this.CardMustHasOwner.Enabled = false;
                         //    this.CashExpiredate.Enabled = false;
                         //    this.PointExpiredate.Enabled = false;
                         //    this.IsPhysicalCard.Enabled = false;
                         //    this.IsImportUIDNumber.Enabled = false;
                         //}
                         //else//未创建卡
                         //{
                         //    if (DALTool.isCardTypeCreatedCardGrade(Model.CardTypeID, ref msg))//已创建CardGrade
                         //    {
                         //        if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                         //        {
                         //            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                         //            {
                         //                this.CardNumMask.Enabled = false;
                         //                this.CardNumPattern.Enabled = false;
                         //                this.CardCheckdigit.Enabled = false;
                         //                this.CardNumberToUID.Enabled = false;
                         //                this.CheckDigitModeID.Enabled = false;
                         //            }
                         //        }
                         //        else
                         //        {
                         //            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                         //            {
                         //                this.CardNumMaskImport.Enabled = false;
                         //                this.CardNumPatternImport.Enabled = false;
                         //            }
                         //            //this.IsConsecutiveUID.Enabled = false;
                         //            //this.UIDCheckDigit.Enabled = false;
                         //            //this.UIDToCardNumber.Enabled = false;
                         //        }

                         //        this.IsImportUIDNumber.Enabled = false;
                         //    }
                         //}

                         checkIsImportCardNumber(false);
                         */
                 
            }
        }
        //可以不重载，但重载性能更佳
        //protected override void SetObject()
        //{
        //    base.SetObject(this.Model, this.Panel1.Controls.GetEnumerator());
        //}


        //protected override SVA.Model.CardType GetPageObject(SVA.Model.CardType obj)
        //{
        //    List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

        //    foreach (System.Web.UI.Control con in this.Panel1.Controls)
        //    {
        //        if (con is FineUI.GroupPanel)
        //        {
        //            foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
        //        }
        //    }
        //    return base.GetPageObject(obj, list.GetEnumerator());
        //}

        //protected void IsImportUIDNumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    checkIsImportCardNumber(true);
        //}

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (this.ValidateInput())
            {
                Edge.SVA.Model.ImportGM item = this.GetUpdateObject();

            //if (!ValidObject(item)) return;

            
                if (item != null)
                {

                    item.UpdatedBy = Convert.ToString(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
                    item.UpdatedOn = System.DateTime.Now;
                    //   item.CardGradeCode = this.CardGradeCode.Text;
                    //   System.Windows.Forms.MessageBox.Show(this.CardGradeCode.Text);
                    //  item.CardTypeCode = this.CardTypeCode.Text;

                    //   item.SKU = item.SKU.ToUpper();
                }
                if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.ImportGM>(item))
                {
                    this.CloseAndRefresh();
                    // CardTypeRepostory.Singleton.Refresh();
                    CloseAndPostBack();
                }
                else
                {
                    this.ShowAddFailed();
                }

            }
            //else
            //{
            //  // ShowUpdateFailed();
            //}
        }


        // added by Darwin Pasco: Input validation
        private bool ValidateInput()
        {
            bool functionReturnVaue = false;
            decimal num;

            if (string.IsNullOrEmpty(UPC.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + UPC.Label.ToString());
                return functionReturnVaue;
            }
            else if (string.IsNullOrEmpty(SKUDesc.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.SKUDesc.Label.ToString());
                return functionReturnVaue;            
            }
            else if (string.IsNullOrEmpty(ProdCode.Text))
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.ProdCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (!decimal.TryParse(SKUUnitAmount.Text, out num))
            {
                ShowWarning(Resources.MessageTips.NotNumeric + ": " + this.SKUUnitAmount.Label.ToString());
                return functionReturnVaue;
            }
            else if (Convert.ToDecimal(SKUUnitAmount.Text) <= 0)
            {
                ShowWarning(Resources.MessageTips.CannotBeLessThanOrEqualToZero + ": " + this.SKUUnitAmount.Label.ToString());
                return functionReturnVaue;
            }
            else if (CardTypeCode.SelectedText == "----------")
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + CardTypeCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (CardGradeCode.SelectedText == "----------")
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.CardGradeCode.Label.ToString());
                return functionReturnVaue;
            }
            else if (!decimal.TryParse(GMValue.Text, out num))
            {
                ShowWarning(Resources.MessageTips.NotNumeric + ": " + this.GMValue.Label.ToString());
                return functionReturnVaue;
            }
            else if (Convert.ToDecimal(GMValue.Text) < 0)
            {
                ShowWarning(Resources.MessageTips.RequiredField + ": " + this.GMValue.Label.ToString());
                return functionReturnVaue;
            }
            else
            {
                functionReturnVaue = true;
            }
            return functionReturnVaue;
        }

     

        //private bool ValidObject(SVA.Model.CardType item)
        //{
        //    if (item == null) return false;

        //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
        //    item.UpdatedOn = System.DateTime.Now;
        //    item.CardTypeCode = item.CardTypeCode.ToUpper();
        //    item.CardNumMask = item.CardNumMask.ToUpper();

        //    if (Edge.Web.Tools.DALTool.isHasCardTypeCode(item.CardTypeCode, item.CardTypeID))
        //    {
        //        ShowWarning(Resources.MessageTips.ExistCardTypeCode);
        //        return false;
        //    }
        //    try
        //    {
        //        if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
        //        {
        //            //-------------------------------导入-------------------------------
        //            item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
        //            item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
        //            item.CardCheckdigit = null;
        //            item.CardNumberToUID = null;

        //            if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
        //            {
        //                if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport, this.CardNumPatternImport))
        //                {
        //                    return false;
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
        //            {
        //                if (!Tools.DALTool.CheckNumberMask(this.CardNumMaskImport.Text.Trim(), this.CardNumPatternImport.Text.Trim(), 0, item.CardTypeID))
        //                {
        //                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
        //                    return false;
        //                }
        //            }

        //            if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
        //            if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");

        //        }
        //        else
        //        {
        //            //-------------------------------手动-------------------------------
        //            item.IsConsecutiveUID = null;
        //            item.UIDCheckDigit = null;
        //            item.UIDToCardNumber = null;


        //            if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
        //            {
        //                if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask, this.CardNumPattern))
        //                {
        //                    return false;
        //                }
        //            }


        //            if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
        //            {
        //                if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, item.CardTypeID))
        //                {
        //                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
        //                    return false;
        //                }
        //            }

        //            if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
        //            if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowWarning(ex.Message);
        //        return false;
        //    }

         
        //    return true;
        //}

        //private void checkIsImportCardNumber(bool clearText)
        //{
        //    if (IsImportUIDNumber.SelectedValue == "1")
        //    {
        //        Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);

        //        this.ManualRoles.Hidden = true;
        //        this.ImportRoles.Hidden = false;
        //    }
        //    else
        //    {
        //        Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        //        this.ManualRoles.Hidden = false;
        //        this.ImportRoles.Hidden = true;
        //    }

        //    if (clearText)
        //    {
        //        this.CardNumPattern.Text = "";
        //        this.CardNumMask.Text = "";
        //        this.CardNumPatternImport.Text = "";
        //        this.CardNumMaskImport.Text = "";
        //    }
        //}
 
        //protected void CardCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        //    CardNumberToUID.SelectedIndex = 0;
        //}

        //protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
        //    UIDToCardNumber.SelectedIndex = 0;
        //}

       
 

        //protected void CardTypeCode_TextChanged(object sender, EventArgs e)
        //{
        //    this.CardTypeCode.Text = this.CardTypeCode.Text.ToUpper();
        //}

        //protected void CardNumMask_TextChanged(object sender, EventArgs e)
        //{
        //    this.CardNumMask.Text = this.CardNumMask.Text.ToUpper();
        //}

        //protected void CardNumPattern_TextChanged(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(this.CardNumMask.Text) && string.IsNullOrEmpty(this.CardNumPattern.Text)) return;

        //    Tools.CheckTool.IsMatchNumMask(CardNumMask, CardNumPattern);
           
        //}

        //protected void CardNumMaskImport_TextChanged(object sender, EventArgs e)
        //{
        //    this.CardNumMaskImport.Text = this.CardNumMaskImport.Text.ToUpper();
        //}

        //protected void CardNumPatternImport_TextChanged(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(this.CardNumMaskImport.Text) && string.IsNullOrEmpty(this.CardNumPatternImport.Text)) return;

        //    Tools.CheckTool.IsMatchNumMask(CardNumMaskImport, CardNumPatternImport);
     
        //}

        //protected void UIDToCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
        //    this.CardNumMaskImport.Text = "";
        //    this.CardNumPatternImport.Text = "";
        //}

        //protected void CardNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        //}
 
    }
}
