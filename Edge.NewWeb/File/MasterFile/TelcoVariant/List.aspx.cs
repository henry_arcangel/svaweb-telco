﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.TelcoVariant;

namespace Edge.Web.File.MasterFile.TelcoVariant
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
                Tools.ControlTool.BindCardTypeCode(CardType);
                Tools.ControlTool.BindCardGradeCode(CardGrade, Convert.ToString(CardType.SelectedValue.ToString()));
                RptBind("UPC>0", "SKUDESC");


               // Tools.ControlTool.BindCardType(CardTypeID);
                //Tools.ControlTool.BindCardGrade(CardTypeID, 0);
            }
        }

        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGradeCode(CardGrade, Convert.ToString(CardType.SelectedValue.ToString()));
        }

        #region Event
        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string upc = this.SKU.Text.Trim();
                    string skudesc = this.Desc.Text.Trim();
                    string gm = this.GrossMargin.Text.Trim();
                    string cardgrade = this.CardGrade.SelectedValue.ToString();
                    string cardtype = this.CardType.SelectedValue.ToString();
                    string prodcode = this.Prod_Code.Text.Trim();
                 
                    if (!string.IsNullOrEmpty(upc))
                    {
                        if (sb.Length > 0)  
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" upc like '%");
                        sb.Append(upc);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(skudesc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "SKUdesc";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(skudesc);
                        sb.Append("%'");
                    }

                    if (!string.IsNullOrEmpty(gm))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "gmvalue";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(gm);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(cardgrade))
                    {

                        if ( cardgrade != "-1") {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "cardgradecode";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(cardgrade);
                        sb.Append("%'"); }
                    }
                    if (!string.IsNullOrEmpty(cardtype))
                    {
                        if (cardtype != "-1")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            string descLan = "cardtypecode";
                            sb.Append(descLan);
                            sb.Append(" like '%");
                            sb.Append(cardtype);
                            sb.Append("%'");
                        }
                    }
                    if (!string.IsNullOrEmpty(prodcode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "prodcode";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(prodcode);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.Customer bll = new Edge.SVA.BLL.Customer();

                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);
                //if (this.Grid1.RecordCount > 0)
                //{
                //    this.btnDelete.Enabled = true;
                //}
                //else
                //{
                //    this.btnDelete.Enabled = false;
                //}

                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

         
              
                TelcoVariantController controller = new TelcoVariantController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
               

                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("TelcoVariant", "Load Failed", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            TelcoVariantController controller = new TelcoVariantController();
            int count = 0;
          //  string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
         //   this.SortField.Text = sortFieldStr;
            string sortFieldStr = sortField;
            this.SortField.Text = sortFieldStr;
            int order = 0;

            if (sortDirection != "ASC") {
                order = 0;
            }
            else
            {
                order = 1;
            }
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text, order);
            this.Grid1.RecordCount = count;
            DataTable table = ds.Tables[0];
            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
            //  BindGridWithSort(e.SortField, e.SortDirection);
        }
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("UPC > 0", "skudesc");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("UPC > 0", "skudesc");
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("upc>0", "upc");
        }
        protected void Prod_Code_SelectedIndexChanged(object sender, EventArgs e)
        {
            //InitStoreByBrand();
        }
    }
}
