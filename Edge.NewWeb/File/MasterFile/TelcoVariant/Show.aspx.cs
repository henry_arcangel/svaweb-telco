﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Text;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.TelcoVariant
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.ImportGM, Edge.SVA.Model.ImportGM>

    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }


                Tools.ControlTool.BindCardTypeCode(CardTypeCode);
                Tools.ControlTool.BindCardGradeCode(CardGradeCode);

                RegisterCloseEvent(btnClose);
            }
        }
        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGradeCode(CardGradeCode, Convert.ToString(CardTypeCode.SelectedValue.ToString()));
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.SVA.Model.ImportGM GM = new Edge.SVA.BLL.ImportGM().GetModelSKU(Convert.ToInt32(Model.UPC));
            }
      
        
            }
    }
}
