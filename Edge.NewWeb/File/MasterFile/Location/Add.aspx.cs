﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Location
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreGroup, Edge.SVA.Model.StoreGroup>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Add ");
            Edge.SVA.Model.StoreGroup item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Add<Edge.SVA.BLL.StoreGroup>(item) > 0)
            {
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }

        }
    }
}
