﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Store.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="StoreCode" runat="server" Label="店铺编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true" ToolTipTitle="店铺编号" ToolTip="Translate__Special_121_Start1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
            </ext:TextBox>
            <ext:TextBox ID="StoreName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="描述" ToolTip="不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="StoreName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                ToolTip="對描述的一個補充。不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="StoreName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                ToolTip="對描述的另一個補充。不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="lblCardIssuer" runat="server" Label="发行商：" MaxLength="512" Required="true"
                ShowRedStar="true" Enabled="false">
            </ext:TextBox>
            <ext:DropDownList ID="BrandID" runat="server" Label="店铺所属品牌列表：" Required="true" ShowRedStar="true"
                OnSelectedIndexChanged="BrandID_SelectedIndexChanged" Resizable="true" CompareType="String"
                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:DropDownList ID="StoreTypeID" runat="server" Label="店铺种类：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="StoreGroupID" runat="server" Label="店铺分组：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="CompanyID" runat="server" Label="公司：" Resizable="true" Required="true" ShowRedStar="true"
            CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:RadioButtonList ID="PickupStoreFlag" runat="server" Label="是否允许提货：" Width="200">
                <ext:RadioItem Text="是" Value="1" />
                <ext:RadioItem Text="否" Value="0" Selected="true"/>
            </ext:RadioButtonList>
<%--            <ext:TextBox ID="StoreCountry" runat="server" Label="店铺所在国家：" MaxLength="512" ToolTipTitle="店铺所在国家"
                ToolTip="不能超過512個字符">
            </ext:TextBox>--%>
            <ext:DropDownList ID="StoreCountry" runat="server" Label="店铺所在国家：" MaxLength="512" ToolTipTitle="店铺所在国家"
                ToolTip="不能超過512個字符" OnSelectedIndexChanged="StoreCountry_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
            </ext:DropDownList>
<%--            <ext:TextBox ID="StoreProvince" runat="server" Label="店鋪所在的省份：" MaxLength="512" ToolTipTitle="店鋪所在的省份"
                ToolTip="不能超過512個字符">
            </ext:TextBox>--%>
            <ext:DropDownList ID="StoreProvince" runat="server" Label="店鋪所在的省份：" MaxLength="512" ToolTipTitle="店鋪所在的省份"
                ToolTip="不能超過512個字符" OnSelectedIndexChanged="StoreProvince_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
            </ext:DropDownList>
<%--            <ext:TextBox ID="StoreCity" runat="server" Label="店鋪所在的城市：" MaxLength="512" ToolTipTitle="店鋪所在的城市"
                ToolTip="不能超過512個字符">
            </ext:TextBox>--%>
            <ext:DropDownList ID="StoreCity" runat="server" Label="店鋪所在的城市：" MaxLength="512" ToolTipTitle="店鋪所在的城市"
                ToolTip="不能超過512個字符" OnSelectedIndexChanged="StoreCity_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
            </ext:DropDownList>
            <ext:DropDownList ID="StoreDistrict" runat="server" Label="店鋪所在的县（区）：" MaxLength="512" ToolTipTitle="店鋪所在的县（区）"
                ToolTip="不能超過512個字符" OnSelectedIndexChanged="StoreDistrict_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
            </ext:DropDownList>
            <ext:TextBox ID="StoreAddressDetail" runat="server" Label="店铺地址：" MaxLength="512"
                ToolTipTitle="店铺地址" ToolTip="不能超過512個字符" OnTextChanged="StoreAddressDetail_OnTextChanged" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="StoreAddressDetail2" runat="server" Label="店铺其他地址1：" MaxLength="512" ToolTipTitle="店铺其他地址1"></ext:TextBox>
            <ext:TextBox ID="StoreAddressDetail3" runat="server" Label="店铺其他地址2：" MaxLength="512" ToolTipTitle="店铺其他地址2"></ext:TextBox>
            <ext:TextBox ID="StoreFullDetail" runat="server" Label="店铺完整地址：" MaxLength="512"
                ToolTipTitle="店铺完整地址" ToolTip="不能超過512個字符" Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="StoreLongitude" runat="server" Label="店铺经度坐标：" MaxLength="512" ToolTipTitle="店铺经度坐标"
                ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="StoreLatitude" runat="server" Label="店铺纬度坐标：" MaxLength="512" ToolTipTitle="店铺纬度坐标"
                ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="StoreTel" runat="server" Label="电话号码：" MaxLength="512" ToolTipTitle="电话号码"
                ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="StoreFax" runat="server" Label="传真号码：" MaxLength="512" ToolTipTitle="传真号码"
                ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="Email" runat="server" Label="电子邮箱：" MaxLength="512"></ext:TextBox>
            <ext:TextBox ID="Contact" runat="server" Label="联系人：" MaxLength="512" ToolTipTitle="联系人"
                ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:DropDownList ID="Status" runat="server" Label="店铺状态：" Resizable="true">
                <ext:ListItem Text="营业中" Value="1" />
                <ext:ListItem Text="未营业" Value="2" />
            </ext:DropDownList>
<%--            <ext:TimePicker ID="StoreOpenTime" runat="server" Label="营业开始时间：" Text="" Increment="120">
            </ext:TimePicker>
            <ext:TimePicker ID="StoreCloseTime" runat="server" Label="营业结束时间：" Text="" Increment="120"
                CompareControl="StoreOpenTime" CompareOperator="GreaterThanEqual" CompareMessage="营业结束时间应该大于等于营业开始时间">
            </ext:TimePicker>--%>
            <ext:TextBox ID="StoreOpenTime" runat="server" Label="营业开始时间"></ext:TextBox>
            <ext:TextBox ID="StoreNote" runat="server" Label="备注：" MaxLength="512"></ext:TextBox>
            <ext:HiddenField ID="uploadFilePath" runat="server"></ext:HiddenField>
            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                        <Items>
                            <ext:FileUpload ID="StorePicFile" runat="server" Label="店铺图片：" ToolTipTitle="店铺图片"
                                ToolTip="Translate__Special_121_Start点击按钮进行上传，上传的文件支持JPG，GIF和PNG，BMP文件大小不能超过10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true"></ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Window ID="WindowPic" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
                CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
                Target="Top" IsModal="True" Width="750px" Height="450px"> 
            </ext:Window>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
