﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Organization;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store, Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreController controller;
        public string strWhere = "1 = 1 ";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (SVASessionInfo.CurrentUser.UserName.Equals(ConstParam.SystemAdminName))
                {
                    ControlTool.BindBrandOfMasterStore(BrandID);
                }
                else
                {
                    ControlTool.BindBrand(BrandID);
                }

                //地址
                ControlTool.BindCountry(StoreCountry);

                Edge.Web.Tools.ControlTool.BindStoreType(StoreTypeID);
                Edge.Web.Tools.ControlTool.BindStoreGroup(StoreGroupID);
                ControlTool.BindCompanyID(CompanyID); //Add By Robin 2015-02-28
                
                RegisterCloseEvent(btnClose);
                SVASessionInfo.StoreController = null;
                ViewState["sotrID"] = Request["id"];
                string sotrID = "";
                if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                    sotrID = ViewState["sotrID"].ToString();

                btnNew.OnClientClick = Window1.GetShowReference(string.Format("StoreAttribute/Add.aspx?RuleType=2&PageFlag=0&StoreID={0}", sotrID), "新增");

                BindingGrid_StoreAttributeList();
                //BindingDataGrid_GetCouponGradeList();
            }
            controller = SVASessionInfo.StoreController;
        }
        // Add by Alex 2014-07-01 ++
        #region CardGradeHoldCouponType

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in this.Grid_StoreAttributeList.SelectedRowIndexArray)
            {
                sb.Append(Grid_StoreAttributeList.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString());
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("StoreAttribute/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));

        }

        private void BindingGrid_StoreAttributeList()
        {
            ViewState["sotrID"] = Request["id"];
            string sotrID = "";
            if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                sotrID = ViewState["sotrID"].ToString();


            StringBuilder sb = new StringBuilder("");
            //   string code = controller.ViewModel.MainTable.StoreCode;

            //if (!string.IsNullOrEmpty(sotrID))
            //{
            //    string  StoreAttributeID = "";
            //    sb.Append(" StoreAttributeID in (select StoreAttributeID from StoreAttributeList where StoreID in ( ");

            //    sb.Append(sotrID);
            //    sb.Append("))");
            //}

            //strWhere = sb.ToString();

            //ViewState["strWhere"] = strWhere;


            //StoreAttributeListController AttributeController = new StoreAttributeListController();

            //int count = 0;
            //DataSet ds = AttributeController.GetTransactionList(strWhere, this.Grid_StoreAttributeList.PageSize, this.Grid_StoreAttributeList.PageIndex, out count, "");
            //this.Grid_StoreAttributeList.RecordCount = count;
            //if (ds != null)
            //{
            //    this.Grid_StoreAttributeList.DataSource = ds.Tables[0].DefaultView;
            //    this.Grid_StoreAttributeList.DataBind();
            //}
            //else
            //{
            //    this.Grid_StoreAttributeList.Reset();
            //}


            Edge.SVA.BLL.Store store = new SVA.BLL.Store();
            DataSet ds = store.GetStoreAttributeList(sotrID);

            if (ds != null)
            {
                this.Grid_StoreAttributeList.DataSource = ds.Tables[0].DefaultView;
                this.Grid_StoreAttributeList.DataBind();
            }
            else
            {
                this.Grid_StoreAttributeList.Reset();
            }



        }
        protected void Grid_StoreAttribute_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_StoreAttributeList.PageIndex = e.NewPageIndex;
            BindingGrid_StoreAttributeList();

        }



        private void BindingDataGrid_GetCouponGradeList()
        {
            //this.Grid_GetCouponGradeList.RecordCount = controller.ViewModel.GetCouponGradeList.Count;
            //this.Grid_GetCouponGradeList.DataSource = controller.ViewModel.GetCouponGradeList;
            //this.Grid_GetCouponGradeList.DataBind();
        }
        protected void Grid_GetCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //this.Grid_GetCouponGradeList.PageIndex = e.NewPageIndex;
        }
        #endregion
        // Add by Alex 2014-07-01 --
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                }
                else
                {
                    this.lblCardIssuer.Text = "";
                }
                //if (Model != null)
                //{
                //    StoreCountry.SelectedValue = Model.StoreCountry == null ? "-1" : Model.StoreCountry.ToString().Trim();
                //    InitProvinceByCountry();
                //    StoreProvince.SelectedValue = Model.StoreProvince == null ? "-1" : Model.StoreProvince.ToString().Trim();
                //    InitCityByProvince();
                //    StoreCity.SelectedValue = Model.StoreCity == null ? "-1" : Model.StoreCity.ToString().Trim();
                //    InitDistrictByCity();
                //    StoreDistrict.SelectedValue = Model.StoreDistrict == null ? "-1" : Model.StoreDistrict.ToString().Trim();

                //    AppendFullAddress();


                //    //存在图片时不需要验证此字段
                //    if (!string.IsNullOrEmpty(Model.StorePicFile))
                //    {
                //        this.FormLoad.Hidden = true;
                //        this.FormReLoad.Hidden = false;
                //        this.btnBack.Hidden = false;
                //    }
                //    else
                //    {
                //        this.FormLoad.Hidden = false;
                //        this.FormReLoad.Hidden = true;
                //        this.btnBack.Hidden = true;
                //    }

                //    this.uploadFilePath.Text = Model.StorePicFile;

                //    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                //}
                controller.LoadViewModel(Model.StoreID);
                if (controller.ViewModel.MainTable != null)
                {
                    StoreCountry.SelectedValue = controller.ViewModel.MainTable.StoreCountry == null ? "-1" : controller.ViewModel.MainTable.StoreCountry.ToString().Trim();
                    InitProvinceByCountry();
                    StoreProvince.SelectedValue = controller.ViewModel.MainTable.StoreProvince == null ? "-1" : controller.ViewModel.MainTable.StoreProvince.ToString().Trim();
                    InitCityByProvince();
                    StoreCity.SelectedValue = controller.ViewModel.MainTable.StoreCity == null ? "-1" : controller.ViewModel.MainTable.StoreCity.ToString().Trim();
                    InitDistrictByCity();
                    StoreDistrict.SelectedValue = controller.ViewModel.MainTable.StoreDistrict == null ? "-1" : controller.ViewModel.MainTable.StoreDistrict.ToString().Trim();

                    AppendFullAddress();


                    //存在图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.StorePicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.StorePicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            //Edge.SVA.Model.Store item = this.GetUpdateObject();

            //if (item != null)
            //{
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = DateTime.Now;
            //    item.StoreCode = item.StoreCode.ToUpper();
            //    //item.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
            //    if (!string.IsNullOrEmpty(this.picSFile.ShortFileName) && this.FormLoad.Hidden == false)
            //    {
            //        item.StorePicFile = this.picSFile.SaveToServer("Images/Store");
            //    }
            //    else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
            //    {
            //        item.StorePicFile = this.uploadFilePath.Text;
            //    }
            //}
            //if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(item.StoreCode, item.BrandID.GetValueOrDefault(), item.StoreID))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreCodeInBrand);
            //    return;
            //}
            //if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Store>(item))
            //{
            //    if (this.FormReLoad.Hidden == true)
            //    {
            //        DeleteFile(this.uploadFilePath.Text);
            //    }
            //    CloseAndPostBack();
            //}
            //else
            //{
            //    ShowUpdateFailed();
            //}

            controller.ViewModel.MainTable = this.GetUpdateObject();

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.StoreCode = controller.ViewModel.MainTable.StoreCode.ToUpper();
                if (!string.IsNullOrEmpty(this.StorePicFile.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateImg(this.StorePicFile.FileName))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateImg(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.StorePicFile = this.uploadFilePath.Text;
                }
            }
            string message = controller.ValidataObject(this.StoreCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()), controller.ViewModel.MainTable.StoreID);
            if (!string.IsNullOrEmpty(message))
            {
                ShowWarning(message);
                return;
            }
            if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(controller.ViewModel.MainTable.StoreCode, controller.ViewModel.MainTable.BrandID.GetValueOrDefault(), controller.ViewModel.MainTable.StoreID))
            {
                ShowWarning(Resources.MessageTips.ExistStoreCodeInBrand);
                return;
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable.StoreCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.StoreCode);
                ShowAddFailed();
            }
        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.StorePicFile.ShortFileName))
            {
                this.PicturePath.Text = this.StorePicFile.SaveToServer("Images/Store");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);

            BindingGrid_StoreAttributeList();//Add by Nathan 20140703
        }

        protected void StoreCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreCountry.SelectedValue != "-1")
            {
                ControlTool.BindProvince(this.StoreProvince, this.StoreCountry.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindProvince(StoreProvince, "-1");
            }
            StoreProvince_SelectedIndexChanged(null, null);
            StoreCity_SelectedIndexChanged(null, null);
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreProvince.SelectedValue != "-1")
            {
                ControlTool.BindCity(this.StoreCity, this.StoreProvince.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindCity(StoreCity, "-1");
            }
            StoreCity_SelectedIndexChanged(null, null);
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreCity.SelectedValue != "-1")
            {
                ControlTool.BindDistrict(this.StoreDistrict, this.StoreCity.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindDistrict(StoreDistrict, "-1");
            }
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void StoreAddressDetail_OnTextChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AppendFullAddress()
        {
            string country = this.StoreCountry.SelectedValue == "-1" ? "" : this.StoreCountry.SelectedText;
            string province = this.StoreProvince.SelectedValue == "-1" ? "" : this.StoreProvince.SelectedText;
            string city = this.StoreCity.SelectedValue == "-1" ? "" : this.StoreCity.SelectedText;
            string district = this.StoreDistrict.SelectedValue == "-1" ? "" : this.StoreDistrict.SelectedText;
            string detail = this.StoreAddressDetail.Text;
            StoreFullDetail.Text = country + province + city + district + detail;
        }


        private void InitProvinceByCountry()
        {
            if (this.StoreCountry.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindProvince(StoreProvince, this.StoreCountry.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindProvince(StoreProvince, " -1");
        }

        private void InitCityByProvince()
        {
            if (this.StoreProvince.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindCity(StoreCity, this.StoreProvince.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindCity(StoreCity, " -1");
        }

        private void InitDistrictByCity()
        {
            if (this.StoreCity.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindDistrict(StoreDistrict, this.StoreCity.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindDistrict(StoreDistrict, " -1");
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
