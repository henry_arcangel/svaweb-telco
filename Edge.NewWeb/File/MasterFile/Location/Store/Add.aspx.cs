﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using Edge.SVA.BLL.Domain.DataResources;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using System.Linq;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store, Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                ControlTool.BindStoreType(this.StoreTypeID);
                ControlTool.BindCompanyID(CompanyID); //Add By Robin 2015-02-28
                Edge.Web.Tools.ControlTool.BindStoreGroup(StoreGroupID);
                if (SVASessionInfo.CurrentUser.UserName.Equals(ConstParam.SystemAdminName))
                {
                    ControlTool.BindBrandOfMasterStore(BrandID);
                }
                else
                {
                    ControlTool.BindBrand(BrandID);
                }

                //地址
                ControlTool.BindCountry(StoreCountry);
                this.StoreProvince.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                this.StoreCity.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                this.StoreDistrict.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });

                this.lblCardIssuer.Text = DALTool.GetCardIssuerName();
                SVASessionInfo.StoreController = null;
            }
            controller = SVASessionInfo.StoreController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            //if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(this.StoreCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()), 0))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreCodeInBrand);
            //    return;
            //}

            //Edge.SVA.Model.Store item = this.GetAddObject();
            //if (item != null)
            //{
            //    item.CreatedBy = DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.StoreCode = item.StoreCode.ToUpper();
            //    item.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
            //}

            //if (DALTool.Add<Edge.SVA.BLL.Store>(item) > 0)
            //{
            //    CloseAndRefresh();
            //}
            //else
            //{
            //    ShowAddFailed();
            //}

            string message = controller.ValidataObject(this.StoreCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()), 0);
            if (!string.IsNullOrEmpty(message))
            {
                ShowWarning(message);
                return;
            }
            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                if (!ValidateImg(this.StorePicFile.FileName))
                {
                    return;
                }

                controller.ViewModel.MainTable.CreatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.StoreCode = controller.ViewModel.MainTable.StoreCode.ToUpper();
                controller.ViewModel.MainTable.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable.StoreCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.StoreCode);
                ShowAddFailed();
            }
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ControlTool.AddTitle(this.BrandID);

            Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()));
            if (brand != null)
            {
                Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
            }
            else
            {
                this.lblCardIssuer.Text = "";
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.StorePicFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.StorePicFile.SaveToServer("Images/Store");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }

        protected void StoreCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreCountry.SelectedValue != "-1")
            {
                ControlTool.BindProvince(this.StoreProvince, this.StoreCountry.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindProvince(StoreProvince,"-1");
            }
            StoreProvince_SelectedIndexChanged(null, null);
            StoreCity_SelectedIndexChanged(null, null);
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreProvince.SelectedValue != "-1")
            {
                ControlTool.BindCity(this.StoreCity, this.StoreProvince.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindCity(StoreCity, "-1");
            }
            StoreCity_SelectedIndexChanged(null, null);
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreCity.SelectedValue != "-1")
            {
                ControlTool.BindDistrict(this.StoreDistrict, this.StoreCity.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindDistrict(StoreDistrict, "-1");
            }
            StoreDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void StoreDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void StoreAddressDetail_OnTextChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AppendFullAddress()
        {
            string country = this.StoreCountry.SelectedValue == "-1" ? "" : this.StoreCountry.SelectedText;
            string province = this.StoreProvince.SelectedValue == "-1" ? "" : this.StoreProvince.SelectedText;
            string city = this.StoreCity.SelectedValue == "-1" ? "" : this.StoreCity.SelectedText;
            string district = this.StoreDistrict.SelectedValue == "-1" ? "" : this.StoreDistrict.SelectedText;
            string detail = this.StoreAddressDetail.Text;
            StoreFullDetail.Text = country + province + city + district + detail;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
