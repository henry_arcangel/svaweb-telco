﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.MasterFile.Location.Store.Attribute
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store_Attribute, Edge.SVA.Model.Store_Attribute>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);


                //Edge.Web.Tools.ControlTool.BindStoreCode(StoreCode);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasStoreAttributeCode(this.SACode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistStoreAttributeCode);
                this.SACode.Focus();
                return;
            }

            //if (!Edge.Web.Tools.DALTool.isHasStoreAttributeCodeWithStore(this.StoreAttributeCode.Text.Trim(), 0,this.StoreCode.Text.Trim()))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreAttributeCodeWith);
            //    this.StoreAttributeCode.Focus();
            //    return;
            //}

            Edge.SVA.Model.Store_Attribute item = this.GetAddObject();

            if (item != null)
            {
                if (!ValidateImg(this.SAPic.FileName))
                {
                    return;
                }
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.StoreID = item.StoreID;
                //item.StoreCode = item.StoreCode.ToUpper();
                item.SACode = item.SACode.ToUpper();
                item.SADesc1 = item.SADesc1;
                item.SADesc2 = item.SADesc2;
                item.SADesc3 = item.SADesc3;
                item.SAPic = this.SAPic.SaveToServer("Images/StoreAttribute");
            }

            if (DALTool.Add<Edge.SVA.BLL.Store_Attribute>(item) > 0)
            {
               CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.SAPic.ShortFileName))
            {
                this.uploadFilePath.Text = this.SAPic.SaveToServer("Images/StoreAttribute");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}