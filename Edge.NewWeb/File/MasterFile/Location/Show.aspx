﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
</head>
<body style="padding:10px;">
    <form id="form1" runat="server" enableviewstate="false">
   <div class="navigation">
      <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom:10px;"></div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable" >
                   <tr>
					    <th colspan="2" align="left"><%=this.PageName %></th>
                    </tr>
				    <tr>
					    <td width="25%" align="right">描述：</td>
					    <td width="75%"><asp:Label id="StoreGroupName1" tabIndex="1" runat="server" Width="249px" ></asp:Label></td>
				    </tr>
				    <tr>
				        <td align="right">其他描述1：</td>
					    <td><asp:Label id="StoreGroupName2" tabIndex="2" runat="server" Width="249px" ></asp:Label></td>
				    </tr>
				    <tr>
				        <td align="right">其他描述2：</td>
					    <td><asp:Label id="StoreGroupName3" tabIndex="4" runat="server" Width="249px"></asp:Label></td>
				    </tr>
				     <tr>
				        <td align="right">创建时间：</td>
					    <td><asp:Label ID="CreatedOn" runat = "server" Width="249px"></asp:Label></td>
				    </tr>
				    <tr>
				        <td align="right">创建人：</td>
					    <td><asp:Label ID="CreatedBy" runat = "server" Width="249px"></asp:Label></td>
				    </tr>
				     <tr>
				        <td align="right">上次修改时间：</td>
					    <td><asp:Label ID="UpdatedOn" runat = "server" Width="249px"></asp:Label></td>
				    </tr>
				     <tr>
				        <td align="right">上次修改人：</td>
					    <td><asp:Label ID="UpdatedBy" runat = "server" Width="249px"></asp:Label></td>
				    </tr>
				    <tr>
					    <td colSpan="6" align="center"><asp:label id="lblMsg" runat="server" ForeColor="Red"></asp:label></td>
				    </tr>
				    <tr>
		               <td colSpan="6" align="center">
			              <div align="center">
				            <input type="button" name="button1"  value="返 回" onclick= "location.href= 'List.aspx' " class="submit"/>
				          </div>
		              </td>
	                </tr>
			 </table>
			<div style="margin-top:10px;text-align:center;"></div>	
            <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
