﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;

using System.Data;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;

namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Member
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Promotion_Member, PromotionMemberViewModel>
    {
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        protected void Page_Load(object sender, EventArgs e)
        {
            controller = SVASessionInfo.BuyingNewPromotionController;
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(this.btnClose);

                InitData();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            PromotionMemberViewModel item = this.GetAddObject();
            if (item != null)
            {
                item.OprFlag = "Add";
                item.LoyaltyTypeName = this.LoyaltyType.SelectedText;
                item.LoyaltyValueName = this.LoyaltyValue.SelectedText;
                item.LoyaltyBirthdayFlagName = this.LoyaltyBirthdayFlag.SelectedText;
                item.LoyaltyPromoScopeName = this.LoyaltyPromoScope.SelectedText;
                if (!controller.ValidateMemberIsExists(item))
                {
                    controller.AddPromotionMemberViewModel(SVASessionInfo.SiteLanguage, item);
                    CloseAndPostBack();
                }
                else
                {
                    ShowError("Data Duplicated");
                }
            }
            else
            {
                ShowError(Resources.MessageTips.UnKnownSystemError);
            }
        }

        protected void InitData()
        {
            this.PromotionCode.Text = Request.Params["PromotionCode"].ToString();
            //controller.BindCardType(this.LoyaltyType);
            controller.BindLoyaltyType(this.LoyaltyType,false);
            controller.BindLoyaltyBirthdayFlag(this.LoyaltyBirthdayFlag, false);
            controller.BindLoyaltyPromoScope(this.LoyaltyPromoScope, false);
        }

        protected void LoyaltyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.LoyaltyType.SelectedValue != "-1")
            {
                controller.BindLoyaltyValue(this.LoyaltyValue, this.LoyaltyType.SelectedValue);
            }
        }
    }
}