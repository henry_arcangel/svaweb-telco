﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;

using System.Data;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;

namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Gift
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Promotion_Gift, PromotionGiftViewModel>
    {
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        protected void Page_Load(object sender, EventArgs e)
        {
            controller = SVASessionInfo.BuyingNewPromotionController;
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(this.btnClose);

                InitData();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            PromotionGiftViewModel item = this.GetAddObject();
            if (item != null)
            {
                if (item.PromotionGiftType == 3)
                {
                    int val=int.Parse(this.PromotionValue_1.SelectedValue);
                    //if (val<0)
                    //{
                    //    this.PromotionValue_1.MarkInvalid("Please select!");
                    //    return;
                    //}
                    item.PromotionValue = val;
                    item.PromotionValueName = this.PromotionValue_1.SelectedText;
                }
                else
                {
                    if (!item.PromotionValue.HasValue)
                    {
                        this.PromotionValue.MarkInvalid("can't be null!");
                        return;
                    }
                    item.PromotionValueName = item.PromotionValue.Value.ToString("N2");
                }
                item.OprFlag = "Add";
                //item.LogicalOprName = DALTool.GetLogicalOprName(this.GiftLogicalOpr.SelectedValue);
                //item.GiftTypeName = this.PromotionGiftType.SelectedText;
                //item.GiftItemName = this.PromotionGiftItem.SelectedText;
                //item.DiscountTypeName = this.PromotionDiscountType.SelectedText;
                //item.DiscountOnName = this.PromotionDiscountOn.SelectedText;
                //item.GiftPluTable = controller.ViewModel.GiftPluTable;
                item.PromotionGiftTypeName = this.PromotionGiftType.SelectedText;
                if (!controller.ValidateGiftIsExists(item))
                {
                    controller.AddPromotionGiftViewModel(SVASessionInfo.SiteLanguage, item);
                    CloseAndPostBack();
                }
                else
                {
                    ShowError("Data Duplicated");
                }
            }
            else
            {
                ShowError(Resources.MessageTips.UnKnownSystemError);
            }
        }

        protected void PromotionGiftType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePromotionGiftType();
        }

        private void ChangePromotionGiftType()
        {
            if (this.PromotionGiftType.SelectedValue == "3")
            {
                this.PromotionValue.Hidden = true;
                this.PromotionValue_1.Hidden = false;
                Edge.Web.Tools.ControlTool.BindCouponType(this.PromotionValue_1, "IsImportCouponNumber = 0  order by CouponTypeCode");
                this.PromotionAdjValue.Enabled = false;
            }
            else
            {
                this.PromotionValue.Hidden = false;
                this.PromotionValue_1.Hidden = true;
                this.PromotionAdjValue.Enabled = true;
            }
        }
        protected void InitData()
        {
            this.PromotionCode.Text = Request.Params["PromotionCode"].ToString();
            controller.BindPromotionGiftType(this.PromotionGiftType, true);

        }

        #region 操作PLU表
        protected void btnGiftPLUAdd_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Promotion_Gift_PLU/Add.aspx?PromotionCode={0}&GiftSeq={1}", this.PromotionCode.Text, this.GiftSeq.Text)));
        }
        protected void btnClearAllGiftPLUItem_Click(object sender, EventArgs e)
        {
            //ClearGird(this.Grid1);
            controller.ViewModel.GiftPluTable = null;
        }
        protected void btnDeleteGiftPLUItem_Click(object sender, EventArgs e)
        {
            //if (controller.ViewModel.GiftPluTable != null)
            //{
            //    DataTable addDT = controller.ViewModel.GiftPluTable;

            //    foreach (int row in Grid1.SelectedRowIndexArray)
            //    {
            //        string EntityType = Grid1.DataKeys[row][0].ToString();
            //        string EntityNum = Grid1.DataKeys[row][1].ToString();
            //        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
            //        {
            //            if (addDT.Rows[j]["EntityType"].ToString().Trim() == EntityType && addDT.Rows[j]["EntityNum"].ToString().Trim() == EntityNum)
            //            {
            //                addDT.Rows.Remove(addDT.Rows[j]);
            //            }
            //        }
            //        addDT.AcceptChanges();
            //    }
            //    controller.ViewModel.GiftPluTable = addDT;
            //    BindGiftPLUList(controller.ViewModel.GiftPluTable);
            //}
        }
        private void BindGiftPLUList(DataTable dt)
        {
            //if (dt != null)
            //{

            //    this.Grid1.PageSize = webset.ContentPageNum;
            //    this.Grid1.RecordCount = dt.Rows.Count;

            //    DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
            //    this.Grid1.DataSource = viewDT;
            //    this.Grid1.DataBind();

            //}
            //else
            //{
            //    this.Grid1.PageSize = webset.ContentPageNum;
            //    this.Grid1.PageIndex = 0;
            //    this.Grid1.RecordCount = 0;
            //    this.Grid1.DataSource = null;
            //    this.Grid1.DataBind();
            //}

            //this.btnDeleteGiftPLUItem.Enabled = btnClearAllGiftPLUItem.Enabled = Grid1.RecordCount > 0 ? true : false;
        }
        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //Grid1.PageIndex = e.NewPageIndex;

            //BindGiftPLUList(controller.ViewModel.GiftPluTable);
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindGiftPLUList(controller.ViewModel.GiftPluTable);
        }
        protected void PromotionGiftItem_SelectedChanged(object sender, EventArgs e)
        {
            //string item = this.PromotionGiftItem.SelectedValue;
            //switch (item)
            //{
            //    case "0":
            //        btnGiftPLUAdd.Enabled = btnDeleteGiftPLUItem.Enabled = btnClearAllGiftPLUItem.Enabled = false;
            //        this.Grid1.Enabled = false;
            //        ClearGird(Grid1);
            //        break;
            //    default:
            //        btnGiftPLUAdd.Enabled = btnDeleteGiftPLUItem.Enabled = btnClearAllGiftPLUItem.Enabled = true;
            //        this.Grid1.Enabled = true;
            //        break;
            //}
        }
        #endregion
    }
}