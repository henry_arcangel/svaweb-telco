﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Gift.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="SimpleForm1" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                       OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" AutoHeight="true" AutoWidth="true" Title="基础内容">
            <Items>
                <ext:SimpleForm ID="SimpleForm2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server"  LabelAlign="Right">
                    <Items>
                        <ext:TextBox ID="PromotionCode" runat="server" Label="促销编号：" Required="true" ShowRedStar="true" Enabled="false"/>
                        <ext:NumberBox ID="GiftSeq" runat="server" Label="礼品条件序号：" ToolTip="请以整数输入礼品条件序号，不能重复" Required="true" ShowRedStar="true"/>
                        <ext:DropDownList ID="PromotionGiftType" runat="server" Label="促销实现方式：" Resizable="true" ToolTip="请选择促销实现方式"  AutoPostBack="true" OnSelectedIndexChanged="PromotionGiftType_OnSelectedIndexChanged">
                        </ext:DropDownList>
                        <ext:DropDownList ID="PromotionValue_1" runat="server" Label="促销内容值：" Resizable="true" Hidden="true">
                        </ext:DropDownList>
                        <ext:NumberBox ID="PromotionValue" runat="server" Label="促销内容值：" ToolTip="请输入促销内容的数值，小数位不超过2位" NoDecimal="false" DecimalPrecision="2" />
                        <ext:NumberBox ID="PromotionAdjValue" runat="server" Label="促销结果数值调整值：" ToolTip="请输入促销结果数值调整值的数值，小数位不超过2位" NoDecimal="false" DecimalPrecision="2"/>
                       
                    </Items>
                </ext:SimpleForm>            
            </Items>
        </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowSearch" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="550px" Height="350px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
