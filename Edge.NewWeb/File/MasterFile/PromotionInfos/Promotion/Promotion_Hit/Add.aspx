﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Hit.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="SimpleForm1" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" OnClick="btnClose_Click" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" AutoHeight="true"
                AutoWidth="true" Title="基础内容">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" ShowHeader="false" EnableBackgroundColor="true"
                        ShowBorder="false" runat="server" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="PromotionCode" runat="server" Label="促销编号：" Required="true" ShowRedStar="true"
                                Enabled="false" />
                            <ext:NumberBox ID="HitSeq" runat="server" Label="命中条件序号：" Required="true" ShowRedStar="true"
                                ToolTip="请输入命中条件序号，不能超过10个字符" />
                            <ext:DropDownList ID="HitType" runat="server" Label="命中类型：" Resizable="true" ToolTip="请选择命中类型">
                            </ext:DropDownList>
                            <ext:NumberBox ID="HitValue" runat="server" Label="命中金额/数量：" ToolTip="请输入命中金额或数量，不能超过10个字符"
                                NoDecimal="true" MaxLength="9" />
                            <ext:DropDownList ID="HitOP" runat="server" Label="命中关系操作符：" Resizable="true" ToolTip="请选择命中符号">
                            </ext:DropDownList>
                            <ext:DropDownList ID="HitItem" runat="server" Label="命中货品条件：" Resizable="true" ToolTip="请选择命中货品条件"
                                OnSelectedIndexChanged="HitItem_SelectedChanged" AutoPostBack="true">
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" AutoHeight="true"
                AutoWidth="true" Title="促销命中表的指定货品列表">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar5" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnHitPLUAdd" Icon="Add" runat="server" Text="添加" OnClick="btnHitPLUAdd_Click"
                                Enabled="false">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator5" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnDeleteHitPLUItem" Icon="Delete" runat="server" Text="删除" OnClick="btnDeleteHitPLUItem_Click"
                                Enabled="false">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearAllHitPLUItem" Icon="Delete" runat="server" Text="清空" OnClick="btnClearAllHitPLUItem_Click"
                                Enabled="false">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="EntityType,EntityNum"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" Enabled="false">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="货品命中类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("PLUHitTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="货品过滤条件">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("HitSignName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="实体类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("EntityTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="实体号码">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("EntityNum")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowSearch" Title="Search" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="550px" Height="350px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
