﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;


namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Promotion_H, Edge.SVA.Model.Promotion_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //对详情表的操作
                this.WindowSearch.Title = "搜索";
                //if (this.Grid1.RecordCount == 0)
                //{
                //    this.btnClearAllHitItem.Enabled = false;
                //}
                //else
                //{
                //    this.btnClearAllHitItem.Enabled = true;
                //}
                SVASessionInfo.BuyingNewPromotionController = null;
                InitData();

                controller = SVASessionInfo.BuyingNewPromotionController;
                if (Request.QueryString["iscopy"] != null)
                {
                    controller.IsCopy = true;
                }

            }
            else
            {
                controller = SVASessionInfo.BuyingNewPromotionController;
            }
        }
        
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.PromotionCode, SVASessionInfo.SiteLanguage);


                if (controller.ViewModel.MainTable != null)
                {
                    this.StartTime.Text = controller.ViewModel.MainTable.StartTime.HasValue ? controller.ViewModel.MainTable.StartTime.Value.ToString("HH:mm:ss") : string.Empty;
                    this.EndTime.Text = controller.ViewModel.MainTable.EndTime.HasValue ? controller.ViewModel.MainTable.EndTime.Value.ToString("HH:mm:ss") : string.Empty;

                    //Add By Robin 2014-12-11 for 根据DayFlagID获取DayFlagCode
                    
                    //controller.ViewModel.DayCode = controller.ViewModel.MainTable.DayFlagID.ToString();
                    //controller.ViewModel.WeekCode = controller.ViewModel.MainTable.WeekFlagID.ToString();
                    //controller.ViewModel.MonthCode = controller.ViewModel.MainTable.MonthFlagID.ToString();

                    this.DayFlagID.Text = DALTool.GetDayFlagCode(controller.ViewModel.MainTable.DayFlagID.ToString());
                    this.WeekFlagID.Text = DALTool.GetWeekFlagCode(controller.ViewModel.MainTable.WeekFlagID.ToString());
                    this.MonthFlagID.Text = DALTool.GetMonthFlagCode(controller.ViewModel.MainTable.MonthFlagID.ToString());
                    controller.ViewModel.DayCode = this.DayFlagID.Text;
                    controller.ViewModel.WeekCode = this.WeekFlagID.Text;
                    controller.ViewModel.MonthCode = this.MonthFlagID.Text;
                    //End;
                }


                //下拉框取值
                if (this.StoreID.SelectedValue != "-1")
                {
                    this.StoreGroupID.Enabled = false;
                }
                else
                {
                    this.StoreGroupID.Enabled = true;
                }
                if (this.StoreGroupID.SelectedValue != "-1")
                {
                    this.StoreID.Enabled = false;
                }
                else
                {
                    this.StoreID.Enabled = true;
                }
                if (controller.ViewModel.MainTable.LoyaltyFlag.Value == 0)
                {
                    this.Grid3.Enabled = false;
                    this.btnMemberAdd.Enabled = btnMemberDelete.Enabled = btnClearAllMember.Enabled = false;
                    ClearGird(this.Grid3);
                    controller.ViewModel.PromotionMemberList.Clear();
                }
                else
                {
                    this.Grid3.Enabled = true;
                    this.btnMemberAdd.Enabled = true;
                }

                //Member
                if (controller.ViewModel.PromotionMemberList != null)
                {
                    BindMemberList(controller.ViewModel.PromotionMemberList.FindAll(mm => mm.OprFlag != "Delete"));
                }
                //Hit表取值
                if (controller.ViewModel.PromotionHitList != null)
                {
                    BindHitList(controller.ViewModel.PromotionHitList.FindAll(mm => mm.OprFlag != "Delete"));
                }
                //Gift
                if (controller.ViewModel.PromotionGiftList != null)
                {
                    BindGiftList(controller.ViewModel.PromotionGiftList.FindAll(mm => mm.OprFlag != "Delete"));
                }


                if (Request.QueryString["iscopy"] != null)
                {
                    string pCode=DALTool.GetREFNOCode("PROM");
                    this.PromotionCode.Text = pCode;

                    foreach (var item in controller.ViewModel.PromotionMemberList)
                    {
                        item.PromotionCode = pCode;
                    }
                    foreach (var item in controller.ViewModel.PromotionHitList)
                    {
                        item.PromotionCode = pCode;                        
                    }
                    foreach (var item in controller.ViewModel.PromotionGiftList)
                    {
                        item.PromotionCode = pCode;
                    }
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");

            //Add By Robin 2014-12-11 for 根据FlagCode获取FlagID
            this.DayFlagID.Text = DALTool.GetDayFlagID(this.DayFlagID.Text).ToString();
            this.WeekFlagID.Text = DALTool.GetWeekFlagID(this.WeekFlagID.Text).ToString();
            this.MonthFlagID.Text = DALTool.GetMonthFlagID(this.MonthFlagID.Text).ToString();
            //End

            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (controller.ViewModel.MainTable != null)
            {
                DateTime dt = DateTime.Now;
                controller.ViewModel.MainTable.StartTime = DateTime.Parse(dt.ToString("yyyy-MM-dd ") + this.StartTime.Text);
                controller.ViewModel.MainTable.EndTime = DateTime.Parse(dt.ToString("yyyy-MM-dd ") + this.EndTime.Text);

                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = dt;

                
            }

            ExecResult er = controller.Update(); //controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Promotion Success Code:" + controller.ViewModel.MainTable.PromotionCode);
                if (controller.IsCopy)
                {
                    CloseAndRefresh();
                }
                else 
                {
                    CloseAndPostBack();
                }
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Promotion Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.PromotionCode);
                ShowSaveFailed(er.Message);
            }

        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.StoreID.SelectedValue != "-1")
            {
                this.StoreGroupID.Enabled = false;
            }
            else
            {
                this.StoreGroupID.Enabled = true;
            }
        }

        protected void StoreGroupID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.StoreGroupID.SelectedValue != "-1")
            {
                this.StoreID.Enabled = false;
            }
            else
            {
                this.StoreID.Enabled = true;
            }
        }

        protected void LoyaltyFlag_SelectedIndexChanged(object sender, EventArgs e)
        {
            //当非会员时不允许新增记录到会员子表
            if (this.LoyaltyFlag.SelectedValue == "0")
            {
                this.Grid3.Enabled = false;
                this.btnMemberAdd.Enabled = btnMemberDelete.Enabled = btnClearAllMember.Enabled = false;
                ClearGird(this.Grid3);
                controller.ViewModel.PromotionMemberList.Clear();
            }
            else
            {
                this.Grid3.Enabled = true;
                this.btnMemberAdd.Enabled = btnMemberDelete.Enabled = btnClearAllMember.Enabled = true;
            }
        }

        #region 操作命中表
        protected void btnHitAdd_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Promotion_Hit/Add.aspx?PromotionCode={0}", this.PromotionCode.Text)));
        }

        protected void btnClearAllHitItem_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid1);
            controller.ViewModel.PromotionHitList.Clear();
        }
        protected void btnDeleteHitItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.PromotionHitList != null && controller.ViewModel.PromotionHitList.Count > 0)
            {
                foreach (int row in Grid1.SelectedRowIndexArray)
                {
                    string KeyID = Grid1.DataKeys[row][0].ToString();
                    for (int j = controller.ViewModel.PromotionHitList.Count - 1; j >= 0; j--)
                    {
                        if (controller.ViewModel.PromotionHitList[j].HitSeq.ToString().Trim() == KeyID)
                        {
                            controller.ViewModel.PromotionHitList[j].OprFlag = "Delete";
                        }
                    }
                }
                BindHitList(controller.ViewModel.PromotionHitList.FindAll(mm => mm.OprFlag != "Delete"));
            }
        }

        private void BindHitList(List<PromotionHitViewModel> list)
        {
            this.Grid1.RecordCount = list.Count; //controller.ViewModel.PromotionHitList.Count;
            this.Grid1.DataSource = list;//controller.ViewModel.PromotionHitList;
            this.Grid1.DataBind();

            this.btnDeleteHitItem.Enabled = btnClearAllHitItem.Enabled = Grid1.RecordCount > 0 ? true : false;
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            BindHitList(controller.ViewModel.PromotionHitList.FindAll(mm => mm.OprFlag != "Delete"));
        }
        #endregion

        #region 操作礼品表
        protected void btnGiftAdd_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Promotion_Gift/Add.aspx?PromotionCode={0}", this.PromotionCode.Text)));
        }

        protected void btnClearAllGiftItem_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid2);
            controller.ViewModel.PromotionGiftList.Clear();
        }
        protected void btnDeleteGiftItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.PromotionGiftList != null && controller.ViewModel.PromotionGiftList.Count > 0)
            {
                foreach (int row in Grid2.SelectedRowIndexArray)
                {
                    string KeyID = Grid2.DataKeys[row][0].ToString();
                    for (int j = controller.ViewModel.PromotionGiftList.Count - 1; j >= 0; j--)
                    {
                        if (controller.ViewModel.PromotionGiftList[j].GiftSeq.ToString().Trim() == KeyID)
                        {
                            controller.ViewModel.PromotionGiftList[j].OprFlag = "Delete";
                        }
                    }
                }
                BindGiftList(controller.ViewModel.PromotionGiftList.FindAll(mm => mm.OprFlag != "Delete"));
            }
        }

        private void BindGiftList(List<PromotionGiftViewModel> list)
        {
            this.Grid2.RecordCount = list.Count; //controller.ViewModel.PromotionHitList.Count;
            this.Grid2.DataSource = list;//controller.ViewModel.PromotionHitList;
            this.Grid2.DataBind();

            this.btnDeleteGiftItem.Enabled = btnClearAllGiftItem.Enabled = Grid2.RecordCount > 0 ? true : false;
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid2.PageIndex = e.NewPageIndex;
            BindGiftList(controller.ViewModel.PromotionGiftList.FindAll(mm => mm.OprFlag != "Delete"));
        }
        #endregion

        #region 操作促销会员表
        protected void btnMemberAdd_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Promotion_Member/Add.aspx?PromotionCode={0}", this.PromotionCode.Text)));
        }

        protected void btnClearAllMemberItem_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid3);
            controller.ViewModel.PromotionMemberList.Clear();
        }
        protected void btnDeleteMemberItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.PromotionMemberList != null && controller.ViewModel.PromotionMemberList.Count > 0)
            {
                foreach (int row in Grid3.SelectedRowIndexArray)
                {
                    string KeyID = Grid3.DataKeys[row][0].ToString();
                    for (int j = controller.ViewModel.PromotionMemberList.Count - 1; j >= 0; j--)
                    {
                        if (controller.ViewModel.PromotionMemberList[j].ObjectKey.ToString().Trim() == KeyID)
                        {
                            controller.ViewModel.PromotionMemberList[j].OprFlag = "Delete";
                        }
                    }
                }
                BindMemberList(controller.ViewModel.PromotionMemberList.FindAll(mm => mm.OprFlag != "Delete"));
            }
        }

        private void BindMemberList(List<PromotionMemberViewModel> list)
        {
            this.Grid3.RecordCount = list.Count;
            this.Grid3.DataSource = list;
            this.Grid3.DataBind();

            this.btnMemberDelete.Enabled = btnClearAllMember.Enabled = Grid3.RecordCount > 0 ? true : false;
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid3.PageIndex = e.NewPageIndex;
            BindMemberList(controller.ViewModel.PromotionMemberList.FindAll(mm => mm.OprFlag != "Delete"));
        }
        #endregion

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindMemberList(controller.ViewModel.PromotionMemberList.FindAll(mm => mm.OprFlag != "Delete"));
            BindHitList(controller.ViewModel.PromotionHitList.FindAll(mm => mm.OprFlag != "Delete"));
            BindGiftList(controller.ViewModel.PromotionGiftList.FindAll(mm => mm.OprFlag != "Delete"));
        }

        protected void Window1Edit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            this.DayFlagID.Text = controller.ViewModel.DayCode;
            this.MonthFlagID.Text = controller.ViewModel.MonthCode;
            this.WeekFlagID.Text = controller.ViewModel.WeekCode;
        }

        protected void InitData()
        {
            controller.BindStore(StoreID);
            controller.BindStoreGroup(StoreGroupID);
        }

        protected void btnAddDay_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("BUY_DAYFLAG/List.aspx"));
        }

        protected void btnAddWeek_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("BUY_WEEKFLAG/List.aspx"));
        }

        protected void btnAddMonth_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("BUY_MONTHFLAG/List.aspx"));
        }

    }
}