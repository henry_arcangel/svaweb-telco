﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.NumberRulesSetting
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.REFNO,Edge.SVA.Model.REFNO>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
            }
        }  
    }
}
