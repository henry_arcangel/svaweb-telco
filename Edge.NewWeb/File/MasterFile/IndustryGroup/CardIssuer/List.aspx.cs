﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");

                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind("CardIssuerID>0", "CreatedOn");

                btnNew.Hidden = true;
                btnDelete.Hidden = true;

                btnNew.OnClientClick = Window1.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CardIssuer bll = new Edge.SVA.BLL.CardIssuer();

            //获得总条数
            this.Grid1.RecordCount = bll.GetRecordCount(strWhere);
            if (this.Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            int startindex = this.Grid1.PageSize * this.Grid1.PageIndex + 1;
            int endindex = this.Grid1.PageSize * (this.Grid1.PageIndex + 1);
            ds = bll.GetListByPage(strWhere, orderby, startindex, endindex);
            Tools.DataTool.AddCardIssuerName(ds, "CardIssuerName", "CardIssuerID");
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CardIssuerID>0", "CreatedOn");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("CardIssuerID>0", "CreatedOn");
        }
    }
}
