﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardTypeCode" runat="server" Label="卡类型编号：">
                                            </ext:Label>
                                            <ext:Label ID="CardTypeName1" runat="server" Label="描述：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardTypeName2" runat="server" Label="其他描述1：">
                                            </ext:Label>
                                            <ext:Label ID="CardTypeName3" runat="server" Label="其他描述2：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow3" runat="server">
                                        <Items>
                                            <ext:Label ID="BrandID" runat="server" Label="品牌：">
                                            </ext:Label>
                                            <ext:Label ID="CurrencyID" runat="server" Label="币种：">
                                            </ext:Label>
                                        </Items>
                                        <Items>
                                            <ext:RadioButtonList ID="IsDumpCard" runat="server" Label="是否为转储卡："                                 AutoPostBack="true" Width="100px">
                                            <ext:RadioItem Text="是" Value="1" />
                                            <ext:RadioItem Text="否" Value="0" Selected="true" />
                                            </ext:RadioButtonList>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="卡号规则">
                <Items>
                    <ext:SimpleForm ID="sform2" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Label ID="IsImportUIDNumberView" runat="server" Label="卡号码是否导入：">
                            </ext:Label>
                            <ext:RadioButtonList ID="IsImportUIDNumber" runat="server" Label="卡号码是否导入：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ManualRoles" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                <Items>
                    <ext:SimpleForm ID="sform3" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow4" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardNumMask" runat="server" Label="卡号编码规则：">
                                            </ext:Label>
                                            <ext:Label ID="CardNumPattern" runat="server" Label="卡号前缀号码：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow5" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardCheckdigitView" runat="server" Label="卡号是否添加校验位：">
                                            </ext:Label>
                                            <ext:Label ID="CheckDigitModeIDView" runat="server" Label="校验位计算方法：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow6" runat="server">
                                        <Items>
                                            <ext:Label ID="CardNumberToUIDView" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:RadioButtonList ID="CardCheckdigit" runat="server" Label="卡号是否添加校验位：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px">
                                <ext:ListItem Value="1" Text="EAN13" />
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="CardNumberToUID" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End"
                                Enabled="false">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ImportRoles" runat="server" EnableCollapse="True" Title="导入物理编号规则">
                <Items>
                    <ext:SimpleForm ID="sform4" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form4" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardNumMaskImport" runat="server" Label="卡号编码规则：">
                                            </ext:Label>
                                            <ext:Label ID="CardNumPatternImport" runat="server" Label="卡号前缀号码：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="IsConsecutiveUIDView" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End">
                                            </ext:Label>
                                            <ext:Label ID="UIDCheckDigitView" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow9" runat="server">
                                        <Items>
                                            <ext:Label ID="CheckDigitModeID2View" runat="server" Label="校验位计算方法：" />
                                            <ext:Label ID="UIDToCardNumberView" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>

                            <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End"
                                Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：">
                                <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="UIDToCardNumber" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End"
                                Enabled="false">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="使用规则">
                <Items>
                    <ext:SimpleForm ID="sform5" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form5" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow10" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardMustHasOwnerView" runat="server" Label="是否必须指定会员：">
                                            </ext:Label>
                                            <ext:Label ID="CashExpiredateView" runat="server" Label="现金帐套是否有多重有效期：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow11" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="PointExpiredateView" runat="server" Label="积分帐套是否有多重有效期：">
                                            </ext:Label>
                                            <ext:Label ID="IsPhysicalCardView" runat="server" Label="是否需要写卡：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:RadioButtonList ID="CardMustHasOwner" runat="server" Label="是否必须指定会员：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="CashExpiredate" runat="server" Label="现金帐套是否有多重有效期：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="PointExpiredate" runat="server" Label="积分帐套是否有多重有效期：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="IsPhysicalCard" runat="server" Label="是否需要写卡：" Enabled="false">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="有效性">
                <Items>
                    <ext:SimpleForm ID="sform6" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form6" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow12" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CardTypeStartDate" runat="server" Label="卡类型启用日期：">
                                            </ext:Label>
                                            <ext:Label ID="CardTypeEndDate" runat="server" Label="卡类型结束日期：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="其他信息">
                <Items>
                    <ext:SimpleForm ID="sform7" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form7" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                <Rows>
                                    <ext:FormRow ID="FormRow13" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                            </ext:Label>
                                            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow14" runat="server" ColumnWidths="50% 50%">
                                        <Items>
                                            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                                            </ext:Label>
                                            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="卡级别列表">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True"
                        AutoWidth="true" ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="卡级别编号">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CardGradeCode") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别等级">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("CardgradeRank") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别描述">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("CardGradeName1") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
