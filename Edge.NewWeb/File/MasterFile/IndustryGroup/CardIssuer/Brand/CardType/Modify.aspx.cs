﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Text;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                RegisterCloseEvent(btnClose);
            
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CardTypeID"] = Model.CardTypeID;

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                string brandID = brand.BrandID.ToString();
                this.BrandID.Items.Add(new FineUI.ListItem() { Text = brandText, Value = brandID });

                string msg = "";

                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }
                else if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMask.Text = Model.CardNumMask;
                    this.CardNumPattern.Text = Model.CardNumPattern;
                }


                //如果已经创建Card或者已创建CardGrade， 就不能修改其他规则
                if ((DALTool.isCardTypeCreatedCard(Model.CardTypeID, ref msg)) || DALTool.isCardTypeCreatedCardGrade(Model.CardTypeID, ref msg))
                {
                    if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                    {
                        this.CardNumMask.Enabled = false;
                        this.CardNumPattern.Enabled = false;
                        this.CardCheckdigit.Enabled = false;
                        this.CardNumberToUID.Enabled = false;
                        this.CheckDigitModeID.Enabled = false;
                    }
                    else
                    {
                        this.IsConsecutiveUID.Enabled = false;
                        this.UIDCheckDigit.Enabled = false;
                        this.UIDToCardNumber.Enabled = false;
                        this.CardNumMaskImport.Enabled = false;
                        this.CardNumPatternImport.Enabled = false;
                    }

                    this.CardTypeCode.Enabled = false;
                    this.CardMustHasOwner.Enabled = false;
                    this.CashExpiredate.Enabled = false;
                    this.PointExpiredate.Enabled = false;
                    this.IsPhysicalCard.Enabled = false;
                    this.IsImportUIDNumber.Enabled = false;
                    this.BrandID.Enabled = false;                 
                }

                //if (DALTool.isCardTypeCreatedCard(Model.CardTypeID, ref msg))//如果已经创建卡就不能修改其他规则
                //{
                //    if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                //    {
                //        this.CardNumMask.Enabled = false;
                //        this.CardNumPattern.Enabled = false;
                //        this.CardCheckdigit.Enabled = false;
                //        this.CardNumberToUID.Enabled = false;
                //        this.CheckDigitModeID.Enabled = false;
                //    }
                //    else
                //    {
                //        this.IsConsecutiveUID.Enabled = false;
                //        this.UIDCheckDigit.Enabled = false;
                //        this.UIDToCardNumber.Enabled = false;
                //        this.CardNumMaskImport.Enabled = false;
                //        this.CardNumPatternImport.Enabled = false;
                //    }

                //    this.CardTypeCode.Enabled = false;
                //    this.CardMustHasOwner.Enabled = false;
                //    this.CashExpiredate.Enabled = false;
                //    this.PointExpiredate.Enabled = false;
                //    this.IsPhysicalCard.Enabled = false;
                //    this.IsImportUIDNumber.Enabled = false;
                //}
                //else//未创建卡
                //{
                //    if (DALTool.isCardTypeCreatedCardGrade(Model.CardTypeID, ref msg))//已创建CardGrade
                //    {
                //        if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                //        {
                //            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                //            {
                //                this.CardNumMask.Enabled = false;
                //                this.CardNumPattern.Enabled = false;
                //                this.CardCheckdigit.Enabled = false;
                //                this.CardNumberToUID.Enabled = false;
                //                this.CheckDigitModeID.Enabled = false;
                //            }
                //        }
                //        else
                //        {
                //            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                //            {
                //                this.CardNumMaskImport.Enabled = false;
                //                this.CardNumPatternImport.Enabled = false;
                //            }
                //            //this.IsConsecutiveUID.Enabled = false;
                //            //this.UIDCheckDigit.Enabled = false;
                //            //this.UIDToCardNumber.Enabled = false;
                //        }

                //        this.IsImportUIDNumber.Enabled = false;
                //    }
                //}

                checkIsImportCardNumber(false);

                BindCardGrad();
            }
        }
        //可以不重载，但重载性能更佳
        protected override void SetObject()
        {
            base.SetObject(this.Model, this.Panel1.Controls.GetEnumerator());
        }


        protected override SVA.Model.CardType GetPageObject(SVA.Model.CardType obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void IsImportUIDNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkIsImportCardNumber(true);
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);
                                                                                      
            Edge.SVA.Model.CardType item = this.GetUpdateObject();
           
            if (!ValidObject(item)) return;

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardType>(item))
            {
                //Update card grade
                if (this.IsImportUIDNumber.SelectedValue == "0")//手动
                {

                   //if ((this.CardNumMask.Enabled) && (!string.IsNullOrEmpty(CardNumMask.Text.Trim())))//号码规则可以修改，并且不为空的时候更新
                    //{
                    //    List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
                    //    foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
                    //    {
                    //        grade.CardNumMask = item.CardNumMask.ToUpper();
                    //        grade.CardNumPattern = item.CardNumPattern.Trim();
                    //        grade.CardCheckdigit = item.CardCheckdigit.GetValueOrDefault();
                    //        grade.CheckDigitModeID = item.CheckDigitModeID.GetValueOrDefault();
                    //        grade.CardNumberToUID = item.CardNumberToUID.GetValueOrDefault();
                    //        new Edge.SVA.BLL.CardGrade().Update(grade);
                    //    }
                    //}

                }
                else//导入
                {
                    //if (IsConsecutiveUID.Enabled && UIDCheckDigit.Enabled && UIDToCardNumber.Enabled)//可以修改的时候更新
                    //{
                    //    List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
                    //    foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
                    //    {
                    //        grade.IsConsecutiveUID = item.IsConsecutiveUID;
                    //        grade.UIDCheckDigit = item.UIDCheckDigit;
                    //        grade.UIDToCardNumber = item.UIDToCardNumber;
                    //        new Edge.SVA.BLL.CardGrade().Update(grade);
                    //    }
                    //}
                }
                CardTypeRepostory.Singleton.Refresh();
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        private bool ValidObject(SVA.Model.CardType item)
        {
            if (item == null) return false;

            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardTypeCode = item.CardTypeCode.ToUpper();
            item.CardNumMask = item.CardNumMask.ToUpper();

            if (Edge.Web.Tools.DALTool.isHasCardTypeCode(item.CardTypeCode, item.CardTypeID))
            {
                ShowWarning(Resources.MessageTips.ExistCardTypeCode);
                return false;
            }
            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport, this.CardNumPatternImport))
                        {
                            return false;
                        }
                    }

                    if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMaskImport.Text.Trim(), this.CardNumPatternImport.Text.Trim(), 0, item.CardTypeID))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");

                }
                else
                {
                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;


                    if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask, this.CardNumPattern))
                        {
                            return false;
                        }
                    }


                    if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, item.CardTypeID))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");

                }
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
                return false;
            }

            //if (!string.IsNullOrEmpty(item.CardNumMask) || !string.IsNullOrEmpty(item.CardNumPattern))
            //{
            //    //if (item.UIDToCardNumber.GetValueOrDefault() == 0 && !Controllers.RegexController.GetInstance().IsNumMask(item.CardNumMask, item.CardNumPattern))
            //    //{
            //    //    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //    //    //this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90383"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //    //    ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90383"));
            //    //    return false;
            //    //}

            //    if (Tools.DALTool.isHasCardTypeCardNumMask(item.CardNumMask, item.CardNumPattern, item.CardTypeID))
            //    {
            //        string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //        //this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }

            //    if (Tools.DALTool.isHasCardCardeCardNumMaskWithCardType(item.CardNumMask, item.CardNumPattern, item.CardTypeID))
            //    {
            //        string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //        //this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }


            //    if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, item.CardTypeID))
            //    {
            //       // this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }
            //}

            return true;
        }

        private void checkIsImportCardNumber(bool clearText)
        {
            if (IsImportUIDNumber.SelectedValue == "1")
            {
                Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);

                this.ManualRoles.Hidden = true;
                this.ImportRoles.Hidden = false;
            }
            else
            {
                Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
                this.ManualRoles.Hidden = false;
                this.ImportRoles.Hidden = true;
            }

            if (clearText)
            {
                this.CardNumPattern.Text = "";
                this.CardNumMask.Text = "";
                this.CardNumPatternImport.Text = "";
                this.CardNumMaskImport.Text = "";
            }
        }

        //private void checkCheckDigitMode4Manual()
        //{
        //    if (CardCheckdigit.SelectedValue == "1")
        //    {
        //        CheckDigitModeID.Hidden = true;

        //        foreach (FineUI.ListItem item in CardNumberToUID.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in CardNumberToUID.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID.Hidden = false;
        //    }
        //}

        //private void checkCheckDigitMode4Import()
        //{
        //    if (UIDCheckDigit.SelectedValue == "1")
        //    {
        //        foreach (FineUI.ListItem item in UIDToCardNumber.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in UIDToCardNumber.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //}

        protected void CardCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
            CardNumberToUID.SelectedIndex = 0;
        }

        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            UIDToCardNumber.SelectedIndex = 0;
        }

        private void BindCardGrad()
        {
            this.Grid1.PageSize = webset.ContentPageNum;
            this.Grid1.RecordCount = new Edge.SVA.BLL.CardGrade().GetCount("CardTypeID='" + ViewState["CardTypeID"].ToString() + "'");
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(Grid1.PageSize, Grid1.PageIndex, "CardTypeID='" + ViewState["CardTypeID"].ToString() + "'", "CardGradeCode");

            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count>0)
            {
                this.CardTypeCode.Enabled = false;
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }

        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindCardGrad();
        }

        protected void CardTypeCode_TextChanged(object sender, EventArgs e)
        {
            this.CardTypeCode.Text = this.CardTypeCode.Text.ToUpper();
        }

        protected void CardNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMask.Text = this.CardNumMask.Text.ToUpper();
        }

        protected void CardNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMask.Text) && string.IsNullOrEmpty(this.CardNumPattern.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CardNumMask, CardNumPattern);
            //if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask.Text, this.CardNumPattern.Text,false))
            //{
            //    CardNumPattern.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CardNumPattern.Text));
            //}
        }

        protected void CardNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMaskImport.Text = this.CardNumMaskImport.Text.ToUpper();
        }

        protected void CardNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMaskImport.Text) && string.IsNullOrEmpty(this.CardNumPatternImport.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CardNumMaskImport, CardNumPatternImport);
            //if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask.Text, this.CardNumPattern.Text,false))
            //{
            //    CardNumPatternImport.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CardNumPatternImport.Text));
            //}
        }

        protected void UIDToCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            this.CardNumMaskImport.Text = "";
            this.CardNumPatternImport.Text = "";
        }

        protected void CardNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    logger.WriteOperationLog(this.PageName, "Update");
        //    int page = 0;
        //    int.TryParse(Request.Params["page"], out page);

        //    Edge.SVA.Model.CardType item = this.GetUpdateObject();

        //    if (!ValidObject(item)) return;
     
        //    if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardType>(item))
        //    {
        //        //Update card grade
        //        if (this.IsImportUIDNumber.SelectedValue == "0")//手动
        //        {
        //            if ((this.CardNumMask.Enabled) && (!string.IsNullOrEmpty(CardNumMask.Text.Trim())))//号码规则可以修改，并且不为空的时候更新
        //            {
        //                List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
        //                foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
        //                {
        //                    grade.CardNumMask = item.CardNumMask.ToUpper();
        //                    grade.CardNumPattern = item.CardNumPattern.Trim();
        //                    grade.CardCheckdigit = item.CardCheckdigit.GetValueOrDefault();
        //                    grade.CheckDigitModeID = item.CheckDigitModeID.GetValueOrDefault();
        //                    grade.CardNumberToUID = item.CardNumberToUID.GetValueOrDefault();
        //                    new Edge.SVA.BLL.CardGrade().Update(grade);
        //                }
        //            }

        //        }
        //        else//导入
        //        {
        //            if (IsConsecutiveUID.Enabled && UIDCheckDigit.Enabled && UIDToCardNumber.Enabled)//可以修改的时候更新
        //            {
        //                List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
        //                foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
        //                {
        //                    grade.IsConsecutiveUID = item.IsConsecutiveUID;
        //                    grade.UIDCheckDigit = item.UIDCheckDigit;
        //                    grade.UIDToCardNumber = item.UIDToCardNumber;
        //                    new Edge.SVA.BLL.CardGrade().Update(grade);
        //                }
        //            }
        //        }
        //        JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
        //    }
        //    else
        //    {
        //        JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
        //    }
        //}

     

        //private void BindCardGrad()
        //{
        //    this.rptCardGradeListPager.PageSize = webset.ContentPageNum;
        //    this.rptCardGradeListPager.RecordCount = new Edge.SVA.BLL.CardGrade().GetCount("CardTypeID='" + ViewState["CardTypeID"].ToString() + "'");
        //    DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(this.rptCardGradeListPager.PageSize, this.rptCardGradeListPager.CurrentPageIndex - 1, "CardTypeID='" + ViewState["CardTypeID"].ToString() + "'", "CardGradeCode");
        //    if (ds.Tables[0] != null)
        //    {
        //        this.rptCardGradeList.DataSource = ds.Tables[0].DefaultView;
        //        this.rptCardGradeList.DataBind();
        //    }
        //    else
        //    {
        //        this.rptCardGradeList.DataSource = null;
        //        this.rptCardGradeList.DataBind();
        //    }

        //}

        //protected void rptCardGradeListPager_PageChanged(object sender, EventArgs e)
        //{
        //    BindCardGrad();
        //}
    }
}
