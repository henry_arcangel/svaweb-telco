﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using System.Text;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                RegisterCloseEvent(btnClose);

                checkIsImportCardNumber(false);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            if (Edge.Web.Tools.DALTool.isHasCardTypeCode(this.CardTypeCode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistCardTypeCode);
                return;
            }

            Edge.SVA.Model.CardType item = this.GetAddObject();
            if (!ValidObject(item)) return;

            if (Tools.DALTool.Add<Edge.SVA.BLL.CardType>(item) > 0)
            {
                CardTypeRepostory.Singleton.Refresh();
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        private bool ValidObject(SVA.Model.CardType item)
        {
            if (item == null) return false;

            item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.CreatedOn = System.DateTime.Now;
            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardTypeCode = item.CardTypeCode.ToUpper();
            item.CardNumMask = item.CardNumMask.ToUpper();
            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport, this.CardNumPatternImport))
                        {
                            return false;
                        }
                    }


                    if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMaskImport.Text.Trim(), this.CardNumPatternImport.Text.Trim(), 0, 0))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
                else
                {

                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;

                    if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask, this.CardNumPattern))
                        {
                            return false;
                        }
                    }

                    if (!string.IsNullOrEmpty(this.CardNumMask.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPattern.Text.Trim()))
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, 0))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }

            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
                return false;
            }

            //if (!string.IsNullOrEmpty(item.CardNumMask) || !string.IsNullOrEmpty(item.CardNumPattern))
            //{
            //    //if (item.UIDToCardNumber.GetValueOrDefault() == 0 && !Controllers.RegexController.GetInstance().IsNumMask(item.CardNumMask, item.CardNumPattern))
            //    //{
            //    //    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //    //    ///  this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90383"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //    //    ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90383"));
            //    //    return false;
            //    //}

            //    if (Tools.DALTool.isHasCardTypeCardNumMask(item.CardNumMask, item.CardNumPattern, 0))
            //    {
            //        string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //        //  this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }

            //    if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, 0))
            //    {
            //        string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
            //        // this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }

            //    if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, 0))
            //    {
            //        //this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
            //        ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
            //        return false;
            //    }
            //}

            return true;
        }

        protected void IsImportUIDNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
           checkIsImportCardNumber(true);
        }

        private void checkIsImportCardNumber(bool clearText)
        {
            if (IsImportUIDNumber.SelectedValue == "1")
            {
                checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);

                this.ManualRoles.Hidden = true;
                this.ImportRoles.Hidden = false;
            }
            else
            {
                checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);

                this.ManualRoles.Hidden = false;
                this.ImportRoles.Hidden = true;
            }


            if (clearText)
            {
                this.CardNumPattern.Text = "";
                this.CardNumMask.Text = "";
                this.CardNumPatternImport.Text = "";
                this.CardNumMaskImport.Text = "";
            }
        }

        //private void checkCheckDigitMode4Manual()
        //{
        //    if (CardCheckdigit.SelectedValue == "1")
        //    {
        //        CheckDigitModeID.Hidden = true;

        //        foreach (FineUI.ListItem item in CardNumberToUID.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in CardNumberToUID.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID.Hidden = false;
        //    }
        //}

        //private void checkCheckDigitMode4Import()
        //{
        //    if (UIDCheckDigit.SelectedValue == "1")
        //    {
        //        foreach (FineUI.ListItem item in UIDToCardNumber.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in UIDToCardNumber.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }
        //    }
        //}

        protected void CardCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
            CardNumberToUID.SelectedIndex = 0;
        }

        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            UIDToCardNumber.SelectedIndex = 0;
        }


        protected override SVA.Model.CardType GetPageObject(SVA.Model.CardType obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CardTypeCode_TextChanged(object sender, EventArgs e)
        {
            this.CardTypeCode.Text = this.CardTypeCode.Text.ToUpper();
        }

        protected void CardNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMask.Text = this.CardNumMask.Text.ToUpper();
        }

        protected void CardNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMask.Text) && string.IsNullOrEmpty(this.CardNumPattern.Text)) return;
            Tools.CheckTool.IsMatchNumMask(CardNumMask, CardNumPattern);
        }

        protected void CardNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMaskImport.Text = this.CardNumMaskImport.Text.ToUpper();
        }

        protected void CardNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMaskImport.Text) && string.IsNullOrEmpty(this.CardNumPatternImport.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CardNumMaskImport, CardNumPatternImport);
        }

        protected void UIDToCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            this.CardNumMaskImport.Text = "";
            this.CardNumPatternImport.Text = "";
        }

        protected void CardNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        }


        #region Add & Edit public function
        public static void checkCheckDigitMode4Manual(FineUI.RadioButtonList cardCheckdigit, FineUI.DropDownList cardNumberToUID, FineUI.DropDownList checkDigitModeID)
        {
            //Check select CD
            if (cardCheckdigit.SelectedValue == "1")
            {
                foreach (FineUI.ListItem item in cardNumberToUID.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in cardNumberToUID.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (cardNumberToUID.SelectedValue == "3" && cardCheckdigit.SelectedValue == "0")
                {
                    checkDigitModeID.Hidden = false;
                }
                else
                {
                    checkDigitModeID.Hidden = true;
                }
            }
        }

        public static void checkCheckDigitMode4Import(FineUI.RadioButtonList uIDCheckDigit, FineUI.DropDownList uIDToCardNumber, FineUI.DropDownList checkDigitModeID2, FineUI.TextBox cardNumMaskImport, FineUI.TextBox cardNumPatternImport)
        {
            //Check select CD
            if (uIDCheckDigit.SelectedValue == "1")
            {
                foreach (FineUI.ListItem item in uIDToCardNumber.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID2.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in uIDToCardNumber.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (uIDToCardNumber.SelectedValue == "3")
                {
                    checkDigitModeID2.Hidden = false;
                }
                else
                {
                    checkDigitModeID2.Hidden = true;
                }
            }

            //Check CouponNumMask
            if (uIDToCardNumber.SelectedValue == "0")
            {
                cardNumMaskImport.Hidden = false;
                cardNumPatternImport.Hidden = false;
            }
            else
            {
                cardNumMaskImport.Hidden = true;
                cardNumPatternImport.Hidden = true;
            }
        }

        #endregion
    }
}
