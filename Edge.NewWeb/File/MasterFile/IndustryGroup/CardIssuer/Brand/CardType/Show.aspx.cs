﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!Page.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CardTypeID"] = Model.CardTypeID;

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                Edge.SVA.Model.Currency currency = new Edge.SVA.BLL.Currency().GetModel(Model.CurrencyID.GetValueOrDefault());
                string currencyText = currency == null ? "" : DALTool.GetStringByCulture(currency.CurrencyName1, currency.CurrencyName2, currency.CurrencyName3);
                this.CurrencyID.Text = currency == null ? "" : ControlTool.GetDropdownListText(currencyText, currency.CurrencyCode);

                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                this.CardCheckdigitView.Text = this.CardCheckdigit.SelectedItem == null ? "" : this.CardCheckdigit.SelectedItem.Text;
                this.CardMustHasOwnerView.Text = this.CardMustHasOwner.SelectedItem == null ? "" : this.CardMustHasOwner.SelectedItem.Text;
                this.IsImportUIDNumberView.Text = this.IsImportUIDNumber.SelectedItem == null ? "" : this.IsImportUIDNumber.SelectedItem.Text;
                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CardNumberToUIDView.Text = this.CardNumberToUID.SelectedItem == null ? "" : this.CardNumberToUID.SelectedItem.Text;
                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.UIDToCardNumberView.Text = this.UIDToCardNumber.SelectedItem == null ? "" : this.UIDToCardNumber.SelectedItem.Text;
                this.CashExpiredateView.Text = this.CashExpiredate.SelectedItem == null ? "" : this.CashExpiredate.SelectedItem.Text;
                this.PointExpiredateView.Text = this.PointExpiredate.SelectedItem == null ? "" : this.PointExpiredate.SelectedItem.Text;
                this.IsPhysicalCardView.Text = this.IsPhysicalCard.SelectedItem == null ? "" : this.IsPhysicalCard.SelectedItem.Text;         

                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }

                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    this.CheckDigitModeID2.SelectedValue = Model.CheckDigitModeID.GetValueOrDefault().ToString();
                    this.CheckDigitModeID2View.Text = this.CheckDigitModeID2.SelectedItem == null ? "" : this.CheckDigitModeID2.SelectedItem.Text;
                }

                //Check import or manual
                checkIsImportCouponNumber(false);

                this.CardCheckdigit.Hidden = true;
                this.CardMustHasOwner.Hidden = true;
                this.IsImportUIDNumber.Hidden = true;
                this.CheckDigitModeID.Hidden = true;
                this.CheckDigitModeID2.Hidden = true;
                this.CardNumberToUID.Hidden = true;
                this.UIDCheckDigit.Hidden = true;
                this.IsConsecutiveUID.Hidden = true;
                this.UIDToCardNumber.Hidden = true;
                this.CashExpiredate.Hidden = true;
                this.PointExpiredate.Hidden = true;
                this.IsPhysicalCard.Hidden = true;
               

                BindCardGrad();

            }
        }

        //可以不重载，但重载性能更佳
        protected override void SetObject()
        {
            base.SetObject(this.Model, this.Panel1.Controls.GetEnumerator());
        }

        private void BindCardGrad()
        {
            this.Grid1.PageSize = webset.ContentPageNum;
            this.Grid1.RecordCount = new Edge.SVA.BLL.CardGrade().GetCount("CardTypeID='" + ViewState["CardTypeID"].ToString() + "'");
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(Grid1.PageSize, Grid1.PageIndex, "CardTypeID='" + ViewState["CardTypeID"].ToString() + "'", "CardGradeCode");
            if (ds.Tables[0] != null)
            {
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }

        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindCardGrad();
        }


        private void checkIsImportCouponNumber(bool clearText)
        {
            if (this.IsImportUIDNumber.SelectedValue == "1")
            {
                this.ManualRoles.Hidden = true;
                this.ImportRoles.Hidden = false;

                checkCheckDigitMode4Import();
            }
            else
            {
                this.ManualRoles.Hidden = false;
                this.ImportRoles.Hidden = true;

                checkCheckDigitMode4Manual();
            }

            if (clearText)
            {
                this.CardNumMask.Text = "";
                this.CardNumPattern.Text = "";
                this.CardNumMaskImport.Text = "";
                this.CardNumPatternImport.Text = "";
            }
        }

        public void checkCheckDigitMode4Manual()
        {
            //Check select CD
            if (CardCheckdigit.SelectedValue == "1")
            {
                //Check Digit Mode
                CheckDigitModeIDView.Hidden = false;
            }
            else
            {
                //Check Digit Mode
                if (CardNumberToUID.SelectedValue == "3" && CardCheckdigit.SelectedValue == "0")
                {
                    CheckDigitModeIDView.Hidden = false;
                }
                else
                {
                    CheckDigitModeIDView.Hidden = true;
                }
            }
        }

        public void checkCheckDigitMode4Import()
        {
            //Check select CD
            if (UIDCheckDigit.SelectedValue == "1")
            {
                //Check Digit Mode
                CheckDigitModeID2View.Hidden = false;
            }
            else
            {
                //Check Digit Mode
                if (UIDToCardNumber.SelectedValue == "3")
                {
                    CheckDigitModeID2View.Hidden = false;
                }
                else
                {
                    CheckDigitModeID2View.Hidden = true;
                }
            }

            //Check CouponNumMask
            if (UIDToCardNumber.SelectedValue == "0")
            {
                CardNumMaskImport.Hidden = false;
                CardNumPatternImport.Hidden = false;
            }
            else
            {
                CardNumMaskImport.Hidden = true;
                CardNumPatternImport.Hidden = true;
            }
        }   
        //private void checkIsImportCardNumber()
        //{
        //    if (IsImportUIDNumber.SelectedValue == "1")
        //    {
        //        this.ManualRoles.Hidden = true;
        //        this.ImportRoles.Hidden = false;
        //    }
        //    else
        //    {
        //        this.ManualRoles.Hidden = false;
        //        this.ImportRoles.Hidden = true;
        //        checkCheckDigitMode();
        //    }
        //}

        //private void checkCheckDigitMode()
        //{
        //    if (CardCheckdigit.SelectedValue == "0")
        //    {
        //        CheckDigitModeID.Hidden = true;
        //    }
        //    else
        //    {
        //        CheckDigitModeID.Hidden = false;
        //    }


        //}
    }
}
