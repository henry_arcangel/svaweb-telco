﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");

                RptBind("CardTypeID>=0", "CardTypeCode");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                ControlTool.BindBrand(this.Brand);
               
            }
           
        }
        #region Event
        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    int brandid = this.Brand.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.Brand.SelectedValue);
                    string code = this.Code.Text.Trim();
                    string desc = this.Desc.Text.Trim();
                    if (brandid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" BrandID =");
                        sb.Append(brandid);
                        sb.Append("");
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardTypeCode like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(desc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CardTypeName1";

                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(desc);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion


                //Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();

                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);
                //if (this.Grid1.RecordCount > 0)
                //{
                //    this.btnDelete.Enabled = true;
                //}
                //else
                //{
                //    this.btnDelete.Enabled = false;
                //}
                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

                //Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

                //ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));
                //Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
                //Edge.SVA.BLL.CardIssuer bllCardIssuer = new Edge.SVA.BLL.CardIssuer();

                //foreach (DataRow row in ds.Tables[0].Rows)
                //{
                //    Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(int.Parse(row["BrandID"].ToString()));
                //    if (mBrand != null)
                //    {
                //        Edge.SVA.Model.CardIssuer mCardIssuer = bllCardIssuer.GetModel(mBrand.CardIssuerID.GetValueOrDefault());
                //        if (mCardIssuer != null) row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetStringByCulture(mCardIssuer.CardIssuerName1, mCardIssuer.CardIssuerName2, mCardIssuer.CardIssuerName3);
                //    }

                //}
                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();

                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                CardTypeController controller = new CardTypeController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("CardType", "Load Faild", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CardTypeController controller = new CardTypeController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            //DataView view1 = table.DefaultView;
            //view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CardTypeID>=0", "CardTypeCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("CardTypeID>=0", "CardTypeCode");
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("CardTypeID>0", "CardTypeCode");
        }
    }
}
