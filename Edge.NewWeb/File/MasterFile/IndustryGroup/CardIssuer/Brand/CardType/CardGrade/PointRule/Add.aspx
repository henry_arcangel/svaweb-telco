﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm2,SimpForm3" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料"
                AutoHeight="true" AutoWidth="true">
            <Items>
                <ext:SimpleForm ID="SimpForm2" ShowBorder="false" ShowHeader="false" runat="server"
                    BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                    LabelAlign="Right">
                    <Items>
                        <ext:TextBox ID="PointRuleCode" runat="server" Label="规则编号：" ShowRedStar="true"
                            Required="true" OnTextChanged="PointRuleCode_TextChanged" AutoPostBack="true">
                        </ext:TextBox>
<%--                        <ext:NumberBox ID="PointRuleSeqNo" runat="server" Label="记录序号：" ShowRedStar="true"
                            Required="true">
                        </ext:NumberBox>--%>
                        <ext:TextBox ID="PointRuleDesc1" runat="server" Label="规则描述1：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                        </ext:TextBox>
                        <ext:TextBox ID="PointRuleDesc2" runat="server" Label="规则描述2：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                        </ext:TextBox>
                        <ext:TextBox ID="PointRuleDesc3" runat="server" Label="规则描述3：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                        </ext:TextBox>
                        <ext:DropDownList ID="CardTypeID" runat="server" Enabled="false" Label="卡类型：" Resizable="true">
                        </ext:DropDownList>
                        <ext:DropDownList ID="CardGradeID" runat="server" Enabled="false" Label="卡等级：" Resizable="true">
                        </ext:DropDownList>
                        <ext:DropDownList ID="MemberDateType" runat="server"  Label="会员范围：" Resizable="true">
                        </ext:DropDownList>
                        <ext:DropDownList ID="PointRuleType" runat="server"  Label="交易类型：" Resizable="true">
                        </ext:DropDownList>
                        <ext:DropDownList ID="PointRuleOper" runat="server"  Label="计算方式设置：" Resizable="true" ShowRedStar="true" Required="true"
                            ToolTipTitle="计算方式设置" ToolTip="Translate__Special_121_Start注意：0：每当消费PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分。  如：PointRuleAmount=100，PointRulePoints=10， 消费2000， 就可以获得 （2000 /100 ）* 10 = 2001：当消费金额大于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分2：当消费金额小于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分Translate__Special_121_End">
                        </ext:DropDownList>
                        <ext:NumberBox ID="PointRuleAmount" runat="server" Label="设定的积分规则金额：" NoDecimal="false" 
                            ShowRedStar="true" Required="true"  Text="0" MaxValue="1000000000" MinValue="0">
                        </ext:NumberBox>
                        <ext:NumberBox ID="PointRulePoints" runat="server" Label="设定的积分规则积分：" NoDecimal="true" ShowRedStar="true"
                            Required="true" Text="0" MaxValue="99999999" MinValue="0">
                        </ext:NumberBox>
                        <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px">
                            <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                            <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
                        </ext:RadioButtonList>
                        <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
                    </Items>
                </ext:SimpleForm>
            </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="生效时间设置"
                AutoHeight="true" AutoWidth="true">
            <Items>
                <ext:SimpleForm ID="SimpForm3" ShowBorder="false" ShowHeader="false" runat="server"
                    BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                    LabelAlign="Right">
                    <Items>
                        <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                            ShowRedStar="true" Required="true">
                        </ext:DatePicker>
                        <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                            CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于等于开始日期"
                            ShowRedStar="true" Required="true">
                        </ext:DatePicker>
                        <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                            <Rows>
                                <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                                    <Items>
                                        <ext:TextBox ID="DayFlagCode" runat="server" Label="一月中促销生效日："></ext:TextBox>
                                        <ext:Button ID="btnAddDay" runat="server" Text="添加" Icon="Add" OnClick="btnAddDay_Click"></ext:Button>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                        </ext:Form>
                        <ext:Form ID="Form3" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                            <Rows>
                                <ext:FormRow ID="FormRow2" ColumnWidths="85% 15%" runat="server">
                                    <Items>
                                        <ext:TextBox ID="WeekFlagCode" runat="server" Label="一周中促销生效日："></ext:TextBox>
                                        <ext:Button ID="btnAddWeek" runat="server" Text="添加" Icon="Add" OnClick="btnAddWeek_Click"></ext:Button>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                        </ext:Form>
                        <ext:Form ID="Form4" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                            <Rows>
                                <ext:FormRow ID="FormRow3" ColumnWidths="85% 15%" runat="server">
                                    <Items>
                                        <ext:TextBox ID="MonthFlagCode" runat="server" Label="促销生效月："></ext:TextBox>
                                        <ext:Button ID="btnAddMonth" runat="server" Text="添加" Icon="Add" OnClick="btnAddMonth_Click"></ext:Button>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                        </ext:Form>
                    </Items>
                </ext:SimpleForm>
            </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="生效地点设置"
                AutoHeight="true" AutoWidth="true">
            <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnAddStore" Icon="Add" runat="server" Text="添加" OnClick="btnAddStore_Click" Enabled="false">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnDeleteStore" Icon="Delete" OnClick="btnDeleteStore_Click"
                                runat="server" Text="删除" Enabled="false">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearStore" Icon="Delete" Enabled="false"
                                OnClick="btnClearStore_Click" runat="server" Text="清空">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="3" runat="server" EnableCheckBoxSelect="true" DataKeyNames="StoreID"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="店铺">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("StoreName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="积分计算规则的货品条件"
                AutoHeight="true" AutoWidth="true" Hidden="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar5" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnViewSearch" Icon="Add" runat="server" Text="添加" OnClick="btnViewSearch_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator5" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnDeleteResultItem" Icon="Delete" OnClick="btnDeleteResultItem_Click"
                                runat="server" Text="删除">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearAllResultItem" Icon="Delete"
                                OnClick="btnClearAllResultItem_Click" runat="server" Text="清空">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="AddResultListGrid" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="3" runat="server" EnableCheckBoxSelect="true" DataKeyNames="PointRuleCode"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="AddResultListGrid_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="130px" HeaderText="积分规则编码">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("PointRuleCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="130px" HeaderText="积分规则适用类型">
                                <ItemTemplate>
                                    <asp:Label ID="glblApproveStatus" runat="server" Text='<%#Eval("EntityTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="积分规则适用编码">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("EntityNum")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
    </Items>
    </ext:SimpleForm>
    <ext:Window ID="Window2" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="400px">
    </ext:Window>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit1_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="250px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
