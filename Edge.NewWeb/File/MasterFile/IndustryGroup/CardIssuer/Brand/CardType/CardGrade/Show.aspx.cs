﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using Edge.SVA.Model;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>
    {

        Tools.Logger logger = Tools.Logger.Instance;
        CardGradeModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                //CardExtensionRuleRptBind("CardGradeID=" + Request.Params["id"]);
                //CardPointRuleRptBind("CardGradeID=" + Request.Params["id"]);


                //其他规则初始化
                CardExtensionRuleRptBind("CardGradeID=" + Request.Params["id"]);
                CardPointRuleRptBind("CardGradeID=" + Request.Params["id"]);

                this.Tab1.Hidden = this.Tab2.Hidden = true;
                SVASessionInfo.CardGradeAddController = null;

                //新增字段CardGradeUpdCouponTypeID 
                ControlTool.BindCouponType(CardGradeUpdCouponTypeID);
            }
            controller = SVASessionInfo.CardGradeModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.Model.CardType cardType = new Edge.SVA.BLL.CardType().GetModel(Model.CardTypeID);
                string cardTypeText = cardType == null ? "" : DALTool.GetStringByCulture(cardType.CardTypeName1, cardType.CardTypeName2, cardType.CardTypeName3);
                this.CardTypeID.Text = cardType == null ? "" : ControlTool.GetDropdownListText(cardTypeText, cardType.CardTypeCode);

                Edge.SVA.Model.PasswordRule password = new Edge.SVA.BLL.PasswordRule().GetModel(Model.PasswordRuleID.GetValueOrDefault());
                string passwordText = password == null ? "" : DALTool.GetStringByCulture(password.Name1, password.Name2, password.Name3);
                this.PasswordRuleID.Text = password == null ? "" : ControlTool.GetDropdownListText(passwordText, password.PasswordRuleCode);

                Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(Model.CampaignID.GetValueOrDefault());
                string campaignText = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                this.CampaignID.Text = campaign == null ? "" : ControlTool.GetDropdownListText(campaignText, campaign.CampaignCode);

                this.CardPointToAmountRate.Text = ((int)Model.CardPointToAmountRate.GetValueOrDefault()).ToString("N00");       //todo: 数据库类型不正确
                this.CardGradeDiscCeiling.Text = ((int)(Model.CardGradeDiscCeiling.GetValueOrDefault() * 100)).ToString();

                this.CardCheckdigitView.Text = this.CardCheckdigit.SelectedItem == null ? "" : this.CardCheckdigit.SelectedItem.Text;
                this.CardCheckdigit.Visible = false;

                this.IsAllowStoreValueView.Text = this.IsAllowStoreValue.SelectedItem == null ? "" : this.IsAllowStoreValue.SelectedItem.Text;
                this.IsAllowStoreValue.Visible = false;

                this.IsAllowConsumptionPointView.Text = this.IsAllowConsumptionPoint.SelectedItem == null ? "" : this.IsAllowConsumptionPoint.SelectedItem.Text;
                this.IsAllowConsumptionPoint.Visible = false;

                this.ActiveResetExpiryDateView.Text = this.ActiveResetExpiryDate.SelectedItem == null ? "" : this.ActiveResetExpiryDate.SelectedItem.Text;
                this.ActiveResetExpiryDate.Visible = false;

                this.ForfeitAmountAfterExpiredView.Text = this.ForfeitAmountAfterExpired.SelectedItem == null ? "" : this.ForfeitAmountAfterExpired.SelectedItem.Text;
                this.ForfeitAmountAfterExpired.Visible = false;

                this.ForfeitPointAfterExpiredView.Text = this.ForfeitPointAfterExpired.SelectedItem == null ? "" : this.ForfeitPointAfterExpired.SelectedItem.Text;
                this.ForfeitPointAfterExpired.Visible = false;

                this.CardGradeUpdMethodView.Text = this.CardGradeUpdMethod.SelectedItem == null ? "" : this.CardGradeUpdMethod.SelectedItem.Text;
                this.CardGradeUpdMethod.Visible = false;

                this.CardValidityUnitView.Text = this.CardValidityUnit.SelectedItem == null ? "" : this.CardValidityUnit.SelectedItem.Text;
                this.CardValidityUnit.Visible = false;

                this.GracePeriodUnitView.Text = this.GracePeriodUnit.SelectedItem == null ? "" : this.GracePeriodUnit.SelectedItem.Text;
                this.GracePeriodUnit.Visible = false;

                this.CardPointTransferView.Text = this.CardPointTransfer.SelectedItem == null ? "" : this.CardPointTransfer.SelectedItem.Text;
                this.CardPointTransfer.Visible = false;

                this.CardAmountTransferView.Text = this.CardAmountTransfer.SelectedItem == null ? "" : this.CardAmountTransfer.SelectedItem.Text;
                this.CardAmountTransfer.Visible = false;


                this.IsImportUIDNumberView.Text = this.IsImportUIDNumber.SelectedItem == null ? "" : this.IsImportUIDNumber.SelectedItem.Text;
                this.IsImportUIDNumber.Visible = false;

                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.UIDCheckDigit.Visible = false;

                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CheckDigitModeID.Visible = false;


                // Add by Alex 2014-07-01 ++
                this.AllowOfflineQRCodeView.Text = this.AllowOfflineQRCode.SelectedItem == null ? "" : this.AllowOfflineQRCode.SelectedItem.Text;
                this.AllowOfflineQRCode.Visible = false;

                this.QRCodePrefixView.Text = this.QRCodePrefix.SelectedItem == null ? "" : this.QRCodePrefix.SelectedItem.Text;
                this.QRCodePrefix.Visible = false;

                this.TrainingModeView.Text = this.TrainingMode.SelectedItem == null ? "" : this.TrainingMode.SelectedItem.Text;
                this.TrainingMode.Visible = false;

                //Add by Alex 2014-07-01 --

                this.CardNumberToUIDView.Text = this.CardNumberToUID.SelectedItem == null ? "" : this.CardNumberToUID.SelectedItem.Text;
                this.CardNumberToUID.Visible = false;

                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.IsConsecutiveUID.Visible = false;

                this.UIDToCardNumberView.Text = this.UIDToCardNumber.SelectedItem == null ? "" : this.UIDToCardNumber.SelectedItem.Text;
                this.UIDToCardNumber.Visible = false;

                this.CardTypeIDView.Text = this.CardTypeID.SelectedText;
                this.PasswordRuleIDView.Text = this.PasswordRuleID.SelectedText;
                this.CampaignIDView.Text = this.CampaignID.SelectedText;

                //新增字段CardGradeUpdCouponTypeID 
                this.CardGradeUpdCouponTypeIDView.Text = this.CardGradeUpdCouponTypeID.SelectedText;

                //string type = this.IsImportUIDNumber.SelectedValue.Trim();
                //if (type == "0")
                //{
                //    this.IsConsecutiveUID.Visible = false;
                //    this.IsConsecutiveUIDView.Visible = false;  
                //    this.UIDCheckDigit.Visible = false;
                //    this.UIDCheckDigitView.Visible = false;
                //    this.UIDToCardNumber.Visible = false;
                //    this.UIDToCardNumberView.Visible = false;
                //}
                //else
                //{
                //    this.CardCheckdigit.Visible = false;
                //    this.CardCheckdigitView.Visible = false;
                //    this.CheckDigitModeID.Visible = false;
                //    this.CheckDigitModeIDView.Visible = false;
                //    this.CardNumberToUID.Visible = false;
                //    this.CardNumberToUIDView.Visible = false;
                //    this.CardNumMask.Visible = false;
                //    this.CardNumPattern.Visible = false;
                //}


                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }
                if (Model != null)
                {
                    this.uploadFilePath.Text = Model.CardGradePicFile;
                    this.uploadFilePath1.Text = Model.CardGradeLayoutFile;
                    this.uploadFilePath2.Text = Model.CardGradeStatementFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    //this.btnPreview1.OnClientClick = WindowPic.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.uploadFilePath1.Text, "图片");
                    //this.btnPreview2.OnClientClick = WindowPic.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.uploadFilePath2.Text, "图片");
                    

                    //this.CardGradeNotes.Text = this.CardGradeNotes.Text.Replace("<br />", "\r\n");
                    //this.CardGradeNotes.Text = Model.CardGradeNotes;

                    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.CardGradePicFile) ? true : false;//没有卡图片时不显示查看按钮(Len)
                    this.btnExport.Hidden = string.IsNullOrEmpty(Model.CardGradeLayoutFile) ? true : false;//没有卡设计模板时不显示查看按钮(Len)
                    this.btnExport1.Hidden = string.IsNullOrEmpty(Model.CardGradeStatementFile) ? true : false;//没有卡结算模板时不显示查看按钮(Len)

                    controller.LoadViewModel(Model.CardGradeID, SVASessionInfo.SiteLanguage);

                    BindingDataGrid_CanHoldCouponGradeList();
                    BindingDataGrid_GetCouponGradeList();

                    this.MemberClauseDesc1.Text = controller.ViewModel.MemberClause.MemberClauseDesc1;
                    this.MemberClauseDesc2.Text = controller.ViewModel.MemberClause.MemberClauseDesc2;
                    this.MemberClauseDesc3.Text = controller.ViewModel.MemberClause.MemberClauseDesc3;

                }
            }
        }



        private void CardExtensionRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);
            this.rptExtensionRuleList.DataSource = ds.Tables[0].DefaultView;
            this.rptExtensionRuleList.DataBind();
        }


        private void CardPointRuleRptBind(string strWhere)
        {
            try
            {
                Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();
                DataSet ds = new DataSet();
                ds = bll.GetList(strWhere);
                this.rptEarnAmountPointList.DataSource = ds.Tables[0].DefaultView;
                this.rptEarnAmountPointList.DataBind();
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

        #region CardGradeHoldCouponType

        private void BindingDataGrid_CanHoldCouponGradeList()
        {
            this.Grid_CanHoldCouponGradeList.RecordCount = controller.ViewModel.CanHoldCouponGradeList.Count;
            this.Grid_CanHoldCouponGradeList.DataSource = controller.ViewModel.CanHoldCouponGradeList;
            this.Grid_CanHoldCouponGradeList.DataBind();
        }
        protected void Grid_CanHoldCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_CanHoldCouponGradeList.PageIndex = e.NewPageIndex;

        }
        private void BindingDataGrid_GetCouponGradeList()
        {
            this.Grid_GetCouponGradeList.RecordCount = controller.ViewModel.GetCouponGradeList.Count;
            this.Grid_GetCouponGradeList.DataSource = controller.ViewModel.GetCouponGradeList;
            this.Grid_GetCouponGradeList.DataBind();
        }
        protected void Grid_GetCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_GetCouponGradeList.PageIndex = e.NewPageIndex;
        }
        #endregion

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath1.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CardGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("CardCaede Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CardGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnExport1_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath2.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CardCaede download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CardCaede download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }


        //private void CardExtensionRuleRptBind(string strWhere)
        //{
        //    Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

        //    DataSet ds = new DataSet();
        //    ds = bll.GetList(strWhere);

        //    this.rptExtensionRuleList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptExtensionRuleList.DataBind();
        //}

        //private void CardPointRuleRptBind(string strWhere)
        //{

        //    Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();


        //    DataSet ds = new DataSet();
        //    ds = bll.GetList(strWhere);

        //    this.rptPointRuleList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptPointRuleList.DataBind();
        //}

        //protected override void SetObject()
        //{
        //    foreach (System.Web.UI.Control con in this.Panel1.Controls)
        //    {
        //        if (con is FineUI.GroupPanel)
        //        {
        //            base.SetObject(Model, con.Controls.GetEnumerator());
        //        }
        //    }
        //}
    }
}
