﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model.Domain.File;
using Edge.Utils.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CouponGrade
{
    public partial class Add : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["RuleType"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.RuleType.Text = Request["RuleType"].ToString();
                }
                if (Request["PageFlag"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.PageFlag.Text = Request["PageFlag"].ToString();
                }
                RegisterCloseEvent(btnClose);
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                BrandID_SelectedIndexChanged(null, null);
            }
        }
        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(CouponTypeID, string.Format("BrandID = {0} order by CouponTypeCode", brandID));
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            CardGradeController controller;
            if (this.PageFlag.Text.Trim() == "0")
            {
                controller = SVASessionInfo.CardGradeAddController;
            }
            else
            {
                controller = SVASessionInfo.CardGradeModifyController;
            }
            CardGradeHoldCouponRuleViewModel model = new CardGradeHoldCouponRuleViewModel();
            model.MainTable.RuleType = StringHelper.ConvertToInt(this.RuleType.Text);
            model.MainTable.HoldCount = StringHelper.ConvertToInt(this.HoldCount.Text);
            model.MainTable.CouponTypeID = StringHelper.ConvertToInt(this.CouponTypeID.SelectedValue);
            if (model.MainTable.CouponTypeID<=0)
            {
                ShowWarning(" CouopnType must has value!");
                    return;
            }
            if (model.MainTable.RuleType == 0)
            {
                if (!controller.ValidateIfCanAddCanHoldCouponGradeListItem(model))
                {
                    ShowWarning("Exist the same coupon type!");
                       return;
                }
                controller.AddCanHoldCouponGradeListItem(model);
            }
            else if (model.MainTable.RuleType == 1)
            {
                if (!controller.ValidateIfCanAddGetCouponGradeListItem(model))
                {
                    ShowWarning("Exist the same coupon type!");
                    return;
                }
                controller.AddGetCouponGradeListItem(model);
            }
            else if (model.MainTable.RuleType == 2)
            {
                if (!controller.ValidateIfCanAddCanHoldCouponGradeListItem(model))
                {
                    ShowWarning("Exist the same coupon type!");
                    return;
                }
                controller.AddCanHoldCouponGradeListItem(model);
            }


            CloseAndPostBack();
        }

    }
}