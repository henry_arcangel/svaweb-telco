﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Store
{
    public partial class List : PageBase
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        public int cardGradeID;
        public int storeConditionTypeID;
        public int conditionTypeID;
        public int type;
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;
            ViewState["cardGradeID"] = this.cardGradeID = Request.Params["CardGradeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString());
            ViewState["storeConditionTypeID"] = this.storeConditionTypeID = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
            ViewState["conditionTypeID"] = this.conditionTypeID = 3;
            ViewState["type"] = this.type=Request.Params["type"] == null ? 1 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["type"].ToString());

            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");
                //this.lbtnDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";

                int cardGradeID = Convert.ToInt32(ViewState["cardGradeID"]);
                if (cardGradeID > 0)
                {
                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CardGradeID={0}", cardGradeID));
                    sbWhere.Append(string.Format(" and StoreConditionType={0}", ViewState["storeConditionTypeID"]));
                    sbWhere.Append(string.Format(" and ConditionType={0}", ViewState["conditionTypeID"]));

                    RptBind(sbWhere.ToString(), "CardGradeStoreConditionID");
                }

                logger.WriteOperationLog(this.PageName, "List");
                btnNew.OnClientClick = Window1.GetShowReference(string.Format("add.aspx?CardGradeID={0}&StoreConditionTypeID={1}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"]), "新增");
                btnClose.OnClientClick = FineUI.ActiveWindow.GetHideReference();

                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                if (ViewState["type"].ToString() == "1")
                {
                    this.btnDelete.Visible = false;
                    this.btnNew.Visible = false;
                }
                else
                {
                    this.btnDelete.Visible = true;
                    this.btnNew.Visible = true;
                }
                //if (ViewState["type"].ToString() == "1")
                //{
                //    this.lbtnDel.Visible = false;
                //    this.lbtnAdd.Visible = false;
                //}
                //else
                //{
                //    this.lbtnDel.Visible = true;
                //    this.lbtnAdd.Visible = true;
                //}
            }
        }


        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(this.StrWhere, "CardGradeStoreConditionID");

        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind(this.StrWhere, "CardGradeStoreConditionID");
        }
        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CardGradeStoreCondition bll = new Edge.SVA.BLL.CardGradeStoreCondition();
            //if (!int.TryParse(Request.Params["page"] as string, out this.page))
            //{
            //    this.page = 0;
            //}
            //获得总条数
            //this.pcount = bll.GetCount(strWhere);
            //if (this.pcount > 0)
            //{
            //    this.btnDelete.Enabled = true;
            //}
            //else
            //{
            //    this.btnDelete.Enabled = false;
            //}
            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);
            if (this.Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
            Edge.Web.Tools.DataTool.AddStoreName(ds, "StoreName", "ConditionID");
            Edge.Web.Tools.DataTool.AddStoreCode(ds, "StoreCode", "ConditionID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }

            logger.WriteOperationLog(this.PageName, "Delete " + sb.ToString());
            ExecuteJS(HiddenWindowForm.GetShowReference(string.Format("Delete.aspx?CardGradeID={0}&StoreConditionTypeID={1}&ids={2}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"], sb.ToString())));

        }

        //protected void lbtnDel_Click(object sender, EventArgs e)
        //{
        //    string ids = "";
        //    for (int i = 0; i < rptList.Items.Count; i++)
        //    {
        //        int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
        //        CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
        //        if (cb.Checked)
        //        {
        //            ids += string.Format("{0};", id.ToString());
        //        }
        //    }
        //    if (string.IsNullOrEmpty(ids))
        //    {
        //        JscriptPrint(Resources.MessageTips.NotSelected, "", Resources.MessageTips.WARNING_TITLE);
        //        return;
        //    }
        //    //Response.Redirect("Delete.aspx?ids=" + ids);
        //    logger.WriteOperationLog(this.PageName, "Delete " + ids);
        //    FineUI.PageContext.Redirect(string.Format("Delete.aspx?CardGradeID={0}&StoreConditionTypeID={1}&ids={2}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"], ids));
        //}

        //protected void lbtnAdd_Click(object sender, EventArgs e)
        //{
        //    logger.WriteOperationLog(this.PageName, "Add");
        //    ViewState["cardGradeID"] = Request.Params["CardGradeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString());
        //    ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
        //    Response.Redirect(string.Format("add.aspx?CardGradeID={0}&StoreConditionTypeID={1}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"]));
        //}

        #region 页面属性

        public int CardGradeID
        {
            get { return Request.Params["CardGradeID"] == null ? 0 : Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString()); }
        }

        public int StoreConditionTypeID
        {
            get { return Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString()); }
        }

        public int ConditionTypeID
        {
            get { return 3; }
        }

        public int Type
        {
            get { return Request.Params["type"] == null ? 1 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["type"].ToString()); }
        }

        public string StrWhere
        {
            get
            {
                StringBuilder sbWhere = new StringBuilder();
                sbWhere.Append(string.Format(" CardGradeID={0}", this.CardGradeID));
                sbWhere.Append(string.Format(" and StoreConditionType={0}", this.StoreConditionTypeID));
                sbWhere.Append(string.Format(" and ConditionType={0}", this.ConditionTypeID));

                return sbWhere.ToString();
            }

        }
        #endregion
    }
}
