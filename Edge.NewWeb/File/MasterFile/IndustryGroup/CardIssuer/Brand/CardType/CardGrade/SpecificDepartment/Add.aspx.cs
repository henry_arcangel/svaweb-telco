﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.SpecificDepartment
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                ViewState["cardGradeID"] = Request.Params["CardGradeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString());

                RegisterCloseEvent(btnClose);

                RptBind();
                this.Grid1.EnableRowDoubleClick = true;
                this.Grid1.EnableCheckBoxSelect = false;
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int cardGradeID = Edge.Web.Tools.ConvertTool.ToInt(ViewState["cardGradeID"].ToString());
            logger.WriteOperationLog(this.PageName, "Add cardGradeID " + cardGradeID.ToString());
            if (cardGradeID > 0)
            {


                Edge.SVA.Model.CardGradeExchangeBinding model = new Edge.SVA.Model.CardGradeExchangeBinding();
                model.BindingType = 2;
                model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                model.DepartCode = DepartCode.Text.Trim();
                model.CardGradeID = cardGradeID;

                Edge.SVA.BLL.CardGradeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CardGradeExchangeBinding();

                //验证PLU不能在同一个Brand下
                //if (bllExchangeBinding.GetCount(string.Format("BindingType=1 and BrandID={0} and CouponTypeID={1} ", Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue), couponTypeID)) > 0)
                //{
                //    this.ShowWarning(Resources.MessageTips.ExistBrandCode);
                //    return;
                //}
                // Add by Alex 2014-06-20 ++
                if (DepartCode.Text.Trim() == "")
                {
                    this.ShowWarning(Resources.MessageTips.DepartmentCodeEmpty);
                    return;
                }
                if (model.BrandID == -1)
                {
                    this.ShowWarning(Resources.MessageTips.BrandCodeEmpty);
                    return;
                }
                // Add by Alex 2014-06-20 --
                if (bllExchangeBinding.GetRecordCount(string.Format("BindingType=2 and BrandID={0} and DepartCode='{1}' and CardGradeID={2} ", Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue), this.DepartCode.Text.Trim(), cardGradeID)) > 0)
                {
                    this.ShowWarning(Resources.MessageTips.Exists);
                    return;
                }

                if (bllExchangeBinding.Add(model) > 0)
                {
                    //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
                    CloseAndRefresh();
                }
                else
                {
                    //this.ShowWarming(Resources.MessageTips.AddFailed);
                    ShowAddFailed();
                }
            }
            else
            {
                //this.ShowWarming(Resources.MessageTips.AddFailed);

            }
        }

        protected void RptBind()
        {
            Edge.SVA.BLL.Department bll = new SVA.BLL.Department();
            DataSet ds = bll.GetList("");
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(ds.Tables[0], this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.PageIndex = 0;
                this.Grid1.RecordCount = 0;
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind();
        }

        protected void Grid1_OnRowDoubleClick(object sender, FineUI.GridRowClickEventArgs e)
        {
            this.DepartCode.Text = this.Grid1.DataKeys[e.RowIndex][0].ToString();
        }
    }
}