﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.EarnCouponRules
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    SVASessionInfo.Tabs = Add.tab;
                    if (string.IsNullOrEmpty(ids))
                    {
                        //Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, "../Modify.aspx?id=" + Request.Params["id"] + "&tabs=3");
                        Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        return;
                    }
                    logger.WriteOperationLog(this.PageName, "Delete " + ids);
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrEmpty(id)) continue;
                        Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.PointRule>(Tools.ConvertTool.ToInt(id));
                    }
                    Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", MessageBoxIcon.Information, ActiveWindow.GetHideRefreshReference());
                    //Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", MessageBoxIcon.Information, "../Modify.aspx?id=" + Request.Params["id"] + "&tabs=3");
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}
