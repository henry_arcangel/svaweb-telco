﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.MemberReward
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.SVARewardRules, Edge.SVA.Model.SVARewardRules>
    {
        public const string tab = "3";
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                string cardGradeId = Request.Params["CardGradeID"];
                this.CardGradeID.Text = cardGradeId;
                Edge.Web.Tools.ControlTool.BindCouponType(this.RewardCouponTypeID, "IsImportCouponNumber = 0  order by CouponTypeCode");

                ControlTool.BindKeyValueList(this.SVARewardType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.SVARewardType), true);
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            Edge.SVA.Model.SVARewardRules item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;

                item.RewardCouponTypeID = this.RewardCouponTypeID.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.RewardCouponTypeID.SelectedValue);
            }
            if (IsExistsRuleNumber(this.SVARewardRulesCode.Text))
            {
                ShowWarning(Edge.Messages.Manager.MessagesTool.instance.GetMessage("90487"));
                return;
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.SVARewardRules>(item) > 0)
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void RewardCouponTypeID_SelectedChanged(object sender, EventArgs e)
        {
            if (this.RewardCouponTypeID.SelectedValue != "-1")
            {
                this.RewardCouponCount.Enabled = true;
            }
            else
            {
                this.RewardCouponCount.Enabled = false;
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + Request.Params["CardGradeID"] + "&tabs="+Add.tab);
        }

        protected bool IsExistsRuleNumber(string strRulesCode)
        {
            Edge.SVA.BLL.SVARewardRules bll = new SVA.BLL.SVARewardRules();
            return bll.GetModelList("SVARewardRulesCode='" + strRulesCode + "'").Count > 0 ? true : false;
        }
    }
}
