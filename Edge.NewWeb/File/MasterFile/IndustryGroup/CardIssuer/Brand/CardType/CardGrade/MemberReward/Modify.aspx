﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.MemberReward.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--   <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" 
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:HiddenField ID="SVARewardRulesID" runat="Server"></ext:HiddenField>
             <ext:HiddenField ID="CardGradeID" runat="Server" ></ext:HiddenField>
             <ext:TextBox ID="SVARewardRulesCode" runat="Server" Label="规则编号：" Enabled="false"></ext:TextBox>
             <ext:DropDownList ID="SVARewardType" runat="server" Label="奖励类型：" ShowRedStar="true" Required="true" Resizable="true">
            </ext:DropDownList>
            <ext:NumberBox ID="RewardPoint" runat="server" Label="奖励积分：" NoDecimal="true" Text="0" MaxValue="99999999" MinValue="0">
            </ext:NumberBox>
            <ext:NumberBox ID="RewardAmount" runat="server" Label="增值金额：" NoDecimal="false" Text="0" MaxValue="1000000000" MinValue="0">
            </ext:NumberBox>
            <ext:DropDownList ID="RewardCouponTypeID" runat="server" Label="奖励优惠券类型：" 
                ShowRedStar="true" Required="true" Resizable="true"
                OnSelectedIndexChanged="RewardCouponTypeID_SelectedChanged" AutoPostBack="true">
            </ext:DropDownList>
            <ext:NumberBox ID="RewardCouponCount" runat="server" Label="优惠券数量：" ShowRedStar="true"
                Required="true" NoDecimal="true">
            </ext:NumberBox>
            <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于等于开始日期"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
