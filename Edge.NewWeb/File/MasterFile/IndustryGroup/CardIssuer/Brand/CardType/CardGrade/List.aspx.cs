﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class List : PageBase
    {

        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {         
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");
                //this.lbtnDel.OnClientClick = "return checkAndConfirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                RptBind("CardGradeID>0", "CardGradeCode");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                ControlTool.BindBrand(this.Brand);
                ControlTool.BindCardType(this.CardType);
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    string str = "";
                    int brandid = this.Brand.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.Brand.SelectedValue);
                    int cardtypeid = this.CardType.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.CardType.SelectedValue);
                    string code = this.Code.Text.Trim();
                    string desc = this.Desc.Text.Trim();
                    Edge.SVA.BLL.CardType blltype = new SVA.BLL.CardType();
                    if (brandid > 0)
                    {
                        List<int> list = blltype.GetCardTypes(brandid);
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        if (list.Count >= 1)
                        {
                            sb.Append(" CardTypeID in (");
                            foreach (var item in list)
                            {
                                str += item + ",";
                            }
                            str = str.TrimEnd(',');
                            sb.Append(str);
                            sb.Append(")");
                        }
                        else
                        {
                            sb.Append(" CardTypeID = 0");
                        }
                    }
                    if (cardtypeid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardTypeID =");
                        sb.Append(cardtypeid);
                        sb.Append("");
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardGradeCode like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(desc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CardGradeName1";

                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(desc);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();

                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);
                //if (this.Grid1.RecordCount > 0)
                //{
                //    this.btnDelete.Enabled = true;
                //}
                //else
                //{
                //    this.btnDelete.Enabled = false;
                //}
                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

                //AddNames(ds);
                //Tools.DataTool.AddBrandCodeByCardType(ds, "BrandCode", "CardTypeID");
                //Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");

                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();



                //ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));
                //Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
                //Edge.SVA.BLL.CardIssuer bllCardIssuer = new Edge.SVA.BLL.CardIssuer();

                //foreach (DataRow row in ds.Tables[0].Rows)
                //{
                //    Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(int.Parse(row["BrandID"].ToString()));
                //    if (mBrand != null)
                //    {
                //        Edge.SVA.Model.CardIssuer mCardIssuer = bllCardIssuer.GetModel(mBrand.CardIssuerID.GetValueOrDefault());
                //        if (mCardIssuer != null) row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetStringByCulture(mCardIssuer.CardIssuerName1, mCardIssuer.CardIssuerName2, mCardIssuer.CardIssuerName3);
                //    }

                //}

                CardGradeController controller = new CardGradeController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("CardGrade", "Load error", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CardGradeController controller = new CardGradeController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        private static void AddNames(DataSet ds)
        {
            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");

            ds.Tables[0].Columns.Add(new DataColumn("BrandName", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));

            Edge.SVA.BLL.CardType bllCardType = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
            Dictionary<int, string> brandCache = new Dictionary<int, string>();
            Dictionary<int, string> cardIssuerCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Edge.SVA.Model.CardType mCardType = bllCardType.GetModel(int.Parse(row["CardTypeID"].ToString()));
                row["BrandName"] = Edge.Web.Tools.DALTool.GetBrandName(mCardType.BrandID, brandCache);

                Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(mCardType.BrandID);
                row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetCardIssuerName(mBrand.CardIssuerID.GetValueOrDefault(), cardIssuerCache);
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CardGradeID>0", "CardGradeCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("CardGradeID>0", "CardGradeCode");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("CardGradeID>0", "CardGradeCode");
        }
    }
}
