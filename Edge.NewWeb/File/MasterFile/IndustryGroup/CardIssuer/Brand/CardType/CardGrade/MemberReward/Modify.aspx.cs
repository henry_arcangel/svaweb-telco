﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.MemberReward
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.SVARewardRules,Edge.SVA.Model.SVARewardRules>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCouponType(this.RewardCouponTypeID, "IsImportCouponNumber = 0  order by CouponTypeCode");

                ControlTool.BindKeyValueList(this.SVARewardType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.SVARewardType), true);
            }

        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                if (this.RewardCouponTypeID.SelectedValue != "-1")
                {
                    this.RewardCouponCount.Enabled = true;
                }
                else
                {
                    this.RewardCouponCount.Enabled = false;
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.SVARewardRules item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;

                item.RewardCouponTypeID = this.RewardCouponTypeID.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.RewardCouponTypeID.SelectedValue);
            }

            if (DALTool.Update<Edge.SVA.BLL.SVARewardRules>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                //CloseAndPostBack();
                CloseAndRefresh();
            }
            else
            {
                ShowUpdateFailed();
            }

        }

        protected void RewardCouponTypeID_SelectedChanged(object sender, EventArgs e)
        {
            if (this.RewardCouponTypeID.SelectedValue != "-1")
            {
                this.RewardCouponCount.Enabled = true;
            }
            else
            {
                this.RewardCouponCount.Enabled = false;
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + this.CardGradeID.Text + "&tabs=" + Add.tab);
        }
    }
}
