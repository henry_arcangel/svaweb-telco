﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign, Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CampaignAddController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Tools.ControlTool.BindBrand(BrandID);
                RegisterCloseEvent(btnClose);

                this.Window1.Title = "新增";
                SVASessionInfo.CampaignAddController = null;

                controller = SVASessionInfo.CampaignAddController;
                btnDelete.OnClientClick = this.Grid_CardGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                btnDelete1.OnClientClick = this.Grid_CouponTypeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete1.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete1.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
                BindingDataGrid_CardGradeList();
                BindingDataGrid_CouponTypeList();
            }
            controller = SVASessionInfo.CampaignAddController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasCampaignCode(this.CampaignCode.Text.Trim(), 0))
            {
                Alert.ShowInTop(Resources.MessageTips.ExistCampaignCode, MessageBoxIcon.Warning);
                return;
            }

            Edge.SVA.Model.Campaign item = this.GetAddObject();
            if (item != null)
            {
                //校验图片类型
                if (!ValidateImg(this.CampaignPicFile.FileName))
                {
                    return;
                }

                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CampaignCode = item.CampaignCode.ToUpper();
                item.CampaignPicFile = this.CampaignPicFile.SaveToServer("Campaign");
            }

            //int id = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Campaign>(item);
            //if (id > 0)
            //{
            //    controller.ViewModel.MainTable.CampaignID = id;

            //    ExecResult er = controller.Submit();
            //    if (er.Success)
            //    {
            //        // 2. 关闭本窗体，然后刷新父窗体
            //        CloseAndRefresh();
            //    }
            //    else
            //    {
            //        logger.WriteErrorLog("Campaign ", " Add error", er.Ex);
            //        ShowAddFailed();
            //    }
            //}
            //else
            //{
            //    ShowAddFailed();
            //}
            controller.ViewModel.MainTable = this.GetAddObject();
            ExecResult er = controller.Save();
            if (er.Success)
            {
                //2. 关闭本窗体，然后刷新父窗体
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        #region CardGrade CouponType
        protected void BrandID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            controller.ViewModel.CardGradeList.Clear();
            controller.ViewModel.CouponTypeList.Clear();
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();
        }

        protected void btnNew_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference(string.Format("CardGrade/Add.aspx?BrandID={0}&PageFlag=0", this.BrandID.SelectedValue)));
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CardGradeList.SelectedRowIndexArray;
            int count = this.Grid_CardGradeList.PageIndex * this.Grid_CardGradeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CardGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCardGrade(list[i]);
            }
            BindingDataGrid_CardGradeList();
        }
        protected void btnNew1_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference(string.Format("CouponType/Add.aspx?BrandID={0}&PageFlag=0",this.BrandID.SelectedValue)));
        }
        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CouponTypeList.SelectedRowIndexArray;
            int count = this.Grid_CouponTypeList.PageIndex * this.Grid_CouponTypeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CouponTypeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCouponType(list[i]);
            }
            BindingDataGrid_CouponTypeList();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();

            //图片处理
            DeleteFile(this.uploadFilePath.Text);
        }

        private void BindingDataGrid_CardGradeList()
        {
            this.Grid_CardGradeList.RecordCount = controller.ViewModel.CardGradeList.Count;
            this.Grid_CardGradeList.DataSource = controller.ViewModel.CardGradeList;
            this.Grid_CardGradeList.DataBind();
        }
        private void BindingDataGrid_CouponTypeList()
        {
            this.Grid_CouponTypeList.RecordCount = controller.ViewModel.CouponTypeList.Count;
            this.Grid_CouponTypeList.DataSource = controller.ViewModel.CouponTypeList;
            this.Grid_CouponTypeList.DataBind();
        }
        #endregion

        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CampaignPicFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.CampaignPicFile.SaveToServer("Campaign");
                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }
        #endregion

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
