﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class List : PageBase
    {
        //public int pcount;                      //总条数
        //public int page;                        //当前页
        //public int pagesize;                    //设置每页显示的大小
        //Tools.Logger logger = Tools.Logger.Instance;
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.pagesize = webset.ContentPageNum;

        //    if (!Page.IsPostBack)
        //    {
        //        logger.WriteOperationLog(this.PageName, "List");
        //        this.lbtnDel.OnClientClick = "return checkAndConfirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
        //        RptBind("CampaignID>0", "CampaignCode");
        //    }

        //}



        //#region 数据列表绑定
        //private void RptBind(string strWhere, string orderby)
        //{
        //    if (!int.TryParse(Request.Params["page"] as string, out this.page))
        //    {
        //        this.page = 0;
        //    }

        //    Edge.SVA.BLL.Campaign bll = new Edge.SVA.BLL.Campaign();


        //    //获得总条数
        //    this.pcount = bll.GetCount(strWhere);
        //    if (this.pcount > 0)
        //    {
        //        this.lbtnDel.Enabled = true;
        //    }
        //    else
        //    {
        //        this.lbtnDel.Enabled = false;
        //    }

        //    DataSet ds = new DataSet();
        //    ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
        //    Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
        //    Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
        //    Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}
        //#endregion

        //protected void lbtnDel_Click(object sender, EventArgs e)
        //{

        //    string ids = "";
        //    for (int i = 0; i < rptList.Items.Count; i++)
        //    {
        //        int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
        //        CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
        //        if (cb.Checked)
        //        {
        //            ids += string.Format("{0};", id.ToString());
        //        }
        //    }
        //    Response.Redirect("Delete.aspx?ids=" + ids);

        //}

        //protected void lbtnAdd_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("add.aspx");
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "List");

                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind("CampaignID>0", "CampaignCode");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
        }

        #region Event

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string code = this.Code.Text.Trim();
                    string desc = this.Desc.Text.Trim();
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CampaignCode like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(desc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CampaignName1";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(desc);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.Campaign bll = new Edge.SVA.BLL.Campaign();

                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);
                //if (this.Grid1.RecordCount > 0)
                //{
                //    this.btnDelete.Enabled = true;
                //}
                //else
                //{
                //    this.btnDelete.Enabled = false;
                //}

                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);
                //Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
                //Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();

                CampaignController controller = new CampaignController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch(Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Campaign", "Load Faild", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CampaignController controller = new CampaignController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CampaignID>0", "CampaignCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("CampaignID>0", "CampaignCode");
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("CampaignID>0", "CampaignCode");
        }
    }
}
