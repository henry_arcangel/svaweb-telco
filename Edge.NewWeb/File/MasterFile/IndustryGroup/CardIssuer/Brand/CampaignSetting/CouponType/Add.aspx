﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CouponType.Add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        runat="server" Text="添加" OnClick="btnSaveClose_Click">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:HiddenField ID="BrandID" runat="server">
            </ext:HiddenField>
            <ext:HiddenField ID="PageFlag" runat="server">
            </ext:HiddenField>
            <ext:Grid ID="Grid_CardGradeList" ForceFitAllTime="true" Title="优惠券类型列表" PageSize="10"
                ShowBorder="true" ShowHeader="true" AllowPaging="true" runat="server"
                EnableCheckBoxSelect="True" AutoWidth="true" Height="290" DataKeyNames="Key" OnPageIndexChange="RegisterGrid_OnPageIndexChange"
                EnableRowNumber="True" ClearSelectedRowsAfterPaging="false">
                <Columns>
                    <ext:BoundField ColumnID="Key" DataField="Key" Hidden="true" />
                    <ext:BoundField ColumnID="Value" DataField="Value" HeaderText="优惠券类型" />
                </Columns>
            </ext:Grid>
        </Items>
    </ext:SimpleForm>
    <ext:HiddenField ID="hfSelectedIDS" runat="server">
    </ext:HiddenField>
    </form>
</body>
</html>
