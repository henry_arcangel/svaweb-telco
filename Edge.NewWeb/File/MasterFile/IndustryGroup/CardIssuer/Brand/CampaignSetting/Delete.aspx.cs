﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CampaignDeleteController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                controller = SVASessionInfo.CampaignDeleteController;
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        return;
                    }
                    logger.WriteOperationLog(this.PageName, "Delete " + ids);
                    //foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    //{
                    //    if (string.IsNullOrEmpty(id)) continue;
                    //    string msg = "";
                    //    if (!Tools.DALTool.isCanDeleteCampaign(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                    //    {
                    //        Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                    //        return;
                    //    }
                    //    //Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.Campaign>(Tools.ConvertTool.ToInt(id));
                    //    controller.Delete(Tools.ConvertTool.ToInt(id));
                    //}
                    List<int> CampaignIDList = new List<int>();
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrEmpty(id)) continue;
                        string msg = "";
                        if (!Tools.DALTool.isCanDeleteCampaign(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                        {
                            Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                            return;
                        }
                        CampaignIDList.Add(Tools.ConvertTool.ToInt(id));
                    }
                    controller.Delete(CampaignIDList);

                    Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
            SVASessionInfo.CampaignDeleteController = null;
        }
    }
}
