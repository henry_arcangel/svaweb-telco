﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.Accounts;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.Model;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Brand, Edge.SVA.Model.Brand>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        BrandController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindIndustry(IndustryID);
                Edge.Web.Tools.ControlTool.BindCardIssuer(CardIssuerID);

                Edge.Web.Tools.ControlTool.BindCurrency(this.DomesticCurrencyID);

                RegisterCloseEvent(btnClose);
                SVASessionInfo.BrandController = null;
            }
            controller = SVASessionInfo.BrandController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                List<Edge.SVA.Model.CardIssuer> models =  new Edge.SVA.BLL.CardIssuer().GetModelList("");
                if (models == null || models.Count <= 0) return ;

               this.CardIssuerID.SelectedValue = models[0].CardIssuerID.ToString();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            if (string.IsNullOrEmpty(this.BrandDesc1.Text) || this.BrandDesc1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.BrandDesc1CannotBeEmpty);
                return;
            }
            string message = controller.ValidataObject(this.BrandCode.Text.Trim(), 0);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                this.BrandCode.Focus();
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                //校验图片类型是否正确
                if (!ValidateImg(this.BrandPicSFile.FileName))
                {
                    return;
                }
                if (!ValidateImg(this.BrandPicMFile.FileName))
                {
                    return;
                }
                if (!ValidateImg(this.BrandPicGFile.FileName))
                {
                    return;
                }

                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.BrandCode = controller.ViewModel.MainTable.BrandCode.ToUpper();
                controller.ViewModel.MainTable.BrandPicSFile = this.BrandPicSFile.SaveToServer("Brand");
                controller.ViewModel.MainTable.BrandPicMFile = this.BrandPicMFile.SaveToServer("Brand");
                controller.ViewModel.MainTable.BrandPicGFile = this.BrandPicGFile.SaveToServer("Brand");
            }
            int brandID = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Brand>(controller.ViewModel.MainTable);
            if (brandID > 0)
            {
                User user = SVASessionInfo.CurrentUser;
                if (user.Style == 1)
                {
                    try
                    {
                        BrandInfo bi = new BrandInfo();
                        bi.Key = brandID.ToString();
                        user.BrandInfoList.Add(bi);
                        AccountController ac = new AccountController();
                        ac.UpdateRelation(user);
                        SessionInfo.BrandIDsStr = SVASessionInfo.CurrentUser.SqlConditionBrandIDs;
                        SessionInfo.StoreIDsStr = SVASessionInfo.CurrentUser.SqlConditionStoreIDs; //Add By Robin 2014-12-01 for 过滤用户可用店铺
                    }
                    catch (System.Exception ex)
                    {
                        logger.WriteErrorLog("Add Brand", "add error", ex);
                    }
                }
                BrandRepostory.Singleton.Refresh();
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }

            //if (Edge.Web.Tools.DALTool.isHasBrandCode(this.BrandCode.Text.Trim(), 0))
            //{
            //    ShowWarning(Resources.MessageTips.ExistBrandCode);
            //    this.BrandCode.Focus();
            //    return;
            //}
            //if (string.IsNullOrEmpty(this.BrandDesc1.Text) || this.BrandDesc1.Text == "<br>")
            //{
            //    ShowWarning(Resources.MessageTips.BrandDesc1CannotBeEmpty);
            //    return;
            //}
            //Edge.SVA.Model.Brand item = this.GetAddObject();
            //if (item != null)
            //{
            //    item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = System.DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.BrandCode = item.BrandCode.ToUpper();

            //    item.BrandPicSFile = this.BrandPicSFile.SaveToServer("Brand");
            //    item.BrandPicMFile = this.BrandPicMFile.SaveToServer("Brand");
            //    item.BrandPicGFile = this.BrandPicGFile.SaveToServer("Brand");
            //}
            //int brandID=Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Brand>(item) ;
            //if (brandID> 0)
            //{
            //    User user = SVASessionInfo.CurrentUser;
            //    if (user.Style==1)
            //    {
            //        try
            //        {
            //            BrandInfo bi = new BrandInfo();
            //            bi.Key = brandID.ToString();
            //            user.BrandInfoList.Add(bi);
            //            AccountController ac = new AccountController();
            //            ac.UpdateRelation(user);
            //            SessionInfo.BrandIDsStr = SVASessionInfo.CurrentUser.SqlConditionBrandIDs;
            //        }
            //        catch (System.Exception ex)
            //        {
            //            logger.WriteErrorLog("Add Brand","add error",ex);
            //        }
            //    }
            //    BrandRepostory.Singleton.Refresh();
            //    CloseAndRefresh();
            //}
            //else
            //{
            //    ShowAddFailed();
            //}
 
        
        }

        #region 图片处理
        protected void ViewPicture1(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicSFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.BrandPicSFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected void ViewPicture2(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicMFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.BrandPicMFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected void ViewPicture3(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicGFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.BrandPicGFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }
        #endregion

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
