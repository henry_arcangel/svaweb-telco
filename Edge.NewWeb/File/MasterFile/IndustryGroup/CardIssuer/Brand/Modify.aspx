﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="BrandCode" runat="server" Label="品牌编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true" ToolTipTitle="品牌编号" ToolTip="Translate__Special_121_StartKey identifier of brand.1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"
                Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="BrandName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="描述" ToolTip="请输入规范的品牌名称。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="BrandName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                ToolTip="對品牌的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="BrandName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                ToolTip="對品牌的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:HtmlEditor ID="BrandDesc1" runat="server" Height="100" Label="说明1：" ShowRedStar="true" ></ext:HtmlEditor>
            <ext:HtmlEditor ID="BrandDesc2" runat="server" Height="100" Label="说明2："></ext:HtmlEditor>
            <ext:HtmlEditor ID="BrandDesc3" runat="server" Height="100" Label="说明3："></ext:HtmlEditor>
            <ext:DropDownList ID="CardIssuerID" runat="server" Label="发行商：" Required="true" ShowRedStar="true"
                EnableEdit="false"  Enabled="false" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:DropDownList ID="IndustryID" runat="server" Label="行业：" Required="true" ShowRedStar="true"
                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:DropDownList ID="DomesticCurrencyID" runat="server" Label="本币：" Resizable="true">
            </ext:DropDownList>
            <ext:HiddenField ID="PicturePath" runat="server"></ext:HiddenField>
            <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%" runat="server">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="BrandPicSFile" runat="server" Label="小图片：" ToolTipTitle="小图片"
                                ToolTip="Translate__Special_121_Start品牌標誌的圖標(小圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack1" runat="server" Text="返回" HideMode="Display" Hidden="true" OnClick="btnBack1_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="69% 11% 20%" runat="server">
                        <Items>
                        <ext:Label ID="Label1" runat="server" Label="小图片："></ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad1" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad1_Click"> 
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>

            <ext:Form ID="FormLoad1" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%" runat="server">
                        <Items>
                            <ext:Label ID="uploadFilePath1" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="BrandPicMFile" runat="server" Label="中图片：" ToolTipTitle="中图片"
                                ToolTip="Translate__Special_121_Start品牌標誌的圖標(中圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack2" runat="server" Text="返回" HideMode="Display" Hidden="true" OnClick="btnBack2_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad1" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow2" ColumnWidths="69% 11% 20%" runat="server">
                        <Items>
                            <ext:Label ID="Label2" runat="server" Label="中图片："></ext:Label>
                            <ext:Button ID="btnPreview1" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad2" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad2_Click"> 
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>

            <ext:Form ID="FormLoad2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%" runat="server">
                        <Items>
                            <ext:Label ID="uploadFilePath2" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="BrandPicGFile" runat="server" Label="大图片：" ToolTipTitle="大图片"
                                ToolTip="Translate__Special_121_Start品牌標誌的圖標(大圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack3" runat="server" Text="返回" HideMode="Display" Hidden="true" OnClick="btnBack3_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad2" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow3" ColumnWidths="69% 11% 20%" runat="server">
                        <Items>
                            <ext:Label ID="Label3" runat="server" Label="大图片："></ext:Label>
                            <ext:Button ID="btnPreview2" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad3" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad3_Click"> 
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="WindowPicture" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px"> 
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
