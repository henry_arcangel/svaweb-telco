﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.TermsConditions
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberClause, Edge.SVA.Model.MemberClause>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindBrand(BrandID); 
            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
              
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.MemberClause item = this.GetUpdateObject();

            if (item != null)
            {
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.StoreAttributeID = item.StoreID;
                //item.StoreCode = item.StoreCode.ToUpper();
                item.BrandID = item.BrandID;
                item.ClauseTypeCode = item.ClauseTypeCode.ToUpper();
                item.ClauseSubCode = item.ClauseSubCode;
                item.ClauseName = item.ClauseName;
                item.ClauseName2 = item.ClauseName2;
                item.ClauseName3 = item.ClauseName3;
                item.MemberClauseDesc1 = item.MemberClauseDesc1;
                item.MemberClauseDesc2 = item.MemberClauseDesc2;
                item.MemberClauseDesc3 = item.MemberClauseDesc3;
             
            }

            //if (Edge.Web.Tools.DALTool.isHasMemberClauseCode(this.ClauseTypeCode.Text.Trim(), item.MemberClauseID))
            //{
            //    ShowWarning(Resources.MessageTips.ExistMemberClauseCode);
            //    this.ClauseTypeCode.Focus();
            //    return;
            //}

            //if (Edge.Web.Tools.DALTool.isHasMemberClauseCodeWithBrand(this.ClauseTypeCode.Text.Trim(), item.MemberClauseID, this.BrandID.SelectedValue.Trim()))
            //{
            //    ShowWarning(Resources.MessageTips.ExistMemberClauseCodeWith);
            //    this.ClauseTypeCode.Focus();
            //    return;
            //}

            if (DALTool.Update<Edge.SVA.BLL.MemberClause>(item))
            {
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
        }

    }
}