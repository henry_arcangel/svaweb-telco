﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Brand, Edge.SVA.Model.Brand>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindIndustry(IndustryID);
                Edge.Web.Tools.ControlTool.BindCardIssuer(CardIssuerID);
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                List<Edge.SVA.Model.CardIssuer> models = new Edge.SVA.BLL.CardIssuer().GetModelList("");
                if (models == null || models.Count <= 0) return;
                this.CardIssuerID.SelectedValue = models[0].CardIssuerID.ToString();
                if (Model != null)
                {
                    //存在小图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.BrandPicSFile))
                    {
                        this.Form2.Hidden = true;
                        this.Form5.Hidden = false;
                        this.btnBack1.Hidden = false;
                        this.btnView1.Hidden = true;
                    }
                    else
                    {
                        this.Form2.Hidden = false;
                        this.Form5.Hidden = true;
                        this.btnBack1.Hidden = true;
                        this.btnView1.Hidden = false;
                    }
                    //存在中图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.BrandPicMFile))
                    {
                        this.Form3.Hidden = true;
                        this.Form6.Hidden = false;
                        this.btnBack2.Hidden = false;
                        this.btnView2.Hidden = true;
                    }
                    else
                    {
                        this.Form3.Hidden = false;
                        this.Form6.Hidden = true;
                        this.btnBack2.Hidden = true;
                        this.btnView2.Hidden = false;
                    }
                    //存在大图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.BrandPicGFile))
                    {
                        this.Form4.Hidden = true;
                        this.Form7.Hidden = false;
                        this.btnBack3.Hidden = false;
                        this.btnView3.Hidden = true;
                    }
                    else
                    {
                        this.Form4.Hidden = false;
                        this.Form7.Hidden = true;
                        this.btnBack3.Hidden = true;
                        this.btnView3.Hidden = false;
                    }

                    this.uploadFilePath.Text = Model.BrandPicSFile;
                    this.uploadFilePath1.Text = Model.BrandPicMFile;
                    this.uploadFilePath2.Text = Model.BrandPicGFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    this.btnPreview1.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath1.Text, "图片");
                    this.btnPreview2.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath2.Text, "图片");
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.Brand item = this.GetUpdateObject();
            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.BrandCode = item.BrandCode.ToUpper();
                if (!string.IsNullOrEmpty(this.BrandPicSFile.ShortFileName))
                {
                    item.BrandPicSFile = this.BrandPicSFile.SaveToServer("Brand");
                }
                else if (this.Form5.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    item.BrandPicSFile = this.uploadFilePath.Text;
                }
                if (!string.IsNullOrEmpty(this.BrandPicMFile.ShortFileName))
                {
                    item.BrandPicMFile = this.BrandPicMFile.SaveToServer("Brand");
                }
                else if (this.Form6.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath1.Text))
                {
                    item.BrandPicMFile = this.uploadFilePath1.Text;
                }
                if (!string.IsNullOrEmpty(this.BrandPicGFile.ShortFileName))
                {
                    item.BrandPicGFile = this.BrandPicGFile.SaveToServer("Brand");
                }
                else if (this.Form7.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath2.Text))
                {
                    item.BrandPicGFile = this.uploadFilePath2.Text;
                }

            }
            if (Edge.Web.Tools.DALTool.isHasBrandCode(item.BrandCode, item.BrandID))
            {
                ShowWarming(Resources.MessageTips.ExistBrandCode);
                this.BrandCode.Focus();
                return;
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Brand>(item))
            {
                BrandRepostory.Singleton.Refresh();
                if (this.Form5.Hidden == true && string.IsNullOrEmpty(item.BrandPicSFile))
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                if (this.Form6.Hidden == true && string.IsNullOrEmpty(item.BrandPicMFile))
                {
                    DeleteFile(this.uploadFilePath1.Text);
                }
                if (this.Form7.Hidden == true && string.IsNullOrEmpty(item.BrandPicGFile))
                {
                    DeleteFile(this.uploadFilePath2.Text);
                }
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        #region 图片处理
        protected void ViewPicture1(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicSFile.ShortFileName))
            {
                this.PicturePath.Text = this.BrandPicSFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPicture.GetShowReference("../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected void ViewPicture2(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicMFile.ShortFileName))
            {
                this.PicturePath.Text = this.BrandPicMFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPicture.GetShowReference("../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected void ViewPicture3(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.BrandPicGFile.ShortFileName))
            {
                this.PicturePath.Text = this.BrandPicGFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPicture.GetShowReference("../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);
        }

        protected void btnReUpLoad1_Click(object sender, EventArgs e)
        {
            this.Form2.Hidden = false;
            this.Form5.Hidden = true;
        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.Form2.Hidden = true;
                this.Form5.Hidden = false;
            }
        }

        protected void btnReUpLoad2_Click(object sender, EventArgs e)
        {
            this.Form3.Hidden = false;
            this.Form6.Hidden = true;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.Form3.Hidden = true;
                this.Form6.Hidden = false;
            }
        }

        protected void btnReUpLoad3_Click(object sender, EventArgs e)
        {
            this.Form4.Hidden = false;
            this.Form7.Hidden = true;
        }

        protected void btnBack3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.Form4.Hidden = true;
                this.Form7.Hidden = false;
            }
        }
        #endregion
    }
}
