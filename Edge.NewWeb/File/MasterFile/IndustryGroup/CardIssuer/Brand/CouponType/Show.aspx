﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="TabStrip1" runat="server" />
    <ext:TabStrip ID="TabStrip1" ShowBorder="true" ActiveTabIndex="0" runat="server">
        <Tabs>
            <ext:Tab ID="Tab1" Title="基本信息" EnableBackgroundColor="true" Layout="Form" runat="server"
                AutoScroll="true" BodyPadding="10px">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                                Text="关闭">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form4" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponTypeCode" runat="server" Label="优惠劵类型编号：" />
                                                    <ext:Label ID="CouponTypeName1" runat="server" Label="描述：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponTypeName2" runat="server" Label="其他描述1：" />
                                                    <ext:Label ID="CouponTypeName3" runat="server" Label="其他描述2：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow40" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc1" runat="server" Label="条款条例1：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow41" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc2" runat="server" Label="条款条例2：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow42" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc3" runat="server" Label="条款条例3：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                             <%-- Add by Nathan 20140702 ++ --%>
                                            <ext:FormRow ID="FormRow47" runat="server">
                                                <Items>
                                                    <ext:DropDownList ID="CouponTypeNatureID" runat="server" Label="优惠券类型性质：" Resizable="true" Enabled="false"
                                                        CompareType="String" CompareValue="-1">
                                                        <ext:ListItem Text="优惠券" Value="0" />
                                                        <ext:ListItem Text="邮票" Value="4" />
                                                    </ext:DropDownList>
                                                </Items>
                                            </ext:FormRow>
                                            <%-- Add by Nathan 20140702 -- --%>
                                            <ext:FormRow ID="FormRow3" runat="server">
                                                <Items>
                                                    <ext:Label ID="BrandID" runat="server" Label="品牌：" />
                                                    <ext:Label ID="CampaignID" runat="server" Label="活动：" Hidden="true" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow4" runat="server">
                                                <Items>
                                                    <ext:Label ID="CoupontypeFixedAmountView" runat="server" Label="是否固定面额：" />
                                                    <ext:Label ID="CouponTypeAmount" runat="server" Label="面额：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow5" runat="server">
                                                <Items>
                                                    <ext:Label ID="CurrencyID" runat="server" Label="币种：" />
                                                    <ext:LinkButton ID="lbSaleProduct" runat="server" Label="销售货号匹配清单：" Text="查看销售货号" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow6" runat="server">
                                                <Items>
                                                    <ext:LinkButton ID="lbProduct" runat="server" Label="指定限购商品：" Text="查看限购商品" />
                                                    <ext:LinkButton ID="lbDeparment" runat="server" Label="指定限购部门：" Text="查看限购部门" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="0% 40% 20%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:Label ID="Label1" Text=" " runat="server" Label="优惠券布局：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                                        EnableAjax="false" DisableControlBeforePostBack="false" HideMode="Visibility">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow runat="server" ID="FormRow8" ColumnWidths="0% 40% 20%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath1" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:Label ID="Label2" Text="" runat="server" Label="优惠券图片：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnPreview1" runat="server" Text="查看" Icon="Picture">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow9" runat="server">
                                                <Items>
                                                    <ext:Label ID="IsMemberBindView" runat="server" Label="是否会员优惠券：" />
                                                    <ext:LinkButton ID="lbIssueBrand" runat="server" Label="发行品牌：" Text="品牌" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow10" runat="server">
                                                <Items>
                                                    <ext:LinkButton ID="lbIssueStore" runat="server" Label="发行店铺：" Text="店铺" />
                                                    <ext:LinkButton ID="lbUseBrand" runat="server" Label="使用品牌：" Text="品牌" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow11" runat="server">
                                                <Items>
                                                    <ext:LinkButton ID="lbUseStore" runat="server" Label="使用店铺：" Text="店铺" />
                                                    <ext:Label ID="PasswordRuleID" runat="server" Label="密码规则：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow12" runat="server">
                                                <Items>
                                                    <ext:Label ID="CouponforfeitControlView" runat="server" Label="优惠券面额清空控制：" />
                                                </Items>
                                            </ext:FormRow>
                                               <%--add by Alex 2014-07-01--%>
                                            <ext:FormRow ID="FormRow43" runat="server">
                                                <Items>
                                                    <ext:Label ID="AllowDeleteCouponView" runat="server" Label="Translate__Special_121_Start 删除优惠券（是 / 否）：Translate__Special_121_End" />
                                                    <ext:Label ID="AllowShareCouponView" runat="server" Label="Translate__Special_121_Start 优惠券分享（是 / 否）：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow44" runat="server">
                                                <Items>
                                                    <ext:Label ID="MaxDownloadCoupons" runat="server" Label="最大优惠券每次的下载数量：" />
                                                    <ext:Label ID="CouponReturnValue" runat="server" Label="Translate__Special_121_Start 优惠券退回数值（天）：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow45" runat="server">
                                                <Items>
                                                    <ext:Label ID="UnlimitedUsageView" runat="server" Label="无限使用：" />
                                                    <ext:Label ID="TrainingModeView" runat="server" Label="是否训练优惠券：" />
                                                </Items>
                                            </ext:FormRow>
                                            <%--add by Alex 2014-07-01--%>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Label ID="CouponTypeRedeemCount" runat="server" Label="可用数量：" />
                                    <ext:Label ID="CouponTypeDiscount" runat="server" Label="优惠券折扣：" />
                                    <ext:Label ID="SponsorIDView" runat="server" Label="赞助商："/>
                                    <ext:Label ID="SponsoredValue" runat="server" Label="赞助比例："/>
                                    <ext:Label ID="SupplierIDView" runat="server" Label="供货商："/>
                                    <ext:DropDownList ID="SponsorID" runat="server" Label="赞助商：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="SupplierID" runat="server" Label="供货商：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:RadioButtonList ID="CoupontypeFixedAmount" runat="server" Label="是否固定面额：">
                                        <ext:RadioItem Text="是的" Value="1" />
                                        <ext:RadioItem Text="不是" Value="0" Selected="true" />
                                    </ext:RadioButtonList>
                                    <%--<ext:Label ID="CouponTypeLayoutFile" runat="server" Label="优惠券布局：" />
                                    <ext:Label ID="CouponTypePicFile" runat="server" Label="优惠券图片：" />--%>
                                    <ext:Form ID="Form2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 40% 60%">
                                                <Items>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                        </Rows>
                                    </ext:Form>
                                    <ext:RadioButtonList ID="IsMemberBind" runat="server" Label="是否会员优惠券：">
                                        <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                        <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CouponforfeitControl" runat="server" Label="优惠券面额清空控制：">
                                        <ext:ListItem Value="0" Text="直接清空" Selected="true"></ext:ListItem>
                                        <ext:ListItem Value="1" Text="当日清空"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="不清空"></ext:ListItem>
                                    </ext:DropDownList>
                                    <%--Add by Alex 2014-06-30 ++--%>
                                    <ext:RadioButtonList ID="AllowDeleteCoupon" runat="server" Label="Translate__Special_121_Start 删除优惠券（是 / 否）：Translate__Special_121_End">
                                        <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                        <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="AllowShareCoupon" runat="server" Label="Translate__Special_121_Start 优惠券分享（是 / 否）：Translate__Special_121_End">
                                        <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                        <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <%-- <ext:NumberBox ID="MaxDownloadCoupons" runat="server" Label="Maximum Number for download coupon：" NoNegative="true" NoDecimal="true" MaxValue="99"
                                ToolTipTitle="可用数量" ToolTip="必須输入數字，並且是整數" Text = "0" >
                            </ext:NumberBox>--%>
                                    <%--   <ext:NumberBox ID="CouponReturnValue" runat="server" Label="Coupon Return Value (Day)：" NoNegative="true" NoDecimal="true" MaxValue="99"
                                ToolTipTitle="可用数量" ToolTip="必須输入數字，並且是整數" Text="0">
                            </ext:NumberBox>--%>
                                    <ext:RadioButtonList ID="UnlimitedUsage" runat="server" Label="无限使用：">
                                        <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                        <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="TrainingMode" runat="server" Label="是否训练优惠券：">
                                        <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                        <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <%--Add by Alex 2014-06-30 ++--%>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="优惠券号码规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm2" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Label ID="IsImportCouponNumberView" runat="server" Label="优惠券号码是否导入：" />
                                    <ext:RadioButtonList ID="IsImportCouponNumber" runat="server" Label="优惠券号码是否导入：">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="true" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="gpManual" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm3" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:RadioButtonList ID="CouponCheckdigit" runat="server" Label="是否包含校验位：">
                                        <ext:RadioItem Text="是" Value="True"></ext:RadioItem>
                                        <ext:RadioItem Text="否" Value="False" Selected="True"></ext:RadioItem>
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：">
                                        <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="MOD10" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CouponNumberToUID" runat="server" Label="Translate__Special_121_Start是否优惠券号码复制到优惠券物理编号（是/否）：Translate__Special_121_End">
                                        <ext:ListItem Value="1" Text="全部复制"></ext:ListItem>
                                        <ext:ListItem Value="0" Text="绑定"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="复制去掉校验位"></ext:ListItem>
                                        <ext:ListItem Value="3" Text="复制加上校验位"></ext:ListItem>
                                    </ext:DropDownList>
                                      <%--Add by Alex 2014-06-30 ++--%>
                                      <ext:DropDownList ID="QRCodePrefix" runat="server" Label="Translate__Special_121_Start QR码前缀号码：Translate__Special_121_End" Resizable="true">
                                        <ext:ListItem Value="" Text="-------" />
                                      <ext:ListItem Value="Q7FC" Text="Q7FC" />
                                       <ext:ListItem Value="Q7FI" Text="Q7FI" />
                                    </ext:DropDownList>
                                     <%--Add by Alex 2014-06-30 ++--%>
                                    <ext:Form ID="Form5" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                           <%--Add by Alex 2014-06-30 ++--%>
                                            <ext:FormRow ID="FormRow46" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="QRCodePrefixView" runat="server" Label="Translate__Special_121_Start QR码前缀号码：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                            <%--Add by Alex 2014-06-30 ++--%>
                                            <ext:FormRow ID="FormRow13" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponNumMask" runat="server" Label="优惠券号码编号规则：" />
                                                    <ext:Label ID="CouponNumPattern" runat="server" Label="优惠券号码编号前缀号码：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow14" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponCheckdigitView" runat="server" Label="是否包含校验位：" />
                                                    <ext:Label ID="CheckDigitModeIDView" runat="server" Label="校验位计算方法：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow15" runat="server">
                                                <Items>
                                                    <ext:Label ID="CouponNumberToUIDView" runat="server" Label="Translate__Special_121_Start是否优惠券号码复制到优惠券物理编号（是/否）：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="gpImport" runat="server" EnableCollapse="True" Title="导入物理编号规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm4" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form6" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow16" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponNumMaskImport" runat="server" Label="优惠券号码编号规则：" />
                                                    <ext:Label ID="CouponNumPatternImport" runat="server" Label="优惠券号码编号前缀号码：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow17" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="IsConsecutiveUIDView" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End" />
                                                    <ext:Label ID="UIDCheckDigitView" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow18" runat="server">
                                                <Items>
                                                    <ext:Label ID="CheckDigitModeID2View" runat="server" Label="校验位计算方法：" />
                                                    <ext:Label ID="UIDToCouponNumberView" runat="server" Label="Translate__Special_121_Start是否优惠券物理编号复制到优惠券号码：（是/否）：Translate__Special_121_End" />
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End">
                                        <ext:RadioItem Text="是" Value="1" Selected="true" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End">
                                        <ext:RadioItem Text="是" Value="1" Selected="true" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：">
                                        <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="MOD10" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="UIDToCouponNumber" runat="server" Label="Translate__Special_121_Start是否优惠券物理编号复制到优惠券号码：（是/否）：Translate__Special_121_End">
                                        <ext:ListItem Value="1" Text="全部复制"></ext:ListItem>
                                        <ext:ListItem Value="0" Text="绑定"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="复制去掉校验位"></ext:ListItem>
                                        <ext:ListItem Value="3" Text="复制加上校验位"></ext:ListItem>
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="有效期规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm5" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form7" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow19" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponValidityDuration" runat="server" Label="延长有效期值：" />
                                                    <ext:Label ID="CouponValidityUnitView" runat="server" Label="延长有效期的单位：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow20" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponSpecifyExpiryDate" runat="server" Label="优惠券固定过期日期：" />
                                                    <ext:Label ID="ActiveResetExpiryDateView" runat="server" Label="激活是否重置有效期：" />
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="CouponValidityUnit" runat="server" Label="延长有效期的单位：">
                                        <ext:ListItem Value="1" Text="年" Selected="True"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="月"></ext:ListItem>
                                        <ext:ListItem Value="3" Text="星期"></ext:ListItem>
                                        <ext:ListItem Value="4" Text="天"></ext:ListItem>
                                        <ext:ListItem Value="6" Text="指定失效日期"></ext:ListItem>
                                    </ext:DropDownList>
                                    <ext:RadioButtonList ID="ActiveResetExpiryDate" runat="server" Label="激活是否重置有效期：">
                                        <ext:RadioItem Text="是" Value="1" Selected="true" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="优惠券转增规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm6" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Label ID="CouponTypeTransferView" runat="server" Label="优惠劵是否可以转赠：" />
                                    <ext:DropDownList ID="CouponTypeTransfer" runat="server" Label="优惠劵是否可以转赠：">
                                        <ext:ListItem Value="0" Text="不允许"></ext:ListItem>
                                        <ext:ListItem Value="1" Text="同品牌转赠"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="同卡级别转赠"></ext:ListItem>
                                        <ext:ListItem Value="3" Text="同卡类型转赠"></ext:ListItem>
                                        <ext:ListItem Value="4" Text="任意转赠"></ext:ListItem>
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="有效性资料">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm7" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form8" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow21" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CouponTypeStartDate" runat="server" Label="优惠劵类型启用日期：" />
                                                    <ext:Label ID="CouponTypeEndDate" runat="server" Label="优惠劵类型结束日期：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow39" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="StartTime" runat="server" Label="启用时间：" />
                                                    <ext:Label ID="EndTime" runat="server" Label="结束时间：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow22" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CreatedOn" runat="server" Label="创建时间：" />
                                                    <ext:Label ID="CreatedBy" runat="server" Label="创建人：" />
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow23" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：" />
                                                    <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：" />
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="自动创建规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm8" ShowBorder="false" ShowHeader="false" runat="server"
                                EnableBackgroundColor="true" Title="" LabelAlign="Right">
                                <Items>
                                    <ext:RadioButtonList ID="AutoReplenish" runat="server" Label="是否自动补货：" Readonly="true" >
                                        <ext:RadioItem Text="是" Value="1" Selected="true" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:SimpleForm>
                            <ext:Form ID="DailySettingForm" runat="server" ShowBorder="false" ShowHeader="false" Title="每日设置" EnableBackgroundColor="true"
                                BodyPadding="5">
                                <Rows>
                                    <ext:FormRow ID="FormRow0" runat="server">
                                        <Items>
                                            <ext:Form ID="Form9" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow24" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="CheckAll" ShowLabel="true" Text="全选" AutoPostBack="true"
                                                                OnCheckedChanged="CheckAll_CheckedChanged">
                                                            </ext:CheckBox>
                                                            <%--<ext:Label ID="Label9" runat="server" ShowLabel="true" Label="" Text="开始时间" Hidden="true">
                                                    </ext:Label>
                                                    <ext:Label ID="Label10" runat="server" ShowLabel="true" Label="" Text="结束时间" Hidden="true">
                                                    </ext:Label>--%>
                                                            <ext:Label ID="Label3" runat="server" ShowLabel="true" Label="常规库存" Text="">
                                                            </ext:Label>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow25" runat="server">
                                        <Items>
                                            <ext:Form ID="Form11" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <%--LabelWidth="50"--%>
                                                <Rows>
                                                    <ext:FormRow ID="FormRow26" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay1" ShowLabel="true" Text="星期一">
                                                            </ext:CheckBox>
                                                            <%--<ext:TimePicker ID="WeekDayStartTime1" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00" Hidden="true">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime1" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59" Hidden="true">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity1" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow27" runat="server">
                                        <Items>
                                            <ext:Form ID="Form40" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow28" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay2" ShowLabel="true" Text="星期二">
                                                            </ext:CheckBox>
                                                            <%-- <ext:TimePicker ID="WeekDayStartTime2" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime2" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity2" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow29" runat="server">
                                        <Items>
                                            <ext:Form ID="Form10" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow30" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay3" ShowLabel="true" Text="星期三">
                                                            </ext:CheckBox>
                                                            <%--<ext:TimePicker ID="WeekDayStartTime3" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime3" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity3" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow31" runat="server">
                                        <Items>
                                            <ext:Form ID="Form41" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow32" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay4" ShowLabel="true" Text="星期四">
                                                            </ext:CheckBox>
                                                            <%-- <ext:TimePicker ID="WeekDayStartTime4" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime4" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity4" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow33" runat="server">
                                        <Items>
                                            <ext:Form ID="Form12" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow34" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay5" ShowLabel="true" Text="星期五">
                                                            </ext:CheckBox>
                                                            <%-- <ext:TimePicker ID="WeekDayStartTime5" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime5" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity5" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow35" runat="server">
                                        <Items>
                                            <ext:Form ID="Form13" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow36" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay6" ShowLabel="true" Text="星期六">
                                                            </ext:CheckBox>
                                                            <%-- <ext:TimePicker ID="WeekDayStartTime6" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime6" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity6" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ID="FormRow37" runat="server">
                                        <Items>
                                            <ext:Form ID="Form14" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                                Title="">
                                                <Rows>
                                                    <ext:FormRow ID="FormRow38" runat="server">
                                                        <Items>
                                                            <ext:CheckBox runat="server" ID="WeekDay7" ShowLabel="true" Text="星期日">
                                                            </ext:CheckBox>
                                                            <%-- <ext:TimePicker ID="WeekDayStartTime7" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime7" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                            <ext:NumberBox ID="WeekDayCouponQuantity7" runat="server" ShowLabel="false" Text="0"
                                                                NoDecimal="true" NoNegative="true" MaxValue="100000000" Readonly="true">
                                                            </ext:NumberBox>
                                                        </Items>
                                                    </ext:FormRow>
                                                </Rows>
                                            </ext:Form>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="优惠券类别">
                        <Items>
                            <ext:Grid ID="Grid_CouponNatureList" ShowBorder="true" ShowHeader="true" Title="优惠券类别列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_CouponNature_OnPageIndexChange">
                                <Columns>
                                    <ext:BoundField ColumnID="CouponNatureCode" DataField="CouponNatureCode" HeaderText="优惠券类别编号" />
                                    <ext:BoundField ColumnID="CouponNatureName" DataField="CouponNatureName1" HeaderText="优惠券类别名称" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab2" Title="消费金额转换优惠券规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnConsumeAmountList" runat="server" ShowBorder="false" ShowHeader="false"
                        AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="兑换类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="lblExchangeConsumeAmount" runat="server" Text='<%#Eval("ExchangeConsumeAmount","{0:N02}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则起始日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则失效日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="KeyID,CouponTypeID" DataIFrameUrlFormatString="EarnCouponRule/EarnConsumeAmount/Show.aspx?id={0}&CouponTypeID={1}&type=show"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab3" Title="积分兑换优惠券规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnPointList" runat="server" ShowBorder="false" ShowHeader="false"
                        AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="兑换类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="积分">
                                <ItemTemplate>
                                    <asp:Label ID="lblExchangePoint" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则起始日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则失效日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="KeyID,CouponTypeID" DataIFrameUrlFormatString="EarnCouponRule/EarnPoint/Show.aspx?id={0}&CouponTypeID={1}&type=show"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab4" Title="现金兑换优惠券规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnAmountList" runat="server" ShowBorder="false" ShowHeader="false"
                        AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="兑换类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="兑换金额">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%#Eval("ExchangeAmount","{0:N02}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则起始日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则失效日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="KeyID,CouponTypeID" DataIFrameUrlFormatString="EarnCouponRule/EarnAmount/Show.aspx?id={0}&CouponTypeID={1}&type=show"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab5" Title="优惠券兑换优惠券规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnCouponList" runat="server" ShowBorder="false" ShowHeader="false"
                        AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="兑换类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="数量">
                                <ItemTemplate>
                                    <asp:Label ID="Label18" runat="server" Text='<%#Eval("ExchangeCouponCount")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则起始日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label19" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则失效日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label20" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="Label21" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="KeyID,CouponTypeID" DataIFrameUrlFormatString="EarnCouponRule/EarnCoupon/Show.aspx?id={0}&CouponTypeID={1}&type=show"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab6" Title="组合兑换规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnAmountPointList" runat="server" ShowBorder="false" ShowHeader="false"
                        AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="兑换类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="积分">
                                <ItemTemplate>
                                    <asp:Label ID="Label22" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="Label26" runat="server" Text='<%#Eval("ExchangeAmount","{0:N02}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则起始日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label23" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则失效日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label24" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="Label25" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="KeyID,CouponTypeID" DataIFrameUrlFormatString="EarnCouponRule/EarnCoupon/Show.aspx?id={0}&CouponTypeID={1}&type=show"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
        </Tabs>
    </ext:TabStrip>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
