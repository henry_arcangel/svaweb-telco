﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.SaleProduct
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];

                    ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
                    if (string.IsNullOrEmpty(ids))
                    {
                        //FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, "", FineUI.MessageBoxIcon.Warning, string.Format("location.href='List.aspx?CouponTypeID={0}&type=2'", ViewState["couponTypeID"]));
                        Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        return;
                    }
                    logger.WriteOperationLog(this.PageName, "Delete " + ids);
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {

                        if (string.IsNullOrEmpty(id)) continue;
                        Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.CouponTypeExchangeBinding>(Tools.ConvertTool.ToInt(id));
                    }
                    Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                    //FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Warning, string.Format("location.href='List.aspx?CouponTypeID={0}&type=2'", ViewState["couponTypeID"]));
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}