﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using System.Data;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using System.Linq;
using System.IO;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.CouponNature;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType, Edge.SVA.Model.CouponType>
    {
        CouponTypeModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                int tabs = 0;
                tabs = int.TryParse(Request.Params["tabs"], out tabs) ? tabs : SVASessionInfo.Tabs == null ? 0 : int.TryParse(SVASessionInfo.Tabs, out tabs) ? tabs : 0;
                this.TabStrip1.ActiveTabIndex = tabs < TabStrip1.Tabs.Count ? tabs : 0;
                SVASessionInfo.Tabs = null;

                //SetControl(this.btnEarnConsumeAmountNew, this.btnEarnConsumeAmountDel, this.rptEarnConsumeAmountList, "EarnCouponRule/EarnConsumeAmount/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=5");
                //SetControl(this.btnEarnPointAdd, this.btnEarnPointDel, this.rptEarnPointList, "EarnCouponRule/EarnPoint/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=2");
                //SetControl(this.btnEarnAmountAdd, this.btnEarnAmountDel, this.rptEarnAmountList, "EarnCouponRule/EarnAmount/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=1");
                //SetControl(this.btnEarnCouponAdd, this.btnEarnCouponDel, this.rptEarnCouponList, "EarnCouponRule/EarnCoupon/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=4");
                //SetControl(this.btnEarnAmountPointAdd, this.btnEarnAmountPointDel, this.rptEarnAmountPointList, "EarnCouponRule/EarnAmountPoint/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=3");

                //Add By Robin 2015-01-06 
                this.Grid_CouponNatureList.PageSize = webset.ContentPageNum;
                btnNew.OnClientClick = Window1.GetShowReference(string.Format("CouponNatureList/Add.aspx?RuleType=2&PageFlag=0&CouponTypeID={0}", Request["id"]), "新增");
                //End
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                BindSponsorID(this.SponsorID);
                BindSupplierID(this.SupplierID);

                EarnCouponRuleRptBind("CouponTypeID=" + Request.Params["id"]);


                // Add.CheckImportCoupon(this.IsImportCouponNumber, gpManual, gpImport);


                this.lbIssueBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CouponTypeID={0}&type=2&StoreConditionTypeID=1", Request.Params["id"]), lbIssueBrand.Text);
                this.lbIssueStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CouponTypeID={0}&type=2&StoreConditionTypeID=1", Request.Params["id"]), lbIssueStore.Text);

                this.lbUseBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CouponTypeID={0}&type=2&StoreConditionTypeID=2", Request.Params["id"]), lbUseBrand.Text);
                this.lbUseStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CouponTypeID={0}&type=2&StoreConditionTypeID=2", Request.Params["id"]), lbUseStore.Text);

                this.lbDeparment.OnClientClick = Window1.GetShowReference(string.Format("Department/List.aspx?Url=&CouponTypeID={0}&type=2", Request.Params["id"]), lbDeparment.Text);
                this.lbProduct.OnClientClick = Window1.GetShowReference(string.Format("Product/List.aspx?Url=&CouponTypeID={0}&type=2", Request.Params["id"]), lbProduct.Text);
                this.lbSaleProduct.OnClientClick = Window1.GetShowReference(string.Format("SaleProduct/List.aspx?Url=&CouponTypeID={0}&type=2", Request.Params["id"]), lbSaleProduct.Text);
                //Add by Alex 2014-06-25 ++
                this.lblTenderMapping.OnClientClick = Window1.GetShowReference(string.Format("TenderMapping/List.aspx?Url=&CouponTypeID={0}&type=2", Request.Params["id"]), lbSaleProduct.Text);
                //Add by Alex 2014-06-25 --
                //兑换积分规则合并到Tab8，隐藏原先规则
                //this.Tab3.Hidden = this.Tab4.Hidden = this.Tab5.Hidden = this.Tab6.Hidden = this.Tab7.Hidden = true;

                #region 兑换积分规则
                BindEarnCouponRule("CouponTypeID=" + Request.Params["id"]);
                SetControl(this.btnAddEarnCouponRule, this.btnDelEarnCouponRule, this.rptEarnCouponRule, "EarnCouponRule/Add.aspx?CouponTypeID=" + Request.Params["id"]);
                #endregion

                #region 兑换积分规则
                BindCouponRedeemCondition("CouponTypeID=" + Request.Params["id"]);
                SetControl(this.btnAddCouponRedeemCondition, this.btnDelCouponRedeemCondition, this.rptCouponRedeemCondition, "CouponRedeemCondition/Add.aspx?CouponTypeID=" + Request.Params["id"]);
                #endregion

                BindingGrid_CouponNature();//Add By Robin 2015-01-07

                SVASessionInfo.CouponTypeModifyController = null;
            }
            controller = SVASessionInfo.CouponTypeModifyController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                    string brandText = DALTool.GetStringByCulture(brand.BrandCode + "-" + brand.BrandName1, brand.BrandCode + "-" + brand.BrandName2, brand.BrandCode + "-" + brand.BrandName3);
                    string brandID = brand.BrandID.ToString();
                    this.BrandID.Items.Add(new FineUI.ListItem() { Text = brandText, Value = brandID });

                    Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID, Model.BrandID);

                    this.CampaignID.SelectedValue = Model.CampaignID.GetValueOrDefault().ToString();

                    this.CouponTypeDiscount.Text = Model.CouponTypeDiscount.ToString().TrimEnd('M');
                    this.SponsoredValue.Text = Model.SponsoredValue.ToString().TrimEnd('M');

                    string msg = "";
                    if (Model.IsImportCouponNumber.GetValueOrDefault() == 1 && Model.UIDToCouponNumber.GetValueOrDefault() == 0)
                    {
                        this.CouponNumMaskImport.Text = Model.CouponNumMask;
                        this.CouponNumPatternImport.Text = Model.CouponNumPattern;
                    }


                    if ((DALTool.isCouponTypeCreateCoupon(Model.CouponTypeID, ref msg)))//已创建优惠券，不能修改
                    {
                        if (Model.IsImportCouponNumber.GetValueOrDefault() == 0)            //手动      
                        {
                            if (!string.IsNullOrEmpty(CouponNumMask.Text.Trim()))//号码规则不为空
                            {
                                this.CouponNumMask.Enabled = false;
                                this.CouponNumPattern.Enabled = false;
                                this.CouponCheckdigit.Enabled = false;
                                this.CheckDigitModeID.Enabled = false;
                                this.CouponNumberToUID.Enabled = false;
                                this.IsImportCouponNumber.Enabled = false;
                            }
                        }
                        else                                                                //导入
                        {
                            this.UIDCheckDigit.Enabled = false;
                            this.IsConsecutiveUID.Enabled = false;
                            this.UIDToCouponNumber.Enabled = false;
                            this.IsImportCouponNumber.Enabled = false;
                        }

                        //除了号码规则外，其他规则都不能修改
                        CouponTypeCode.Enabled = false;
                        CoupontypeFixedAmount.Enabled = false;
                        CouponTypeAmount.Enabled = false;
                        IsMemberBind.Enabled = false;
                        CouponValidityDuration.Enabled = false;
                        CouponValidityUnit.Enabled = false;
                        ActiveResetExpiryDate.Enabled = false;
                        CouponTypeTransfer.Enabled = false;
                    }

                    //Add.CheckImportCoupon(this.IsImportCouponNumber, gpManual, gpImport);

                    //存在文件时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CouponTypeLayoutFile))
                    {
                        this.FormLoad1.Hidden = true;
                        this.FormReLoad1.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad1.Hidden = false;
                        this.FormReLoad1.Hidden = true;
                        this.btnBack.Hidden = true;
                    }
                    //存在图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CouponTypePicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack1.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack1.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.CouponTypeLayoutFile;
                    this.uploadFilePath1.Text = Model.CouponTypePicFile;

                    //this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    this.btnPreview1.OnClientClick = WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath1.Text, "图片");
                    this.StartTime.Text = Model.CouponTypeStartDate.HasValue ? Model.CouponTypeStartDate.Value.ToString("HH:mm") : string.Empty;
                    this.EndTime.Text = Model.CouponTypeEndDate.HasValue ? Model.CouponTypeEndDate.Value.ToString("HH:mm") : string.Empty;

                    controller.LoadViewModel(Model.CouponTypeID, SVASessionInfo.SiteLanguage);
                    BindCouponReplenishDailyViewModelListToForm(controller.ViewModel.CouponReplenishDailyViewModelList);

                    this.MemberClauseDesc1.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc1);
                    this.MemberClauseDesc2.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc2);
                    this.MemberClauseDesc3.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc3);
                }

                checkIsImportCouponNumber(false);
                BrandID_SelectedIndexChanged(null, null);
                CoupontypeFixedAmount_SelectedIndexChanged(null, null);
                AutoReplenish_OnSelectedIndexChanged(null, null);
                CouponValidityUnit_SelectedIndexChanged(null, null);
                //if (Model != null)
                //{
                //    if (Model.IsImportCouponNumber == 1)
                //    {
                //        if (Model.UIDCheckDigit != null)
                //        {
                //            this.UIDCheckDigit.SelectedValue = Model.UIDCheckDigit.ToString();
                //        }
                //        if (Model.UIDToCouponNumber != null)
                //        {
                //            this.UIDToCouponNumber.SelectedValue = Model.UIDToCouponNumber.ToString();
                //        }
                //    }
                //    else
                //    {
                //        if (Model.CouponCheckdigit != null)
                //        {
                //            if (Model.CouponCheckdigit==true)
                //            {
                //                this.CouponCheckdigit.SelectedValue = "True";
                //            } 
                //            else
                //            {
                //                this.CouponCheckdigit.SelectedValue = "False";
                //            }
                //        }
                //        if (Model.CouponNumberToUID != null)
                //        {
                //            this.CouponNumberToUID.SelectedValue = Model.CouponNumberToUID.ToString();
                //        }
                //    }
                //}
            }
        }

        protected override void SetObject()
        {
            base.SetObject(this.Model, this.TabStrip1.Controls.GetEnumerator());
        }

        protected override SVA.Model.CouponType GetPageObject(SVA.Model.CouponType obj)
        {
            List<System.Web.UI.Control> list = new List<Control>();
            foreach (System.Web.UI.Control con in this.TabStrip1.Controls)
            {
                if (!(con is FineUI.GroupPanel)) continue;

                foreach (System.Web.UI.Control item in con.Controls) list.Add(item);
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.MemberClauseDesc1.Text) || this.MemberClauseDesc1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.Term1CannotBeEmpty);
                return;
            }

            Edge.SVA.Model.CouponType item = this.GetUpdateObject();
            item.CouponTypeStartDate = Edge.Utils.Tools.ConvertTool.GetInstance().ConverNullable<DateTime>(this.CouponTypeStartDate.Text + " " + this.StartTime.Text);
            item.CouponTypeEndDate = Edge.Utils.Tools.ConvertTool.GetInstance().ConverNullable<DateTime>(this.CouponTypeEndDate.Text + " " + this.EndTime.Text);
            controller.SetCouponReplenishDailyViewModelList(GetCouponReplenishDailyViewModelListFromForm());

            if (!Add.CheckStartEndDate(this.CouponTypeStartDate, this.CouponTypeEndDate))
            {
                return;
            }

            if (!Add.CheckInputNumRule(this.IsImportCouponNumber, this.CouponNumMaskImport, this.CouponNumPatternImport, this.CouponNumMask, this.CouponNumPattern))
            {
                return;
            }

            if (!Add.CheckValidityDuration(this.CouponValidityUnit, this.CouponValidityDuration, this.CouponSpecifyExpiryDate))
            {
                return;
            }

            //if (string.IsNullOrEmpty(CouponTypeStartDate.Text) && (!string.IsNullOrEmpty(CouponTypeEndDate.Text)))
            //{
            //    CouponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
            //    CouponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);

            //    return;
            //}
            //else if ((!string.IsNullOrEmpty(CouponTypeStartDate.Text)) && string.IsNullOrEmpty(CouponTypeEndDate.Text))
            //{
            //    CouponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
            //    CouponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);

            //    return;
            //}
            //else if ((!string.IsNullOrEmpty(CouponTypeStartDate.Text)) && (!string.IsNullOrEmpty(CouponTypeEndDate.Text)))
            //{
            //    if ((CouponTypeStartDate.SelectedDate != null) && (CouponTypeEndDate.SelectedDate != null))
            //    {
            //        if (CouponTypeStartDate.SelectedDate > CouponTypeEndDate.SelectedDate)
            //        {
            //            CouponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
            //            CouponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
            //            return;
            //        }
            //    }
            //}

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.CouponTypeCode = item.CouponTypeCode.ToUpper();
                item.CouponNumMask = item.CouponNumMask.ToUpper();

                if (item.IsImportCouponNumber != null)
                {
                    if (item.IsImportCouponNumber.GetValueOrDefault() == 1)
                    {
                        item.CouponNumMask = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumMaskImport.Text.ToUpper().Trim() : "";
                        item.CouponNumPattern = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumPatternImport.Text.ToUpper().Trim() : "";
                        //item.CouponCheckdigit = null; Modify by Nathan 20140630
                        item.CouponCheckdigit = false;
                        if (CheckDigitModeID2.Hidden == false)
                        {
                            item.CheckDigitModeID = Tools.ConvertTool.ConverType<int>(this.CheckDigitModeID2.SelectedValue);
                        }
                        else
                        {
                            item.CheckDigitModeID = null;
                        }
                        item.CouponNumberToUID = null;
                    }
                    else
                    {
                        item.UIDCheckDigit = null;
                        item.IsConsecutiveUID = null;
                        item.UIDToCouponNumber = null;
                        if (CheckDigitModeID.Hidden == false)
                        {
                            item.CheckDigitModeID = Tools.ConvertTool.ConverType<int>(this.CheckDigitModeID.SelectedValue);
                        }
                        else
                        {
                            item.CheckDigitModeID = null;
                        }
                    }
                }

                if (new Edge.SVA.BLL.CouponType().isHasCouponTypeCode(item.CouponTypeCode, item.CouponTypeID))
                {
                    this.ShowWarning(Resources.MessageTips.ExistCouponTypeCode);
                    return;
                }

                ////检查是否已经存在号码规则
                //if (this.IsImportCouponNumber.SelectedValue.Trim() == "0")
                //{
                //    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), item.CouponTypeID))
                //    {
                //        this.ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                //        return;
                //    }
                //}
                //if (item.IsImportCouponNumber.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 0)
                //{
                //    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), item.CouponTypeID))
                //    {
                //        this.ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                //        return;
                //    }
                //}


                //检查是否创建卡是可能重复号码规则
                if (item.IsImportCouponNumber.GetValueOrDefault() == 1)     //导入
                {
                    if (!this.CouponNumMaskImport.Hidden && !CouponNumPatternImport.Hidden)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), 2, item.CouponTypeID))
                        {
                            this.ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                            return;
                        }
                    }
                }
                else
                {
                    if (!this.CouponNumMask.Hidden && !CouponNumPattern.Hidden)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 2, item.CouponTypeID))
                        {
                            this.ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                            return;
                        }
                    }
                }
                //if (!Tools.DALTool.CheckNumberMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 2, item.CouponTypeID))
                //{
                //    this.ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                //    return;
                //}

                if (!string.IsNullOrEmpty(this.CouponTypeLayoutFile.ShortFileName) && this.FormLoad1.Hidden == false)
                {                
                    //校验文件类型
                    if (!ValidateFile(this.CouponTypeLayoutFile.FileName))
                    {
                        this.CouponTypeLayoutFile.Reset();
                        return;
                    }
                    item.CouponTypeLayoutFile = this.CouponTypeLayoutFile.SaveToServer("Images/CouponType");
                }
                else if (this.FormReLoad1.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateFile(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    item.CouponTypeLayoutFile = this.uploadFilePath.Text;
                }
                if (!string.IsNullOrEmpty(this.CouponTypePicFile.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateImg(this.CouponTypePicFile.FileName))
                    {
                        this.CouponTypePicFile.Reset();
                        return;
                    }
                    item.CouponTypePicFile = this.CouponTypePicFile.SaveToServer("Images/CouponType");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath1.Text))
                {
                    if (!ValidateImg(this.uploadFilePath1.Text))
                    {
                        return;
                    }
                    item.CouponTypePicFile = this.uploadFilePath1.Text;
                }


                HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                controller.ViewModel.MemberClause.MemberClauseDesc1 = clearTool.ClearSource(this.MemberClauseDesc1.Text);
                controller.ViewModel.MemberClause.MemberClauseDesc2 = clearTool.ClearSource(this.MemberClauseDesc2.Text);
                controller.ViewModel.MemberClause.MemberClauseDesc3 = clearTool.ClearSource(this.MemberClauseDesc3.Text);
                controller.ViewModel.MemberClause.BrandID = string.IsNullOrEmpty(this.BrandID.SelectedValue) ? 0 : Convert.ToInt32(this.BrandID.SelectedValue);
            }

            //item.UIDCheckDigit = GetCheckDigit();
            item.UIDCheckDigit = GetUIDCheckDigit();
            item.CouponCheckdigit = GetCouponCheckDigit();

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CouponType>(item))
            {
                #region 店铺/区域/品牌
                //List<Edge.SVA.Model.CouponTypeStoreCondition> allStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

                //if (Session["CouponTypeEarnStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeEarnStoreCheckedList"]);
                //}
                //else
                //{
                //    allStoreCheckedList.AddRange(new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + item.CouponTypeID + " and StoreConditionType=" + 1));
                //}

                //if (Session["CouponTypeConsumeStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeConsumeStoreCheckedList"]);
                //}
                //else
                //{
                //    allStoreCheckedList.AddRange(new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + item.CouponTypeID + " and StoreConditionType=" + 2));
                //}

                ////为每个新的条件赋值CouponTypeID
                //foreach (Edge.SVA.Model.CouponTypeStoreCondition newItem in allStoreCheckedList)
                //{
                //    newItem.CouponTypeID = item.CouponTypeID;
                //}

                //int add = new Edge.SVA.BLL.CouponTypeStoreCondition().UpdateList(allStoreCheckedList, item.CouponTypeID,Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
                //if (add > 0)
                //{
                //    JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //}
                //else
                //{
                //    JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);

                //}
                #endregion

                controller.Submit();
                CouponTypeRepostory.Singleton.Refresh();
                //没有选择文件时要删除加载时所赋的值
                if (this.FormReLoad1.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                //没有选择图片时要删除加载时所赋的值
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath1.Text);
                }

                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }

        protected void IsImportCouponNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkIsImportCouponNumber(true);
        }

        private void EarnCouponRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new Edge.SVA.BLL.EarnCouponRule();
            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            Edge.Web.Tools.DataTool.AddExchangeType(ds, "ExchangeTypeName", "ExchangeType");
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");

            DataView consumeAmountDV = ds.Tables[0].DefaultView;
            consumeAmountDV.RowFilter = "ExchangeType='5'";
            //this.btnEarnConsumeAmountDel.Enabled = consumeAmountDV.Count > 0 ? true : false;
            //this.rptEarnConsumeAmountList.DataSource = consumeAmountDV;
            //this.rptEarnConsumeAmountList.DataBind();

            DataView pointDV = ds.Tables[0].DefaultView;
            pointDV.RowFilter = "ExchangeType='2'";
            //this.lbtnEarnPointDel.Enabled = pointDV.Count > 0 ? true : false;
            //this.rptEarnPointList.DataSource = pointDV;
            //this.rptEarnPointList.DataBind();

            DataView amountDV = ds.Tables[0].DefaultView;
            amountDV.RowFilter = "ExchangeType='1'";
            //this.lbtnEarnAmountDel.Enabled = amountDV.Count > 0 ? true : false;
            //this.rptEarnAmountList.DataSource = amountDV;
            //this.rptEarnAmountList.DataBind();


            DataView couponDV = ds.Tables[0].DefaultView;
            couponDV.RowFilter = "ExchangeType='4'";
            //this.lbtnEarnCouponDel.Enabled = couponDV.Count > 0 ? true : false;
            //this.rptEarnCouponList.DataSource = couponDV;
            //this.rptEarnCouponList.DataBind();

            DataView amountPointDV = ds.Tables[0].DefaultView;
            amountPointDV.RowFilter = "ExchangeType='3'";
            //this.lbtnEarnAmountPointDel.Enabled = amountPointDV.Count > 0 ? true : false;
            //this.rptEarnAmountPointList.DataSource = amountPointDV;
            //this.rptEarnAmountPointList.DataBind();
        }

        #region Delete Event

        protected void btnEarnConsumeAmountDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (int row in rptEarnConsumeAmountList.SelectedRowIndexArray)
            //{
            //    sb.Append(rptEarnConsumeAmountList.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/EarnConsumeAmount/Delete.aspx?ids=" + sb.ToString().TrimEnd(',') + "&id=" + Request.Params["id"]));
        }

        protected void btnEarnPointDel_Click(object sender, EventArgs e)
        {
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (int row in this.rptEarnPointList.SelectedRowIndexArray)
            //{
            //    sb.Append(rptEarnPointList.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/EarnPoint/Delete.aspx?ids=" + sb.ToString().TrimEnd(',') + "&id=" + Request.Params["id"]));
        }

        protected void btnEarnAmountDel_Click(object sender, EventArgs e)
        {
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (int row in this.rptEarnAmountList.SelectedRowIndexArray)
            //{
            //    sb.Append(rptEarnAmountList.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/EarnAmount/Delete.aspx?ids=" + sb.ToString().TrimEnd(',') + "&id=" + Request.Params["id"]));
        }

        protected void btnEarnCouponDel_Click(object sender, EventArgs e)
        {
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (int row in this.rptEarnCouponList.SelectedRowIndexArray)
            //{
            //    sb.Append(rptEarnCouponList.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/EarnCoupon/Delete.aspx?ids=" + sb.ToString().TrimEnd(',') + "&id=" + Request.Params["id"]));
        }

        protected void btnEarnAmountPointDel_Click(object sender, EventArgs e)
        {
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (int row in this.rptEarnAmountPointList.SelectedRowIndexArray)
            //{
            //    sb.Append(rptEarnAmountPointList.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //}
            //ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/EarnAmountPoint/Delete.aspx?ids=" + sb.ToString().TrimEnd(',') + "&id=" + Request.Params["id"]));
        }

        #endregion

        private void SetControl(FineUI.Button add, FineUI.Button delete, FineUI.Grid grid, string addpath)
        {
            add.OnClientClick = Window1.GetShowReference(addpath, "新增");

            delete.OnClientClick = grid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            delete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
            delete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
        }

        private void checkIsImportCouponNumber(bool clearText)
        {
            if (this.IsImportCouponNumber.SelectedValue == "1")
            {
                Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);

                this.gpManual.Hidden = true;
                this.gpImport.Hidden = false;

                if (UIDToCouponNumber.SelectedValue == "0")
                {
                    this.CouponNumMaskImport.Hidden = false;
                    this.CouponNumPatternImport.Hidden = false;
                }
                else
                {
                    this.CouponNumMaskImport.Hidden = true;
                    this.CouponNumPatternImport.Hidden = true;
                }
            }
            else
            {

                Add.checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);

                this.gpManual.Hidden = false;
                this.gpImport.Hidden = true;


                this.CouponNumMaskImport.Hidden = false;
                this.CouponNumPatternImport.Hidden = false;
            }

            if (clearText)
            {
                this.CouponNumMask.Text = "";
                this.CouponNumPattern.Text = "";
                this.CouponNumMaskImport.Text = "";
                this.CouponNumPatternImport.Text = "";
            }
        }

        //private void checkCheckDigitMode4Manual()
        //{
        //    if (CouponCheckdigit.SelectedValue == "True")
        //    {

        //        foreach (FineUI.ListItem item in CouponNumberToUID.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID.Hidden = false;
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in CouponNumberToUID.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID.Hidden = true;
        //    }
        //}

        //private void checkCheckDigitMode4Import()
        //{
        //    if (UIDCheckDigit.SelectedValue == "1")
        //    {
        //        foreach (FineUI.ListItem item in UIDToCouponNumber.Items)
        //        {
        //            if (item.Value == "3")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID2.Hidden = false;
        //    }
        //    else
        //    {
        //        foreach (FineUI.ListItem item in UIDToCouponNumber.Items)
        //        {
        //            if (item.Value == "2")
        //                item.EnableSelect = false;
        //            else
        //                item.EnableSelect = true;
        //        }

        //        CheckDigitModeID2.Hidden = true;
        //    }
        //}

        protected void CouponCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);
            CouponNumberToUID.SelectedIndex = 0;
        }

        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);
            UIDToCouponNumber.SelectedIndex = 0;
        }


        protected void CouponTypeCode_TextChanged(object sender, EventArgs e)
        {
            this.CouponTypeCode.Text = this.CouponTypeCode.Text.ToUpper();
        }

        protected void CouponNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CouponNumMask.Text = this.CouponNumMask.Text.ToUpper();
        }

        protected void CouponNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CouponNumMask.Text) && string.IsNullOrEmpty(this.CouponNumPattern.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CouponNumMask, CouponNumPattern);
            //if (!Tools.CheckTool.IsMatchNumMask(this.CouponNumMask.Text, this.CouponNumPattern.Text,true))
            //{
            //    CouponNumPattern.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CouponNumPattern.Text));
            //}
        }

        protected void CouponNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CouponNumMaskImport.Text = this.CouponNumMaskImport.Text.ToUpper();
        }

        protected void CouponNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CouponNumMaskImport.Text) && string.IsNullOrEmpty(this.CouponNumPatternImport.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CouponNumMaskImport, CouponNumPatternImport);

            //if (!Tools.CheckTool.IsMatchNumMask(this.CouponNumMaskImport.Text, this.CouponNumPatternImport.Text,true))
            //{
            //    CouponNumPatternImport.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CouponNumPatternImport.Text));
            //}
        }

        protected void CoupontypeFixedAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CoupontypeFixedAmount.SelectedValue == "0")
            {
                CouponTypeAmount.Enabled = false;
                CouponTypeAmount.Text = "";
            }
            else
            {
                CouponTypeAmount.Enabled = true;
            }
        }

        protected void UIDToCouponNumber_SelectedIndexChanged(object sender, EventArgs e)
        {

            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);
            this.CouponNumMaskImport.Text = "";
            this.CouponNumPatternImport.Text = "";

            //this.CouponNumMaskImport.Text = "";
            //this.CouponNumPatternImport.Text = "";

            //if (UIDToCouponNumber.SelectedValue == "0")
            //{
            //    this.CouponNumMaskImport.Hidden = false;
            //    this.CouponNumPatternImport.Hidden = false;

            //    if (UIDCheckDigit.SelectedValue == "0")
            //    {
            //        CheckDigitModeID2.Hidden = true;
            //    }
            //}
            //else if (UIDToCouponNumber.SelectedValue == "3")
            //{
            //    this.CouponNumMaskImport.Hidden = true;
            //    this.CouponNumPatternImport.Hidden = true;

            //    if (UIDCheckDigit.SelectedValue == "0")
            //    {
            //        CheckDigitModeID2.Hidden = false;
            //    }
            //}
            //else
            //{
            //    this.CouponNumMaskImport.Hidden = true;
            //    this.CouponNumPatternImport.Hidden = true;

            //    if (UIDCheckDigit.SelectedValue == "0")
            //    {
            //        CheckDigitModeID2.Hidden = true;
            //    }
            //}
        }

        protected void CouponNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);
            //    if (CouponNumberToUID.SelectedValue == "3" && CouponCheckdigit.SelectedValue == "False")
            //    {
            //        CheckDigitModeID.Hidden = false;
            //    }
            //    else
            //    {
            //        CheckDigitModeID.Hidden = true;
            //    }
        }

        protected void CouponValidityUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CouponValidityUnit.SelectedValue == "6")
            {
                this.CouponValidityDuration.Enabled = false;
                this.CouponValidityDuration.Text = "";
                this.CouponSpecifyExpiryDate.Enabled = true;
            }
            else
            {
                this.CouponValidityDuration.Enabled = true;
                this.CouponSpecifyExpiryDate.Text = "";
                this.CouponSpecifyExpiryDate.Enabled = false;
            }
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.BrandID.SelectedValue))
            {
                Tools.ControlTool.BindCampaign(CampaignID);
            }
            else
            {
                Tools.ControlTool.BindCampaign(CampaignID, Tools.ConvertTool.ToInt(this.BrandID.SelectedValue));
            }
        }
        #region CouponReplenishDaily
        private List<CouponReplenishDailyViewModel> GetCouponReplenishDailyViewModelListFromForm()
        {
            List<CouponReplenishDailyViewModel> list = new List<CouponReplenishDailyViewModel>();
            CouponReplenishDailyViewModel model;
            string date = DateTime.Today.ToString("yyyy-MM-dd ");
            if (this.WeekDay1.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 1;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime1.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime1.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity1.Text);
                list.Add(model);
            }
            if (this.WeekDay2.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 2;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime2.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime2.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity2.Text);
                list.Add(model);
            }
            if (this.WeekDay3.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 3;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime3.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime3.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity3.Text);
                list.Add(model);
            }
            if (this.WeekDay4.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 4;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime4.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime4.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity4.Text);
                list.Add(model);
            }
            if (this.WeekDay5.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 5;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime5.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime5.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity5.Text);
                list.Add(model);
            }
            if (this.WeekDay6.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 6;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime6.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime6.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity6.Text);
                list.Add(model);
            }
            if (this.WeekDay7.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 7;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime7.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime7.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity7.Text);
                list.Add(model);
            }
            return list;
        }
        private void BindCouponReplenishDailyViewModelListToForm(List<CouponReplenishDailyViewModel> list)
        {
            foreach (var item in list)
            {
                int? weekdaynum = item.MainTable.WeekDayNum;
                string starttimeStr = item.MainTable.StartTime.HasValue ? item.MainTable.StartTime.Value.ToString("HH:mm") : "00:00";
                string endtimeStr = item.MainTable.EndTime.HasValue ? item.MainTable.EndTime.Value.ToString("HH:mm") : "23:59";
                string quantityStr = item.MainTable.MaxReplenishCount.ToString();
                switch (weekdaynum)
                {
                    case 1:
                        this.WeekDay1.Checked = true;
                        //this.WeekDayStartTime1.Text = starttimeStr;
                        //this.WeekDayEndTime1.Text = endtimeStr;
                        this.WeekDayCouponQuantity1.Text = quantityStr;
                        break;
                    case 2:
                        this.WeekDay2.Checked = true;
                        //this.WeekDayStartTime2.Text = starttimeStr;
                        //this.WeekDayEndTime2.Text = endtimeStr;
                        this.WeekDayCouponQuantity2.Text = quantityStr;
                        break;
                    case 3:
                        this.WeekDay3.Checked = true;
                        //this.WeekDayStartTime3.Text = starttimeStr;
                        //this.WeekDayEndTime3.Text = endtimeStr;
                        this.WeekDayCouponQuantity3.Text = quantityStr;
                        break;
                    case 4:
                        this.WeekDay4.Checked = true;
                        //this.WeekDayStartTime4.Text = starttimeStr;
                        //this.WeekDayEndTime4.Text = endtimeStr;
                        this.WeekDayCouponQuantity4.Text = quantityStr;
                        break;
                    case 5:
                        this.WeekDay5.Checked = true;
                        //this.WeekDayStartTime5.Text = starttimeStr;
                        //this.WeekDayEndTime5.Text = endtimeStr;
                        this.WeekDayCouponQuantity5.Text = quantityStr;
                        break;
                    case 6:
                        this.WeekDay6.Checked = true;
                        //this.WeekDayStartTime6.Text = starttimeStr;
                        //this.WeekDayEndTime6.Text = endtimeStr;
                        this.WeekDayCouponQuantity6.Text = quantityStr;
                        break;
                    case 7:
                        this.WeekDay7.Checked = true;
                        //this.WeekDayStartTime7.Text = starttimeStr;
                        //this.WeekDayEndTime7.Text = endtimeStr;
                        this.WeekDayCouponQuantity7.Text = quantityStr;
                        break;
                    default:
                        break;
                }
            }
        }
        protected void CheckAll_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.CheckAll.Checked;
            this.WeekDay1.Checked = check;
            this.WeekDay2.Checked = check;
            this.WeekDay3.Checked = check;
            this.WeekDay4.Checked = check;
            this.WeekDay5.Checked = check;
            this.WeekDay6.Checked = check;
            this.WeekDay7.Checked = check;
        }

        protected void AutoReplenish_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.AutoReplenish.SelectedValue == "1")
            {
                SetEnableWeekDayControls(true);
            }
            else
            {
                SetEnableWeekDayControls(false);
            }
        }
        private void SetEnableWeekDayControls(bool bol)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(this.CheckAll);
            tool.AddControl(this.WeekDay1);
            tool.AddControl(this.WeekDay2);
            tool.AddControl(this.WeekDay3);
            tool.AddControl(this.WeekDay4);
            tool.AddControl(this.WeekDay5);
            tool.AddControl(this.WeekDay6);
            tool.AddControl(this.WeekDay7);
            tool.AddControl(this.WeekDayCouponQuantity1);
            tool.AddControl(this.WeekDayCouponQuantity2);
            tool.AddControl(this.WeekDayCouponQuantity3);
            tool.AddControl(this.WeekDayCouponQuantity4);
            tool.AddControl(this.WeekDayCouponQuantity5);
            tool.AddControl(this.WeekDayCouponQuantity6);
            tool.AddControl(this.WeekDayCouponQuantity7);
            tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, bol);
        }
        //private void SetHideWeekDayControls(bool hidden)
        //{
        //    this.WeekDayStartTime1.Hidden = hidden;
        //    this.WeekDayStartTime2.Hidden = hidden;
        //    this.WeekDayStartTime3.Hidden = hidden;
        //    this.WeekDayStartTime4.Hidden = hidden;
        //    this.WeekDayStartTime5.Hidden = hidden;
        //    this.WeekDayStartTime6.Hidden = hidden;
        //    this.WeekDayStartTime7.Hidden = hidden;

        //    this.WeekDayEndTime1.Hidden = hidden;
        //    this.WeekDayEndTime2.Hidden = hidden;
        //    this.WeekDayEndTime3.Hidden = hidden;
        //    this.WeekDayEndTime4.Hidden = hidden;
        //    this.WeekDayEndTime5.Hidden = hidden;
        //    this.WeekDayEndTime6.Hidden = hidden;
        //    this.WeekDayEndTime7.Hidden = hidden;
        //}
        #endregion

        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CouponTypePicFile.ShortFileName))
            {
                this.PicturePath.Text = this.CouponTypePicFile.SaveToServer("Images/CouponType");
                FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("../../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath1.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);
            BindingGrid_CouponNature();//Add by Robin 2015-01-08
        }
        #endregion

        #region 文件上传处理
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CuponGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CuponGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnReUpLoad1_Click(object sender, EventArgs e)
        {
            this.FormLoad1.Hidden = false;
            this.FormReLoad1.Hidden = true;
        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad1.Hidden = true;
                this.FormReLoad1.Hidden = false;
            }
        }
        #endregion

        //校验位逻辑（Add by Len）
        //UID校验位逻辑（Add by Len）
        protected int GetUIDCheckDigit()
        {
            int Digit = 0;
            if (this.IsImportCouponNumber.SelectedValue == "0")
            {
                if (this.CouponCheckdigit.SelectedValue.ToLower() == "false")
                {
                    if (this.CouponNumberToUID.SelectedValue == "3")
                    {
                        Digit = 1;
                    }
                }
                else
                {
                    if (this.CouponNumberToUID.SelectedValue == "1")
                    {
                        Digit = 1;
                    }
                }
            }
            else
            {
                if (this.UIDCheckDigit.SelectedValue == "1")
                {
                    Digit = 1;
                }
            }

            return Digit;
        }

        //Number校验位逻辑(Add by Len)
        protected bool GetCouponCheckDigit()
        {
            bool Digit = false;
            if (this.IsImportCouponNumber.SelectedValue == "1")
            {
                if (this.UIDCheckDigit.SelectedValue == "1")
                {
                    if (this.UIDToCouponNumber.SelectedValue == "1")
                    {
                        Digit = true;
                    }
                }
                else
                {
                    if (this.UIDToCouponNumber.SelectedValue == "3")
                    {
                        Digit = true;
                    }
                }
            }
            else
            {
                if (this.CouponCheckdigit.SelectedValue == "True")
                {
                    Digit = true;
                }
            }

            return Digit;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验模板文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.CouponTypeFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CouponTypeFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        #region 兑换积分规则
        protected void BindEarnCouponRule(string strwhere)
        {
            try
            {
                EarnCouponRuleController con = new EarnCouponRuleController();
                int count = 0;
                DataSet ds = con.GetTransactionList(strwhere, webset.ContentPageNum, this.rptEarnCouponRule.PageIndex, out count, "KeyID");
                this.rptEarnCouponRule.RecordCount = count;
                if (ds != null)
                {
                    this.rptEarnCouponRule.DataSource = ds.Tables[0].DefaultView;
                    this.rptEarnCouponRule.DataBind();
                }
                else
                {
                    this.rptEarnCouponRule.Reset();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("EarnCouponRule", "Load Faild", ex);
            }
        }
        protected void btnDelEarnCouponRule_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in rptEarnCouponRule.SelectedRowIndexArray)
            {
                sb.Append(rptEarnCouponRule.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("EarnCouponRule/Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }
        protected void rptEarnCouponRule_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            rptEarnCouponRule.PageIndex = e.NewPageIndex;

            BindEarnCouponRule("CouponTypeID=" + Request.Params["id"]);
        }

        #endregion

        #region 优惠劵使用条件设置表
        protected void BindCouponRedeemCondition(string strwhere)
        {
            try
            {
                Edge.SVA.BLL.CouponTypeRedeemCondition bll = new SVA.BLL.CouponTypeRedeemCondition();
                DataSet ds = bll.GetList(strwhere);
                if (ds != null)
                {
                    this.rptCouponRedeemCondition.RecordCount = ds.Tables[0].Rows.Count;

                    Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
                    Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
                    Tools.DataTool.AddRedeemLimitTypeName(ds, "RedeemLimitTypeName", "RedeemLimitType");
                    this.rptCouponRedeemCondition.DataSource = ds.Tables[0].DefaultView;
                    this.rptCouponRedeemCondition.DataBind();
                }
                else
                {
                    this.rptCouponRedeemCondition.Reset();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("CouponRedeemCondition", "Load Faild", ex);
            }
        }
        protected void btnDelCouponRedeemCondition_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in rptCouponRedeemCondition.SelectedRowIndexArray)
            {
                sb.Append(rptCouponRedeemCondition.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("CouponRedeemCondition/Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }
        protected void rptCouponRedeemCondition_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            rptEarnCouponRule.PageIndex = e.NewPageIndex;

            BindCouponRedeemCondition("CouponTypeID=" + Request.Params["id"]);
        }
        #endregion

        protected void BindSponsorID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Sponsor bll = new SVA.BLL.Sponsor();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SponsorID", "SponsorName1", "SponsorName2", "SponsorName3", "SponsorCode");
        }

        protected void BindSupplierID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Supplier bll = new SVA.BLL.Supplier();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SupplierID", "SupplierDesc1", "SupplierDesc2", "SupplierDesc3", "SupplierCode");
        }
        #region Add by Nathan 20140701
        private static string FormatPath(string item)
        {
            string key = item.Trim();
            //key = key.Replace("\\","/");
            key = Regex.Replace(key, @"[/\\][/\\    ]*", "/");
            if (!key.Substring(0, 1).Equals("/"))
            {
                key = "/" + key;
            }
            return key.ToLower();
        }

        protected void btnGenerateHtml_Click(object sender, EventArgs e)
        {

            string webPath = Request.Url.Scheme + "://" + Request.Url.Authority;


            string htmlPath = "";

            string fileName = this.CouponTypeCode.Text + "_" + SVASessionInfo.SiteLanguage + ".html";
            string htmlSavePath = "";
            try
            {
                htmlPath = webset.CouponHtmlSavePath;
                if (!htmlPath.StartsWith("~"))
                {
                    htmlPath = htmlPath.Insert(0, "~");
                }

                htmlSavePath = System.Web.HttpContext.Current.Server.MapPath(htmlPath + fileName);

                string htmlContent = GetHtmlTmpContent();
                if (string.IsNullOrEmpty(htmlContent))
                {
                    return;
                }
                else
                {
                    string url = this.uploadFilePath1.Text;

                    string virtualPath = webset.WebPath;
                    string strVirtualPath = FormatPath(virtualPath);
                    if (strVirtualPath.EndsWith("/"))
                    {
                        strVirtualPath = strVirtualPath.Substring(0, strVirtualPath.Length - 1);
                    }

                    if (!strVirtualPath.StartsWith("/") && !string.IsNullOrEmpty(strVirtualPath))
                    {
                        strVirtualPath = strVirtualPath.Insert(0, "/");
                    }

                    //Modified By Robin 2014-11-17 for 7-11
                    //string includePath = webPath + strVirtualPath + "/Template";
                    //string imgUrl = webPath + strVirtualPath + url;
                    string includePath = webset.AttchFileServer + strVirtualPath + "/Template";
                    string imgUrl = webset.AttchFileServer + strVirtualPath + url;
                    //End

                    //if (!url.StartsWith("~"))
                    //{
                    //    url = url.Insert(0, "~");
                    //}
                    //string imgPath = System.Web.HttpContext.Current.Server.MapPath(htmlPath + "images/coupon/");


                    //if (!Directory.Exists(imgPath))
                    //    Directory.CreateDirectory(imgPath);

                    //string oldImgFile =System.Web.HttpContext.Current.Server.MapPath(this.uploadFilePath1.Text.Trim());
                    //string newFileName = "";
                    //if (System.IO.File.Exists(oldImgFile))
                    //{
                    //    newFileName = System.IO.Path.GetFileName(oldImgFile);
                    //    string newFile = imgPath + newFileName;
                    //    System.IO.File.Copy(oldImgFile, newFile, true);
                    //}

                    string couponExpiryDate = "";
                    DateTime eDate = Tools.ConvertTool.ToDateTime(this.CouponTypeEndDate.Text);
                    if (eDate != DateTime.MinValue)
                    {
                        couponExpiryDate = eDate.ToString("yyyy/MM/dd");
                    }

                    htmlContent = htmlContent.Replace("%%CouponTypePicture%%", imgUrl);
                    htmlContent = htmlContent.Replace("%%ExpiryDate%%", couponExpiryDate);
                    htmlContent = htmlContent.Replace("%%WebPath%%", includePath);


                    if (SVASessionInfo.SiteLanguage.ToLower() == "zh-cn")
                    {
                        htmlContent = htmlContent.Replace("%%CouponTypeName%%", CouponTypeName2.Text);
                        htmlContent = htmlContent.Replace("%%MemberClauseDesc%%", MemberClauseDesc2.Text);
                    }
                    else if (SVASessionInfo.SiteLanguage.ToLower() == "zh-hk")
                    {
                        htmlContent = htmlContent.Replace("%%CouponTypeName%%", CouponTypeName3.Text);
                        htmlContent = htmlContent.Replace("%%MemberClauseDesc%%", MemberClauseDesc3.Text);

                    }
                    else
                    {
                        htmlContent = htmlContent.Replace("%%CouponTypeName%%", CouponTypeName1.Text);
                        htmlContent = htmlContent.Replace("%%MemberClauseDesc%%", MemberClauseDesc1.Text);

                    }



                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(htmlPath)))
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(htmlPath));

                    if (System.IO.File.Exists(htmlSavePath))
                        System.IO.File.Delete(htmlSavePath);

                    using (StreamWriter sw = new StreamWriter(htmlSavePath, true))
                    {
                        sw.Write(htmlContent);
                    }

                    ShowWarning(Resources.MessageTips.FileSaveAs + fileName);

                }

                //Tools.ExportTool.ExportFile(fileName);
                //Tools.Logger.Instance.WriteOperationLog("CuponGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CouponType Generate to Html ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        private string GetHtmlTmpContent()
        {
            string htmlTmpPath = "";

            if (SVASessionInfo.SiteLanguage.ToLower() == "zh-cn")
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/coupon_zh-cn.html");
            }
            else if (SVASessionInfo.SiteLanguage.ToLower() == "zh-hk")
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/coupon_zh-hk.html");
            }
            else
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/coupon.html");
            }

            if (System.IO.File.Exists(htmlTmpPath))
            {
                StreamReader sr = new StreamReader(htmlTmpPath, Encoding.UTF8);
                string str = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                return str;
            }
            else
                return "";
        }


        #endregion

        protected void DeleteLayout_OnClick(object sender, EventArgs e)
        {
            this.CouponTypeLayoutFile.Reset();
        }

        protected void DeletePic_OnClick(object sender, EventArgs e)
        {
            this.CouponTypePicFile.Reset();
        }

        //Add By Robin 2015-01-07 
        #region CouponNature 优惠券类别
        protected void Grid_CouponNature_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid_CouponNatureList.PageIndex = e.NewPageIndex;
            this.BindingGrid_CouponNature();
        }

        private void BindingGrid_CouponNature()
        {
            ViewState["sotrID"] = Request["id"];
            string sotrID = "";
            if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                sotrID = ViewState["sotrID"].ToString();


            Edge.SVA.BLL.CouponType coupontype = new SVA.BLL.CouponType();
            DataSet ds = coupontype.GetCouponNatureList(sotrID);

            if (ds != null)
            {
                this.Grid_CouponNatureList.DataSource = ds.Tables[0].DefaultView;
                this.Grid_CouponNatureList.DataBind();
            }
            else
            {
                this.Grid_CouponNatureList.Reset();
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in this.Grid_CouponNatureList.SelectedRowIndexArray)
            {
                sb.Append(Grid_CouponNatureList.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("CouponNatureList/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
        }
        #endregion
        //End
    }
}
