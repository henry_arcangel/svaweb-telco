﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnConsumeAmount
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CouponTypeID.Text = Tools.DALTool.GetCouponTypeText(Model.CouponTypeID);

                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.ExchangeTypeView.Text = this.ExchangeType.SelectedText;
                this.ExchangeType.Hidden = true;

                this.StatusView.Text = this.Status.SelectedItem == null ? "" : this.Status.SelectedItem.Text;
                this.Status.Hidden = true;

                this.ExchangeConsumeRuleOperView.Text = this.ExchangeConsumeRuleOper.SelectedText;
                this.ExchangeConsumeRuleOper.Hidden = true;
            }
        }

       
    }
}
