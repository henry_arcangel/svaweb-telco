﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmount.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Label ID="CouponTypeID" runat="server" Label="优惠券类型：" />
            <ext:Label ID="ExchangeTypeView" runat="server" Label="兑换类型：" />
            <ext:DropDownList ID="ExchangeType" runat="server" Label="兑换类型：" Enabled="false">
                <ext:ListItem Text="金额兑换" Value="1" />
                <ext:ListItem Text="积分兑换" Value="2" />
                <ext:ListItem Text="金额+积分兑换" Value="3" />
                <ext:ListItem Text="优惠券兑换" Value="4" />
                <ext:ListItem Text="消费金额兑换" Value="5" />
            </ext:DropDownList>
            <ext:Label ID="ExchangeAmount" runat="server" Label="兑换需要的金额：" />
            <ext:Label ID="StartDate" runat="server" Label="规则起始日期：" />
            <ext:Label ID="EndDate" runat="server" Label="规则结束日期：" />
            <ext:Label ID="StatusView" runat="server" Label="状态：" />
            <ext:RadioButtonList ID="Status" runat="server" Label="状态：">
                <ext:RadioItem Text="有效" Value="1" Selected="true" />
                <ext:RadioItem Text="无效" Value="0" />
            </ext:RadioButtonList>
            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：" />
            <ext:Label ID="CreatedBy" runat="server" Label="创建人：" />
            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：" />
            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：" />
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
