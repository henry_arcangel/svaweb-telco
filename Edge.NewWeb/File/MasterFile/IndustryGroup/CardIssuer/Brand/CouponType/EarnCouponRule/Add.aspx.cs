﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        public const string tab = "1";
        EarnCouponRuleController controller = new EarnCouponRuleController();
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                InitData();
            }

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.CouponTypeID = ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());

                //不选择时，填入数据库中的默认值设置为0或者null
                controller.ViewModel.MainTable.CardTypeIDLimit = this.CardTypeIDLimit.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.CardTypeIDLimit.SelectedValue);
                controller.ViewModel.MainTable.CardGradeIDLimit = this.CardGradeIDLimit.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.CardGradeIDLimit.SelectedValue);
                controller.ViewModel.MainTable.MemberBirthdayLimit = this.MemberBirthdayLimit.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.MemberBirthdayLimit.SelectedValue);
                controller.ViewModel.MainTable.ExchangeCouponTypeID = this.ExchangeCouponTypeID.SelectedValue == "-1" ? 0 : ConvertTool.ToInt(this.ExchangeCouponTypeID.SelectedValue);

                ExecResult er = controller.Add();
                if (er.Success)
                {
                    SVASessionInfo.Tabs = Add.tab;
                    CloseAndRefresh();
                }
                else
                {
                    ShowAddFailed();
                }
            }
        }

        protected void InitData()
        {
            ControlTool.BindCardType(CardTypeIDLimit);
            ControlTool.BindCardGrade(CardGradeIDLimit);
            ControlTool.BindKeyValueListNoEmpty(this.MemberBirthdayLimit, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
            ControlTool.BindKeyValueList(this.ExchangeType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ExchangeType), true);
            ControlTool.BindKeyValueList(this.ExchangeConsumeRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ExchangeConsumeRuleOper), true);
            ControlTool.BindCouponType(this.ExchangeCouponTypeID);
            this.Status.SelectedValue = "1";

            this.ExchangeConsumeRuleOper.Enabled = false;
            this.ExchangeConsumeAmount.Enabled = false;
            this.ExchangeAmount.Enabled = false;
            this.ExchangePoint.Enabled = false;
            this.ExchangeCouponTypeID.Enabled = false;
            this.ExchangeCouponCount.Enabled = false;

            this.CouponType.Text = DALTool.GetCouponTypeText(Convert.ToInt32(Request.Params["CouponTypeID"]));

            string sql = @"select a.BrandID,b.BrandCode from CouponType a join Brand b on a.BrandID=b.BrandID where CouponTypeID = " + Request.Params["CouponTypeID"];
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    string brandcode = ds.Tables[0].Rows[0]["BrandCode"].ToString();
                    int brandid = Convert.ToInt32(ds.Tables[0].Rows[0]["BrandID"]);
                    this.Brand.Text = brandcode + "-" + DALTool.GetBrandName(brandid, null);
                }
            }
        }

        protected void ExchangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.ExchangeType.SelectedValue)
            {
                case "5":
                    this.ExchangeConsumeRuleOper.Enabled = true;
                    this.ExchangeConsumeAmount.Enabled = true;
                    this.ExchangeAmount.Enabled = false;
                    this.ExchangePoint.Enabled = false;
                    this.ExchangeCouponTypeID.Enabled = false;
                    this.ExchangeCouponCount.Enabled = false;

                    this.ExchangeAmount.Text = "0";
                    this.ExchangePoint.Text = "0";
                    this.ExchangeCouponTypeID.SelectedValue = "-1";
                    this.ExchangeCouponCount.Text = string.Empty;
                    break;
                case "1":
                    this.ExchangeConsumeRuleOper.Enabled = false;
                    this.ExchangeConsumeAmount.Enabled = false;
                    this.ExchangePoint.Enabled = false;
                    this.ExchangeCouponTypeID.Enabled = false;
                    this.ExchangeCouponCount.Enabled = false;
                    this.ExchangeAmount.Enabled = true;

                    this.ExchangeConsumeRuleOper.SelectedValue = "-1";
                    this.ExchangeConsumeAmount.Text = "0";
                    this.ExchangePoint.Text = "0";
                    this.ExchangeCouponTypeID.SelectedValue = "-1";
                    this.ExchangeCouponCount.Text = string.Empty;
                    break;
                case "2":
                    this.ExchangeConsumeRuleOper.Enabled = false;
                    this.ExchangeConsumeAmount.Enabled = false;
                    this.ExchangeAmount.Enabled = false;
                    this.ExchangeCouponTypeID.Enabled = false;
                    this.ExchangeCouponCount.Enabled = false;
                    this.ExchangePoint.Enabled = true;

                    this.ExchangeConsumeRuleOper.SelectedValue = "-1";
                    this.ExchangeConsumeAmount.Text = "0";
                    this.ExchangeAmount.Text = "0";
                    this.ExchangeCouponTypeID.SelectedValue = "-1";
                    this.ExchangeCouponCount.Text = string.Empty;
                    break;
                case "4":
                    this.ExchangeConsumeRuleOper.Enabled = false;
                    this.ExchangeConsumeAmount.Enabled = false;
                    this.ExchangeAmount.Enabled = false;
                    this.ExchangePoint.Enabled = false;
                    this.ExchangeCouponTypeID.Enabled = true;
                    this.ExchangeCouponCount.Enabled = true;

                    this.ExchangeConsumeRuleOper.SelectedValue = "-1";
                    this.ExchangeConsumeAmount.Text = "0";
                    this.ExchangeAmount.Text = "0";
                    this.ExchangePoint.Text = "0";
                    break;
                case "3":
                    this.ExchangeConsumeRuleOper.Enabled = false;
                    this.ExchangeConsumeAmount.Enabled = false;
                    this.ExchangeAmount.Enabled = true;
                    this.ExchangePoint.Enabled = true;
                    this.ExchangeCouponTypeID.Enabled = true;
                    this.ExchangeCouponCount.Enabled = true;

                    this.ExchangeConsumeRuleOper.SelectedValue = "-1";
                    this.ExchangeConsumeAmount.Text = "0";
                    break;
                default:
                    this.ExchangeConsumeRuleOper.Enabled = false;
                    this.ExchangeConsumeAmount.Enabled = false;
                    this.ExchangeAmount.Enabled = false;
                    this.ExchangePoint.Enabled = false;
                    this.ExchangeCouponTypeID.Enabled = false;
                    this.ExchangeCouponCount.Enabled = false;

                    this.ExchangeConsumeRuleOper.SelectedValue = "-1";
                    this.ExchangeConsumeAmount.Text = "0";
                    this.ExchangeAmount.Text = "0";
                    this.ExchangePoint.Text = "0";
                    this.ExchangeCouponTypeID.SelectedValue = "-1";
                    this.ExchangeCouponCount.Text = string.Empty;
                    break;
            }
        }

        protected void CardTypeIDLimit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CardTypeIDLimit.SelectedValue != "-1")
            {
                ControlTool.BindCardGrade(CardGradeIDLimit, ConvertTool.ToInt(this.CardTypeIDLimit.SelectedValue));
            }
        }
    }
}