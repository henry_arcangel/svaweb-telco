﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Brand
{
    public partial class List : PageBase
    {

        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");

                btnNew.OnClientClick = Window1.GetShowReference(string.Format("add.aspx?CouponTypeID={0}&StoreConditionTypeID={1}", this.CouponTypeID, this.StoreConditionTypeID), "新增");
                btnClose.OnClientClick = FineUI.ActiveWindow.GetHideReference();

                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

              
                if (this.CouponTypeID > 0)
                {
                    RptBind(this.StrWhere, "CouponTypeStoreConditionID");
                }

                if (this.Type == 1)
                {
                    this.btnDelete.Hidden = true;
                    this.btnNew.Hidden = true;
                    this.Grid1.EnableCheckBoxSelect = false;
                }
              
            }
        }

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference(string.Format("Delete.aspx?CouponTypeID={0}&StoreConditionTypeID={1}&ids={2}",this.CouponTypeID, this.StoreConditionTypeID, sb.ToString())));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(this.StrWhere, "CouponTypeStoreConditionID");
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind(this.StrWhere, "CouponTypeStoreConditionID");
        }
        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CouponTypeStoreCondition bll = new Edge.SVA.BLL.CouponTypeStoreCondition();
          
            //获得总条数
            Grid1.RecordCount = bll.GetCount(strWhere);
            this.btnDelete.Enabled = this.Grid1.RecordCount > 0;

            DataSet ds = new DataSet();
            ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

            Tools.DataTool.AddBrandName(ds, "BrandName", "ConditionID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "ConditionID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion


        #region 页面属性

        public int CouponTypeID
        {
            get
            {
                return Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString()); 
            }
        }

        public int StoreConditionTypeID
        {
            get
            {
                return Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString()); 
            }
        }

        public int Type
        {
            get
            {
                return Request.Params["type"] == null ? 1 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["type"].ToString()); 
            }
        }

        public int ConditionTypeID
        {
            get
            {
                return 1;
            }
        }

        public string StrWhere
        {
            get
            {
                StringBuilder sbWhere = new StringBuilder();
                sbWhere.Append(string.Format(" CouponTypeID={0}", this.CouponTypeID));
                sbWhere.Append(string.Format(" and StoreConditionType={0}", this.StoreConditionTypeID));
                sbWhere.Append(string.Format(" and ConditionType={0}", this.ConditionTypeID));

                return sbWhere.ToString();
            }
        }

        #endregion

    }
}
