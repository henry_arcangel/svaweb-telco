﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.File.BasicViewModel;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType,Edge.SVA.Model.CouponType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CouponTypeModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.rptEarnConsumeAmountList.PageSize = webset.ContentPageNum;

                Session["CouponTypeConsumeStoreCheckedList"] = null;
                Session["CouponTypeEarnStoreCheckedList"] = null;
     
                EarnCouponRuleRptBind("CouponTypeID=" + Request.Params["id"]);

                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);

                this.lbIssueBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CouponTypeID={0}&type=1&StoreConditionTypeID=1", Request.Params["id"]));
                this.lbIssueStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CouponTypeID={0}&type=1&StoreConditionTypeID=1", Request.Params["id"]));

                this.lbUseBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CouponTypeID={0}&type=1&StoreConditionTypeID=2", Request.Params["id"]));
                this.lbUseStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CouponTypeID={0}&type=1&StoreConditionTypeID=2", Request.Params["id"]));

                this.lbDeparment.OnClientClick = Window1.GetShowReference(string.Format("Department/List.aspx?Url=&CouponTypeID={0}&type=1", Request.Params["id"]));
                this.lbProduct.OnClientClick = Window1.GetShowReference(string.Format("Product/List.aspx?Url=&CouponTypeID={0}&type=1", Request.Params["id"]));
                this.lbSaleProduct.OnClientClick = Window1.GetShowReference(string.Format("SaleProduct/List.aspx?Url=&CouponTypeID={0}&type=1", Request.Params["id"]));

                this.Tab2.Hidden = this.Tab3.Hidden = this.Tab4.Hidden = this.Tab5.Hidden = this.Tab6.Hidden = true;

                BindSponsorID(this.SponsorID);
                BindSupplierID(this.SupplierID);
                BindingGrid_CouponNature(); //Add By Robin 2015-01-08
                SVASessionInfo.CouponTypeModifyController = null;
            }
            controller = SVASessionInfo.CouponTypeModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);
  
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = brand == null ? "":DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(Model.CampaignID.GetValueOrDefault());
                string campaignText = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                this.CampaignID.Text = campaign == null ? "" : ControlTool.GetDropdownListText(campaignText, campaign.CampaignCode);

                Edge.SVA.Model.Currency currency = new Edge.SVA.BLL.Currency().GetModel(Model.CurrencyID.GetValueOrDefault());
                string currencyText = currency == null ? "" : DALTool.GetStringByCulture(currency.CurrencyName1, currency.CurrencyName2, currency.CurrencyName3);
                this.CurrencyID.Text = currency == null ? "" : ControlTool.GetDropdownListText(currencyText, currency.CurrencyCode);

                Edge.SVA.Model.PasswordRule password = new Edge.SVA.BLL.PasswordRule().GetModel(Model.PasswordRuleID.GetValueOrDefault());
                string passwordText = password == null ? "" : DALTool.GetStringByCulture(password.Name1, password.Name2, password.Name3);
                this.PasswordRuleID.Text = password == null ? "" : ControlTool.GetDropdownListText(passwordText, password.PasswordRuleCode);

                this.IsMemberBindView.Text = this.IsMemberBind.SelectedItem == null ? "" : this.IsMemberBind.SelectedItem.Text;        
                this.CoupontypeFixedAmountView.Text = this.CoupontypeFixedAmount.SelectedItem == null ? "" : this.CoupontypeFixedAmount.SelectedItem.Text;
                this.CouponCheckdigitView.Text = this.CouponCheckdigit.SelectedItem == null ? "" : this.CouponCheckdigit.SelectedItem.Text;
                this.CouponValidityUnitView.Text = this.CouponValidityUnit.SelectedItem == null ? "" : this.CouponValidityUnit.SelectedItem.Text;
                this.CouponTypeTransferView.Text = this.CouponTypeTransfer.SelectedItem == null ? "" : this.CouponTypeTransfer.SelectedItem.Text;
                this.IsImportCouponNumberView.Text = this.IsImportCouponNumber.SelectedItem == null ? "" : this.IsImportCouponNumber.SelectedItem.Text;
                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CouponNumberToUIDView.Text = this.CouponNumberToUID.SelectedItem == null ? "" : this.CouponNumberToUID.SelectedItem.Text;
                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.UIDToCouponNumberView.Text = this.UIDToCouponNumber.SelectedItem == null ? "" : this.UIDToCouponNumber.SelectedItem.Text;
                this.ActiveResetExpiryDateView.Text = this.ActiveResetExpiryDate.SelectedItem == null ? "" : this.ActiveResetExpiryDate.SelectedItem.Text;
                this.CouponforfeitControlView.Text = this.CouponforfeitControl.SelectedItem == null ? "" : this.CouponforfeitControl.SelectedItem.Text;

                //add by Alex 2014-07-01 ++
                this.AllowDeleteCouponView.Text = this.AllowDeleteCoupon.SelectedItem == null ? "" : this.AllowDeleteCoupon.SelectedItem.Text;
                this.AllowDeleteCoupon.Visible = false;
                this.QRCodePrefixView.Text = this.QRCodePrefix.SelectedItem == null ? "" : this.QRCodePrefix.SelectedItem.Text;
                this.QRCodePrefix.Visible = false;

                this.AllowShareCouponView.Text = this.AllowShareCoupon.SelectedItem == null ? "" : this.AllowShareCoupon.SelectedItem.Text;
                this.AllowShareCoupon.Visible = false;

                this.UnlimitedUsageView.Text = this.UnlimitedUsage.SelectedItem == null ? "" : this.UnlimitedUsage.SelectedItem.Text;
                this.UnlimitedUsage.Visible = false;

                this.TrainingModeView.Text = this.TrainingMode.SelectedItem == null ? "" : this.TrainingMode.SelectedItem.Text;
                this.TrainingMode.Visible = false;

                //add by Alex 2014-07-01 --

                this.CouponTypeDiscount.Text = Model.CouponTypeDiscount.ToString().TrimEnd('M');
                this.SponsorIDView.Text = this.SponsorID.SelectedText;
                this.SponsoredValue.Text = Model.SponsoredValue.ToString().TrimEnd('M');
                this.SupplierIDView.Text = this.SupplierID.SelectedText;

                if (Model.IsImportCouponNumber.GetValueOrDefault() == 1)
                {
                    this.CheckDigitModeID2.SelectedValue = Model.CheckDigitModeID.GetValueOrDefault().ToString();
                    this.CheckDigitModeID2View.Text = this.CheckDigitModeID2.SelectedItem == null ? "" : this.CheckDigitModeID2.SelectedItem.Text;
                }

                //Check import or manual
                checkIsImportCouponNumber(false);

                this.IsMemberBind.Hidden = true;
                this.CoupontypeFixedAmount.Hidden = true;
                this.CouponCheckdigit.Hidden = true;
                this.CouponValidityUnit.Hidden = true;
                this.CouponTypeTransfer.Hidden = true;
                this.IsImportCouponNumber.Hidden = true;
                this.UIDCheckDigit.Hidden = true;
                this.CheckDigitModeID.Hidden = true;
                this.CheckDigitModeID2.Hidden = true;
                this.CouponNumberToUID.Hidden = true;
                this.IsConsecutiveUID.Hidden = true;
                this.UIDToCouponNumber.Hidden = true;
                this.ActiveResetExpiryDate.Hidden = true;
                this.CouponforfeitControl.Hidden = true;

                if (Model.IsImportCouponNumber.GetValueOrDefault() == 1 && Model.UIDToCouponNumber.GetValueOrDefault() == 0)
                {
                    this.CouponNumMaskImport.Text = Model.CouponNumMask;
                    this.CouponNumPatternImport.Text = Model.CouponNumPattern;
                }

                //Add.CheckImportCoupon(this.IsImportCouponNumber, gpManual, gpImport);
                if (Model != null)
                {
                    this.uploadFilePath.Text = Model.CouponTypeLayoutFile;
                    this.uploadFilePath1.Text = Model.CouponTypePicFile;

                    this.btnPreview1.OnClientClick = WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath1.Text, "图片");

                    this.btnExport.Hidden = string.IsNullOrEmpty(Model.CouponTypeLayoutFile) ? true : false;
                    this.btnPreview1.Hidden = string.IsNullOrEmpty(Model.CouponTypePicFile) ? true : false;

                    this.StartTime.Text = Model.CouponTypeStartDate.HasValue ? Model.CouponTypeStartDate.Value.ToString("HH:mm") : string.Empty;
                    this.EndTime.Text = Model.CouponTypeEndDate.HasValue ? Model.CouponTypeEndDate.Value.ToString("HH:mm") : string.Empty;
                    
                    controller.LoadViewModel(Model.CouponTypeID, SVASessionInfo.SiteLanguage);
                    BindCouponReplenishDailyViewModelListToForm(controller.ViewModel.CouponReplenishDailyViewModelList);
                    this.AutoReplenish.Enabled = false;
                    SetEnableWeekDayControls(false);

                    this.MemberClauseDesc1.Text = controller.ViewModel.MemberClause.MemberClauseDesc1;
                    this.MemberClauseDesc2.Text = controller.ViewModel.MemberClause.MemberClauseDesc2;
                    this.MemberClauseDesc3.Text = controller.ViewModel.MemberClause.MemberClauseDesc3;

                }
            }
        }

        protected override void SetObject()
        {
            base.SetObject(this.Model, this.SimpleForm1.Controls.GetEnumerator());
        }

        private void EarnCouponRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new Edge.SVA.BLL.EarnCouponRule();
            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            Edge.Web.Tools.DataTool.AddExchangeType(ds, "ExchangeTypeName", "ExchangeType");
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");

            DataView consumeAmountDV = ds.Tables[0].DefaultView;
            consumeAmountDV.RowFilter = "ExchangeType='5'";
            //this.lbtnEarnConsumeAmountDel.Enabled = consumeAmountDV.Count > 0 ? true : false;
            this.rptEarnConsumeAmountList.DataSource = consumeAmountDV;
            this.rptEarnConsumeAmountList.DataBind();

            DataView pointDV = ds.Tables[0].DefaultView;
            pointDV.RowFilter = "ExchangeType='2'";
            //this.lbtnEarnPointDel.Enabled = pointDV.Count > 0 ? true : false;
            this.rptEarnPointList.DataSource = pointDV;
            this.rptEarnPointList.DataBind();

            DataView amountDV = ds.Tables[0].DefaultView;
            amountDV.RowFilter = "ExchangeType='1'";
            //this.lbtnEarnAmountDel.Enabled = amountDV.Count > 0 ? true : false;
            this.rptEarnAmountList.DataSource = amountDV;
            this.rptEarnAmountList.DataBind();

            DataView couponDV = ds.Tables[0].DefaultView;
            couponDV.RowFilter = "ExchangeType='4'";
            //this.lbtnEarnCouponDel.Enabled = couponDV.Count > 0 ? true : false;
            this.rptEarnCouponList.DataSource = couponDV;
            this.rptEarnCouponList.DataBind();

            DataView amountPointDV = ds.Tables[0].DefaultView;
            amountPointDV.RowFilter = "ExchangeType='3'";
            //this.lbtnEarnAmountPointDel.Enabled = amountPointDV.Count > 0 ? true : false;
            this.rptEarnAmountPointList.DataSource = amountPointDV;
            this.rptEarnAmountPointList.DataBind();
        }

        private void checkIsImportCouponNumber(bool clearText)
        {
            if (this.IsImportCouponNumber.SelectedValue == "1")
            {
                this.gpManual.Hidden = true;
                this.gpImport.Hidden = false;

                checkCheckDigitMode4Import();
            }
            else
            {
                this.gpManual.Hidden = false;
                this.gpImport.Hidden = true;

                checkCheckDigitMode4Manual();
            }

            if (clearText)
            {
                this.CouponNumMask.Text = "";
                this.CouponNumPattern.Text = "";
                this.CouponNumMaskImport.Text = "";
                this.CouponNumPatternImport.Text = "";
            }
        }

        public void checkCheckDigitMode4Manual()
        {
            //Check select CD
            if (CouponCheckdigit.SelectedValue == "True")
            {
                //Check Digit Mode
                CheckDigitModeIDView.Hidden = false;
            }
            else
            {
                //Check Digit Mode
                if (CouponNumberToUID.SelectedValue == "3" && CouponCheckdigit.SelectedValue == "False")
                {
                    CheckDigitModeIDView.Hidden = false;
                }
                else
                {
                    CheckDigitModeIDView.Hidden = true;
                }
            }
        }

        public void checkCheckDigitMode4Import()
        {
            //Check select CD
            if (UIDCheckDigit.SelectedValue == "1")
            {
                //Check Digit Mode
                CheckDigitModeID2View.Hidden = false;
            }
            else
            {
                //Check Digit Mode
                if (UIDToCouponNumber.SelectedValue == "3")
                {
                    CheckDigitModeID2View.Hidden = false;
                }
                else
                {
                    CheckDigitModeID2View.Hidden = true;
                }
            }

            //Check CouponNumMask
            if (UIDToCouponNumber.SelectedValue == "0")
            {
                CouponNumMaskImport.Hidden = false;
                CouponNumPatternImport.Hidden = false;
            }
            else
            {
                CouponNumMaskImport.Hidden = true;
                CouponNumPatternImport.Hidden = true;
            }
        }

        #region CouponReplenishDaily
        private List<CouponReplenishDailyViewModel> GetCouponReplenishDailyViewModelListFromForm()
        {
            List<CouponReplenishDailyViewModel> list = new List<CouponReplenishDailyViewModel>();
            CouponReplenishDailyViewModel model;
            string date = DateTime.Today.ToString("yyyy-MM-dd ");
            if (this.WeekDay1.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 1;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime1.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime1.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity1.Text);
                list.Add(model);
            }
            if (this.WeekDay2.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 2;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime2.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime2.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity2.Text);
                list.Add(model);
            }
            if (this.WeekDay3.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 3;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime3.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime3.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity3.Text);
                list.Add(model);
            }
            if (this.WeekDay4.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 4;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime4.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime4.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity4.Text);
                list.Add(model);
            }
            if (this.WeekDay5.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 5;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime5.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime5.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity5.Text);
                list.Add(model);
            }
            if (this.WeekDay6.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 6;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime6.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime6.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity6.Text);
                list.Add(model);
            }
            if (this.WeekDay7.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 7;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime7.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime7.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity7.Text);
                list.Add(model);
            }
            return list;
        }
        private void BindCouponReplenishDailyViewModelListToForm(List<CouponReplenishDailyViewModel> list)
        {
            foreach (var item in list)
            {
                int? weekdaynum = item.MainTable.WeekDayNum;
                string starttimeStr = item.MainTable.StartTime.HasValue ? item.MainTable.StartTime.Value.ToString("HH:mm") : "00:00";
                string endtimeStr = item.MainTable.EndTime.HasValue ? item.MainTable.EndTime.Value.ToString("HH:mm") : "23:59";
                string quantityStr = item.MainTable.MaxReplenishCount.ToString();
                switch (weekdaynum)
                {
                    case 1:
                        this.WeekDay1.Checked = true;
                        //this.WeekDayStartTime1.Text = starttimeStr;
                        //this.WeekDayEndTime1.Text = endtimeStr;
                        this.WeekDayCouponQuantity1.Text = quantityStr;
                        break;
                    case 2:
                        this.WeekDay2.Checked = true;
                        //this.WeekDayStartTime2.Text = starttimeStr;
                        //this.WeekDayEndTime2.Text = endtimeStr;
                        this.WeekDayCouponQuantity2.Text = quantityStr;
                        break;
                    case 3:
                        this.WeekDay3.Checked = true;
                        //this.WeekDayStartTime3.Text = starttimeStr;
                        //this.WeekDayEndTime3.Text = endtimeStr;
                        this.WeekDayCouponQuantity3.Text = quantityStr;
                        break;
                    case 4:
                        this.WeekDay4.Checked = true;
                        //this.WeekDayStartTime4.Text = starttimeStr;
                        //this.WeekDayEndTime4.Text = endtimeStr;
                        this.WeekDayCouponQuantity4.Text = quantityStr;
                        break;
                    case 5:
                        this.WeekDay5.Checked = true;
                        //this.WeekDayStartTime5.Text = starttimeStr;
                        //this.WeekDayEndTime5.Text = endtimeStr;
                        this.WeekDayCouponQuantity5.Text = quantityStr;
                        break;
                    case 6:
                        this.WeekDay6.Checked = true;
                        //this.WeekDayStartTime6.Text = starttimeStr;
                        //this.WeekDayEndTime6.Text = endtimeStr;
                        this.WeekDayCouponQuantity6.Text = quantityStr;
                        break;
                    case 7:
                        this.WeekDay7.Checked = true;
                        //this.WeekDayStartTime7.Text = starttimeStr;
                        //this.WeekDayEndTime7.Text = endtimeStr;
                        this.WeekDayCouponQuantity7.Text = quantityStr;
                        break;
                    default:
                        break;
                }
            }
        }
        protected void CheckAll_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.CheckAll.Checked;
            this.WeekDay1.Checked = check;
            this.WeekDay2.Checked = check;
            this.WeekDay3.Checked = check;
            this.WeekDay4.Checked = check;
            this.WeekDay5.Checked = check;
            this.WeekDay6.Checked = check;
            this.WeekDay7.Checked = check;
        }
        protected void AutoReplenish_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.AutoReplenish.SelectedValue == "1")
            {
                SetEnableWeekDayControls(true);
            }
            else
            {
                SetEnableWeekDayControls(false);
            }
        }
        private void SetEnableWeekDayControls(bool bol)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(this.CheckAll);
            tool.AddControl(this.WeekDay1);
            tool.AddControl(this.WeekDay2);
            tool.AddControl(this.WeekDay3);
            tool.AddControl(this.WeekDay4);
            tool.AddControl(this.WeekDay5);
            tool.AddControl(this.WeekDay6);
            tool.AddControl(this.WeekDay7);
            tool.AddControl(this.WeekDayCouponQuantity1);
            tool.AddControl(this.WeekDayCouponQuantity2);
            tool.AddControl(this.WeekDayCouponQuantity3);
            tool.AddControl(this.WeekDayCouponQuantity4);
            tool.AddControl(this.WeekDayCouponQuantity5);
            tool.AddControl(this.WeekDayCouponQuantity6);
            tool.AddControl(this.WeekDayCouponQuantity7);
            tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, bol);
        }
        //private void SetHideWeekDayControls(bool hidden)
        //{
        //    this.WeekDayStartTime1.Hidden = hidden;
        //    this.WeekDayStartTime2.Hidden = hidden;
        //    this.WeekDayStartTime3.Hidden = hidden;
        //    this.WeekDayStartTime4.Hidden = hidden;
        //    this.WeekDayStartTime5.Hidden = hidden;
        //    this.WeekDayStartTime6.Hidden = hidden;
        //    this.WeekDayStartTime7.Hidden = hidden;

        //    this.WeekDayEndTime1.Hidden = hidden;
        //    this.WeekDayEndTime2.Hidden = hidden;
        //    this.WeekDayEndTime3.Hidden = hidden;
        //    this.WeekDayEndTime4.Hidden = hidden;
        //    this.WeekDayEndTime5.Hidden = hidden;
        //    this.WeekDayEndTime6.Hidden = hidden;
        //    this.WeekDayEndTime7.Hidden = hidden;
        //}
        #endregion

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CuponGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("CardCaede Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CuponGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        //#region CouponReplenishDaily
        //private List<CouponReplenishDailyViewModel> GetCouponReplenishDailyViewModelListFromForm()
        //{
        //    List<CouponReplenishDailyViewModel> list = new List<CouponReplenishDailyViewModel>();
        //    CouponReplenishDailyViewModel model;
        //    string date = DateTime.Today.ToString("yyyy-MM-dd ");
        //    if (this.WeekDay1.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 1;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime1.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime1.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity1.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay2.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 2;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime2.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime2.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity2.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay3.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 3;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime3.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime3.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity3.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay4.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 4;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime4.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime4.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity4.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay5.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 5;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime5.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime5.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity5.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay6.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 6;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime6.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime6.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity6.Text);
        //        list.Add(model);
        //    }
        //    if (this.WeekDay7.Checked)
        //    {
        //        model = new CouponReplenishDailyViewModel();
        //        model.MainTable.WeekDayNum = 7;
        //        model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime7.Text);
        //        model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime7.Text);
        //        model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity7.Text);
        //        list.Add(model);
        //    }
        //    return list;
        //}
        //private void BindCouponReplenishDailyViewModelListToForm(List<CouponReplenishDailyViewModel> list)
        //{
        //    foreach (var item in list)
        //    {
        //        int? weekdaynum = item.MainTable.WeekDayNum;
        //        string starttimeStr = item.MainTable.StartTime.HasValue ? item.MainTable.StartTime.Value.ToString("HH:mm") : "00:00";
        //        string endtimeStr = item.MainTable.EndTime.HasValue ? item.MainTable.EndTime.Value.ToString("HH:mm") : "23:59";
        //        string quantityStr = item.MainTable.MaxReplenishCount.ToString();
        //        switch (weekdaynum)
        //        {
        //            case 1:
        //                this.WeekDay1.Checked = true;
        //                this.WeekDayStartTime1.Text = starttimeStr;
        //                this.WeekDayEndTime1.Text = endtimeStr;
        //                this.WeekDayCouponQuantity1.Text = quantityStr;
        //                break;
        //            case 2:
        //                this.WeekDay2.Checked = true;
        //                this.WeekDayStartTime2.Text = starttimeStr;
        //                this.WeekDayEndTime2.Text = endtimeStr;
        //                this.WeekDayCouponQuantity2.Text = quantityStr;
        //                break;
        //            case 3:
        //                this.WeekDay3.Checked = true;
        //                this.WeekDayStartTime3.Text = starttimeStr;
        //                this.WeekDayEndTime3.Text = endtimeStr;
        //                this.WeekDayCouponQuantity3.Text = quantityStr;
        //                break;
        //            case 4:
        //                this.WeekDay4.Checked = true;
        //                this.WeekDayStartTime4.Text = starttimeStr;
        //                this.WeekDayEndTime4.Text = endtimeStr;
        //                this.WeekDayCouponQuantity4.Text = quantityStr;
        //                break;
        //            case 5:
        //                this.WeekDay5.Checked = true;
        //                this.WeekDayStartTime5.Text = starttimeStr;
        //                this.WeekDayEndTime5.Text = endtimeStr;
        //                this.WeekDayCouponQuantity5.Text = quantityStr;
        //                break;
        //            case 6:
        //                this.WeekDay6.Checked = true;
        //                this.WeekDayStartTime6.Text = starttimeStr;
        //                this.WeekDayEndTime6.Text = endtimeStr;
        //                this.WeekDayCouponQuantity6.Text = quantityStr;
        //                break;
        //            case 7:
        //                this.WeekDay7.Checked = true;
        //                this.WeekDayStartTime7.Text = starttimeStr;
        //                this.WeekDayEndTime7.Text = endtimeStr;
        //                this.WeekDayCouponQuantity7.Text = quantityStr;
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}
        //protected void CheckAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    bool check = this.CheckAll.Checked;
        //    this.WeekDay1.Checked = check;
        //    this.WeekDay2.Checked = check;
        //    this.WeekDay3.Checked = check;
        //    this.WeekDay4.Checked = check;
        //    this.WeekDay5.Checked = check;
        //    this.WeekDay6.Checked = check;
        //    this.WeekDay7.Checked = check;
        //}
        //#endregion

        protected void BindSponsorID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Sponsor bll = new SVA.BLL.Sponsor();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SponsorID", "SponsorName1", "SponsorName2", "SponsorName3", "SponsorCode");
        }

        protected void BindSupplierID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Supplier bll = new SVA.BLL.Supplier();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SupplierID", "SupplierDesc1", "SupplierDesc2", "SupplierDesc3", "SupplierCode");
        }

        //Add By Robin 2015-01-07 
        #region CouponNature 优惠券类别
        protected void Grid_CouponNature_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid_CouponNatureList.PageIndex = e.NewPageIndex;
            this.BindingGrid_CouponNature();
        }

        private void BindingGrid_CouponNature()
        {
            ViewState["sotrID"] = Request["id"];
            string sotrID = "";
            if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                sotrID = ViewState["sotrID"].ToString();


            Edge.SVA.BLL.CouponType coupontype = new SVA.BLL.CouponType();
            DataSet ds = coupontype.GetCouponNatureList(sotrID);

            if (ds != null)
            {
                this.Grid_CouponNatureList.DataSource = ds.Tables[0].DefaultView;
                this.Grid_CouponNatureList.DataBind();
            }
            else
            {
                this.Grid_CouponNatureList.Reset();
            }
        }

        #endregion
        //End
    }
}
