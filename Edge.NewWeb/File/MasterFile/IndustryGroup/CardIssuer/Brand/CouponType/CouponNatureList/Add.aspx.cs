﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model.Domain.File;
using Edge.Utils.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.CouponNatureList
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponNatureList, Edge.SVA.Model.CouponNatureList>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["RuleType"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.RuleType.Text = Request["RuleType"].ToString();
                }
                if (Request["PageFlag"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.PageFlag.Text = Request["PageFlag"].ToString();
                }
                string CouponTypeID = Request["CouponTypeID"].ToString();

                if (Request["CouponTypeID"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    ViewState["CouponTypeID"] = Request["CouponTypeID"];
                    setCouponTypeCode(CouponTypeID);
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCouponNature(CouponNatureID, "1 = 1");

            }
        }

        private void setCouponTypeCode(string CouponTypeID)
        {
            Edge.Web.Tools.ControlTool.BindCouponType(this.CouponTypeID);
            this.CouponTypeID.SelectedValue = CouponTypeID;
            this.CouponTypeID.Enabled = false;
            
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int _CouponTypeID = StringHelper.ConvertToInt(ViewState["CouponTypeID"].ToString());
            int _CouponNatureID = StringHelper.ConvertToInt(this.CouponNatureID.SelectedValue);
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.IsHasCouponTypeCodeNatureCode(ViewState["CouponTypeID"].ToString(), _CouponNatureID))
            {
                ShowWarning(Resources.MessageTips.ExistCouponNatureCode);
                this.CouponNatureID.Focus();
                return;
            }

            Edge.SVA.Model.CouponNatureList item = this.GetAddObject();

           
            if (item != null)
            {
                item.CouponTypeID = _CouponTypeID;
                item.CouponNatureID = _CouponNatureID;
               
            }

            if (DALTool.Add<Edge.SVA.BLL.CouponNatureList>(item) > 0)
            {
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

    }
}