﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
	{
        if (!this.IsPostBack)
        {
            try
            {
                if (!hasRight)
                {
                    return;
                }
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, "", FineUI.MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Delete " + ids);
                foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {

                    if (string.IsNullOrEmpty(id)) continue;
                    string msg = "";
                    if (!Tools.DALTool.isCanDeleteCouponType(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                    {
                        FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", FineUI.MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        return;
                    }
                    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.CouponType>(Tools.ConvertTool.ToInt(id));
                }

                FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
            }
            catch (System.Exception ex)
            {
                logger.WriteErrorLog(this.PageName, "Delete", ex);
                Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
            }
        }
	}
    }
}
