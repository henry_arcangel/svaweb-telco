﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:SimpleForm ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1,SimpleForm2,SimpleForm3,SimpleForm4,SimpleForm5,SimpleForm6,SimpleForm7"
                        Icon="SystemSaveClose" OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
                <Items>
                    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CouponTypeCode" runat="server" Label="优惠劵类型编号：" Required="true"
                                ShowRedStar="true" MaxLength="20" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                                RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" ToolTipTitle="优惠劵类型编号"
                                ToolTip="Translate__Special_121_StartKey identifier of Coupon Type. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CouponTypeName1" runat="server" Label="描述：" Required="true" ShowRedStar="true"
                                MaxLength="512" ToolTipTitle="描述" ToolTip="请输入规范的優惠券類型的名称。不能超過512個字符" 
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CouponTypeName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                                ToolTip="對優惠券類型的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CouponTypeName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                                ToolTip="對優惠券類型的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                           <%-- Add by Nathan 20140702 ++ --%>
                            <ext:DropDownList ID="CouponTypeNatureID" runat="server" Label="优惠券类型性质：" AutoPostBack="true"
                                Required="true" ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1"
                                CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                <ext:ListItem Text="优惠券" Value="0" />
                                <ext:ListItem Text="邮票" Value="4" />
                            </ext:DropDownList>
                            <%-- Add by Nathan 20140702 -- --%>
                            <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" AutoPostBack="true" OnSelectedIndexChanged="BrandID_SelectedIndexChanged"
                                Required="true" ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:HtmlEditor ID="MemberClauseDesc1" runat="server" Height="200" Label="条款条例1：" ShowRedStar="true"></ext:HtmlEditor>
                            <ext:HtmlEditor ID="MemberClauseDesc2" runat="server" Height="200" Label="条款条例2："></ext:HtmlEditor>
                            <ext:HtmlEditor ID="MemberClauseDesc3" runat="server" Height="200" Label="条款条例3："></ext:HtmlEditor>
                            <ext:DropDownList ID="CampaignID" runat="server" Label="活动：" Hidden="true" Resizable="true">
                            </ext:DropDownList>
                            <ext:RadioButtonList ID="CoupontypeFixedAmount" runat="server" Label="是否固定面额：" AutoPostBack="True"
                                OnSelectedIndexChanged="CoupontypeFixedAmount_SelectedIndexChanged">
                                <ext:RadioItem Text="是的" Value="1" />
                                <ext:RadioItem Text="不是" Value="0" Selected="true" />
                            </ext:RadioButtonList>
                            <ext:NumberBox ID="CouponTypeAmount" runat="server" Label="面额：" NoDecimal="false"
                                ToolTipTitle="面额" ToolTip="请输入正數，最大兩位小數" NoNegative="true" MaxValue="100000000">
                            </ext:NumberBox>
                            <ext:DropDownList ID="CurrencyID" runat="server" Label="币种：" Required="true" ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:Label ID="label1" runat="server" Label="销售货号匹配清单：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label2" runat="server" Label="指定限购商品：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label3" runat="server" Label="指定限购部门：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Form ID="Form4" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow17" ColumnWidths="70% 15% 15%" runat="server">
                                        <Items>
                                            <ext:FileUpload ID="CouponTypeLayoutFile" runat="server" Label="优惠券布局：" ToolTipTitle="优惠券布局"
                                                ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End">
                                            </ext:FileUpload>
                                            <ext:Label ID="label" runat="server" HideMode="Display" Hidden="true"></ext:Label>
                                            <ext:Button ID="DeleteLayoutFile" runat="server" Text="删除" Icon="Delete" OnClick="DeleteLayout_OnClick"></ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:HiddenField ID="uploadFilePath" runat="server"></ext:HiddenField>
                            <ext:Form ID="Form3" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow16" ColumnWidths="70% 15% 15%" runat="server">
                                        <Items>
                                            <ext:FileUpload ID="CouponTypePicFile" runat="server" Label="优惠券图片：" ToolTipTitle="优惠券图片"
                                                ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                                            </ext:FileUpload>
                                            <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true"></ext:Button>
                                            <ext:Button ID="DeletePicFile" runat="server" Text="删除" Icon="Delete" OnClick="DeletePic_OnClick"></ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>

                            <ext:RadioButtonList ID="IsMemberBind" runat="server" Label="是否会员优惠券：">
                                <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <ext:Label ID="label4" runat="server" Label="发行品牌：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label5" runat="server" Label="发行店铺：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label6" runat="server" Label="使用品牌：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label7" runat="server" Label="使用店铺：" Text="请新增成功优惠券类型后添加该项。">
                            </ext:Label>
                            <ext:DropDownList ID="PasswordRuleID" runat="server" Required="true" ShowRedStar="true"
                                Label="密码规则：" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CouponforfeitControl" runat="server" TabIndex="15" 
                                Label="优惠券面额清空控制：" Resizable="true">
                                <ext:ListItem Value="0" Text="直接清空" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="1" Text="当日清空"></ext:ListItem>
                                <ext:ListItem Value="2" Text="不清空"></ext:ListItem>
                            </ext:DropDownList>
                            <ext:NumberBox ID="CouponTypeRedeemCount" runat="server" Label="可用数量：" NoNegative="true" NoDecimal="true" MaxValue="100000000"
                                ToolTipTitle="可用数量" ToolTip="必須输入數字，並且是整數" Text="999999">
                            </ext:NumberBox>
                            <ext:NumberBox ID="CouponTypeDiscount" runat="server" Label="优惠券折扣：" NoDecimal="false" DecimalPrecision="6">
                            </ext:NumberBox>
                            <ext:DropDownList ID="SponsorID" runat="server" Label="赞助商：" Resizable="true">
                            </ext:DropDownList>
                            <ext:NumberBox ID="SponsoredValue" runat="server" Label="赞助比例："  NoDecimal="false" DecimalPrecision="6"></ext:NumberBox>
                            <ext:DropDownList ID="SupplierID" runat="server" Label="供货商：" Resizable="true">
                            </ext:DropDownList>
                              <%--Add by Alex 2014-06-30 ++--%>
                            <ext:RadioButtonList ID="AllowDeleteCoupon" runat="server" Label="Translate__Special_121_Start 删除优惠券（是 / 否）：Translate__Special_121_End">
                                <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="AllowShareCoupon" runat="server" Label="Translate__Special_121_Start 分享优惠券（是 / 否）：Translate__Special_121_End">
                                <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <ext:NumberBox ID="MaxDownloadCoupons" runat="server" Label="最大优惠券每次的下载数量："
                                NoNegative="true" NoDecimal="true" MaxValue="99" ToolTipTitle="可用数量" ToolTip="必須输入數字，並且是整數"
                                Text="0">
                            </ext:NumberBox>
                            <ext:NumberBox ID="CouponReturnValue" runat="server" Label="Translate__Special_121_Start 优惠券退回数值（天）：Translate__Special_121_End"
                                NoNegative="true" NoDecimal="true" MaxValue="99" ToolTipTitle="可用数量" ToolTip="必須输入數字，並且是整數"
                                Text="0">
                            </ext:NumberBox>
                            <ext:RadioButtonList ID="UnlimitedUsage" runat="server" Label="无限使用：">
                                <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="TrainingMode" runat="server" Label="是否训练优惠券：">
                                <ext:RadioItem Text="是的" Value="1"></ext:RadioItem>
                                <ext:RadioItem Text="不是" Value="0" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <%--Add by Alex 2014-06-30 ++--%>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="优惠券号码规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:RadioButtonList ID="IsImportCouponNumber" runat="server" Label="优惠券号码是否导入："
                                AutoPostBack="true" OnSelectedIndexChanged="IsImportCouponNumber_SelectedIndexChanged">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="true" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="gpManual" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm3" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                           <%--Add by Alex 2014-06-30 ++--%>
                            <ext:DropDownList ID="QRCodePrefix" runat="server" Label="Translate__Special_121_Start QR码前缀号码：Translate__Special_121_End" Resizable="true">
                                        <ext:ListItem Value="" Text="-------" />
                                      <ext:ListItem Value="Q7FC" Text="Q7FC" />
                                       <ext:ListItem Value="Q7FI" Text="Q7FI" />
                            </ext:DropDownList>
                            <%--Add by Alex 2014-06-30 ++--%>
                            <ext:TextBox ID="CouponNumMask" runat="server" MaxLength="30" Label="优惠券号码编号规则："
                                ShowRedStar="true" Regex="(^A*9+$)|(^9+$)" RegexMessage="编码规则输入不正确" AutoPostBack="True"
                                OnTextChanged="CouponNumMask_TextChanged" ToolTipTitle="优惠券号码编号规则" ToolTip="Translate__Special_121_Start 1~30個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CouponNumPattern" runat="server" MaxLength="20" Label="优惠券号码编号前缀号码："
                                ShowRedStar="true" RegexPattern="NUMBER" RegexMessage="编码规则输入不正确" AutoPostBack="True"
                                OnTextChanged="CouponNumPattern_TextChanged" ToolTipTitle="优惠券号码编号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="CouponCheckdigit" runat="server" Label="是否包含校验位：" AutoPostBack="True"
                                OnSelectedIndexChanged="CouponCheckdigit_SelectedIndexChanged">
                                <ext:RadioItem Text="是" Value="True"></ext:RadioItem>
                                <ext:RadioItem Text="否" Value="False" Selected="True"></ext:RadioItem>
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px" Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="CouponNumberToUID" runat="server" Label="Translate__Special_121_Start是否优惠券号码复制到优惠券物理编号（是/否）：Translate__Special_121_End"
                                AutoPostBack="True" OnSelectedIndexChanged="CouponNumberToUID_SelectedIndexChanged" Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制"></ext:ListItem>
                                <ext:ListItem Value="0" Text="绑定"></ext:ListItem>
                                <ext:ListItem Value="2" Text="复制去掉校验位"></ext:ListItem>
                                <ext:ListItem Value="3" Text="复制加上校验位"></ext:ListItem>
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="gpImport" runat="server" EnableCollapse="True" Title="导入物理编号规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm4" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CouponNumMaskImport" runat="server" MaxLength="20" ShowRedStar="true"
                                Label="优惠券号码编号规则：" Regex="(^A*9+$)|(^9+$)" RegexMessage="编码规则输入不正确" AutoPostBack="True"
                                OnTextChanged="CouponNumMaskImport_TextChanged" ToolTipTitle="优惠券号码编号规则" ToolTip="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CouponNumPatternImport" runat="server" MaxLength="20" Label="优惠券号码编号前缀号码："
                                RegexPattern="NUMBER" RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="True"
                                OnTextChanged="CouponNumPatternImport_TextChanged" ToolTipTitle="优惠券号码编号前缀号码"
                                ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End">
                                <ext:RadioItem Text="是" Value="1" Selected="true" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                AutoPostBack="True" OnSelectedIndexChanged="UIDCheckDigit_SelectedIndexChanged">
                                <ext:RadioItem Text="是" Value="1" Selected="true" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：" Width="100px" Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="UIDToCouponNumber" runat="server" Label="Translate__Special_121_Start是否优惠券物理编号复制到优惠券号码：（是/否）：Translate__Special_121_End"
                                AutoPostBack="True" OnSelectedIndexChanged="UIDToCouponNumber_SelectedIndexChanged" Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制"></ext:ListItem>
                                <ext:ListItem Value="0" Text="绑定"></ext:ListItem>
                                <ext:ListItem Value="2" Text="复制去掉校验位"></ext:ListItem>
                                <ext:ListItem Value="3" Text="复制加上校验位"></ext:ListItem>
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="有效期规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm5" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="CouponValidityDuration" runat="server" Label="延长有效期值：" MaxLength="3"
                                NoDecimal="true" NoNegative="true" ShowRedStar="true" ToolTipTitle="延长有效期值" ToolTip="必須输入數字，並且是整數">
                            </ext:NumberBox>
                            <ext:DropDownList ID="CouponValidityUnit" runat="server" Label="延长有效期的单位：" AutoPostBack="True"
                                OnSelectedIndexChanged="CouponValidityUnit_SelectedIndexChanged" Resizable="true">
                                <ext:ListItem Value="1" Text="年" Selected="True"></ext:ListItem>
                                <ext:ListItem Value="2" Text="月"></ext:ListItem>
                                <ext:ListItem Value="3" Text="星期"></ext:ListItem>
                                <ext:ListItem Value="4" Text="天"></ext:ListItem>
                                <ext:ListItem Value="6" Text="指定失效日期"></ext:ListItem>
                            </ext:DropDownList>
                            <ext:DatePicker ID="CouponSpecifyExpiryDate" runat="server" Label="优惠券固定过期日期：" DateFormatString="yyyy-MM-dd"
                                ToolTipTitle="优惠券固定过期日期" ToolTip="日期格式：YYYY-MM-DD">
                            </ext:DatePicker>
                            <ext:RadioButtonList ID="ActiveResetExpiryDate" runat="server" Label="激活是否重置有效期：">
                                <ext:RadioItem Text="是" Value="1" Selected="true" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="优惠券转增规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm6" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:DropDownList ID="CouponTypeTransfer" runat="server" Label="优惠劵是否可以转赠：" Resizable="true">
                                <ext:ListItem Value="0" Text="不允许"></ext:ListItem>
                                <ext:ListItem Value="1" Text="同品牌转赠"></ext:ListItem>
                                <ext:ListItem Value="2" Text="同卡级别转赠"></ext:ListItem>
                                <ext:ListItem Value="3" Text="同卡类型转赠"></ext:ListItem>
                                <ext:ListItem Value="4" Text="任意转赠"></ext:ListItem>
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="有效日期设定">
                <Items>
                    <ext:SimpleForm ID="SimpleForm7" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:DatePicker ID="CouponTypeStartDate" runat="server" Label="优惠劵类型启用日期：" DateFormatString="yyyy-MM-dd"
                                ToolTipTitle="优惠劵类型启用日期" ToolTip="日期格式：YYYY-MM-DD">
                            </ext:DatePicker>
                            <ext:DatePicker ID="CouponTypeEndDate" runat="server" Label="优惠劵类型结束日期：" DateFormatString="yyyy-MM-dd"
                                ToolTipTitle="优惠劵类型结束日期" ToolTip="日期格式：YYYY-MM-DD">
                            </ext:DatePicker>
                            <ext:TimePicker ID="StartTime" runat="server" Label="启用时间：" Text="00:00" Increment="120">
                            </ext:TimePicker>
                            <ext:TimePicker ID="EndTime" runat="server" Label="结束时间：" Text="23:59" Increment="120">
                            </ext:TimePicker>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="自动创建规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm8" ShowBorder="false" ShowHeader="false" runat="server"
                        EnableBackgroundColor="true" Title="" LabelAlign="Right">
                        <Items>
                            <ext:RadioButtonList ID="AutoReplenish" runat="server" Label="是否自动补货：" AutoPostBack="true" OnSelectedIndexChanged="AutoReplenish_OnSelectedIndexChanged">
                                <ext:RadioItem Text="是" Value="1"/>
                                <ext:RadioItem Text="否" Value="0" Selected="true"/>
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                    <ext:Form ID="DailySettingForm" runat="server" ShowBorder="false" ShowHeader="false" Title="每日设置"
                        EnableBackgroundColor="true" BodyPadding="5">
                        <Rows>
                            <ext:FormRow ID="FormRow0" runat="server">
                                <Items>
                                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow15" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="CheckAll" ShowLabel="true" Text="全选" AutoPostBack="true"
                                                        OnCheckedChanged="CheckAll_CheckedChanged">
                                                    </ext:CheckBox>
                                                    <%--<ext:Label ID="Label9" runat="server" ShowLabel="true" Label="" Text="开始时间" Hidden="true">
                                                    </ext:Label>
                                                    <ext:Label ID="Label10" runat="server" ShowLabel="true" Label="" Text="结束时间" Hidden="true">
                                                    </ext:Label>--%>
                                                    <ext:Label ID="Label11" runat="server" ShowLabel="true" Label="常规库存" Text="">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow1" runat="server">
                                <Items>
                                    <ext:Form ID="Form11" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <%--LabelWidth="50"--%>
                                        <Rows>
                                            <ext:FormRow ID="FormRow14" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay1" ShowLabel="true" Text="星期一">
                                                    </ext:CheckBox>
                                                    <%--<ext:TimePicker ID="WeekDayStartTime1" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00" Hidden="true">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime1" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59" Hidden="true">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity1" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow2" runat="server">
                                <Items>
                                    <ext:Form ID="Form10" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow13" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay2" ShowLabel="true" Text="星期二">
                                                    </ext:CheckBox>
                                                    <%-- <ext:TimePicker ID="WeekDayStartTime2" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime2" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity2" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow3" runat="server">
                                <Items>
                                    <ext:Form ID="Form9" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow12" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay3" ShowLabel="true" Text="星期三">
                                                    </ext:CheckBox>
                                                    <%--<ext:TimePicker ID="WeekDayStartTime3" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime3" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity3" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow4" runat="server">
                                <Items>
                                    <ext:Form ID="Form8" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow11" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay4" ShowLabel="true" Text="星期四">
                                                    </ext:CheckBox>
                                                    <%-- <ext:TimePicker ID="WeekDayStartTime4" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime4" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity4" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow5" runat="server">
                                <Items>
                                    <ext:Form ID="Form7" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow10" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay5" ShowLabel="true" Text="星期五">
                                                    </ext:CheckBox>
                                                    <%-- <ext:TimePicker ID="WeekDayStartTime5" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime5" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity5" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow6" runat="server">
                                <Items>
                                    <ext:Form ID="Form6" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow9" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay6" ShowLabel="true" Text="星期六">
                                                    </ext:CheckBox>
                                                    <%-- <ext:TimePicker ID="WeekDayStartTime6" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime6" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity6" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow7" runat="server">
                                <Items>
                                    <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true"
                                        Title="">
                                        <Rows>
                                            <ext:FormRow ID="FormRow8" runat="server">
                                                <Items>
                                                    <ext:CheckBox runat="server" ID="WeekDay7" ShowLabel="true" Text="星期日">
                                                    </ext:CheckBox>
                                                    <%-- <ext:TimePicker ID="WeekDayStartTime7" runat="server" ShowLabel="false" Increment="120"
                                                        Text="00:00">
                                                    </ext:TimePicker>
                                                    <ext:TimePicker ID="WeekDayEndTime7" runat="server" ShowLabel="false" Increment="120"
                                                        Text="23:59">
                                                    </ext:TimePicker>--%>
                                                    <ext:NumberBox ID="WeekDayCouponQuantity7" runat="server" ShowLabel="false" Text="0"
                                                        NoDecimal="true" NoNegative="true" MaxValue="100000000">
                                                    </ext:NumberBox>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="优惠券类别" Hidden=true>
                        <Items>
                            <ext:Grid ID="Grid_CouponNatureList" ShowBorder="true" ShowHeader="true" Title="优惠券类别列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_CouponNature_OnPageIndexChange">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar3" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                            </ext:Button>
                                            <ext:Button ID="btnDelete" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete_OnClick">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField ColumnID="CouponNatureCode" DataField="CouponNatureCode" HeaderText="优惠券类别编号" />
                                    <ext:BoundField ColumnID="CouponNatureName" DataField="CouponNatureName1" HeaderText="优惠券类别名称" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPicture" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <ext:Window ID="Window1" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <ext:Window ID="HiddenWindowFormSpecial" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
