﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using System.Web.Services;
using System.Text;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType, Edge.SVA.Model.CouponType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CouponTypeAddController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(this.BrandID);
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                BindSponsorID(this.SponsorID);
                BindSupplierID(this.SupplierID);

                logger.WriteOperationLog(this.PageName, " Add");
                RegisterCloseEvent(btnClose);

                checkIsImportCouponNumber(false);
                BrandID_SelectedIndexChanged(null, null);
                CoupontypeFixedAmount_SelectedIndexChanged(null, null);
                CouponValidityUnit_SelectedIndexChanged(null, null);
                //Add.CheckImportCoupon(this.IsImportCouponNumber, gpManual, gpImport);

                if (this.AutoReplenish.SelectedValue == "1")//自动补货默认选项为否时，要禁用日期选择控件Add By Len
                {
                    SetEnableWeekDayControls(true);
                }
                else
                {
                    SetEnableWeekDayControls(false);
                }

                //Add By Robin 2015-01-06 
                this.Grid_CouponNatureList.PageSize = webset.ContentPageNum;
                btnNew.OnClientClick = Window1.GetShowReference(string.Format("CouponNatureList/Add.aspx?RuleType=2&PageFlag=0&CouponTypeID={0}", Request["id"]), "新增");
                //End

                SVASessionInfo.CouponTypeAddController = null;
            }
            controller = SVASessionInfo.CouponTypeAddController;
        }

        protected override SVA.Model.CouponType GetPageObject(SVA.Model.CouponType obj)
        {
            List<System.Web.UI.Control> list = new List<Control>();
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (!(con is FineUI.GroupPanel)) continue;

                foreach (System.Web.UI.Control item in con.Controls) list.Add(item);
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void IsImportCouponNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkIsImportCouponNumber(true);
            CouponNumberToUID.SelectedIndex = 0;
            UIDToCouponNumber.SelectedIndex = 0;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            if (string.IsNullOrEmpty(this.MemberClauseDesc1.Text) || this.MemberClauseDesc1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.Term1CannotBeEmpty);
                return;
            }

            Edge.SVA.Model.CouponType item = this.GetAddObject();
            item.CouponTypeStartDate = Edge.Utils.Tools.ConvertTool.GetInstance().ConverNullable<DateTime>(this.CouponTypeStartDate.Text + " " + this.StartTime.Text);
            item.CouponTypeEndDate = Edge.Utils.Tools.ConvertTool.GetInstance().ConverNullable<DateTime>(this.CouponTypeEndDate.Text + " " + this.EndTime.Text);

            controller.SetCouponReplenishDailyViewModelList(GetCouponReplenishDailyViewModelListFromForm());
            
            if (!CheckStartEndDate(this.CouponTypeStartDate, this.CouponTypeEndDate))
            {
                return;
            }

            if (!CheckInputNumRule(this.IsImportCouponNumber, this.CouponNumMaskImport, this.CouponNumPatternImport, this.CouponNumMask, this.CouponNumPattern))
            {
                return;
            }

            if (!CheckValidityDuration(this.CouponValidityUnit, this.CouponValidityDuration, this.CouponSpecifyExpiryDate))
            {
                return;
            }

            if (item != null)
            {   
                //校验文件类型
                if (!ValidateFile(this.CouponTypeLayoutFile.FileName))
                {
                    this.CouponTypeLayoutFile.Reset();
                    return;
                }
                if (!ValidateImg(this.CouponTypePicFile.FileName))
                {
                    this.CouponTypePicFile.Reset();
                    return;
                }

                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CouponTypeCode = item.CouponTypeCode.ToUpper();
                item.CouponNumMask = item.CouponNumMask.ToUpper();

                item.CouponTypeLayoutFile = this.CouponTypeLayoutFile.SaveToServer("Images/CouponType");
                item.CouponTypePicFile = this.CouponTypePicFile.SaveToServer("Images/CouponType");

                //item.CouponTypeDiscount = Convert.ToDecimal(this.CouponTypeDiscount.Text);
                try
                {
                    if (item.IsImportCouponNumber.GetValueOrDefault() == 1)     //导入
                    {
                        item.CouponNumMask = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumMaskImport.Text.ToUpper().Trim() : "";
                        item.CouponNumPattern = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumPatternImport.Text.ToUpper().Trim() : "";
                        //item.CouponCheckdigit = null; Modify by Nathan 20140630
                        item.CouponCheckdigit = false;
                        if (CheckDigitModeID2.Hidden == false)
                        {
                            item.CheckDigitModeID = Tools.ConvertTool.ConverType<int>(this.CheckDigitModeID2.SelectedValue);
                        }
                        else
                        {
                            item.CheckDigitModeID = null;
                        }
                        item.CouponNumberToUID = null;

                        if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                        if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCouponNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                        if (item.UIDToCouponNumber.GetValueOrDefault() == 0 && (string.IsNullOrEmpty(this.CouponNumMaskImport.Text.Trim()) || string.IsNullOrEmpty(this.CouponNumPatternImport.Text.Trim())))
                            throw new Exception("Binding Coupon Number Mask Rule or Coupon Number Prefix ");
                    }
                    else//手动
                    {
                        item.UIDCheckDigit = null;
                        item.IsConsecutiveUID = null;
                        item.UIDToCouponNumber = null;
                        if (CheckDigitModeID.Hidden == false)
                        {
                            item.CheckDigitModeID = Tools.ConvertTool.ConverType<int>(this.CheckDigitModeID.SelectedValue);
                        }
                        else
                        {
                            item.CheckDigitModeID = null;
                        }

                        //if (item.CouponCheckdigit.GetValueOrDefault() && item.CouponNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");Modify by Nathan 20140630
                        //if (!item.CouponCheckdigit.GetValueOrDefault() && item.CouponNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");Modify by Nathan 20140630
                        if (item.CouponCheckdigit && item.CouponNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                        if (!item.CouponCheckdigit && item.CouponNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                    }
                }
                catch (Exception ex)
                {
                    this.ShowWarning(ex.Message);
                    return;
                }
                if (new Edge.SVA.BLL.CouponType().isHasCouponTypeCode(item.CouponTypeCode, 0))
                {
                    this.ShowWarning(Resources.MessageTips.ExistCouponTypeCode);
                    return;
                }

                ////检查是否已经存在号码规则
                //if (this.IsImportCouponNumber.SelectedValue.Trim() == "0")//手动
                //{
                //    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 0))
                //    {
                //        this.ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                //        return;
                //    }

                //}

                //if (item.IsImportCouponNumber.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 0)//导入
                //{
                //    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), 0))
                //    {
                //        this.ShowWarming(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                //        return;
                //    }

                //}

                //检查是否可能重复号码规则
                if (item.IsImportCouponNumber.GetValueOrDefault() == 1)     //导入
                {
                    if (!this.CouponNumMaskImport.Hidden && !CouponNumPatternImport.Hidden)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), 2, 0))
                        {
                            this.ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                            return;
                        }
                    }
                }
                else
                {
                    if (!this.CouponNumMask.Hidden && !CouponNumPattern.Hidden)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 2, 0))
                        {
                            this.ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90394"));
                            return;
                        }
                    }
                }
            }

            item.UIDCheckDigit = GetUIDCheckDigit();
            item.CouponCheckdigit = GetCouponCheckDigit();

            int couponTypeID=Tools.DALTool.Add<Edge.SVA.BLL.CouponType>(item);
            if ( couponTypeID> 0)
            {
                #region 新增使用产品，使用部门

                //List<Edge.SVA.Model.CouponTypeExchangeBinding> newCouponTypeExchangeBindingList = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
                //if (Session["DepartmentCouponTypeExchangeBinding"] != null)
                //{
                //    List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];
                //    foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                //    {

                //        newCouponTypeExchangeBindingList.Add(model);
                //    }
                //}

                //if (Session["ProductCouponTypeExchangeBinding"] != null)
                //{
                //    List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];

                //    foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                //    {

                //        newCouponTypeExchangeBindingList.Add(model);
                //    }
                //}

                //Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                //foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in newCouponTypeExchangeBindingList)
                //{
                //    model.CouponTypeID = id;
                //    bll.Add(model);
                //}
                #endregion

                #region 新增使用店铺/区域/品牌
                //List<Edge.SVA.Model.CouponTypeStoreCondition> allStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

                //if (Session["CouponTypeEarnStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeEarnStoreCheckedList"]);
                //}

                //if (Session["CouponTypeConsumeStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeConsumeStoreCheckedList"]);
                //}

                //if (allStoreCheckedList.Count > 0)
                //{
                //    //为每个新的条件赋值CouponTypeID 和时间
                //    foreach (Edge.SVA.Model.CouponTypeStoreCondition newItem in allStoreCheckedList)
                //    {
                //        newItem.CouponTypeID = id;
                //        newItem.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //        newItem.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //        newItem.CreatedOn = System.DateTime.Now;
                //        newItem.UpdatedOn = System.DateTime.Now;
                //    }

                //    int add = new Edge.SVA.BLL.CouponTypeStoreCondition().AddList(allStoreCheckedList);
                //    if (add > 0)
                //    {

                //        JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //    }
                //    else
                //    {
                //        new Edge.SVA.BLL.CouponType().Delete(id);

                //        JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);

                //    }
                //}
                //else
                //{
                //    JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //}
                #endregion

                if (!string.IsNullOrEmpty(this.CouponTypeLayoutFile.ShortFileName))
                {
                    item.CouponTypeLayoutFile = this.CouponTypeLayoutFile.SaveToServer("Images/CouponType");
                }
                if (!string.IsNullOrEmpty(this.CouponTypePicFile.ShortFileName))
                {
                    item.CouponTypePicFile = this.CouponTypePicFile.SaveToServer("Images/CouponType");
                }
                HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                controller.ViewModel.MemberClause.MemberClauseDesc1 = clearTool.ClearSource(this.MemberClauseDesc1.Text);
                controller.ViewModel.MemberClause.MemberClauseDesc2 = clearTool.ClearSource(this.MemberClauseDesc2.Text);
                controller.ViewModel.MemberClause.MemberClauseDesc3 = clearTool.ClearSource(this.MemberClauseDesc3.Text);
                controller.ViewModel.MemberClause.BrandID = string.IsNullOrEmpty(this.BrandID.SelectedValue) ? 0 : Convert.ToInt32(this.BrandID.SelectedValue);

                controller.ViewModel.MainTable.CouponTypeID = couponTypeID;
                controller.Submit();
                CouponTypeRepostory.Singleton.Refresh();
                this.CloseAndRefresh();
            }
            else
            {
                this.ShowAddFailed();
            }
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.BrandID.SelectedValue))
            {
                Tools.ControlTool.BindCampaign(CampaignID);
            }
            else
            {
                Tools.ControlTool.BindCampaign(CampaignID, Tools.ConvertTool.ToInt(this.BrandID.SelectedValue));
            }
        }

        public static void CheckImportCoupon(FineUI.RadioButtonList isImport, FineUI.GroupPanel manual, FineUI.GroupPanel import)
        {
            if (isImport.SelectedValue == "0")             //手动
            {
                manual.Hidden = false;
                import.Hidden = true;

            }
            else                                           //导入
            {
                manual.Hidden = true;
                import.Hidden = false;
            }
            foreach (System.Web.UI.Control con in manual.Controls)
            {
                if (con is FineUI.TextBox && con.ID.Contains("CouponNumMask")) ((FineUI.TextBox)con).Required = !manual.Hidden;
                if (con is FineUI.TextBox && con.ID.Contains("CouponNumPattern")) ((FineUI.TextBox)con).Required = !manual.Hidden;
            }
            foreach (System.Web.UI.Control con in import.Controls)
            {
                if (con is FineUI.TextBox && con.ID.Contains("CouponNumMask")) ((FineUI.TextBox)con).Required = !import.Hidden;
                if (con is FineUI.TextBox && con.ID.Contains("CouponNumPattern")) ((FineUI.TextBox)con).Required = !import.Hidden;
            }
        }

        private void checkIsImportCouponNumber(bool clearText)
        {
            if (this.IsImportCouponNumber.SelectedValue == "1")
            {
                checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);

                this.gpManual.Hidden = true;
                this.gpImport.Hidden = false;

                this.CouponNumMaskImport.Hidden = true;
                this.CouponNumPatternImport.Hidden = true;
            }
            else
            {
                checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);

                this.gpManual.Hidden = false;
                this.gpImport.Hidden = true;

                this.CouponNumMaskImport.Hidden = false;
                this.CouponNumPatternImport.Hidden = false;
            }

            if (clearText)
            {
                this.CouponNumMask.Text = "";
                this.CouponNumPattern.Text = "";
                this.CouponNumMaskImport.Text = "";
                this.CouponNumPatternImport.Text = "";
            }
        }

        protected void CouponCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);
            CouponNumberToUID.SelectedIndex = 0;

        }

        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);
            UIDToCouponNumber.SelectedIndex = 0;
        }

        protected void CouponTypeCode_TextChanged(object sender, EventArgs e)
        {
            this.CouponTypeCode.Text = this.CouponTypeCode.Text.ToUpper();
        }

        protected void CouponNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CouponNumMask.Text = this.CouponNumMask.Text.ToUpper();
        }

        protected void CouponNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CouponNumMask.Text) && string.IsNullOrEmpty(this.CouponNumPattern.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CouponNumMask, CouponNumPattern);

            //if (!Tools.CheckTool.IsMatchNumMask(this.CouponNumMask.Text, this.CouponNumPattern.Text, true))
            //{
            //    CouponNumPattern.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CouponNumPattern.Text));
            //}
        }

        protected void CouponNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CouponNumMaskImport.Text = this.CouponNumMaskImport.Text.ToUpper();
        }

        protected void CouponNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CouponNumMaskImport.Text) && string.IsNullOrEmpty(this.CouponNumPatternImport.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CouponNumMaskImport, CouponNumPatternImport);

            //if (!Tools.CheckTool.IsMatchNumMask(this.CouponNumMaskImport.Text, this.CouponNumPatternImport.Text,true))
            //{
            //    CouponNumPatternImport.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CouponNumPatternImport.Text));
            //}
        }

        protected void CoupontypeFixedAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CoupontypeFixedAmount.SelectedValue == "0")
            {
                CouponTypeAmount.Enabled = false;
                CouponTypeAmount.Text = "";
            }
            else
            {
                CouponTypeAmount.Enabled = true;
            }
        }

        protected void UIDToCouponNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCouponNumber, CheckDigitModeID2, CouponNumMaskImport, CouponNumPatternImport);
            this.CouponNumMaskImport.Text = "";
            this.CouponNumPatternImport.Text = "";
        }

        protected void CouponNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Manual(CouponCheckdigit, CouponNumberToUID, CheckDigitModeID);
        }

        protected void CouponValidityUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CouponValidityUnit.SelectedValue == "6")
            {
                this.CouponValidityDuration.Enabled = false;
                this.CouponValidityDuration.Text = "";
                this.CouponSpecifyExpiryDate.Enabled = true;
            }
            else
            {
                this.CouponValidityDuration.Enabled = true;
                this.CouponSpecifyExpiryDate.Text = "";
                this.CouponSpecifyExpiryDate.Enabled = false;
            }
        }

        #region Add & Edit public function
        public static void checkCheckDigitMode4Manual(FineUI.RadioButtonList couponCheckdigit, FineUI.DropDownList couponNumberToUID, FineUI.DropDownList checkDigitModeID)
        {
            //Check select CD
            if (couponCheckdigit.SelectedValue == "True")
            {
                foreach (FineUI.ListItem item in couponNumberToUID.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in couponNumberToUID.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (couponNumberToUID.SelectedValue == "3" && couponCheckdigit.SelectedValue == "False")
                {
                    checkDigitModeID.Hidden = false;
                }
                else
                {
                    checkDigitModeID.Hidden = true;
                }
            }
        }

        public static void checkCheckDigitMode4Import(FineUI.RadioButtonList uIDCheckDigit, FineUI.DropDownList uIDToCouponNumber, FineUI.DropDownList checkDigitModeID2, FineUI.TextBox couponNumMaskImport, FineUI.TextBox couponNumPatternImport)
        {
            //Check select CD
            if (uIDCheckDigit.SelectedValue == "1")
            {
                foreach (FineUI.ListItem item in uIDToCouponNumber.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID2.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in uIDToCouponNumber.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (uIDToCouponNumber.SelectedValue == "3")
                {
                    checkDigitModeID2.Hidden = false;
                }
                else
                {
                    checkDigitModeID2.Hidden = true;
                }
            }

            //Check CouponNumMask
            if (uIDToCouponNumber.SelectedValue == "0")
            {
                couponNumMaskImport.Hidden = false;
                couponNumPatternImport.Hidden = false;
            }
            else
            {
                couponNumMaskImport.Hidden = true;
                couponNumPatternImport.Hidden = true;
            }
        }

        //Check number rule
        public static bool CheckInputNumRule(FineUI.RadioButtonList isImportCouponNumber, FineUI.TextBox couponNumMaskImport, FineUI.TextBox couponNumPatternImport, FineUI.TextBox couponNumMask, FineUI.TextBox couponNumPattern)
        {
            if (isImportCouponNumber.SelectedValue == "1")     //导入
            {
                couponNumMaskImport.ClearInvalid();
                couponNumPatternImport.ClearInvalid();

                if (!couponNumMaskImport.Hidden && !couponNumPatternImport.Hidden)
                {
                    if (!Tools.CheckTool.IsMatchNumMask(couponNumMaskImport, couponNumPatternImport))
                    {
                        return false;
                    }

                    //if (string.IsNullOrEmpty(couponNumMaskImport.Text) && !string.IsNullOrEmpty(couponNumPatternImport.Text))
                    //{
                    //    couponNumMaskImport.MarkInvalid(Resources.MessageTips.RequiredField);
                    //    return false;
                    //}

                    //if (!string.IsNullOrEmpty(couponNumMaskImport.Text) && string.IsNullOrEmpty(couponNumPatternImport.Text))
                    //{
                    //    int len = couponNumMaskImport.Text.Trim().Length;
                    //    if (len > 18)
                    //    {
                    //        couponNumMaskImport.MarkInvalid(string.Format(Resources.MessageTips.MaxInputLimit,18));
                    //        return false;
                    //    }
                    //}

                    //if (!Tools.CheckTool.IsMatchNumMask(couponNumMaskImport.Text, couponNumPatternImport.Text, true))
                    //{
                    //    couponNumMaskImport.MarkInvalid(Resources.MessageTips.RequiredField);
                    //    couponNumPatternImport.MarkInvalid(Resources.MessageTips.RequiredField);
                    //    return false;
                    //}
                }

                //if (!string.IsNullOrEmpty(couponNumPatternImport.Text.Trim()) && (!couponNumPatternImport.Hidden))
                //{
                //    if (!Tools.CheckTool.IsMatchNumMask(couponNumMaskImport.Text, couponNumPatternImport.Text,true))
                //    {
                //        couponNumPatternImport.MarkInvalid(Resources.MessageTips.RequiredField);
                //        return false;
                //    }
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(couponNumMaskImport.Text.Trim()) && (!couponNumMaskImport.Hidden))
                //    {
                //        couponNumPatternImport.MarkInvalid(Resources.MessageTips.RequiredField);
                //        return false;
                //    }
                //}
            }
            else//手动
            {
                couponNumMask.ClearInvalid();
                couponNumPattern.ClearInvalid();

                if (!Tools.CheckTool.IsMatchNumMask(couponNumMask, couponNumPattern))
                {
                    return false;
                }

                //if (string.IsNullOrEmpty(couponNumMask.Text) && !string.IsNullOrEmpty(couponNumPattern.Text))
                //{
                //    couponNumMask.MarkInvalid(Resources.MessageTips.RequiredField);
                //    return false;
                //}

                //if (!string.IsNullOrEmpty(couponNumMask.Text) && string.IsNullOrEmpty(couponNumPattern.Text))
                //{
                //    int len = couponNumMask.Text.Trim().Length;
                //    if (len > 18)
                //    {
                //        couponNumMask.MarkInvalid(string.Format(Resources.MessageTips.MaxInputLimit, 18));
                //        return false;
                //    }
                //}

                //if (!Tools.CheckTool.IsMatchNumMask(couponNumMask.Text, couponNumPattern.Text, true))
                //{
                //    couponNumMask.MarkInvalid(Resources.MessageTips.RequiredField);
                //    couponNumPattern.MarkInvalid(Resources.MessageTips.RequiredField);
                //    return false;
                //}

                //if (!string.IsNullOrEmpty(couponNumPattern.Text.Trim()))
                //{
                //    if (!Tools.CheckTool.IsMatchNumMask(couponNumMask.Text, couponNumPattern.Text,true))
                //    {
                //        couponNumPattern.MarkInvalid(Resources.MessageTips.RequiredField);
                //        return false;
                //    }
                //}
                //else
                //{
                //    if (string.IsNullOrEmpty(couponNumMask.Text.Trim()))
                //    {
                //        couponNumMask.MarkInvalid(Resources.MessageTips.RequiredField);
                //        return false;
                //    }

                //    if (string.IsNullOrEmpty(couponNumPattern.Text.Trim()))
                //    {
                //        couponNumPattern.MarkInvalid(Resources.MessageTips.RequiredField);
                //        return false;
                //    }   
                //}
            }

            return true;
        }

        //Check Start Date and End Date
        public static bool CheckStartEndDate(FineUI.DatePicker couponTypeStartDate, FineUI.DatePicker couponTypeEndDate)
        {
            if (string.IsNullOrEmpty(couponTypeStartDate.Text) && (!string.IsNullOrEmpty(couponTypeEndDate.Text)))
            {
                couponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
                couponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);

                return false;
            }
            else if ((!string.IsNullOrEmpty(couponTypeStartDate.Text)) && string.IsNullOrEmpty(couponTypeEndDate.Text))
            {
                couponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
                couponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);

                return false;
            }
            else if ((!string.IsNullOrEmpty(couponTypeStartDate.Text)) && (!string.IsNullOrEmpty(couponTypeEndDate.Text)))
            {
                if ((couponTypeStartDate.SelectedDate != null) && (couponTypeEndDate.SelectedDate != null))
                {
                    if (couponTypeStartDate.SelectedDate > couponTypeEndDate.SelectedDate)
                    {
                        couponTypeStartDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
                        couponTypeEndDate.MarkInvalid(Resources.MessageTips.EndDateGreaterStartDate);
                        return false;
                    }
                }
            }

            return true;
        }

        //Check ValidityDuration
        public static bool CheckValidityDuration(FineUI.DropDownList couponValidityUnit, FineUI.NumberBox couponValidityDuration, FineUI.DatePicker couponSpecifyExpiryDate)
        {
            if (couponValidityUnit.SelectedValue == "6")
            {
                if (string.IsNullOrEmpty(couponSpecifyExpiryDate.Text))
                {
                    couponSpecifyExpiryDate.MarkInvalid(Resources.MessageTips.RequiredField);
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(couponValidityDuration.Text))
                {
                    couponValidityDuration.MarkInvalid(Resources.MessageTips.RequiredField);
                    return false;
                }
            }


            return true;
        }
        #endregion

        #region CouponReplenishDaily
        private List<CouponReplenishDailyViewModel> GetCouponReplenishDailyViewModelListFromForm()
        {
            List<CouponReplenishDailyViewModel> list = new List<CouponReplenishDailyViewModel>();
            CouponReplenishDailyViewModel model;
            string date = DateTime.Today.ToString("yyyy-MM-dd ");
            if (this.WeekDay1.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 1;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime1.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime1.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity1.Text);
                list.Add(model);
            }
            if (this.WeekDay2.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 2;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime2.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime2.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity2.Text);
                list.Add(model);
            }
            if (this.WeekDay3.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 3;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime3.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime3.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity3.Text);
                list.Add(model);
            }
            if (this.WeekDay4.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 4;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime4.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime4.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity4.Text);
                list.Add(model);
            }
            if (this.WeekDay5.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 5;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime5.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime5.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity5.Text);
                list.Add(model);
            }
            if (this.WeekDay6.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 6;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime6.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime6.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity6.Text);
                list.Add(model);
            }
            if (this.WeekDay7.Checked)
            {
                model = new CouponReplenishDailyViewModel();
                model.MainTable.WeekDayNum = 7;
                //model.MainTable.StartTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayStartTime7.Text);
                //model.MainTable.EndTime = Edge.Web.Tools.ConvertTool.ConverNullable<DateTime>(date + this.WeekDayEndTime7.Text);
                model.MainTable.MaxReplenishCount = Edge.Web.Tools.ConvertTool.ConverNullable<int>(this.WeekDayCouponQuantity7.Text);
                list.Add(model);
            }
            return list;
        }
        private void BindCouponReplenishDailyViewModelListToForm(List<CouponReplenishDailyViewModel> list)
        {
            foreach (var item in list)
            {
                int? weekdaynum = item.MainTable.WeekDayNum;
                string starttimeStr = item.MainTable.StartTime.HasValue ? item.MainTable.StartTime.Value.ToString("HH:mm") : "00:00";
                string endtimeStr = item.MainTable.EndTime.HasValue ? item.MainTable.EndTime.Value.ToString("HH:mm") : "23:59";
                string quantityStr = item.MainTable.MaxReplenishCount.ToString();
                switch (weekdaynum)
                {
                    case 1:
                        this.WeekDay1.Checked = true;
                        //this.WeekDayStartTime1.Text = starttimeStr;
                        //this.WeekDayEndTime1.Text = endtimeStr;
                        this.WeekDayCouponQuantity1.Text = quantityStr;
                        break;
                    case 2:
                        this.WeekDay2.Checked = true;
                        //this.WeekDayStartTime2.Text = starttimeStr;
                        //this.WeekDayEndTime2.Text = endtimeStr;
                        this.WeekDayCouponQuantity2.Text = quantityStr;
                        break;
                    case 3:
                        this.WeekDay3.Checked = true;
                        //this.WeekDayStartTime3.Text = starttimeStr;
                        //this.WeekDayEndTime3.Text = endtimeStr;
                        this.WeekDayCouponQuantity3.Text = quantityStr;
                        break;
                    case 4:
                        this.WeekDay4.Checked = true;
                        //this.WeekDayStartTime4.Text = starttimeStr;
                        //this.WeekDayEndTime4.Text = endtimeStr;
                        this.WeekDayCouponQuantity4.Text = quantityStr;
                        break;
                    case 5:
                        this.WeekDay5.Checked = true;
                        //this.WeekDayStartTime5.Text = starttimeStr;
                        //this.WeekDayEndTime5.Text = endtimeStr;
                        this.WeekDayCouponQuantity5.Text = quantityStr;
                        break;
                    case 6:
                        this.WeekDay6.Checked = true;
                        //this.WeekDayStartTime6.Text = starttimeStr;
                        //this.WeekDayEndTime6.Text = endtimeStr;
                        this.WeekDayCouponQuantity6.Text = quantityStr;
                        break;
                    case 7:
                        this.WeekDay7.Checked = true;
                        //this.WeekDayStartTime7.Text = starttimeStr;
                        //this.WeekDayEndTime7.Text = endtimeStr;
                        this.WeekDayCouponQuantity7.Text = quantityStr;
                        break;
                    default:
                        break;
                }
            }
        }
        protected void CheckAll_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.CheckAll.Checked;
            this.WeekDay1.Checked = check;
            this.WeekDay2.Checked = check;
            this.WeekDay3.Checked = check;
            this.WeekDay4.Checked = check;
            this.WeekDay5.Checked = check;
            this.WeekDay6.Checked = check;
            this.WeekDay7.Checked = check;
        }
        protected void AutoReplenish_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.AutoReplenish.SelectedValue == "1")
            {
                SetEnableWeekDayControls(true);
            }
            else
            {
                SetEnableWeekDayControls(false);
            }
        }
        private void SetEnableWeekDayControls(bool bol)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(this.CheckAll);
            tool.AddControl(this.WeekDay1);
            tool.AddControl(this.WeekDay2);
            tool.AddControl(this.WeekDay3);
            tool.AddControl(this.WeekDay4);
            tool.AddControl(this.WeekDay5);
            tool.AddControl(this.WeekDay6);
            tool.AddControl(this.WeekDay7);
            tool.AddControl(this.WeekDayCouponQuantity1);
            tool.AddControl(this.WeekDayCouponQuantity2);
            tool.AddControl(this.WeekDayCouponQuantity3);
            tool.AddControl(this.WeekDayCouponQuantity4);
            tool.AddControl(this.WeekDayCouponQuantity5);
            tool.AddControl(this.WeekDayCouponQuantity6);
            tool.AddControl(this.WeekDayCouponQuantity7);
            tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, bol);
        }

        //private void SetHideWeekDayControls(bool hidden)
        //{
        //    this.WeekDayStartTime1.Hidden = hidden;
        //    this.WeekDayStartTime2.Hidden = hidden;
        //    this.WeekDayStartTime3.Hidden = hidden;
        //    this.WeekDayStartTime4.Hidden = hidden;
        //    this.WeekDayStartTime5.Hidden = hidden;
        //    this.WeekDayStartTime6.Hidden = hidden;
        //    this.WeekDayStartTime7.Hidden = hidden;

        //    this.WeekDayEndTime1.Hidden = hidden;
        //    this.WeekDayEndTime2.Hidden = hidden;
        //    this.WeekDayEndTime3.Hidden = hidden;
        //    this.WeekDayEndTime4.Hidden = hidden;
        //    this.WeekDayEndTime5.Hidden = hidden;
        //    this.WeekDayEndTime6.Hidden = hidden;
        //    this.WeekDayEndTime7.Hidden = hidden;
        //}
        #endregion

        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CouponTypePicFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.CouponTypePicFile.SaveToServer("Images/CouponType");
                FineUI.PageContext.RegisterStartupScript(WindowPicture.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
            BindingGrid_CouponNature();//Add by Robin 2015-01-08
        }
        #endregion

        //UID校验位逻辑（Add by Len）
        protected int GetUIDCheckDigit()
        {
            int Digit = 0;
            if (this.IsImportCouponNumber.SelectedValue == "0")
            {
                if (this.CouponCheckdigit.SelectedValue.ToLower() == "false")
                {
                    if (this.CouponNumberToUID.SelectedValue == "3")
                    {
                        Digit = 1;
                    }
                }
                else
                {
                    if (this.CouponNumberToUID.SelectedValue == "1")
                    {
                        Digit = 1;
                    }
                }
            }
            else
            {
                if (this.UIDCheckDigit.SelectedValue == "1")
                {
                    Digit = 1;
                }
            }

            return Digit;
        }

        //Number校验位逻辑(Add by Len)
        protected bool GetCouponCheckDigit()
        {
            bool Digit = false;
            if (this.IsImportCouponNumber.SelectedValue == "1")
            {
                if (this.UIDCheckDigit.SelectedValue == "1")
                {
                    if (this.UIDToCouponNumber.SelectedValue == "1")
                    {
                        Digit = true;
                    }
                }
                else
                {
                    if (this.UIDToCouponNumber.SelectedValue == "3")
                    {
                        Digit = true;
                    }
                }
            }
            else
            {
                if (this.CouponCheckdigit.SelectedValue == "True")
                {
                        Digit = true;
                }
            }

            return Digit;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验模板文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.CouponTypeFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CouponTypeFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        protected void BindSponsorID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Sponsor bll = new SVA.BLL.Sponsor();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SponsorID", "SponsorName1", "SponsorName2", "SponsorName3", "SponsorCode");
        }

        protected void BindSupplierID(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Supplier bll = new SVA.BLL.Supplier();
            DataSet ds = bll.GetList("");
            ControlTool.BindDataSet(ddl, ds, "SupplierID", "SupplierDesc1", "SupplierDesc2", "SupplierDesc3", "SupplierCode");
        }

        protected void DeleteLayout_OnClick(object sender, EventArgs e)
        {
            this.CouponTypeLayoutFile.Reset();
        }

        protected void DeletePic_OnClick(object sender, EventArgs e)
        {
            this.CouponTypePicFile.Reset();
        }

        //Add By Robin 2015-01-07 
        #region CouponNature 优惠券类别
        protected void Grid_CouponNature_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid_CouponNatureList.PageIndex = e.NewPageIndex;
            this.BindingGrid_CouponNature();
        }

        private void BindingGrid_CouponNature()
        {
            ViewState["sotrID"] = Request["id"];
            string sotrID = "";
            if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                sotrID = ViewState["sotrID"].ToString();

            Edge.SVA.BLL.CouponType coupontype = new SVA.BLL.CouponType();
            DataSet ds = coupontype.GetCouponNatureList(sotrID);

            if (ds != null)
            {
                this.Grid_CouponNatureList.DataSource = ds.Tables[0].DefaultView;
                this.Grid_CouponNatureList.DataBind();
            }
            else
            {
                this.Grid_CouponNatureList.Reset();
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in this.Grid_CouponNatureList.SelectedRowIndexArray)
            {
                sb.Append(Grid_CouponNatureList.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("CouponNatureList/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
        }
        #endregion
        //End
    }
}
