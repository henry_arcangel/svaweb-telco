﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Product
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());

                RegisterCloseEvent(btnClose);

                RptBind();
                this.Grid1.EnableRowDoubleClick = true;
                this.Grid1.EnableCheckBoxSelect = false;
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int couponTypeID = Edge.Web.Tools.ConvertTool.ToInt(ViewState["couponTypeID"].ToString());
            logger.WriteOperationLog(this.PageName, "Add couponTypeID " + couponTypeID.ToString());
            if (couponTypeID > 0)
            {                
                Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                model.BindingType = 2;
                model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                model.ProdCode = ProdCode.Text.Trim();
                model.CouponTypeID = couponTypeID;

                Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();

                if (bllExchangeBinding.GetCount(string.Format("BindingType=2 and BrandID={0} and ProdCode='{1}' and CouponTypeID={2} ", Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue), ProdCode.Text.Trim(), couponTypeID)) > 0)
                {
                    this.ShowWarning(Resources.MessageTips.Exists);
                    return;
                }

                if (bllExchangeBinding.Add(model) > 0)
                {
                    //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
                    CloseAndRefresh();
                }
                else
                {
                    //this.ShowWarming(Resources.MessageTips.AddFailed);
                    ShowAddFailed();
                }
            }
            else
            {
                //this.ShowWarming(Resources.MessageTips.AddFailed);
                ShowAddFailed();
            }
        }

        protected void RptBind()
        {
            Edge.SVA.BLL.Product bll = new SVA.BLL.Product();
            DataSet ds = bll.GetList("");
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                this.Grid1.RecordCount = dt.Rows.Count;
                this.Grid1.DataSource = dt.DefaultView;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind();
        }

        protected void Grid1_OnRowDoubleClick(object sender, FineUI.GridRowClickEventArgs e)
        {
            this.ProdCode.Text = this.Grid1.DataKeys[e.RowIndex][0].ToString();
        }
    }
}
