﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponNature
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponNature, Edge.SVA.Model.CouponNature>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Tools.ControlTool.BindBrand(BrandID);
                Tools.ControlTool.BindCouponNature(ParentID);
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasCouponNatureCode(this.CouponNatureCode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistCouponNatureCode);
                return;
            }

            Edge.SVA.Model.CouponNature item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CouponNatureCode = item.CouponNatureCode.ToUpper();
  
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CouponNature>(item) > 0)
            {
                CouponTypeNatureRepostory.Singleton.Refresh();
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }
    }
}