﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponNature
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponNature, Edge.SVA.Model.CouponNature>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Tools.ControlTool.BindBrand(BrandID);
                Tools.ControlTool.BindCouponNature(ParentID);
                RegisterCloseEvent(btnClose);
            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.CouponNature item = this.GetUpdateObject();

            if (item == null)
            {
                ShowUpdateFailed();
                return;
            }

            if (Tools.DALTool.isHasCouponNatureCode(this.CouponNatureCode.Text.Trim(), item.CouponNatureID))
            {
                ShowWarning(Resources.MessageTips.ExistCouponNatureCode);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CouponNatureCode = item.CouponNatureCode.ToUpper();

            if (DALTool.Update<Edge.SVA.BLL.CouponNature>(item))
            {
                CouponTypeNatureRepostory.Singleton.Refresh();
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }
    }
}