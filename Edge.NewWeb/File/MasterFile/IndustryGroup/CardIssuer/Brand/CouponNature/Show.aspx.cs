﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponNature
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponNature, Edge.SVA.Model.CouponNature>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //Tools.ControlTool.BindBrand(BrandID);
                RegisterCloseEvent(btnClose);
                logger.WriteOperationLog(this.PageName, "Show");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string text = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(text, brand.BrandCode);
                Edge.SVA.Model.CouponNature couponnature = new Edge.SVA.BLL.CouponNature().GetModel(Model.ParentID);
                string text1 = couponnature == null ? "" : DALTool.GetStringByCulture(couponnature.CouponNatureName1, couponnature.CouponNatureName2, couponnature.CouponNatureName3);
                this.ParentID.Text = couponnature == null ? "" : ControlTool.GetDropdownListText(text1, couponnature.CouponNatureCode);
            }
        }

    }
}