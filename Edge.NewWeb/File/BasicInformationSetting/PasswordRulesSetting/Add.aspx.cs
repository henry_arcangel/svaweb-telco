﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.PasswordRulesSetting;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        PasswordRulesSettingController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                PWDStructure_SelectedIndexChanged(null, null);
                SVASessionInfo.PasswordRulesSettingController = null;
            }
            controller = SVASessionInfo.PasswordRulesSettingController;
        
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Add");
            if (Tools.DALTool.isHasPasswordRuleCode(this.PasswordRuleCode.Text.Trim(), 0))
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSettingCode, FineUI.MessageBoxIcon.Warning);
                return;
            }
            if (this.MemberPWDRule.SelectedValue == "1")
            {
                System.Data.DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("MemberPWDRule = 1");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistMemberRule, FineUI.MessageBoxIcon.Warning);
                    return;
                }
            }

            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            if (bll.GetCount(string.Format("PWDSecurityLevel = {0}",Tools.ConvertTool.ConverType<int>(this.PWDSecurityLevel.Text))) > 0)
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSecurity, FineUI.MessageBoxIcon.Warning);
                return;
            }

            //Edge.SVA.Model.PasswordRule item = this.GetAddObject();
            
            //if (item != null)
            //{
            //    item.CreatedBy = DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.PasswordRuleCode = item.PasswordRuleCode.ToUpper();
            //    item.PWDMinLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMinLength.GetValueOrDefault();
            //    item.PWDMaxLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMaxLength.GetValueOrDefault();

            //}
            //if (DALTool.Add<Edge.SVA.BLL.PasswordRule>(item) > 0)
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + item.PasswordRuleCode);
            //    CloseAndRefresh();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + item == null ? "No Data" : item.PasswordRuleCode);
            //    ShowAddFailed();
            //}

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.PasswordRuleCode = controller.ViewModel.MainTable.PasswordRuleCode.ToUpper();
                controller.ViewModel.MainTable.PWDMinLength = controller.ViewModel.MainTable.PWDStructure.GetValueOrDefault() == 0 ? 0 : controller.ViewModel.MainTable.PWDMinLength.GetValueOrDefault();
                controller.ViewModel.MainTable.PWDMaxLength = controller.ViewModel.MainTable.PWDStructure.GetValueOrDefault() == 0 ? 0 : controller.ViewModel.MainTable.PWDMaxLength.GetValueOrDefault();
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + controller.ViewModel.MainTable.PasswordRuleCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.PasswordRuleCode);
                ShowAddFailed();
            }
        }

        protected void PWDStructure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PWDStructure.SelectedValue == "0")
            {
                PWDMinLength.Text = "0";
                PWDMaxLength.Text = "0";
                PWDMinLength.Enabled = false;
                PWDMaxLength.Enabled = false;
            }
            else
            {
                PWDMinLength.Enabled = true;
                PWDMaxLength.Enabled = true;
            }
        }
    }
}
