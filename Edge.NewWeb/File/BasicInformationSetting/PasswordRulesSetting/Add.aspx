﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.PasswordRulesSetting.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="PasswordRuleCode" runat="server" Label="密码规则编号：" Required="true" ShowRedStar="true" MaxLength="20" 
                RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                ToolTipTitle="密码编号" ToolTip="Translate__Special_121_StartKey identifier of Password Rule Setting.1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
            </ext:TextBox>
            <ext:TextBox ID="Name1" runat="server" Label="描述：" Required="true" ShowRedStar="true" ToolTipTitle="描述"
                    ToolTip="请输入规范的密碼規則名称。不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="Name2" runat="server" Label="其他描述1：" ToolTipTitle="其他描述1"
                    ToolTip="對密碼規則的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="Name3" runat="server" Label="其他描述2：" ToolTipTitle="其他描述2"
                    ToolTip="對密碼規則的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:DropDownList ID="PWDStructure" runat="server" Label="密码构成："  
                Required="true" ShowRedStar="true" AutoPostBack="True" 
                OnSelectedIndexChanged="PWDStructure_SelectedIndexChanged" Resizable="true">
                <ext:ListItem Value="0" Text="No Requirement"></ext:ListItem>
                <ext:ListItem Value="1" Text="Numbers"></ext:ListItem>
                <ext:ListItem Value="2" Text="Alphabets"></ext:ListItem>
                <ext:ListItem Value="3" Text="Numbers + Alphabets"></ext:ListItem>
                <ext:ListItem Value="4" Text="Numbers +Alphabets +Symbols"></ext:ListItem>
            </ext:DropDownList>
            <ext:NumberBox ID="PWDMinLength" runat="server" Label="密码最小长度："  Required="true" ShowRedStar="true"  CompareControl="PWDMaxLength" CompareType="Int" CompareOperator="LessThanEqual" ToolTipTitle="密码最小长度" ToolTip="只能輸入正整數">
            </ext:NumberBox>
            <ext:NumberBox  ID="PWDMaxLength" runat="server" Label="密码最大长度："   Required="true" ShowRedStar="true" ToolTipTitle="密码最大长度" ToolTip="只能輸入正整數">
            </ext:NumberBox >
            <ext:TextBox ID="PWDSecurityLevel" runat="server" Label="密码安全级别："  Required="true" ShowRedStar="true" ToolTipTitle="密码安全级别" ToolTip="只能输入整数，0为最低，数字越大，级别越高">
            </ext:TextBox>
<%--            <ext:TextBox ID="PWDValidDays" runat="server" Label="密码有效天数："  Required="true" ShowRedStar="true" ToolTipTitle="密码有效天数" ToolTip="只能輸入正整數"
                MaxValue="100000000" NoDecimal="true" NoNegative="true">
            </ext:TextBox>--%>
            <ext:NumberBox ID="PWDValidDays" runat="server" Label="密码有效天数："  Required="true" ShowRedStar="true" ToolTipTitle="密码有效天数" ToolTip="只能輸入正整數"
                MaxValue="1000" NoDecimal="true" NoNegative="true">
            </ext:NumberBox>
            <ext:DropDownList ID="PWDValidDaysUnit" runat="server" Label="密码有效天数的单位："  Required="true" ShowRedStar="true" Resizable="true" CompareType="String"
                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                <ext:ListItem Text="----------" Value="-1" Selected="True"></ext:ListItem>
                <ext:ListItem Text="年" Value="1"></ext:ListItem>
                <ext:ListItem Text="月" Value="2"></ext:ListItem>
                <ext:ListItem Text="星期" Value="3"></ext:ListItem>
                <ext:ListItem Text="天" Value="4"></ext:ListItem>
            </ext:DropDownList>
            <ext:RadioButtonList ID="MemberPWDRule" runat="server" Label="是否会员密码规则："  Required="true" ShowRedStar="true">
                <ext:RadioItem Text="是" Value="1" />
                <ext:RadioItem Text="否" Value="0" Selected="true" />
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
