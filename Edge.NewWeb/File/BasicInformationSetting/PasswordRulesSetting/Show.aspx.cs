﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.PasswordRulesSetting;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        PasswordRulesSettingController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                logger.WriteOperationLog(this.PageName, "Show");
                SVASessionInfo.PasswordRulesSettingController = null;
            }
            controller = SVASessionInfo.PasswordRulesSettingController;

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                controller.LoadViewModel(Model.PasswordRuleID);
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);


                this.PWDStructureView.Text = this.PWDStructure.SelectedItem == null ? "" : this.PWDStructure.SelectedItem.Text;
                this.PWDStructure.Visible = false;

                this.PWDValidDaysUnitView.Text = this.PWDValidDaysUnit.SelectedItem == null ? "" : this.PWDValidDaysUnit.SelectedItem.Text;
                this.PWDValidDaysUnit.Visible = false;

                this.MemberPWDRuleView.Text = this.MemberPWDRule.SelectedItem == null ? "" : this.MemberPWDRule.SelectedItem.Text;
                this.MemberPWDRule.Visible = false;
            }
        }
    }
}
