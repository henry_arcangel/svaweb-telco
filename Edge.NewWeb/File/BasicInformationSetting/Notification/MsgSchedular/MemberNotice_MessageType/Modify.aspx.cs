﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_MessageType
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberNotice_MessageType, Edge.SVA.Model.MemberNotice_MessageType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.SVA.BLL.MessageTemplate bll = new Edge.SVA.BLL.MessageTemplate();
                List<Edge.SVA.Model.MessageTemplate> list = bll.GetModelList("");
                foreach (Edge.SVA.Model.MessageTemplate item in list)
                {
                    this.MessageTemplateID.Items.Add(new FineUI.ListItem(item.MessageTemplateCode, item.MessageTemplateID.ToString()));
                }

                foreach (int item in Enum.GetValues(typeof(EnumFrequencyUnit)))
                {
                    this.FrequencyUnit.Items.Add(new FineUI.ListItem(((EnumFrequencyUnit)item).ToString(), item.ToString()));
                }
            }
        }


        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    //this.BrandID.SelectedValue = Model.BrandID.ToString();
                    //this.MessageType.SelectedValue = Model.MessageType.ToString();
                    //this.MessageServiceTypeID.SelectedValue = Model.MessageServiceTypeID.ToString();
                    //this.SendTimeStr.Text = Model.SendTime.Value.ToString("HH:mm:ss");
                    if (Model.SendTime.HasValue)
                    {
                        this.StartDate.Text = Model.SendTime.Value.ToString("yyyy-MM-dd");
                        this.StartTime.Text = Model.SendTime.Value.ToString("HH:mm");
                    }
                }
                if (this.StartDate.Text.Trim().Length==0)
                {
                    this.StartDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                }
                DesignSendTimes_SelectedIndexChanged(null, null);
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            ////if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            ////{
            ////    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistDistributeTemplateCode, FineUI.MessageBoxIcon.Warning);
            ////    return;
            ////}
            Edge.SVA.Model.MemberNotice_MessageType item = this.GetUpdateObject();

            if (item != null)
            {
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.DistributionCode = item.DistributionCode.ToUpper();
                //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");
                //item.SendTime = DateTime.Parse(DateTime.Today.ToString("yyyy-MM-dd ") + this.SendTimeStr.Text);
                item.SendTime = DateTime.Parse(this.StartDate.Text + " " + this.StartTime.Text);
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.MemberNotice_MessageType>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_MessageType Update Success\tCode:" + item.KeyID.ToString());
                messageNotifyUtil.UpdateStatusP(item.NoticeNumber);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_MessageType Update Failed\tCode:" + item == null ? "No Data" : item.KeyID.ToString());
                ShowUpdateFailed();
            }
        }

        protected void DesignSendTimes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            //tool.AddControl(this.StartDate);
            //tool.AddControl(this.StartTime);
            ExtAspNetPropertyTool tool1 = new ExtAspNetPropertyTool();
            tool1.AddControl(this.FrequencyUnit);
            tool1.AddControl(this.FrequencyValue);
            if (this.DesignSendTimes.SelectedValue == "1")
            {
                //tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
                tool1.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);
            }
            else
            {
                //tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);
                tool1.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
            //tool.Clear();
            tool1.Clear();
        }
    }
}
