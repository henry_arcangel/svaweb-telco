﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_MessageType
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.MemberNotice_MessageType, Edge.SVA.Model.MemberNotice_MessageType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);
            }
           
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    this.MessageTemplateID.Text = DALTool.GetMessageTemplateCode(Model.MessageTemplateID, null);
                    this.Status.Text = ((EnumYesNo)Model.Status).ToString();
                    this.FrequencyUnit.Text = ((EnumFrequencyUnit)Model.FrequencyUnit).ToString();
                    this.SendTimeStr.Text = Model.SendTime.Value.ToString("HH:mm:ss");
                }
            }
        }
    }
}
