﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.MemberNotice, Edge.SVA.Model.MemberNotice>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);
                Grid1.PageSize = webset.ContentPageNum;
                Grid2.PageSize = webset.ContentPageNum;
            }
           
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    if (string.IsNullOrEmpty(this.NoticeNumber.Text.Trim()))
                    {
                        RptBind(" ", "KeyID");
                    }
                    else
                    {
                        RptBind(" NoticeNumber='" + this.NoticeNumber.Text.Trim() + "'", "KeyID");
                    }

                    if (string.IsNullOrEmpty(this.NoticeNumber.Text.Trim()))
                    {
                        RptBind2(" ", "KeyID");
                    }
                    else
                    {
                        RptBind2(" NoticeNumber='" + this.NoticeNumber.Text.Trim() + "'", "KeyID");
                    }
                }
            }
        }
        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {

            Edge.SVA.BLL.MemberNotice_Filter bll = new Edge.SVA.BLL.MemberNotice_Filter();

            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);
            
            DataSet ds = new DataSet();
            ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, strWhere, orderby);

            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            Tools.DataTool.AddBooleanName(ds, "OnBirthDayName", "OnBirthDay");
            //Tools.DataTool.AddMessageType(ds, "MessageType");
            //Tools.DataTool.AddMessageServiceType(ds, "MessageServiceTypeID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion
        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            if (string.IsNullOrEmpty(this.NoticeNumber.Text.Trim()))
            {
                RptBind(" ", "KeyID");
            }
            else
            {
                RptBind(" NoticeNumber='" + this.NoticeNumber.Text.Trim() + "'", "KeyID");
            }
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid2.PageIndex = e.NewPageIndex;

            if (string.IsNullOrEmpty(this.NoticeNumber.Text.Trim()))
            {
                RptBind2(" ", "KeyID");
            }
            else
            {
                RptBind2(" NoticeNumber='" + this.NoticeNumber.Text.Trim() + "'", "KeyID");
            }
        }
        private void RptBind2(string strWhere, string orderby)
        {

            Edge.SVA.BLL.MemberNotice_MessageType bll = new Edge.SVA.BLL.MemberNotice_MessageType();

            //获得总条数
            this.Grid2.RecordCount = bll.GetRecordCount(strWhere);

            DataSet ds = new DataSet();
            //ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex, strWhere, orderby);
            int pagesize = this.Grid2.PageSize;
            int pageindex = this.Grid2.PageIndex;
            ds = bll.GetListByPage(strWhere, orderby, pagesize * pageindex + 1, pagesize * (pageindex + 1));

            Tools.DataTool.AddBooleanName(ds, "StatusName", "Status");
            Tools.DataTool.AddMessageTemplateCode(ds, "MessageTemplateCode", "MessageTemplateID");
            Tools.DataTool.AddFrequencyUnitName(ds, "FrequencyUnitName", "FrequencyUnit");
            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();
        }
    }
}
