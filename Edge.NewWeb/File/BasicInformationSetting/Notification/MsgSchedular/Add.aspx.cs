﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using Edge.SVA.Model.Domain;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.MemberNotice, Edge.SVA.Model.MemberNotice>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                this.NoticeNumber.Text = DALTool.GetREFNOCode("MNO");
                this.StartDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            ////if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            ////{
            ////    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistDistributeTemplateCode, FineUI.MessageBoxIcon.Warning);
            ////    return;
            ////}
            Edge.SVA.Model.MemberNotice item = this.GetAddObject();

            if (item != null)
            {
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.DistributionCode = item.DistributionCode.ToUpper();

                //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");

            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.MemberNotice>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice Add Success\tCode:" + item.NoticeNumber);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice Add Failed\tCode:" + item == null ? "No Data" : item.NoticeNumber);
                ShowAddFailed();
            }

        }
    }
}
