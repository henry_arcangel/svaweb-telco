﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_Filter
{
    public partial class List : PageBase
    {

        Tools.Logger logger = Tools.Logger.Instance;
        string id;
        protected void Page_Load(object sender, EventArgs e)
        {      

            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");
                id = Request.Params["id"];
                this.PrivateKey.Text = id;

                Grid1.PageSize = webset.ContentPageNum;
                if (string.IsNullOrEmpty(id))
                {
                    RptBind(" ", "KeyID");
                }
                else
                {
                    RptBind(" NoticeNumber='"+id+"'", "KeyID");
                }
                btnClose.OnClientClick = FineUI.ActiveWindow.GetConfirmHideRefreshReference();

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx?NoticeNumber=" + id, "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
            id = this.PrivateKey.Text;
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {

            Edge.SVA.BLL.MemberNotice_Filter bll = new Edge.SVA.BLL.MemberNotice_Filter();

            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);
            if (this.Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, strWhere, orderby);

            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            Tools.DataTool.AddBooleanName(ds, "OnBirthDayName", "OnBirthDay");
            //Tools.DataTool.AddMessageType(ds, "MessageType");
            //Tools.DataTool.AddMessageServiceType(ds, "MessageServiceTypeID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));

            string ids = sb.ToString().TrimEnd(',');
            if (string.IsNullOrEmpty(ids))
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, FineUI.MessageBoxIcon.Warning);
                return;
            }
            logger.WriteOperationLog(this.PageName, "Delete");
            foreach (string item in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                if (string.IsNullOrEmpty(item)) continue;
                Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.MemberNotice_Filter>(Tools.ConvertTool.ToInt(item));
            }
            FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, "");
            Tools.Logger.Instance.WriteOperationLog("MemberNotice_Filter Delete", "MessageTemplate Delete\t Code:" + ids);


            if (string.IsNullOrEmpty(id))
            {
                RptBind(" ", "KeyID");
            }
            else
            {
                messageNotifyUtil.UpdateStatusP(id);

                RptBind(" NoticeNumber='" + id + "'", "KeyID");
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            if (string.IsNullOrEmpty(id))
            {
                RptBind(" ", "KeyID");
            }
            else
            {
                RptBind(" NoticeNumber='" + id+"'", "KeyID");
            }
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (string.IsNullOrEmpty(id))
            {
                RptBind(" ", "KeyID");
            }
            else
            {
                RptBind(" NoticeNumber='" + id + "'", "KeyID");
            }
        }
    }
}
