﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.MessageTemplate, Edge.SVA.Model.MessageTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MsgTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.WindowEdit.Title = "新增";
                RegisterCloseEvent(btnClose);
                SVASessionInfo.MsgTemplateController = null;
                controller = SVASessionInfo.MsgTemplateController;
                controller.LoadPublicInfos(SVASessionInfo.SiteLanguage);

                ControlTool.BindBrandInfoList(BrandID,controller.ViewModel.BrandInfoList);
                ControlTool.BindKeyValueList(OprID, controller.ViewModel.TransactionTypeList); 

                btnDelete.OnClientClick = Grid_NotificationTemplate.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
            controller = SVASessionInfo.MsgTemplateController;
        }

        protected void btnNew_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(WindowEdit.GetShowReference("MsgTemplateDetail/Add.aspx"));
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_NotificationTemplate.SelectedRowIndexArray;
            int count = this.Grid_NotificationTemplate.PageIndex * this.Grid_NotificationTemplate.PageSize;
            List<MessageTemplateDetailViewModel> list = new List<MessageTemplateDetailViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.MessageTemplateDetailViewModelList[rowIndexs[i] + count]);
            }
            controller.RemoveMessageTemplateDetailViewModelList(list);
            BindingDataGrid_NotificationTemplate();
        }
        private void BindingDataGrid_NotificationTemplate()
        {
            this.Grid_NotificationTemplate.RecordCount = controller.ViewModel.MessageTemplateDetailViewModelList.Count;
            this.Grid_NotificationTemplate.DataSource = controller.ViewModel.MessageTemplateDetailViewModelList;
            this.Grid_NotificationTemplate.DataBind();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_NotificationTemplate();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            if (Tools.DALTool.ExistsMsgTemplateCode(this.MessageTemplateCode.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.ExistsCode);
                return;
            }
            
            Edge.SVA.Model.MessageTemplate item = this.GetAddObject();

            controller.ViewModel.MainTable = item;

            ExecResult er = controller.AddData();//controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Add Success\tCode:" + item.MessageTemplateCode);
                CloseAndRefresh();
            } 
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Add Failed\tCode:" + item == null ? "No Data" : item.MessageTemplateCode);
                ShowAddFailed();
            }



            //if (item != null)
            //{
            //    //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    //item.CreatedOn = System.DateTime.Now;
            //    //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    //item.UpdatedOn = System.DateTime.Now;
            //    //item.DistributionCode = item.DistributionCode.ToUpper();

            //    //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");


            //    //int len=this.TemplateFile.PostedFile.ContentLength;
            //    //byte[] templateObj=new byte[len];
            //    //this.TemplateFile.PostedFile.InputStream.Read(templateObj, 0, len);
            //    //item.TemplateObject = templateObj;
            //    //if (!string.IsNullOrEmpty(this.TemplateFile.ShortFileName))
            //    //{
            //    //    this.TemplateFile.SaveAs(Server.MapPath("~" + ConstUtil.CreateServerTemplatePath(this.TemplateFile.ShortFileName)));
            //    //    item.TemplateFile = ConstUtil.CreateServerTemplatePath(this.TemplateFile.ShortFileName);
            //    //}
            //    //item.TemplateFile = this.TemplateFile.SaveToServer("Messages/Template");
            //}
            //if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.MessageTemplate>(item) > 0)
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Add Success\tCode:" + item.MessageTemplateCode);
            //    CloseAndRefresh();

            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Add Failed\tCode:" + item == null ? "No Data" : item.MessageTemplateCode);
            //    ShowAddFailed();
            //}

        }
        protected void OprID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            controller.TransactionTypeID = this.OprID.SelectedValue;
        }
    }
}
