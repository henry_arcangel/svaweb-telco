﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.StoreNature;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.StoreNature
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreType,Edge.SVA.Model.StoreType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreNatureController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.StoreNatureController = null;
            }
            controller = SVASessionInfo.StoreNatureController;
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");


            //Edge.SVA.Model.StoreType item = this.GetUpdateObject();

            //if (Tools.DALTool.isHasStoreTypeCode(this.StoreTypeCode.Text.Trim(), item.StoreTypeID))
            //{
            //    this.ShowWarning(Resources.MessageTips.ExistStoreGroupCode);
            //    return;
            //}

            //if (item != null)
            //{
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.StoreTypeCode = item.StoreTypeCode.ToUpper();
            //}

            //if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.StoreType>(item))
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Delete\t Code:" + item.StoreTypeCode);
            //    this.CloseAndPostBack();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Delete\t Code:" + item == null ? "No Data" : item.StoreTypeCode);
            //    this.ShowUpdateFailed();
            //}

            controller.ViewModel.MainTable = this.GetUpdateObject();

            string message = controller.ValidataObject(this.StoreTypeCode.Text.Trim(), controller.ViewModel.MainTable.StoreTypeID);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.StoreTypeCode = controller.ViewModel.MainTable.StoreTypeCode.ToUpper();

            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Update\t Code:" + controller.ViewModel.MainTable.StoreTypeCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.StoreTypeCode);
                ShowUpdateFailed();
            }
        }
    }
}
