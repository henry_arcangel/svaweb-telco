﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Label ID="PromotionMsgCode" runat="server" Label="广告编号：">
                            </ext:Label>
                            <ext:Label ID="ParentIDName" runat="server" Label="优惠消息类型1：">
                            </ext:Label>
                            <ext:DropDownList ID="ParentID" runat="server" Label="优惠消息类型1：" Hidden="true">
                            </ext:DropDownList>
                            <ext:Label ID="PromotionMsgTypeIDByName" runat="server" Label="优惠消息类型2：">
                            </ext:Label>
                            <ext:DropDownList ID="PromotionMsgTypeID" runat="server" Label="优惠消息类型2：" Hidden="true">
                            </ext:DropDownList>
                            <ext:Label ID="PromotionTitle1" runat="server" Label="广告标题1：" >
                            </ext:Label>
                            <ext:Label ID="PromotionTitle2" runat="server" Label="广告标题2：">
                            </ext:Label>
                            <ext:Label ID="PromotionTitle3" runat="server" Label="广告标题3：">
                            </ext:Label>
                            <ext:Label ID="StatusName" runat="server" Label="广告状态：">
                            </ext:Label>
                            <ext:RadioButtonList ID="Status" runat="server" Label="广告状态：" AutoPostBack="True" Enabled="false" Hidden="true">
                                <ext:RadioItem Text="生效" Value="1" Selected="true"/>
                                <ext:RadioItem Text="屏蔽" Value="2"/>
                            </ext:RadioButtonList>
                            <ext:HiddenField ID="PicturePath" runat="server"></ext:HiddenField>
                            <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                ShowBorder="false" runat="server" HideMode="Offsets">
                                <Rows>
                                    <ext:FormRow ColumnWidths="0% 40% 10%" runat="server">
                                        <Items>
                                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                            </ext:Label>
                                            <ext:Label ID="Label2" Text="" runat="server" Label="广告主题图片：">
                                            </ext:Label>
                                            <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                                            </ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:Label runat="server" ID="AttachmentFilePath" Label="附件上传："></ext:Label>
                            <ext:Label ID="PromotionMsgStr1" EncodeText="false" runat="server" Label="广告内容1："></ext:Label>
                            <ext:Label ID="PromotionMsgStr2" EncodeText="false" runat="server" Label="广告内容2："></ext:Label>
                            <ext:Label ID="PromotionMsgStr3" EncodeText="false" runat="server" Label="广告内容3："></ext:Label>
                            <ext:Label ID="PromotionRemark" runat="server" Label="备注："></ext:Label>
                            <%--Add by Alex 2014-07-17 --%>
                             <ext:Label ID="Priority" EncodeText="false" runat="server" Label="优先次序：">
                            </ext:Label>
                            <ext:Label ID="Link" EncodeText="false" runat="server" Label="视频链接：">
                            </ext:Label>
                            <%--Add by Alex 2014-07-17 --%>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="广告有效期设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="sform6" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Label ID="StartDate" runat="server" Label="广告起始日期：">
                            </ext:Label>
                            <ext:Label ID="EndDate" runat="server" Label="广告结束日期：">
                            </ext:Label>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="广告播放规则设置"
                AutoHeight="true" AutoWidth="true" Layout="Form">
                <Items>
                    <ext:Form ID="FormReLoad1" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px">
                        <Rows>
                            <ext:FormRow ID="FormRow2" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Tree runat="server" ID="CardGradeTree" Title="会员品牌-卡类型-卡级别列表">
                                    </ext:Tree>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:CheckRight ID="CheckRight1" runat="server"></uc2:CheckRight>
    </form>
</body>
</html>
