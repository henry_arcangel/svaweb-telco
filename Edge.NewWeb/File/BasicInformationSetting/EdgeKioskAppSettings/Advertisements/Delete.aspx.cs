﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Controllers.File.BasicInformationSetting.Advertisements;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        AdvertisementsDeleteController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                controller = SVASessionInfo.AdvertisementsDeleteController;
                
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        return;
                    }
                    logger.WriteOperationLog(this.PageName, " Delete " + ids);
                    //foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    //{
                    //    //controller.Delete(Tools.ConvertTool.ToInt(id));
                    //    controller.DeleteData(Tools.ConvertTool.ToInt(id));
                    //}
                    List<int> keyIDList = new List<int>();
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        keyIDList.Add(Tools.ConvertTool.ToInt(id));
                    }
                    controller.DeleteData(keyIDList);

                    Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
            SVASessionInfo.AdvertisementsDeleteController = null;
        }
    }
}