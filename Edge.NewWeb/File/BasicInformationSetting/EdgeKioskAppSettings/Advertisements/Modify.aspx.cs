﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.BasicInformationSetting.Advertisements;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PromotionMsg, Edge.SVA.Model.PromotionMsg>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        AdvertisementsModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.AdvertisementsModifyController = null;
                controller = SVASessionInfo.AdvertisementsModifyController;


                InitParintIDList();
                //InitPromotionMsgTypeList();
            }
            controller = SVASessionInfo.AdvertisementsModifyController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.KeyID, SVASessionInfo.SiteLanguage);
                if (controller.ViewModel.MainTable != null)
                {
                    //存在图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.PromotionPicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.PromotionPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");


                    this.ParentID.SelectedValue = controller.GetParentIDListByTypeID(controller.ViewModel.MainTable.KeyID);
                    InitPromotionMsgTypeList();
                    this.PromotionMsgTypeID.SelectedValue = controller.ViewModel.MainTable.PromotionMsgTypeID.ToString().Trim();
                    //加载树结构
                    LoadTree();

                    //附件路径加载
                    this.AttchFile.Text = controller.ViewModel.MainTable.AttachmentFilePath;
                }
            }
        }

        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                    controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;

                    if (!string.IsNullOrEmpty(this.PromotionPicFile.ShortFileName) && this.FormLoad.Hidden == false)
                    {                    //校验图片以及文件类型是否正确
                        if (!ValidateImg(this.PromotionPicFile.FileName))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.PromotionPicFile = this.PromotionPicFile.SaveToServer("Files/Advertisements");
                    }
                    else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                    {
                        if (!ValidateImg(this.uploadFilePath.Text))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.PromotionPicFile = this.uploadFilePath.Text;
                    }
                    HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();

                    controller.ViewModel.MainTable.PromotionMsgStr1 = clearTool.ClearSource(this.PromotionMsgStr1.Text).Replace("\r\n", "");
                    controller.ViewModel.MainTable.PromotionMsgStr2 = clearTool.ClearSource(this.PromotionMsgStr2.Text).Replace("\r\n", "");
                    controller.ViewModel.MainTable.PromotionMsgStr3 = clearTool.ClearSource(this.PromotionMsgStr3.Text).Replace("\r\n", "");

                    if (!ValidateFile(this.AttchFile.Text))
                    {
                        return;
                    }
                    //保存附件到数据库
                    AttachmentFilePath.SaveAttachFileToServer("Files/Advertisements");
                    controller.ViewModel.MainTable.AttachmentFilePath = this.AttchFile.Text;

                }

                //获取树节点的值，并保存到对象
                controller.ViewModel.CardGradeIDList.Clear();
                controller.ViewModel.BrandInfo.Clear();
                foreach (FineUI.TreeNode brandNode in CardGradeTree.Nodes)
                {
                    if (brandNode.Checked)
                    {
                        BrandInfo bi = new BrandInfo();
                        bi.Key = brandNode.NodeID;
                        bi.Value = brandNode.Text;
                        foreach (FineUI.TreeNode cardtypeNode in brandNode.Nodes)
                        {
                            if (cardtypeNode.Checked)
                            {
                                CardTypeInfo ct = new CardTypeInfo();
                                ct.Key = cardtypeNode.NodeID.Split('_')[1];
                                ct.Value = cardtypeNode.Text;
                                foreach (var cardgradeNode in cardtypeNode.Nodes)
                                {
                                    if (cardgradeNode.Checked)
                                    {
                                        ct.CardGrades.Add(new CardGradeInfo { Key = cardgradeNode.NodeID.ToString(), Value = cardgradeNode.Text });

                                        int CardGradeID = string.IsNullOrEmpty(cardgradeNode.NodeID.Split('_')[2]) ? 0 : Convert.ToInt32(cardgradeNode.NodeID.Split('_')[2]);
                                        controller.ViewModel.CardGradeIDList.Add(CardGradeID);
                                    }
                                }
                                bi.CardTypeInfos.Add(ct);
                            }
                            controller.ViewModel.BrandInfo.Add(bi);
                        }
                    }
                }

                //ExecResult er = controller.Submit();
                ExecResult er = controller.Save();
                if (er.Success)
                {
                    if (this.FormReLoad.Hidden == true)
                    {
                        DeleteFile(this.uploadFilePath.Text);
                    }

                    // 2. 关闭本窗体，然后刷新父窗体
                    CloseAndPostBack();
                }
                else
                {
                    logger.WriteErrorLog("PromotionMsg ", " Update error", er.Ex);
                    ShowUpdateFailed();
                }
            }

        }
        protected void CardGradeTree_NodeCheck(object sender, FineUI.TreeCheckEventArgs e)
        {
            if (e.Checked)
            {
                if (e.Node.ParentNode != null)
                {
                    if (e.Node.ParentNode.ParentNode != null && !e.Node.ParentNode.ParentNode.Checked)
                    {
                        e.Node.ParentNode.ParentNode.Checked = true;
                    }
                }
                if (e.Node.ParentNode != null && !e.Node.ParentNode.Checked)
                {
                    e.Node.ParentNode.Checked = true;
                }
                if (e.Node.Nodes.Count >= 1)
                {
                    CardGradeTree.CheckAllNodes(e.Node.Nodes);
                }
            }
            else
            {
                //if (e.Node.ParentNode != null && e.Node.ParentNode.Checked)
                //{
                //    bool needChecked = false;
                //    foreach (FineUI.TreeNode item in e.Node.ParentNode.Nodes)
                //    {
                //        if (item.Checked)
                //        {
                //            needChecked = true;
                //        }
                //    }
                //    e.Node.ParentNode.Checked = needChecked;
                //}
                //if (e.Node.ParentNode != null)
                //{
                //    if (e.Node.ParentNode.ParentNode != null && e.Node.ParentNode.ParentNode.Checked)
                //    {
                //        bool needChecked = false;
                //        foreach (FineUI.TreeNode item in e.Node.ParentNode.ParentNode.Nodes)
                //        {
                //            if (item.Checked)
                //            {
                //                needChecked = true;
                //            }
                //        }
                //        e.Node.ParentNode.ParentNode.Checked = needChecked;
                //    }
                //}

                if (e.Node.ParentNode != null )
                {
                    bool needChecked = false;

                    if (e.Node.ParentNode.Checked)
                    {
                        foreach (FineUI.TreeNode item in e.Node.ParentNode.Nodes)
                        {
                            if (item.Checked)
                            {
                                needChecked = true;
                            }
                        }
                        e.Node.ParentNode.Checked = needChecked;
                    }
                    needChecked = false;
                    if (e.Node.ParentNode.ParentNode != null && e.Node.ParentNode.ParentNode.Checked)
                    {
                        foreach (FineUI.TreeNode item in e.Node.ParentNode.ParentNode.Nodes)
                        {
                            if (item.Checked)
                            {
                                needChecked = true;
                            }
                        }
                        e.Node.ParentNode.ParentNode.Checked = needChecked;
                    }
                }
                CardGradeTree.UncheckAllNodes(e.Node.Nodes);
            }
        }
        protected void CheckAll_CheckedChanged(object sender, System.EventArgs e)
        {
            if (this.CheckAll.Checked)
            {
                CardGradeTree.CheckAllNodes(CardGradeTree.Nodes);
            }
            else
            {
                CardGradeTree.UncheckAllNodes(CardGradeTree.Nodes);
            }
        }
        //加载树结构
        protected void LoadTree()
        {
            CardGradeTree.AutoWidth = true;
            List<BrandInfo> listBrandInfo = controller.ViewModel.BrandInfo;
            
            foreach (BrandInfo brandItem in listBrandInfo)
            {
                if (brandItem.CardTypeInfos.Count >= 1)
                {
                    FineUI.TreeNode brandNode = new FineUI.TreeNode();
                    brandNode.EnableCheckBox = true;
                    brandNode.AutoPostBack = true;
                    brandNode.NodeID = brandItem.Key;
                    brandNode.Text = brandItem.Value;
                    CardGradeTree.Nodes.Add(brandNode);
                    foreach (var cardtypeitem in brandItem.CardTypeInfos)
                    {
                        FineUI.TreeNode cardtypeNode = new FineUI.TreeNode();
                        cardtypeNode.EnableCheckBox = true;
                        cardtypeNode.AutoPostBack = true;
                        cardtypeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key;
                        cardtypeNode.Text = cardtypeitem.Value;
                        brandNode.Nodes.Add(cardtypeNode);
                        foreach (var cardgradeitem in cardtypeitem.CardGrades)
                        {
                            FineUI.TreeNode cardgradeNode = new FineUI.TreeNode();
                            cardgradeNode.EnableCheckBox = true;
                            cardgradeNode.AutoPostBack = true;
                            cardgradeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key + "_" + cardgradeitem.Key;
                            cardgradeNode.Text = cardgradeitem.Value;
                            cardtypeNode.Nodes.Add(cardgradeNode);
                        }
                    }
                }
            }

            //根据CardGradeID加载选中的树节点
            CardGradeTree.UncheckAllNodes(CardGradeTree.Nodes);
            //TreeCheckEventArgs treeArgs = null;                   
            if (controller.ViewModel.CardGradeIDList.Count > 0)
            {
                foreach (var item in controller.ViewModel.CardGradeIDList)
                {
                    foreach (FineUI.TreeNode brandNode in CardGradeTree.Nodes)
                    {
                        foreach (FineUI.TreeNode cardtypeNode in brandNode.Nodes)
                        {
                            foreach (var cardgradeNode in cardtypeNode.Nodes)
                            {
                                if (cardgradeNode.NodeID.Split('_')[2].ToString() == item.ToString())
                                {
                                    cardgradeNode.Checked = true;
                                    cardgradeNode.ParentNode.Checked = true;
                                    cardgradeNode.ParentNode.ParentNode.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.PromotionMsgStr1.Text) || this.PromotionMsgStr1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.PromotionMsg1CannotBeEmpty);
                return false;
            }

            return true;
        }
        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.PromotionPicFile.ShortFileName))
            {
                this.PicturePath.Text = this.PromotionPicFile.SaveToServer("Files/Advertisements");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }
        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }
        #endregion

        protected void InitParintIDList()
        {
            controller.ViewModel.ParentIDList = controller.GetParentIDList(SVASessionInfo.SiteLanguage);
            ControlTool.BindKeyValueList(this.ParentID, controller.ViewModel.ParentIDList);
        }
        protected void InitPromotionMsgTypeList()
        {
            controller.ViewModel.PromotionMsgTypeList = controller.AddPromotionMsgTypeList(SVASessionInfo.SiteLanguage, Convert.ToInt32(this.ParentID.SelectedValue));
            ControlTool.BindKeyValueList(this.PromotionMsgTypeID, controller.ViewModel.PromotionMsgTypeList);
        }

        protected void ParentID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PromotionMsgTypeID.Items.Clear();
            InitPromotionMsgTypeList();
        }

        protected void AttachmentFilePath_FileSelected(object sender, EventArgs e)
        {
            if (AttachmentFilePath.HasFile)
            {
                string fileName = AttachmentFilePath.ShortFileName;
                fileName = webset.AttchFileServer + webset.WebFilePath + "/Files/Advertisements/" + fileName;
                this.AttchFile.Text = fileName;
            }

        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.');
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.AdvertisementFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.AdvertisementFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }


        #region Add by Nathan 20140701
        private static string FormatPath(string item)
        {
            string key = item.Trim();
            //key = key.Replace("\\","/");
            key = Regex.Replace(key, @"[/\\][/\\    ]*", "/");
            if (!key.Substring(0, 1).Equals("/"))
            {
                key = "/" + key;
            }
            return key.ToLower();
        }

        protected void btnGenerateHtml_Click(object sender, EventArgs e)
        {

            string webPath = Request.Url.Scheme + "://" + Request.Url.Authority;


            string htmlPath = "";

            string fileName = this.PromotionMsgCode.Text + "_" + SVASessionInfo.SiteLanguage + ".html";
            string htmlSavePath = "";
            try
            {
                htmlPath = webset.NewsHtmlSavePath;
                if (!htmlPath.StartsWith("~"))
                {
                    htmlPath = htmlPath.Insert(0, "~");
                }

                htmlSavePath = System.Web.HttpContext.Current.Server.MapPath(htmlPath + fileName);

                string htmlContent = GetHtmlTmpContent();
                if (string.IsNullOrEmpty(htmlContent))
                {
                    return;
                }
                else
                {
                    string url = this.uploadFilePath.Text;

                    string virtualPath = webset.WebPath;
                    string strVirtualPath = FormatPath(virtualPath);
                    if (strVirtualPath.EndsWith("/"))
                    {
                        strVirtualPath = strVirtualPath.Substring(0, strVirtualPath.Length - 1);
                    }

                    if (!strVirtualPath.StartsWith("/") && !string.IsNullOrEmpty(strVirtualPath))
                    {
                        strVirtualPath = strVirtualPath.Insert(0, "/");
                    }


                    //Modified By Robin 2014-11-17 for 7-11
                    //string includePath = webPath + strVirtualPath + "/Template";
                    //string imgUrl = webPath + strVirtualPath + url;
                    string includePath = webset.AttchFileServer + strVirtualPath + "/Template";
                    string imgUrl = webset.AttchFileServer + strVirtualPath + url;
                    //End

                    htmlContent = htmlContent.Replace("%%NewsPicture%%", imgUrl);
                    htmlContent = htmlContent.Replace("%%WebPath%%", includePath);


                    if (SVASessionInfo.SiteLanguage.ToLower() == "zh-cn")
                    {
                        htmlContent = htmlContent.Replace("%%NewsTitle%%", PromotionTitle2.Text);
                        htmlContent = htmlContent.Replace("%%NewsContent%%", PromotionMsgStr2.Text);
                    }
                    else if (SVASessionInfo.SiteLanguage.ToLower() == "zh-hk")
                    {
                        htmlContent = htmlContent.Replace("%%NewsTitle%%", PromotionTitle3.Text);
                        htmlContent = htmlContent.Replace("%%NewsContent%%", PromotionMsgStr3.Text);

                    }
                    else
                    {
                        htmlContent = htmlContent.Replace("%%NewsTitle%%", PromotionTitle1.Text);
                        htmlContent = htmlContent.Replace("%%NewsContent%%", PromotionMsgStr1.Text);

                    }



                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(htmlPath)))
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(htmlPath));

                    if (System.IO.File.Exists(htmlSavePath))
                        System.IO.File.Delete(htmlSavePath);

                    using (StreamWriter sw = new StreamWriter(htmlSavePath, true))
                    {
                        sw.Write(htmlContent);
                    }

                    ShowWarning(Resources.MessageTips.FileSaveAs + fileName);

                }
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.Logger.Instance.WriteErrorLog("News Generate to Html ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        private string GetHtmlTmpContent()
        {
            string htmlTmpPath = "";

            if (SVASessionInfo.SiteLanguage.ToLower() == "zh-cn")
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/news_zh-cn.html");
            }
            else if (SVASessionInfo.SiteLanguage.ToLower() == "zh-hk")
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/news_zh-hk.html");
            }
            else
            {
                htmlTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/Template/news.html");
            }

            if (System.IO.File.Exists(htmlTmpPath))
            {
                StreamReader sr = new StreamReader(htmlTmpPath, Encoding.UTF8);
                string str = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                return str;
            }
            else
                return "";
        }


        #endregion
    }
}