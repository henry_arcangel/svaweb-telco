﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CSVXMLMointoringRule.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="4px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Basic Settings"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:TextBox ID="RuleName" runat="server" Label="Rule Number：" Required="true" ShowRedStar="true"
                         MaxLength="64" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" />
                    <ext:TextBox ID="Description" runat="server" Label="Description：" Required="true" ShowRedStar="true" ToolTipTitle="描述"
                         OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" />
                    <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                         ToolTipTitle="开始日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                    </ext:DatePicker>    
                    <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                         ToolTipTitle="结束日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                    </ext:DatePicker>  
                    <ext:RadioButtonList ID="Status" runat="server" Label="状态：" Required="true" ShowRedStar="true">
                         <ext:RadioItem Text="生效" Value="1" Selected="true" />
                         <ext:RadioItem Text="关闭" Value="0" />
                    </ext:RadioButtonList>                                          
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="Function Information"
                 AutoHeight="true" AutoWidth="true">
               <Items>
                   <ext:DropDownList ID="Func1" runat="server" Label="Function：" Resizable="true" Required="true" ShowRedStar="true" >
                       <ext:ListItem Text="Member Import" Value="Member Import"  Selected="true"/>
                       <ext:ListItem Text="Points Adjustment" Value="Points Adjustment" />
                   </ext:DropDownList>   
                   <ext:TextBox ID="DownloadPath" runat="server" Label="Download Path：" ToolTipTitle="Download Path" ToolTip="Download Path"
                      AutoPostBack="true"/>
               </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="Time Setting Message"
                 AutoHeight="true" AutoWidth="true">
               <Items>
                   <ext:DropDownList ID="DayFlagID" runat="server" Label="有效天数设置：" Resizable="true" Required="true" ShowRedStar="true">
                   </ext:DropDownList>
                   <ext:DropDownList ID="WeekFlagID" runat="server" Label="有效周设置：" Resizable="true" Required="true" ShowRedStar="true">
                   </ext:DropDownList>
                   <ext:DropDownList ID="MonthFlagID" runat="server" Label="有效月设置：" Resizable="true" Required="true" ShowRedStar="true">
                   </ext:DropDownList>
                   <ext:TimePicker ID="ActiveTime" runat="server" Label="Set Valid Time：" Required="true" ShowRedStar="true">
                   </ext:TimePicker>   
               </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
