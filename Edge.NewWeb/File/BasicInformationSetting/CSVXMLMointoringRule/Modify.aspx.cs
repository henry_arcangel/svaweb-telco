﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLMointoringRule;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CSVXMLMointoringRule
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CSVXMLMointoringRule, Edge.SVA.Model.CSVXMLMointoringRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CSVXMLMointoringRuleController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Modify");

                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);
                RegisterCloseEvent(btnClose);
                SVASessionInfo.CSVXMLMointoringRuleController = null;
            }
            controller = SVASessionInfo.CSVXMLMointoringRuleController;           
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.ActiveTime.Text = DateTime.Parse(this.Model.ActiveTime.ToString()).ToString("hh:mm");
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.Func = this.Func1.SelectedValue.ToString();
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLMointoringRule Update\t Code:" + controller.ViewModel.MainTable.RuleName);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLMointoringRule Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.RuleName);
                ShowUpdateFailed();
            }
        }
    }
}
