﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLMointoringRule;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CSVXMLMointoringRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CSVXMLMointoringRule, Edge.SVA.Model.CSVXMLMointoringRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CSVXMLMointoringRuleController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);

                RegisterCloseEvent(btnClose);
                SVASessionInfo.CSVXMLMointoringRuleController = null;
            }
            controller = SVASessionInfo.CSVXMLMointoringRuleController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            string message = controller.ValidataObject(this.RuleName.Text.Trim());
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.Func = this.Func1.SelectedValue.ToString();
                controller.ViewModel.MainTable.RuleName = controller.ViewModel.MainTable.RuleName.ToUpper();
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLMointoringRule Add\t Code:" + controller.ViewModel.MainTable.RuleName); 
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLMointoringRule Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.RuleName);
                ShowAddFailed();
            }
        }
    }
}
