﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Reason;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.Reason
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Reason,Edge.SVA.Model.Reason>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ReasonController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ReasonController = null;
            }
            controller = SVASessionInfo.ReasonController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            //if (Tools.DALTool.isHasReasonCode(this.ReasonCode.Text.Trim(), 0))
            //{
            //    this.ShowWarning(Resources.MessageTips.ExistReasonCode);
            //    return;
            //}

            //Edge.SVA.Model.Reason item = this.GetAddObject();
            //if (item != null)
            //{
            //    item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = System.DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.ReasonCode = item.ReasonCode.ToUpper();
            //}
       
            //if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Reason>(item) > 0)
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + item.ReasonCode);
            //    this.CloseAndRefresh();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + item == null ? "No Data" : item.ReasonCode);
            //    this.ShowAddFailed();
            //}

            string message = controller.ValidataObject(this.ReasonCode.Text.Trim(), 0);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.ReasonCode = controller.ViewModel.MainTable.ReasonCode.ToUpper();
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + controller.ViewModel.MainTable.ReasonCode); 
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ReasonCode);
                ShowAddFailed();
            }
        }
    }
}
