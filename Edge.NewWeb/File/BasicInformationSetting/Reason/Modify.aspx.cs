﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Reason;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.Reason
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Reason, Edge.SVA.Model.Reason>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ReasonController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Modify");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ReasonController = null;
            }
            controller = SVASessionInfo.ReasonController;
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            //Edge.SVA.Model.Reason item = this.GetUpdateObject();

            //if (Tools.DALTool.isHasReasonCode(this.ReasonCode.Text.Trim(), item.ReasonID))
            //{
            //    this.ShowWarning(Resources.MessageTips.ExistReasonCode);
            //    return;
            //}

            //if (item != null)
            //{
            //    item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.ReasonCode = item.ReasonCode.ToUpper();
            //}
            //if (DALTool.Update<Edge.SVA.BLL.Reason>(item))
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + item.ReasonCode);
            //    this.CloseAndPostBack();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + item == null ? "No Data" : item.ReasonCode);
            //    this.ShowUpdateFailed();
            //}
            controller.ViewModel.MainTable = this.GetUpdateObject();

            string message = controller.ValidataObject(this.ReasonCode.Text.Trim(), controller.ViewModel.MainTable.ReasonID);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.ReasonCode = controller.ViewModel.MainTable.ReasonCode.ToUpper();

            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + controller.ViewModel.MainTable.ReasonCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ReasonCode);
                ShowUpdateFailed();
            }
        }
    }
}
