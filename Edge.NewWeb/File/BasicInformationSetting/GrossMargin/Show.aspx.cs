﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.GrossMargin
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.GrossMargin, Edge.SVA.Model.GrossMargin>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        GrossMarginController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.GrossMarginController = null;
            }
            controller = SVASessionInfo.GrossMarginController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            Dictionary<int, string> brandCache = new Dictionary<int, string>();
            Dictionary<int, string> storeCache = new Dictionary<int, string>();
            Dictionary<int, string> cardtypeCache = new Dictionary<int, string>();
            Dictionary<int, string> cardgradeCache = new Dictionary<int, string>();
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.GrossMarginCode);
                VenderType.Text = Model.VenderType == 1 ? "Brand":"Supplier";
                BuyerType.Text = Model.BuyerType == 1 ? "Company" : "Store";
                VenderID.Text = Model.VenderType == 1 ? Edge.Web.Tools.DALTool.GetBrandName(controller.ViewModel.MainTable.VenderID, brandCache) : Edge.Web.Tools.DALTool.GetSupplierDesc(controller.ViewModel.MainTable.VenderID);
                BuyerID.Text = Model.BuyerType == 1 ? Edge.Web.Tools.DALTool.GetCompanyName(controller.ViewModel.MainTable.BuyerID) : Edge.Web.Tools.DALTool.GetStoreName(controller.ViewModel.MainTable.BuyerID, storeCache);
                CardTypeID.Text = Edge.Web.Tools.DALTool.GetCardTypeName(Model.CardTypeID, cardtypeCache);
                CardGradeID.Text = Edge.Web.Tools.DALTool.GetCardGradeName(Model.CardGradeID, cardgradeCache);

                BindingDataGrid();
            }
        }
        private void BindingDataGrid()
        {
            int[] rowIndexs = this.MobileNumberPattern.SelectedRowIndexArray;
            controller = SVASessionInfo.GrossMarginController;
            this.MobileNumberPattern.RecordCount = controller.ViewModel.DetailTable.Count;
            this.MobileNumberPattern.DataSource = controller.ViewModel.DetailTable;
            this.MobileNumberPattern.DataBind();
            this.MobileNumberPattern.SelectedRowIndexArray = rowIndexs;
        }
    }
}