﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using System.Data;
using System.Text;
using Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using Edge.Utils.Tools;

namespace Edge.Web.File.BasicInformationSetting.GrossMargin
{
    public partial class addDetail : Edge.Web.Tools.BasePage<Edge.SVA.BLL.GrossMargin_MobileNo, Edge.SVA.Model.GrossMargin_MobileNo>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            GrossMarginController controller = SVASessionInfo.GrossMarginController;
            Edge.SVA.Model.GrossMargin_MobileNo md = new SVA.Model.GrossMargin_MobileNo();
            md.GrossMarginCode = Request["GrossMargin"];
            md.MobileNoPattern = this.MobileNoPattern.Text;
            md.PatternStart = int.Parse(this.PatternStart.Text);
            md.PatternLen = int.Parse(this.PatternLen.Text);
            md.KeyID = 0;
            int IsFind = 0;
            foreach (Edge.SVA.Model.GrossMargin_MobileNo a in controller.ViewModel.DetailTable)
            {
                if ((a.MobileNoPattern == md.MobileNoPattern) && (a.PatternStart == md.PatternStart))
                {
                    IsFind = 1;
                    break;
                }   
            }
            if (IsFind == 0)
                controller.ViewModel.DetailTable.Add(md);

            CloseAndPostBack();
        }

    }
}