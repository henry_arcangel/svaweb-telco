﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using System.Data;


namespace Edge.Web.File.BasicInformationSetting.GrossMargin
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.GrossMargin, Edge.SVA.Model.GrossMargin>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        GrossMarginController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                Tools.ControlTool.BindCardType(CardTypeID);
                
                SVASessionInfo.GrossMarginController = null;
            }

            controller = SVASessionInfo.GrossMarginController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.GrossMarginCode = controller.ViewModel.MainTable.GrossMarginCode.ToUpper();

                foreach (SVA.Model.GrossMargin_MobileNo md in controller.ViewModel.DetailTable)
                {
                    md.GrossMarginCode = controller.ViewModel.MainTable.GrossMarginCode;
                }
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "GrossMarginCode Add\t Code:" + controller.ViewModel.MainTable.GrossMarginCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "GrossMarginCode Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.GrossMarginCode);
                ShowAddFailed();                
            }
        }

        protected void btnNew1_OnClick(object sender, EventArgs e)
        {
            //btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1&MediaType=1", "Add");
            //转移界面
            ExecuteJS(WindowSearch.GetShowReference("addDetail.aspx?GrossMarginCode="+ GrossMarginCode.Text, "Add"));
            
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.MobileNumberPattern.SelectedRowIndexArray;
            List<Edge.SVA.Model.GrossMargin_MobileNo> modelList = new List<Edge.SVA.Model.GrossMargin_MobileNo>();

            controller.GetDetailList(controller.ViewModel.MainTable.GrossMarginCode);
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (SVA.Model.GrossMargin_MobileNo model in controller.ViewModel.DetailTable)
                {
                    if (this.MobileNumberPattern.Rows[rowIndexs[i]].Values[0] == model.KeyID.ToString() && this.MobileNumberPattern.Rows[rowIndexs[i]].Values[1] == model.GrossMarginCode.ToString())
                    {
                        modelList.Add(model);
                        break;
                    }
                }
            }

            foreach (Edge.SVA.Model.GrossMargin_MobileNo model in modelList)
            {
                controller.ViewModel.DetailTable.Remove(model);
            }
            SVASessionInfo.GrossMarginController = controller;
            this.MobileNumberPattern.SelectedRowIndexArray = new int[0];
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            SVASessionInfo.GrossMarginController = null;
            controller = SVASessionInfo.GrossMarginController;
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.MobileNumberPattern.SelectedRowIndexArray;
            controller = SVASessionInfo.GrossMarginController;
            this.MobileNumberPattern.RecordCount = controller.ViewModel.DetailTable.Count;
            this.MobileNumberPattern.DataSource = controller.ViewModel.DetailTable;
            this.MobileNumberPattern.DataBind();
            this.MobileNumberPattern.SelectedRowIndexArray = rowIndexs;
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGrade(CardGradeID, int.Parse(CardTypeID.SelectedValue.ToString()));
        }
        protected void VenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (VenderType.SelectedValue == "1")
                Tools.ControlTool.BindBrand(VenderID);
            else if (VenderType.SelectedValue == "2")
                Tools.ControlTool.BindAllSupplier(VenderID);
            else
            {
                VenderID.Items.Clear();
                VenderID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            }
        }
        protected void BuyerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BuyerType.SelectedValue == "1")
                Tools.ControlTool.BindCompanyID(BuyerID);
            else if (BuyerType.SelectedValue == "2")
                Tools.ControlTool.BindStore(BuyerID);
            else
            {
                BuyerID.Items.Clear();
                BuyerID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            }
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            //新界面
            BindingDataGrid();
        }
    }
}