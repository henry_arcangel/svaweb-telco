﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.GrossMargin
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RptBind("", "GrossMarginCode");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                Tools.ControlTool.BindCardType(CardTypeID);
                Tools.ControlTool.BindCardGrade(CardTypeID, 0);
            }
        }



        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    
                    string code = this.Code.Text.Trim();
                    string desc = this.Desc.Text.Trim();
                    string cardtypeid = "";
                    string cardgradeid = "";
                    string buyertype = "";
                    string buyerid = "";
                    string vendertype = "";
                    string venderid = "";
                    string report = "";
                    if (!(CardTypeID.SelectedValue == null))
                        cardtypeid = CardTypeID.SelectedValue.Trim() == "-1" ? "" : CardTypeID.SelectedValue.Trim();
                    if (!(CardGradeID.SelectedValue == null))
                        cardgradeid = CardGradeID.SelectedValue.Trim() == "-1" ? "" : CardGradeID.SelectedValue.Trim();
                    if (!(BuyerType.SelectedValue == null))
                        buyertype = BuyerType.SelectedValue.Trim()== "-1" ? "" : BuyerType.SelectedValue.Trim();
                    if (!(BuyerID.SelectedValue == null))
                        buyerid = BuyerID.SelectedValue.Trim()== "-1" ? "" : BuyerID.SelectedValue.Trim();
                    if (!(VenderType.SelectedValue == null))
                        vendertype = VenderType.SelectedValue.Trim() == "-1" ? "" : VenderType.SelectedValue.Trim();
                    if (!(VenderID.SelectedValue == null))
                        venderid = VenderID.SelectedValue.Trim() == "-1" ? "" : VenderID.SelectedValue.Trim();
                    if (!(Report.SelectedValue == null))
                        report = Report.SelectedValue.Trim() == "-1" ? "" : Report.SelectedValue.Trim();

                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" GrossMarginCode like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(desc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Description1";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(desc);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(buyertype))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" BuyerType ='");
                        sb.Append(buyertype);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(buyerid))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" BuyerID ='");
                        sb.Append(buyerid);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(vendertype))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" VenderType ='");
                        sb.Append(vendertype);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(venderid))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" VenderID ='");
                        sb.Append(venderid);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(cardtypeid))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardTypeID ='");
                        sb.Append(cardtypeid);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(cardgradeid))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardGradeID ='");
                        sb.Append(cardgradeid);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(report))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" Report ='");
                        sb.Append(report);
                        sb.Append("'");
                    }

                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                GrossMarginController controller = new GrossMarginController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("GrossMargin", "Load Failed", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            GrossMarginController controller = new GrossMarginController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "GrossMarginCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "GrossMarginCode");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "GrossMarginCode");
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tools.ControlTool.BindCardGrade(CardGradeID, int.Parse(CardTypeID.SelectedValue.ToString()));
        }
        protected void VenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (VenderType.SelectedValue == "1")
                Tools.ControlTool.BindBrand(VenderID);
            else if (VenderType.SelectedValue == "2")
                Tools.ControlTool.BindAllSupplier(VenderID);
            else
            {
                VenderID.Items.Clear();
                VenderID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            }                
        }
        protected void BuyerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BuyerType.SelectedValue == "1")
                Tools.ControlTool.BindCompanyID(BuyerID);
            else if (BuyerType.SelectedValue == "2")
                Tools.ControlTool.BindStore(BuyerID);
            else
            {
                BuyerID.Items.Clear();
                BuyerID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            }   
        }
    }
}