﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.GrossMargin.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
            <ext:TextBox ID="GrossMarginCode" runat="server" Label="Gross Margin Code：" Required="true" ShowRedStar="true"
                MaxLength="20" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" />
            <ext:TextBox ID="Description1" runat="server" Label="Description：" Required="true" ShowRedStar="true" AutoPostBack="true"/>
            <ext:TextBox ID="Description2" runat="server" Label="Other Description1：" AutoPostBack="true"/>
            <ext:TextBox ID="Description3" runat="server" Label="Other Description2：" AutoPostBack="true"/>
            <ext:DropDownList ID="VenderType" runat="server" Label="Vendor Type：" AutoPostBack="true" Resizable="true" OnSelectedIndexChanged="VenderType_SelectedIndexChanged">
                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                <ext:ListItem Text="Brand" Value="1" />
                <ext:ListItem Text="Supplier" Value="2" />
            </ext:DropDownList>
            <ext:DropDownList ID="VenderID" runat="server" Label="Vendor Name：" AutoPostBack="true" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="BuyerType" runat="server" Label="Buyer："  Required="true" ShowRedStar="true"　AutoPostBack="true" Resizable="true"　OnSelectedIndexChanged="BuyerType_SelectedIndexChanged">
                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                <ext:ListItem Text="Company" Value="1" />
                <ext:ListItem Text="Store" Value="2" />
            </ext:DropDownList>
            <ext:DropDownList ID="BuyerID" runat="server" Label="Buyer Name：" AutoPostBack="true" Resizable="true">
            </ext:DropDownList>
      　　　<ext:DropDownList ID="CardTypeID" runat="server" Label="Card Type：" AutoPostBack="true" Resizable="true"
           　　　　OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged">
　　　　　　</ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Label="Card Grade：" AutoPostBack="true" Resizable="true">
            </ext:DropDownList>
            <ext:TextBox ID="VAT" runat="server" Label="VAT：" Required="true" ShowRedStar="true" AutoPostBack="true"/>
            <ext:DropDownList ID="Report" runat="server" Label="Report："  AutoPostBack="true" Resizable="true" >
               <ext:ListItem Text="-------" Value=""  Selected="true"/>
               <ext:ListItem Text="Sales & Gross Margin" Value="1" />
               <ext:ListItem Text="Telco Purchases" Value="2" />
               <ext:ListItem Text="Accounting Report" Value="3" />
               <ext:ListItem Text="Telco Order Summary" Value="4" />
            </ext:DropDownList>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Mobile Number Pattern">
                <Items>
                    <ext:Grid ID="MobileNumberPattern" ShowBorder="true" ShowHeader="true" Title="Mobile Number Pattern"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew1" Text="新增" Icon="Add" runat="server" OnClick="btnNew1_OnClick">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                    <ext:Button ID="btnClear" Text="清空" Icon="Delete" runat="server" OnClick="btnClear_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="KeyID" DataField="DetailTable.KeyID" HeaderText="主键" Hidden="true" />
                            <ext:BoundField ColumnID="GrossMarginCode" DataField="DetailTable.GrossMarginCode" HeaderText="编号"  Hidden="true"/>
                            <ext:BoundField ColumnID="MobileNoPattern" DataField="DetailTable.MobileNoPattern" HeaderText="Mobile Number Patter" />
                            <ext:BoundField ColumnID="PatternStart" DataField="DetailTable.PatternStart" HeaderText="Patter Start" />
                            <ext:BoundField ColumnID="PatternLen" DataField="DetailTable.PatternLen" HeaderText="Patter Length" />
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window1" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataIFrameUrlFields="DetailTable.GrossMarginCode,DetailTable.KeyID"
                                DataIFrameUrlFormatString="modifyDetail.aspx?GrossMarginCode={0}&KeyID={1}"  Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>

    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <ext:Window ID="WindowSearch" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="560px">
    </ext:Window>

    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>


