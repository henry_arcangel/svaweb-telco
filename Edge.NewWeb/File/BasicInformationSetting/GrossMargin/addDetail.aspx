﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addDetail.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.GrossMargin.addDetail" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sForm4,sForm5" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="添加后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="MobileNoPattern" runat="server" Label="MobileNo Pattern：" Required="true" ShowRedStar="true" AutoPostBack="true"/>
            <ext:TextBox ID="PatternStart" runat="server" Label="Pattern Start：" Required="true" ShowRedStar="true" AutoPostBack="true"/>
            <ext:TextBox ID="PatternLen" runat="server" Label="Pattern Length：" Required="true" ShowRedStar="true" AutoPostBack="true"/>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>