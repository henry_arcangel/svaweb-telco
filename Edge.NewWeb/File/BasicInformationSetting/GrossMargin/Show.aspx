﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.GrossMargin.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" >
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
        
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
            <ext:Label ID="GrossMarginCode" runat="server" Label="Gross Margin Code：" ShowRedStar="true" />
            <ext:Label ID="Description1" runat="server" Label="Description：" ShowRedStar="true" />
            <ext:Label ID="Description2" runat="server" Label="Other Description1："/>
            <ext:Label ID="Description3" runat="server" Label="Other Description2："/>
            <ext:Label ID="VenderType" runat="server" Label="Vendor："/>
            <ext:Label ID="VenderID" runat="server" Label="Vendor Name："/>
            <ext:Label ID="BuyerType" runat="server" Label="Buyer：" />
            <ext:Label ID="BuyerID" runat="server" Label="Buyer Name：" />
      　　　<ext:Label ID="CardTypeID" runat="server" Label="Card Type：" />
            <ext:Label ID="CardGradeID" runat="server" Label="Card Grade：">
            </ext:Label>
            <ext:Label ID="VAT" runat="server" Label="VAT：" ShowRedStar="true"/>
            <ext:Label ID="Report" runat="server" Label="Report：" />
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Mobile Number Pattern">
                <Items>
                    <ext:Grid ID="MobileNumberPattern" ShowBorder="true" ShowHeader="true" Title="Mobile Number Pattern"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true">
                        <Columns>
                            <ext:BoundField ColumnID="KeyID" DataField="DetailTable.KeyID" HeaderText="主键" Hidden="true" />
                            <ext:BoundField ColumnID="GrossMarginCode" DataField="DetailTable.GrossMarginCode" HeaderText="编号"  Hidden="true"/>
                            <ext:BoundField ColumnID="MobileNoPattern" DataField="DetailTable.MobileNoPattern" HeaderText="Mobile Number Patter" />
                            <ext:BoundField ColumnID="PatternStart" DataField="DetailTable.PatternStart" HeaderText="Patter Start" />
                            <ext:BoundField ColumnID="PatternLen" DataField="DetailTable.PatternLen" HeaderText="Patter Length" />                            
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>


