﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.DistributeTemplate;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.IO;

namespace Edge.Web.File.BasicInformationSetting.DistributeTemplate
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.DistributeTemplate, Edge.SVA.Model.DistributeTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DistributeTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.DistributeTemplateController = null;
            }
            controller = SVASessionInfo.DistributeTemplateController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            //if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            //{
            //    ShowWarning(Resources.MessageTips.ExistDistributeTemplateCode);
            //    return;
            //}

            string message = controller.ValidataObject(this.DistributionCode.Text.Trim(), 0);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                if (!ValidateFile(this.TemplateFile.FileName))
                {
                    return;
                }

                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");

            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Add Success\tCode:" + controller.ViewModel.MainTable.DistributionCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Add Failed\tCode:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.DistributionCode);
                ShowAddFailed();
            }

        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.DistributeTemplateFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.DistributeTemplateFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
