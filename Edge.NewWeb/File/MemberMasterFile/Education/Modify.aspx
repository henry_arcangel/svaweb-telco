﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MemberMasterFile.Education.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="EducationCode" runat="server" Label="学历编号：" Required="true" ShowRedStar="true"
                MaxLength="20" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="EducationName1" runat="server" Label="描述：" Required="true" ShowRedStar="true"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="EducationName2" runat="server" Label="其他描述1："
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="EducationName3" runat="server" Label="其他描述2："
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
