﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Nation
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Nation,Edge.SVA.Model.Nation>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Modify");
                RegisterCloseEvent(btnClose);
            }
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.Nation item = this.GetUpdateObject();

            if (item == null)
            {
                this.ShowUpdateFailed();
                return;
            }

            if (Tools.DALTool.isHasNationCode(this.NationCode.Text.Trim(), item.NationID))
            {
                this.ShowWarning(Resources.MessageTips.ExistNationCode);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.NationCode = item.NationCode.ToUpper().Trim();
                item.CountryCode = item.CountryCode.ToUpper().Trim();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Nation>(item))
            {
                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }
    }
}
