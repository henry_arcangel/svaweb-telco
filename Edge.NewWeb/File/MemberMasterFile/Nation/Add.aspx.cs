﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Nation
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Nation,Edge.SVA.Model.Nation>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Add");
                RegisterCloseEvent(btnClose);
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasNationCode(this.NationCode.Text.Trim(), 0))
            {
                this.ShowWarning(Resources.MessageTips.ExistNationCode);
                return;
            }

            Edge.SVA.Model.Nation item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.NationCode = item.NationCode.ToUpper().Trim();
                item.CountryCode = item.CountryCode.ToUpper().Trim();
            }


            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Nation>(item) > 0)
            {
                this.CloseAndRefresh();
            }
            else
            {
                this.ShowAddFailed();
            }
            
        }
    }
}
