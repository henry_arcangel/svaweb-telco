﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BatchAutoComplete.ascx.cs"
    Inherits="Edge.Web.Controls.BatchAutoComplete" %>
<asp:TextBox ID="BatchCouponID" runat="server" MaxLength="20"></asp:TextBox>
<asp:HiddenField ID="BatchCouponIDValue" runat="server" />
<asp:Repeater ID="NewBatch" runat="server">
    <HeaderTemplate>
        <table id="newBatchIDS" class="selectList">
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td width="90%"><%#Eval("value") %></td>
            <td width="10%">
                <asp:HiddenField ID="value" runat="server" Value='<%#Eval("key") %>' />
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>

<script type="text/javascript">
    $(function() {

        var id = "#<%=BatchCouponID.ClientID %>";
        var valueID = "#<%=BatchCouponIDValue.ClientID %>";
        var top = $(id).offset().top;
        var left = $(id).offset().left;

        $("#newBatchIDS").css({
            "top": top + $(id).height() + 3 + "px",
            "left": left + "px",
            "width": $(id).width() + 8 + "px"
        });

        $("#newBatchIDS tr").hover(
                        function() {
                            $(this).addClass("selectedItem");
                        },
			            function() {
			                $(this).removeClass("selectedItem");
			            }).click(function() {
			                var id = "#<%=BatchCouponID.ClientID %>";
			                $(id).attr("value", $.trim($(this).text()));
			                $(valueID).attr("value", $.trim($(this).find("td input").val()));
			                closeBatch();
			            });


        $("#newBatchIDS").hide();

        $("#<%=BatchCouponID.ClientID %>").bind("keyup", function() {

            var value = $(id).val();
            if (value.length <= 1) return;

            setTimeout(fillResult(id,valueID, value), 400);

        }).focus(function() {

            var value = $(id).val();
            if (value.length >= 12) return;

            if (value.length > 1) {
                fillData(id,valueID, value);
            }
            else {
                $("#newBatchIDS").show();
            }
        }).blur(function() {
            var isSelect = false;
            $("#newBatchIDS tr").each(function() {
                if ($(this).hasClass("selectedItem")) {
                    isSelect = true;
                    return;
                };
            });
            if (isSelect) return;
            $("#BatchIDS tr").each(function() {
                if ($(this).hasClass("selectedItem")) {
                    isSelect = true;
                    return;
                }
            });
            if (isSelect) return;
            closeBatch();

        });
    });

    function closeBatch() {
        $("#BatchIDS").empty();
        $("#BatchIDS").remove();
        $("#newBatchIDS").hide();

    }

    function fillResult(id,valueID, value) {
        return function() {
            if (value == $(id).val()) {

                fillData(id,valueID, value);
            }
        }
    }

    function fillData(id,valueID, value) {
        var coupontTypeClientID = '<%=CoupontTypeClientID %>';

        if (coupontTypeClientID.length > 0) {
            var couponTypeID = $("#<%=CoupontTypeClientID %>").val();
        }
     
        $.ajax({
            type: "get",
            url: '<%=ResolveUrl("~/Ashx/GetBatchID.ashx") %>',
            data: { "batchCode": value, "top": 10, "couponTypeID": couponTypeID },
            success: function(data, textStatus) {

                closeBatch();

                if (data == null || data == "") return;
                if (data.list == undefined && data.JSonModel == undefined) return;

                var html = "<table id=\"BatchIDS\" class=\"selectList\">";
                for (i = 0; ; i++) {
                    var list = data.list || data.JSonModel;
                    if (list[i] == undefined) {
                        if (i == 0) return;
                        else break;
                    }
                    html += "<tr><td>" + list[i].BatchCouponCode + "</td><td><input type='hidden' value='" + list[i].BatchCouponID + "'/></td></tr>";
                }

                var top = $(id).offset().top;
                var left = $(id).offset().left;

                html += "</table>";
                $("body").append(html);

                if (top + $("#BatchIDS").height() > $(parent.frames["sysMain"]).height()) {
                    top = top - $("#BatchIDS").height() - 3;
                }
                else {
                    top = top + $(id).height() + 3;
                }



                $("#BatchIDS").css({
                    "top": top + "px",
                    "left": left + "px",
                    "width": $(id).width() + 8 + "px"
                });



                $("#BatchIDS tr").hover(
                        function() {
                            $(this).addClass("selectedItem");
                        },
			            function() {
			                $(this).removeClass("selectedItem");
			            });

                $("#BatchIDS tr").click(function() {
                    $(id).attr("value", $.trim($(this).text()));
                    $(valueID).attr("value", $.trim($(this).find("td input").val()));
                    closeBatch();
                });
            }
        });
    }
</script>

