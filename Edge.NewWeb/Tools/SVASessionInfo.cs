﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;
using FineUI;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponIssue;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponActive;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponRedeem;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponVoid;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.ChangeExpiryDate;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.ChangeDenomination;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.ChangeStatus;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponPushManual;
using Edge.Web.Controllers.File.BasicInformationSetting.Advertisements;
using Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;
using Edge.Web.Controllers.File.BasicInformationSetting.CurrencyExchangeRate;
using Edge.Web.Controllers.File.BasicInformationSetting.DistributeTemplate;
using Edge.Web.Controllers.File.BasicInformationSetting.PasswordRulesSetting;
using Edge.Web.Controllers.File.BasicInformationSetting.Reason;
using Edge.Web.Controllers.File.BasicInformationSetting.StoreNature;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.Web.Controllers.File.BasicInformationSetting.Departments;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1;
using Edge.Web.Controllers.File.BasicInformationSetting.Sponsor;
using Edge.Web.Controllers.File.BasicInformationSetting.Supplier;
using Edge.Web.Controllers.File.BasicInformationSetting.Company;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;
using Edge.Web.Controllers.File.BasicInformationSetting.StoreGroup;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangePoints;
using Edge.Web.Controllers.WebBuying.MasterFiles.PromotionInfos.BUY_DAYFLAG;
using Edge.Web.Controllers.WebBuying.MasterFiles.PromotionInfos.BUY_MONTHFLAG;
using Edge.Web.Controllers.WebBuying.MasterFiles.PromotionInfos.BUY_WEEKFLAG;
using Edge.Web.Controllers;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardRedeem;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardActive;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardTopup;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeExpiryDate;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardVoid;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardIssue;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeStatus;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardTransfer;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.TelcoOrder;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.TelcoStoreWalletTransfer;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.WalletBalanceAdjustment;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.MobileTopUp;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.OfflineTopUp;
using Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.TelcoReturnOrder;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.WalletRule;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLBinding;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLMointoringRule;

namespace Edge.Web.Tools
{
    public class SVASessionInfo
    {
        private const string CardGradeAddControllerStr = "SVASessionInfo_CardGradeAddController";
        private const string CardGradeModifyControllerStr = "SVASessionInfo_CardGradeModifyController";
        private const string CouponTypeAddControllerStr = "SVASessionInfo_CouponTypeAddController";
        private const string CouponTypeModifyControllerStr = "SVASessionInfo_CouponTypeModifyControllerStr";
        private const string CampaignAddControllerStr = "SVASessionInfo_CampaignAddControllerStr";
        private const string CampaignModifyControllerStr = "SVASessionInfo_CampaignModifyControllerStr";
        private const string CampaignDeleteControllerStr = "SVASessionInfo_CampaignDeleteControllerStr";
        private const string CouponReplenishRuleAddControllerStr = "SVASessionInfo_CouponReplenishRuleAddControllerStr";
        private const string InventoryReplenishRuleAddControllerStr = "SVASessionInfo_InventoryReplenishRuleAddControllerStr";
        private const string MsgTemplateControllerStr = "SVASessionInfo_MsgTemplateControllerStr";

        private const string MessageHTMLStr = "SVASessionInfo_MessageHTMLStr";
        private const string PassErrorCountStr = "SVASessionInfo_PassErrorCountStr";
        private const string SiteLanguageStr = "SVASessionInfo_SiteLanguageStr";
        private const string CheckCodeStr = "SVASessionInfo_CheckCodeStr";
        private const string ReturnPageStr = "SVASessionInfo_ReturnPageStr";
        private const string CurrentUserStr = "SVASessionInfo_CurrentUserStr";
        private const string UserStyleStr = "SVASessionInfo_UserStyleStr";
        private const string TabsStr = "SVASessionInfo_TabsStr";
        private const string MessageOfApproveStr = "SVASessionInfo_MessageOfApproveStr";
        private const string StrWheresysStr = "SVASessionInfo_StrWheresysStr";
        private const string ExportTimeStr = "SVASessionInfo_ExportTimeStr";
        private const string TempImageStr = "SVASessionInfo_TempImageStr";
        private const string CardTypeCodeNameStr = "SVASessionInfo_CardTypeCodeNameStr";
        private const string CardGradeCodeNameStr = "SVASessionInfo_CardGradeCodeNameStr";
        private const string HiddenFormInfoStr = "SVASessionInfo_HiddenFormInfoStr";
        private const string CouponIssueControllerStr = "SVASessionInfo_CouponIssueControllerStr";
        private const string CouponActiveControllerStr = "SVASessionInfo_CouponActiveControllerStr";
        private const string CouponRedeemControllerStr = "SVASessionInfo_CouponRedeemControllerStr";
        private const string CouponVoidControllerStr = "SVASessionInfo_CouponVoidControllerStr";
        private const string ChangeExpiryDateControllerStr = "SVASessionInfo_ChangeExpiryDateControllerStr";
        private const string ChangeDenominationControllerStr = "SVASessionInfo_ChangeDenominationControllerStr";
        private const string ChangeStatusControllerStr = "SVASessionInfo_ChangeStatusControllerStr";
        private const string AdvertisementsAddControllerStr = "SVASessionInfo_AdvertisementsAddControllerStr";
        private const string AdvertisementsModifyControllerStr = "SVASessionInfo_AdvertisementsModifyControllerStr";
        private const string AdvertisementsDeleteControllerStr = "SVASessionInfo_AdvertisementsDeleteControllerStr";
        private const string USEFULEXPRESSIONSAddControllerStr = "SVASessionInfo_USEFULEXPRESSIONSAddControllerStr";
        private const string USEFULEXPRESSIONSModifyControllerStr = "SVASessionInfo_USEFULEXPRESSIONSModifyControllerStr";
        private const string CurrencyControllerStr = "SVASessionInfo_CurrencyControllerStr";
        private const string DistributeTemplateControllerStr = "SVASessionInfo_DistributeTemplateControllerStr";
        private const string PasswordRulesSettingControllerStr = "SVASessionInfo_PasswordRulesSettingControllerStr";
        private const string ReasonControllerStr = "SVASessionInfo_ReasonControllerStr";
        private const string StoreNatureControllerStr = "SVASessionInfo_StoreNatureControllerStr";
        private const string BrandControllerStr = "SVASessionInfo_BrandControllerStr";
        private const string CouponPushControllerStr = "SVASessionInfo_CouponPushControllerStr";
        private const string StoreControllerStr = "SVASessionInfo_StoreControllerStr";
        private const string ImportMemberCotrollerStr = "SVASessionInfo_ImportMemberCotrollerStr";
        private const string ImportStorePathStr = "SVASessionInfo_ImportStorePathStr";
        private const string DepartmentsControllerStr = "SVASessionInfo_DepartmentsControllerStr";
        private const string ChangeCardDenominationControllerStr = "SVASessionInfo_ChangeCardDenominationControllerStr";
        private const string ImportMemberInfos1ControllerStr = "SVASessionInfo_ImportMemberInfos1ControllerStr";
        private const string SponsorControllerStr = "SVASessionInfo_SponsorControllerStr";
        private const string SupplierControllerStr = "SVASessionInfo_SupplierControllerStr";
        private const string CompanyControllerStr = "SVASessionInfo_CompanyControllerStr";
        private const string PointRuleControllerStr = "SVASessionInfo_PointRuleControllerStr";
        private const string StoreGroupControllerStr = "SVASessionInfo_StoreGroupControllerStr";
        private const string MemberOrderControllerStr = "SVASessionInfo_MemberOrderControllerStr";
        private const string CardChangePointsControllerStr = "SVASessionInfo_CardChangePointsControllerStr";
        private const string CheckDBNameStr = "SVASessionInfo_CheckDBNameStr";
        private const string BuyingDayFlagControllerStr = "SVASessionInfo_BuyingDayFlagControllerStr";
        private const string BuyingMonthFlagControllerStr = "SVASessionInfo_BuyingMonthFlagControllerStr";
        private const string BuyingWeekFlagControllerStr = "SVASessionInfo_BuyingWeekFlagControllerStr";


        private const string CardIssueControllerStr = "SVASessionInfo_CardIssueControllerStr"; //Add by Nathan 20140609
        private const string CardActiveControllerStr = "SVASessionInfo_CardActiveControllerStr"; //Add by Nathan 20140609
        private const string CardVoidControllerStr = "SVASessionInfo_CardVoidControllerStr"; //Add by Alex 2014-06-12
        private const string CardRedeemControllerStr = "SVASessionInfo_CardRedeemControllerStr"; //Add by Alex 2014-06-12
        private const string TenderControllerStr = "SVASessionInfo_CardRedeemControllerStr"; //Add by Nathan 2014-06-26
        private const string ImportTenderPathStr = "SVASessionInfo_ImportTenderPathStr";//Add by Nathan 2014-06-26
        private const string MemberClauseControllerStr = "SVASessionInfo_MemberClauseControllerStr"; // Add by Alex 2014-07-02

        private const string CardChangeStatusControllerStr = "SVASessionInfo_CardChangeStatusControllerStr"; //Add by Alex 2014-07-03

        private const string ChangeCardExpiryDateControllerStr = "SVASessionInfo_ChangeCardExpiryDateControllerStr"; //Add by Alex 2014-07-12

        private const string CardTopupControllerStr = "SVASessionInfo_CardTopupControllerStr"; //Add by Alex 2014-07-12

        //add by frank 20140720
        private const string BuyingNewPromotionControllerStr = "SVASessionInfo_BuyingNewPromotionControllerStr";
        private const string BuyingProdCodeStr = "SVASessionInfo_BuyingProdCodeStr";
        private const string BuyingProdDescStr = "SVASessionInfo_BuyingProdDescStr";
        private const string BuyingDepartCodeStr = "SVASessionInfo_BuyingDepartCodeStr";
        private const string BuyingTendCodeStr = "SVASessionInfo_BuyingTendCodeStr";
        private const string BuyingCouponTypeCodeStr = "SVASessionInfo_BuyingCouponTypeCodeStr";

        private const string TempObjectStr = "SVASessionInfo_TempObjectStr";

        private const string CardTransferControllerStr = "SVASessionInfo_CardTransferControllerStr";

        //Add by Gavin 2015-02-03
        private const string TelcoOrderControllerStr = "SVASessionInfo_TelcoOrderControllerStr";
        private const string TelcoStoreWalletTransferControllerStr = "SVASessionInfo_TelcoStoreWalletTransferController";
        private const string AirloadMonitoringRuleControllerStr = "SVASessionInfo_AirloadMonitoringRuleController";
        private const string WalletBalanceAdjustmentControllerStr = "SVASessionInfo_WalletBalanceAdjustmentController";
        private const string MobileTopUpControllerStr = "SVASessionInfo_MobileTopUpController";
        private const string OfflineTopUpControllerStr = "SVASessionInfo_OfflineTopUpController";
        private const string GrossMarginControllerStr = "SVASessionInfo_GrossMarginController";
        private const string TelcoReturnOrderControllerStr = "SVASessionInfo_TelcoReturnOrderController";
        private const string WalletRuleControllerStr = "SVASessionInfo_WalletRuleController";
        private const string ImportCouponRedeemPathStr = "SVASessionInfo_ImportCouponRedeemPathStr";

        private const string CSVXMLBindingControllerStr = "SVASessionInfo_CSVXMLBindingControllerStr";
        private const string CSVXMLMointoringRuleControllerStr = "SVASessionInfo_CSVXMLMointoringRuleControllerStr";

        #region CardGrade Controllers
        public static CardGradeAddController CardGradeAddController
        {
            get
            {
                if (HttpContext.Current.Session[CardGradeAddControllerStr] == null)
                {
                    HttpContext.Current.Session[CardGradeAddControllerStr] = new CardGradeAddController();
                }
                return HttpContext.Current.Session[CardGradeAddControllerStr] as CardGradeAddController;
            }
            set { HttpContext.Current.Session[CardGradeAddControllerStr] = value; }
        }
        public static CardGradeModifyController CardGradeModifyController
        {
            get
            {
                if (HttpContext.Current.Session[CardGradeModifyControllerStr] == null)
                {
                    HttpContext.Current.Session[CardGradeModifyControllerStr] = new CardGradeModifyController();
                }
                return HttpContext.Current.Session[CardGradeModifyControllerStr] as CardGradeModifyController;
            }
            set { HttpContext.Current.Session[CardGradeModifyControllerStr] = value; }
        }
        #endregion

        #region CouponType Controllers
        public static CouponTypeAddController CouponTypeAddController
        {
            get
            {
                if (HttpContext.Current.Session[CouponTypeAddControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponTypeAddControllerStr] = new CouponTypeAddController();
                }
                return HttpContext.Current.Session[CouponTypeAddControllerStr] as CouponTypeAddController;
            }
            set { HttpContext.Current.Session[CouponTypeAddControllerStr] = value; }
        }
        public static CouponTypeModifyController CouponTypeModifyController
        {
            get
            {
                if (HttpContext.Current.Session[CouponTypeModifyControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponTypeModifyControllerStr] = new CouponTypeModifyController();
                }
                return HttpContext.Current.Session[CouponTypeModifyControllerStr] as CouponTypeModifyController;
            }
            set { HttpContext.Current.Session[CouponTypeModifyControllerStr] = value; }
        }
        #endregion

        #region Campaign Controllers
        public static CampaignAddController CampaignAddController
        {
            get
            {
                if (HttpContext.Current.Session[CampaignAddControllerStr] == null)
                {
                    HttpContext.Current.Session[CampaignAddControllerStr] = new CampaignAddController();
                }
                return HttpContext.Current.Session[CampaignAddControllerStr] as CampaignAddController;
            }
            set { HttpContext.Current.Session[CampaignAddControllerStr] = value; }
        }
        public static CampaignModifyController CampaignModifyController
        {
            get
            {
                if (HttpContext.Current.Session[CampaignModifyControllerStr] == null)
                {
                    HttpContext.Current.Session[CampaignModifyControllerStr] = new CampaignModifyController();
                }
                return HttpContext.Current.Session[CampaignModifyControllerStr] as CampaignModifyController;
            }
            set { HttpContext.Current.Session[CampaignModifyControllerStr] = value; }
        }
        public static CampaignDeleteController CampaignDeleteController
        {
            get
            {
                if (HttpContext.Current.Session[CampaignDeleteControllerStr] == null)
                {
                    HttpContext.Current.Session[CampaignDeleteControllerStr] = new CampaignDeleteController();
                }
                return HttpContext.Current.Session[CampaignDeleteControllerStr] as CampaignDeleteController;
            }
            set { HttpContext.Current.Session[CampaignDeleteControllerStr] = value; }
        }
        #endregion

        #region MsgTempLate
        public static MsgTemplateController MsgTemplateController
        {
            get
            {
                if (HttpContext.Current.Session[MsgTemplateControllerStr] == null)
                {
                    HttpContext.Current.Session[MsgTemplateControllerStr] = new MsgTemplateController();
                }
                return HttpContext.Current.Session[MsgTemplateControllerStr] as MsgTemplateController;
            }
            set { HttpContext.Current.Session[MsgTemplateControllerStr] = value; }
        }
        #endregion

        #region Sample

        //public static CouponTypeModifyController Controller
        //{
        //    get
        //    {
        //        if (HttpContext.Current.Session[ControllerStr] == null)
        //        {
        //            HttpContext.Current.Session[ControllerStr] = new CouponTypeModifyController();
        //        }
        //        return HttpContext.Current.Session[ControllerStr] as CouponTypeModifyController;
        //    }
        //    set { HttpContext.Current.Session[ControllerStr] = value; }
        //}
        #endregion

        #region Message HTML
        public static string MessageHTML
        {
            get
            {
                if (HttpContext.Current.Session[MessageHTMLStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[MessageHTMLStr].ToString();
            }
            set { HttpContext.Current.Session[MessageHTMLStr] = value; }
        }
        #endregion

        #region PassErrorCount Admin
        public static int PassErrorCount
        {
            get
            {
                if (HttpContext.Current.Session[PassErrorCountStr] == null)
                {
                    return 0;
                }
                int val = 0;
                if (int.TryParse(HttpContext.Current.Session[PassErrorCountStr].ToString(), out val))
                {
                    return val;
                }
                else 
                {
                    return 0;
                }
            }
            set
            {
                if (value == 0)
                {
                    HttpContext.Current.Session[PassErrorCountStr] = null;
                }
                else
                {
                    HttpContext.Current.Session[PassErrorCountStr] = value;
                }
            }
        }
        #endregion

        #region SiteLanguage
        public static string SiteLanguage
        {
            get
            {
                if (HttpContext.Current.Session[SiteLanguageStr] != null)
                {
                    return HttpContext.Current.Session[SiteLanguageStr].ToString();
                }
                return "en-us";
            }
            set
            {
                HttpContext.Current.Session[SiteLanguageStr] = value;
            }
        }
        #endregion

        #region CheckCode
        public static string CheckCode
        {
            get
            {
                if (HttpContext.Current.Session[CheckCodeStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[CheckCodeStr].ToString();
            }
            set { HttpContext.Current.Session[CheckCodeStr] = value; }
        }
        #endregion

        #region ReturnPage
        public static string ReturnPage
        {
            get
            {
                if (HttpContext.Current.Session[ReturnPageStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[ReturnPageStr].ToString();
            }
            set { HttpContext.Current.Session[ReturnPageStr] = value; }
        }
        #endregion

        #region CurrentUser
        public static Edge.Security.Manager.User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session[CurrentUserStr] == null)
                {
                    HttpContext.Current.Session[CurrentUserStr] = new Edge.Security.Manager.User();
                }
                return HttpContext.Current.Session[CurrentUserStr] as Edge.Security.Manager.User;
            }
            set { HttpContext.Current.Session[CurrentUserStr] = value; }
        }
        #endregion

        #region UserStyle
        public static int UserStyle
        {
            get
            {
                if (HttpContext.Current.Session[UserStyleStr] == null)
                {
                    return 0;
                }
                int val = 0;
                if (int.TryParse(HttpContext.Current.Session[UserStyleStr].ToString(), out val))
                {
                    return val;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (value == 0)
                {
                    HttpContext.Current.Session[UserStyleStr] = null;
                }
                else
                {
                    HttpContext.Current.Session[UserStyleStr] = value;
                }
            }
        }
        #endregion

        #region Tabs
        public static string Tabs
        {
            get
            {
                if (HttpContext.Current.Session[TabsStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[TabsStr].ToString();
            }
            set { HttpContext.Current.Session[TabsStr] = value; }
        }
        #endregion

        #region MessageOfApprove
        public static string MessageOfApprove
        {
            get
            {
                if (HttpContext.Current.Session[MessageOfApproveStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[MessageOfApproveStr].ToString();
            }
            set { HttpContext.Current.Session[MessageOfApproveStr] = value; }
        }
        #endregion

        #region strWheresys
        public static string StrWheresys
        {
            get
            {
                if (HttpContext.Current.Session[StrWheresysStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[StrWheresysStr].ToString();
            }
            set { HttpContext.Current.Session[StrWheresysStr] = value; }
        }
        #endregion

        #region ExportTime
        public static string ExportTime
        {
            get
            {
                if (HttpContext.Current.Session[ExportTimeStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[ExportTimeStr].ToString();
            }
            set { HttpContext.Current.Session[ExportTimeStr] = value; }
        }
        #endregion

        #region TempImage
        public static byte[] TempImage
        {
            get
            {
                if (HttpContext.Current.Session[TempImageStr] == null)
                {
                    HttpContext.Current.Session[TempImageStr] = new byte[0];
                }
                return HttpContext.Current.Session[TempImageStr] as byte[];
            }
            set { HttpContext.Current.Session[TempImageStr] = value; }
        }
        #endregion

        #region CardTypeCodeName
        public static string CardTypeCodeName
        {
            get
            {
                if (HttpContext.Current.Session[CardTypeCodeNameStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[CardTypeCodeNameStr].ToString();
            }
            set { HttpContext.Current.Session[CardTypeCodeNameStr] = value; }
        }
        #endregion

        #region CardGradeCodeName
        public static string CardGradeCodeName
        {
            get
            {
                if (HttpContext.Current.Session[CardGradeCodeNameStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[CardGradeCodeNameStr].ToString();
            }
            set { HttpContext.Current.Session[CardGradeCodeNameStr] = value; }
        }
        #endregion

        #region HiddenFormInfo
        public static HiddenFormInfo HiddenFormInfo
        {
            get
            {
                if (HttpContext.Current.Session[HiddenFormInfoStr] == null)
                {
                    HiddenFormInfo model = new HiddenFormInfo();
                    model.MessageBoxIcon = MessageBoxIcon.Question;
                    model.Title = "System Info";
                    model.ExecSuccess = false;
                    HttpContext.Current.Session[HiddenFormInfoStr] = model;
                }
                return HttpContext.Current.Session[HiddenFormInfoStr] as HiddenFormInfo;
            }
            set { HttpContext.Current.Session[HiddenFormInfoStr] = value; }
        }
        #endregion

        #region CouponIssue
        public static CouponIssueController CouponIssueController
        {
            get
            {
                if (HttpContext.Current.Session[CouponIssueControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponIssueControllerStr] = new CouponIssueController();
                }
                return HttpContext.Current.Session[CouponIssueControllerStr] as CouponIssueController;
            }
            set { HttpContext.Current.Session[CouponIssueControllerStr] = value; }
        }
        #endregion

        #region CouponActive
        public static CouponActiveController CouponActiveController
        {
            get
            {
                if (HttpContext.Current.Session[CouponActiveControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponActiveControllerStr] = new CouponActiveController();
                }
                return HttpContext.Current.Session[CouponActiveControllerStr] as CouponActiveController;
            }
            set { HttpContext.Current.Session[CouponActiveControllerStr] = value; }
        }
        #endregion

        #region CouponRedeem
        public static CouponRedeemController CouponRedeemController
        {
            get
            {
                if (HttpContext.Current.Session[CouponRedeemControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponRedeemControllerStr] = new CouponRedeemController();
                }
                return HttpContext.Current.Session[CouponRedeemControllerStr] as CouponRedeemController;
            }
            set { HttpContext.Current.Session[CouponRedeemControllerStr] = value; }
        }
        #endregion

        #region CouponVoid
        public static CouponVoidController CouponVoidController
        {
            get
            {
                if (HttpContext.Current.Session[CouponVoidControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponVoidControllerStr] = new CouponVoidController();
                }
                return HttpContext.Current.Session[CouponVoidControllerStr] as CouponVoidController;
            }
            set { HttpContext.Current.Session[CouponVoidControllerStr] = value; }
        }
        #endregion

        #region ChangeExpiryDate
        public static ChangeExpiryDateController ChangeExpiryDateController
        {
            get
            {
                if (HttpContext.Current.Session[ChangeExpiryDateControllerStr] == null)
                {
                    HttpContext.Current.Session[ChangeExpiryDateControllerStr] = new ChangeExpiryDateController();
                }
                return HttpContext.Current.Session[ChangeExpiryDateControllerStr] as ChangeExpiryDateController;
            }
            set { HttpContext.Current.Session[ChangeExpiryDateControllerStr] = value; }
        }
        #endregion

        #region ChangeDenomination
        public static ChangeDenominationController ChangeDenominationController
        {
            get
            {
                if (HttpContext.Current.Session[ChangeDenominationControllerStr] == null)
                {
                    HttpContext.Current.Session[ChangeDenominationControllerStr] = new ChangeDenominationController();
                }
                return HttpContext.Current.Session[ChangeDenominationControllerStr] as ChangeDenominationController;
            }
            set { HttpContext.Current.Session[ChangeDenominationControllerStr] = value; }
        }
        #endregion

        #region ChangeStatus
        public static ChangeStatusController ChangeStatusController
        {
            get
            {
                if (HttpContext.Current.Session[ChangeStatusControllerStr] == null)
                {
                    HttpContext.Current.Session[ChangeStatusControllerStr] = new ChangeStatusController();
                }
                return HttpContext.Current.Session[ChangeStatusControllerStr] as ChangeStatusController;
            }
            set { HttpContext.Current.Session[ChangeStatusControllerStr] = value; }
        }
        #endregion

        #region Advertisements
        public static AdvertisementsAddController AdvertisementsAddController
        {
            get
            {
                if (HttpContext.Current.Session[AdvertisementsAddControllerStr] == null)
                {
                    HttpContext.Current.Session[AdvertisementsAddControllerStr] = new AdvertisementsAddController();
                }
                return HttpContext.Current.Session[AdvertisementsAddControllerStr] as AdvertisementsAddController;
            }
            set { HttpContext.Current.Session[AdvertisementsAddControllerStr] = value; }
        }
        public static AdvertisementsModifyController AdvertisementsModifyController
        {
            get
            {
                if (HttpContext.Current.Session[AdvertisementsModifyControllerStr] == null)
                {
                    HttpContext.Current.Session[AdvertisementsModifyControllerStr] = new AdvertisementsModifyController();
                }
                return HttpContext.Current.Session[AdvertisementsModifyControllerStr] as AdvertisementsModifyController;
            }
            set { HttpContext.Current.Session[AdvertisementsModifyControllerStr] = value; }
        }
        public static AdvertisementsDeleteController AdvertisementsDeleteController
        {
            get
            {
                if (HttpContext.Current.Session[AdvertisementsDeleteControllerStr] == null)
                {
                    HttpContext.Current.Session[AdvertisementsDeleteControllerStr] = new AdvertisementsDeleteController();
                }
                return HttpContext.Current.Session[AdvertisementsDeleteControllerStr] as AdvertisementsDeleteController;
            }
            set { HttpContext.Current.Session[AdvertisementsDeleteControllerStr] = value; }
        }
        #endregion

        #region USEFULEXPRESSIONS
        public static USEFULEXPRESSIONSAddController USEFULEXPRESSIONSAddController
        {
            get
            {
                if (HttpContext.Current.Session[USEFULEXPRESSIONSAddControllerStr] == null)
                {
                    HttpContext.Current.Session[USEFULEXPRESSIONSAddControllerStr] = new USEFULEXPRESSIONSAddController();
                }
                return HttpContext.Current.Session[USEFULEXPRESSIONSAddControllerStr] as USEFULEXPRESSIONSAddController;
            }
            set { HttpContext.Current.Session[USEFULEXPRESSIONSAddControllerStr] = value; }
        }
        public static USEFULEXPRESSIONSModifyController USEFULEXPRESSIONSModifyController
        {
            get
            {
                if (HttpContext.Current.Session[USEFULEXPRESSIONSModifyControllerStr] == null)
                {
                    HttpContext.Current.Session[USEFULEXPRESSIONSModifyControllerStr] = new USEFULEXPRESSIONSModifyController();
                }
                return HttpContext.Current.Session[USEFULEXPRESSIONSModifyControllerStr] as USEFULEXPRESSIONSModifyController;
            }
            set { HttpContext.Current.Session[USEFULEXPRESSIONSModifyControllerStr] = value; }
        }
        #endregion

        #region Currency
        public static CurrencyController CurrencyController
        {
            get
            {
                if (HttpContext.Current.Session[CurrencyControllerStr] == null)
                {
                    HttpContext.Current.Session[CurrencyControllerStr] = new CurrencyController();
                }
                return HttpContext.Current.Session[CurrencyControllerStr] as CurrencyController;
            }
            set { HttpContext.Current.Session[CurrencyControllerStr] = value; }
        }
        #endregion

        #region DistributeTemplate
        public static DistributeTemplateController DistributeTemplateController
        {
            get
            {
                if (HttpContext.Current.Session[DistributeTemplateControllerStr] == null)
                {
                    HttpContext.Current.Session[DistributeTemplateControllerStr] = new DistributeTemplateController();
                }
                return HttpContext.Current.Session[DistributeTemplateControllerStr] as DistributeTemplateController;
            }
            set { HttpContext.Current.Session[DistributeTemplateControllerStr] = value; }
        }
        #endregion

        #region PasswordRule
        public static PasswordRulesSettingController PasswordRulesSettingController
        {
            get
            {
                if (HttpContext.Current.Session[PasswordRulesSettingControllerStr] == null)
                {
                    HttpContext.Current.Session[PasswordRulesSettingControllerStr] = new PasswordRulesSettingController();
                }
                return HttpContext.Current.Session[PasswordRulesSettingControllerStr] as PasswordRulesSettingController;
            }
            set { HttpContext.Current.Session[PasswordRulesSettingControllerStr] = value; }
        }
        #endregion

        #region Reason
        public static ReasonController ReasonController
        {
            get
            {
                if (HttpContext.Current.Session[ReasonControllerStr] == null)
                {
                    HttpContext.Current.Session[ReasonControllerStr] = new ReasonController();
                }
                return HttpContext.Current.Session[ReasonControllerStr] as ReasonController;
            }
            set { HttpContext.Current.Session[ReasonControllerStr] = value; }
        }
        #endregion

        #region StoreType
        public static StoreNatureController StoreNatureController
        {
            get
            {
                if (HttpContext.Current.Session[StoreNatureControllerStr] == null)
                {
                    HttpContext.Current.Session[StoreNatureControllerStr] = new StoreNatureController();
                }
                return HttpContext.Current.Session[StoreNatureControllerStr] as StoreNatureController;
            }
            set { HttpContext.Current.Session[StoreNatureControllerStr] = value; }
        }
        #endregion

        #region Brand
        public static BrandController BrandController
        {
            get
            {
                if (HttpContext.Current.Session[BrandControllerStr] == null)
                {
                    HttpContext.Current.Session[BrandControllerStr] = new BrandController();
                }
                return HttpContext.Current.Session[BrandControllerStr] as BrandController;
            }
            set { HttpContext.Current.Session[BrandControllerStr] = value; }
        }
        #endregion

        #region CouponPush
        public static CouponPushController CouponPushController
        {
            get
            {
                if (HttpContext.Current.Session[CouponPushControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponPushControllerStr] = new CouponPushController();
                }
                return HttpContext.Current.Session[CouponPushControllerStr] as CouponPushController;
            }
            set { HttpContext.Current.Session[CouponPushControllerStr] = value; }
        }
        #endregion

        #region Store
        public static StoreController StoreController
        {
            get
            {
                if (HttpContext.Current.Session[StoreControllerStr] == null)
                {
                    HttpContext.Current.Session[StoreControllerStr] = new StoreController();
                }
                return HttpContext.Current.Session[StoreControllerStr] as StoreController;
            }
            set { HttpContext.Current.Session[StoreControllerStr] = value; }
        }
        #endregion

        #region ImportMemberCotroller
        public static ImportMemberCotroller ImportMemberCotroller
        {

            get
            {
                if (HttpContext.Current.Session[ImportMemberCotrollerStr] == null)
                {
                    HttpContext.Current.Session[ImportMemberCotrollerStr] = new ImportMemberCotroller();
                }
                return HttpContext.Current.Session[ImportMemberCotrollerStr] as ImportMemberCotroller;
            }
            set { HttpContext.Current.Session[ImportMemberCotrollerStr] = value; }
        }
        #endregion

        #region ImportStorePath
        public static string ImportStorePath
        {

            get
            {
                if (HttpContext.Current.Session[ImportStorePathStr] == null)
                {
                    HttpContext.Current.Session[ImportStorePathStr] = "";
                }
                return HttpContext.Current.Session[ImportStorePathStr].ToString();
            }
            set { HttpContext.Current.Session[ImportStorePathStr] = value; }
        }
        #endregion

        #region DepartmentsController
        public static DepartmentsController DepartmentsController
        {
            get
            {
                if (HttpContext.Current.Session[DepartmentsControllerStr] == null)
                {
                    HttpContext.Current.Session[DepartmentsControllerStr] = new DepartmentsController();
                }
                return HttpContext.Current.Session[DepartmentsControllerStr] as DepartmentsController;
            }
            set { HttpContext.Current.Session[DepartmentsControllerStr] = value; }
        }
        #endregion

        #region ChangeCardDenomination
        public static ChangeCardDenominationController ChangeCardDenominationController
        {
            get
            {
                if (HttpContext.Current.Session[ChangeCardDenominationControllerStr] == null)
                {
                    HttpContext.Current.Session[ChangeCardDenominationControllerStr] = new ChangeCardDenominationController();
                }
                return HttpContext.Current.Session[ChangeCardDenominationControllerStr] as ChangeCardDenominationController;
            }
            set { HttpContext.Current.Session[ChangeCardDenominationControllerStr] = value; }
        }
        #endregion

        #region ImportMemberInfos1Controller
        public static ImportMemberInfos1Controller ImportMemberInfos1Controller
        {

            get
            {
                if (HttpContext.Current.Session[ImportMemberInfos1ControllerStr] == null)
                {
                    HttpContext.Current.Session[ImportMemberInfos1ControllerStr] = new ImportMemberInfos1Controller();
                }
                return HttpContext.Current.Session[ImportMemberInfos1ControllerStr] as ImportMemberInfos1Controller;
            }
            set { HttpContext.Current.Session[ImportMemberInfos1ControllerStr] = value; }
        }
        #endregion

        #region SponsorController
        public static SponsorController SponsorController
        {

            get
            {
                if (HttpContext.Current.Session[SponsorControllerStr] == null)
                {
                    HttpContext.Current.Session[SponsorControllerStr] = new SponsorController();
                }
                return HttpContext.Current.Session[SponsorControllerStr] as SponsorController;
            }
            set { HttpContext.Current.Session[SponsorControllerStr] = value; }
        }
        #endregion

        #region SupplierController
        public static SupplierController SupplierController
        {

            get
            {
                if (HttpContext.Current.Session[SupplierControllerStr] == null)
                {
                    HttpContext.Current.Session[SupplierControllerStr] = new SupplierController();
                }
                return HttpContext.Current.Session[SupplierControllerStr] as SupplierController;
            }
            set { HttpContext.Current.Session[SupplierControllerStr] = value; }
        }
        #endregion

        #region CompanyController
        public static CompanyController CompanyController
        {

            get
            {
                if (HttpContext.Current.Session[CompanyControllerStr] == null)
                {
                    HttpContext.Current.Session[CompanyControllerStr] = new CompanyController();
                }
                return HttpContext.Current.Session[CompanyControllerStr] as CompanyController;
            }
            set { HttpContext.Current.Session[CompanyControllerStr] = value; }
        }
        #endregion

        #region PointRuleController
        public static PointRuleController PointRuleController
        {

            get
            {
                if (HttpContext.Current.Session[PointRuleControllerStr] == null)
                {
                    HttpContext.Current.Session[PointRuleControllerStr] = new PointRuleController();
                }
                return HttpContext.Current.Session[PointRuleControllerStr] as PointRuleController;
            }
            set { HttpContext.Current.Session[PointRuleControllerStr] = value; }
        }
        #endregion

        #region StoreGroupController
        public static StoreGroupController StoreGroupController
        {

            get
            {
                if (HttpContext.Current.Session[StoreGroupControllerStr] == null)
                {
                    HttpContext.Current.Session[StoreGroupControllerStr] = new StoreGroupController();
                }
                return HttpContext.Current.Session[StoreGroupControllerStr] as StoreGroupController;
            }
            set { HttpContext.Current.Session[StoreGroupControllerStr] = value; }
        }
        #endregion

        #region MemberOrderController
        public static MemberOrderController MemberOrderController
        {

            get
            {
                if (HttpContext.Current.Session[MemberOrderControllerStr] == null)
                {
                    HttpContext.Current.Session[MemberOrderControllerStr] = new MemberOrderController();
                }
                return HttpContext.Current.Session[MemberOrderControllerStr] as MemberOrderController;
            }
            set { HttpContext.Current.Session[MemberOrderControllerStr] = value; }
        }
        #endregion

        #region CardChangePointsController
        public static CardChangePointsController CardChangePointsController
        {

            get
            {
                if (HttpContext.Current.Session[CardChangePointsControllerStr] == null)
                {
                    HttpContext.Current.Session[CardChangePointsControllerStr] = new CardChangePointsController();
                }
                return HttpContext.Current.Session[CardChangePointsControllerStr] as CardChangePointsController;
            }
            set { HttpContext.Current.Session[CardChangePointsControllerStr] = value; }
        }
        #endregion

        #region CheckDBName
        public static string CheckDBName
        {
            get
            {
                if (HttpContext.Current.Session[CheckDBNameStr] == null)
                {
                    return "";
                }
                return HttpContext.Current.Session[CheckDBNameStr].ToString();
            }
            set { HttpContext.Current.Session[CheckDBNameStr] = value; }
        }
        #endregion

        #region BuyingDayFlagController
        public static BuyingDayFlagController BuyingDayFlagController
        {
            get
            {
                if (HttpContext.Current.Session[BuyingDayFlagControllerStr] == null)
                {
                    HttpContext.Current.Session[BuyingDayFlagControllerStr] = new BuyingDayFlagController();
                }
                return HttpContext.Current.Session[BuyingDayFlagControllerStr] as BuyingDayFlagController;
            }
            set { HttpContext.Current.Session[BuyingDayFlagControllerStr] = value; }
        }
        #endregion

        #region BuyingMonthFlagController
        public static BuyingMonthFlagController BuyingMonthFlagController
        {
            get
            {
                if (HttpContext.Current.Session[BuyingMonthFlagControllerStr] == null)
                {
                    HttpContext.Current.Session[BuyingMonthFlagControllerStr] = new BuyingMonthFlagController();
                }
                return HttpContext.Current.Session[BuyingMonthFlagControllerStr] as BuyingMonthFlagController;
            }
            set { HttpContext.Current.Session[BuyingMonthFlagControllerStr] = value; }
        }
        #endregion

        #region BuyingWeekFlagController
        public static BuyingWeekFlagController BuyingWeekFlagController
        {
            get
            {
                if (HttpContext.Current.Session[BuyingWeekFlagControllerStr] == null)
                {
                    HttpContext.Current.Session[BuyingWeekFlagControllerStr] = new BuyingWeekFlagController();
                }
                return HttpContext.Current.Session[BuyingWeekFlagControllerStr] as BuyingWeekFlagController;
            }
            set { HttpContext.Current.Session[BuyingWeekFlagControllerStr] = value; }
        }
        #endregion

        public static CouponReplenishRuleAddController CouponReplenishRuleAddController
        {
            get
            {
                if (HttpContext.Current.Session[CouponReplenishRuleAddControllerStr] == null)
                {
                    HttpContext.Current.Session[CouponReplenishRuleAddControllerStr] = new CouponReplenishRuleAddController();
                }
                return HttpContext.Current.Session[CouponReplenishRuleAddControllerStr] as CouponReplenishRuleAddController;
            }
            set { HttpContext.Current.Session[CouponReplenishRuleAddControllerStr] = value; }
        }

        public static InventoryReplenishRuleAddController InventoryReplenishRuleAddController
        {
            get
            {
                if (HttpContext.Current.Session[InventoryReplenishRuleAddControllerStr] == null)
                {
                    HttpContext.Current.Session[InventoryReplenishRuleAddControllerStr] = new InventoryReplenishRuleAddController();
                }
                return HttpContext.Current.Session[InventoryReplenishRuleAddControllerStr] as InventoryReplenishRuleAddController;
            }
            set { HttpContext.Current.Session[InventoryReplenishRuleAddControllerStr] = value; }
        }
        #region ChangeStatus Add by Aelx 2014-07-03
        public static CardChangeStatusController CardChangeStatusController
        {
            get
            {
                if (HttpContext.Current.Session[CardChangeStatusControllerStr] == null)
                {
                    HttpContext.Current.Session[CardChangeStatusControllerStr] = new CardChangeStatusController();
                }
                return HttpContext.Current.Session[CardChangeStatusControllerStr] as CardChangeStatusController;
            }
            set { HttpContext.Current.Session[CardChangeStatusControllerStr] = value; }
        }
        #endregion

        #region MemberClause  Add by Alex 2014-07-02 ++
        public static MemberClauseController MemberClauseController
        {
            get
            {
                if (HttpContext.Current.Session[MemberClauseControllerStr] == null)
                {
                    HttpContext.Current.Session[MemberClauseControllerStr] = new MemberClauseController();
                }
                return HttpContext.Current.Session[MemberClauseControllerStr] as MemberClauseController;
            }
            set { HttpContext.Current.Session[MemberClauseControllerStr] = value; }
        }
        #endregion
        #region ImportTenderPath  Add by Nathan 20140626
        public static string ImportTenderPath
        {

            get
            {
                if (HttpContext.Current.Session[ImportTenderPathStr] == null)
                {
                    HttpContext.Current.Session[ImportTenderPathStr] = "";
                }
                return HttpContext.Current.Session[ImportTenderPathStr].ToString();
            }
            set { HttpContext.Current.Session[ImportTenderPathStr] = value; }
        }
        #endregion

        #region CardIssue Add by Nathan 20140609
        public static CardIssueController CardIssueController
        {
            get
            {
                if (HttpContext.Current.Session[CardIssueControllerStr] == null)
                {
                    HttpContext.Current.Session[CardIssueControllerStr] = new CardIssueController();
                }
                return HttpContext.Current.Session[CardIssueControllerStr] as CardIssueController;
            }
            set { HttpContext.Current.Session[CardIssueControllerStr] = value; }
        }
        #endregion

        #region CardVoid Add by Alex 20140612
        public static CardVoidController CardVoidController
        {
            get
            {
                if (HttpContext.Current.Session[CardVoidControllerStr] == null)
                {
                    HttpContext.Current.Session[CardVoidControllerStr] = new CardVoidController();
                }
                return HttpContext.Current.Session[CardVoidControllerStr] as CardVoidController;
            }
            set { HttpContext.Current.Session[CardVoidControllerStr] = value; }
        }
        #endregion
        #region ChangeCardExpiryDate Add by Alex 2014-07-12
        public static ChangeCardExpiryDateController ChangeCardExpiryDateController
        {
            get
            {
                if (HttpContext.Current.Session[ChangeCardExpiryDateControllerStr] == null)
                {
                    HttpContext.Current.Session[ChangeCardExpiryDateControllerStr] = new ChangeCardExpiryDateController();
                }
                return HttpContext.Current.Session[ChangeCardExpiryDateControllerStr] as ChangeCardExpiryDateController;
            }
            set { HttpContext.Current.Session[ChangeCardExpiryDateControllerStr] = value; }
        }

        #endregion
        #region CardTopup Add by Alex 2014-07-12
        public static CardTopupController CardTopupController
        {
            get
            {
                if (HttpContext.Current.Session[CardTopupControllerStr] == null)
                {
                    HttpContext.Current.Session[CardTopupControllerStr] = new CardTopupController();
                }
                return HttpContext.Current.Session[CardTopupControllerStr] as CardTopupController;
            }
            set { HttpContext.Current.Session[CardTopupControllerStr] = value; }
        }
        #endregion
        #region CardActive Add by Nathan 20140611
        public static CardActiveController CardActiveController
        {
            get
            {
                if (HttpContext.Current.Session[CardActiveControllerStr] == null)
                {
                    HttpContext.Current.Session[CardActiveControllerStr] = new CardActiveController();
                }
                return HttpContext.Current.Session[CardActiveControllerStr] as CardActiveController;
            }
            set { HttpContext.Current.Session[CardActiveControllerStr] = value; }
        }
        #endregion


        #region CardRedeem Add by Nathan 20140612
        public static CardRedeemController CardRedeemController
        {
            get
            {
                if (HttpContext.Current.Session[CardRedeemControllerStr] == null)
                {
                    HttpContext.Current.Session[CardRedeemControllerStr] = new CardRedeemController();
                }
                return HttpContext.Current.Session[CardRedeemControllerStr] as CardRedeemController;
            }
            set { HttpContext.Current.Session[CardRedeemControllerStr] = value; }
        }
        #endregion


        #region TenderController Add by Nathan 20140626
        public static TenderController TenderController
        {
            get
            {
                if (HttpContext.Current.Session[TenderControllerStr] == null)
                {
                    HttpContext.Current.Session[TenderControllerStr] = new TenderController();
                }
                return HttpContext.Current.Session[TenderControllerStr] as TenderController;
            }
            set { HttpContext.Current.Session[TenderControllerStr] = value; }
        }
        #endregion

        #region Promotion BuyingNewPromotionController Add by Frank 20140721
        public static BuyingNewPromotionController BuyingNewPromotionController
        {
            get
            {
                if (HttpContext.Current.Session[BuyingNewPromotionControllerStr] == null)
                {
                    HttpContext.Current.Session[BuyingNewPromotionControllerStr] = new BuyingNewPromotionController();
                }
                return HttpContext.Current.Session[BuyingNewPromotionControllerStr] as BuyingNewPromotionController;
            }
            set { HttpContext.Current.Session[BuyingNewPromotionControllerStr] = value; }
        }
        #endregion
        #region BuyingProdCode
        public static string BuyingProdCode
        {

            get
            {
                if (HttpContext.Current.Session[BuyingProdCodeStr] == null)
                {
                    HttpContext.Current.Session[BuyingProdCodeStr] = "";
                }
                return HttpContext.Current.Session[BuyingProdCodeStr].ToString();
            }
            set { HttpContext.Current.Session[BuyingProdCodeStr] = value; }
        }
        #endregion
        #region BuyingDepartCode
        public static string BuyingDepartCode
        {

            get
            {
                if (HttpContext.Current.Session[BuyingDepartCodeStr] == null)
                {
                    HttpContext.Current.Session[BuyingDepartCodeStr] = "";
                }
                return HttpContext.Current.Session[BuyingDepartCodeStr].ToString();
            }
            set { HttpContext.Current.Session[BuyingDepartCodeStr] = value; }
        }
        #endregion
        
        #region BuyingTendCode
        public static string BuyingTendCode
        {

            get
            {
                if (HttpContext.Current.Session[BuyingTendCodeStr] == null)
                {
                    HttpContext.Current.Session[BuyingTendCodeStr] = "";
                }
                return HttpContext.Current.Session[BuyingTendCodeStr].ToString();
            }
            set { HttpContext.Current.Session[BuyingTendCodeStr] = value; }
        }
        #endregion
        #region BuyingCouponTypeCode
        public static string BuyingCouponTypeCode
        {

            get
            {
                if (HttpContext.Current.Session[BuyingCouponTypeCodeStr] == null)
                {
                    HttpContext.Current.Session[BuyingCouponTypeCodeStr] = "";
                }
                return HttpContext.Current.Session[BuyingCouponTypeCodeStr].ToString();
            }
            set { HttpContext.Current.Session[BuyingCouponTypeCodeStr] = value; }
        }
        #endregion

        #region BuingProdDesc
        public static string BuyingProdDesc
        {

            get
            {
                if (HttpContext.Current.Session[BuyingProdDescStr] == null)
                {
                    HttpContext.Current.Session[BuyingProdDescStr] = "";
                }
                return HttpContext.Current.Session[BuyingProdDescStr].ToString();
            }
            set { HttpContext.Current.Session[BuyingProdDescStr] = value; }
        }
        #endregion

        #region BuyingProdCode
        public static object TempObject
        {
            get
            {
                return HttpContext.Current.Session[TempObjectStr];
            }
            set { HttpContext.Current.Session[TempObjectStr] = value; }
        }
        #endregion
        
        #region ChangeStatus Add by frank 2014-10-22
        public static CardTransferController CardTransferController
        {
            get
            {
                if (HttpContext.Current.Session[CardTransferControllerStr] == null)
                {
                    HttpContext.Current.Session[CardTransferControllerStr] = new CardTransferController();
                }
                return HttpContext.Current.Session[CardTransferControllerStr] as CardTransferController;
            }
            set { HttpContext.Current.Session[CardTransferControllerStr] = value; }
        }
        #endregion

        #region TelcoOrder Add by Gavin 2015-02-03
        public static TelcoOrderController TelcoOrderController
        {
            get
            {
                if (HttpContext.Current.Session[TelcoOrderControllerStr] == null)
                {
                    HttpContext.Current.Session[TelcoOrderControllerStr] = new TelcoOrderController();
                }
                return HttpContext.Current.Session[TelcoOrderControllerStr] as TelcoOrderController;
            }
            set { HttpContext.Current.Session[TelcoOrderControllerStr] = value; }
        }
        #endregion

        #region Telco Store Wallet Transfer  Add by Gavin 2015-02-16
        public static TelcoStoreWalletTransferController TelcoStoreWalletTransferController 
        {
            get
            {
                if (HttpContext.Current.Session[TelcoStoreWalletTransferControllerStr] == null)
                {
                    HttpContext.Current.Session[TelcoStoreWalletTransferControllerStr] = new TelcoStoreWalletTransferController();
                }
                return HttpContext.Current.Session[TelcoStoreWalletTransferControllerStr] as TelcoStoreWalletTransferController;
            }
            set { HttpContext.Current.Session[TelcoStoreWalletTransferControllerStr] = value; }
        }
        #endregion

        #region AirloadMonitoringRuleController  Add by Gavin 2015-02-26
        public static AirloadMonitoringRuleController AirloadMonitoringRuleController
        {
            get
            {
                if (HttpContext.Current.Session[AirloadMonitoringRuleControllerStr] == null)
                {
                    HttpContext.Current.Session[AirloadMonitoringRuleControllerStr] = new AirloadMonitoringRuleController();
                }
                return HttpContext.Current.Session[AirloadMonitoringRuleControllerStr] as AirloadMonitoringRuleController;
            }
            set { HttpContext.Current.Session[AirloadMonitoringRuleControllerStr] = value; }
        }
        #endregion

        #region WalletBalanceAdjustment  Add by Gavin 2015-02-27
        public static WalletBalanceAdjustmentController WalletBalanceAdjustmentController
        {
            get
            {
                if (HttpContext.Current.Session[WalletBalanceAdjustmentControllerStr] == null)
                {
                    HttpContext.Current.Session[WalletBalanceAdjustmentControllerStr] = new WalletBalanceAdjustmentController();
                }
                return HttpContext.Current.Session[WalletBalanceAdjustmentControllerStr] as WalletBalanceAdjustmentController;
            }
            set { HttpContext.Current.Session[WalletBalanceAdjustmentControllerStr] = value; }
        }
        #endregion

        #region Mobile Top Up  Add by Gavin 2015-03-02
        public static MobileTopUpController MobileTopUpController
        {
            get
            {
                if (HttpContext.Current.Session[MobileTopUpControllerStr] == null)
                {
                    HttpContext.Current.Session[MobileTopUpControllerStr] = new MobileTopUpController();
                }
                return HttpContext.Current.Session[MobileTopUpControllerStr] as MobileTopUpController;
            }
            set { HttpContext.Current.Session[MobileTopUpControllerStr] = value; }
        }
        #endregion

        #region Offline Top Up  Add by Gavin 2015-03-02
        public static OfflineTopUpController OfflineTopUpController
        {
            get
            {
                if (HttpContext.Current.Session[MobileTopUpControllerStr] == null)
                {
                    HttpContext.Current.Session[MobileTopUpControllerStr] = new OfflineTopUpController();
                }
                return HttpContext.Current.Session[MobileTopUpControllerStr] as OfflineTopUpController;
            }
            set { HttpContext.Current.Session[MobileTopUpControllerStr] = value; }
        }
        #endregion

        #region Gross Margin Add by Gavin 2015-03-03
        public static GrossMarginController GrossMarginController
        {
            get
            {
                if (HttpContext.Current.Session[GrossMarginControllerStr] == null)
                {
                    HttpContext.Current.Session[GrossMarginControllerStr] = new GrossMarginController();
                }
                return HttpContext.Current.Session[GrossMarginControllerStr] as GrossMarginController;
            }
            set { HttpContext.Current.Session[GrossMarginControllerStr] = value; }
        }
        #endregion

        #region Telco Return Order  Add by Gavin 2015-03-06
        public static TelcoReturnOrderController TelcoReturnOrderController
        {
            get
            {
                if (HttpContext.Current.Session[TelcoReturnOrderControllerStr] == null)
                {
                    HttpContext.Current.Session[TelcoReturnOrderControllerStr] = new TelcoReturnOrderController();
                }
                return HttpContext.Current.Session[TelcoReturnOrderControllerStr] as TelcoReturnOrderController;
            }
            set { HttpContext.Current.Session[TelcoReturnOrderControllerStr] = value; }
        }
        #endregion        

        #region Telco Wallet Rule  Add by Gavin 2015-02-16
        public static WalletRuleController WalletRuleController
        {
            get
            {
                if (HttpContext.Current.Session[WalletRuleControllerStr] == null)
                {
                    HttpContext.Current.Session[WalletRuleControllerStr] = new WalletRuleController();
                }
                return HttpContext.Current.Session[WalletRuleControllerStr] as WalletRuleController;
            }
            set { HttpContext.Current.Session[WalletRuleControllerStr] = value; }
        }
        #endregion

        #region ImportCouponRedeemPath  Add by GAvin 2015-04-01
        public static string ImportCouponRedeemPath
        {

            get
            {
                if (HttpContext.Current.Session[ImportCouponRedeemPathStr] == null)
                {
                    HttpContext.Current.Session[ImportCouponRedeemPathStr] = "";
                }
                return HttpContext.Current.Session[ImportCouponRedeemPathStr].ToString();
            }
            set { HttpContext.Current.Session[ImportCouponRedeemPathStr] = value; }
        }
        #endregion


        #region CSVXMLBinding
        public static CSVXMLBindingController CSVXMLBindingController
        {
            get
            {
                if (HttpContext.Current.Session[CSVXMLBindingControllerStr] == null)
                {
                    HttpContext.Current.Session[CSVXMLBindingControllerStr] = new CSVXMLBindingController();
                }
                return HttpContext.Current.Session[CSVXMLBindingControllerStr] as CSVXMLBindingController;
            }
            set { HttpContext.Current.Session[CSVXMLBindingControllerStr] = value; }
        }
        #endregion

        #region CSVXMLMointoringRule
        public static CSVXMLMointoringRuleController CSVXMLMointoringRuleController
        {
            get
            {
                if (HttpContext.Current.Session[CSVXMLMointoringRuleControllerStr] == null)
                {
                    HttpContext.Current.Session[CSVXMLMointoringRuleControllerStr] = new CSVXMLMointoringRuleController();
                }
                return HttpContext.Current.Session[CSVXMLMointoringRuleControllerStr] as CSVXMLMointoringRuleController;
            }
            set { HttpContext.Current.Session[CSVXMLMointoringRuleControllerStr] = value; }
        }
        #endregion
    }
}