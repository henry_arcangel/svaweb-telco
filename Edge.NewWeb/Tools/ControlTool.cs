﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Utils.Tools;
using System.Text;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain;
using System.Data.SqlClient;

using System.Configuration;

namespace Edge.Web.Tools
{
    /// <summary>
    /// 封装控件相关操作
    /// </summary>
    public class ControlTool
    {

        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {

                    ListItem li = new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = dr[key].ToString().Trim() };
                    li.Attributes["title"] = dr[key].ToString().Trim();
                    ddl.Items.Add(li);
                }
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }



        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(FineUI.DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {

                    FineUI.ListItem li = new FineUI.ListItem() { Value = dr[keyValue].ToString().Trim(), Text = dr[key].ToString().Trim() };
                    //li.Attributes["title"] = dr[key].ToString().Trim();
                    ddl.Items.Add(li);
                }
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });//Add by Nathan for All dropdownlist default value.
        }

        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(), dr[code].ToString().Trim());
                    ListItem li = new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = text };
                    li.Attributes.Add("title", text);

                    ddl.Items.Add(li);
                }
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }


        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(FineUI.DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(), dr[code].ToString().Trim());
                    FineUI.ListItem li = new FineUI.ListItem() { Value = dr[keyValue].ToString().Trim(), Text = text };
                    //li.Attributes.Add("title", text);
                    ddl.Items.Add(li);
                }
            }

            //if (!ddl.Required)
            //{
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });//Add by Nathan for All dropdownlist default value.
            //}
        }

        public static string GetDropdownListText(string text, string code)
        {
            return string.Format("{0} - {1}", code, text);
        }

        public static void AddTitle(DropDownList ddl)
        {
            if (ddl == null) return;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                ddl.Items[i].Attributes.Add("title", ddl.Items[i].Text);
            }
        }


        public static void BindDictionaryValueKey(DropDownList ddl, Dictionary<string, string> source)
        {
            ddl.Items.Clear();
            foreach (KeyValuePair<string, string> keyValue in source)
            {
                ListItem li = new ListItem() { Value = keyValue.Value, Text = keyValue.Key };
                li.Attributes["title"] = keyValue.Key;
                ddl.Items.Add(li);
            }

            ddl.Items.Insert(0, new ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        public static void BindDictionaryValueKey(FineUI.DropDownList ddl, Dictionary<string, string> source)
        {
            ddl.Items.Clear();
            foreach (KeyValuePair<string, string> keyValue in source)
            {
                FineUI.ListItem li = new FineUI.ListItem() { Value = keyValue.Value, Text = keyValue.Key };
                // li.Attributes["title"] = keyValue.Key;
                ddl.Items.Add(li);
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }
        /// <summary>
        /// 根据当前语言绑定数据源到CheckBoxList
        /// </summary>
        /// <param name="ddl">要绑定的CheckBoxList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">CheckBoxList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(CheckBoxList ckbList, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            ckbList.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    ckbList.Items.Add(new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = dr[key].ToString().Trim() });
                }
            }
        }

        public static void BindDataSet(FineUI.CheckBoxList ckbList, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            ckbList.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    ckbList.Items.Add(new FineUI.CheckItem() { Value = dr[keyValue].ToString().Trim(), Text = dr[key].ToString().Trim() });
                }
            }
        }

        public static void BindDataSetCheckboxList(CheckBoxList cbl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            if (source == null || source.Tables == null || source.Tables.Count <= 0) return;
            cbl.Items.Clear();
            string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
            cbl.DataSource = source.Tables[0].DefaultView;
            cbl.DataTextField = key;
            cbl.DataValueField = keyValue;
            cbl.DataBind();
        }

        public static void BindDataSetCheckboxList(CheckBoxList cbl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, List<string> keyValueList)
        {
            if (source == null || source.Tables == null || source.Tables.Count <= 0) return;
            cbl.Items.Clear();

            string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

            foreach (DataRow dr in source.Tables[0].Rows)
            {
                dr["BrandName1"] = dr["BrandCode"] + "-" + dr["BrandName1"];
                dr["BrandName2"] = dr["BrandCode"] + "-" + dr["BrandName2"];
                dr["BrandName3"] = dr["BrandCode"] + "-" + dr["BrandName3"];
            }

            cbl.DataSource = source.Tables[0].DefaultView;
            cbl.DataTextField = key;
            cbl.DataValueField = keyValue;
            cbl.DataBind();

            foreach (ListItem item in cbl.Items)
            {
                if (keyValueList.Exists(i => i.Equals(item.Value)))
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }
        public static void BindDataSetCheckboxList(FineUI.CheckBoxList cbl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, List<string> keyValueList)
        {
            if (source == null || source.Tables == null || source.Tables.Count <= 0) return;
            cbl.Items.Clear();

            string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

            foreach (DataRow dr in source.Tables[0].Rows)
            {
                dr["BrandName1"] = dr["BrandCode"] + "-" + dr["BrandName1"];
                dr["BrandName2"] = dr["BrandCode"] + "-" + dr["BrandName2"];
                dr["BrandName3"] = dr["BrandCode"] + "-" + dr["BrandName3"];
            }

            cbl.DataSource = source.Tables[0].DefaultView;
            cbl.DataTextField = key;
            cbl.DataValueField = keyValue;
            cbl.DataBind();

            foreach (FineUI.CheckItem item in cbl.Items)
            {
                if (keyValueList.Exists(i => i.Equals(item.Value)))
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }

        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardIssuer(DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardIssuer().GetAllList();
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardIssuerID", "CardIssuerName1", "CardIssuerName2", "CardIssuerName3");
        }

        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardIssuer(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardIssuer().GetAllList();
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardIssuerID", "CardIssuerName1", "CardIssuerName2", "CardIssuerName3");
        }


        /// <summary>
        /// 绑定优惠券状态
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponStatus(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CouponController.CouponStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new FineUI.ListItem() { Text = DALTool.GetCouponTypeStatusName((int)value), Value = ((int)value).ToString() });
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });

        }

        /// <summary>
        /// 绑定优惠券库存类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponStockStatus(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CouponController.CouponStockStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new FineUI.ListItem() { Text = DALTool.GetCouponStockStatusName((int)value), Value = ((int)value).ToString() });
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });

        }

        /// <summary>
        /// 绑定卡库存类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardStockStatus(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CardController.CardStockStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new FineUI.ListItem() { Text = DALTool.GetCardStockStatusName((int)value), Value = ((int)value).ToString() });
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });

        }

        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponStatus(DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CouponController.CouponStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new ListItem() { Text = DALTool.GetCouponTypeStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });

        }


        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardStatus(DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CardController.CardStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new ListItem() { Text = DALTool.GetCardStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });

        }

        public static void BindCardStatus(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CardController.CardStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new FineUI.ListItem() { Text = DALTool.GetCardStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });

        }

        /// <summary>
        /// 绑定所有品牌
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindBrand(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Brand().GetList("1 = 1 order by BrandCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3", "BrandCode");
        }

        /// <summary>
        /// 绑定主档Store中的品牌
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindBrandOfMasterStore(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Brand().GetList("");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3", "BrandCode");
        }
        /// <summary>
        /// 绑定所有品牌
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindBrand(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = SVASessionInfo.CurrentUser.BrandInfoList;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }
        /// <summary>
        /// 绑定所有CoouponNature 优惠券级别 //Add By Robin 2015-01-05
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindCouponNature(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponNature().GetList("");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponNatureID", "CouponNatureName1", "CouponNatureName2", "CouponNatureName3", "CouponNatureCode");
        }

        /// <summary>
        /// 绑定所有CoouponNature 优惠券级别 //Add By Robin 2015-01-05
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindCouponNature(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponNature().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponNatureID", "CouponNatureName1", "CouponNatureName2", "CouponNatureName3", "CouponNatureCode");
        }
        public static void BindBrandInfoList(FineUI.DropDownList ddl, List<BrandInfo> list)
        {
            ddl.Items.Clear();
            ddl.DataSource = list;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }
        public static void BindKeyValueList(FineUI.DropDownList ddl, List<KeyValue> list)
        {
            ddl.Items.Clear();
            ddl.DataSource = list;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }
        public static void BindKeyValueList(FineUI.DropDownList ddl, List<KeyValue> list, bool required)
        {
            ddl.Items.Clear();
            ddl.DataSource = list;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            if (required)
            {
                ddl.ShowRedStar = true;
                ddl.Required = true;
                ddl.CompareType = FineUI.CompareType.String;
                ddl.CompareValue = "-1";
                ddl.CompareOperator = FineUI.Operator.NotEqual;
                ddl.CompareMessage = "请选择有效值";
            }
        }
        public static void BindKeyValueListNoEmpty(FineUI.DropDownList ddl, List<KeyValue> list)
        {
            ddl.Items.Clear();
            ddl.DataSource = list;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
        }
        internal static void AddColumnValue(DataSet ds, string refKey, ConstInfosRepostory.InfoType infoType)
        {
            int id = 0;
            string name = refKey + "_Name";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = ConstInfosRepostory.Singleton.GetKeyValueDesc(id.ToString(), SVASessionInfo.SiteLanguage, infoType);
            }
        }

        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardType(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("1 = 1 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        /// <summary>
        ///ADDed by jay 
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardTypeCode(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("1 = 1 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeCode", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }
        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList("1 = 1 order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }

        /// <summary>
        /// 绑定所有行业
        /// </summary>
        /// <param name="ddl">行业DropdownList</param>
        public static void BindIndustry(DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Industry().GetList("1 = 1 order by IndustryCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3", "IndustryCode");
        }

        /// <summary>
        /// 绑定所有行业
        /// </summary>
        /// <param name="ddl">行业DropdownList</param>
        public static void BindIndustry(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Industry().GetList("1 = 1 order by IndustryCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3", "IndustryCode");
        }

        /// <summary>
        /// 绑定所有币种
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCurrency(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Currency().GetList("1 = 1 order by CurrencyCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CurrencyID", "CurrencyName1", "CurrencyName2", "CurrencyName3", "CurrencyCode");
        }

        public static void BindCurrency(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Currency().GetList("1 = 1 order by CurrencyCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CurrencyID", "CurrencyName1", "CurrencyName2", "CurrencyName3", "CurrencyCode");
        }

        /// <summary>
        /// 绑定所有活动
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCampaign(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList("1 = 1 order by CampaignCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }

        /// <summary>
        /// 绑定所有活动
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCampaign(FineUI.DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList(string.Format(" BrandID = {0} order by CampaignCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }

        public static void BindCampaign(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList("1 = 1 order by CampaignCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }

        /// <summary>
        /// 绑定所有活动
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCampaign(DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList(string.Format(" BrandID = {0} order by CampaignCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }
        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("1 = 1 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }


        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardTypeForManual(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("IsImportUIDNumber = 0 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardTypeForManual(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("IsImportUIDNumber = 0 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList("1 = 1 order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }


        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(DropDownList ddl, int cardTypeID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(string.Format("CardTypeID={0} order by CardGradeCode", cardTypeID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }

        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(FineUI.DropDownList ddl, int cardTypeID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(string.Format("CardTypeID={0} order by CardGradeCode", cardTypeID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }
        /// <summary>
        /// Added By jay Return cardgradECode : CardGradeCode
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="cardTypeID"></param>
        public static void BindCardGradeCode(FineUI.DropDownList ddl, string CardtypeCode)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetListView(string.Format("CardtypeCode='{0}' order by CardGradeCode", CardtypeCode));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeCode", "CardGradeName1", "CardGradeName1", "", "CardGradeCode");
        }
        public static void BindCardGradeCode(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetListView("1=1 order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeCode", "CardGradeName1", "CardGradeName1", "", "CardGradeCode");
        }
        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList("1 = 1 order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }


        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList("1 = 1 order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }
        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponTypeWithoutBrand(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetListWithoutBrand(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }
        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(string.Format(" BrandID = {0} order by CouponTypeCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(FineUI.DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(string.Format(" BrandID = {0} order by CouponTypeCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有联系方式类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindSNSType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.SNSType().GetAllList();
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "SNSTypeID", "SNSTypeName1", "SNSTypeName2", "SNSTypeName3");
        }

        /// <summary>
        /// 绑定店铺类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindStoreType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.StoreType().GetList("1 = 1 order by StoreTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreTypeID", "StoreTypeName1", "StoreTypeName2", "StoreTypeName3", "StoreTypeCode");
        }

        /// <summary>
        /// 绑定店铺类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindStoreType(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.StoreType().GetList("1 = 1 order by StoreTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreTypeID", "StoreTypeName1", "StoreTypeName2", "StoreTypeName3", "StoreTypeCode");
        }

        public static void BindStoreGroup(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.StoreGroup().GetList("1 = 1 order by StoreGroupCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreGroupID", "StoreGroupName1", "StoreGroupName2", "StoreGroupName3", "StoreGroupCode");
        }

        /// <summary>
        /// 绑定原因
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindReasonType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Reason().GetList("1 = 1 order by ReasonCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "ReasonID", "ReasonDesc1", "ReasonDesc2", "ReasonDesc3", "ReasonCode");
        }

        /// <summary>
        /// 绑定原因
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindReasonType(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Reason().GetList("1 = 1 order by ReasonCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "ReasonID", "ReasonDesc1", "ReasonDesc2", "ReasonDesc3", "ReasonCode");
        }


        /// <summary>
        /// 绑定语言列表
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindLanguageMap(DropDownList ddl)
        {
            DataSet ds = new Edge.SVA.BLL.LanguageMap().GetAllList();
            ddl.Items.Clear();

            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ddl.DataTextField = "LanguageDesc";
                ddl.DataValueField = "KeyID";
                ddl.DataSource = ds;
                ddl.DataBind();
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }



        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList("1 = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺（店铺）
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(FineUI.DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" StoreTypeID = 2 and BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺（总部）
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(FineUI.DropDownList dll, string storeTypeID = "1")
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(" StoreTypeID = " + storeTypeID + " order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreCodeWithBrand(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            //Add By Robin 2014-07-31 过滤用户可用店铺列表
            string strWhere = "";
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(stores))
            {
                strWhere = " 1!=1 and ";
            }
            else
            {
                strWhere = " storeid in " + stores + " and ";
            }
            //End
            DataSet ds = new Edge.SVA.BLL.Store().GetList(strWhere + string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreCodeWithBrand(FineUI.DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithBrand(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }
        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindAllStoreWithBrand(FineUI.DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }
        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithBrand(FineUI.DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            //DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            //Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
            BrandInfo bi = SVASessionInfo.CurrentUser.BrandInfoList.Find(m => m.Key == brandID.ToString());
            if (bi != null)
            {

                ddl.DataSource = bi.StoreInfos;
                ddl.DataTextField = "Value";
                ddl.DataValueField = "Key";
                ddl.DataBind();
            }
            //if (ddl.Items.Count == 1 && ddl.Required)
            //{

            //}
            //else
            //{
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            //}
        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(CheckBoxList ckbList, int storeTypeID, int brandID, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (storeTypeID > 0)
            {
                sbWhere.Append(string.Format(" StoreTypeID = {0}", storeTypeID));
            }

            if (brandID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
                }
                else
                {
                    sbWhere.Append(string.Format("  BrandID = {0}", brandID));
                }
            }

            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Store().GetList(top, sbWhere.ToString(), "StoreCode");
            }
            else
            {

                ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());
            }

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3");

        }

        public static void BindStore(FineUI.CheckBoxList ckbList, int storeTypeID, int brandID, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (storeTypeID > 0)
            {
                sbWhere.Append(string.Format(" StoreTypeID = {0}", storeTypeID));
            }

            if (brandID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
                }
                else
                {
                    sbWhere.Append(string.Format("  BrandID = {0}", brandID));
                }
            }

            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Store().GetList(top, sbWhere.ToString(), "StoreCode");
            }
            else
            {

                ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());
            }

            //Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3");

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        /// <summary>
        /// FineUI.CheckBoxList
        /// </summary>
        /// <param name="ddl">FineUI.CheckBoxList</param>
        public static void BindStore(FineUI.CheckBoxList ckbList, int storeTypeID, int brandID, string storeCode, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" 1=1 ");
            if (storeTypeID > 0)
            {
                sbWhere.Append(string.Format(" and StoreTypeID = {0}", storeTypeID));
            }
            if (brandID > 0)
            {
                sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
            }
            if (!string.IsNullOrEmpty(name))
            {
                sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
            }
            if (!string.IsNullOrEmpty(storeCode))
            {
                sbWhere.Append(string.Format(" and StoreCode = '{0}'", storeCode));
            }

            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Store().GetList(top, sbWhere.ToString(), "StoreCode");
            }
            else
            {

                ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());
            }

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        public static void BindStore(FineUI.CheckBoxList ckbList, int brandID, int storeTypeID, int storeGroupID, string name)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (brandID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
                }
                else
                {
                    sbWhere.Append(string.Format("  BrandID = {0}", brandID));
                }
            }

            if (storeTypeID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreTypeID = {0}", storeTypeID));
                }
                else
                {
                    sbWhere.Append(string.Format(" StoreTypeID = {0}", storeTypeID));
                }
            }

            if (storeGroupID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreGroupID = {0}", storeGroupID));
                }
                else
                {
                    sbWhere.Append(string.Format(" StoreGroupID = {0}", storeGroupID));
                }
            }

            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();

            ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        public static void BindStore(FineUI.RadioButtonList rblList, int brandID, int storeTypeID, int storeGroupID, string name)
        {
            rblList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (brandID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
                }
                else
                {
                    sbWhere.Append(string.Format("  BrandID = {0}", brandID));
                }
            }

            if (storeTypeID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreTypeID = {0}", storeTypeID));
                }
                else
                {
                    sbWhere.Append(string.Format(" StoreTypeID = {0}", storeTypeID));
                }
            }

            if (storeGroupID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreGroupID = {0}", storeGroupID));
                }
                else
                {
                    sbWhere.Append(string.Format(" StoreGroupID = {0}", storeGroupID));
                }
            }

            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    //sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                    sbWhere.Append(string.Format(" and (StoreName1 like '{0}%' or StoreName2 like '{0}%' or StoreName3 like '{0}%')", name));
                }
                else
                {
                    //sbWhere.Append(string.Format(" (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
                    sbWhere.Append(string.Format(" (StoreName1 like '{0}%' or StoreName2 like '{0}%' or StoreName3 like '{0}%')", name));
                }
            }
            DataSet ds = new DataSet();

            ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());

            Edge.Web.Tools.ControlTool.BindDataSet(rblList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        public static void BindStore(FineUI.CheckBoxList ckbList, int storeID)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (storeID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreID = {0}", storeID));
                }
                else
                {
                    sbWhere.Append(string.Format("  StoreID = {0}", storeID));
                }
            }

            DataSet ds = new DataSet();

            ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        public static void BindStore(FineUI.RadioButtonList rblList, int storeID)
        {
            rblList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (storeID > 0)
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and StoreID = {0}", storeID));
                }
                else
                {
                    sbWhere.Append(string.Format("  StoreID = {0}", storeID));
                }
            }

            DataSet ds = new DataSet();

            ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());

            Edge.Web.Tools.ControlTool.BindDataSet(rblList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }
        /// <summary>
        /// 根据当前语言绑定数据源到CheckBoxList FineUI
        /// </summary>
        /// <param name="ddl">要绑定的CheckBoxList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">CheckBoxList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(FineUI.CheckBoxList ckbList, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            ckbList.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(), dr[code].ToString().Trim());
                    ckbList.Items.Add(new FineUI.CheckItem() { Value = dr[keyValue].ToString().Trim(), Text = text });
                }
            }
        }

        /// <summary>
        /// 根据当前语言绑定数据源到FineUI.RadioButtonList
        /// </summary>
        /// <param name="ddl">要绑定的RadioButtonList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">RadioButtonList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(FineUI.RadioButtonList rbList, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            rbList.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(), dr[code].ToString().Trim());
                    rbList.Items.Add(new FineUI.RadioItem() { Value = dr[keyValue].ToString().Trim(), Text = text });
                }
            }
        }

        /// <summary>
        /// 绑定所有品牌
        /// </summary>
        /// <param name="ddl">品牌CheckBoxList</param>
        public static void BindBrand(CheckBoxList ckbList, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (BrandName1 like '%{0}%' or BrandName2 like '%{0}%' or BrandName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (BrandName1 like '%{0}%' or BrandName2 like '%{0}%' or BrandName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Brand().GetList(top, sbWhere.ToString(), "BrandID");
            }
            else
            {

                ds = new Edge.SVA.BLL.Brand().GetList(sbWhere.ToString());
            }
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3");

        }

        /// <summary>
        /// 绑定所有CouponTypeNature 优惠券类型级别
        /// </summary>
        /// <param name="ddl">品牌CheckBoxList</param>
        public static void BindCouponNature(CheckBoxList ckbList, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (CouponNatureName1 like '%{0}%' or CouponNatureName2 like '%{0}%' or CouponNatureName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (CouponNatureName1 like '%{0}%' or CouponNatureName2 like '%{0}%' or CouponNatureName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Brand().GetList(top, sbWhere.ToString(), "CouponNatureID");
            }
            else
            {

                ds = new Edge.SVA.BLL.Brand().GetList(sbWhere.ToString());
            }
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "CouponNatureID", "CouponNatureName1", "CouponNatureName2", "CouponNatureName3");

        }

        public static void BindBrand(FineUI.CheckBoxList ckbList, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (BrandName1 like '%{0}%' or BrandName2 like '%{0}%' or BrandName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (BrandName1 like '%{0}%' or BrandName2 like '%{0}%' or BrandName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Brand().GetList(top, sbWhere.ToString(), "BrandID");
            }
            else
            {

                ds = new Edge.SVA.BLL.Brand().GetList(sbWhere.ToString());
            }

            //Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3");
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3", "BrandCode");
        }
        /// <summary>
        /// 绑定所有区域
        /// </summary>
        /// <param name="ddl">区域CheckBoxList</param>
        public static void BindLocation(CheckBoxList ckbList, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (StoreGroupName1 like '%{0}%' or StoreGroupName2 like '%{0}%' or StoreGroupName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (StoreGroupName1 like '%{0}%' or StoreGroupName2 like '%{0}%' or StoreGroupName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.StoreGroup().GetList(top, sbWhere.ToString(), "StoreGroupID");
            }
            else
            {

                ds = new Edge.SVA.BLL.StoreGroup().GetList(sbWhere.ToString());
            }
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreGroupID", "StoreGroupName1", "StoreGroupName2", "StoreGroupName3");
        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithStoreCode(DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList("1 = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithStoreCode(FineUI.DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList("1 = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        public static void BindPasswordRule(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("1 = 1 order by PasswordRuleCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "PasswordRuleID", "Name1", "Name2", "Name3", "PasswordRuleCode");
        }
        public static void BindPasswordRule(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("1 = 1 order by PasswordRuleCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "PasswordRuleID", "Name1", "Name2", "Name3", "PasswordRuleCode");
        }
        public static void BindProduct(DropDownList ddl)
        {
            ddl.Items.Clear();
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
            DataSet ds = bll.FetchFields("CouponTypeID,BrandID,ProdCode", "BindingType = 2 and  ProdCode is not null and DepartCode is null", null);
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CouponTypeIDS", "");

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string key = string.Format("{0} - {1}", dr["BrandCode"].ToString().Trim(), dr["ProdCode"].ToString().Trim());
                string couponTypeID = dr["CouponTypeID"].ToString().Trim();
                if (dic.ContainsKey(key))
                {
                    dic[key] += string.Format(",{0}", couponTypeID);
                    continue;
                }
                dic.Add(key, dr["CouponTypeID"].ToString().Trim());
            }

            BindDictionaryValueKey(ddl, dic);

        }

        public static void BindProduct(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
            DataSet ds = bll.FetchFields("CouponTypeID,BrandID,ProdCode", "BindingType = 2 and  ProdCode is not null and DepartCode is null", null);
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CouponTypeIDS", "");

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string key = string.Format("{0} - {1}", dr["BrandCode"].ToString().Trim(), dr["ProdCode"].ToString().Trim());
                string couponTypeID = dr["CouponTypeID"].ToString().Trim();
                if (dic.ContainsKey(key))
                {
                    dic[key] += string.Format(",{0}", couponTypeID);
                    continue;
                }
                dic.Add(key, dr["CouponTypeID"].ToString().Trim());
            }

            BindDictionaryValueKey(ddl, dic);

        }

        public static void BindDeparment(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
            DataSet ds = bll.FetchFields("CouponTypeID,BrandID,DepartCode", "BindingType = 2 and  ProdCode is null and DepartCode is not null", null);
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string key = string.Format("{0} - {1}", dr["BrandCode"].ToString().Trim(), dr["DepartCode"].ToString().Trim());
                string couponTypeID = dr["CouponTypeID"].ToString().Trim();
                if (dic.ContainsKey(key))
                {
                    dic[key] += string.Format(",{0}", couponTypeID);
                    continue;
                }
                dic.Add(key, dr["CouponTypeID"].ToString().Trim());
            }

            BindDictionaryValueKey(ddl, dic);
        }

        public static void BindProduct(DropDownList dll, int couponTypeID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is not null and DepartCode is null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "CouponTypeID", "ProdCode", "ProdCode", "ProdCode", "BrandCode");
        }

        public static void BindProduct(FineUI.DropDownList dll, int couponTypeID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is not null and DepartCode is null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "CouponTypeID", "ProdCode", "ProdCode", "ProdCode", "BrandCode");
        }

        public static void BindDeparment(DropDownList dll, int couponTypeID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is null and DepartCode is not null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "CouponTypeID", "DepartCode", "DepartCode", "DepartCode", "BrandCode");
        }

        public static void BindDeparment(FineUI.DropDownList dll, int couponTypeID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is null and DepartCode is not null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "CouponTypeID", "DepartCode", "DepartCode", "DepartCode", "BrandCode");
        }

        public static void BindCustomer(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Customer().GetList("1 = 1 order by CustomerCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CustomerID", "CustomerDesc1", "CustomerDesc2", "CustomerDesc3", "CustomerCode");
        }

        public static void BindCustomer(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Customer().GetList("1 = 1 order by CustomerCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CustomerID", "CustomerDesc1", "CustomerDesc2", "CustomerDesc3", "CustomerCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindBatchID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCoupon().GetBatchID();
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCouponCode";
            ddl.DataValueField = "BatchCouponID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindBatchID(FineUI.DropDownList ddl, int couponType)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCoupon().GetBatchIDByType(couponType);
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCouponCode";
            ddl.DataValueField = "BatchCouponID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }


        /// <summary>
        /// 绑定所有国家
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCountry(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Country().GetList("1 = 1 order by CountryCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CountryCode", "CountryName1", "CountryName2", "CountryName3");
        }

        /// <summary>
        /// 绑定所有省份
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindProvince(FineUI.DropDownList ddl, string CountryCode)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Province().GetList("CountryCode = '" + CountryCode + "' order by ProvinceCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "ProvinceCode", "ProvinceName1", "ProvinceName2", "ProvinceName3");
        }

        /// <summary>
        /// 绑定所有城市
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCity(FineUI.DropDownList ddl, string ProvinceCode)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.City().GetList("ProvinceCode = '" + ProvinceCode + "' order by CityCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CityCode", "CityName1", "CityName2", "CityName3");
        }

        /// <summary>
        /// 绑定所有区域
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindDistrict(FineUI.DropDownList ddl, string CityCode)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.District().GetList("CityCode = '" + CityCode + "' order by DistrictCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "DistrictCode", "DistrictName1", "DistrictName2", "DistrictName3");
        }

        //Add By Len
        public static void BindCardBatchID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            DataSet ds = bll.GetBatchID();
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        public static void BindCardBatchID(FineUI.DropDownList ddl, int CardGradeID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCard().GetBatchIDByType(CardGradeID);
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        //Add By Len
        public static void BindCardWithoutBrand(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetListWithoutBrand(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        //Add By Len
        public static void BindKeyValueList(FineUI.RadioButtonList rbl, List<KeyValue> list, bool required)
        {
            rbl.Items.Clear();
            rbl.DataSource = list;
            rbl.DataTextField = "Value";
            rbl.DataValueField = "Key";
            rbl.DataBind();
            if (required)
            {
                rbl.ShowRedStar = true;
                rbl.Required = true;
            }
        }

        public static void BindSupplier(FineUI.DropDownList ddl, int couponTypeID)
        {
            int value = DALTool.GetSupplier(couponTypeID);
            string text = DALTool.GetSupplierDesc(value);
            ddl.Items.Add(text, value.ToString());
        }

        public static void BindCouponTypeBySupplierID(FineUI.DropDownList ddl, int supplierID)
        {
            ddl.Items.Clear();
            ddl.DataSource = DALTool.GetCouponTypeList(supplierID).Tables[0];
            ddl.DataValueField = "CouponTypeID";
            ddl.DataTextField = DALTool.GetStringByCulture("CouponTypeName1", "CouponTypeName2", "CouponTypeName3");
            ddl.DataBind();
        }

        public static void BindAllSupplier(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            List<Edge.SVA.Model.Supplier> modelList = DALTool.GetAllSupplier();
            foreach (Edge.SVA.Model.Supplier model in modelList)
            {
                int value = model.SupplierID;
                string text = model.SupplierDesc1;
                ddl.Items.Add(text, value.ToString());
            }
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }

        public static void BindRoleID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = DBUtility.DbHelperSQL.Query("select RoleID,Description from Accounts_Roles order by RoleID").Tables[0];
            ddl.DataTextField = "Description";
            ddl.DataValueField = "RoleID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }

        //Modified By Robin  2014-07-17 default Company
        public static void BindCompanyID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = DBUtility.DbHelperSQL.Query("select companyID,companyCode + '-' + companyName companyName from company order by isDefault Desc,companyID ASC").Tables[0];
            ddl.DataTextField = "companyName";
            ddl.DataValueField = "companyID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });
        }
        public static void BindDayFlagID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = DBUtility.DbHelperSQL.Query("select DayFlagID,DayFlagCode from DayFlag order by DayFlagID").Tables[0];
            ddl.DataTextField = "DayFlagCode";
            ddl.DataValueField = "DayFlagID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });
        }
        public static void BindWeekFlagID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = DBUtility.DbHelperSQL.Query("select WeekFlagID,WeekFlagCode from WeekFlag order by WeekFlagID").Tables[0];
            ddl.DataTextField = "WeekFlagCode";
            ddl.DataValueField = "WeekFlagID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });
        }
        public static void BindMonthFlagID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.DataSource = DBUtility.DbHelperSQL.Query("select MonthFlagID,MonthFlagCode from MonthFlag order by MonthFlagID").Tables[0];
            ddl.DataTextField = "MonthFlagCode";
            ddl.DataValueField = "MonthFlagID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });
        }
        //Add by Alex 2014-06-12 ++
        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardType(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }
        public static void BindCardTypeWithoutBrand(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetListWithoutBrand(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        //Add by Alex 2014-06-12 --
        // Add by Alex 2014-07-02 ++
        public static void BindStoreCodeByStoreID(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreID", "StoreCode", "StoreName1", "StoreName2", "StoreName3");
        }
        /// <summary>
        /// 绑定所有新闻
        /// </summary>
        /// <param name="ddl">新闻CheckBoxList</param>
        public static void BindPromotionMsgCode(FineUI.DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.PromotionMsg().GetList("");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "PromotionMsgCode", "PromotionMsgCode", "PromotionMsgCode", "PromotionMsgCode", "PromotionMsgCode");

        }
        //Add by Alex 2014-07-02 ++
        ///Add by Nathan 20140611++
        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGradeWithoutBrand(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetListWithoutBrand(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }
        ///Add by Nathan 20140611--
        //Add by Nathan 20140707++
        /// <summary>
        /// 绑定语言列表
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindLanguageMap(FineUI.DropDownList ddl)
        {
            DataSet ds = new Edge.SVA.BLL.LanguageMap().GetAllList();
            ddl.Items.Clear();

            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ddl.DataTextField = "LanguageDesc";
                ddl.DataValueField = "KeyID";
                ddl.DataSource = ds;
                ddl.DataBind();
            }

            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        //Add by Nathan 20140707--
        #region add by Nathan 20140630 ++
        /// <summary>
        /// 绑定所有店铺（总部）
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreCode(FineUI.DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(" StoreTypeID = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreCode", "StoreCode", "StoreCode", "StoreCode");

        }
        public static void BindStoreAttributeCodeByStoreID(FineUI.DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store_Attribute().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "SAID", "SACode", "SADesc1", "SADesc2", "SADesc3");
        }


        /// <summary>
        /// 绑定所有优惠券类型性质
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponTypeNature(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeNature().GetList("1 = 1 order by CouponTypeNatureID");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeNatureID", "CouponTypeNatureName1", "CouponTypeNatureName2", "CouponTypeNatureName3");
        }
        #endregion add by Nathan 20140630 --
        // Add by Alex 2014-06-12 ++
        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindBatchCardID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCard().GetBatchID();
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindBatchCardID(FineUI.DropDownList ddl, int cardType)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCard().GetBatchIDByType(cardType);
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "", Value = "" });//Add by Nathan for All dropdownlist default value.
        }
        public static void BindCardActStatus(FineUI.DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CardController.CardStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new FineUI.ListItem() { Text = DALTool.GetCardStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });

        }
        // Add by Alex 2014-06-12 --

        //add++ by frank 20140721
        public static void BindIKeyValueList(FineUI.DropDownList ddl, List<IKeyValue> list, bool required)
        {
            ddl.Items.Clear();
            ddl.DataSource = list;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            if (required)
            {
                ddl.ShowRedStar = true;
                ddl.Required = true;
                ddl.CompareType = FineUI.CompareType.String;
                ddl.CompareValue = "-1";
                ddl.CompareOperator = FineUI.Operator.NotEqual;
                ddl.CompareMessage = "Please select";
            }
        }
        //add--

        /// <summary>
        /// 根据CardTypeCode，CardGradeCode，绑定ImportGM
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindImportGM(FineUI.DropDownList ddl, string cardtypecode, string cardgradecode)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.ImportGM().GetList(string.Format("CardTypeCode='{0}' and CardGradeCode = '{1}' ", cardtypecode, cardgradecode));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "SKU", "SKUDesc", "SKUDesc", "SKUDesc"); //ProdCode
            //assuming yung ProdCode ginawa kong SKU
        }


        //addedbyme
        public DataTable selectedSKU(string sku)
        {
            string connString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    SqlCommand query = new SqlCommand("SELECT SKUUnitAmount, GMValue, NumberMask, NumberPrefix, ProdCode, TransactionType FROM ImportGM WHERE SKU = @sku", conn);

                    query.Parameters.AddWithValue("@sku", sku);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = query;
                    adapter.Fill(dt);

                    return dt;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public string GetCardNumber(string approvalcode)
        {
            string connString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();

                    SqlCommand queryForApproval = new SqlCommand("SELECT CardAdjustNumber FROM Ord_CardAdjust_H WHERE ApprovalCode = @approvalcode", conn);

                    queryForApproval.Parameters.AddWithValue("@approvalcode", approvalcode);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = queryForApproval;
                    adapter.Fill(dt);

                    foreach(DataRow dr in dt.Rows)
                    {
                        return dr[0].ToString();
                    }

                    return null;

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }



        public void InsertApprovalCode(string cardnumber, string approvalcode)
        {
            string connString = ConfigurationSettings.AppSettings["ConnectionString"];

            using(SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    SqlCommand queryForUpdate = new SqlCommand("UPDATE Card_Movement SET ApprovalCode = @approvalcode WHERE RefTxnNo = @ref", conn);

                    queryForUpdate.Parameters.AddWithValue("@approvalcode", approvalcode);
                    queryForUpdate.Parameters.AddWithValue("@ref", cardnumber);

                    int aff = queryForUpdate.ExecuteNonQuery();

                    

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }


        //

    }
}
