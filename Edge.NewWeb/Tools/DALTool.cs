﻿using System;
using System.Collections.Generic;
using System.Web;
using Edge.Security.Manager;
using System.Web.Caching;
using Edge.Common;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using Edge.Web.Controllers;
using System.Text;
using Edge.Utils;

namespace Edge.Web.Tools
{
    /// <summary>
    /// 数据访问工具
    /// </summary>
    public class DALTool
    {
        #region 根据ID，获取名称

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public static string GetUserName(int userId)
        {
            System.Data.DataRow user = new Edge.Security.Data.User().Retrieve(userId);

            return user == null ? "" : user["UserName"].ToString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public static string GetUserName(int userId, Dictionary<int, string> cache)
        {
            if (userId == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(userId)) return cache[userId];

            System.Data.DataRow user = new Edge.Security.Data.User().Retrieve(userId);
            string result = user == null ? "" : user["UserName"].ToString();

            if (cache != null) cache.Add(userId, result);

            return result;
        }

        /// <summary>
        /// 获取当前登录用户
        /// </summary>
        /// <param name="siteLanguage"></param>
        /// <returns></returns>
        public static User GetCurrentUser()
        {
            Edge.Security.Model.WebSet set = HttpContext.Current.Cache["Cache_Webset"] as Edge.Security.Model.WebSet;
            if (set == null)
            {
                set = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));
            }

            if (SVASessionInfo.CurrentUser != null)
            {
                return SVASessionInfo.CurrentUser;
            }
            else
            {
                AccountsPrincipal user = new AccountsPrincipal(HttpContext.Current.User.Identity.Name, SVASessionInfo.SiteLanguage);//todo: 修改成多语言。
                User currentUser = new Edge.Security.Manager.User(user);

                return currentUser;
            }
        }

        public static string GetCardIssuerName()
        {
            List<Edge.SVA.Model.CardIssuer> cardIssuers = new SVA.BLL.CardIssuer().GetModelList("");
            if (cardIssuers == null || cardIssuers.Count <= 0) return "";

            return DALTool.GetStringByCulture(cardIssuers[0].CardIssuerName1, cardIssuers[0].CardIssuerName2, cardIssuers[0].CardIssuerName3);

        }

        /// <summary>
        /// 根据ID获取行业名称,若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">行业Id</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetIndustryName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Industry bll = new Edge.SVA.BLL.Industry();
            Edge.SVA.Model.Industry model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.IndustryName1, model.IndustryName2, model.IndustryName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetApproveStatusString(string status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status.ToUpper())
                    {
                        case "P": return "PENDING";
                        case "A": return "APPROVED";
                        case "V": return "VOID";
                        case "C": return "PICKING"; //Add By Robin 2014-07-10
                    }
                    break;
                case "zh-cn":
                    switch (status.ToUpper())
                    {
                        case "P": return "待批核";
                        case "A": return "已批核";
                        case "V": return "已作废";
                        case "C": return "已产生拣货单"; //Add By Robin 2014-07-10
                    }
                    break;
                case "zh-hk":
                    switch (status.ToUpper())
                    {
                        case "P": return "待批核";
                        case "A": return "已批核";
                        case "V": return "已作廢";
                        case "C": return "已產生檢貨單"; //Add By Robin 2014-07-10
                    }
                    break;
            }
            return "";
        }

        public static string GetOrderPickingApproveStatusString(string status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status.ToUpper())
                    {
                        case "R": return "PENDING";
                        case "P": return "PICKED";
                        case "A": return "APPROVED";
                        case "V": return "VOID";
                    }
                    break;
                case "zh-cn":
                    switch (status.ToUpper())
                    {
                        case "R": return "待批核";
                        case "P": return "已拣货";
                        case "A": return "已批核";
                        case "V": return "已作廢";
                    }
                    break;
                case "zh-hk":
                    switch (status.ToUpper())
                    {
                        case "R": return "待批核";
                        case "P": return "已揀貨";
                        case "A": return "已批核";
                        case "V": return "已作廢";
                    }
                    break;
            }
            return "";
        }

        /// <summary>
        /// 根据ID获取发行商名称,若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">发行商Id</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardIssuerName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardIssuer bll = new Edge.SVA.BLL.CardIssuer();
            Edge.SVA.Model.CardIssuer model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardIssuerName1, model.CardIssuerName2, model.CardIssuerName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        ///根据ID获取品牌名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetBrandName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Brand bll = new Edge.SVA.BLL.Brand();
            Edge.SVA.Model.Brand model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.BrandName1, model.BrandName2, model.BrandName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        ///根据ID获取品牌名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetBrandCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.Model.Brand model = new Edge.SVA.BLL.Brand().GetModel(id);

            string result = model == null ? "" : model.BrandCode;

            if (cache != null) cache.Add(id, result.ToUpper());

            return result;
        }


        /// <summary>
        ///根据ID获取店铺编号，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">店铺ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetStoreCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(id);

            string result = model == null ? "" : model.StoreCode;

            if (cache != null) cache.Add(id, result.ToUpper());

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="storeCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetStoreIDByBrandIDAndStoreCode(string brandID, string storeCode, Dictionary<string, int> cache)
        {
            string key = brandID + "_" + storeCode;

            if (cache != null && cache.ContainsKey(key)) return cache[key];

            Edge.SVA.Model.Store model = null;
            List<Edge.SVA.Model.Store> list = new Edge.SVA.BLL.Store().GetModelList(string.Format(" BrandID='{0}' and StoreCode='{1}'", brandID, storeCode));
            if (list.Count >= 1)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }
            int result = model == null ? 0 : model.StoreID;

            if (cache != null) cache.Add(key, result);

            return result;
        }

        /// <summary>
        /// 根据StoreCode获取BrandID
        /// </summary>
        /// <param name="storeCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetBrandIDByStoreCode(string storeCode, Dictionary<string, int> cache)
        {
            if (string.IsNullOrEmpty(storeCode)) return 0;

            if (cache != null && cache.ContainsKey(storeCode)) return Tools.ConvertTool.ToInt(cache[storeCode].ToString());


            List<Edge.SVA.Model.Store> list = new Edge.SVA.BLL.Store().GetModelList(string.Format("StoreCode='{0}'", storeCode));

            Edge.SVA.Model.Store model = null;

            if (list.Count > 0)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }

            int result = model == null ? 0 : model.BrandID.GetValueOrDefault();

            if (cache != null) cache.Add(storeCode, result);

            return result;
        }
        /// <summary>
        /// 根据BrandCode获取BrandID
        /// </summary>
        /// <param name="brandCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetBrandIDByBrandCode(string brandCode, Dictionary<string, int> cache)
        {
            if (string.IsNullOrEmpty(brandCode)) return 0;

            if (cache != null && cache.ContainsKey(brandCode)) return Tools.ConvertTool.ToInt(cache[brandCode].ToString());


            List<Edge.SVA.Model.Brand> list = new Edge.SVA.BLL.Brand().GetModelList(string.Format("brandCode='{0}'", brandCode));

            Edge.SVA.Model.Brand model = null;

            if (list.Count > 0)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }

            int result = model == null ? 0 : model.BrandID;

            if (cache != null) cache.Add(brandCode, result);

            return result;
        }


        /// <summary>
        ///根据ID获取区域名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetLocationName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.StoreGroup bll = new Edge.SVA.BLL.StoreGroup();
            Edge.SVA.Model.StoreGroup model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.StoreGroupName1, model.StoreGroupName2, model.StoreGroupName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }


        /// <summary>
        ///根据ID获取店铺名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetStoreName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Store bll = new Edge.SVA.BLL.Store();
            Edge.SVA.Model.Store model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.StoreName1, model.StoreName2, model.StoreName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡类型ID，获取卡类型名称
        /// </summary>
        /// <param name="id">卡类型ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();
            Edge.SVA.Model.CardType model = bll.GetModel(id);

            string result = model == null ? "" : model.CardTypeCode + "-" + GetStringByCulture(model.CardTypeName1, model.CardTypeName2, model.CardTypeName3);//显示完整的Code+Name（Add by Len）

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetMessageTemplateCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.MessageTemplate bll = new Edge.SVA.BLL.MessageTemplate();
            Edge.SVA.Model.MessageTemplate model = bll.GetModel(id);

            string result = model == null ? "" : model.MessageTemplateCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }
        public static string GetCardTypeCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();
            Edge.SVA.Model.CardType model = bll.GetModel(id);

            string result = model == null ? "" : model.CardTypeCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡级别ID，获取卡级别编号
        /// </summary>
        /// <param name="id">卡级别ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        /// <modify>Len</modify>
        public static string GetCardGradeCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade model = bll.GetModel(id);

            string result = model == null ? "" : model.CardGradeCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡级别ID，获取卡级别名称
        /// </summary>
        /// <param name="id">卡级别ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardGradeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade model = bll.GetModel(id);

            string result = model == null ? "" : model.CardGradeCode + "-" + GetStringByCulture(model.CardGradeName1, model.CardGradeName2, model.CardGradeName3);//显示完整的Code+Name（Add by Len）

            if (cache != null) cache.Add(id, result);

            return result;
        }
        //Added By Jay Celeste 1/19/2016
        public static string GetCardGradeName1(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade model = bll.GetModel(id);

            string result = model == null ? "" : model.CardGradeName1; 

            if (cache != null) cache.Add(id, result);

            return result;
        }
        public static string GetTnxTypeName(int id)
        {
            return new Edge.SVA.BLL.Coupon_Movement().GetTxnType(id);
        }
        /// <summary>
        /// 根据优惠劵类型ID，获取优惠劵名称
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCouponTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.Model.CouponType model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CouponTypeName1, model.CouponTypeName2, model.CouponTypeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetCouponTypeName(string code, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(code)) return "";

            if (cache != null && cache.ContainsKey(code)) return cache[code];

            Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetImportCouponType(code);

            string result = model == null ? "" : GetStringByCulture(model.CouponTypeName1, model.CouponTypeName2, model.CouponTypeName3);

            if (cache != null) cache.Add(code, result);

            return result;
        }
        /// <summary>
        /// 根据优惠劵类型ID，获取优惠劵名称
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCouponTypeCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.Model.CouponType model = bll.GetModel(id);

            string result = model == null ? "" : model.CouponTypeCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }
        /// <summary>
        /// 根据优惠劵ID，获取优惠劵库存状态
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCouponStockStatusByID(string id, Dictionary<string, string> cache)
        {
            if (id == string.Empty) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            Edge.SVA.Model.Coupon model = bll.GetModel(id);

            string result = model == null ? "" : Convert.ToString(model.StockStatus);

            if (cache != null) cache.Add(id, result);

            return result;
        }
        /// <summary>
        /// 根据优惠劵类型ID，获取品牌ID
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <returns></returns>
        public static int GetCouponTypeBrandID(int id)
        {
            return new Edge.SVA.BLL.CouponType().GetModel(id).BrandID;
        }
        /// <summary>
        /// 从CouponUIDMap获得CouponNumber
        /// </summary>
        /// <param name="id">CouponUID</param>
        /// <returns></returns>
        public static string GetCouponUIDByCouponNumber(string strWhere)
        {
            DataSet ds = new Edge.SVA.BLL.CouponUIDMap().GetList(strWhere);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0]["CouponUID"]);
            }
            else 
            {
                return "";
            }
        }
        /// <summary>
        /// 根据联系方式类型ID，获取联系方式名称
        /// </summary>
        /// <param name="id">联系方式类型ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetSNSTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.SNSType bll = new Edge.SVA.BLL.SNSType();
            Edge.SVA.Model.SNSType model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.SNSTypeName1, model.SNSTypeName2, model.SNSTypeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡类型种类ID，获取卡类型种类名称
        /// </summary>
        /// <param name="id">卡类型种类ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardTypeNatureName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardTypeNature bll = new Edge.SVA.BLL.CardTypeNature();
            Edge.SVA.Model.CardTypeNature model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardTypeNatureName1, model.CardTypeNatureName2, model.CardTypeNatureName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据币种ID，获取币种名称
        /// </summary>
        /// <param name="id">币种ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCurrencyName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Currency bll = new Edge.SVA.BLL.Currency();
            Edge.SVA.Model.Currency model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CurrencyName1, model.CurrencyName2, model.CurrencyName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetUID(string couponNumber, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(couponNumber)) return couponNumber;

            if (cache != null && cache.ContainsKey(couponNumber)) return cache[couponNumber];

            Edge.SVA.BLL.CouponUIDMap bll = new Edge.SVA.BLL.CouponUIDMap();
            List<Edge.SVA.Model.CouponUIDMap> model = bll.GetModelList(string.Format("CouponNumber = '{0}'", couponNumber));

            if (model == null || model.Count <= 0) return "";

            if (cache != null) cache.Add(model[0].CouponNumber, model[0].CouponUID);

            return model[0].CouponUID;
        }

        public static string GetCardUID(string cardNumber, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(cardNumber)) return cardNumber;

            if (cache != null && cache.ContainsKey(cardNumber)) return cache[cardNumber];

            Edge.SVA.BLL.CardUIDMap bll = new Edge.SVA.BLL.CardUIDMap();
            List<Edge.SVA.Model.CardUIDMap> model = bll.GetModelList(string.Format("CardNumber = '{0}'", cardNumber));

            if (model == null || model.Count <= 0) return "";

            if (cache != null) cache.Add(model[0].CardNumber, model[0].CardUID);

            return model[0].CardUID;
        }

        public static string GetCouponTypeStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 0: return Controllers.CouponController.CouponStatus.Dormant.ToString().ToUpper();
                        case 1: return Controllers.CouponController.CouponStatus.Issued.ToString().ToUpper();
                        case 2: return Controllers.CouponController.CouponStatus.Activated.ToString().ToUpper();
                        case 3: return Controllers.CouponController.CouponStatus.Redeemed.ToString().ToUpper();
                        case 4: return Controllers.CouponController.CouponStatus.Expired.ToString().ToUpper();
                        case 5: return Controllers.CouponController.CouponStatus.Voided.ToString().ToUpper();
                        case 6: return Controllers.CouponController.CouponStatus.Recycled.ToString().ToUpper();
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case 0: return "静止的";
                        case 1: return "已发行";
                        case 2: return "已激活";
                        case 3: return "已消费";
                        case 4: return "已过期";
                        case 5: return "已作废";
                        case 6: return "已回收";
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case 0: return "靜止的";
                        case 1: return "已發行";
                        case 2: return "已激活";
                        case 3: return "已消費";
                        case 4: return "已過期";
                        case 5: return "已作廢";
                        case 6: return "已回收";
                    }
                    break;
            }
            //switch (status)
            //{
            //    case 0: return Controllers.CouponController.CouponStatus.Dormant.ToString().ToUpper();
            //    case 1: return Controllers.CouponController.CouponStatus.Issued.ToString().ToUpper();
            //    case 2: return Controllers.CouponController.CouponStatus.Activated.ToString().ToUpper();
            //    case 3: return Controllers.CouponController.CouponStatus.Redeemed.ToString().ToUpper();
            //    case 4: return Controllers.CouponController.CouponStatus.Expired.ToString().ToUpper();
            //    case 5: return Controllers.CouponController.CouponStatus.Voided.ToString().ToUpper();
            //    case 6: return Controllers.CouponController.CouponStatus.Recycled.ToString().ToUpper();
            //}
            return "";
        }

        //Add By Robin 2014-06-20
        public static string GetCouponStockStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return Controllers.CouponController.CouponStockStatus.ForPrinting.ToString().ToUpper();
                        case 2: return Controllers.CouponController.CouponStockStatus.GoodForRelease.ToString().ToUpper();
                        case 3: return Controllers.CouponController.CouponStockStatus.Damaged.ToString().ToUpper();
                        case 4: return Controllers.CouponController.CouponStockStatus.Picked.ToString().ToUpper();
                        case 5: return Controllers.CouponController.CouponStockStatus.InTransit.ToString().ToUpper();
                        case 6: return Controllers.CouponController.CouponStockStatus.InLocation.ToString().ToUpper();
                        case 7: return Controllers.CouponController.CouponStockStatus.Returned.ToString().ToUpper();
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return "总部未确认收货";
                        case 2: return "总部确认收货";
                        case 3: return "总部发现有优惠券损坏";
                        case 4: return "总部收到了店铺的订单";
                        case 5: return "总部收到了店铺的订单并发货";
                        case 6: return "店铺确认收货";
                        case 7: return "店铺退货给总部";
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return "總部未確認收貨";
                        case 2: return "總部確認收貨";
                        case 3: return "總部發現有優惠劵損壞";
                        case 4: return "總部收到了店舖的訂單";
                        case 5: return "總部收到了店舖的訂單並發貨";
                        case 6: return "店舖確認收貨";
                        case 7: return "店舖退貨給總部";
                    }
                    break;
            }
            return "";
        }
        //End

        public static string GetCardStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 0: return Controllers.CardController.CardStatus.Dormant.ToString().ToUpper();
                        case 1: return Controllers.CardController.CardStatus.Issued.ToString().ToUpper();
                        case 2: return Controllers.CardController.CardStatus.Active.ToString().ToUpper();
                        case 3: return Controllers.CardController.CardStatus.Redeemed.ToString().ToUpper();
                        case 4: return Controllers.CardController.CardStatus.Expired.ToString().ToUpper();
                        case 5: return Controllers.CardController.CardStatus.Void.ToString().ToUpper();
                        case 6: return Controllers.CardController.CardStatus.Recycled.ToString().ToUpper();
                        case 7: return Controllers.CardController.CardStatus.Inactive.ToString().ToUpper();//Update by Alex 2014-07-23
                        case 8: return Controllers.CardController.CardStatus.Deactive.ToString().ToUpper();//Added by Robin 2014-11-14 for 7-11用户主动要求停止账户
                        case 9: return Controllers.CardController.CardStatus.Frozen.ToString().ToUpper();//Update by Alex 2014-07-15
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case 0: return "静止的";
                        case 1: return "已发行";
                        case 2: return "已激活";
                        case 3: return "已消费";
                        case 4: return "已过期";
                        case 5: return "已作废";
                        case 6: return "回收";    
                        case 7: return "未验证";//Update by Alex 2014-07-23
                        case 8: return "已关闭";//Added by Robin 2014-11-14 for 7-11用户主动要求停止账户
                        case 9: return "已冻结";//Update by Alex 2014-07-15
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case 0: return "靜止的";
                        case 1: return "已發行";
                        case 2: return "已激活";
                        case 3: return "已消費";
                        case 4: return "已過期";
                        case 5: return "已作廢";
                        case 6: return "回收";
                        case 7: return "未驗證";//Update by Alex 2014-07-23
                        case 8: return "已關閉";//Added by Robin 2014-11-14 for 7-11用户主动要求停止账户
                        case 9: return "已凍結";//Update by Alex 2014-07-15
                    }
                    break;
            }
            //switch (status)
            //{
            //    case 0: return Controllers.CardController.CardStatus.Dormant.ToString().ToUpper();
            //    case 1: return Controllers.CardController.CardStatus.Issued.ToString().ToUpper();
            //    case 2: return Controllers.CardController.CardStatus.Active.ToString().ToUpper();
            //    case 3: return Controllers.CardController.CardStatus.Redeemed.ToString().ToUpper();
            //    case 4: return Controllers.CardController.CardStatus.Expired.ToString().ToUpper();
            //    case 5: return Controllers.CardController.CardStatus.Void.ToString().ToUpper();
            //}
            return "";
        }

        public static Edge.SVA.Model.CouponType GetCouponType(int couponTypeID, Dictionary<int, Edge.SVA.Model.CouponType> cache)
        {
            if (cache != null && cache.ContainsKey(couponTypeID)) return cache[couponTypeID];

            Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

            if (cache != null) cache.Add(couponTypeID, couponType);

            return couponType;
        }

        public static string GetBatchCode(int batchID, Dictionary<int, string> cache)
        {
            if (batchID <= 0) return "";

            if (cache != null && cache.ContainsKey(batchID)) return cache[batchID];

            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Edge.SVA.Model.BatchCoupon model = bll.GetModel(batchID);

            if (model == null) return "";

            if (cache != null) cache.Add(batchID, model.BatchCouponCode);

            return model.BatchCouponCode;
        }

        public static string GetCardBatchCode(int batchID, Dictionary<int, string> cache)
        {
            if (batchID <= 0) return "";

            if (cache != null && cache.ContainsKey(batchID)) return cache[batchID];

            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Edge.SVA.Model.BatchCard model = bll.GetModel(batchID);

            if (model == null) return "";

            if (cache != null) cache.Add(batchID, model.BatchCardCode);

            return model.BatchCardCode;
        }

        public static string GetBrandCodeByCouponTypeID(int couponTypeID, Dictionary<int, string> cache)
        {
            if (couponTypeID <= 0) return "";

            if (cache != null && cache.ContainsKey(couponTypeID)) return cache[couponTypeID];

            Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);
            if (couponType == null) return "";

            Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(couponType.BrandID);

            if (brand == null) return "";

            if (cache != null) cache.Add(couponTypeID, brand.BrandCode);

            return brand.BrandCode;
        }

        public static string GetPasswordFormatName(int format)
        {
            switch (format)
            {
                case 0: return "No Requirement";
                case 1: return "Numbers";
                case 2: return "Alphabets";
                case 3: return "Numbers + Alphabets";
                case 4: return "Numbers +Alphabets +Symbols";
                default: return "";

            }
        }
        #endregion

        #region GetObject Update Add Delete

        public static object GetObject<BLL>(object id) where BLL : new()
        {
            return DALTool.ExecuteMethod<BLL>("GetModel", new object[] { id });
        }

        public static bool Update<BLL>(object obj) where BLL : new()
        {
            if (obj == null) return false;
            object result = ExecuteMethod<BLL>("Update", new object[] { obj });
            if (result is bool) return (bool)result;

            return false;
        }

        public static bool Delete<BLL>(object id) where BLL : new()
        {
            if (id == null) return false;
            object result = ExecuteMethod<BLL>("Delete", new object[] { id });
            if (result is bool) return (bool)result;
            if (result is int) return (int)result > 0 ? true : false;


            return false;
        }

        public static int Add<BLL>(object obj) where BLL : new()
        {
            if (obj == null) return -1;
            object result = ExecuteMethod<BLL>("Add", new object[] { obj });

            if (result is int) return (int)result;
            if (result is bool) return (bool)result ? 1 : -1;
            if (result is Guid) return (Guid)result == Guid.Empty ? -1 : 1;

            return 0;
        }

        #endregion

        #region Common

        private static object ExecuteMethod<T>(string method, object[] parameters) where T : new()
        {
                T obj = new T();
                Type type = typeof(T);
                System.Reflection.MethodInfo mi = null;
                try
                {
                    mi = type.GetMethod(method);
                }
                catch
                {
                    if (mi == null)
                    {
                        Type[] args = GetTypes(parameters);
                        mi = type.GetMethod(method, args);
                    }
                }
                if (mi == null) return default(T);


                System.Reflection.ParameterInfo[] pis = mi.GetParameters();

                for (int i = 0; i < pis.Length; i++)
                {
                    parameters[i] = Convert.ChangeType(parameters[i], pis[i].ParameterType);
                }

                try
                {

                    return mi.Invoke(obj, parameters);
                }
                catch(Exception ex)
                {
                    throw ex;
                }

        }

        private static Type[] GetTypes(object[] args)
        {
            if (args == null) return Type.EmptyTypes;
            Type[] types = new Type[args.Length];
            for (int i = 0; i < types.Length; i++)
            {
                types[i] = args[i].GetType();
            }
            return types;
        }

        /// <summary>
        /// 根据当前语言区域获取各自语言
        /// </summary>
        /// <param name="en">英文</param>
        /// <param name="zhCN">简体中文</param>
        /// <param name="zhHK">繁体中文</param>
        /// <returns></returns>
        public static string GetStringByCulture(string name1, string name2, string name3)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": return name1;
                case "zh-cn": return name2;
                case "zh-hk": return name3;
                default: return name1;
            }
        }

        #endregion

        #region Common Function
        public static int ExecSODEOD(int userID, out DateTime dt)
        {
            int rowEffect = 0;
            int rtnVal = 0;
            dt = DateTime.Today;

            IDataParameter[] parameters = { 

                 new SqlParameter("@UserID", SqlDbType.Int) , 

                 new SqlParameter("@BusDate", SqlDbType.Date) 

             };
            parameters[0].Value = userID;
            parameters[1].Direction = ParameterDirection.Output;

            rtnVal = DBUtility.DbHelperSQL.RunProcedure("EOD", parameters, out rowEffect);
            if (parameters[1].Value != null)
            {
                DateTime.TryParse(parameters[1].Value.ToString(), out dt);
            }
            return rtnVal;
        }
        public static void SODEODSetTime(string timestr)
        {
            int rowEffect = 0;
            int rtnVal = 0;

            IDataParameter[] parameters = { 
                                              
                 new SqlParameter("@jobname", SqlDbType.VarChar,50) , 
                 new SqlParameter("@sql", SqlDbType.NVarChar) , 
                 //new SqlParameter("@servername", SqlDbType.NVarChar) , 
                 new SqlParameter("@dbname", SqlDbType.NVarChar) ,
                 new SqlParameter("@freqtype", SqlDbType.NVarChar) ,
                 new SqlParameter("@fsinterval", SqlDbType.Int) , 
                 new SqlParameter("@time", SqlDbType.NVarChar) , 


             };
            parameters[0].Value = "SODEOD ";
            parameters[1].Value = @"USE " + AppConfig.Singleton.DbName + @"
GO
declare @BusDate date
EXEC	 [dbo].[EOD]
		@UserID = 1,
		@BusDate = @BusDate OUTPUT ";
            //parameters[2].Value = AppConfig.Singleton.ServerName;
            parameters[2].Value = AppConfig.Singleton.DbName;
            parameters[3].Value = "day ";
            parameters[4].Value = 1;
            parameters[5].Value = timestr;

            rtnVal = DBUtility.DbHelperSQL.RunProcedure("[master].[dbo].[CreateScheduleJob]", parameters, out rowEffect);
            //if (parameters[1].Value != null)
            //{
            //    DateTime.TryParse(parameters[1].Value.ToString(), out dt);
            //}
            //return rtnVal;
        }

        public static string GetREFNOCode(string code)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { 

                 new SqlParameter("@CODE", SqlDbType.VarChar,6) , 

                 new SqlParameter("@REFNO", SqlDbType.NVarChar,50) 

             };
            parameters[0].Value = code;
            parameters[1].Direction = ParameterDirection.Output;

            DBUtility.DbHelperSQL.RunProcedure("GetRefNoString", parameters, "ds");
            if (parameters[1].Value != null)
            {
                rtn = parameters[1].Value.ToString();
            }
            return rtn;
        }

        public static string GetOrdCouponAdjustApproveCode(string couponAdjustNumber)
        {
            string rtn = string.Empty;

            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H();

            Edge.SVA.Model.Ord_CouponAdjust_H model = bll.GetModel(couponAdjustNumber);
            if (model != null)
            {
                rtn = model.ApprovalCode;
            }
            return rtn;
        }

        public static string GetBusinessDate()
        {
            string rtn = string.Empty;

            DataSet ds = DBUtility.DbHelperSQL.Query("select BusDate from SODEOD where SOD=1 and EOD=0 ");
            if (ds.Tables.Count > 0)
            {
                rtn = Convert.ToDateTime(ds.Tables[0].Rows[0]["BusDate"]).ToString("yyyy-MM-dd");
            }
            return rtn;
        }
        public static string GetAutoSODEODTime()
        {
            string rtn = string.Empty;
            try
            {
                DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT    msdb.dbo.sysschedules.active_start_time
FROM         msdb.dbo.sysjobs INNER JOIN
                      msdb.dbo.sysjobschedules ON msdb.dbo.sysjobs.job_id = msdb.dbo.sysjobschedules.job_id INNER JOIN
                      msdb.dbo.sysschedules ON msdb.dbo.sysjobschedules.schedule_id = msdb.dbo.sysschedules.schedule_id
WHERE     (msdb.dbo.sysjobs.name = N'SODEOD')");
                if (ds.Tables.Count > 0)
                {
                    string str = ds.Tables[0].Rows[0]["active_start_time"].ToString().PadLeft(6,'0');
                    rtn = str.Substring(0, 2) + ":" + str.Substring(2, 2);
                }
            }
            catch (System.Exception ex)
            {

            }
            return rtn;
        }
        public static string GetSystemDate()
        {
            return System.DateTime.Now.ToString("yyyy-MM-dd");

        }

        public static string GetSystemDateTime()
        {
            return System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        }

        public static string GetCouponTypeExpiryDate(Edge.SVA.Model.CouponType model)
        {
            DateTime nowDate = System.DateTime.Now;

            switch (model.CouponValidityUnit)
            {
                case 0: break;
                case 1: DateTime newYearDate = nowDate.AddYears(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newYearDate);
                case 2: DateTime newMonthDate = nowDate.AddMonths(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newMonthDate);
                case 3: DateTime newWeekDate = nowDate.AddDays(model.CouponValidityDuration * 7);
                    return ConvertTool.ToStringDate(newWeekDate);
                case 4: DateTime newDayDate = nowDate.AddDays(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newDayDate);
                case 5: DateTime newDate = nowDate.AddYears(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(model.CouponTypeEndDate.Value);
                case 6: DateTime spDate = model.CouponSpecifyExpiryDate.GetValueOrDefault();
                    return ConvertTool.ToStringDate(spDate);

            }

            return GetSystemDate();

        }

        public static string GetCardTypeExpiryDate(Edge.SVA.Model.CardGrade model)
        {
            DateTime nowDate = System.DateTime.Now;

            switch (model.CardValidityUnit)
            {
                case 0: break;
                case 1: DateTime newYearDate = nowDate.AddYears(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newYearDate);
                case 2: DateTime newMonthDate = nowDate.AddMonths(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newMonthDate);
                case 3: DateTime newWeekDate = nowDate.AddDays(model.CardValidityDuration.GetValueOrDefault() * 7);
                    return ConvertTool.ToStringDate(newWeekDate);
                case 4: DateTime newDayDate = nowDate.AddDays(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newDayDate);
                //case 5: DateTime newDate = nowDate.AddYears(model.CardValidityDuration);
                //    return ConvertTool.ToStringDate(model.);

            }

            return GetSystemDate();

        }
        #endregion

        #region Checking Function
        public static bool isHasStoreCodeWithBrandID(string storecode, int brandID, int storeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("StoreCode='" + storecode + "' and BrandID=" + brandID);
            if (storeID > 0)
            {
                sbWhere.Append(" and StoreID <> " + storeID);
            }
            int count = new Edge.SVA.BLL.Store().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }


        /// <summary>
        /// 检查couponNatureCode是否存在，当couponNatureCodeID=0时是新增时用。
        /// </summary>
        /// <param name="couponNatureCode"></param>
        /// <param name="couponNatureCodeID"></param>
        /// <returns></returns>
        public static bool isHasCouponNatureCode(string couponNatureCode, int couponNatureCodeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CouponNatureCode='" + couponNatureCode + "'");
            if (couponNatureCodeID > 0)
            {
                sbWhere.Append(" and CouponNatureID <> " + couponNatureCodeID);
            }

            int count = new Edge.SVA.BLL.CouponNature().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查BrandCode是否存在，当brandID=0时是新增时用。
        /// </summary>
        /// <param name="brandCode"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        public static bool isHasBrandCode(string brandCode, int brandID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" BrandCode='" + brandCode + "' ");
            if (brandID > 0)
            {
                sbWhere.Append(" and BrandID <> " + brandID);
            }
            int count = new Edge.SVA.BLL.Brand().GetCountUnlimited(sbWhere.ToString());

            
            return count > 0;
        }


        /// <summary>
        /// 检查CampaignCode是否存在，当campaignID=0时是新增时用。
        /// </summary>
        /// <param name="campaignCode"></param>
        /// <param name="campaignID"></param>
        /// <returns></returns>
        public static bool isHasCampaignCode(string campaignCode, int campaignID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CampaignCode='" + campaignCode + "'");
            if (campaignID > 0)
            {
                sbWhere.Append(" and CampaignID <> " + campaignID);
            }

            int count = new Edge.SVA.BLL.Campaign().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查EducationCode是否存在，当educationID=0时是新增时用。
        /// </summary>
        /// <param name="educationCode"></param>
        /// <param name="educationID"></param>
        /// <returns></returns>
        public static bool isHasEducationCode(string educationCode, int educationID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("EducationCode='" + educationCode + "'");
            if (educationID > 0)
            {
                sbWhere.Append(" and EducationID <> " + educationID);
            }

            int count = new Edge.SVA.BLL.Education().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查NationCode是否存在，当nationID=0时是新增时用。
        /// </summary>
        /// <param name="nationCode"></param>
        /// <param name="nationID"></param>
        /// <returns></returns>
        public static bool isHasNationCode(string nationCode, int nationID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("NationCode='" + nationCode + "'");
            if (nationID > 0)
            {
                sbWhere.Append(" and NationID <> " + nationID);
            }

            int count = new Edge.SVA.BLL.Nation().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查ProfessionCode是否存在，当professionID=0时是新增时用。
        /// </summary>
        /// <param name="professionCode"></param>
        /// <param name="professionID"></param>
        /// <returns></returns>
        public static bool isHasProfessionCode(string professionCode, int professionID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ProfessionCode='" + professionCode + "'");
            if (professionID > 0)
            {
                sbWhere.Append(" and ProfessionID <> " + professionID);
            }

            int count = new Edge.SVA.BLL.Profession().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查IndustryCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasIndustryCode(string industryCode, int industryID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("IndustryCode='" + industryCode + "'");
            if (industryID > 0)
            {
                sbWhere.Append(" and IndustryID <> " + industryID);
            }

            int count = new Edge.SVA.BLL.Industry().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查StoreTypeCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasStoreTypeCode(string storeTypeCode, int storeTypeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("StoreTypeCode='" + storeTypeCode + "'");
            if (storeTypeID > 0)
            {
                sbWhere.Append(" and StoreTypeID <> " + storeTypeID);
            }

            int count = new Edge.SVA.BLL.StoreType().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查CurrencyCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasCurrencyCode(string currencyCode, int currencyID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CurrencyCode='" + currencyCode + "'");
            if (currencyID > 0)
            {
                sbWhere.Append(" and CurrencyID <> " + currencyID);
            }

            int count = new Edge.SVA.BLL.Currency().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查PasswordSettingCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasPasswordRuleCode(string passwordRuleCode, int passwordRuleID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("PasswordRuleCode='" + passwordRuleCode + "'");
            if (passwordRuleID > 0)
            {
                sbWhere.Append(" and PasswordRuleID <> " + passwordRuleID);
            }

            int count = new Edge.SVA.BLL.PasswordRule().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查ReasonCode是否存在，当reasonID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasReasonCode(string reasonCode, int reasonID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ReasonCode='" + reasonCode + "'");
            if (reasonID > 0)
            {
                sbWhere.Append(" and ReasonID <> " + reasonID);
            }

            int count = new Edge.SVA.BLL.Reason().GetCount(sbWhere.ToString());
            return count > 0;
        }
        public static bool isHasCSVXMLBindingCode(string BindingCode)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("BindingCode='" + BindingCode + "'");

            int count = new Edge.SVA.BLL.CSVXMLBinding().GetCount(sbWhere.ToString());
            return count > 0;
        }
        public static bool isHasCSVXMLMointoringRuleCode(string RuleName)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("RuleName='" + RuleName + "'");

            int count = new Edge.SVA.BLL.CSVXMLMointoringRule().GetCount(sbWhere.ToString());
            return count > 0;
        }
        /// <summary>
        /// 检查cardTypeCode是否存在，当cardTypeID=0时是新增时用。
        /// </summary>
        /// <param name="cardTypeCode"></param>
        /// <param name="cardTypeID"></param>
        /// <returns></returns>
        public static bool isHasCardTypeCode(string cardTypeCode, int cardTypeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardTypeCode='" + cardTypeCode + "'");
            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查cardGradCode是否存在，当cardGradID=0时是新增时用。
        /// </summary>
        /// <param name="cardGradCode"></param>
        /// <param name="cardGradID"></param>
        /// <returns></returns>
        public static bool isHasCardGradeCode(string cardGradCode, int cardGradID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardGradeCode='" + cardGradCode + "'");
            if (cardGradID > 0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }


        public static bool isHasCardGradeRank(string rank, int cardTypeID, int cardGradID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" CardGradeRank='" + rank + "'");
            sbWhere.Append(" and CardTypeID='" + cardTypeID + "'");
            if (cardGradID > 0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasUserMessageSetting(string code)
        {
            return new Edge.SVA.BLL.UserMessageSetting_H().Exists(code);
        }

        public static bool isHasCouponAutoPickingRule(string code)
        {
            return new Edge.SVA.BLL.CouponAutoPickingRule_H().Exists(code);
        }

        public static bool isHasCouponReplenishRuleCode(string code)
        {
            return new Edge.SVA.BLL.CouponReplenishRule_H().Exists(code);
        }

        public static bool isHasInventoryReplenishRuleCode(string code)
        {
            return new Edge.SVA.BLL.InventoryReplenishRule_H().Exists(code);
        }

        /// <summary>
        /// 检查customerCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static bool isHasCustomerCode(string customerCode, int customerID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CustomerCode='" + customerCode + "'");
            if (customerID > 0)
            {
                sbWhere.Append(" and CustomerID <> " + customerID);
            }

            int count = new Edge.SVA.BLL.Customer().GetCount(sbWhere.ToString());
            return count > 0;
        }

        public static bool ExistsMsgTemplateCode(string code)
        {
            bool rtn = true;
            StringBuilder sb = new StringBuilder();
            sb.Append(" MessageTemplateCode='");
            sb.Append(code);
            sb.Append("'");
            int count = new Edge.SVA.BLL.MessageTemplate().GetRecordCount(sb.ToString());
            if (count >= 1)
            {
                rtn = true;
            }
            else
            {
                rtn = false;
            }
            return rtn;
        }
        /// <summary>
        /// 检查DistributeTemplateCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static bool isHasDistributeCode(string distributeCode, int distributeID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("DistributionCode='" + distributeCode + "'");
            if (distributeID > 0)
            {
                sbWhere.Append(" and DistributionID <> " + distributeID);
            }

            int count = new Edge.SVA.BLL.DistributeTemplate().GetCount(sbWhere.ToString());
            return count > 0;
        }



        /// <summary>
        /// 检查organizationCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="organizationCode"></param>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public static bool isHasOrganizationCode(string organizationCode, int organizationID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("OrganizationCode='" + organizationCode + "'");
            if (organizationID > 0)
            {
                sbWhere.Append(" and OrganizationID <> " + organizationID);
            }

            int count = new Edge.SVA.BLL.Organization().GetCount(sbWhere.ToString());
            return count > 0;
        }


        public static bool isEmptyCardNumMaskWithCardType(int cardTypeID)
        {
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited(" [CardTypeID] =" + cardTypeID + "and ([CardNumMask] is null or [CardNumMask]='') ") > 0)
            {
                return true;
            }

            return false;

        }



        public static bool isHasCardCardeCardNumMask(string cardNumMask, string cardNumPattern, int cardGradeID, int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");

            if (cardGradeID > 0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradeID);
            }

            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasCardCardeCardNumMaskWithCardType(string cardNumMask, string cardNumPattern, int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");

            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }


        public static bool isHasCardTypeCardNumMask(string cardNumMask, string cardNumPattern, int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");
            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasCouponTypeCouponNumMask(string couponNumMask, string couponNumPattern, int couponTypeID)
        {
            if (string.IsNullOrEmpty(couponNumMask) && string.IsNullOrEmpty(couponNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CouponNumMask]='" + couponNumMask.ToUpper() + "'" + " and [CouponNumPattern]='" + couponNumPattern + "'");
            if (couponTypeID > 0)
            {
                sbWhere.Append(" and CouponTypeID <> " + couponTypeID);
            }

            int count = new Edge.SVA.BLL.CouponType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numMask">将要写入的Number Mask</param>
        /// <param name="numPattern">将要写入的Number Pattern</param>
        /// <param name="type">0: CardTypre. 1: Cardgrade 2:CouponType</param>
        /// <param name="typeID">@TypeID内容根据@Type的设置. 新增时，可以填写0</param>
        /// <returns></returns>
        public static bool CheckNumberMask(string numMask, string numPattern, int type, int typeID)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int) , // 0: CardTypre. 1: Cardgrade 2:CouponType
                                            new SqlParameter("@TypeID", SqlDbType.Int),// @TypeID内容根据@Type的设置. 新增时，可以填写0
                                            new SqlParameter("@NumMask", SqlDbType.VarChar,512), // 将要写入的Number Mask
                                            new SqlParameter("@NumPattern", SqlDbType.VarChar,512),// 将要写入的Number Pattern
                                             new SqlParameter("@ReturnTypeID", SqlDbType.Int)// 正常返回0，有冲突返回冲突的ID，同样根据@Type区分
                                          };
            parameters[0].Value = type;
            parameters[1].Value = typeID;
            parameters[2].Value = numMask;
            parameters[3].Value = numPattern;
            parameters[4].Direction = ParameterDirection.Output;

            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("CheckNumberMask", parameters, out count);
            if (parameters[4].Value != null)
            {
                rtn = parameters[4].Value.ToString();
            }
            return rtn == "0";

        }
        #endregion

        #region Delete Function Checking
        public static bool isCanDeleteBrand(int brandID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.Campaign().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.Store().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount("ConditionID=" + brandID + "and ConditionType=1") > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount("ConditionID=" + brandID + "and ConditionType=1") > 0)
            {
                msg = "";
                return false;
            }
            return true;
        }

        public static bool isCanDeleteCardType(int cardTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCardGrade(int cardGradeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardGradeID=" + cardGradeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCouponType(int couponTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Coupon().GetCount("CouponTypeID=" + couponTypeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCouponNature(int couponNatureID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("CouponNatureID=" + couponNatureID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCampaign(int campaignID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("CampaignID=" + campaignID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteStore(int storeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount("ConditionID=" + storeID + "and ConditionType=3") > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount("ConditionID=" + storeID + "and ConditionType=3") > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteIndustry(int industryID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Brand().GetCountUnlimited("IndustryID=" + industryID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }


        public static bool isCanDeleteTelcoVariant(string SKU, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.ImportGM().GetCount("SKU='" + SKU +"'") > 0)
          
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteStoreNature(int storeNatureID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Store().GetCountUnlimited("StoreTypeID=" + storeNatureID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCurrency(int currencyID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited("CurrencyID=" + currencyID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("CurrencyID=" + currencyID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeletePasswordRuleSetting(int passwordRuleSettingID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("PasswordRuleID=" + passwordRuleSettingID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("PasswordRuleID=" + passwordRuleSettingID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        #endregion

        #region Master Edit Function Checking

        public static bool isCardTypeCreatedCard(int cardTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return true;
            }
            return false;
        }


        public static bool isCardTypeCreatedCardGrade(int cardTypeID, ref string msg)
        {
            msg = "";

            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }


        public static bool isCardGradeCreatedCard(int cardGradeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardGradeID=" + cardGradeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }

        public static bool isCouponTypeCreateCoupon(int couponTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Coupon().GetCount("CouponTypeID=" + couponTypeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }



        #endregion



        public static DataTable GetCouponViewDataTable(DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt.Copy());

            //Edge.Web.Tools.DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
            Edge.Web.Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Edge.Web.Tools.DataTool.AddCouponStatus(ds, "StatusName", "Status");
            //Tools.DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");

            return ds.Tables[0];
        }


        public static int GetPreviousCouponStatus(int orgStatus, string couponNumber)
        {
            string strQuery = "select Top 1 OrgStatus from Coupon_movement where CouponNumber = '" + couponNumber + "' and OrgStatus <> NewStatus  and NewStatus=" + orgStatus + " order by KeyID desc";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int status = Tools.ConvertTool.ToInt(ds.Tables[0].Rows[0]["OrgStatus"].ToString());
                return status;
            }
            else
            {
                return 0;
            }
        }


        public static string GetCouponTypeListByStoreIDBingding(int storeID, int storeConditionType)
        {
            string query = "SELECT distinct [CouponTypeID] FROM [CouponTypeStoreCondition_List] where storeid=" + storeID + " and StoreConditionType=" + storeConditionType;

            string strReturn = "";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CouponTypeID"] + ",";
                }
                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }
            else
            {
                return "-1";
            }
        }


        public static bool isHasCard(string cardNumber)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardNumber='" + cardNumber + "'");
            int count = new Edge.SVA.BLL.Card().GetCount(sbWhere.ToString());
            return count > 0;

        }

        public static string GetCouponTypeText(int couponTypeID)
        {
            SVA.Model.CouponType couponType = new SVA.BLL.CouponType().GetModel(couponTypeID);

            string text = couponType == null ? "" : DALTool.GetStringByCulture(couponType.CouponTypeName1, couponType.CouponTypeName2, couponType.CouponTypeName3);

            return couponType == null ? "" : ControlTool.GetDropdownListText(text, couponType.CouponTypeCode);
        }

        //Add by Len
        public static DataTable GetCardViewDataTable(DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt.Copy());

            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
            Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
            Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");

            return ds.Tables[0];
        }

        public static string GetSaleStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case -1: return "";
                        case 3: return "Wait For Payment";
                        case 4: return "Paid";
                        case 5: return "Complete";
                        case 6: return "Shipped";
                        case 8: return "Rejected";
                        case 9: return "Delayed";
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case -1: return "";
                        case 3: return "未确认支付";
                        case 4: return "已付款未提货";
                        case 5: return "交易完成";
                        case 6: return "交付运送";
                        case 8: return "拒收";
                        case 9: return "已延迟";
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case -1: return "";
                        case 3: return "未確認支付";
                        case 4: return "已付款未提貨";
                        case 5: return "交易完成";
                        case 6: return "交付運送";
                        case 8: return "拒收";
                        case 9: return "已延遲";
                    }
                    break;

            }
            return "";
        }

        public static string GetSaleTypeName(int typeid)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (typeid)
                    {
                        case -1: return "";
                        case 0: return "Normal Sales";
                        case 1: return "Delivery Sales";
                        case 2: return "Add Value";
                    }
                    break;
                case "zh-cn":
                    switch (typeid)
                    {
                        case -1: return "";
                        case 0: return "普通交易";
                        case 1: return "送货交易";
                        case 2: return "充值";
                    }
                    break;
                case "zh-hk":
                    switch (typeid)
                    {
                        case -1: return "";
                        case 0: return "普通交易";
                        case 1: return "送貨交易";
                        case 2: return "充值";
                    }
                    break;
            }
            return "";
        }

        public static string GetSalesName(string storecode)
        {
            SVA.BLL.Store bll = new SVA.BLL.Store();
            SVA.Model.Store store = bll.GetModelList("StoreCode='" + storecode + "'").Count == 0 ? null : bll.GetModelList("StoreCode='" + storecode + "'")[0];
            string text = store == null ? "" : DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3);
            return text;
        }

        public static string GetTendName(string TenderID)
        {
            SVA.BLL.TENDER bll = new SVA.BLL.TENDER();
            SVA.Model.TENDER tend = bll.GetModelList("TenderID='" + TenderID + "'").Count == 0 ? null : bll.GetModelList("TenderID='" + TenderID + "'")[0];
            string text = tend == null ? "" : DALTool.GetStringByCulture(tend.TenderName1, tend.TenderName2, tend.TenderName3);
            return text;
        }

        public static string GetColorName(string ColorID)
        {
            SVA.BLL.Color bll = new SVA.BLL.Color();
            SVA.Model.Color color = bll.GetModelList("ColorID='" + ColorID + "'").Count == 0 ? null : bll.GetModelList("ColorID='" + ColorID + "'")[0];
            string text = color == null ? "" : DALTool.GetStringByCulture(color.ColorName1, color.ColorName2, color.ColorName3);
            return text;
        }

        public static string GetProdSizeName(string ProductSizeID)
        {
            SVA.BLL.Product_Size bll = new SVA.BLL.Product_Size();
            SVA.Model.Product_Size prodsize = bll.GetModelList("ProductSizeID='" + ProductSizeID + "'").Count == 0 ? null : bll.GetModelList("ProductSizeID='" + ProductSizeID + "'")[0];
            string text = prodsize == null ? "" : DALTool.GetStringByCulture(prodsize.ProductSizeName1, prodsize.ProductSizeName2, prodsize.ProductSizeName3);
            return text;
        }

        public static string GetProdName(string ProdCode)
        {
            SVA.BLL.Product bll = new SVA.BLL.Product();
            SVA.Model.Product prod = bll.GetModelList("ProdCode='" + ProdCode + "'").Count == 0 ? null : bll.GetModelList("ProdCode='" + ProdCode + "'")[0];
            string text = prod == null ? "" : DALTool.GetStringByCulture(prod.ProdName1, prod.ProdName2, prod.ProdName3);
            return text;
        }

        public static string GetAddLogisticsProviderName(string LogisticsProviderID)
        {
            SVA.BLL.LogisticsProvider bll = new SVA.BLL.LogisticsProvider();
            SVA.Model.LogisticsProvider Provider = bll.GetModelList("LogisticsProviderID='" + LogisticsProviderID + "'").Count == 0 ? null : bll.GetModelList("LogisticsProviderID='" + LogisticsProviderID + "'")[0];
            string text = Provider == null ? "" : DALTool.GetStringByCulture(Provider.ProviderName1, Provider.ProviderName2, Provider.ProviderName3);
            return text;
        }

        public static string GetExchangeTypeName(string ExchangeTypeID)
        {
            string text = "";
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (ExchangeTypeID)
                    {
                        case "7": text = "Free Exchange"; break;
                        case "5": text = "Redemption Exchange"; break;
                        case "1": text = "Cash Exchange"; break;
                        case "2": text = "Point Exchange"; break;
                        case "4": text = "Coupon Exchange"; break;
                        case "3": text = "Combined Exchange"; break;
                    }
                    break;
                case "zh-cn":
                    switch (ExchangeTypeID)
                    {
                        case "7": text = "免费兑换"; break;
                        case "5": text = "消费金额兑换"; break;
                        case "1": text = "金额兑换"; break;
                        case "2": text = "积分兑换";break;
                        case "4": text = "优惠券兑换";break;
                        case "3": text = "组合兑换";break;
                    }
                    break;
                case "zh-hk":
                    switch (ExchangeTypeID)
                    {
                        case "7": text = "免費兌換"; break;
                        case "5": text = "消費金額兌換"; break;
                        case "1": text = "金額兌換"; break;
                        case "2": text = "積分兌換";break;
                        case "4": text = "積分兌換"; break;
                        case "3": text = "組合兌換"; break;
                    }
                    break;
                default:
                    switch (ExchangeTypeID)
                    {
                        case "7": text = "Free Exchange"; break;
                        case "5": text = "Redemption Exchange"; break;
                        case "1": text = "Cash Exchange"; break;
                        case "2": text = "Point Exchange"; break;
                        case "4": text = "Coupon Exchange"; break;
                        case "3": text = "Combined Exchange"; break;
                    }
                    break;
            }
            return text;
        }

        public static string GetMemberRangeName(string MemberRangeID)
        {
            string text = "";
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (MemberRangeID)
                    {
                        case "0": text = "All Members"; break;
                        case "3": text = "Birthday Member"; break;
                        case "1": text = "Birthday Month Member"; break;
                    }
                    break;
                case "zh-cn":
                    switch (MemberRangeID)
                    {
                        case "0": text = "所有会员"; break;
                        case "3": text = "当天生日的会员"; break;
                        case "1": text = "当月生日的会员"; break;
                    }
                    break;
                case "zh-hk":
                    switch (MemberRangeID)
                    {
                        case "0": text = "所有會員"; break;
                        case "3": text = "當天生日的會員"; break;
                        case "1": text = "當月生日的會員"; break;
                    }
                    break;
                default:
                    switch (MemberRangeID)
                    {
                        case "0": text = "All Members"; break;
                        case "3": text = "Birthday Member"; break;
                        case "1": text = "Birthday Month Member"; break;
                    }
                    break;
            }
            return text;
        }

        public static string GetConsumeRuleOperName(string ConsumeRuleOperID)
        {
            string text = "";
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (ConsumeRuleOperID)
                    {
                        case "0": text = "each full the require amount"; break;
                        case "1": text = "more than or equal to the require amount"; break;
                        case "2": text = "less than or equal to the require amount"; break;
                    }
                    break;
                case "zh-cn":
                    switch (ConsumeRuleOperID)
                    {
                        case "0": text = "每满所需的金额"; break;
                        case "1": text = "大于等于所需的金额"; break;
                        case "2": text = "少于等于所需的金额"; break;
                    }
                    break;
                case "zh-hk":
                    switch (ConsumeRuleOperID)
                    {
                        case "0": text = "每滿所需的金額"; break;
                        case "1": text = "大于等于所需的金額"; break;
                        case "2": text = "少于等于所需的金額"; break;
                    }
                    break;
                default:
                    switch (ConsumeRuleOperID)
                    {
                        case "0": text = "each full the require amount"; break;
                        case "1": text = "more than or equal to the require amount"; break;
                        case "2": text = "less than or equal to the require amount"; break;
                    }
                    break;
            }
            return text;
        }

        public static string GetSVARewardTypeName(string SVARewardTypeID)
        {
            string text = "";
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (SVARewardTypeID)
                    {
                        case "1": text = "Member Get Member"; break;
                        case "2": text = "New Member"; break;
                        case "3": text = "Facebook Share"; break;
                    }
                    break;
                case "zh-cn":
                    switch (SVARewardTypeID)
                    {
                        case "1": text = "会员推荐新会员"; break;
                        case "2": text = "新注册会员"; break;
                        case "3": text = "Facebook分享"; break;
                    }
                    break;
                case "zh-hk":
                    switch (SVARewardTypeID)
                    {
                        case "1": text = "會員推薦新會員"; break;
                        case "2": text = "新注冊會員"; break;
                        case "3": text = "Facebook分享"; break;
                    }
                    break;
                default:
                    switch (SVARewardTypeID)
                    {
                        case "1": text = "Member Get Member"; break;
                        case "2": text = "New Member"; break;
                        case "3": text = "Facebook Share"; break;
                    }
                    break;
            }
            return text;
        }

        public static string GetCurrencyCodeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Currency bll = new Edge.SVA.BLL.Currency();
            Edge.SVA.Model.Currency model = bll.GetModel(id);

            string result = model == null ? "" : model.CurrencyCode + "-" + GetStringByCulture(model.CurrencyName1, model.CurrencyName2, model.CurrencyName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetDepartName(string code)
        {
            Edge.SVA.BLL.Department bll = new Edge.SVA.BLL.Department();
            Edge.SVA.Model.Department model = bll.GetModel(code);
            string result = model == null ? "" : GetStringByCulture(model.DepartName1, model.DepartName2, model.DepartName3);
            return result;
        }

        public static string GetDistributionDesc(int id)
        {
            Edge.SVA.BLL.DistributeTemplate bll = new Edge.SVA.BLL.DistributeTemplate();
            Edge.SVA.Model.DistributeTemplate model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.DistributionDesc1, model.DistributionDesc2, model.DistributionDesc3);
            return result;
        }

        public static string GetPromotionTitle(int id)
        {
            Edge.SVA.BLL.PromotionMsg bll = new Edge.SVA.BLL.PromotionMsg();
            Edge.SVA.Model.PromotionMsg model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.PromotionTitle1, model.PromotionTitle2, model.PromotionTitle3);
            return result;
        }

        public static string GetPasswordRuleName(int id)
        {
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            Edge.SVA.Model.PasswordRule model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.Name1, model.Name2, model.Name3);
            return result;
        }

        public static string GetReasonDesc(int id)
        {
            Edge.SVA.BLL.Reason bll = new Edge.SVA.BLL.Reason();
            Edge.SVA.Model.Reason model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.ReasonDesc1, model.ReasonDesc2, model.ReasonDesc3);
            return result;
        }

        public static string GetStoreTypeName(int id)
        {
            Edge.SVA.BLL.StoreType bll = new Edge.SVA.BLL.StoreType();
            Edge.SVA.Model.StoreType model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.StoreTypeName1, model.StoreTypeName2, model.StoreTypeName3);
            return result;
        }

        public static string GetPhraseTitle(int id)
        {
            Edge.SVA.BLL.USEFULEXPRESSIONS bll = new Edge.SVA.BLL.USEFULEXPRESSIONS();
            Edge.SVA.Model.USEFULEXPRESSIONS model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.PhraseTitle1, model.PhraseTitle2, model.PhraseTitle3);
            return result;
        }

        public static string GetCustomerDesc(int id)
        {
            Edge.SVA.BLL.Customer bll = new Edge.SVA.BLL.Customer();
            Edge.SVA.Model.Customer model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.CustomerDesc1, model.CustomerDesc2, model.CustomerDesc3);
            return result;
        }

        public static string GetIndustryName(int id)
        {
            Edge.SVA.BLL.Industry bll = new Edge.SVA.BLL.Industry();
            Edge.SVA.Model.Industry model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.IndustryName1, model.IndustryName2, model.IndustryName3);
            return result;
        }

        public static string GetCampaignName(int id)
        {
            Edge.SVA.BLL.Campaign bll = new Edge.SVA.BLL.Campaign();
            Edge.SVA.Model.Campaign model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.CampaignName1, model.CampaignName2, model.CampaignName3);
            return result;
        }

        public static string GetCouponNatureName(int id)
        {
            Edge.SVA.BLL.CouponNature bll = new Edge.SVA.BLL.CouponNature();
            Edge.SVA.Model.CouponNature model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.CouponNatureName1, model.CouponNatureName2, model.CouponNatureName3);
            return result;
        }

        public static string GetOrganizationName(int id)
        {
            Edge.SVA.BLL.Organization bll = new Edge.SVA.BLL.Organization();
            Edge.SVA.Model.Organization model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.OrganizationName1, model.OrganizationName2, model.OrganizationName3);
            return result;
        }

        public static string GetEducationName(int id)
        {
            Edge.SVA.BLL.Education bll = new Edge.SVA.BLL.Education();
            Edge.SVA.Model.Education model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.EducationName1, model.EducationName2, model.EducationName3);
            return result;
        }

        public static string GetNationName(int id)
        {
            Edge.SVA.BLL.Nation bll = new Edge.SVA.BLL.Nation();
            Edge.SVA.Model.Nation model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.NationName1, model.NationName2, model.NationName3);
            return result;
        }

        public static string GetProfessionName(int id)
        {
            Edge.SVA.BLL.Profession bll = new Edge.SVA.BLL.Profession();
            Edge.SVA.Model.Profession model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.ProfessionName1, model.ProfessionName2, model.ProfessionName3);
            return result;
        }

        public static string GetSponsorName(int id)
        {
            Edge.SVA.BLL.Sponsor bll = new Edge.SVA.BLL.Sponsor();
            Edge.SVA.Model.Sponsor model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.SponsorName1, model.SponsorName2, model.SponsorName3);
            return result;
        }

        public static string GetSupplierDesc(int id)
        {
            Edge.SVA.BLL.Supplier bll = new Edge.SVA.BLL.Supplier();
            Edge.SVA.Model.Supplier model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.SupplierDesc1, model.SupplierDesc2, model.SupplierDesc3);
            return result;
        }

        public static string GetCompanyName(int id)
        {
            Edge.SVA.BLL.Company bll = new Edge.SVA.BLL.Company();
            Edge.SVA.Model.Company model = bll.GetModel(id);
            string result = model == null ? "" : model.CompanyName;
            return result;
        }

        public static string GetCompanyAddress(int id)
        {
            Edge.SVA.BLL.Company bll = new Edge.SVA.BLL.Company();
            Edge.SVA.Model.Company model = bll.GetModel(id);
            string result = model == null ? "" : model.CompanyAddress;
            return result;
        }

        public static string GetRedeemLimitTypeName(string RedeemLimitTypeID)
        {
            string text = "";
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (RedeemLimitTypeID)
                    {
                        case "1": text = "Total transaction amount is equal or more than the redeeming coupon condition value"; break;
                        case "2": text = "Card redeeming amount of consumption is equal or more than the redeeming coupon condition value"; break;
                        case "3": text = "Card point is equal or more than the redeeming coupon condition value"; break;
                        case "4": text = "In transaction, The specified redeeming product in the binding Coupon list, which total amount is equal or more than the redeeming coupon condition value"; break;
                        case "5": text = "In transaction, The specified redeeming product in the binding Coupon list, which total quantity is equal or more than the redeeming coupon condition value"; break;
                    }
                    break;
                case "zh-cn":
                    switch (RedeemLimitTypeID)
                    {
                        case "1": text = "整单交易总金额大于等于使用优惠劵的条件值"; break;
                        case "2": text = "会员卡累计消费金额大于等于使用优惠劵的条件值"; break;
                        case "3": text = "会员卡积分大于等于使用优惠劵的条件值"; break;
                        case "4": text = "交易单中，在优惠券绑定列表中指定的消费货品，总金额大于等于使用优惠劵的条件值"; break;
                        case "5": text = "交易单中，在优惠券绑定列表中指定的消费货品，总数量大于等于使用优惠劵的条件值"; break;
                    }
                    break;
                case "zh-hk":
                    switch (RedeemLimitTypeID)
                    {
                        case "1": text = "整單交易總金額大于等于使用優惠劵的條件值"; break;
                        case "2": text = "會員卡累計消費金額大于等于使用優惠劵的條件值"; break;
                        case "3": text = "會員卡積分大于等于使用優惠劵的條件值"; break;
                        case "4": text = "交易單中，在優惠券綁定列表中指定的消費貨品，總金額大于等于使用優惠劵的條件值"; break;
                        case "5": text = "交易單中，在優惠券綁定列表中指定的消費貨品，總數量大于等于使用優惠劵的條件值"; break;
                    }
                    break;
                default:
                    switch (RedeemLimitTypeID)
                    {
                        case "1": text = "Total transaction amount is equal or more than the redeeming coupon condition value"; break;
                        case "2": text = "Card redeeming amount of consumption is equal or more than the redeeming coupon condition value"; break;
                        case "3": text = "Card point is equal or more than the redeeming coupon condition value"; break;
                        case "4": text = "In transaction, The specified redeeming product in the binding Coupon list, which total amount is equal or more than the redeeming coupon condition value"; break;
                        case "5": text = "In transaction, The specified redeeming product in the binding Coupon list, which total quantity is equal or more than the redeeming coupon condition value"; break;
                    }
                    break;
            }
            return text;
        }

        public static string GetEntityTypeName(string EntityTypeID)
        {
            string text = "";
            switch (EntityTypeID)
            {
                case "-1": text = ""; break;
                case "0": text = "All Items"; break;
                case "1": text = "ProductBrandCode"; break;
                case "2": text = "DepartCode"; break;
                case "3": text = "ProdCode"; break;
                case "4": text = "CouponTypeCode"; break;
            }
            return text;
        }

        public static string GetStoreGroupName(int id)
        {
            Edge.SVA.BLL.StoreGroup bll = new Edge.SVA.BLL.StoreGroup();
            Edge.SVA.Model.StoreGroup model = bll.GetModel(id);
            string result = model == null ? "" : GetStringByCulture(model.StoreGroupName1, model.StoreGroupName2, model.StoreGroupName3);
            return result;
        }

        public static int GetSupplier(int id)
        {
            int supplierID = Convert.ToInt32(new Edge.SVA.BLL.CouponType().GetModel(id).SupplierID);
            return supplierID;
        }

        public static DataSet GetCouponTypeList(int supplierID)
        {
            return new Edge.SVA.BLL.CouponType().GetList(" SupplierID = " + supplierID);
        }

        public static List<Edge.SVA.Model.Supplier> GetAllSupplier()
        {
            Edge.SVA.BLL.Supplier bll = new Edge.SVA.BLL.Supplier();
            List<Edge.SVA.Model.Supplier> modelList = bll.GetModelList("");
            foreach (Edge.SVA.Model.Supplier model in modelList) 
            {
                model.SupplierDesc1 = model == null ? "" : GetStringByCulture(model.SupplierDesc1, model.SupplierDesc2, model.SupplierDesc3);
            }
            return modelList;
        }
        // Add by Alex 2014-07-04 ++
        public static int GetPreviousCardStatus(int orgStatus, string cardNumber)
        {
            string strQuery = "select Top 1 OrgStatus from Card_movement where CardNumber = '" + cardNumber + "' and OrgStatus <> NewStatus  and NewStatus=" + orgStatus + " order by KeyID desc";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int status = Tools.ConvertTool.ToInt(ds.Tables[0].Rows[0]["OrgStatus"].ToString());
                return status;
            }
            else
            {
                return 0;
            }
        }
        // Add by Alex 2014-07-04 --
        //Add by Alex 2014-07-02 ++
        public static bool IsHasStoreCodeAttributeCode(string storeID, int storeAttributeID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("StoreID ='" + storeID + "'");
            if (storeAttributeID > 0)
            {
                sbWhere.Append(" and SAID  = '" + storeAttributeID + "'");
            }

            int count = new Edge.SVA.BLL.StoreAttributeList().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }

        //add by Gavin @2015-02-05 
        public static string GetCardTypeListByStoreIDBingding(int storeID, int storeConditionType)
        {
            string query = "select distinct CardTypeID from CardGrade where CardGradeID in (SELECT distinct [CardGradeID] FROM [CardGradeStoreCondition_List] where StoreID=" + storeID + " and StoreConditionType=" + storeConditionType + ")";

            string strReturn = "";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CardTypeID"] + ",";
                }
                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }
            else
            {
                return "-1";
            }
        }

        //Add by Alex 2014-07-02 --
        /// Add by Nathan 20140626 ++
        /// <summary>
        /// 检查BrandCode是否存在，当brandID=0时是新增时用。
        /// </summary>
        /// <param name="tenderCode"></param>
        /// <param name="tenderID"></param>
        /// <returns></returns>
        public static bool isHasTenderCode(string tenderCode, int tenderID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" BrandCode='" + tenderCode + "' ");
            if (tenderID > 0)
            {
                sbWhere.Append(" and BrandID <> " + tenderID);
            }
            int count = new Edge.SVA.BLL.TENDER().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }
        /// Add by Nathan 20140626 --
        //Add by Nathan 2014061 ++
        public static string GetCardGradeListByStoreIDBingding(int storeID, int storeConditionType)
        {
            string query = "SELECT distinct [CardGradeID] FROM [CardGradeStoreCondition_List] where storeid=" + storeID + " and StoreConditionType=" + storeConditionType;

            string strReturn = "";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CardGradeID"] + ",";
                }
                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }
            else
            {
                return "-1";
            }
        }
        //Add by Nathan 20140610 --
        //Add by Nathan 20140630 ++
        public static bool isHasStoreAttributeCode(string storeAttributeCode, int storeAttributeID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("SACode='" + storeAttributeCode + "'");
            if (storeAttributeID > 0)
            {
                sbWhere.Append(" and SAID <> " + storeAttributeID);
            }

            int count = new Edge.SVA.BLL.Store_Attribute().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasStoreAttributeCodeWithStore(string storeAttributeCode, int storeAttributeID, string storeCode)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("SACode='" + storeAttributeCode + "' and StoreCode='" + storeCode + "'");
            if (storeAttributeID > 0)
            {
                sbWhere.Append(" and SAID <> " + storeAttributeID);
            }

            int count = new Edge.SVA.BLL.Store_Attribute().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }
        //Add by Nathan 20140630 --

        //Add by Nathan 20140702 ++
        public static bool isHasMemberClauseCode(string memberClauseCode, int memberClauseID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ClauseTypeCode='" + memberClauseCode + "'");
            if (memberClauseID > 0)
            {
                sbWhere.Append(" and MemberClauseID <> " + memberClauseID);
            }

            int count = new Edge.SVA.BLL.MemberClause().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }


        public static bool isHasMemberClauseCodeWithBrand(string memberClauseCode, int memberClauseID, string brandID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ClauseTypeCode='" + memberClauseCode + "' and BrandID='" + brandID + "'");
            if (memberClauseID > 0)
            {
                sbWhere.Append(" and MemberClauseID <> " + memberClauseID);
            }

            int count = new Edge.SVA.BLL.MemberClause().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }
        //Add by Nathan 20140702 --

        // Add by Robin 2014-08-27 for 获取补货规则中的数量
        public static int GetReplenishment(int StoreID, int CouponTypeID)
        {
            string strQuery = "select top 1 OrderRoundUpQty from CouponReplenishRule_D D inner join CouponReplenishRule_H H on D.CouponReplenishCode=H.CouponReplenishCode where GetDate() between  H.StartDate and H.EndDate and StoreID="+StoreID+" and H.CouponTypeID="+CouponTypeID+" order by UpdatedOn Desc";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int status = Tools.ConvertTool.ToInt(ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString());
                return status;
            }
            else
            {
                return 1;
            }
        }
        // End

        // Add by Robin 2014-09-19 for 按CouponNumber获取会员信息
        public static DataSet GetMemberByCoupon(string CouponNumber)
        {
            string strQuery = "select top 1 M.* from Coupon CP inner join  [Card] C on CP.CardNumber=C.CardNumber inner join Member M on C.MemberID=M.MemberID where CouponNumber='"+CouponNumber+"'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);
            return ds;
        }
        //End

        // Add by Robin 2014-10-10 for 按couponReceiveNumber获取Coupon收货单明细总数
        public static int GetCouponReceiveDetailCount(string couponReceiveNumber, string strWhere)
        {
            int rtn = 0;
            string strSQL = "";
            if (strWhere != "")
            {
                strSQL = "select count(*) totalrecords from Ord_CouponReceive_D where couponReceiveNumber='" + couponReceiveNumber + "' and"+ strWhere;
            }
            else
            {
                strSQL = "select count(*) totalrecords from Ord_CouponReceive_D where couponReceiveNumber='" + couponReceiveNumber + "'";
            }
            DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
            if (ds.Tables.Count > 0)
            {
                rtn = Int32.Parse(ds.Tables[0].Rows[0]["totalrecords"].ToString());
            }
            return rtn;
        }
        //End

        //add by Gavin
        /// <summary>
        /// 从CardUIDMap获得CardNumber
        /// </summary>
        /// <param name="id">CardUID</param>
        /// <returns></returns>
        public static string GetCardUIDByCardNumber(string strWhere)
        {
            DataSet ds = new Edge.SVA.BLL.CardUIDMap().GetList(strWhere);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0]["CardUID"]);
            }
            else
            {
                return "";
            }
        }

        public static string GetCardTypeStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 0: return Controllers.CardController.CardStatus.Dormant.ToString().ToUpper();
                        case 1: return Controllers.CardController.CardStatus.Issued.ToString().ToUpper();
                        case 2: return Controllers.CardController.CardStatus.Active.ToString().ToUpper();
                        case 3: return Controllers.CardController.CardStatus.Redeemed.ToString().ToUpper();
                        case 4: return Controllers.CardController.CardStatus.Expired.ToString().ToUpper();
                        case 5: return Controllers.CardController.CardStatus.Void.ToString().ToUpper();
                        case 6: return Controllers.CardController.CardStatus.Recycled.ToString().ToUpper();
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case 0: return "静止的";
                        case 1: return "已发行";
                        case 2: return "已激活";
                        case 3: return "已消费";
                        case 4: return "已过期";
                        case 5: return "已作废";
                        case 6: return "已回收";
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case 0: return "靜止的";
                        case 1: return "已發行";
                        case 2: return "已激活";
                        case 3: return "已消費";
                        case 4: return "已過期";
                        case 5: return "已作廢";
                        case 6: return "已回收";
                    }
                    break;
            }
            //switch (status)
            //{
            //    case 0: return Controllers.CardController.CardStatus.Dormant.ToString().ToUpper();
            //    case 1: return Controllers.CardController.CardStatus.Issued.ToString().ToUpper();
            //    case 2: return Controllers.CardController.CardStatus.Activated.ToString().ToUpper();
            //    case 3: return Controllers.CardController.CardStatus.Redeemed.ToString().ToUpper();
            //    case 4: return Controllers.CardController.CardStatus.Expired.ToString().ToUpper();
            //    case 5: return Controllers.CardController.CardStatus.Voided.ToString().ToUpper();
            //    case 6: return Controllers.CardController.CardStatus.Recycled.ToString().ToUpper();
            //}
            return "";
        }

        public static string GetCardStockStatusName(int status)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return Controllers.CardController.CardStockStatus.ForPrinting.ToString().ToUpper();
                        case 2: return Controllers.CardController.CardStockStatus.GoodForRelease.ToString().ToUpper();
                        case 3: return Controllers.CardController.CardStockStatus.Damaged.ToString().ToUpper();
                        case 4: return Controllers.CardController.CardStockStatus.Picked.ToString().ToUpper();
                        case 5: return Controllers.CardController.CardStockStatus.InTransit.ToString().ToUpper();
                        case 6: return Controllers.CardController.CardStockStatus.InLocation.ToString().ToUpper();
                        case 7: return Controllers.CardController.CardStockStatus.Returned.ToString().ToUpper();
                    }
                    break;
                case "zh-cn":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return "总部未确认收货";
                        case 2: return "总部确认收货";
                        case 3: return "总部发现有优惠券损坏";
                        case 4: return "总部收到了店铺的订单";
                        case 5: return "总部收到了店铺的订单并发货";
                        case 6: return "店铺确认收货";
                        case 7: return "店铺退货给总部";
                    }
                    break;
                case "zh-hk":
                    switch (status)
                    {
                        case 0: return "";
                        case 1: return "總部未確認收貨";
                        case 2: return "總部確認收貨";
                        case 3: return "總部發現有優惠劵損壞";
                        case 4: return "總部收到了店舖的訂單";
                        case 5: return "總部收到了店舖的訂單並發貨";
                        case 6: return "店舖確認收貨";
                        case 7: return "店舖退貨給總部";
                    }
                    break;
            }
            return "";
        }

        /// <summary>
        /// 根据优惠劵ID，获取优惠劵库存状态
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardStockStatusByID(string id, Dictionary<string, string> cache)
        {
            if (id == string.Empty) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
            Edge.SVA.Model.Card model = bll.GetModel(id);

            string result = model == null ? "" : Convert.ToString(model.StockStatus);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static int GetReplenishment(int StoreID, int typeOrGradeID, int mediaType)
        {
            string typeOrGrade = "";
            if (mediaType == 1)
            {
                typeOrGrade = "CouponTypeID";
            }
            else 
            {
                typeOrGrade = "CardGradeID";
            }

            string strQuery = "select top 1 OrderRoundUpQty from InventoryReplenishRule_D D inner join InventoryReplenishRule_H H on D.InventoryReplenishCode=H.InventoryReplenishCode where GetDate() between  H.StartDate and H.EndDate and StoreID=" + StoreID + " and H." + typeOrGrade + "=" + typeOrGradeID + " order by UpdatedOn Desc";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int status = Tools.ConvertTool.ToInt(ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString());
                return status;
            }
            else
            {
                return 1;
            }
        }
        //End

        // Add by Robin 2014-11-07 for 按cardReceiveNumber获取Card收货单明细总数
        public static int GetCardReceiveDetailCount(string cardReceiveNumber, string strWhere)
        {
            int rtn = 0;
            string strSQL = "";
            if (strWhere != "")
            {
                strSQL = "select count(*) totalrecords from Ord_CardReceive_D where cardReceiveNumber='" + cardReceiveNumber + "' and" + strWhere;
            }
            else
            {
                strSQL = "select count(*) totalrecords from Ord_CardReceive_D where cardReceiveNumber='" + cardReceiveNumber + "'";
            }
            DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
            if (ds.Tables.Count > 0)
            {
                rtn = Int32.Parse(ds.Tables[0].Rows[0]["totalrecords"].ToString());
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据DayFlagCode获取DayFlagID
        public static int GetDayFlagID(string DayFlagCode)
        {
            int rtn = 0;
            string strSQL = "";
            if (DayFlagCode != "")
            {
                strSQL = "select DayFlagID from DayFlag where DayFlagCode='" + DayFlagCode + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = Int32.Parse(ds.Tables[0].Rows[0]["DayFlagID"].ToString());
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据DayFlagID获取DayFlagCode
        public static string GetDayFlagCode(string DayFlagID)
        {
            string rtn = "";
            string strSQL = "";
            if (DayFlagID != "")
            {
                strSQL = "select DayFlagCode from DayFlag where DayFlagID='" + DayFlagID + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = ds.Tables[0].Rows[0]["DayFlagCode"].ToString();
                }
                else
                {
                    rtn = "";
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据WeekFlagCode获取WeekFlagID
        public static int GetWeekFlagID(string WeekFlagCode)
        {
            int rtn = 0;
            string strSQL = "";
            if (WeekFlagCode != "")
            {
                strSQL = "select WeekFlagID from WeekFlag where WeekFlagCode='" + WeekFlagCode + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = Int32.Parse(ds.Tables[0].Rows[0]["WeekFlagID"].ToString());
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据WeekFlagID获取WeekFlagCode
        public static string GetWeekFlagCode(string WeekFlagID)
        {
            string rtn = "";
            string strSQL = "";
            if (WeekFlagID != "")
            {
                strSQL = "select WeekFlagCode from WeekFlag where WeekFlagID='" + WeekFlagID + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = ds.Tables[0].Rows[0]["WeekFlagCode"].ToString();
                }
                else
                {
                    rtn = "";
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据MonthFlagCode获取MonthFlagID
        public static int GetMonthFlagID(string MonthFlagCode)
        {
            int rtn = 0;
            string strSQL = "";
            if (MonthFlagCode != "")
            {
                strSQL = "select MonthFlagID from MonthFlag where MonthFlagCode='" + MonthFlagCode + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = Int32.Parse(ds.Tables[0].Rows[0]["MonthFlagID"].ToString());
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2014-12-11 for 根据MonthFlagID获取MonthFlagCode
        public static string GetMonthFlagCode(string MonthFlagID)
        {
            string rtn = "";
            string strSQL = "";
            if (MonthFlagID != "")
            {
                strSQL = "select MonthFlagCode from MonthFlag where MonthFlagID='" + MonthFlagID + "'";
                DataSet ds = DBUtility.DbHelperSQL.Query(strSQL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rtn = ds.Tables[0].Rows[0]["MonthFlagCode"].ToString();
                }
                else
                {
                    rtn = "";
                }
            }
            return rtn;
        }
        //End

        //Add By Robin 2015-01-07 判断是否有重复CouponNatureID
        public static bool IsHasCouponTypeCodeNatureCode(string couponTypeID, int couponNatureID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CouponTypeID ='" + couponTypeID + "'");
            if (couponNatureID > 0)
            {
                sbWhere.Append(" and CouponNatureID  = '" + couponNatureID + "'");
            }

            int count = new Edge.SVA.BLL.CouponNatureList().GetRecordCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// checker function for SKU code
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static bool isHasSKUCode(string SKUCode, int SkuID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("UPC='" + SKUCode + "'");
            if (SkuID > 0)
            {
                sbWhere.Append(" and UPC <> " + SkuID);
            }

            int count = new Edge.SVA.BLL.ImportGM().GetCount(sbWhere.ToString());
            return count > 0;
        }
    }
}
