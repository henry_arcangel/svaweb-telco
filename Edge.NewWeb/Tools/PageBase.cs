﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineUI;
using System.Threading;
using System.Text;
using System.IO;
using System.Globalization;
using Edge.Common;
using Asp.Net.WebFormLib;
using Edge.Security.Manager;
using System.Web.Security;
using Edge.Web.Admin;
using Edge.Messages.Manager;
using Edge.Web.Tools;
using System.Web.UI;

namespace Edge.Web
{
    public class PageBase : WebPageBase
    {
        protected Edge.Web.Tools.MessageNotifyUtil messageNotifyUtil = Edge.Web.Tools.MessageNotifyUtil.GetSingleton();
        protected override void OnInit(EventArgs e)
        {
            if (Context != null && Context.Session != null)
            {
                base.OnInit(e);

                CheckRight();
                
                if (!IsPostBack)
                {
                    if (PageManager.Instance != null)
                    {
                        HttpCookie themeCookie = Request.Cookies["Theme"];
                        if (themeCookie != null)
                        {
                            string themeValue = themeCookie.Value;

                            if (IsSystemTheme(themeValue))
                            {
                                PageManager.Instance.Theme = (Theme)Enum.Parse(typeof(Theme), themeValue, true);
                            }
                            else
                            {
                                PageManager.Instance.CustomTheme = themeValue;
                            }
                        }

                        HttpCookie langCookie = Request.Cookies["Language"];
                        if (langCookie != null)
                        {
                            string langValue = langCookie.Value;
                            PageManager.Instance.Language = (Language)Enum.Parse(typeof(Language), langValue, true);
                        }
                    }
                }
            }


        }

        private bool IsSystemTheme(string themeName)
        {
            string[] themes = Enum.GetNames(typeof(Theme));
            foreach (string theme in themes)
            {
                if (theme.ToLower() == themeName)
                {
                    return true;
                }
            }
            return false;
        }

        protected internal Edge.Security.Model.WebSet webset;

        public PageBase()
        {
            //this.Load += new EventHandler(ManagePage_Load);
            webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Header.DataBind();
        }

        protected override void InitializeCulture()
        {
            if (SVASessionInfo.SiteLanguage != null)
            {
                string lan = SVASessionInfo.SiteLanguage.ToString().ToLower();
                UICulture = lan;
                Culture = lan;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lan);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lan);
            }
            base.InitializeCulture();
        }

        //private void CheckLogin()
        //{
        //    if (this is Edge.Web.Admin.Login)
        //    {
        //        SVASessionInfo.CurrentUser = null;
        //    }
        //    else
        //    {
        //        if (!Context.User.Identity.IsAuthenticated)
        //        {
        //            FormsAuthentication.SignOut();
        //            Session.Clear();
        //            Session.Abandon();
        //            Response.Clear();

        //            string url = null;
        //            if (Request.ApplicationPath == "/")
        //            {
        //                //不存在虚拟目录
        //                url = Request.ApplicationPath + "Login.aspx";
        //            }
        //            else
        //            {
        //                //存在虚拟目录
        //                url = Request.ApplicationPath + "/Login.aspx";
        //            }
        //            FineUI.Alert.ShowInTop(Resources.MessageTips.Timeout, "", FineUI.MessageBoxIcon.Warning, "top.location='" + url + "';");
        //            //Response.Write("<script defer> window.alert('" + Resources.MessageTips.Timeout + "');parent.location='" + url + "';</script>");
        //            //Response.End();
        //        }

        //    }
        //}

        #region 提示窗
        /// <summary>
        /// 添加编辑删除提示(Url = back) 后退
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrint(string msg, string url, string title)
        {
            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\"{0}\",\"{1}\");", msg, title);
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "ShowjAlert", msbox.ToString());
        }

        /// 添加编辑删除提示并且关闭
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrintAndClose(string msg, string url, string title)
        {

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\"{0}\",\"{1}\");", msg, title);
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("window.top.tb_remove();");
            msbox.Append("window.top.$('#TB_load').remove();");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JscriptPrintAndClose", msbox.ToString());
        }

        /// 添加编辑删除提示并且聚焦控件
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrintAndFocus(string msg, string url, string title, string clientID)
        {

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\" {0}\",\"{1}\",function(){{", msg, title);                      //Format{->{{   } ->}}
            msbox.AppendFormat("if($(\"#{0}\").length > 0){{$(\"#{0}\").focus();}}}}); ", clientID);             //Format{->{{   } ->}}
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JscriptPrintAndFocus", msbox.ToString());
        }

        protected void CloseLoading()
        {
            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.Append("window.top.tb_remove();");
            msbox.Append("window.top.$('#TB_load').remove();");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "CloseLoading", msbox.ToString());
        }

        protected void AnimateRoll(string id)
        {
            id = id.StartsWith("#") ? id : "#" + id;

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("if($('{0}').length > 0 ) {{ ", id);
            msbox.AppendFormat("$('html,body').animate({{scrollTop:$('{0}').offset().top}}, 800)}}", id);
            msbox.Append("</script>");

            ClientScript.RegisterStartupScript(Page.GetType(), "AnimateRoll", msbox.ToString());
        }
        #endregion

        #region 备用方法
        ///// <summary>
        ///// 组合URL语句
        ///// </summary>
        ///// <param name="_classId">类别ID</param>
        ///// <param name="_keywords">关健字</param>
        ///// <returns></returns>
        protected string CombUrlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append("keywords=" + HttpContext.Current.Server.UrlEncode(_keywords) + "&");
            }

            return strTemp.ToString();
        }

        /// <summary>
        /// 删除单个文件
        /// </summary>
        /// <param name="_filepath">文件相对路径</param>
        protected void DeleteFile(string _filepath)
        {
            if (string.IsNullOrEmpty(_filepath))
            {
                return;
            }
            string fullpath = Edge.Common.Utils.GetMapPath(_filepath);
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }

        /// <summary>
        /// 生成缩略图的方法
        /// </summary>
        /// <param name="_filepath">文件相对路径</param>
        /// <returns></returns>
        protected string MakeThumbnail(string _filepath)
        {
            if (!string.IsNullOrEmpty(_filepath) && webset.IsThumbnail == 1)
            {
                string _filename = _filepath.Substring(_filepath.LastIndexOf("/") + 1);
                string _newpath = webset.WebFilePath;
                //检查保存的路径 是否有/开头结尾
                if (_newpath.StartsWith("/") == false)
                {
                    _newpath = "/" + _newpath;
                }
                if (_newpath.EndsWith("/") == false)
                {
                    _newpath = _newpath + "/";
                }
                _newpath = _newpath + "Thumbnail/";

                //检查是否有该路径没有就创建
                if (!Directory.Exists(Edge.Common.Utils.GetMapPath(_newpath)))
                {
                    Directory.CreateDirectory(Edge.Common.Utils.GetMapPath(_newpath));
                }
                //调用生成类方法
                ImageThumbnailMake.MakeThumbnail(_filepath, _newpath + _filename, webset.ProWidth, webset.ProHight, "Cut");

                return _newpath + _filename;
            }

            return _filepath;
        }

        /// <summary>
        /// 刷新整个页面
        /// </summary>
        protected void RefreshParentPage()
        {
            Response.Write("<script language=javascript>self.parent.location.reload();  </script>");
        }

        /// <summary>
        /// 父页面跳转页面
        /// </summary>
        protected void RedirectParentPageTo(string url)
        {
            Response.Write("<script language=javascript>self.parent.location='" + url + "';  </script>");
        }

        /// <summary>
        /// 刷新当前页面
        /// </summary>
        protected void RefreshPage()
        {
            Response.Redirect(Request.Url.ToString());
        }

        /// <summary>
        /// 日志写入方法
        /// </summary>
        /// <param name="str"></param>
        protected void SaveLogs(string str, string Particular)
        {
            if (webset.WebLogStatus == 0)
            {
                return;
            }
            Edge.Security.Manager.SysManage bll = new Edge.Security.Manager.SysManage();
            bll.AddLog(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), str, Particular);
        }

        protected static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' ||
                relativeUrl[0] == '\\') return relativeUrl;

            int idxOfScheme =
              relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }
        #endregion

        private static Dictionary<string, Edge.Security.Model.SysNode> nodeCache = null;
        public Dictionary<string, Edge.Security.Model.SysNode> NodeCache
        {
            get
            {
                if (nodeCache == null)
                {
                    nodeCache = new Dictionary<string, Edge.Security.Model.SysNode>();
                }
                return nodeCache;
            }
        }

        public string PageName
        {
            get
            {
                string path = System.Web.HttpContext.Current.Request.Path;

                path = path.Remove(0, System.Web.HttpContext.Current.Request.ApplicationPath.Length);
                if (path.StartsWith("/")) path = path.Remove(0, 1);

                string lan = Thread.CurrentThread.CurrentCulture.Name.ToLower();
                string key = string.Format("{0}_{1}", path, lan);

                lock (typeof(PageBase))
                {
                    if (NodeCache.ContainsKey(key)) return NodeCache[key].Text;

                    Edge.Security.Manager.SysManage manage = new Edge.Security.Manager.SysManage();
                    Edge.Security.Model.SysNode node = manage.GetNodeByUrl(path, lan);
                    if (node == null) return "";

                    NodeCache.Add(key, node);

                    return node.Text;
                }
            }
        }

        #region JS和CS引用方法
        protected string RemainVal(string val)
        {
            return "Translate__Remain_121_Start" + val + "Translate__Remain_121_End";
        }
        protected string GetJSMultiLanguagePath()
        {
            string lan = Thread.CurrentThread.CurrentUICulture.Name.ToLower();
            string path = "~/js/" + lan + "/messages.js";
            return ResolveUrl(path);
        }

        protected string GetJSFunctionPath()
        {
            //string path = "~/js/function.min.js";
            string path = "~/js/function.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryPath()
        {
            string path = "~/js/jquery-1.4.1.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryUiPath()
        {
            string path = "~/js/jquery-ui-1.8.21.custom.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryUiCssPath()
        {
            string path = "~/js/css/redmond/jquery-ui-1.8.21.custom.css";
            return ResolveUrl(path);
        }

        protected string GetjQueryValidatePath()
        {
            string path = "~/js/jquery.validate.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryFormPath()
        {
            string path = "~/js/jquery.form.js";
            return ResolveUrl(path);
        }

        protected string GetPaginationCssPath()
        {
            string path = "~/Style/pagination.css";
            return ResolveUrl(path);
        }

        protected string GetJSPaginationPath()
        {
            string path = "~/js/jquery.pagination.js";
            return ResolveUrl(path);
        }

        protected string GetJSThickBoxPath()
        {
            string path = "~/js/thickbox.js";
            return ResolveUrl(path);
        }

        protected string GetJSThickBoxCssPath()
        {
            string path = "~/Style/thickbox.css";
            return ResolveUrl(path);
        }

        protected string GetMy97DatePickerPath()
        {
            string path = "~/My97DatePicker/WdatePicker.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxJqueryPath()
        {
            string path = "~/js/jquery-1.5.2.min.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxCommonJsPath()
        {
            string path = "~/js/CommonJs.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxDynamicConJsPath()
        {
            string path = "~/js/dynamicCon.js";
            return ResolveUrl(path);
        }

        protected string GetjQeruyAlertsStylePath()
        {
            string path = "~/js/jQeruyAlerts/jquery.alerts.css";
            return ResolveUrl(path);
        }

        protected string GetjQeruyAlertsPath()
        {
            string path = "~/js/jQeruyAlerts/jquery.alerts.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigeruiCSS()
        {
            string path = "~/js/ui/skins/Aqua/css/ligerui-all.css";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigerui()
        {
            string path = "~/js/ui/js/ligerui.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigeruiBase()
        {
            string path = "~/js/ui/js/core/base.js";
            return ResolveUrl(path);
        }
        #endregion

        #region for coupon change status
        protected static string GetCouponSearchStrWhere(int top, int batchCouponID, string couponNumber, string couponTypeID, string couponUID, string strWhere)
        {
            if (batchCouponID > 0)
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Coupon.BatchCouponID =" + batchCouponID;
                }
                else
                {
                    strWhere += " and Coupon.BatchCouponID =" + batchCouponID;
                }
            }
            if (!string.IsNullOrEmpty(couponTypeID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Coupon.CouponTypeID in (" + couponTypeID + ")";
                }
                else
                {
                    strWhere += " and Coupon.CouponTypeID in (" + couponTypeID + ")";
                }
            }

            if (!string.IsNullOrEmpty(couponUID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += string.Format("Coupon.CouponNumber in (select CouponNumber from CouponUIDMap where CouponUID =  '{0}') ", couponUID);

                }
                else
                {
                    strWhere += string.Format("and Coupon.CouponNumber in (select CouponNumber from CouponUIDMap where CouponUID =  '{0}') ", couponUID);
                }
            }


            if (!string.IsNullOrEmpty(couponNumber))
            {
                //if (string.IsNullOrEmpty(strWhere))
                //{
                //    if (top > 0)
                //    {
                //        strWhere += " Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                //    }
                //    else
                //    {
                //        strWhere += " Coupon.CouponNumber = '" + couponNumber + "'";
                //    }
                //}
                //else
                //{
                //    if (top > 0)
                //    {
                //        strWhere += " and Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                //    }
                //    else
                //    {
                //        strWhere += " and Coupon.CouponNumber = '" + couponNumber + "'"; ;
                //    }
                //}

                //不需要补齐GC的逻辑
                Edge.SVA.BLL.CouponType bll = new SVA.BLL.CouponType();
                Edge.SVA.Model.CouponType model = bll.GetModel(Convert.ToInt32(couponTypeID));
                string startcouponnumber = couponNumber;
                string endcouponnumber = couponNumber;
                if (model.CouponCheckdigit == true)
                {
                    startcouponnumber = couponNumber.Substring(0, couponNumber.Length - 1);
                    endcouponnumber = (Convert.ToInt64(couponNumber.Substring(0, couponNumber.Length - 1)) + (top - 1)).ToString() + "9";
                }
                else
                {
                    endcouponnumber = (Convert.ToInt64(couponNumber) + (top - 1)).ToString();
                }
                if (string.IsNullOrEmpty(strWhere))
                {
                    if (top > 0)
                    {
                        strWhere += " Coupon.CouponNumber >='" + startcouponnumber + "' and Coupon.CouponNumber<='" + endcouponnumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                    }
                    else
                    {
                        strWhere += " Coupon.CouponNumber = '" + couponNumber + "'";
                    }
                }
                else
                {
                    if (top > 0)
                    {
                        strWhere += " and Coupon.CouponNumber >='" + startcouponnumber + "' and Coupon.CouponNumber<='" + endcouponnumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                    }
                    else
                    {
                        strWhere += " and Coupon.CouponNumber = '" + couponNumber + "'"; ;
                    }
                }
            }


            return strWhere;
        }
        #endregion

        #region 提示方法
        /// <summary>
        //关闭本窗体，然后刷新父窗体
        /// </summary>
        protected void CloseCurrForm()
        {
            PageContext.RegisterStartupScript(ActiveWindow.GetHideReference());
        }        /// <summary>
        //关闭本窗体，然后刷新父窗体
        /// </summary>
        protected void CloseAndRefresh()
        {
            PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
        }

        /// <summary>
        //关闭本窗体，然后Posback父窗体
        /// </summary>
        protected void CloseAndPostBack()
        {
            PageContext.RegisterStartupScript(ActiveWindow.GetHidePostBackReference());
        }

        protected void ShowError(string msg)
        {
            Alert.ShowInTop(msg, MessageBoxIcon.Error);
        }
        /// <summary>
        //警告式提示
        /// </summary>
        protected void ShowWarning(string msg)
        {
            Alert.ShowInTop(msg, MessageBoxIcon.Warning);
        }

        /// <summary>
        //警告式提示
        /// </summary>
        protected void ShowWarningAndClose(string msg)
        {
            Alert.ShowInTop(msg, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
        }

        /// <summary>
        //更新失败提示
        /// </summary>
        protected void ShowUpdateFailed()
        {
            Alert.ShowInTop(Resources.MessageTips.UpdateFailed, MessageBoxIcon.Error);
        }

        /// <summary>
        //新增失败提示
        /// </summary>
        protected void ShowAddFailed()
        {
            Alert.ShowInTop(Resources.MessageTips.AddFailed, MessageBoxIcon.Error);
        }
        

        /// <summary>
        //新增失败提示自定义消息
        /// </summary>
        protected void ShowSaveFailed(string strmessage)
        {
            Alert.ShowInTop(strmessage, MessageBoxIcon.Error);
        }
        #endregion


        #region 图片
        protected string GetServerPath(string subPath)
        {
            string basePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/");
            return basePath + subPath;
        }
        #endregion


        #region 检查权限
        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            SetButton();
        }
        protected bool hasRight = true;
        private int PermissionID = -1;
        protected void CheckRight()
        {
            if (this is Login)
            {
                SVASessionInfo.CurrentUser = null;
            }
            else
            {
                if (!Context.User.Identity.IsAuthenticated)
                {
                    Logger.Instance.WriteOperationLog("pagebase", "IsAuthenticated is false");
                }
                if (string.IsNullOrEmpty(Context.User.Identity.Name))
                {
                    Logger.Instance.WriteOperationLog("pagebase", "Identity Name is empty");
                }
                if (SVASessionInfo.CurrentUser.UserName.Length<=0)
                {
                    Logger.Instance.WriteOperationLog("pagebase", "SVASessionInfo.CurrentUser.UserName is empty");
                }
                if (Context.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(Context.User.Identity.Name) && SVASessionInfo.CurrentUser.UserName.Length >=1)
                {
                    //Modified By Robin 2014-08-19 For 兼容Domain用户登录
                    //string name = Context.User.Identity.Name;
                    string name = SVASessionInfo.CurrentUser.UserName;
                    //End
                    Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));
                    AccountsPrincipal user = new AccountsPrincipal(name, SVASessionInfo.SiteLanguage);//todo: 修改成多语言。
                    //if (SVASessionInfo.CurrentUser.UserName.Length==0)
                    //{
                    //    Edge.Security.Manager.User currentUser = new Edge.Security.Manager.User(user);
                    //    SVASessionInfo.CurrentUser = currentUser;
                    //    SVASessionInfo.UserStyle = currentUser.Style;
                    //    Response.Write("<script defer>location.reload();</script>");
                    //}
                    PermissionID = PermissionMapper.GetSingleton().GetPermissionIDfromURL(Request.Path);
                    if ((PermissionID != -1) && (!user.HasPermissionID(PermissionID)))
                    {
                        hasRight = false;

                        FineUI.Alert.ShowInTop(Resources.MessageTips.NotPermission, "", FineUI.MessageBoxIcon.Warning, FineUI.ActiveWindow.GetHideRefreshReference());
                    }

                }
                else
                {
                    hasRight = false;

                    FormsAuthentication.SignOut();
                    Session.Clear();
                    Session.Abandon();
                    Response.Clear();

                    string url = null;
                    if (Request.ApplicationPath == "/")
                    {
                        //不存在虚拟目录
                        url = Request.ApplicationPath + "Login.aspx";
                    }
                    else
                    {
                        //存在虚拟目录
                        url = Request.ApplicationPath + "/Login.aspx";
                    }
                    FineUI.Alert.ShowInTop(Resources.MessageTips.Timeout, "", FineUI.MessageBoxIcon.Warning, "top.location='" + url + "';");
                }
            }
        }
        #endregion

        protected virtual void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {

        }

        #region Operation
        protected void ApproveTxns(string ids, string okScript, string cancelScript)
        {
            //string msg = FineUI.Confirm.GetShowReference(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + ids, Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
            //FineUI.PageContext.RegisterStartupScript(msg);

            ShowConfirmDialog(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + ids, Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
        protected void VoidTxns(string ids, string okScript, string cancelScript)
        {
            //string msg = FineUI.Confirm.GetShowReference(MessagesTool.instance.GetMessage("10018") + "\n TXN NO.: \n" + ids, Resources.MessageTips.Void, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
            //FineUI.PageContext.RegisterStartupScript(msg);
            ShowConfirmDialog(MessagesTool.instance.GetMessage("10018") + "\n TXN NO.: \n" + ids, Resources.MessageTips.Void, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
        protected void NewApproveTxns(FineUI.Grid Grid, FineUI.Window Window)
        {            
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid.SelectedRowIndexArray)
            {
                sb.Append(Grid.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window.Title = Resources.MessageTips.Approve;
            string okScript = Window.GetShowReference("Approve.aspx?ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
        protected void NewVoidTxns(FineUI.Grid Grid, FineUI.Window Window)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid.SelectedRowIndexArray)
            {
                sb.Append(Grid.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window.Title = Resources.MessageTips.Void;
            string okScript = Window.GetShowReference("Void.aspx?ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog(MessagesTool.instance.GetMessage("10018") + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), Resources.MessageTips.Void, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
        #endregion
        #region Alert
        #endregion

       
        #region Basic Confirm
        public void ShowConfirmDialog(string msg,string title,MessageBoxIcon icon,string okScript,string cancelScript)
        {
            FineUI.PageContext.RegisterStartupScript(Confirm.GetShowReference(msg, title, icon, okScript, cancelScript, Target.Top));
        }
        #endregion
        #region Dec Basic Confirm
        public void AreYouSure(string okScript, string cancelScript)
        {
            ShowConfirmDialog("Are you sure?", "System Info", MessageBoxIcon.Question, okScript, cancelScript);
        }
        #endregion


        #region Public Button Event
        public void ExecuteJS(string jsStr)
        {
            PageContext.RegisterStartupScript(jsStr);
        }
        protected void RegisterCloseEvent(FineUI.Button btn)
        {
            btn.OnClientClick = ActiveWindow.GetHideReference();
        }
        protected virtual void RegisterGrid_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid grid = sender as Grid;
            if (grid != null)
            {
                grid.PageIndex = e.NewPageIndex;
            }
        }
        #endregion

        #region Textbox转换为正 Int格式
        protected virtual void ConvertTextboxToInt(object sender, EventArgs e)
        {
            FineUI.TextBox tf = sender as FineUI.TextBox;
            if (tf != null)
            {
                int dec;
                if (int.TryParse(tf.Text.Trim(), out dec))
                {
                    if (dec >= 0 && dec <= 100000000)
                    {
                        tf.ClearInvalid();
                        tf.Text = dec.ToString();
                    }
                    else
                    {
                        tf.MarkInvalid(tf.RegexMessage);
                    }
                }
                else
                {
                    tf.MarkInvalid(tf.RegexMessage);
                }
            }
        }
        #endregion

        #region Textbox转换为正 Decimal格式
        protected virtual void ConvertTextboxToDecimal(object sender, EventArgs e)
        {
            FineUI.TextBox tf = sender as FineUI.TextBox;
            if (tf != null)
            {
                decimal dec;
                if (decimal.TryParse(tf.Text.Trim(), out dec))
                {
                    if (dec >= 0 && dec <= 100000000)
                    {
                        tf.ClearInvalid();
                        tf.Text = String.Format("{0:F}", dec);
                    }
                    else
                    {
                        tf.MarkInvalid(tf.RegexMessage);
                    }
                }
                else
                {
                    tf.MarkInvalid(tf.RegexMessage);
                }
            }
        }
        #endregion

        #region ValidateForm Rules

        protected virtual bool CheckAndConvertTextboxToDecimal(FineUI.TextBox tf)
        {
            decimal dec;
            if (decimal.TryParse(tf.Text.Trim(), out dec))
            {
                if (dec >= 0 && dec <= 100000000)
                {
                    tf.ClearInvalid();
                    tf.Text = String.Format("{0:F}", dec);
                    return true;
                }
                else
                {
                    tf.MarkInvalid(tf.RegexMessage);
                    return false;
                }
            }
            else
            {
                tf.MarkInvalid(tf.RegexMessage);
                return false;
            }
        }
        protected virtual bool CheckAndConvertTextboxListToDecimal(List<FineUI.TextBox> tfs)
        {
            bool rtn = true;
            foreach (var item in tfs)
            {
                if (!CheckAndConvertTextboxToDecimal(item))
                {
                    rtn = false;
                }
            }
            return rtn;
        }
        #endregion

        public void InitWindowSize(Window window)
        {
            int width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            window.Width = width-300;
        }

        public void SetGirdSelectAll(Grid grid, bool check)
        {
            //if (check)
            //{
            //    int len = grid.PageSize;
            //    int[] indexs = new int[len];
            //    for (int i = 0; i < len; i++)
            //    {
            //        indexs[i] = i;
            //    }
            //    grid.SelectedRowIndexArray = indexs;
            //}
            //else
            //{
            //    grid.SelectedRowIndexArray = new int[] { };
            //}
            grid.Enabled = !check;
        }

        /// <summary>
        /// 清空列表记录
        /// </summary>
        /// <param name="grid"></param>
        public void ClearGird(Grid grid)
        {
            grid.RecordCount = 0;
            grid.PageIndex = 0;
            grid.DataSource = null;
            grid.DataBind();
        }

        #region For Card
        //Add by Nathan 20140611
        protected static string GetCardSearchStrWhere(int top, int batchCardID, string cardNumber, string cardGradeID, string cardUID, string strWhere)
        {
            if (batchCardID > 0)
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Card.BatchCardID =" + batchCardID;
                }
                else
                {
                    strWhere += " and Card.BatchCardID =" + batchCardID;
                }
            }
            if (!string.IsNullOrEmpty(cardGradeID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Card.CardGradeID in (" + cardGradeID + ")";
                }
                else
                {
                    strWhere += " and Card.CardGradeID in (" + cardGradeID + ")";
                }
            }

            if (!string.IsNullOrEmpty(cardUID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += string.Format("Card.CardNumber in (select CardNumber from CardUIDMap where CardUID =  '{0}') ", cardUID);

                }
                else
                {
                    strWhere += string.Format("and Card.CardNumber in (select CardNumber from CardUIDMap where CardUID =  '{0}') ", cardUID);
                }
            }


            if (!string.IsNullOrEmpty(cardNumber))
            {
                //if (string.IsNullOrEmpty(strWhere))
                //{
                //    if (top > 0)
                //    {
                //        strWhere += " Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                //    }
                //    else
                //    {
                //        strWhere += " Coupon.CouponNumber = '" + couponNumber + "'";
                //    }
                //}
                //else
                //{
                //    if (top > 0)
                //    {
                //        strWhere += " and Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                //    }
                //    else
                //    {
                //        strWhere += " and Coupon.CouponNumber = '" + couponNumber + "'"; ;
                //    }
                //}

                //不需要补齐GC的逻辑
                Edge.SVA.BLL.CardGrade bll = new SVA.BLL.CardGrade();
                Edge.SVA.Model.CardGrade model = bll.GetModel(Convert.ToInt32(cardGradeID));
                string startcouponnumber = cardNumber;
                string endcouponnumber = cardNumber;
                if (model.CardCheckdigit.GetValueOrDefault() == 1)
                {
                    startcouponnumber = cardNumber.Substring(0, cardNumber.Length - 1);
                    endcouponnumber = (Convert.ToInt64(cardNumber.Substring(0, cardNumber.Length - 1)) + (top - 1)).ToString() + "9";
                }
                else
                {
                    endcouponnumber = (Convert.ToInt64(cardNumber) + (top - 1)).ToString();
                }
                if (string.IsNullOrEmpty(strWhere))
                {
                    if (top > 0)
                    {
                        strWhere += " Card.CardNumber >='" + startcouponnumber + "' and Card.CouponNumber<='" + endcouponnumber + "'" + " and Card.CardGradeID = (select top 1 CardGradeID from Card where Card.CardNumber = '" + cardNumber + "') ";
                    }
                    else
                    {
                        strWhere += " Card.CardNumber = '" + cardNumber + "'";
                    }
                }
                else
                {
                    if (top > 0)
                    {
                        strWhere += " and Card.CardNumber >='" + startcouponnumber + "' and Card.CardNumber<='" + endcouponnumber + "'" + " and Card.CardGradeID = (select top 1 CardGradeID from Card where Card.CardNumber = '" + cardNumber + "') ";
                    }
                    else
                    {
                        strWhere += " and Card.CardNumber = '" + cardNumber + "'"; ;
                    }
                }
            }


            return strWhere;
        }
        #endregion

        //add by frank 20140721
        void SetButton()
        {
            //Modified By Robin 2014-08-19 For 兼容Domain用户登录
            //if (Context.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(Context.User.Identity.Name) && SVASessionInfo.CurrentUser.UserName.Length >= 1)
            if (Context.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(SVASessionInfo.CurrentUser.UserName) && SVASessionInfo.CurrentUser.UserName.Length >= 1)
            //End
            {
                string url = Request.Path.ToLower();
                if (!url.EndsWith("/list.aspx"))
                {
                    return;
                }
                //Modified By Robin 2014-08-19 For 兼容Domain用户登录
                //string name = Context.User.Identity.Name;
                string name = SVASessionInfo.CurrentUser.UserName;
                //End
                AccountsPrincipal user = new AccountsPrincipal(name, SVASessionInfo.SiteLanguage);

                List<Control> list = new List<Control>();
                GetControls1(this.Form, ref list);
                List<FineUI.Button> btnList = new List<Button>();
                foreach (Control item in list)
                {
                    if (!string.IsNullOrEmpty(item.ID) && item is Button)
                    {
                        btnList.Add(item as Button);
                    }
                }
                url = url.Substring(0, url.LastIndexOf('/') + 1);
                foreach (var item in btnList)
                {
                    string pagename = string.Empty;
                    if (item.Text.Trim() == "批核")
                    {
                        pagename = "Approve.aspx";
                    }
                    else if (item.Text.Trim() == "作废")
                    {
                        pagename = "Void.aspx";
                    }
                    if (!string.IsNullOrEmpty(pagename))
                    {
                        PermissionID = PermissionMapper.GetSingleton().GetPermissionIDfromURL(url + pagename);
                        if ((PermissionID != -1) && (!user.HasPermissionID(PermissionID)))
                        {
                            item.Enabled = false;
                        }
                    }
                }
            }

        }

        void GetControls1(Control con, ref List<Control> list)
        {
            foreach (Control item in con.Controls)
            {
                if (item.Controls.Count >= 1)
                {
                    GetControls1(item, ref list);
                }
                else
                {
                    list.Add(item);
                }
            }
        }
    }
}