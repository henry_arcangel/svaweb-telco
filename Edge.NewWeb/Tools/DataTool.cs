﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.SVA.Model.Domain;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.SVA;

namespace Edge.Web.Tools
{
    public class DataTool
    {
        /// <summary>
        /// 在DataSet中Table增加发行商名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">发行商ID</param>
        internal static void AddCardIssuerName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardIssuerName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加发行商名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">发行商ID</param>
        internal static void AddCardIssuerName(DataSet ds, string name)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[name] = Edge.Web.Tools.DALTool.GetCardIssuerName();
            }
        }

        /// <summary>
        /// 在DataSet中Table增加品牌名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddBrandName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBrandName(id, cache);
            }
        }
        internal static void AddMessageType(DataSet ds, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn("MessageTypeDesc", typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row["MessageTypeDesc"] = ((Edge.SVA.Model.Domain.EnumMessageType)id).ToString();
            }
        }
        internal static void AddMessageServiceType(DataSet ds, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn("MessageServiceType", typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row["MessageServiceType"] = ((Edge.SVA.Model.Domain.EnumMessageServiceType)id).ToString();
            }
        }

        /// <summary>
        /// 在DataSet中Table增加品牌名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddBrandCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBrandCode(id, cache);
            }
        }
        /// <summary>
        /// 在DataSet中Table增加品牌描述
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddBrandInfoDesc(DataSet ds, string name, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            List<BrandInfo> brandInfoList = PublicInfoReostory.Singleton.GetAllBrandInfoList(SVASessionInfo.SiteLanguage);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row[refKey]!=null)
                {
                    BrandInfo bi = brandInfoList.Find(m => m.Key == row[refKey].ToString());
                    if (bi!=null)
                    {
                        row[name]=bi.Value;
                    } 
                    else
                    {
                        row[name]=string.Empty;
                    }
                } 
                else
                {
                    row[name] = string.Empty;
                }
            }
        }
        /// <summary>
        /// 在DataSet中Table增加交易类型
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddTransactionTypeDesc(DataSet ds, string name, string refKey)
        {
            string id =string.Empty;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row[refKey] != null)
                {
                    id = row[refKey].ToString();
                    KeyValue kv = TransactionTypeRepostory.Singleton.GetTransactionTypeList(SVASessionInfo.SiteLanguage).Find(mm => mm.Key == id.ToString());
                    if (kv != null)
                    {
                        row[name] = kv.Value;
                    }
                    else
                    {
                        row[name] = string.Empty;

                    }
                }
                else
                {
                    row[name] = string.Empty;
                }
            }
        }

        internal static void AddBrandCodeByCardType(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                Edge.SVA.Model.CardType cardtype = new SVA.BLL.CardType().GetModel(id);
                if (cardtype == null) continue;

                row[name] = Edge.Web.Tools.DALTool.GetBrandCode(cardtype.BrandID, cache);
            }
        }



        internal static void AddBrandCodeByCouponTypeID(DataSet ds, string name, string couponTypeID)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            Dictionary<int, string> couponTypeCache = new Dictionary<int, string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[couponTypeID].ToString(), out id) ? id : int.MinValue;

                row[name] = Edge.Web.Tools.DALTool.GetBrandCodeByCouponTypeID(id, cache);
            }
        }

        internal static void AddBrandNameByCouponTypeID(DataSet ds, string name, string couponTypeID)
        {
            int id = 0;
            string brandcode = "";
            int brandid =0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            Dictionary<int, string> couponTypeCache = new Dictionary<int, string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                brandcode = "";
                brandid = 0;
                id = int.TryParse(row[couponTypeID].ToString(), out id) ? id : int.MinValue;
                brandcode = Edge.Web.Tools.DALTool.GetBrandCodeByCouponTypeID(id, cache);
                brandid = Edge.Web.Tools.DALTool.GetBrandIDByBrandCode(brandcode, null);
                row[name] = Edge.Web.Tools.DALTool.GetBrandName(brandid, cache);
            }
        }


        internal static void AddBatchCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBatchCode(id, cache);
            }
        }

        internal static void AddCardBatchCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardBatchCode(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加区域名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">区域ID</param>
        internal static void AddLocationName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetLocationName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加店铺编号
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">店铺ID</param>
        internal static void AddStoreCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetStoreCode(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加店铺名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddStoreName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetStoreName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加卡类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡类型ID</param>
        internal static void AddCardTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardTypeName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加卡类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡类型ID</param>
        internal static void AddCardTypeCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardTypeCode(id, cache);
            }
        }
        internal static void AddMessageTemplateCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetMessageTemplateCode(id, cache);
            }
        }
        /// <summary>
        /// 转换 数字到 EnumFrequencyUnit的说明
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">FrequencyUnit</param>
        internal static void AddFrequencyUnitName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;

                row[name] = ((EnumFrequencyUnit)id).ToString();
            }
        }
        /// <summary>
        /// 转换0 1 到 否 是
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">优惠劵ID</param>
        internal static void AddBooleanName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                //if (id == 0)
                //{
                //    row[name] = "否";
                //}
                //else
                //{
                //    row[name] = "是";
                //}
                row[name] = ConstInfosRepostory.Singleton.GetKeyValueDesc(id.ToString(), SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.YesNo);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加优惠劵类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">优惠劵ID</param>
        internal static void AddCouponTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(id, cache);
            }
        }

        internal static void AddCouponTypeNameByCode(DataSet ds, string name, string refKey)
        {
            string code = "";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = row[refKey].ToString().Trim();
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(code, cache);
            }

        }


        internal static void AddCouponTypeNameByID(DataSet ds, string name, string refKey)
        {
            int code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToInt32(row[refKey].ToString().Trim());
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(code, cache);
            }

        }
        internal static void AddCouponTypeCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeCode(id, cache);
            }
        }

        internal static void AddCouponStockStatusByID(DataSet ds, string name, string refKey)
        {
            string id = string.Empty;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = Convert.ToString(row[refKey]);
                row[name] = Edge.Web.Tools.DALTool.GetCouponStockStatusByID(id, cache);
            }
        }
        /// <summary>
        /// 在DataSet中Table增加联系方式类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">联系方式类型ID</param>
        internal static void AddSNSTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetSNSTypeName(id, cache);
            }
        }

        internal static void AddUserName(DataSet ds, string newColumn, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetUserName(id, cache);
            }
        }

        internal static void AddTxnTypeName(DataSet ds, string newColumn, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetTnxTypeName(id);
            }
        }
        /// <summary>
        /// 在DataSet中Table增加兑换方式类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">兑换类型ID</param>
        internal static void AddExchangeType(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    //case 1: row[name] = "金额兑换"; break;
                    //case 2: row[name] = "积分兑换"; break;
                    //case 3: row[name] = "金额+积分兑换"; break;
                    //case 4: row[name] = "优惠券兑换"; break;
                    //case 5: row[name] = "消费金额兑换"; break;

                    case 1: row[name] = Resources.MessageTips.CashExchange; break;
                    case 2: row[name] = Resources.MessageTips.PointExchange; break;
                    case 3: row[name] = Resources.MessageTips.CashAndPointExchange; break;
                    case 4: row[name] = Resources.MessageTips.CouponExchange; break;
                    case 5: row[name] = Resources.MessageTips.RedeemedAmountExchange; break;
                }

            }
        }


        /// <summary>
        /// 在DataSet中Table增加状态
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">状态ID</param>
        internal static void AddStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    //case 0: row[name] = "无效"; break;
                    //case 1: row[name] = "有效"; break;

                    case 0: row[name] = Resources.MessageTips.Inactive; break;
                    case 1: row[name] = Resources.MessageTips.Active; break;
                }

            }
        }

        internal static void AddIsActivated(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = Resources.MessageTips.Inactivated; break;
                    case 1: row[name] = Resources.MessageTips.Activated; break;
                }

            }
        }


        /// <summary>
        /// 性别
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddSex(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = Resources.MessageTips.Female; break;
                    case 1: row[name] = Resources.MessageTips.Male; break;
                }

            }
        }
        /// <summary>
        /// 加上CouponStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCouponStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCouponTypeStatusName(id);
            }
        }

        /// <summary>
        /// 加上CouponStockStatus By Robin 2014-06-20
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCouponStockStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCouponStockStatusName(id);
            }
        }

        /// <summary>
        /// 加上CouponStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddDateFormat(DataSet ds, string name, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[name] = Tools.ConvertTool.ConverType<DateTime>(row[refKey].ToString()).ToString("yyyy-MM-dd");
            }
        }


        /// <summary>
        /// 加上CouponStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCardStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCardStatusName(id);
            }
        }

        /// <summary>
        /// 加上CouponPreviousStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCouponPreviousStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Controllers.CouponController.GetPreviousCouponStatus(id, row["CouponNumber"].ToString());
            }
        }

        internal static void UpdateCouponStatus(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {
            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }
                if (coupon == null) continue;
                row[name] = Tools.DALTool.GetCouponTypeStatusName(coupon.Status);
            }
        }

        internal static void UpdateCouponBatch(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;
            Dictionary<int, string> batchCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }
                if (coupon == null) continue;

                row[name] = DALTool.GetBatchCode(coupon.BatchCouponID.GetValueOrDefault(), batchCache);
            }
        }
        internal static void AddCouponApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetApproveStatusString(row[approveStatus].ToString());

            }
        }
        internal static void AddOrderPickingApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetOrderPickingApproveStatusString(row[approveStatus].ToString());

            }
        }
        internal static void UpdateCouponExpiryDate(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {
            Edge.SVA.Model.Coupon coupon = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null) row[name] = coupon.CouponExpiryDate;
            }
        }

        internal static void UpdateCouponDenomination(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null)
                {
                    row[name] = coupon.CouponAmount.GetValueOrDefault();
                }

            }
        }

        internal static void UpdateCouponNumber(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null) row[name] = coupon.CouponNumber;
            }
        }

        internal static void UpdateCouponAmout(DataSet ds, string couponAmount, string couponTypeID)
        {
            Dictionary<int, Edge.SVA.Model.CouponType> cache = new Dictionary<int, SVA.Model.CouponType>();
            Edge.SVA.Model.CouponType couponType = null;
            int id = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[couponTypeID].ToString(), out id) ? id : 0;
                couponType = DALTool.GetCouponType(id, cache);
                if (couponType != null) row[couponAmount] = couponType.CouponTypeAmount;
            }
        }

        internal static void AddPasswordFormat(DataSet ds, string newFormat, string format)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newFormat, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int i = -1;
                i = int.TryParse(row[format].ToString(), out i) ? i : -1;
                row[newFormat] = DALTool.GetPasswordFormatName(i);

            }
        }
        internal static void AddID(DataSet ds, string newColumn, int size, int page)
        {
            long id = size * page;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(long)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = ++id;
            }
        }

        internal static void AddCouponUID(DataSet ds, string newColumn, string couponNumberColumn)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = DALTool.GetUID(row[couponNumberColumn].ToString(), cache);
            }
        }

        internal static void AddCardUID(DataSet ds, string newColumn, string cardNumberColumn)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            Edge.SVA.BLL.CardUIDMap map = new Edge.SVA.BLL.CardUIDMap();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = DALTool.GetCardUID(row[cardNumberColumn].ToString(), cache);
            }
        }

        internal static void AddColumn(DataSet ds, string newColumn, object value)
        {
            Type type = value != null ? value.GetType() : typeof(object);
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, type));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = value;
            }
        }

        internal static void AddColumn(DataTable dt, string newColumn, object value)
        {
            Type type = value != null ? value.GetType() : typeof(object);
            dt.Columns.Add(new DataColumn(newColumn, type));

            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = value;
            }
        }

        internal static void AddColumnWithValue(DataSet ds, string newColumn, string value)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(object)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = row[value];
            }
        }

        internal static void Sort(DataSet ds, string newColumn, string sortColumn, Type type)
        {
            ds.Tables[0].Columns.Add(newColumn, type);

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][newColumn] = ConvertTool.ChangeType(type, ds.Tables[0].Rows[i][sortColumn].ToString());
            }
        }

        internal static void ClearEndRow(DataTable dt)
        {
            if (dt == null) return;

            for (int i = dt.Rows.Count - 1; i > 0; i--)
            {
                bool isNull = true;
                for (int col = 0; col < dt.Columns.Count; col++)
                {
                    if (dt.Rows[i][col] != null && dt.Rows[i][col].ToString() != "")
                    {
                        isNull = false;
                        break;
                    }
                }
                if (isNull) dt.Rows.Remove(dt.Rows[i]);
                else break;
            }
        }

        internal static void AddCouponAmount(DataSet ds, string name, string refKey, string couponTypeKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int type = Edge.Web.Tools.ConvertTool.ToInt(row[couponTypeKey].ToString());
                Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(type);
                if (model != null)
                {
                    if (model.CoupontypeFixedAmount.GetValueOrDefault() == 1)//Fixed
                    {
                        id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                        row[name] = model.CouponTypeAmount.ToString("N02");
                    }
                    else
                    {
                        id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                        row[name] = "--";//Modify by Nathan
                        row[name] = Resources.MessageTips.IsOpenAmount;

                    }
                }
                else
                {
                    id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                    row[name] = "??";
                }
            }
        }

        #region for card operation
        internal static void AddCardApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetApproveStatusString(row[approveStatus].ToString());

            }
        }
        /// <summary>
        /// 在DataSet中Table增加卡级别名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡级别ID</param>
        internal static void AddCardGradeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardGradeName(id, cache);
            }
        }
        #endregion

        internal static IEnumerable<DataRow> GetPaggingTable(int index, int pageSize, DataTable source)
        {
            if (source == null) return null;
            var result = (from row in source.AsEnumerable()
                          //select row).Skip(index * pageSize).Take(10); //Modified By Robin 2014-07-17 fixed page size bug
                          select row).Skip(index * pageSize).Take(pageSize);
            return result;
        }

        internal static void AddSaleStatusName(DataTable dt, string newColumn, string refKey)
        {
            int id = 0;
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in dt.Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetSaleStatusName(id);
            }
        }

        internal static void AddSaleTypeName(DataTable dt, string newColumn, string refKey)
        {
            int id = 0;
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in dt.Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetSaleTypeName(id);
            }
        }

        internal static void AddGender(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = ""; break;
                    case 1: row[name] = Resources.MessageTips.Male; break;
                    case 2: row[name] = row[name] = Resources.MessageTips.Female; break;
                }

            }
        }

        internal static void AddSalesName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetSalesName(row[refKey].ToString());
            }
        }

        internal static void AddTendName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetTendName(row[refKey].ToString());
            }
        }

        internal static void AddColorName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetColorName(row[refKey].ToString());
            }
        }

        internal static void AddProdSizeName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetProdSizeName(row[refKey].ToString());
            }
        }

        internal static void AddProdName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetProdName(row[refKey].ToString());
            }
        }

        internal static void AddLogisticsProviderName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = Edge.Web.Tools.DALTool.GetAddLogisticsProviderName(row[refKey].ToString());
            }
        }

        internal static void AddExchangeTypeName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetExchangeTypeName(row[refKey].ToString());
                }
            }
        }

        internal static void AddMemberRangeName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetMemberRangeName(row[refKey].ToString());
                }
            }
        }

        internal static void AddConsumeRuleOperName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetConsumeRuleOperName(row[refKey].ToString());
                }
            }
        }

        internal static void AddSVARewardTypeName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetSVARewardTypeName(row[refKey].ToString());
                }
            }
        }

        internal static void AddCurrencyName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCurrencyName(ConvertTool.ToInt(row[refKey].ToString()), null);
                }
            }
        }
        internal static void AddDepartmentName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetDepartName(row[refKey].ToString());
                }
            }
        }

        internal static void AddDistributionDesc(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetDistributionDesc(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddPromotionTitle(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetPromotionTitle(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddPasswordRuleName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetPasswordRuleName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddReasonDesc(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetReasonDesc(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddStoreTypeName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetStoreTypeName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddPhraseTitle(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetPhraseTitle(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddCustomerDesc(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCustomerDesc(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddIndustryName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetIndustryName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddCampaignName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCampaignName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddCouponNatureName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCouponNatureName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddOrganizationName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetOrganizationName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddEducationName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetEducationName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddNationName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetNationName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddProfessionName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetProfessionName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddSponsorName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetSponsorName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddSupplierDesc(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetSupplierDesc(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddCompanyName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCompanyName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddCompanyAddress(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetCompanyAddress(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddRedeemLimitTypeName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetRedeemLimitTypeName(row[refKey].ToString());
                }
            }
        }

        internal static void AddEntityTypeName(DataTable dt, string newColumn, string refKey)
        {
            dt.Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetEntityTypeName(row[refKey].ToString());
                }
            }
        }

        internal static void AddStoreGroupName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row[newColumn] = Edge.Web.Tools.DALTool.GetStoreGroupName(ConvertTool.ToInt(row[refKey].ToString()));
                }
            }
        }

        internal static void AddDayFlagName(DataSet ds, string name, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[name] = GetDayFlagStatusName(ConvertTool.ToInt(row[refKey].ToString()), System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower());
            }
        }
        internal static string GetDayFlagStatusName(int status, string lan)
        {
            switch (lan.ToLower())
            {
                case "en-us":
                    switch (status)
                    {
                        case 1:
                            return "Active";
                        default:
                            return "Invalid";
                    }
                case "zh-cn":
                    switch (status)
                    {
                        case 1:
                            return "生效";
                        default:
                            return "无效";
                    }
                case "zh-hk":
                    return "";
                default:
                    switch (status)
                    {
                        case 1:
                            return "Active";
                        default:
                            return "Invalid";
                    }
            }
        }
        internal static void AddCouponTypeBrandID(DataSet ds, string name, string refKey) 
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(long)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeBrandID(id);
            }
        }

        internal static void AddCouponUIDByCouponNumber(DataSet ds, string name, string refKey)
        {
            string strWhere = "";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                strWhere = " CouponTypeID=" + row["CouponTypeID"].ToString() + " and CouponNumber='" + row[refKey].ToString() + "'";
                row[name] = Edge.Web.Tools.DALTool.GetCouponUIDByCouponNumber(strWhere);
            }
        }

        internal static void AddCouponReplenishStatus(DataSet ds, string name, string refKey)
        {
            string statusName = "";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                statusName = Convert.ToString(row[refKey]) == "1" ? Resources.MessageTips.Active : Resources.MessageTips.Inactive;
                row[name] = statusName;
            }
        }
        // Add by Alex 2014-06-11 ++
        internal static void AddCardGradeCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardGradeCode(id, cache);
            }
        }
        //Add by Aelx 2014-06-10 ++
        internal static void UpdateCardExpiryDate(DataSet ds, string name, string cardUID, Dictionary<string, Edge.SVA.Model.Card> cache)
        {
            Edge.SVA.Model.Card card = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[cardUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    card = cache[uid];
                }
                else
                {
                    card = new Edge.SVA.BLL.Card().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, card);
                }

                if (card != null) row[name] = card.CardExpiryDate;
            }
        }
        internal static void UpdateCardDenomination(DataSet ds, string name, string cardUID, Dictionary<string, Edge.SVA.Model.Card> cache)
        {

            Edge.SVA.Model.Card card = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[cardUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    card = cache[uid];
                }
                else
                {
                    card = new Edge.SVA.BLL.Card().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, card);
                }

                if (card != null)
                {
                    row[name] = card.TotalAmount.GetValueOrDefault();
                }

            }
        }


        internal static void UpdateCardNumber(DataSet ds, string name, string cardUID, Dictionary<string, Edge.SVA.Model.Card> cache)
        {

            Edge.SVA.Model.Card card = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[cardUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    card = cache[uid];
                }
                else
                {
                    card = new Edge.SVA.BLL.Card().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, card);
                }

                if (card != null) row[name] = card.CardNumber;
            }
        }
        internal static void UpdateCardStatus(DataSet ds, string name, string cardUID, Dictionary<string, Edge.SVA.Model.Card> cache)
        {
            Edge.SVA.Model.Card card = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[cardUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    card = cache[uid];
                }
                else
                {
                    card = new Edge.SVA.BLL.Card().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, card);
                }
                if (card == null) continue;
                row[name] = Tools.DALTool.GetCardStatusName(card.Status);
            }
        }
        internal static void UpdateCardBatch(DataSet ds, string name, string cardUID, Dictionary<string, Edge.SVA.Model.Card> cache)
        {

            Edge.SVA.Model.Card card = null;
            Dictionary<int, string> batchCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[cardUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    card = cache[uid];
                }
                else
                {
                    card = new Edge.SVA.BLL.Card().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, card);
                }
                if (card == null) continue;

                row[name] = DALTool.GetCardBatchCode(card.BatchCardID.GetValueOrDefault(), batchCache);
            }
        }
        //Add by Aelx 2014-06-10 --
        // Add by Alex 2014-06-11 --
        /// <summary>
        /// 加上CardPreviousStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCardPreviousStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Controllers.CardController.GetPreviousCardStatus(id, row["CardNumber"].ToString());
            }
        }
        //Add by Alex 2014-08-01 ++
        /// <summary>
        /// 在DataSet中Table增加交易数值
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">OprID</param>
        internal static void AddAmount(DataSet ds, string name, string refKey)
        {
            int id = 0;
            decimal  points =0;
            decimal amount = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(decimal)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                try
                {
                    points = Convert.ToDecimal(row["Points"].ToString());
                }
                catch
                {
                    points = 0;
                }
                try
                {
                    amount = Convert.ToDecimal(row["Amount"].ToString());
                }
                catch
                {
                    amount = 0;
                }
                if (id != 0)
                {
                    row[name] = points != 0 ? points : amount;
                }
                else
                {
                    row[name] = 0; // OprID = 0時， 交易數值為0
                }
                points = 0;
                amount = 0;
                //switch (id)
                //{
                  
                //    case 13:
                //    case 15:
                //    case 16:
                //    case 17:
                //    case 25:
                //    case 23:
                //    case 27:
                //        row[name] = row["Points"].ToString() == "0" ? "0.00" : row["Points"].ToString();
                //        break;
                //    case 2:
                //    case 3:
                //    case 6:
                //    case 7:
                //    case 9:
                //    case 10:
                //    case 12:
                //    case 22:
                //    case 31:
                //    case 44:
                //        row[name] = row["Amount"].ToString() == "0.0000" ? "0.00" : row["Amount"].ToString();
                //        break;
                //    default:
                //        row[name] = "0.00";
                //        break;
                  
                //}

               // row[name] = Edge.Web.Tools.DALTool.GetCardMovementAount(id, cache);
            }
        }
        //Add by Alex 2014-08-01 --

        //Add by Gavin
        internal static void AddCardTypeNameByID(DataSet ds, string name, string refKey)
        {
            int code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToInt32(row[refKey].ToString().Trim());
                row[name] = Edge.Web.Tools.DALTool.GetCardTypeName(code, cache);
            }

        }

        internal static void AddCardGradeNameByID(DataSet ds, string name, string refKey)
        {
            int code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToInt32(row[refKey].ToString().Trim());
                row[name] = Edge.Web.Tools.DALTool.GetCardGradeName(code, cache);
            }

        }

        internal static void AddCardUIDByCardNumber(DataSet ds, string name, string refKey)
        {
            string strWhere = "";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                strWhere = " CardNumber='" + row[refKey].ToString() + "'";
                row[name] = Edge.Web.Tools.DALTool.GetCardUIDByCardNumber(strWhere);
            }
        }

        internal static void AddCardStockStatusByID(DataSet ds, string name, string refKey)
        {
            string id = string.Empty;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = Convert.ToString(row[refKey]);
                row[name] = Edge.Web.Tools.DALTool.GetCardStockStatusByID(id, cache);
            }
        }

        /// <summary>
        /// 加上CardStockStatus By Robin 2014-06-20
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCardStockStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCardStockStatusName(id);
            }
        }
        //Add by Gavin

        //Add by Gavin @2015-03-03 --
        internal static void AddVenderName(DataSet ds, string name)
        {
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["VenderType"].ToString() == "1")
                {
                    row[name] = Edge.Web.Tools.DALTool.GetBrandName(Convert.ToInt16(row["VenderID"]), cache);
                }
                else
                {
                    row[name] = Edge.Web.Tools.DALTool.GetSupplierDesc(Convert.ToInt16(row["VenderID"]));
                }
            }
        }
        internal static void AddBuyerName(DataSet ds, string name)
        {
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["BuyerType"].ToString() == "1")
                {
                    row[name] = Edge.Web.Tools.DALTool.GetCompanyName(Convert.ToInt16(row["BuyerID"]));
                }
                else
                {
                    row[name] = Edge.Web.Tools.DALTool.GetStoreName(Convert.ToInt16(row["BuyerID"]), cache);
                }
            }

        }
        //Add by Gavin @2015-03-03 --


        /// <summary>
        /// Add datarowColumn For all DataSets
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="ColumnName"></param>
       
        // added Jay Celeste
        internal static void AddCardGradeName1(DataSet ds, string name, string refKey)
        {
            int code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToInt32(row[refKey].ToString().Trim());
                row[name] = Edge.Web.Tools.DALTool.GetCardGradeName1(code, cache);
            }

        }

        internal static void AddCardTypeNameByCode(DataSet ds, string name, string refKey)
        {
            String code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToString(row[refKey]);

               // row[name] = Edge.Web.Tools.DALTool.GetCardTypeName(code, cache);
            }

        }
    }
}
