﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Edge.SVA.Model.Domain.WebService.Response;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Edge.SVA.Model.Domain.WebService;
using Edge.SVA.Model.Domain.WebService.Request;

namespace Edge.Web.Tools
{
    public class ResetPasswordUtil
    {
        Tools.Logger logger = Tools.Logger.Instance;
        string key = "19A1708FCEFB1929";
        byte[] keyByte;
        
        public ResetPasswordUtil()
        {
            keyByte = HexStringToToHexByte(key);

        }
        /// <summary>
        /// 重设密码
        /// </summary>
        /// <param name="useID">当前操作员的userid</param>
        /// <param name="memberRegisterMobileNum">会员的注册手机号</param>
        /// <returns>true，重设成功；false，重设失败。</returns>
        public bool ResetPassord(string useID, string memberRegisterMobileNum)
        {
            logger.WriteOperationLog("ResetPassord", useID + " " + memberRegisterMobileNum);
            //调用WebService之前，需要先检查当前数据库与服务数据库是否一致，不一致则不允许使用(Add By Len)
            if (SVASessionInfo.CheckDBName != "true")
            {
                logger.WriteOperationLog("Start WebService ", "CheckDBName Failed");
                return false;
            }
            bool rtn = false;
            try
            {
                SVAWebReference.SVAWebService service = new SVAWebReference.SVAWebService();
                string response = string.Empty;
                string tempstr = GetRequestStr(useID, memberRegisterMobileNum);
                logger.WriteOperationLog("ResetPassord request str:", tempstr);
                
                response = service.request(tempstr, key);
                logger.WriteOperationLog("ResetPassord  request:", response);
                MemberResetPasswordResponse mrpr = GetMemberResetPasswordResponse(response);
                if (mrpr != null)
                {
                    if (mrpr.ResponseCode.Equals(0))
                    {
                        rtn = true;
                    }
                    logger.WriteOperationLog("ResetPassord ResponseCode", mrpr.ResponseCode.ToString());
                    if (mrpr.ResponseCode.Equals(90286))
                    {
                        logger.WriteOperationLog("email not exists !", mrpr.ResponseCode.ToString());
                    }
                }
                else
                {
                    logger.WriteErrorLog("ResetPassord", " return string is empty!");
                }
            }
            catch (System.Exception ex)
            {
                logger.WriteErrorLog("ResetPassord", " error ", ex);
            }
            return rtn;
        }
        private string GetRequestStr(string useID, string memberRegisterMobileNum)
        {
            MemberResetPasswordRequest request = new MemberResetPasswordRequest();
            request.Action = "LostMemberPassword";
            request.UserID = useID;
            request.MemberRegistID = memberRegisterMobileNum;
            return Encode(JsonConvert.SerializeObject(request),key);
        }
        //private bool SendRegisteredInfo(NewMemberAccountRequest request)
        //{
        //    request.Action = "NewMemberAccount";
        //    logger.WriteOperationLog("NewMemberAccount ", request.CardNumber + " " +request.MemberRegisterMobile);
        //    bool rtn = false;
        //    try
        //    {
        //        SVAWebReference.SVAWebService service = new SVAWebReference.SVAWebService();
        //        string response = string.Empty;
        //        response = service.request(Encode(JsonConvert.SerializeObject(request), key), key);

        //        NewMemberAccountResponse mrpr = GetNewMemberAccountResponse(response);
        //        if (mrpr != null)
        //        {
        //            if (mrpr.ResponseCode.Equals(0))
        //            {
        //                rtn = true;
        //            }
        //            logger.WriteOperationLog("NewMemberAccount ResponseCode", mrpr.ResponseCode.ToString());                    
        //        }
        //        else
        //        {
        //            logger.WriteErrorLog("NewMemberAccount", " return string is empty!");
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        logger.WriteErrorLog("NewMemberAccount", " error ", ex);
        //    }
        //    return rtn;
        //}

        public ResultInfo[] BatchSendMessage(BatchSendMessageRequest request,string lan)
        {
            request.Action = "BatchSendMessage";
            request.Type = "1";
            if (lan=="zh-cn") //confirm by michael he 2013-09-13
            {
                lan = "zh_CN";
            }
            else if (lan=="zh-hk")
            {
                lan = "zh_TW";
            }
            else
            {
                lan = "en_US";
            }

            request.Language = lan;
            try
            {
                SVAWebReference.SVAWebService service = new SVAWebReference.SVAWebService();
                string response = string.Empty;
                string msg=JsonConvert.SerializeObject(request);
                logger.WriteOperationLog("BatchSendMessage ", msg);
                string tempstr = Encode(msg, key);
                response = service.request(tempstr, key);
                logger.WriteOperationLog("BatchSendMessage Response ", response);
                BatchSendMessageResponse responseObj = GetBatchSendMessageResponse(response);
                if (responseObj != null)
                {
                    logger.WriteOperationLog("BatchSendMessage ResponseCode", responseObj.ResponseCode.ToString());
                    if (responseObj.ResponseCode.Equals(0))
                    {
                        return responseObj.ResultInfo;
                    }
                }
                else
                {
                    logger.WriteErrorLog("BatchSendMessage", " return string is empty!");
                }
            }
            catch (System.Exception ex)
            {
                logger.WriteErrorLog("BatchSendMessage", " error ", ex);
            }
            return new ResultInfo[0];
        }

        private MemberResetPasswordResponse GetMemberResetPasswordResponse(string response)
        {
            return JsonConvert.DeserializeObject<MemberResetPasswordResponse>(response);
        }
        private NewMemberAccountResponse GetNewMemberAccountResponse(string response)
        {
            return JsonConvert.DeserializeObject<NewMemberAccountResponse>(response);
        }
        private BatchSendMessageResponse GetBatchSendMessageResponse(string response)
        {
            return JsonConvert.DeserializeObject<BatchSendMessageResponse>(response);
        }
        private string Encode(string str, string key)
        {
            try
            {
                SVAWebReference.SVAWebService service = new SVAWebReference.SVAWebService();
                if (service.SVAWebServiceIsEncrypt)
                {
                    DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                    provider.Mode = CipherMode.ECB;
                    provider.Key = keyByte;
                    byte[] bytes = Encoding.GetEncoding("GBK").GetBytes(str);
                    MemoryStream stream = new MemoryStream();
                    CryptoStream stream2 = new CryptoStream(stream, provider.CreateEncryptor(), CryptoStreamMode.Write);
                    stream2.Write(bytes, 0, bytes.Length);
                    stream2.FlushFinalBlock();
                    StringBuilder builder = new StringBuilder();
                    foreach (byte num in stream.ToArray())
                    {
                        builder.AppendFormat("{0:X2}", num);
                    }
                    stream.Close();
                    return builder.ToString();
                }
                else
                    return str;
            }
            catch (Exception ex)
            {
                return "xxxx";
            }
        }
        private static byte[] HexStringToToHexByte(string hexString)
        {
            if (string.IsNullOrEmpty(hexString))
            {
                return new byte[] { };
            }
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public bool CheckDBName()
        {
            logger.WriteOperationLog("CheckDBName", "");
            bool rtn = false;
            try
            {
                SVAWebReference.SVAWebService service = new SVAWebReference.SVAWebService();
                string response = string.Empty;
                string tempstr = GetCheckDBRequestStr();
                logger.WriteOperationLog("CheckDBName GetCheckDBRequestStr: ", tempstr);
                response = service.request(tempstr, key);
                logger.WriteOperationLog("CheckDBName request: ", response);
                CheckDBResponse mrpr = GetCheckDBResponse(response);
                logger.WriteOperationLog("CheckDBName: ", " call GetCheckDBResponse");
                if (mrpr != null)
                {
                    if (mrpr.ResponseCode.Equals(0))
                    {
                        rtn = true;
                    }
                    logger.WriteOperationLog("CheckDB ResponseCode", mrpr.ResponseCode.ToString());
                }
                else
                {
                    logger.WriteErrorLog("CheckDB", " return string is empty!");
                }
            }
            catch (System.Exception ex)
            {
                logger.WriteErrorLog("CheckDB", " error ", ex);
            }
            return rtn;
        }

        private string GetCheckDBRequestStr()
        {
            CheckDBRequest request = new CheckDBRequest();
            request.Action = "CheckDB";
            Edge.Utils.AppConfig appconfig = Edge.Utils.AppConfig.Singleton;
            request.DBURL = appconfig.ServerName;
            request.DataBaseName = appconfig.DbName;
            logger.WriteOperationLog("GetCheckDBRequestStr","SVADBName is " + appconfig.DbName);
            return Encode(JsonConvert.SerializeObject(request), key);
        }

        private CheckDBResponse GetCheckDBResponse(string response)
        {
            return JsonConvert.DeserializeObject<CheckDBResponse>(response);
        }
    }
}