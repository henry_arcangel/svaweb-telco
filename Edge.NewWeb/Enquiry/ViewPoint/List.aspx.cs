﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Enquiry.ViewPoint
{
    public partial class List : PageBase
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = this.webset.ContentPageNum;
            if (!Page.IsPostBack)
            {
                RptBind("", "CardNumber");
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();


            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }


        
    }
}
