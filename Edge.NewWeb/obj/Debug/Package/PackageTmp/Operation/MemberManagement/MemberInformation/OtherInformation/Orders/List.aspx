﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders.List" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelSeparator="0" LabelAlign="Right" LabelWidth="160">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="30% 30% 30% 10%">
                        <Items>
                            <ext:TextBox ID="TxnNo" runat="server" Label="订单编号："></ext:TextBox>
                            <ext:DropDownList ID="Status" runat="server" Label="订单状态：" Resizable="true">
                                <ext:ListItem Text="-------" Value="-1"  Selected="true"/>
                                <%--<ext:ListItem Text="Wait For Payment" Value="3" />
                                <ext:ListItem Text="Paid" Value="4" />
                                <ext:ListItem Text="Complete" Value="5" />
                                <ext:ListItem Text="Shipped" Value="6" />
                                <ext:ListItem Text="Rejected" Value="8" />
                                <ext:ListItem Text="Delayed" Value="9" />--%>
                                <ext:ListItem Text="未确认支付" Value="3" />
                                <ext:ListItem Text="已付款未提货" Value="4" />
                                <ext:ListItem Text="交易完成" Value="5" />
                                <ext:ListItem Text="交付运送" Value="6" />
                                <ext:ListItem Text="拒收" Value="8" />
                                <ext:ListItem Text="已延迟" Value="9" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="SalesType" runat="server" Label="订单类型：" Resizable="true">
                                <ext:ListItem Text="-------" Value="-1"  Selected="true"/>
                                <ext:ListItem Text="普通交易" Value="0" />
                                <ext:ListItem Text="送货交易" Value="1" />
                                <ext:ListItem Text="充值" Value="2" />
                            </ext:DropDownList>
                            <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click" ValidateForms="SearchForm">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server" ColumnWidths="30% 30% 30% 10%">
                        <Items>
                            <ext:TextBox ID="CardNumber" runat="server" Label="卡号码："></ext:TextBox>
                            <ext:TextBox ID="MemberRegisterMobile" runat="server" Label="注册号码："></ext:TextBox>
                            <ext:TextBox ID="MemberMobilePhone" runat="server" Label="手机号码："></ext:TextBox>
                            <ext:Label ID="Label3" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="30% 30% 30% 10%">
                        <Items>
                            <ext:DatePicker ID="CreateStartDate" runat="server" Label="确认时间从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="CreateEndDate" runat="server" Label="确认时间到：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="PaymentStartDate" runat="server" Label="付款时间从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:Label ID="Label1" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server" ColumnWidths="30% 30% 30% 10%">
                        <Items>
                            <ext:DatePicker ID="PaymentEndDate" runat="server" Label="付款时间到：" DateFormatString="yyyy-MM-dd"> 
                            </ext:DatePicker>
                            <ext:DatePicker ID="DeliverStartOn" runat="server" Label="发货时间从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="DeliverEndOn" runat="server" Label="发货时间到：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:Label ID="Label4" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="TxnNo" OnPreRowDataBound="Grid1_PreRowDataBound"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" 
                         OnSort="Grid1_Sort" AllowSorting="true"><%--OnRowDataBound="Grid1_RowDataBound" OnPreRowDataBound="Grid1_PreRowDataBound"--%>
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSend" Text="已发货" Icon="Lightning" runat="server" OnClick="btnSend_Click">
                                    </ext:Button>
                                    <ext:Button ID="Button1" Text="已支付" Icon="Money" runat="server" OnClick="btnPay_Click">
                                    </ext:Button>
                                    <ext:Button ID="btnImport" Text="导入" Icon="Attach" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="订单编号" SortField="TxnNo">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%# Eval("TxnNo") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardNumber" runat="server" Text='<%# Eval("CardNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订单类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblSalesType" runat="server" Text='<%# Eval("SalesTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订单状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="确认时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="付款时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentDoneOn" runat="server" Text='<%#Eval("PaymentDoneOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="发货时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeliverStartOn" runat="server" Text='<%#Eval("DeliverStartOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="TxnNo"
                                DataIFrameUrlFormatString="Show.aspx?id={0}" Title="查看" />
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window3" Icon="PageEdit"
                                Text="修改" ToolTip="修改" DataTextFormatString="{0}" DataIFrameUrlFields="TxnNo"
                                DataIFrameUrlFormatString="Modify.aspx?id={0}" Title="修改" />
                        </Columns>
                    </ext:Grid>
                    </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="900px"
        Height="580px">
    </ext:Window>
    <ext:Window ID="Window2" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="Window3" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="580px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window> 
    <ext:Window ID="Window4" Title="导入" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank" OnClose="WindowEdit_Close"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="700px"
        Height="130px">
    </ext:Window>
    <ext:Window ID="Window5" Title="导入" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank" OnClose="WindowEdit_Close"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="580px"
        Height="350px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField>
    <ext:HiddenField ID="SortField" Text="" runat="server"></ext:HiddenField>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
