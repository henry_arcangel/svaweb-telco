﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Basic.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Modify</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form2" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="MemberRegisterMobile" runat="server" Label="注册号码：" MaxLength="512"
                                Required="true" ShowRedStar="true" Enabled="false">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberAppellation" runat="server" Label="称呼：" MaxLength="20">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberEngFamilyName" runat="server" Label="英文名（姓）：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberEngGivenName" runat="server" Label="英文名（名）：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberChiFamilyName" runat="server" Label="中文名（姓）：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberChiGivenName" runat="server" Label="中文名（名）：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="NickName" runat="server" Label="昵称：" MaxLength="512">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="MemberSex" runat="server" Label="性别：">
                                <ext:RadioItem Text="保密" Value="0" Selected="true" />
                                <ext:RadioItem Text="男性" Value="1" />
                                <ext:RadioItem Text="女性" Value="2" />
                            </ext:RadioButtonList>
                            <ext:DatePicker ID="MemberDateOfBirth" runat="server" Label="生日 ：" DateFormatString="yyyy-MM-dd"
                                Required="true" ShowRedStar="true">
                            </ext:DatePicker>
                            <%--<ext:DropDownList ID="NationID" runat="server" Label="国家码：" Resizable="true">
                            </ext:DropDownList>--%>
                            <ext:TextBox ID="CountryCode" runat="server" Label="国家码："></ext:TextBox>
                            <ext:NumberBox ID="MemberMobilePhone" runat="server" Label="手机号码：" MaxLength="512">
                            </ext:NumberBox>
                            <ext:RadioButtonList ID="MemberMarital" runat="server" Label="婚姻情况：">
                                <ext:RadioItem Text="保密" Value="0" Selected="true" />
                                <ext:RadioItem Text="未婚" Value="1" />
                                <ext:RadioItem Text="已婚" Value="2" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="MemberIdentityType" runat="server" Label="证件类别：" Resizable="true">
                                <ext:ListItem Value="0" Text="---------" />
                                <ext:ListItem Value="1" Text="手机" />
                                <ext:ListItem Value="2" Text="邮箱" />
                                <ext:ListItem Value="3" Text="身份证" />
                            </ext:DropDownList>
                            <ext:TextBox ID="MemberIdentityRef" runat="server" Label="证件号码：" MaxLength="512">
                            </ext:TextBox>
                            <%--Add by Nathan 2014-07-07 ++ --%>
                        <ext:DropDownList ID="MemberDefLanguage" runat="server" Label="会员默认语言：" MaxLength="512"
                            ToolTipTitle="会员默认语言" ToolTip="不能超過512個字符" Resizable="true">
                        </ext:DropDownList>
                        <ext:RadioButtonList ID="ReceiveAllAdvertising" runat="server" Label="接收促销讯息："
                            Width="100px">
                            <ext:RadioItem Text="是" Value="1" Selected="True" />
                            <ext:RadioItem Text="否" Value="0" />
                        </ext:RadioButtonList>
                        <ext:RadioButtonList ID="ReceiveSMSPromotionMessage" runat="server" Label="接收短信促销讯息："
                            Width="100px">
                            <ext:RadioItem Text="是" Value="1" Selected="True" />
                            <ext:RadioItem Text="否" Value="0" />
                        </ext:RadioButtonList>
                        <ext:RadioButtonList ID="ReceiveEmailPromotionMessage" runat="server" Label="接收电子邮件促销讯息："
                            Width="100px">
                            <ext:RadioItem Text="是" Value="1" Selected="True" />
                            <ext:RadioItem Text="否" Value="0" />
                        </ext:RadioButtonList>
                        <ext:RadioButtonList ID="AcceptPhoneAdvertising" runat="server" Label="接收电话呼叫促销讯息：："
                            Width="100px">
                            <ext:RadioItem Text="是" Value="1" Selected="True" />
                            <ext:RadioItem Text="否" Value="0" />
                        </ext:RadioButtonList>
                        <%--Add by Nathan 2014-07-07 -- --%>
                            <ext:DropDownList ID="EducationID" runat="server" Label="学历：" Resizable="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="ProfessionID" runat="server" Label="专业：" Resizable="true">
                            </ext:DropDownList>
                            <ext:TextBox ID="MemberPosition" runat="server" Label="职位：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="HomeTelNum" runat="server" Label="固定电话：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberEmail" runat="server" Label="邮箱：" MaxLength="512">
                            </ext:TextBox>
                            <ext:DropDownList ID="AddressCountry" runat="server" Label="国家编码：" MaxLength="512"
                                ToolTipTitle="国家编码" ToolTip="不能超過512個字符" OnSelectedIndexChanged="AddressCountry_SelectedIndexChanged"
                                Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="AddressProvince" runat="server" Label="省（州）编码：" MaxLength="512"
                                ToolTipTitle="省（州）编码" ToolTip="不能超過512個字符" OnSelectedIndexChanged="AddressProvince_SelectedIndexChanged"
                                Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="AddressCity" runat="server" Label="城市编码：" MaxLength="512" ToolTipTitle="城市编码"
                                ToolTip="不能超過512個字符" OnSelectedIndexChanged="AddressCity_SelectedIndexChanged"
                                Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="AddressDistrict" runat="server" Label="区县编码：" MaxLength="512"
                                ToolTipTitle="区县编码" ToolTip="不能超過512個字符" OnSelectedIndexChanged="AddressDistrict_SelectedIndexChanged"
                                Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:TextBox ID="AddressDetail" runat="server" Label="详细地址：" OnTextChanged="AddressFullDetail_OnTextChanged"
                                AutoPostBack="true" />
                            <ext:TextBox ID="AddressFullDetail" runat="server" Label="完整地址：" Enabled="false" />
                            <ext:TextBox ID="txtFaceBook" runat="server" Label="Translate__Special_121_StartFaceBook：Translate__Special_121_End" />
                            <ext:TextBox ID="txtQQ" runat="server" Label="Translate__Special_121_StartQQ：Translate__Special_121_End" />
                            <ext:TextBox ID="txtMSN" runat="server" Label="Translate__Special_121_StartMSN：Translate__Special_121_End" />
                            <ext:TextBox ID="txtSina" runat="server" Label="新浪微博：" />
                            <ext:TextBox ID="OtherContact" runat="server" Label="其他联系方式：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="Hobbies" runat="server" Label="兴趣爱好：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="SpRemark" runat="server" Label="备注：" MaxLength="512">
                            </ext:TextBox>
                            <ext:SimpleForm runat="server" ID="SimpleForm" ShowHeader="false" EnableBackgroundColor="true"
                                ShowBorder="false">
                                <Items>
                                    <ext:HiddenField runat="server" ID="PicturePath">
                                    </ext:HiddenField>
                                    <ext:Form ID="FormLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                                        runat="server" HideMode="Offsets">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 80% 10%" runat="server">
                                                <Items>
                                                    <ext:HiddenField runat="server" ID="uploadFilePath">
                                                    </ext:HiddenField>
                                                    <ext:FileUpload ID="MemberPictureFile" runat="server" Label="会员照片：">
                                                    </ext:FileUpload>
                                                    <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" CssClass="mleft20"
                                                        OnClick="btnBack_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                                        runat="server" HideMode="Display" Hidden="true">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="68% 15% 17%" runat="server">
                                                <Items>
                                                    <ext:Label ID="Label1" runat="server" Label="会员照片：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                                                    </ext:Button>
                                                    <ext:Button ID="btnReUpLoad" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:SimpleForm>
                            <ext:Label ID="lblDesc" runat="server" Text="*为必填项" CssStyle="font-size:12px;color:red">
                            </ext:Label>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="所绑定的卡的列表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="CardNumber" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="品牌">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("CardTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="现有金额">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("TotalAmount","{0:0.00}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="现有积分">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("TotalPoints")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="WindowPic1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    </form>
</body>
</html>
