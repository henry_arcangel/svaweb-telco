﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.ImportBIFile.Modify" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
            <Items>
            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:Label ID="CouponDispenseNumber" runat="server" Label="交易编号：">
                    </ext:Label>
   
                    <ext:Label ID="ApproveStatus" runat="server" Label="交易状态：">
                    </ext:Label>

                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                    </ext:Label>
    
                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                    </ext:Label>

                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                    </ext:Label>

                    <ext:Label ID="CreatedByName" runat="server" Label="创建人：">
                    </ext:Label>
                    <ext:HiddenField ID="CreatedBy" runat="server" />

                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                    </ext:Label>

                    <ext:Label ID="ApproveByName" runat="server" Label="批核人：">
                    </ext:Label>
                    <ext:HiddenField ID="ApproveBy" runat="server" />

                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                    </ext:Label>
  
                    <ext:TextBox ID="Note" runat="server" Label="备注：" MaxLength="512">
                    </ext:TextBox>  
                    
                    <ext:HiddenField ID="Description" runat="server"></ext:HiddenField>                  
                </Items>
            </ext:SimpleForm>
            </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="优惠劵资料">
            <Items>
                <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" DataKeyNames="CouponDispenseNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="50px" HeaderText="优惠劵类型编号">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%# Eval("CouponTypeCode") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="50px" HeaderText="优惠劵类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("CouponTypeIDName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="50px" HeaderText="会员手机号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("MemberRegisterMobile")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="50px" HeaderText="活动编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("CampaignCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="导出时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("ExportDatetime","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="创建日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateBy" runat="server" Text='<%#Eval("CreatedDate", "{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
            </Items>
        </ext:GroupPanel>
        </Items>
    </ext:Panel>
    </form>
</body>

</html>
