﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.ImportMemberInfos1.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="from1,form2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarFill ID="ToolbarFill3" runat="server">
                    </ext:ToolbarFill>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="交易信息"
                AutoHeight="true" AutoWidth="true">
            <Items>
                <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                    <Items>
                        <ext:HiddenField ID="CreatedBy" runat="server" />
                        <ext:HiddenField ID="ApproveBy" runat="server" />
                        <ext:HiddenField ID="ApproveStatus" runat="server" Text="P" />
                        <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                            <Rows>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="CreateMemberNumber" runat="server" Label="交易编号：">
                                        </ext:Label>
                                        <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                        </ext:Label>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                        </ext:Label>
                                        <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                        </ext:Label>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                        </ext:Label>
                                        <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                        </ext:Label>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                        </ext:Label>
                                        <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                        </ext:Label>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                        </ext:Label>
                                        <ext:TextBox ID="Remark" runat="server" Label="备注：">
                                        </ext:TextBox>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:RadioButtonList ID="BindCardFlag" runat="server" Label="是否绑定卡：" Width="200px" Required="true" 
                                            ShowRedStar="true" OnSelectedIndexChanged="BindCardFlag_SelectedIndexChanged" AutoPostBack="true">
                                            <ext:RadioItem Text="是" Value="1" />
                                            <ext:RadioItem Text="否" Value="0" />
                                        </ext:RadioButtonList>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Resizable="true" 
                                            Enabled="false" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" AutoPostBack="true">
                                        </ext:DropDownList>
                                        <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别：" Resizable="true" Enabled="false">
                                        </ext:DropDownList>
                                    </Items>
                                </ext:FormRow>
                                <ext:FormRow>
                                    <Items>
                                        <ext:Label ID="ImportFile" runat="server" Label="导入文件：">
                                        </ext:Label>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                        </ext:Form>
                    </Items>
                </ext:SimpleForm>
            </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="详细信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="CreateMemberNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="国家码">
                                <ItemTemplate>
                                    <asp:Label ID="glblApproveStatus" runat="server" Text='<%#Eval("CountryCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="手机号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("MobileNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="英文名（姓）">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("EngFamilyName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="英文名（名）">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("EngGivenName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="中文名（姓）">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveOn" runat="server" Text='<%#Eval("ChiFamilyName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="中文名（名）">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBy2" runat="server" Text='<%#Eval("ChiGivenName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="生日">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%#Eval("Birthday","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="性别">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("GenderName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="100px" HeaderText="详细地址">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%#Eval("HomeAddress")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        Target="Top" IsModal="True" Width="500px" Height="350px"> 
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
