﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ParentChildList.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery.ParentChildList" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="子卡列表" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                runat="server" EnableCheckBoxSelect="True" DataKeyNames="ParentCardNumber,ChildCardNumber" AllowPaging="true"
                IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                OnPageIndexChange="Grid1_PageIndexChange">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server">
                        <Items>
                            <ext:Button ID="btnNew" Text="新增" Icon="Add" runat="server" OnClick="btnNew_OnClick">
                            </ext:Button>
                            <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Columns>
                    <ext:TemplateField Width="60px" HeaderText="主卡编号">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("ParentCardNumber")%>'></asp:Label>
                        </ItemTemplate>
                    </ext:TemplateField>
                    <ext:TemplateField Width="60px" HeaderText="子卡编号">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("ChildCardNumber")%>'></asp:Label>
                        </ItemTemplate>
                    </ext:TemplateField>
                </Columns>
            </ext:Grid>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="Window1" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack"  IFrameUrl="about:blank" OnClose="WindowEdit_Close"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="450px"
        Height="250px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
