﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPR.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardOrderFromSupplierPointAmount.PrintPR" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div id="toolsbar" align="center" style="width: 100%; text-align: center;">
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table border="0" >
                    <tr>
                        <td align="left"  colspan="5">
                            PURCHASE ORDER
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" >
                            ________________________________________________________________________________________________________________________________________________________________
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="10%">
                            VENDOR:
                        </td>
                        <td align="left" colspan="4" >
                            <%#Eval("SupplierName")%>
                        </td>

                    </tr>
                    <tr>
                        <td width="10%" align="left">
                            SHIP TO:
                        </td>
                        <td width="25%" align="left" colspan="4">
                            <%#Eval("CompanyName")%>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                            ADDRESS:
                        </td>
                        <td width="25%" colspan="4">
                            <%#Eval("CompanyAddress")%>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                            BUYER:
                        </td>
                        <td width="25%" colspan="4">
                            <%#Eval("Buyer")%>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="5">
                            ________________________________________________________________________________________________________________________________________________________________
                        </td>
                    </tr>
                    <tr>
                    <tr>
                        <td colspan="10">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td width="10%">
                                                STYLE CODE/<br>SKU CODE
                                            </td>
                                            <td  width="15%">
                                                STYLE DESCRIPTION/SKU<br>DESCRIPTION
                                            </td>
                                            <td  width="5%">
                                                UPC
                                            </td>
                                            <td  width="10%">
                                                VENDER PART<br />NUMBER
                                            </td>
                                            <td width="10%">
                                                BUYING<br />UOM
                                            </td>
                                            <td align="right">
                                                QTY<br />ORDERED
                                            </td>
                                            <td align="right" width="10%">
                                                UNIT RETAIL
                                            </td>
                                            <td align="right" width="10%">
                                                UNIT COST
                                            </td>
                                            <td align="right" width="10%">
                                                GM%
                                            </td>
                                            <td align="right" width="10%">
                                                TOTAL NET<br />COST
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" colspan="10">
                                                  ________________________________________________________________________________________________________________________________________________________________
                                            </td>
                                       </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblSKU" runat="server" Text='<%#Eval("SKU")%>'></asp:Label>
                                        </td>
                                        <td align="left" width="15%">
                                            <asp:Label ID="lblSKUDesc" runat="server" Text='<%#Eval("SKUDesc")%>'></asp:Label>
                                        </td>
                                        <td align="left" width="5%">
                                            <asp:Label ID="lblUPC" runat="server" Text='<%#Eval("UPC")%>'></asp:Label>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblVPTNumber" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblUOM" runat="server" Text="PCS"></asp:Label>
                                        </td>
                                        <td align="right" width="10%">
                                            <asp:Label ID="lblOrderAmount" runat="server" Text='<%#Eval("OrderAmount","{0:F2}")%>'></asp:Label>
                                        </td>
                                        <td align="right" width="10%">
                                            <asp:Label ID="lblUnitRetail" runat="server" Text='<%#Eval("UnitRetail")%>'></asp:Label>
                                        </td>
                                        <td align="right" width="10%">
                                            <asp:Label ID="lblUnitCost" runat="server" Text='<%#Eval("UnitCost")%>'></asp:Label>
                                        </td>
                                        <td align="right" width="10%">
                                            <asp:Label ID="lblGM" runat="server" Text='<%#Eval("GM")%>'></asp:Label>
                                        </td>
                                        <td align="right" width="10%">
                                            <asp:Label ID="lblNetCost" runat="server" Text='<%#Eval("NetCost")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td width="100%" colspan="10">
                                             ________________________________________________________________________________________________________________________________________________________________
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5">
                                            TOTAL GROSS AMOUNT
                                        </td>
                                        <td align="right" colspan="1">
                                         <asp:Label ID="lblTotalGrossAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="right" colspan="1">
                                         <asp:Label ID="lblTotalRetailAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="right" colspan="3">
                                         <asp:Label ID="lblTotalAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            LESS:TOTAL DISCOUNTS
                                        </td>
                                        <td align="right" colspan="6">
                                           <asp:Label ID="lblTotalDiscountAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            TOTAL NET AMOUNT
                                        </td>
                                        <td align="right" colspan="6">
                                           <asp:Label ID="lblTotalNetAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
