﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.BatchCreationOfCards.Add"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--   <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            jQuery.validator.addMethod("twoDecimal", function (value, element) {
                return checkTwoDecimal(value);
            }, jQuery.validator.messages.TwoDecimal);
        });

    </script>
    <style type="text/css">
        .submit
        {
            height: 26px;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Text="P">
                    </ext:HiddenField>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardCreateNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Note" runat="server" Label="备注：" MaxLength="512">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="交易资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged"
                                        AutoPostBack="true" Required="true" ShowRedStar="true" Resizable="true" CompareType="String"
                                        CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别：" OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged"
                                        AutoPostBack="true" Required="true" ShowRedStar="true" Resizable="true" CompareType="String"
                                        CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="BatchCardID" runat="server" Label="卡批次编号：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="InitAmount" runat="server" Label="卡初始金额：" MaxValue="100000000"
                                        ShowRedStar="true" Required="true" NoNegative="true">
                                    </ext:NumberBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="InitPoints" runat="server" Label="卡初始积分：" MaxValue="100000000"
                                        ShowRedStar="true" Required="true" NoNegative="true" NoDecimal="true">
                                    </ext:NumberBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="txtCardStatus" runat="server" Label="状态：" Enabled="false" Text="Dormant"
                                        ShowRedStar="true">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="CreatedCards" runat="server" Label="已创建卡数量：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="RemainCards" runat="server" Label="剩余可创建卡数量：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="LastCreatedCards" runat="server" Label="已创建的最后一个卡号码：" MaxLength="512"
                                        Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="CardCount" runat="server" Label="卡的数量：" ShowRedStar="true" Required="true"
                                        NoNegative="true" NoDecimal="true" MaxValue="100000000">
                                    </ext:NumberBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="IssuedDate" runat="server" Label="创建日期：" DateFormatString="yyyy-MM-dd"
                                        MaxLength="512" Enabled="false">
                                    </ext:DatePicker>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="ExpiryDate" runat="server" Label="过期日期：" DateFormatString="yyyy-MM-dd"
                                        MaxLength="512" CompareControl="IssuedDate" CompareOperator="GreaterThanEqual"
                                        CompareMessage="过期日期日期应该大于创建日期" ShowRedStar="true" Required="true">
                                    </ext:DatePicker>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <%--   <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                基本信息
            </th>
        </tr>
        <tr>
            <td align="right">
                交易编号：
            </td>
            <td>
                <asp:Label ID="CardCreateNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易状态：
            </td>
            <td>
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="Note" TabIndex="1" runat="server" MaxLength="512" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                交易资料
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡类型：
            </td>
            <td width="75%">
                <asp:DropDownList ID="CardTypeID" TabIndex="2" runat="server" CssClass="dropdownlist required"
                    AutoPostBack="True" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged">
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡级别：
            </td>
            <td width="75%">
                <asp:DropDownList ID="CardGradeID" TabIndex="2" runat="server" CssClass="dropdownlist required"
                    AutoPostBack="True" OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged">
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡批次编号：
            </td>
            <td>
                <asp:TextBox ID="BatchCardID" runat="server" MaxLength="512" CssClass="input" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡初始金额：
            </td>
            <td>
                <asp:TextBox ID="InitAmount" TabIndex="3" runat="server" MaxLength="512" CssClass="input required svaAmount"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡初始积分：
            </td>
            <td>
                <asp:TextBox ID="InitPoints" TabIndex="3" runat="server" MaxLength="512" CssClass="input required svaPoint"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:TextBox ID="txtCardStatus" runat="server" Text="Dormant" Enabled="false"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                已创建卡数量：
            </td>
            <td>
                <asp:TextBox ID="CreatedCards" TabIndex="4" runat="server" MaxLength="512" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                剩余可创建卡数量：
            </td>
            <td>
                <asp:TextBox ID="RemainCards" TabIndex="4" runat="server" MaxLength="512" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                已创建的最后一个卡号码：
            </td>
            <td>
                <asp:TextBox ID="LastCreatedCards" TabIndex="4" runat="server" MaxLength="512"
                    Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡的数量：
            </td>
            <td>
                <asp:TextBox ID="CardCount" TabIndex="5" runat="server" MaxLength="512" CssClass="input required svaQty"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:TextBox ID="IssuedDate" TabIndex="6" runat="server" MaxLength="512" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'ExpiryDate\',{d:0});}'})"
                    CssClass="input required" Enabled="false"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                过期日期：
            </td>
            <td>
                <asp:TextBox ID="ExpiryDate" TabIndex="7" runat="server" MaxLength="512" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'IssuedDate\',{d:0});}'})"
                    CssClass="input required"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit"
                        OnClientClick="return confirm( 'Are you sure? ');"></asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
