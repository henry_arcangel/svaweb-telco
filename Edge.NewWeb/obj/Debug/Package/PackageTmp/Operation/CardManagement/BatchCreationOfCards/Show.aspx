﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.BatchCreationOfCards.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:Button ID="btnExport" runat="server" Text="导 出" OnClick="btnExport_Click" Icon="PageExcel"
                        EnableAjax="false" DisableControlBeforePostBack="false">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Text="P">
                    </ext:HiddenField>
                    <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardCreateNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Note" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="交易资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardTypeID" runat="server" Label="卡类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardGradeID" runat="server" Label="卡级别：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="BatchCardID" runat="server" Label="卡批次编号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="InitAmount" runat="server" Label="卡初始金额：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="InitPoints" runat="server" Label="卡初始积分：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardStatus" runat="server" Label="状态：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedCards" runat="server" Label="已创建卡数量：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="RemainCards" runat="server" Label="剩余可创建卡数量：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="LastCreatedCards" runat="server" Label="已创建的最后一个卡号码：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardCount" runat="server" Label="卡的数量：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="IssuedDate" runat="server" Label="创建日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ExpiryDate" runat="server" Label="过期日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <%--    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                基本信息
            </th>
        </tr>
        <tr>
            <td align="right">
                交易编号：
            </td>
            <td>
                <asp:Label ID="CardCreateNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易状态：
            </td>
            <td>
                <asp:Label ID="ApproveStatus" runat="server" Text="P"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核工作日：
            </td>
            <td>
                <asp:Label ID="ApproveBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核时间：
            </td>
            <td>
                <asp:Label ID="ApproveOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="lblApproveBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                授权号：
            </td>
            <td>
                <asp:Label ID="ApprovalCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="Note" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                交易资料
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡类型：
            </td>
            <td width="75%">
                <asp:Label ID="CardTypeID" runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡级别：
            </td>
            <td width="75%">
                <asp:Label ID="CardGradeID" TabIndex="2" runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡批次编号：
            </td>
            <td>
                <asp:Label ID="BatchCardID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡初始金额：
            </td>
            <td>
                <asp:Label ID="InitAmount" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡初始积分：
            </td>
            <td>
                <asp:Label ID="InitPoints" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="CardStatus" runat="server" Text="Dormant"></asp:Label>
            </td>
        </tr>
              <tr>
            <td align="right">
                已创建卡数量：
            </td>
            <td>
                <asp:Label ID="CreatedCards" TabIndex="4" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                剩余可创建卡数量：
            </td>
            <td>
                <asp:Label ID="RemainCards" TabIndex="4" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                已创建的最后一个卡号码：
            </td>
            <td>
                <asp:Label ID="LastCreatedCards" TabIndex="4" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡的数量：
            </td>
            <td>
                <asp:Label ID="CardCount" TabIndex="4" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:Label ID="IssuedDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                过期日期：
            </td>
            <td>
                <asp:Label ID="ExpiryDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="导 出" CssClass="submit" />
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
