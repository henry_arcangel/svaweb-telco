﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPR.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic.PrintPR" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div id="toolsbar" align="center" style="width: 100%; text-align: center;">
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" >
                    <tr>
                        <th colspan="4" align="center">
                            MEMO
                        </th>
                    </tr>
                    <tr>
                        <td width="25%">
                            Date:
                        </td>
                        <td width="25%">
                            <%#Eval("ApproveOn")%>
                        </td>
                        <td width="25%">
                            &nbsp;
                        </td>
                        <td width="25%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td>
                            <%#Eval("SupplierName")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td>
                            <%#Eval("CompanyName")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Re:
                        </td>
                        <td>
                            <%#Eval("Subject")%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            This is to request the preparation of a Purchase Order for the following item/s:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Item:
                        </td>
                        <td colspan="2">
                            <tr>
                            <td colspan="3">
                                <asp:Repeater ID="rptCardGradeList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblQty" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                            </tr>
                        </td>
                        <%--<td colspan="2">
                            <%#Eval("CardGradeName")%>
                        </td>--%>
                    </tr>
                    <tr>
                        <td>
                            Details as follows:
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td align="center">
                            (Special Instructions)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <th width="10%">
                                                GC Denomination
                                            </th>
                                            <th width="10%">
                                                Qty
                                            </th>
                                            <th width="10%">
                                                Unit
                                            </th>
                                            <th width="10%">
                                                Unit Cost<br />(vat inclusive)
                                            </th>
                                            <th width="10%">
                                                Amount<br />(vat inclusive)
                                            </th>
                                            <th width="10%">
                                                Expiry Date
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblCardGradeAmount" runat="server" Text='<%#Eval("CardGradeAmount")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblQty" runat="server" Text='<%#Eval("Qty")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblUnit" runat="server" Text="bundles"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblUnitCost" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblExpiryDate" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="left">
                                            TOTAL
                                        </td>
                                        <td align="left">
                                           <asp:Label ID="lblTotalQty" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            bundles
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            GC Series -
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList2" runat="server" OnItemDataBound="rptOrderList2_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <th width="40%">
                                                Denomination
                                            </th>
                                            <th width="10%">
                                                Prefix
                                            </th>
                                            <th width="20%">
                                                From
                                            </th>
                                            <th width="20%">
                                               To
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblCardGradeAmountAddCardQty" runat="server" Text='<%#Eval("CardGradeAmountAddCardQty")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblCardNumPattern" runat="server" Text='<%#Eval("CardNumPattern")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblFirstCardNumber" runat="server" Text='<%#Eval("FirstCardNumber")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblEndCardNumber" runat="server" Text='<%#Eval("EndCardNumber")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Thank you.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                        </td>
                        <%--<td colspan="2">
                            Requirements based on 4 months regular
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <%--<td colspan="2">
                            sales forecast
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <%--<td colspan="2">
                            Staggered delivery. Paid as delivered.
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            ____________________
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedByUserName")%>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            ____________________
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Eval("ApprovedByUserName")%>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
