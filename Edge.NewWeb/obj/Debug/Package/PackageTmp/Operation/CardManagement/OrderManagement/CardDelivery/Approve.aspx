﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Approve.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardDelivery.Approve" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>--%>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtablelist">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <th colspan="2" align="left">
                    <%=this.PageName %>
                </th>
            </tr>
            <tr>
                <td width="25%">
                    交易编号:
                </td>
                <td width="75%">
                    <asp:Label ID="TxnNo" runat="server" Text='<%#Eval("TxnNo") %>'></asp:Label>
                </td>
            </tr>
            <tr style="text-align: center; color: Red; font-size: large;">
                <td>
                    <%#Eval("ApprovalMsg")%>
                </td>
                <td>
                    <asp:Label ID="errorMsg" runat="server" Text='<%#Eval("ApproveCode") %>'></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div style="padding-bottom: 10px;">
        <div style="text-align: center;">
<%--            <input type="button" value="关 闭" class="submit" onclick="javascript:window.top.tb_remove();parent.frames['sysMain'].location.href= 'List.aspx' " />--%>
            <%--<asp:Button ID="btnPrint" runat="server" Text="打印" OnClientClick="printdiv('div_print')" />--%>
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
   <asp:RadioButtonList ID="NeedActive" Label="是否激活" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem Value="1" Text="是"></asp:ListItem>
        <asp:ListItem Value="0" Text="否" Selected="True"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:RadioButtonList ID="CustomerType" Label="客户类型" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal">
        <asp:ListItem Text="客户订货" Value="1"></asp:ListItem>
        <asp:ListItem Text="店铺订货" Value="2" Selected="True"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:DropDownList ID="SendMethod" Label="发送方式" runat="server" CssClass="dropdownlist">
        <asp:ListItem Text="直接交付（打印）" Value="1" Selected="True"></asp:ListItem>
        <asp:ListItem Text="SMS" Value="2"></asp:ListItem>
        <asp:ListItem Text="Email" Value="3"></asp:ListItem>
        <asp:ListItem Text="Social Network" Value="4"></asp:ListItem>
    </asp:DropDownList>
    <div id="div_print" runat="server">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtable">
                    <tr>
                        <th colspan="4" align="center">
                            发货单列表
                        </th>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            交易信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" width="25%">
                            交易编号：
                        </td>
                        <td width="25%">
                            <%#Eval("CardDeliveryNumber")%>
                        </td>
                        <td align="right" width="25%">
                            交易状态：
                        </td>
                        <td width="25%">
                            <%#Eval("ApproveStatus")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            交易创建工作日期：
                        </td>
                        <td>
                            <%#Eval("CreatedBusDate")%>
                        </td>
                        <td align="right">
                            交易批核工作日期：
                        </td>
                        <td>
                            <%#Eval("ApproveBusDate")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            交易创建时间：
                        </td>
                        <td>
                            <%#Eval("CreatedOn")%>
                        </td>
                        <td align="right">
                            创建人：
                        </td>
                        <td>
                            <%#Eval("CreatedBy")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            批核时间：
                        </td>
                        <td>
                            <%#Eval("ApproveOn")%>
                        </td>
                        <td align="right">
                            批核人：
                        </td>
                        <td>
                            <%#Eval("ApproveBy")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            授权号：
                        </td>
                        <td>
                            <%#Eval("ApprovalCode")%>
                        </td>
                        <td align="right">
                            备注：
                        </td>
                        <td>
                            <%#Eval("Remark")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            是否激活卡：
                        </td>
                        <td colspan="3">
                            <%#Eval("NeedActive")%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            订单信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" colspan="1">
                            订单类型：
                        </td>
                        <td>
                            <%#Eval("CustomerType")%>
                        </td>
                        <td align="right">
                            订货数量:
                        </td>
                        <td>
                            <%#Eval("OrdersCount")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            订货品牌：
                        </td>
                        <td>
                            <%#Eval("Brand")%>
                        </td>
                        <td align="right">
                            店铺：
                        </td>
                        <td>
                            <%#Eval("StoreID")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            订货客户：
                        </td>
                        <td>
                            <%#Eval("CustomerID")%>
                        </td>
                        <td align="right">
                            送货单发送方式：
                        </td>
                        <td>
                            <%#Eval("SendMethod")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            送货地址：
                        </td>
                        <td>
                            <%#Eval("SendAddress")%>
                        </td>
                        <td align="right">
                            联系人：
                        </td>
                        <td>
                            <%#Eval("ContactName")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            邮件发送：
                        </td>
                        <td>
                            <%#Eval("Email")%>
                        </td>
                        <td align="right">
                            SMS/MMS发送：
                        </td>
                        <td>
                            <%#Eval("SMSMMS")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            联系电话：
                        </td>
                        <td>
                            <%#Eval("ContactNumber")%>
                        </td>
                        <td align="right">
                            备注：
                        </td>
                        <td>
                            <%#Eval("Remark")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtablelist">
                                        <tr>
                                            <th width="6%">
                                                序号
                                            </th>
<%--                                            <th width="10%">
                                                卡类型编号
                                            </th>
                                            <th width="10%">
                                                卡类型
                                            </th>--%>
                                                <th width="10%">
                                    卡级别编号
                                </th>
                                <th width="6%">
                                    卡级别
                                </th>
                                            <th width="10%">
                                                订单数量
                                            </th>
                                            <th width="10%">
                                                捡货数量
                                            </th>
                                            <th width="10%">
                                                卡起始编号
                                            </th>
                                            <th width="10%">
                                                卡结束编号
                                            </th>
                                            <%--                            <th width="10%">
                                    卡批次编号
                                </th>--%>
                                            <%--                                <th width="10%">
                                    捡货时间
                                </th>--%>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblSeq" runat="server" Text=''></asp:Label>
                                        </td>
                                        <td align="center">
<%--                                            <asp:Label ID="lblCardTypeCode" runat="server" Text='<%#Eval("CardTypeCode")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCardType" runat="server" Text='<%#Eval("CardType")%>'></asp:Label>
                                        </td>--%>
                                         <td align="center">
                             <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>   
                            </td>
                            <td align="center">
                            <asp:Label ID="lblCardGrade" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>  
                            </td>
                                        <td align="center">
                                            <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblFirstCardNumber" runat="server" Text='<%#Eval("FirstCardNumber")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblEndCardNumber" runat="server" Text='<%#Eval("EndCardNumber")%>'></asp:Label>
                                        </td>
                                        <%--              <td align="center">
                                <asp:Label ID="lblCardBatchID" runat="server" Text='<%#Eval("CardBatchID")%>'></asp:Label>
                            </td>--%>
                                        <%--                            <td align="center">
                             <asp:Label ID="lblPickDatetime" runat="server" Text='<%#Eval("PickDatetime")%>'></asp:Label>
                            </td>--%>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="center" colspan="3">
                                            总计：
                                        </td>
                                        <td align="center"  colspan="2">
                                             订单数量总计：<asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" colspan="2">
                                            捡货数量总计：<asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
