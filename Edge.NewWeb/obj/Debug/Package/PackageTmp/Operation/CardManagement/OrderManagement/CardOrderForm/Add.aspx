﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardOrderForm.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript"> 
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );

            //检查客户类型
            $("input[name='CustomerType']").change(function () {
                checkOrderType();

                //清空记录
                $("#SendAddress,#ContactName,#ContactNumber").val("");

            });

            checkOrderType();
        });
        function checkDetail() {
            $("#StoreID,#ddlBrand,#CustomerID,#SendMethod,#SendAddress,#ContactName,#ContactNumber").removeClass("required");
            $("#brandDetail,#ddlCardType,#txtOrderCount").addClass("required");
        }
        function checkAdd() {
            $("#SendMethod,#SendAddress,#ContactName,#ContactNumber").addClass("required");
            $("#brandDetail,#ddlCardType,#txtOrderCount").removeClass("required");

            checkOrderType();
        }

        //检查客户类型
        function checkOrderType() {
            var type = $("input[name='CustomerType']:checked").val();
            if (type == '1') //客户
            {

                $('select#ddlBrand')[0].selectedIndex = 0;
                $('select#StoreID')[0].selectedIndex = 0; 

                $("#CustomerID").addClass("required");
                $("#StoreID,#ddlBrand").removeClass("required");

                $("#StoreID,#ddlBrand").attr("disabled", "disabled");
                $("#CustomerID").removeAttr("disabled");

            }
            else {//店铺

                $('select#CustomerID')[0].selectedIndex = 0; 


                $("#StoreID,#ddlBrand").addClass("required");
                $("#CustomerID").removeClass("required");

                $("#CustomerID").attr("disabled", "disabled");
                $("#StoreID,#ddlBrand").removeAttr("disabled");



            }
        }
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="订单信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Label="交易状态：" Text="P">
                    </ext:HiddenField>
                    <ext:HiddenField ID="OrderType" runat="server" Label="订单类型：" Text="0">
                    </ext:HiddenField>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardOrderFormNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="lblOrderType" runat="server" Label="订单类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Remark" runat="server" Label="备注：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="库存(出)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="FromStoreID" runat="server" Label="总部：" OnSelectedIndexChanged="StoreID_SelectedIndexChanged2"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:TextBox ID="FromAddress" runat="server" Label="地址：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="FromContactName" runat="server" Label="联系人：">
                                    </ext:TextBox>
                                    <ext:TextBox ID="FromContactNumber" runat="server" Label="联系电话：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="订单内容"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="CustomerType" runat="server" Label="订单类型：" AutoPostBack="True"
                                        OnSelectedIndexChanged="CustomerType_SelectedIndexChanged" Width="250">
                                        <ext:RadioItem Text="客户订货" Value="1" />
                                        <ext:RadioItem Text="店铺订货" Value="2" Selected="true" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="ddlBrand" runat="server" Label="订货品牌：" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" Resizable="true" Required="true" ShowRedStar="true" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">                             
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="StoreID" runat="server" Label="店铺：" OnSelectedIndexChanged="StoreID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CustomerID" runat="server" Label="订货客户：" OnSelectedIndexChanged="CustomerID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="SendMethod" runat="server" Label="送货单发送方式：" Resizable="true">
                                        <ext:ListItem Text="直接交付（打印）" Value="1" Selected="true" />
                                        <ext:ListItem Text="SMS" Value="2" />
                                        <ext:ListItem Text="Email" Value="3" />
                                        <ext:ListItem Text="Social Network" Value="4" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="SendAddress" runat="server" Label="送货地址：" Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="StoreContactName" runat="server" Label="联系人：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="StoreContactEmail" runat="server" Label="邮件发送：">
                                    </ext:TextBox>
                                    <%--<ext:TextBox ID="SMSMMS" runat="server" Label="Translate__Special_121_StartSMS/MMS发送：Translate__Special_121_End">
                                    </ext:TextBox>--%>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="StoreContactPhone" runat="server" Label="联系电话：" Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="Remark1" runat="server" Label="备注：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="订单明细"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="brandDetail" runat="server" Label="品牌：" AutoPostBack="true"
                                        OnSelectedIndexChanged="brandDetail_SelectedIndexChanged" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别："
                                        Resizable="true" Required="true" ShowRedStar="true" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="txtOrderCount" runat="server" Label="订货数量：" MaxValue="100000000" 
                                        NoDecimal="true" NoNegative="true" Required="true" ShowRedStar="true">
                                    </ext:NumberBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="ID" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_PageIndexChange" OnRowCommand="Grid1_RowCommand">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server" Position="Top" CssStyle="width: 100%;">
                                <Items>
                                    <ext:Button ID="btnAddDetail" Icon="Add" EnablePostBack="true" runat="server" Text="添加"
                                        OnClick="btnAddDetail_Click">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
<%--                            <ext:TemplateField Width="60px" HeaderText="品牌编号">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["BrandCode"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="品牌">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["BrandName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="卡级别编号">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CardGradeCode"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CardGradeName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订货数量">
                                <ItemTemplate>
                                    <%#int.Parse(((System.Data.DataRow)Container.DataItem)["CardQty"].ToString()).ToString("N00")%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:LinkButtonField HeaderText="&nbsp;" Width="60px" CommandName="Delete" Text="删除" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <%--    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="4" align="left">
                订单信息
            </th>
        </tr>
        <tr>
            <td width="15%" align="right">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CardOrderFormNumber" runat="server" MaxLength="512"></asp:Label>
            </td>
            <td width="15%" align="right">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td colspan="3">
                <asp:Label ID="CreatedByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                订单内容
            </th>
        </tr>
        <tr>
            <td align="right" colspan="1">
                订单类型：
            </td>
            <td colspan="3">
                <asp:RadioButtonList ID="CustomerType" TabIndex="1" runat="server" RepeatLayout="Table"
                    RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Text="客户订货" Value="1"></asp:ListItem>
                    <asp:ListItem Text="店铺订货" Value="2" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货品牌：
            </td>
            <td>
                <asp:DropDownList ID="ddlBrand" runat="server" TabIndex="2" CssClass="dropdownlist"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="right">
                店铺：
            </td>
            <td>
                <asp:DropDownList ID="StoreID" runat="server" TabIndex="3" 
                    CssClass="dropdownlist required" 
                    onselectedindexchanged="StoreID_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货客户：
            </td>
            <td>
                <asp:DropDownList ID="CustomerID" runat="server" TabIndex="4" 
                    CssClass="dropdownlist" 
                    onselectedindexchanged="CustomerID_SelectedIndexChanged" 
                    AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="right">
                送货单发送方式：
            </td>
            <td>
                <asp:DropDownList ID="SendMethod" runat="server" CssClass="dropdownlist" TabIndex="5">
                    <asp:ListItem Text="直接交付（打印）" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="SMS" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Email" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Social Network" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货地址：
            </td>
            <td>
                <asp:TextBox ID="SendAddress" runat="server" TabIndex="6" CssClass="input"></asp:TextBox>
            </td>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:TextBox ID="ContactName" runat="server" TabIndex="7" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮件发送：
            </td>
            <td>
                <asp:TextBox ID="Email" runat="server" CssClass="input" TabIndex="8"></asp:TextBox>
            </td>
            <td align="right">
                SMS/MMS 发送：
            </td>
            <td>
                <asp:TextBox ID="SMSMMS" runat="server" CssClass="input" TabIndex="9"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:TextBox ID="ContactNumber" runat="server" CssClass="input" TabIndex="10"></asp:TextBox>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="Remark" runat="server" CssClass="input" TabIndex="11"></asp:TextBox>
            </td>
        </tr>
        <tr id="tranctionDetial">
            <th colspan="2" align="left">
                订单明细：
            </th>
            <th colspan="2" align="right">
                <asp:Button ID="btnAddDetail" runat="server" Text="添 加" OnClick="btnAddDetail_Click"
                    OnClientClick="checkDetail();" CssClass="submit" />
            </th>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:DropDownList ID="brandDetail" runat="server" TabIndex="12" AutoPostBack="true"
                    OnSelectedIndexChanged="brandDetail_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="right">
                优惠劵类型：
            </td>
            <td>
                <asp:DropDownList ID="ddlCardType" runat="server" TabIndex="13">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货数量：
            </td>
            <td>
                <asp:TextBox ID="txtOrderCount" runat="server" CssClass="input svaQty" TabIndex="15"></asp:TextBox>
                <a name="brandDetail"></a>
            </td>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="15%">
                                    品牌编号
                                </th>
                                <th width="25%">
                                    品牌
                                </th>
                                <th width="15%">
                                    优惠劵类型编号
                                </th>
                                <th width="25%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    订货数量
                                </th>
                                <th width="10%">
                                    &nbsp;
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["BrandCode"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["BrandName"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["CardTypeCode"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["CardTypeName"]%>
                            </td>
                            <td align="center">
                                <%#int.Parse(((System.Data.DataRow)Container.DataItem)["CardQty"].ToString()).ToString("N00")%>
                            </td>
                            <td>
                                <span class="btn_bg">
                                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="cancel" OnClick="btnDelete_Click"
                                        Cardid='<%#((System.Data.DataRow)Container.DataItem)["ID"] %>' Text="删 除" />
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提 交" OnClick="btnAdd_Click" CssClass="submit"
                        OnClientClick="checkAdd();"></asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

