﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.TelcoManagement.TelcoStoreWalletTransfer.Add" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/BatchAutoComplete.ascx" TagName="batchAutoComplete"
    TagPrefix="bac" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="from1,form2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarFill ID="ToolbarFill3" runat="server">
                    </ext:ToolbarFill>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="交易信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="CreatedBy" runat="server" />
                    <ext:HiddenField ID="ApproveBy" runat="server" />
                    <ext:HiddenField ID="ApproveStatus" runat="server" Text="P" />
                    <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardTransferNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
									<ext:Label ID="lblOrderType" runat="server" Label="交易类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
								    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
									<ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>							
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>   
                                    <ext:TextBox ID="Remark" runat="server" Label="备注：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>					
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="Stock(Exit) Location Information"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow runat="Server">
                                <Items>
                                    <ext:DropDownList ID="FromBrandID" runat="server" Label="Brand:" ShowRedStar="true" Required="true"
                                        AutoPostBack="True" OnSelectedIndexChanged="FromBrandID_SelectedIndexChanged" Resizable="true"
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>	
                                    <ext:DropDownList ID="FromStoreID" runat="server" Label="Store Code:" ShowRedStar="true" Required="true"
                                        AutoPostBack="True" OnSelectedIndexChanged="FromStoreID_SelectedIndexChanged" Resizable="true"
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>									
                                </Items>
                            </ext:FormRow>   
                            <ext:FormRow ID="FormRow1" runat="Server">
                                <Items>                                    
                                    <ext:TextBox ID="FromCardNumber" Label="FromCardNumber" runat="Server" Hidden="true"></ext:TextBox>
                                </Items>
                            </ext:FormRow>                                                    
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="Stock(Entry) Location Information"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow ID="FormRow2" runat="Server">
                                <Items>
                                    <ext:DropDownList ID="ToBrandID" runat="server" Label="Brand:" ShowRedStar="true" Required="true"
                                        AutoPostBack="True" OnSelectedIndexChanged="ToBrandID_SelectedIndexChanged" Resizable="true"
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>	
                                    <ext:DropDownList ID="ToStoreID" runat="server" Label="Store Code:" ShowRedStar="true" Required="true"
                                        AutoPostBack="True" OnSelectedIndexChanged="ToStoreID_SelectedIndexChanged" Resizable="true"
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>									
                                </Items>
                            </ext:FormRow>   
                            <ext:FormRow ID="FormRow3" runat="Server">
                                <Items>                                    
                                    <ext:TextBox ID="ToCardNumber" Label="ToCardNumber" runat="Server" Hidden="true"></ext:TextBox>
                                </Items>
                            </ext:FormRow>                                                    
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="Transfer Detail"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>                        								
                            <ext:FormRow runat="Server">
                                <Items>                                       
                                     <ext:DropDownList ID="CardTypeID" runat="server" AutoPostBack="true" Label="Card Type:"
                                                OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" Resizable="true">
                                     </ext:DropDownList>    
                                     <ext:TextBox ID="ActAmount" runat="server" Label="Airload Amount:" MaxLength="512">
                                     </ext:TextBox>                                       									 
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow runat="Server">
                                <Items>
                                     <ext:DropDownList ID="CardGradeID" runat="server" AutoPostBack="true" Label="卡级别："
                                                OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged" Resizable="true">
                                     </ext:DropDownList>
                                </Items>
                            </ext:FormRow>                                                       							
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>			
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="Order Detail"
                AutoHeight="true" AutoWidth="true">
               <Toolbars>
                        <ext:Toolbar ID="Toolbar3" runat="server">
                            <Items>
                                <ext:Button ID="btnAddSearchItem" Icon="Add" runat="server" Text="添加" OnClick="btnAddSearchItem_Click">
                                </ext:Button>
                                <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                </ext:ToolbarSeparator>
                                <ext:Button ID="btnDeleteResultItem" Icon="Delete" runat="server" Text="删除" OnClick="btnDeleteResultItem_Click">
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill4" runat="server">
                                </ext:ToolbarFill>
                            </Items>
                        </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="GridCardTo" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="3" runat="server" EnableCheckBoxSelect="true" DataKeyNames="CardNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField Width="140px" HeaderText="卡级别编码">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="180px" HeaderText="卡级别名称">
                                <ItemTemplate>
                                    <asp:Label ID="Label22" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="80px" HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%#Eval("ActAmount","{0:f2}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>	
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window EnableClose="true" ID="Window1" Title="" Popup="false" EnableIFrame="true"
        runat="server" CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Self" IsModal="True">
    </ext:Window>

    <ext:Window ID="WindowSearch" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="560px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
