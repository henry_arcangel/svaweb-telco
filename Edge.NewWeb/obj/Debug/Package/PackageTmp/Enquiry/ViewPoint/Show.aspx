﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Enquiry.ViewPoint.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：显示交易信息</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                显示交易
            </th>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:Label ID="CardTypeID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡发行日期：
            </td>
            <td width="75%">
                <asp:Label ID="CardIssueDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡过期日期：
            </td>
            <td>
                <asp:Label ID="CardExpiryDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡级别：
            </td>
            <td>
                <asp:Label ID="CardGradeID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                会员：
            </td>
            <td>
                <asp:Label ID="MemberID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡状态：
            </td>
            <td>
                <asp:Label ID="CardStatus" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                使用次数：
            </td>
            <td>
                <asp:Label ID="UsedCount" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡密码：
            </td>
            <td>
                <asp:Label ID="CardPassword" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                积分余额：
            </td>
            <td>
                <asp:Label ID="TotalPoints" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                金额余额：
            </td>
            <td>
                <asp:Label ID="TotalAmount" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                主卡号：
            </td>
            <td>
                <asp:Label ID="ParentCard" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                时间印戳：
            </td>
            <td>
                <asp:Label ID="SqlPost" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                失效的积分余额：
            </td>
            <td>
                <asp:Label ID="CardForfeitPoints" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                失效的金额余额：
            </td>
            <td>
                <asp:Label ID="CardForfeitAmount" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否需要重置密码：
            </td>
            <td>
                <asp:Label ID="ResetPassword" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡中金额有效期：
            </td>
            <td>
                <asp:Label ID="CardAmountExpiryDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡中积分有效期：
            </td>
            <td>
                <asp:Label ID="CardPointExpiryDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计获得的积分：
            </td>
            <td>
                <asp:Label ID="CumulativeEarnPoints" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计消费金额：
            </td>
            <td>
                <asp:Label ID="CumulativeConsumptionAmt" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:Label ID="CreatedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                更新日期：
            </td>
            <td>
                <asp:Label ID="UpdatedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                更新人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核日期：
            </td>
            <td>
                <asp:Label ID="ApprovedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="Approvedby" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
