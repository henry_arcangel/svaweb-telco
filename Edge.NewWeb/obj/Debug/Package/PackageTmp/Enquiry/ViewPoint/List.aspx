﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Enquiry.ViewPoint.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：查看积分</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="6%">
                        卡号
                    </th>
                    <th width="15%">
                        卡发行日期
                    </th>
                    <th width="15%">
                        卡过期日期
                    </th>
                    <th width="10%">
                        使用次数
                    </th>
                    <th width="15%">
                        积分余额
                    </th>
                    <th width="10%">
                        金额余额
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <a href="show.aspx?id=<%#Eval("CardNumber") %>">
                        <asp:Label ID="lb_id" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label></a>
                </td>
                <td align="center">
                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("CardIssueDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName1" runat="server" Text='<%#Eval("CardExpiryDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName2" runat="server" Text='<%#Eval("UsedCount")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName3" runat="server" Text='<%#Eval("TotalPoints")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("TotalAmount")%>'></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr" runat="server" visible="false">
        </div>
        <uc2:checkright ID="Checkright1" runat="server" />
    </form>

    <script type="text/javascript">
        
          $(function() {
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id);
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

</body>
</html>
