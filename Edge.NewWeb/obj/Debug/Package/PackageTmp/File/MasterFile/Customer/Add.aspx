﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.Customer.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body >
    <form id="form1" runat="server">
      <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="CustomerCode" runat="server" Label="客户编号：" MaxLength="20" Required="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字"
                ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                ToolTipTitle="客户编号" ToolTip="Translate__Special_121_Start1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
            </ext:TextBox>
            <ext:TextBox ID="CustomerDesc1" runat="server" Label="描述：" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="描述" ToolTip="不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="CustomerDesc2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1" ToolTip="對描述的一個補充。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="CustomerDesc3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2" ToolTip="對描述的另一個補充。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="CustomerAddress" runat="server" Label="地址：" MaxLength="512" ToolTipTitle="地址" ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="Contact" runat="server" Label="联系人：" MaxLength="512" ToolTipTitle="联系人" ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="ContactPhone" runat="server" Label="联系电话：" MaxLength="512" ToolTipTitle="联系电话" ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:TextBox ID="Remark" runat="server" Label="备注：" MaxLength="512" ToolTipTitle="备注" ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
