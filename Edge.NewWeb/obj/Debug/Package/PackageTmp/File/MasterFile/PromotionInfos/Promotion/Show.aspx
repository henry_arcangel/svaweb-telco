﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="PromotionCode" runat="server" Label="促销编码：">
                                    </ext:Label>
                                    <ext:Label ID="PromotionDesc1" runat="server" Label="促销描述1：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="PromotionDesc2" runat="server" Label="促销描述2：">
                                    </ext:Label>
                                    <ext:Label ID="PromotionDesc3" runat="server" Label="促销描述3：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow3" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="PromotionNote" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow12" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="LoyaltyFlagView" runat="server" Label="是否仅会员促销：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow15" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="单据创建时的交易日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="单据批核时的交易日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow5" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveStatus" runat="server" Label="单据状态：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow6" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="UpdatedOn" runat="server" Label="修改时间：">
                                    </ext:Label>
                                    <ext:Label ID="UpdatedBy" runat="server" Label="修改人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow4" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="TopLimit" runat="server" Label="循环结束：" />
                                    <ext:RadioButtonList ID="LoyaltyFlag" runat="server" Label="是否仅会员促销：">
                                        <ext:RadioItem Text="不是" Value="0" Selected="true" />
                                        <ext:RadioItem Text="是的" Value="1" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow9" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:RadioButtonList ID="MutexFlag" runat="server" Label="是否与其他促销共同命中：">
                                        <ext:RadioItem Text="可以" Value="0" Selected="true" />
                                        <ext:RadioItem Text="不可以" Value="1" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Priority" runat="server" Label="优先值：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow10" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:RadioButtonList ID="HitRelation" runat="server" Label="命中条件的关系：">
                                        <ext:RadioItem Text="和" Value="1" Selected="true" />
                                        <ext:RadioItem Text="或" Value="2" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="PromotionType" runat="server" Label="促销命中条件类型：" Hidden="true">
                                        <ext:ListItem Text="Promotion By Hit Qty" Value="0" />
                                        <ext:ListItem Text="Promotion By Hit Amount" Value="1" />
                                        <ext:ListItem Text="Promotion By Sales Amount" Value="2" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="店铺设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm4" ShowBorder="false" ShowHeader="false" runat="server"
                        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                        LabelAlign="Right">
                        <Items>
                            <ext:Label ID="StoreIDView" runat="server" Label="店铺编码：">
                            </ext:Label>
                            <ext:Label ID="StoreGroupIDView" runat="server" Label="店铺组编码：">
                            </ext:Label>
                            <ext:DropDownList ID="StoreID" runat="server" Label="店铺编码：" Hidden="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="StoreGroupID" runat="server" Label="店铺组编码：" Hidden="true">
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="促销时间设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" ShowBorder="false" ShowHeader="false" runat="server"
                        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                        LabelAlign="Right">
                        <Items>
                            <ext:Label ID="StartDate" runat="server" Label="促销生效日期：">
                            </ext:Label>
                            <ext:Label ID="EndDate" runat="server" Label="促销失效日期：">
                            </ext:Label>
                            <ext:Label ID="StartTime" runat="server" Label="促销生效时间：">
                            </ext:Label>
                            <ext:Label ID="EndTime" runat="server" Label="促销失效时间：">
                            </ext:Label>
                            <ext:Label ID="DayFlagID" runat="server" Label="一月中促销生效日：">
                            </ext:Label>
                            <ext:Label ID="WeekFlagID" runat="server" Label="一周中促销生效日：">
                            </ext:Label>
                            <ext:Label ID="MonthFlagID" runat="server" Label="促销生效月：">
                            </ext:Label>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="促销针对人群表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid3" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="ObjectKey" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid3_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="促销会员范围">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("LoyaltyTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销会员范围值">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("LoyaltyValueName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销指定的会员忠诚度阀值">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("LoyaltyThreshold") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="是否销售当日生日促销">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("LoyaltyBirthdayFlagName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销指定的会员范围">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("LoyaltyPromoScopeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="促销命中条件表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" DataKeyNames="KeyID" AllowPaging="true" IsDatabasePaging="true"
                        EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="30px" HeaderText="命中类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblEntityType" runat="server" Text='<%# Eval("HitTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="50px" HeaderText="命中金额或数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("HitValue") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:BoundField Width="50px" HeaderText="命中关系操作符" DataField="HitOPName"  />
                            <ext:TemplateField Width="60px" HeaderText="命中货品条件">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("HitItemName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="EditWindowField" Width="20px" WindowID="WindowSearch" Icon="PageEdit"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="HitSeq,PromotionCode"
                                DataIFrameUrlFormatString="Promotion_Hit/Show.aspx?id={0}&promotioncode={1}" DataWindowTitleFormatString="查看"
                                Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="促销礼品表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" DataKeyNames="KeyID" AllowPaging="true" IsDatabasePaging="true"
                        EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true" OnPageIndexChange="Grid2_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="促销实现方式">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("PromotionGiftTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销内容值">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("PromotionValueName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="65px" HeaderText="促销结果数值调整值">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("PromotionAdjValue","{0:F2}") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowSearch" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
         IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
