﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                       OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" ShowBorder="false" ShowHeader="false" runat="server"
                        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="PromotionCode" runat="server" Label="促销编码：" Required="true" ShowRedStar="true"
                                Enabled="false" ToolTip="请输入促销代码，不能超过64个字符"/>
                            <ext:TextBox ID="PromotionDesc1" runat="server" Label="促销描述1："
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" ToolTip="请输入促销描述，不能超过512个字符"/>
                            <ext:TextBox ID="PromotionDesc2" runat="server" Label="促销描述2："
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" ToolTip="请输入备用促销描述，不能超过512个字符"/>
                            <ext:TextBox ID="PromotionDesc3" runat="server" Label="促销描述3："
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" ToolTip="请输入备用促销描述，不能超过512个字符"/>
                            <ext:TextBox ID="PromotionNote" runat="server" Label="备注：" ToolTip="请输入备注，不超过512字符"/>
                            
                            <ext:NumberBox ID="TopLimit" runat="server" Label="循环结束：" DecimalPrecision="0"  Width="50px" ToolTip="循环次数，-1表示不限制"/> 
                            <ext:RadioButtonList ID="LoyaltyFlag" runat="server" Label="是否仅会员促销："
                                    OnSelectedIndexChanged="LoyaltyFlag_SelectedIndexChanged" AutoPostBack="true">
                                    <ext:RadioItem Text="不是" Value="0" Selected="true" />
                                    <ext:RadioItem Text="是的" Value="1" />
                                </ext:RadioButtonList>
                                <ext:RadioButtonList ID="MutexFlag" runat="server" Label="是否与其他促销共同命中：" >
                                    <ext:RadioItem Text="可以" Value="0" Selected="true" />
                                    <ext:RadioItem Text="不可以" Value="1" />
                                </ext:RadioButtonList>
                                <ext:NumberBox ID="Priority" runat="server" Label="优先值：" DecimalPrecision="0"  Width="50px"/>
                                <ext:RadioButtonList ID="HitRelation" runat="server" Label="命中条件的关系：">
                                    <ext:RadioItem Text="和" Value="1" Selected="true" />
                                    <ext:RadioItem Text="或" Value="2" />
                                </ext:RadioButtonList>    
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="店铺设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm4" ShowBorder="false" ShowHeader="false" runat="server"
                        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
                        <Items>
                            <ext:DropDownList ID="StoreID" runat="server" Label="店铺编码：" ToolTip="请选择店铺代码"
                                    OnSelectedIndexChanged="StoreID_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="StoreGroupID" runat="server" Label="店铺组编码：" ToolTip="请选择店铺组代码"
                                OnSelectedIndexChanged="StoreGroupID_SelectedIndexChanged" Resizable="true" AutoPostBack="true">
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="促销时间设置"
                 AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm3" ShowBorder="false" ShowHeader="false" runat="server"
                        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
                        <Items>
                            <ext:DatePicker ID="StartDate" runat="server" Label="价格生效日期：" ToolTip="Translate__Special_121_Start日期格式：YYYY-MM-DDTranslate__Special_121_End"></ext:DatePicker>
                            <ext:DatePicker ID="EndDate" runat="server" Label="价格失效日期：" ToolTip="Translate__Special_121_Start日期格式：YYYY-MM-DDTranslate__Special_121_End"
                                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于开始日期">
                            </ext:DatePicker>
                            <ext:TimePicker ID="StartTime" runat="server" Label="促销生效时间：" Text="00:00:00" Increment="120"
                                ToolTip="Translate__Special_121_Start时间格式：HH：MM 24小时格式Translate__Special_121_End" TimeFormatString="HH:mm:ss">
                            </ext:TimePicker>
                            <ext:TimePicker ID="EndTime" runat="server" Label="促销失效时间：" Text="23:59:59" Increment="120"
                                ToolTip="Translate__Special_121_Start时间格式：HH：MM 24小时格式Translate__Special_121_End" TimeFormatString="HH:mm:ss">
                            </ext:TimePicker>
                            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:TextBox ID="DayFlagID" runat="server" Label="一月中促销生效日：" ToolTip="请输入一月中促销生效日的代码，不能超过64个字符"></ext:TextBox>
                                            <ext:Button ID="btnAddDay" runat="server" Text="添加" Icon="Add" OnClick="btnAddDay_Click"></ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:Form ID="Form3" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow2" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:TextBox ID="WeekFlagID" runat="server" Label="一周中促销生效日：" ToolTip="请输入一周中促销生效日的代码，不能超过64个字符"></ext:TextBox>
                                            <ext:Button ID="btnAddWeek" runat="server" Text="添加" Icon="Add" OnClick="btnAddWeek_Click"></ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:Form ID="Form4" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow3" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:TextBox ID="MonthFlagID" runat="server" Label="促销生效月：" ToolTip="请输入促销生效月的代码，不能超过64个字符"></ext:TextBox>
                                            <ext:Button ID="btnAddMonth" runat="server" Text="添加" Icon="Add" OnClick="btnAddMonth_Click"></ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="促销针对人群表"
                AutoHeight="true" AutoWidth="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar3" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnMemberAdd" Icon="Add" runat="server" Text="添加" OnClick="btnMemberAdd_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator6" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnMemberDelete" Icon="Delete" 
                                runat="server" Text="删除" OnClick="btnDeleteMemberItem_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator7" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearAllMember" Icon="Delete"
                                 runat="server" Text="清空" OnClick="btnClearAllMemberItem_Click">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="Grid3" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="10" runat="server" EnableCheckBoxSelect="true" DataKeyNames="ObjectKey"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid3_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="促销会员范围">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("LoyaltyTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销会员范围值">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("LoyaltyValueName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="80px" HeaderText="促销指定的会员忠诚度阀值">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("LoyaltyThreshold") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="是否销售当日生日促销">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("LoyaltyBirthdayFlagName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销指定的会员范围">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("LoyaltyPromoScopeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="EditWindowField" Width="40px" WindowID="WindowSearch" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="ObjectKey,PromotionCode"
                                DataIFrameUrlFormatString="Promotion_Member/Modify.aspx?id={0}&promotioncode={1}" DataWindowTitleFormatString="编辑"
                                Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="促销命中条件表"
                AutoHeight="true" AutoWidth="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar5" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnHitAdd" Icon="Add" runat="server" Text="添加" OnClick="btnHitAdd_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator5" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnDeleteHitItem" Icon="Delete" 
                                runat="server" Text="删除" OnClick="btnDeleteHitItem_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearAllHitItem" Icon="Delete"
                                 runat="server" Text="清空" OnClick="btnClearAllHitItem_Click">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="10" runat="server" EnableCheckBoxSelect="true" DataKeyNames="HitSeq"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="命中类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblEntityType" runat="server" Text='<%# Eval("HitTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="命中金额或数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("HitValue") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:BoundField Width="60px" HeaderText="命中关系操作符" DataField="HitOPName"  />
                            <ext:TemplateField Width="60px" HeaderText="命中货品条件">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("HitItemName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="WindowSearch" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="HitSeq,PromotionCode"
                                DataIFrameUrlFormatString="Promotion_Hit/Modify.aspx?id={0}&promotioncode={1}" DataWindowTitleFormatString="编辑"
                                Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="促销礼品表"
                AutoHeight="true" AutoWidth="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnGiftAdd" Icon="Add" runat="server" Text="添加" OnClick="btnGiftAdd_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnDeleteGiftItem" Icon="Delete" 
                                runat="server" Text="删除" OnClick="btnDeleteGiftItem_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnClearAllGiftItem" Icon="Delete"
                                 runat="server" Text="清空" OnClick="btnClearAllGiftItem_Click">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="10" runat="server" EnableCheckBoxSelect="true" DataKeyNames="GiftSeq"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid2_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="促销实现方式">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("PromotionGiftTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="促销内容值">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("PromotionValueName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="65px" HeaderText="促销结果数值调整值">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("PromotionAdjValue","{0:F2}") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="EditWindowField" Width="40px" WindowID="WindowSearch" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="GiftSeq,PromotionCode"
                                DataIFrameUrlFormatString="Promotion_Gift/Modify.aspx?id={0}&promotioncode={1}" DataWindowTitleFormatString="编辑"
                                Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="Window1Edit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="250px">
    </ext:Window>
    <ext:Window ID="WindowSearch" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    </form>
</body>
</html>
