﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmountPoint.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="CouponTypeID" runat="server" Label="优惠券类型：" Enabled="false" Resizable="true"/>
            <ext:DropDownList ID="ExchangeType" runat="server" Label="兑换类型：" Enabled="false" Resizable="true">
                <ext:ListItem Text="金额兑换" Value="1" />
                <ext:ListItem Text="积分兑换" Value="2" />
                <ext:ListItem Text="金额+积分兑换" Value="3" />
                <ext:ListItem Text="优惠券兑换" Value="4" />
                <ext:ListItem Text="消费金额兑换" Value="5" />
            </ext:DropDownList>
            <ext:NumberBox ID="ExchangeAmount" runat="server" Label="兑换需要的金额：" Required="true" ShowRedStar="true" />
            <ext:NumberBox ID="ExchangePoint" runat="server" Label="兑换需要的积分：" Required="true"
                ShowRedStar="true" />
            <ext:DatePicker ID="StartDate" runat="server" Label="规则起始日期：" Required="true" ShowRedStar="true"
                DateFormatString="yyyy-MM-dd" />
            <ext:DatePicker ID="EndDate" runat="server" Label="规则结束日期：" Required="true" ShowRedStar="true"
                DateFormatString="yyyy-MM-dd" CompareOperator="GreaterThanEqual" CompareMessage="规则结束日期应该大于规则起始日期" />
            <ext:RadioButtonList ID="Status" runat="server" Label="状态：">
                <ext:RadioItem Text="有效" Value="1" Selected="true" />
                <ext:RadioItem Text="无效" Value="0" />
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项" CssClass="showMessage" />
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
