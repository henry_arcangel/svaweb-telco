﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponNature.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Grid1"/>
    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
        runat="server" EnableCheckBoxSelect="True" DataKeyNames="CouponNatureID" AllowPaging="true"
        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
        OnPageIndexChange="Grid1_PageIndexChange">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                    </ext:Button>
                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Columns>
            <ext:TemplateField Width="60px" HeaderText="优惠券类别编号">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CouponNatureCode") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="描述">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("CouponNatureName1") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="发行商">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("CardIssuerName") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="品牌">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                    -
                    <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="主类别名称">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ParentName")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="CouponNatureID"
                DataIFrameUrlFormatString="Show.aspx?id={0}" DataWindowTitleFormatString="查看"
                Title="查看" />
            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="CouponNatureID"
                DataIFrameUrlFormatString="Modify.aspx?id={0}" DataWindowTitleFormatString="编辑"
                Title="编辑" />
        </Columns>
    </ext:Grid>
    <ext:Window ID="Window1" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="250px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="450px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    </form>
</body>
</html>
