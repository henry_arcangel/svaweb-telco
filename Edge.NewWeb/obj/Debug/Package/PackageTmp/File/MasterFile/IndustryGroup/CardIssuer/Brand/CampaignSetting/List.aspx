﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1"/>
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelAlign="Right">
                <Rows>
                    <ext:FormRow>
                        <Items>
                            <ext:TextBox ID="Code" runat="server" Label="活动编号：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="Desc" runat="server" Label="描述：" MaxLength="512">
                            </ext:TextBox>
                            <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>    
                <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                    runat="server" EnableCheckBoxSelect="True" DataKeyNames="CampaignID,CampaignName1"
                    AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                    ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" OnSort="Grid1_Sort" AllowSorting="true">
                    <Toolbars>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                </ext:Button>
                                <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </Toolbars>
                    <Columns>
                        <ext:TemplateField Width="60px" HeaderText="活动编号" SortField="CampaignCode">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("CampaignCode") %>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="描述">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("CampaignName") %>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="发行商">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("CardIssuerName") %>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="品牌">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                                -
                                <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window3" Icon="Page"
                            Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="CampaignID"
                            DataIFrameUrlFormatString="Show.aspx?id={0}" DataWindowTitleFormatString="查看"
                            Title="查看" />
                        <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                            Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="CampaignID"
                            DataIFrameUrlFormatString="Modify.aspx?id={0}" DataWindowTitleFormatString="编辑"
                            Title="编辑" />
                    </Columns>
                </ext:Grid>
                    </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="550px">
    </ext:Window>
    <ext:Window ID="Window3" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="900px"
        Height="550px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField>
    <ext:HiddenField ID="SortField" Text="" runat="server"></ext:HiddenField>
    <%--  <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="10%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                    <th width="15%">
                        活动编号
                    </th>
                    <th width="25%">
                        描述
                    </th>
                    <th width="15%">
                        发行商
                    </th>
                    <th width="25%">
                        品牌
                    </th>
                    <th width="10%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" /><asp:HiddenField ID="lb_id"
                        runat="server" Value='<%#Eval("CampaignID")%>'></asp:HiddenField>
                </td>
                <td align="center">
                    <asp:Label ID="lblCampaignCode" runat="server" Text='<%#Eval("CampaignCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCampaignName1" runat="server" Text='<%#Eval("CampaignName1")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="CardIssuerName" runat="server" Text='<%#Eval("CardIssuerName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                    -
                    <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg"><a href="show.aspx?id=<%#Eval("CampaignID") %>">查看</a> <a href="modify.aspx?id=<%#Eval("CampaignID") %>&page=<%=page %>">
                        修改</a> </span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr">
        </div>
        <div class="left">
            <span class="btn_bg">
                <asp:LinkButton ID="lbtnDel" runat="server" OnClick="lbtnDel_Click">删除</asp:LinkButton>
                <asp:LinkButton ID="lbtnAdd" runat="server" OnClick="lbtnAdd_Click">添加</asp:LinkButton>
            </span>
        </div>
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <%--    <script type="text/javascript">
        
        $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>--%>
</body>
</html>
