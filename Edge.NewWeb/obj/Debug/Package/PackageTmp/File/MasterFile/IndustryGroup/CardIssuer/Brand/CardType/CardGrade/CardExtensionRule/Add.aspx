﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="RuleType" runat="server" Label="规则类型：" ShowRedStar="true" Required="true" Resizable="true">
                <ext:ListItem Text="卡有效期延长规则" Value="0"></ext:ListItem>
                <ext:ListItem Text="金额有效期规则" Value="1"></ext:ListItem>
                <ext:ListItem Text="积分有效期规则" Value="2"></ext:ListItem>
            </ext:DropDownList>
            <ext:DropDownList ID="CardTypeID" runat="server" Enabled="false" Label="卡类型：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Enabled="false" Label="卡等级：" Resizable="true">
            </ext:DropDownList>
            <ext:NumberBox ID="ExtensionRuleSeqNo" runat="server" Label="规则记录序号：" ShowRedStar="true"
                Required="true">
            </ext:NumberBox>
            <ext:NumberBox ID="MaxLimit" runat="server" Label="最大增加数：" ShowRedStar="true" Required="true">
            </ext:NumberBox>
            <ext:NumberBox ID="RuleAmount" runat="server" Label="增值金额：" NoDecimal="false" ShowRedStar="true"
                Required="true">
            </ext:NumberBox>
            <ext:NumberBox ID="Extension" runat="server" Label="延长日期：" ShowRedStar="true" Required="true">
            </ext:NumberBox>
            <ext:DropDownList ID="ExtensionUnit" runat="server" Label="有效期延长单位：" Resizable="true" OnSelectedIndexChanged="ExtensionUnit_Changed" AutoPostBack=true>
                <ext:ListItem Text="永久" Value="0"></ext:ListItem>
                <ext:ListItem Text="年" Value="1"></ext:ListItem>
                <ext:ListItem Text="月" Value="2"></ext:ListItem>
                <ext:ListItem Text="星期" Value="3"></ext:ListItem>
                <ext:ListItem Text="天" Value="4"></ext:ListItem>
                <ext:ListItem Text="有效期不变更" Value="5"></ext:ListItem>
                <ext:ListItem Text="指定有效期" Value="6"></ext:ListItem>
            </ext:DropDownList>
            <ext:DropDownList ID="ExtendType" runat="server" Label="有效期延长方式：" Resizable="true">
                <ext:ListItem Text="增值时重置有效期" Value="0"></ext:ListItem>
                <ext:ListItem Text="增值时延长有效期" Value="1"></ext:ListItem>
                <ext:ListItem Text="增值时不变更有效期" Value="2"></ext:ListItem>
            </ext:DropDownList>
            <ext:DatePicker ID="SpecifyExpiryDate" runat="server" Label="指定日期：" DateFormatString="yyyy-MM-dd"
                ShowRedStar="true">
            </ext:DatePicker>
            <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于等于开始日期"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
