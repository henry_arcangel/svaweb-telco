﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.SpecificDepartment.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="增加产品" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" AutoHeight="true" AutoWidth="true" Title="基础内容">
            <Items>
                <ext:SimpleForm ID="SimpleForm2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server"  LabelAlign="Right">
                    <Items>
                        <ext:TextBox ID="DepartCode" runat="server" Label="部门：" Required ="true" ShowRedStar="true"  ToolTipTitle="部门"  ToolTip="自定义部门编号，不能超过512个字符" Enabled="false"/>
                        <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Required="true" ShowRedStar="true" Resizable="true"/>
                    </Items>
                </ext:SimpleForm>            
            </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" AutoHeight="true" AutoWidth="true" Title="部门列表">
            <Items>
                <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                    runat="server" EnableCheckBoxSelect="True" DataKeyNames="DepartCode" AllowPaging="true"
                    IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                    OnPageIndexChange="Grid1_PageIndexChange" OnRowDoubleClick="Grid1_OnRowDoubleClick">
                    <Columns>
                        <ext:TemplateField Width="60px" HeaderText="部门编号">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("DepartCode")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="部门名称">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("DepartName1")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                    </Columns>
                </ext:Grid>
            </Items>
        </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <div style="padding-bottom: 10px;">
    </div>
    
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
