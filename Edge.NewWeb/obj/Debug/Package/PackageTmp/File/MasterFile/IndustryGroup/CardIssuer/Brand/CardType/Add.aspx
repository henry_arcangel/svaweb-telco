﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2,sform3,sform4,sform5,sform6"
                        Icon="SystemSaveClose" OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CardTypeCode" runat="server" Label="卡类型编号：" MaxLength="20" Required="true"
                                ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                                RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" ToolTipTitle="卡类型编号" ToolTip="Translate__Special_121_StartKey identifier of Card Type. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardTypeName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                                ShowRedStar="true" ToolTipTitle="描述" ToolTip="请输入规范的卡類型名称。不能超過512個字符" 
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CardTypeName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                                ToolTip="對卡類型的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CardTypeName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                                ToolTip="對卡類型的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Required="true" ShowRedStar="true"
                                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CurrencyID" runat="server" Label="币种：" Required="true" ShowRedStar="true"
                                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:RadioButtonList ID="IsDumpCard" runat="server" Label="是否为转储卡："                                 AutoPostBack="true" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="true" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="卡号规则">
                <Items>
                    <ext:SimpleForm ID="sform2" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:RadioButtonList ID="IsImportUIDNumber" runat="server" Label="卡号码是否导入：" OnSelectedIndexChanged="IsImportUIDNumber_SelectedIndexChanged"
                                AutoPostBack="true" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="true" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ManualRoles" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                <Items>
                    <ext:SimpleForm ID="sform3" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CardNumMask" runat="server" Label="卡号编码规则：" MaxLength="20" Regex="(^A*9+$)|(^9+$)"
                                RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumMask_TextChanged"
                                ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardNumPattern" runat="server" Label="卡号前缀号码：" MaxLength="20" RegexPattern="NUMBER"
                                RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumPattern_TextChanged"
                                ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="CardCheckdigit" runat="server" Label="卡号是否添加校验位：" AutoPostBack="True"
                                OnSelectedIndexChanged="CardCheckdigit_SelectedIndexChanged" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="True" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" />
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="CardNumberToUID" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End"
                                AutoPostBack="True" OnSelectedIndexChanged="CardNumberToUID_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ImportRoles" runat="server" EnableCollapse="True" Title="导入物理编号规则">
                <Items>
                    <ext:SimpleForm ID="sform4" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CardNumMaskImport" runat="server" Label="卡号编码规则：" MaxLength="20"
                                Regex="(^A*9+$)|(^9+$)" RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumMaskImport_TextChanged"
                                ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardNumPatternImport" runat="server" Label="卡号前缀号码：" MaxLength="20"
                                RegexPattern="NUMBER" RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumPatternImport_TextChanged"
                                ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End"
                                Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                Width="100px" OnSelectedIndexChanged="UIDCheckDigit_SelectedIndexChanged" AutoPostBack="True">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：" Width="100px"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="UIDToCardNumber" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End"
                                AutoPostBack="True" OnSelectedIndexChanged="UIDToCardNumber_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="使用规则">
                <Items>
                    <ext:SimpleForm ID="sform5" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:RadioButtonList ID="CardMustHasOwner" runat="server" Label="是否必须指定会员：" Width="100px">
                                <ext:RadioItem Text="是" Value="1"  Selected="True" />
                                <ext:RadioItem Text="否" Value="0"/>
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="CashExpiredate" runat="server" Label="现金帐套是否有多重有效期：" Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="PointExpiredate" runat="server" Label="积分帐套是否有多重有效期：" Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="IsPhysicalCard" runat="server" Label="是否需要写卡：" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="True" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="有效性">
                <Items>
                    <ext:SimpleForm ID="sform6" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:DatePicker ID="CardTypeStartDate" runat="server" Label="卡类型启用日期：" DateFormatString="yyyy-MM-dd"
                                ShowRedStar="true" Required="true" ToolTipTitle="卡类型启用日期" ToolTip="日期格式：YYYY-MM-DD">
                            </ext:DatePicker>
                            <ext:DatePicker ID="CardTypeEndDate" runat="server" Label="卡类型结束日期：" DateFormatString="yyyy-MM-dd"
                                CompareControl="CardTypeStartDate" CompareOperator="GreaterThanEqual" CompareMessage="卡类型结束日期应该大于卡类型启用日期"
                                ShowRedStar="true" Required="true" ToolTipTitle="卡类型结束日期" ToolTip="日期格式：YYYY-MM-DD">
                            </ext:DatePicker>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
