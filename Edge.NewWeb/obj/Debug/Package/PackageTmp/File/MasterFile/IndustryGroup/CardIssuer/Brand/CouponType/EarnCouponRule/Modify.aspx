﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="Brand" runat="server" Label="品牌：" Enabled="false"/>
            <ext:TextBox ID="CouponType" runat="server" Label="优惠券类型：" Enabled="false"></ext:TextBox>
            <ext:DropDownList ID="CardTypeIDLimit"  runat="server" Label="卡类型：" Resizable="true"
                 OnSelectedIndexChanged="CardTypeIDLimit_SelectedIndexChanged" AutoPostBack="true">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeIDLimit"  runat="server" Label="卡级别：" Resizable="true"></ext:DropDownList>
            <ext:DropDownList ID="MemberBirthdayLimit"  runat="server" Label="会员范围：" Resizable="true"></ext:DropDownList>
            <ext:DropDownList ID="ExchangeType"  runat="server" Label="兑换类型：" Resizable="true" Required="true" ShowRedStar="true" 
                OnSelectedIndexChanged="ExchangeType_SelectedIndexChanged" AutoPostBack="true">
            </ext:DropDownList>
            <ext:DropDownList ID="ExchangeConsumeRuleOper"  runat="server" Label="计算方式：" Resizable="true" Required="true" ShowRedStar="true" 
                Enabled="false"></ext:DropDownList>
            <ext:NumberBox ID="ExchangeConsumeAmount" runat="server" Label="消费金额：" Required="true" ShowRedStar="true" 
                NoDecimal="false" Text="0" MaxValue="99999999" MinValue="0" Enabled="false"/>
            <ext:NumberBox ID="ExchangeAmount" runat="server" Label="所需金额：" Required="true" ShowRedStar="true" 
                NoDecimal="false" Text="0" MaxValue="99999999" MinValue="0" Enabled="false"/>
            <ext:NumberBox ID="ExchangePoint" runat="server" Label="所需积分：" Required="true" ShowRedStar="true" NoDecimal="true"
                Text="0" MaxValue="99999999" MinValue="0" Enabled="false"/>
            <ext:DropDownList ID="ExchangeCouponTypeID"  runat="server" Label="所需优惠券类型：" Resizable="true" Enabled="false"
                Required="true" ShowRedStar="true"></ext:DropDownList>
            <ext:NumberBox ID="ExchangeCouponCount" runat="server" Label="所需优惠券数量：" Required="true" ShowRedStar="true"
                NoDecimal="true" Text="0" MaxValue="99999999" MinValue="0" Enabled="false"/>
            <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于等于开始日期"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:RadioButtonList ID="Status" runat="server" Label="状态：" Required="true" ShowRedStar="true" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
