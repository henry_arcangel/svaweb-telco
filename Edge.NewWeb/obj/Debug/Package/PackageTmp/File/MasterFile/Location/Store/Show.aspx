﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Store.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="false">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="StoreCode" runat="server" Label="店铺编号：">
                            </ext:Label>
                            <ext:Label ID="StoreName1" runat="server" Label="描述：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="StoreName2" runat="server" Label="其他描述1：">
                            </ext:Label>
                            <ext:Label ID="StoreName3" runat="server" Label="其他描述2：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server">
                        <Items>
                            <ext:Label ID="lblCardIssuer" runat="server" Label="发行商：">
                            </ext:Label>
                            <ext:Label ID="BrandID" runat="server" Label="店铺所属品牌列表：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server">
                        <Items>
                            <ext:Label ID="StoreTypeID" runat="server" Label="店铺种类：">
                            </ext:Label>
                            <ext:Label ID="StoreGroupView" runat="server" Label="店铺分组：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow18" runat="server">
                        <Items>
                            <ext:Label ID="CompanyID" runat="server" Label="公司：">
                            </ext:Label>
                            <ext:Label ID="PickupStoreFlagView" runat="server" Label="是否允许提货：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow5" runat="server">
                        <Items>
                            <ext:Label ID="StoreCountry" runat="server" Label="店铺所在国家：">
                            </ext:Label>
                            <ext:Label ID="StoreProvince" runat="server" Label="店鋪所在的省份：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow29" runat="server">
                        <Items>
                            <ext:Label ID="StoreCity" runat="server" Label="店鋪所在的城市：">
                            </ext:Label>
                            <ext:Label ID="StoreDistrict" runat="server" Label="店鋪所在的县（区）：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>    
            <ext:Form ID="Form4" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow6" runat="server">
                        <Items>
                            <ext:Label ID="StoreAddressDetail" runat="server" Label="店铺地址：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form8" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow14" runat="server">
                        <Items>
                            <ext:Label ID="StoreAddressDetail2" runat="server" Label="店铺其他地址1：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form9" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow15" runat="server">
                        <Items>
                            <ext:Label ID="StoreAddressDetail3" runat="server" Label="店铺其他地址2：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow13" runat="server">
                        <Items>
                            <ext:Label ID="StoreFullDetail" runat="server" Label="店铺完整地址：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form5" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow7" runat="server">
                        <Items>
                            <ext:Label ID="StoreLongitude" runat="server" Label="店铺经度坐标：">
                            </ext:Label>
                            <ext:Label ID="StoreLatitude" runat="server" Label="店铺纬度坐标：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow8" runat="server">
                        <Items>
                            <ext:Label ID="StoreTel" runat="server" Label="电话号码：">
                            </ext:Label>
                            <ext:Label ID="StoreFax" runat="server" Label="传真号码：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>   
                    <ext:FormRow ID="FormRow9" runat="server">
                        <Items>
                            <ext:Label ID="Email" runat="server" Label="电子邮箱：">
                            </ext:Label>
                            <ext:Label ID="Contact" runat="server" Label="联系人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow> 
                    <ext:FormRow ID="FormRow17" runat="server">
                        <Items>
                            <ext:Label ID="StatusView" runat="server" Label="店铺状态：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="Form6" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow10" runat="server">
                        <Items>
                            <ext:Label ID="StoreOpenTime" runat="server" Label="营业开始时间：">
                            </ext:Label>
                            <ext:Label ID="StoreCloseTime" runat="server" Label="营业结束时间：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form10" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow16" runat="server">
                        <Items>
                            <ext:Label ID="StoreNote" runat="server" Label="备注：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    </Rows>
            </ext:Form>
            <ext:Form ID="Form7" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow runat="server" ColumnWidths="0% 40% 10%">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:Label ID="Label1" Text="" runat="server" Label="店铺图片：">
                            </ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow11" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                            </ext:Label>
                            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow12" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                            </ext:Label>
                            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:DropDownList ID="StoreGroupID" runat="server" Label="店铺分组：" Resizable="true" Hidden="true">
            </ext:DropDownList>
            <ext:DropDownList ID="Status" runat="server" Label="店铺状态：" Resizable="true" Hidden="true">
                <ext:ListItem Text="营业中" Value="1" />
                <ext:ListItem Text="未营业" Value="2" />
            </ext:DropDownList>
            <ext:RadioButtonList ID="PickupStoreFlag" runat="server" Label="是否允许提货：" Hidden="true">
                <ext:RadioItem Text="是" Value="1" />
                <ext:RadioItem Text="否" Value="0"/>
            </ext:RadioButtonList>
             <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="店铺属性">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                          <%--  <ext:NumberBox ID="HoldCouponCount" runat="server" Label="可获取累计最大数量：" NoNegative="true"
                                NoDecimal="true" ToolTipTitle="可获取累计最大数量" ToolTip="请输入正數" AutoPostBack="true"
                                OnTextChanged="HoldCouponCount_OnTextChanged" MaxValue="100000000" Hidden="true">
                            </ext:NumberBox>--%>
                        </Items>
                    </ext:SimpleForm>
                    <ext:Grid ID="Grid_StoreAttributeList" ShowBorder="true" ShowHeader="true" Title="店铺属性列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid_StoreAttribute_OnPageIndexChange">
                       
                        <Columns>
                            <ext:BoundField ColumnID="SACode" DataField="SACode" HeaderText="店铺属性编号" />
                            <ext:BoundField ColumnID="SADesc1" DataField="SADesc1"
                                HeaderText="店铺属性" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
           <ext:Window ID="HiddenWindowFormSpecial" Title="" Popup="false" EnableIFrame="true"
        runat="server" CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="50px"
        Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
