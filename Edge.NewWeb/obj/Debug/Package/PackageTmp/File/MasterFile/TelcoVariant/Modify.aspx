﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.TelcoVariant.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
 <form id="Form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
   

            <ext:TextBox ID="UPC" Enabled="false"  runat="server" Label="SKU：" Required="true" ShowRedStar="true"  MaxLength="20" ToolTipTitle="SKU" ToolTip="The SKU must not be more than 512 characters"
               AutoPostBack="true">
            </ext:TextBox>
            
            <ext:TextBox ID="SKUDesc" runat="server" Label="Description：" Required="true" ShowRedStar="true"  ToolTipTitle="SKU Description" ToolTip="The SKU Description must not be more than 150 characters"
                    MaxLength="150"     AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="ProdCode" runat="server" Label="Product Code：" ShowRedStar="true"  MaxLength="150" ToolTipTitle="Product Code" ToolTip="The Product Code must not be more than 150 characters"
               AutoPostBack="true">
            </ext:TextBox>
            
            <ext:TextBox ID="SKUUnitAmount" runat="server"   Label="Amount：" ShowRedStar="true"  MaxLength="9" ToolTipTitle="SKU Unit Amount" ToolTip="The Amount must not be more than 9 characters"
                AutoPostBack="true">
            </ext:TextBox>
            
            <ext:RadioButtonList ID="Export" Label="GM Type" runat="server">
                <ext:RadioItem Text="PR" Value="PR" Selected="true"/>
                <ext:RadioItem Text="Telco Variant" Value="NULL"/>
            </ext:RadioButtonList>
            
            <ext:DropDownList ID="CardTypeCode" runat="server" Label="Card Type :" Required="true" ShowRedStar="true"  AutoPostBack="true"
                OnSelectedIndexChanged="CardType_SelectedIndexChanged">
            </ext:DropDownList>

             <ext:DropDownList id="CardGradeCode" runat="server" ShowRedStar="true" Required="true"  Label="Card Grade :"
         AutoPostBack="true">
            </ext:DropDownList>

             <ext:TextBox ID="GMValue" runat="server" ShowRedStar="true" Label="Gross Margin：" ToolTipTitle="GM Value" MaxLength="9" ToolTip="The GM Value must not be more than 9 characters"
               AutoPostBack="true">
            </ext:TextBox>
             <ext:TextBox ID="TransactionType" runat="server" Label="Transaction Type：" MaxLength="50" ToolTipTitle="Transaction Type" ToolTip="The Transaction Type must not be more than 50 characters"
               AutoPostBack="true">
            </ext:TextBox>

             <ext:TextBox ID="NumberPrefix" runat="server" Label="Mobile Number Prefixes：" MaxLength ="512" ToolTipTitle="Mobile Prefix" ToolTip="The Mobile Prefix must not be more than 512 characters"
              AutoPostBack="true">
            </ext:TextBox>

            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    
</body>
</html>
