﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.TelcoVariant.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
 <form id="Form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
   

            <ext:TextBox ID="UPC" Enabled="false"  runat="server" Label="SKU：" Required="true" ShowRedStar="true"  MaxLength="20" ToolTipTitle="SKU" ToolTip="The SKU must not be more than 512 characters"
               AutoPostBack="true">
            </ext:TextBox>

            <ext:Label ID="SKUDesc" runat="server" Label="Description：">  
                            
            </ext:Label>

            <ext:Label ID="ProdCode" runat="server" Label="Product Code：" >

            </ext:Label>
            
            <ext:Label ID="SKUUnitAmount" runat="server" Label="Amount：" >
            </ext:Label>
            
            <ext:RadioButtonList ID="Export" Enabled="false" Label="GM Type" runat="server">
                <ext:RadioItem Text="PR"    Value="PR" Selected="true"/>
                <ext:RadioItem Text="Telco Variant" Value="NULL"/>
            </ext:RadioButtonList>
            
            <ext:DropDownList ID="CardTypeCode" Enabled="false" runat="server" Label="Card Type :" AutoPostBack="true"
                OnSelectedIndexChanged="CardType_SelectedIndexChanged">
            </ext:DropDownList>

             <ext:DropDownList id="CardGradeCode" Enabled="false" runat="server" Label="Card Grade :"
         AutoPostBack="true">
            </ext:DropDownList>

             <ext:Label ID="GMValue" runat="server"  Label="Gross Margin：">
            </ext:Label>
             <ext:Label ID="TransactionType" runat="server" Label="Transaction Type：" >
            </ext:Label>

             <ext:Label ID="NumberPrefix" runat="server" Label="Mobile Number Prefixes：" >
            </ext:Label>


            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>


    
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    
</body>
</html>
