﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.MasterFile.TelcoVariant.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1" />
<ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelAlign="Right" LabelWidth="110">
                <Rows>
                    <ext:FormRow  runat="server"  ColumnWidths="23% 23% 23% 23% 8%"> 
                        <Items>
                           <ext:TextBox ID="SKU" runat="server" Label="SKU：" MaxLength="512">
                            </ext:TextBox>
                          
                            <ext:TextBox ID="Desc" runat="server" Label="Description：" MaxLength="512">

                            </ext:TextBox>

                              <ext:Textbox ID="Prod_Code" runat="server" Label="Product Code："  
                              AutoPostBack="true" MaxLength="512" >
                              
                            </ext:Textbox>

                           <ext:Textbox ID="GrossMargin" runat="server" Label="Gross Margin："    
                                  AutoPostBack="true" MaxLength="512"> 
                               
                            </ext:Textbox>
                            <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click">
                            </ext:Button>
                           

                        </Items>
                    </ext:FormRow>
                    <ext:FormRow runat="server" ColumnWidths="23% 23% 23%" >
                    <Items> 
                     <ext:DropDownList ID="CardType" runat="server" Label="Card Type："  MaxLength="300"
                        OnSelectedIndexChanged="CardType_SelectedIndexChanged"    AutoPostBack="true" Resizable="true">
                         
                            </ext:DropDownList>
                         
                            <ext:DropDownList ID="CardGrade" runat="server" Label="Card Grade：" MaxLength="300"  
                              AutoPostBack="true" Resizable="true">
                                 
                            </ext:DropDownList>
                                <ext:Textbox ID="Amount" runat="server" Label="Amount :"  MaxLength="512" AutoPostBack="true"  >
                  
                            </ext:Textbox>
                            </Items>
                        
                      </ext:FormRow>
                </Rows>
            </ext:Form>

             
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="UPC,skudesc"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" OnSort="Grid1_Sort" AllowSorting="true">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="SKU" SortField="UPC">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("UPC") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Description">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("SKUDESC") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>


                             <ext:TemplateField Width="60px" HeaderText="Product Code" >
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("ProdCode") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("SKUunitAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>

                                      <ext:TemplateField Width="60px" HeaderText="Gross Margin" >
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("GMValue") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                     

                                      <ext:TemplateField Width="60px" HeaderText="Card Type"  >
                                <ItemTemplate>
                                    <asp:Label ID="CardType1" runat="server" Text='<%# Eval("CardTypeName1") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Card Grade">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("CardGradeName1") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>



                            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="upc"
                                DataIFrameUrlFormatString="Show.aspx?id={0}" DataWindowTitleFormatString="查看"
                                Title="查看" />
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="UPC"
                                DataIFrameUrlFormatString="Modify.aspx?id={0}" DataWindowTitleFormatString="编辑"
                                Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="350px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="450px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField>
    <ext:HiddenField ID="SortField" Text="" runat="server"></ext:HiddenField>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
