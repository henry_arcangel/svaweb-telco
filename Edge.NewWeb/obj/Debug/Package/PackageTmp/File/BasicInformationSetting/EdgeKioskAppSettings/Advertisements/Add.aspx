﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements.Add" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform6" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="PromotionMsgCode" runat="server" Label="广告编号：" Required="true" ShowRedStar="true"
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:DropDownList ID="ParentID" runat="server" Label="优惠消息类型1：" Required="true" ShowRedStar="true"
                                Resizable="true" CompareType="String" AutoPostBack="true"
                                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值" OnSelectedIndexChanged="ParentID_SelectedIndexChanged">
                            </ext:DropDownList>
                            <ext:DropDownList ID="PromotionMsgTypeID" runat="server" Label="优惠消息类型2：" Required="true" ShowRedStar="true"
                                Resizable="true" CompareType="String"
                                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:TextBox ID="PromotionTitle1" runat="server" Label="广告标题1：" Required="true" ShowRedStar="true"
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="PromotionTitle2" runat="server" Label="广告标题2："
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="PromotionTitle3" runat="server" Label="广告标题3："
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="Status" runat="server" Label="广告状态：">
                                <ext:RadioItem Text="生效" Value="1" Selected="true"/>
                                <ext:RadioItem Text="屏蔽" Value="2"/>
                            </ext:RadioButtonList>
                            <ext:HiddenField ID="uploadFilePath" runat="server"></ext:HiddenField>
                            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow1" runat="server"  ColumnWidths="88% 12%">
                                        <Items>
                                            <ext:TextBox ID="PromotionPicFile" runat="server"  Label="广告主题图片：" Readonly="true" ></ext:TextBox> 
                                            <ext:FileUpload ID="PromotionPicFilePath" runat="server" ShowLabel="false"  Width="65px"
                                                AutoPostBack="true" OnFileSelected="PromotionPicFile_FileSelected" Height="20px" ButtonOnly="true">
                                            </ext:FileUpload>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>  
                            <ext:Form ID="Form3" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server" LabelAlign="Left" >
                                <Rows>
                                    <ext:FormRow ID="FormRow3" runat="server"  ColumnWidths="88% 12%">
                                        <Items>
                                            <ext:TextBox ID="AttchFile" runat="server"  Label="附件上传：" Readonly="true" ></ext:TextBox>                                
                                            <ext:FileUpload ID="AttachmentFilePath" runat="server" ShowLabel="false"  Width="65px"
                                                AutoPostBack="true" OnFileSelected="AttachmentFilePath_FileSelected" Height="20px" ButtonOnly="true">
                                            </ext:FileUpload>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:HtmlEditor ID="PromotionMsgStr1" runat="server" Height="200" Label="广告内容1：" ShowRedStar="true"></ext:HtmlEditor>
                            <ext:HtmlEditor ID="PromotionMsgStr2" runat="server" Height="200" Label="广告内容2："></ext:HtmlEditor>
                            <ext:HtmlEditor ID="PromotionMsgStr3" runat="server" Height="200" Label="广告内容3："></ext:HtmlEditor>
                            <ext:TextBox ID="PromotionRemark" runat="server" Label="备注：">
                            </ext:TextBox>
                            <%--Add by Alex 2014-07-17 --%>
                              <ext:NumberBox ID="Priority" runat="server" Label="优先次序：" MaxLength="2"  MaxValue="99" NoDecimal="true" NoNegative="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="Link" runat="server" Label="视频链接：" MaxLength="512" ToolTipTitle="Translate__Special_121_Start Youtube link Translate__Special_121_End"
                                ToolTip="Translate__Special_121_StartYoutube link,不能超過512個字符Translate__Special_121_End">
                            </ext:TextBox>
                           <%--Add by Alex 2014-07-17 --%>
                            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="广告有效期设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="sform6" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:DatePicker ID="StartDate" runat="server" Label="广告起始日期：" DateFormatString="yyyy-MM-dd" Required="true" ShowRedStar="true">
                            </ext:DatePicker>
                            <ext:DatePicker ID="EndDate" runat="server" Label="广告结束日期：" DateFormatString="yyyy-MM-dd" Required="true" ShowRedStar="true" 
                                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于启用日期">
                            </ext:DatePicker>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="广告播放规则设置"
                AutoHeight="true" AutoWidth="true" Layout="Form">
                <Items>
                    <ext:Form ID="FormReLoad1" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px">
                        <Rows>
                            <ext:FormRow ID="FormRow2" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Tree runat="server" ID="CardGradeTree" OnNodeCheck="CardGradeTree_NodeCheck" Title="会员品牌-卡类型-卡级别列表">
                                        <Toolbars>
                                            <ext:Toolbar ID="Toolbar2" runat="server">
                                                <Items>
                                                    <ext:CheckBox ID="CheckAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="CheckAll_CheckedChanged">
                                                    </ext:CheckBox>
                                                </Items>
                                            </ext:Toolbar>
                                        </Toolbars>
                                    </ext:Tree>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc1:CheckRight ID="CheckRight1" runat="server"></uc1:CheckRight>
    </form>
</body>
</html>
