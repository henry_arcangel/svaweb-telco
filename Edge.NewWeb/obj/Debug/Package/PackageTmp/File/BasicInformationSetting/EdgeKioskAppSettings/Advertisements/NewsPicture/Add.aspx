﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.NewsPicture.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
                <ext:DropDownList ID="PromotionMsgCode" Required="true" ShowRedStar="true" runat="server" Label="新闻编号：" Resizable="true">
                  </ext:DropDownList>
            <%--<ext:TextBox ID="PromotionMsgCode" runat="server" Label="新闻编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true">--%>
            <ext:HiddenField ID="uploadFilePath" runat="server">
            </ext:HiddenField>
            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false"
                runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                        <Items>
                            <ext:FileUpload ID="ExtraPic" runat="server" Label="新闻额外图片：" ToolTipTitle="新闻图片" ToolTip="Translate__Special_121_Start新闻的图片。点击按钮进行上传，上传的文件支持JPG,GIF，PNG，BMP,文件大小不能超过10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:TextBox ID="PicRemark" runat="server" Label="新闻图片备注：" MaxLength="512"  ToolTipTitle="描述" ToolTip="不能超過512個字符" 
                AutoPostBack="true">
            </ext:TextBox>
            <ext:Window ID="WindowPic" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
                CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
                EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
                Height="450px">
            </ext:Window>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项" CssStyle="font-size:12px;color:red">
            </ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
