﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.BasicInformationSetting.USEFULEXPRESSIONS.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="Form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="Code" runat="server" Label="祝福语编号：" Required="true" ShowRedStar="true" Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="PhraseTitle1" runat="server" Label="祝福语标题1：" Required="true" ShowRedStar="true"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="PhraseTitle2" runat="server" Label="祝福语标题2："
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="PhraseTitle3" runat="server" Label="祝福语标题3："
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:HtmlEditor ID="PhraseContent1" runat="server" Height="100" Label="祝福语内容1：" ShowRedStar="true"></ext:HtmlEditor>
            <ext:HtmlEditor ID="PhraseContent2" runat="server" Height="100" Label="祝福语内容2："></ext:HtmlEditor>
            <ext:HtmlEditor ID="PhraseContent3" runat="server" Height="100" Label="祝福语内容3："></ext:HtmlEditor>
            <ext:HiddenField ID="uploadFilePath" runat="server"></ext:HiddenField>
            <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ID="FormRow2" ColumnWidths="0% 80% 10%" runat="server">
                        <Items>
                            <ext:Label ID="Label2" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="PhrasePicFile" runat="server" Label="图片：">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" Hidden="true" OnClick="btnBack_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow3" ColumnWidths="69% 11% 20%" runat="server">
                        <Items>
                        <ext:Label ID="Label3" runat="server" Label="图片："></ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad1" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad_Click"> 
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
