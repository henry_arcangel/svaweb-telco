﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.BasicInformationSetting.Departments.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="DepartCode" runat="server" Label="部门编号：" Required="true" ShowRedStar="true" Enabled="false"/>
            <ext:TextBox ID="DepartName1" runat="server" Label="描述：" Required="true" ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:TextBox ID="DepartName2" runat="server" Label="其他描述1：" ToolTipTitle="其他描述1" ToolTip="對部门的描述,不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:TextBox ID="DepartName3" runat="server" Label="其他描述2：" ToolTipTitle="其他描述2" ToolTip="對部门的另一個描述,不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Resizable="true"></ext:DropDownList>
            <ext:HtmlEditor ID="DepartNote" runat="server" Height="200" Label="说明：" ShowRedStar="true"></ext:HtmlEditor>
            <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%" runat="server">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="DepartPicFile" runat="server" Label="图片：">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" CssClass="mleft20"
                                OnClick="btnBack_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="69% 11% 20%" runat="server">
                        <Items>
                        <ext:Label ID="Label1" runat="server" Label="图片："></ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad" runat="server" Text="重新上传" HideMode="Display" 
                                OnClick="btnReUpLoad_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
