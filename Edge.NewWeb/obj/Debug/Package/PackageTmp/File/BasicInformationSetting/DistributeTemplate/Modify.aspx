﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.DistributeTemplate.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="DistributionCode" runat="server" Label="发布方法编号：" Required="true" MaxLength="20"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                ToolTipTitle="发布方法编号" ToolTip="Translate__Special_121_Start1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"
                Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="DistributionDesc1" runat="server" Label="描述：" Required="true" ShowRedStar="true" ToolTipTitle="描述" ToolTip="不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="DistributionDesc2" runat="server" Label="其他描述1：" ToolTipTitle="其他描述1" ToolTip="對描述的一個補充。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="DistributionDesc3" runat="server" Label="其他描述2：" ToolTipTitle="其他描述2" ToolTip="對描述的另一個補充。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <%--<ext:FileUpload ID="TemplateFile" runat="server" Label="模板文件" EnableAjax="false">
            </ext:FileUpload>--%>
             <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="TemplateFile" runat="server" Label="模板文件：" ToolTipTitle="模板文件" ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End" >
                            </ext:FileUpload>
                            <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" Hidden="true" OnClick="btnBack_Click" CssClass="mleft20">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ColumnWidths="68% 15% 17%">
                        <Items>
                            <ext:Label ID="Label3" runat="server" Label="模板文件："></ext:Label>
                            <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                EnableAjax="false" DisableControlBeforePostBack="false">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad" runat="server" Text="重新上传" OnClick="btnReUpLoad_Click"></ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:TextBox ID="Remark" runat="server" Label="备注：" ToolTipTitle="备注" ToolTip="不能超過512個字符">
            </ext:TextBox>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
