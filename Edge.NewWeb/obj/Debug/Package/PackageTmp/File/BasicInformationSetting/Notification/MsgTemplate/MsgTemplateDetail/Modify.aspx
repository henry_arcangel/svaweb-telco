﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate.MsgTemplateDetail.Modify" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right" LabelWidth="150">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:HiddenField ID="KeyID" runat="Server" ></ext:HiddenField>
            <ext:HiddenField ID="ObjectKey" runat="Server" ></ext:HiddenField>
            <ext:HiddenField ID="MessageTemplateID" runat="server" ></ext:HiddenField>
            <ext:DropDownList ID="MessageServiceTypeID" runat="server" Label="通知方式:" Required="true"
                ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值" Enabled="false"  AutoPostBack="true" OnSelectedIndexChanged="MessageServiceTypeID_SelectedIndexChanged">
            </ext:DropDownList>
            <ext:RadioButtonList ID="status" runat="server" Label="通知状态：" Width="100px">
                <ext:RadioItem Text="启动" Value="1" />
                <ext:RadioItem Text="关闭" Value="0" Selected="true" />
            </ext:RadioButtonList>
            <ext:TextBox ID="MessageTitle1" runat="server" Label="标题1：" Required="true" ShowRedStar="true">
            </ext:TextBox>
            <ext:TextBox ID="MessageTitle2" runat="server" Label="标题2：">
            </ext:TextBox>
            <ext:TextBox ID="MessageTitle3" runat="server" Label="标题3：">
            </ext:TextBox>
            <ext:HtmlEditor ID="TemplateContent1" runat="server" Height="150" Label="模版1："
                ShowRedStar="true">
            </ext:HtmlEditor>
            <ext:HtmlEditor ID="TemplateContent2" runat="server" Height="150" Label="模版2：">
            </ext:HtmlEditor>
            <ext:HtmlEditor ID="TemplateContent3" runat="server" Height="150" Label="模版3：">
            </ext:HtmlEditor>
        </Items>
    </ext:SimpleForm>
    </form>
</body>
</html>
