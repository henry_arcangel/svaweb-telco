﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_MessageType.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
          <ext:TextBox ID="NoticeNumber" runat="server" Label="通知编号:" Hidden=true Required="true" MaxLength="30">
            </ext:TextBox>
            <ext:DropDownList ID="MessageTemplateID" runat="server" Label="发送模版:" Required="true" ShowRedStar="true" Resizable="true">
            </ext:DropDownList> 
            <ext:RadioButtonList ID="Status" Label="是否生效" runat="server">
                <ext:RadioItem Text="否" Value="0"  Selected=true/>
                <ext:RadioItem Text="是" Value="1" />
            </ext:RadioButtonList>    
            <ext:RadioButtonList ID="DesignSendTimes" Label="是否一次性消息" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DesignSendTimes_SelectedIndexChanged">
                <ext:RadioItem Text="否" Value="0" />
                <ext:RadioItem Text="是" Value="1"  Selected="true"/>
            </ext:RadioButtonList>             
            <ext:DropDownList ID="FrequencyUnit" runat="server" Label="发送频率单位:" Required="true" ShowRedStar="true" Resizable="true">
            </ext:DropDownList> 
            <ext:TextBox ID="FrequencyValue" runat="server" Label="发送频率数量:" Required="true" ShowRedStar="true" RegexPattern=NUMBER RegexMessage="必须是数字">
            </ext:TextBox>                
            <ext:DatePicker ID="StartDate" runat="server" Label="发送日期：" Required="true" ShowRedStar="true" DateFormatString="yyyy-MM-dd">
            </ext:DatePicker>
            <ext:TimePicker ID="StartTime" runat="server" Label="发送时间：" Text="00:00" Increment="120"  Required="true" ShowRedStar="true">
            </ext:TimePicker>
            <%--<ext:TextBox ID="SendTimeStr" runat="server" Label="发送时间点：" Required="true" ShowRedStar="true"
                Regex="[01]?[4-9]|[012]?[0-3]:[0-5][0-9]:[0-5][0-9]" RegexMessage="请输入HH:mm:ss格式">
            </ext:TextBox>--%>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
