﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
</head>
<body>
    <form id="form1" runat="server">
     <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Grid1" />
    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
        runat="server" EnableCheckBoxSelect="True" DataKeyNames="NoticeNumber,NoticeDesc,StartDate,EndDate,ApproveStatus"
        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                    </ext:Button>
                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                    </ext:Button>
                    <ext:Button ID="btnApprove" Text="批核" Icon="Accept" OnClick="btnApprove_Click" runat="server">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Columns>
            <ext:TemplateField Width="60px" HeaderText="通知编号">
                <ItemTemplate>
                    <asp:Label ID="NoticeNumber" runat="server" Text='<%# Eval("NoticeNumber") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="通知描述">
                <ItemTemplate>
                    <asp:Label ID="NoticeDesc" runat="server" Text='<%# Eval("NoticeDesc") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>            
            <ext:TemplateField Width="60px" HeaderText="生效日期">
                <ItemTemplate>
                   <asp:Label ID="StartDate" runat="server" Text='<%#Eval("StartDate", "{0:yyyy-MM-dd}")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>                      
            <ext:TemplateField Width="60px" HeaderText="失效日期">
                <ItemTemplate>
                    <asp:Label ID="EndDate" runat="server" Text='<%#Eval("EndDate", "{0:yyyy-MM-dd}")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>                 
            <ext:TemplateField Width="60px" HeaderText="状态">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("ApproveStatus")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:WindowField ColumnID="ViewWindowField" Width="30px" WindowID="Window1" Icon="Page"
                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="NoticeNumber"
                DataIFrameUrlFormatString="Show.aspx?id={0}" 
                Title="查看" />
            <ext:WindowField ColumnID="EditWindowField" Width="30px" WindowID="Window2" Icon="PageEdit"
                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="NoticeNumber"
                DataIFrameUrlFormatString="Modify.aspx?id={0}" 
                Title="编辑" />
             <ext:WindowField ColumnID="SetFilterWindowField" Width="30px" WindowID="Window2" Icon="PageEdit"
                Text="设置筛选" ToolTip="设置筛选" DataTextFormatString="{0}" DataIFrameUrlFields="NoticeNumber"
                DataIFrameUrlFormatString="MemberNotice_Filter/List.aspx?id={0}" 
                Title="设置筛选"/>
             <ext:WindowField ColumnID="SetTriggerWindowField" Width="30px" WindowID="Window2" Icon="PageEdit"
                Text="设置模版" ToolTip="设置模版" DataTextFormatString="{0}" DataIFrameUrlFields="NoticeNumber"
                DataIFrameUrlFormatString="MemberNotice_MessageType/List.aspx?id={0}" 
                Title="设置模版"/>
        </Columns>
    </ext:Grid>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="900px"
        Height="450px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
