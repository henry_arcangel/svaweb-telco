﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_Filter.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="NoticeNumber" runat="server" Label="通知编号：" Required="true" MaxLength="30"
                Hidden="true">
            </ext:TextBox>
            <ext:DropDownList ID="BrandID" runat="server" Label="品牌:" Required="true" ShowRedStar="true"
                AutoPostBack="true" OnSelectedIndexChanged="BrandID_SelectedIndexChanged" Resizable="true"
                CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型:" AutoPostBack="true"
                OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别:">
            </ext:DropDownList>
            <ext:DropDownList ID="CouponTypeID" runat="server" Label="优惠券类型:">
            </ext:DropDownList>
            <ext:RadioButtonList ID="OnBirthDay" Label="是否生日触发:" runat="server">
                <ext:RadioItem Text="不是" Value="0" Selected="true" />
                <ext:RadioItem Text="是" Value="1" />
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
