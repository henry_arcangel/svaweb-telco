﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" LabelAlign="Right" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false"
                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="NoticeNumber" runat="server" Label="通知编号:" Text="">
                                    </ext:Label>
                                    <ext:Label ID="NoticeDesc" runat="server" Label="通知模板设置名称:" Text="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="StartDate" runat="server" Label="开始日期" Text="">
                                    </ext:Label>
                                    <ext:Label ID="EndDate" runat="server" Label="结束日期" Text="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Label ID="ApproveStatus" runat="server" Label="状态:" Text="">
                    </ext:Label>
                </Items>
            </ext:SimpleForm>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="筛选条件"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="KeyID,NoticeNumber,BrandCode,BrandName,CardTypeID,CardTypeCode,CardTypeName,CardGradeID,CardGradeName,CouponTypeID,CouponTypeCode,CouponTypeName,OnBirthDay,OnBirthDayName"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="keyid" Hidden="true">
                                <ItemTemplate>
                                    <asp:Label ID="KeyID" runat="server" Text='<%# Eval("KeyID") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="品牌">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrandCode" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                                    -
                                    <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡类型">
                                <ItemTemplate>
                                    <asp:Label ID="CardTypeCode" runat="server" Text='<%# Eval("CardTypeCode") %>'></asp:Label>
                                    -
                                    <asp:Label ID="CardTypeName" runat="server" Text='<%# Eval("CardTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="CardGradeName" runat="server" Text='<%# Eval("CardGradeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券类型">
                                <ItemTemplate>
                                    <asp:Label ID="CouponTypeCode" runat="server" Text='<%# Eval("CouponTypeCode") %>'></asp:Label>
                                    -
                                    <asp:Label ID="CouponTypeName" runat="server" Text='<%# Eval("CouponTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="是否生日触发">
                                <ItemTemplate>
                                    <asp:Label ID="OnBirthDayName" runat="server" Text='<%# Eval("OnBirthDayName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="KeyID"
                                DataIFrameUrlFormatString="MemberNotice_Filter/Show.aspx?id={0}" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="设置模版"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="KeyID,NoticeNumber,BrandCode,BrandName,CardTypeID,CardTypeCode,CardTypeName,CardGradeID,CardGradeName,CouponTypeID,CouponTypeCode,CouponTypeName,OnBirthDay,OnBirthDayName"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid2_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="0px" HeaderText="通知编号" Hidden="true">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoticeNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="发送模版">
                                <ItemTemplate>
                                    <asp:Label ID="MessageTemplateCode" runat="server" Text='<%# Eval("MessageTemplateCode") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="是否生效">
                                <ItemTemplate>
                                    <asp:Label ID="Status" runat="server" Text='<%# Eval("StatusName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="发送频率单位">
                                <ItemTemplate>
                                    <asp:Label ID="FrequencyUnitName" runat="server" Text='<%#Eval("FrequencyUnitName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="发送频率数量">
                                <ItemTemplate>
                                    <asp:Label ID="FrequencyValue" runat="server" Text='<%# Eval("FrequencyValue") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="发送时间点">
                                <ItemTemplate>
                                    <asp:Label ID="SendTime" runat="server" Text='<%# Eval("SendTime","{0:HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="KeyID"
                                DataIFrameUrlFormatString="MemberNotice_MessageType/Show.aspx?id={0}" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="740px" Height="400px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
