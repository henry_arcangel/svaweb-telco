﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.SysManage.CouponAutoPickingRule.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform4" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="CouponAutoPickingRuleCode" runat="server" Label="规则编号：" Required="true" ShowRedStar="true" Enabled="false">
                                    </ext:TextBox>
                                     <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Description" runat="server" Label="描述：" Required="true" ShowRedStar="true">
                                    </ext:TextBox>
                                     <ext:Label ID="Label2" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="开始日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label3" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="结束日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label4" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="Status" runat="server" Label="状态：" Required="true" ShowRedStar="true">
                                        <ext:RadioItem Text="生效" Value="1" Selected="true" />
                                        <ext:RadioItem Text="关闭" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="时间设置信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="DayFlagID" runat="server" Label="有效天数设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblDayFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="WeekFlagID" runat="server" Label="有效周设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblWeekFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="MonthFlagID" runat="server" Label="有效月设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblMonthFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="Inventory Location List"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Button ID="SearchButton1" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton1_Click">
                                    </ext:Button>
                                    <ext:Button ID="btnNew1" Text="添加" Icon="Add" runat="server" OnClick="btnNew1_Click">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="ListType" runat="server" Label="Select Brand/Store：" Required="true" AutoPostBack="true" OnSelectedIndexChanged="ListType_SelectedIndexChanged">
                                        <ext:RadioItem Text="Brand" Value="1" Selected="true" />
                                        <ext:RadioItem Text="Store" Value="2" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label10" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" 
                                        OnSelectedIndexChanged="BrandID_SelectedIndexChanged" AutoPostBack="true" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Label6" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="BrandName" runat="server" Label="Brand Name：">
                                    </ext:TextBox>
                                    <ext:Label ID="Label7" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="StoreID" runat="server" Label="店铺：" 
                                        OnSelectedIndexChanged="StoreID_SelectedIndexChanged" AutoPostBack="true" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Label8" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="StoreName" runat="server" Label="Store Name：">
                                    </ext:TextBox>
                                    <ext:Label ID="Label9" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="Select Result"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Panel ID="Panel11" runat="Server" BodyPadding="3px" EnableBackgroundColor="true" ShowHeader="false" ShowBorder="false" Layout="Column">
                                <Items>
                                    <ext:CheckBox ID="cbSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="cbSelectAll_OnCheckedChanged" Text="添加所有搜索结果">
                                    </ext:CheckBox>
                                </Items>
                    </ext:Panel>
                    <ext:Form ID="sForm3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:CheckBoxList ID="checkBoxList1"  runat="server" ColumnNumber="5" ColumnVertical="true"></ext:CheckBoxList>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="Brand/Store List">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="true" ShowHeader="true" Title="自动绑定列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                    <ext:Button ID="btnClear" Text="清空" Icon="Delete" runat="server" OnClick="btnClear_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="BrandCode" DataField="BrandCode" HeaderText="Brand Code" />
                            <ext:BoundField ColumnID="BrandName" DataField="BrandName" HeaderText="Brand Name" />
                            <ext:BoundField ColumnID="StoreCode" DataField="StoreCode" HeaderText="Store Code" />
                            <ext:BoundField ColumnID="StoreName" DataField="StoreName" HeaderText="Store Name" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>




