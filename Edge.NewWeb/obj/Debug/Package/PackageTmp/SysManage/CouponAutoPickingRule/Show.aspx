﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.SysManage.CouponAutoPickingRule.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                     <ext:Label ID="CouponAutoPickingRuleCode" runat="server" Label="规则编号：">
                                    </ext:Label>
                                     <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                     <ext:Label ID="Description" runat="server" Label="描述：">
                                    </ext:Label>
                                     <ext:Label ID="Label2" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                     <ext:Label ID="StartDate" runat="server" Label="开始日期：">
                                    </ext:Label>
                                    <ext:Label ID="Label3" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                     <ext:Label ID="EndDate" runat="server" Label="结束日期：">
                                    </ext:Label>
                                    <ext:Label ID="Label4" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StatusName" runat="server" Label="状态：">
                                    </ext:Label>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="时间设置信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="DayFlagID" runat="server" Label="有效天数设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblDayFlagCode" runat="server" Label="有效天数设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="WeekFlagID" runat="server" Label="有效周设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblWeekFlagCode" runat="server" Label="有效周设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="MonthFlagID" runat="server" Label="有效月设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblMonthFlagCode" runat="server" Label="有效月设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="Brand/Store List">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="true" ShowHeader="true" Title="自动绑定列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_OnPageIndexChange">
                        <Columns>
                            <ext:BoundField ColumnID="BrandCode" DataField="BrandCode" HeaderText="Brand Code" />
                            <ext:BoundField ColumnID="BrandName" DataField="BrandName" HeaderText="Brand Name" />
                            <ext:BoundField ColumnID="StoreCode" DataField="StoreCode" HeaderText="Store Code" />
                            <ext:BoundField ColumnID="StoreName" DataField="StoreName" HeaderText="Store Name" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
