﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addDetail.aspx.cs" Inherits="Edge.Web.SysManage.AirloadMonitoringRule.addDetail" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sForm4,sForm5" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="添加后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="库存(出)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="Supplier" runat="server" Label="供应商名称：" Resizable="true" AutoPostBack="true" OnSelectedIndexChanged="Supplier_SelectedIndexChanged"></ext:DropDownList>
                                    <ext:DropDownList ID="Brand1" runat="server" Label="店铺所属品牌：" Resizable="true"></ext:DropDownList>
                                    <ext:DropDownList ID="StoreType1" runat="server" Label="店铺类型：" Resizable="true" Enabled="false">
                                        <ext:ListItem Text="总部" Value="1" />
                                        <ext:ListItem Text="店铺" Value="2" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Desc1" runat="server" Label="店铺名称：" MaxLength="512">
                                    </ext:TextBox>
                                    <ext:Button ID="SearchButton1" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton1_Click">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="可选店铺"
                AutoHeight="true" AutoWidth="true">
                <Items>

                    <ext:Form ID="sForm2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:CheckBoxList ID="checkBoxList1"  runat="server" ColumnNumber="5" ColumnVertical="true"></ext:CheckBoxList>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="库存(入)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="Brand2" runat="server" Label="店铺所属品牌：" Resizable="true"></ext:DropDownList>
                                        <ext:DropDownList ID="StoreType2" runat="server" Label="店铺类型：" Resizable="true" Enabled="false">
                                            <ext:ListItem Text="总部" Value="1" />
                                            <ext:ListItem Text="店铺" Value="2" />
                                        </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Desc2" runat="server" Label="店铺名称：" MaxLength="512">
                                    </ext:TextBox>
                                    <ext:Button ID="SearchButton2" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton2_Click">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="可选店铺"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Panel ID="Panel11" runat="Server" BodyPadding="3px" EnableBackgroundColor="true" ShowHeader="false" ShowBorder="false" Layout="Column">
                         <Items>
                            <ext:CheckBox ID="cbSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="cbSelectAll_OnCheckedChanged" Text="添加所有搜索结果">
                            </ext:CheckBox>
                        </Items>
                    </ext:Panel>
                    <ext:Form ID="sForm3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:CheckBoxList ID="checkBoxList2" runat="server"  ColumnNumber="5" ColumnVertical="true"></ext:CheckBoxList>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="补货信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Priority" runat="server" Label="优先级：" ShowRedStar="true" Required="true" Regex="^[0-9]*[1-9][0-9]*$">
                                    </ext:TextBox>
                                    <ext:TextBox ID="MinAmtBalance" runat="server" Label="最低库存：" ShowRedStar="true" Required="true" Regex="^[0-9]*[1-9][0-9]*$">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="RunningAmtBalance" runat="server" Label="常规库存：" ShowRedStar="true" Required="true" Regex="^[0-9]*[1-9][0-9]*$">
                                    </ext:TextBox>
                                    <ext:Label ID="Label3" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>