﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="modifyDetail.aspx.cs" Inherits="Edge.Web.SysManage.AirloadMonitoringRule.modifyDetail" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sForm5" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="补货信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="OrderTargetID" runat="server" Label="库存(出)地点：" Resizable="true" Enabled="false"></ext:DropDownList>
                                     <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="StoreID" runat="server" Label="库存(入)地点：" Resizable="true" Enabled="false"></ext:DropDownList>
                                     <ext:Label ID="Label2" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Priority" runat="server" Label="优先级：" ShowRedStar="true" Required="true" Regex="^[0-9]*[1-9][0-9]*$">
                                    </ext:TextBox>
                                     <ext:Label ID="Label3" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="MinAmtBalance" runat="server" Label="最低库存：" ShowRedStar="true" Required="true">
                                    </ext:NumberBox>
                                    <ext:Label ID="Label4" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:NumberBox ID="RunningAmtBalance" runat="server" Label="常规库存：" ShowRedStar="true" Required="true">
                                    </ext:NumberBox>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
