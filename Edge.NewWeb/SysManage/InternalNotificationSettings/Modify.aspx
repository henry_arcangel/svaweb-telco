﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.SysManage.InternalNotificationSettings.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="内部消息通知设置"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="UserMessageCode" runat="server" Label="通知编号：" Required="true" ShowRedStar="true" Enabled="false">
                                    </ext:TextBox>
                                     <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="UserMessageDesc" runat="server" Label="通知描述：">
                                    </ext:TextBox>
                                     <ext:Label ID="Label2" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="StartDate" runat="server" Label="开始时间：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="开始时间" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label3" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="EndDate" runat="server" Label="结束时间：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="结束时间" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label4" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="Status" runat="server" Label="状态：">
                                        <ext:RadioItem Text="启动" Value="1" Selected="true" />
                                        <ext:RadioItem Text="关闭" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="SendUserName" runat="server" Label="发送用户：" Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="SendUserID" runat="server" Label="发送用户：" Hidden="true">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="UserMessageType" runat="server" Label="功能类型：" Resizable="true" Required="true" ShowRedStar="true"
                                     CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                        <ext:ListItem Text="-------" Value="-1"  Selected="true"/>
                                        <ext:ListItem Text="Ord_CardBatchCreate" Value="1" />
                                        <ext:ListItem Text="Card Order" Value="2" />
                                        <ext:ListItem Text="Card AR" Value="3" />
                                        <ext:ListItem Text="Card AR Confirmation" Value="4" />
                                        <ext:ListItem Text="Ord_CardAdjust_H" Value="5" />
                                        <ext:ListItem Text="Ord_CardTransfer" Value="6" />
                                        <ext:ListItem Text="Ord_CouponBatchCreate" Value="7" />
                                        <ext:ListItem Text="Coupon Order" Value="8" />
                                        <ext:ListItem Text="Coupon Creation - Automatic" Value="9" />
                                        <ext:ListItem Text="Coupon Return Confirmation" Value="10" />
                                        <ext:ListItem Text="Coupon AR" Value="11" />
                                        <ext:ListItem Text="Coupon AR Confirmation" Value="12" />
                                        <ext:ListItem Text="Coupon Return Order" Value="13" />
                                        <ext:ListItem Text="Ord_CouponPush_H" Value="14" />
                                        <ext:ListItem Text="Ord_CouponAdjust_H" Value="15" />
                                        <ext:ListItem Text="Ord_ImportCardUID_H" Value="16" />
                                        <ext:ListItem Text="Ord_ImportCouponUID_H" Value="17" />
                                        <ext:ListItem Text="Ord_ImportCouponDispense_H" Value="18" />
                                        <ext:ListItem Text="Ord_ImportMember_H" Value="19" />
                                    </ext:DropDownList>
                                    <ext:Label ID="Label10" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="TriggeType" runat="server" Label="发送通知的触发条件：">
                                        <ext:RadioItem Text="创建" Value="0" Selected="true" />
                                        <ext:RadioItem Text="批核" Value="1" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label9" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="UserMessageTitle" runat="server" Label="标题1：" Required="true" ShowRedStar="true">
                                    </ext:TextBox>
                                     <ext:Label ID="Label11" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextArea ID="UserMessageContent" runat="server" Label="讯息内容1：" Required="true" ShowRedStar="true" >
                                    </ext:TextArea>
                                     <ext:Label ID="Label12" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="收件用户列表">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="true" ShowHeader="true" Title="自动绑定列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew1" Text="添加" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="AccountNumber" DataField="AccountNumber" HeaderText="收件用户" />
                            <ext:BoundField ColumnID="MessageServiceTypeName" DataField="MessageServiceTypeName" HeaderText="发送通知渠道" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>