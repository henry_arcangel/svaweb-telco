﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.SysManage.InternalNotificationSettings
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.UserMessageSetting_H, Edge.SVA.Model.UserMessageSetting_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                SendUserID.Text = DALTool.GetCurrentUser().UserID.ToString();
                SendUserName.Text = DALTool.GetCurrentUser().UserName;

                RegisterCloseEvent(btnClose);
                btnNew1.OnClientClick = Window1.GetShowReference("AddDetail.aspx", "新增");

                DataTable dt = new DataTable();
                dt.Columns.Add("KeyID", typeof(int));
                dt.Columns.Add("UserMessageCode", typeof(string));
                dt.Columns.Add("ReceiveUserID", typeof(int));
                dt.Columns.Add("MessageServiceTypeID", typeof(int));
                dt.Columns.Add("AccountNumber", typeof(string));
                dt.Columns.Add("MessageServiceTypeName", typeof(string));
                Session["UserMessageSetting_D"] = dt;
            }

            BindingDataGrid();

        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            DataTable dt = (DataTable)Session["UserMessageSetting_D"];
            this.Grid1.RecordCount = dt.Rows.Count;
            this.Grid1.DataSource = dt;
            this.Grid1.DataBind();
            this.Grid1.SelectedRowIndexArray = rowIndexs;
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["UserMessageSetting_D"];
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (this.Grid1.Rows[rowIndexs[i]].Values[0] == dr["AccountNumber"].ToString() && this.Grid1.Rows[rowIndexs[i]].Values[1] == dr["MessageServiceTypeName"].ToString())
                    {
                        dt.Rows.Remove(dr);
                        break;
                    }
                }
            }

            Session["UserMessageSetting_D"] = dt;
            BindingDataGrid();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.UserMessageSetting_H item = this.GetAddObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("UserMessageSetting  {0} No Data", this.UserMessageCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            DataTable dt = (DataTable)Session["UserMessageSetting_D"];
            if (dt.Rows.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("UserMessageSetting Form  {0} Detail No Data", UserMessageCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UserMessageContent = UserMessageContent.Text;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;
            item.CreatedOn = DateTime.Now;

            if (Tools.DALTool.isHasUserMessageSetting(UserMessageCode.Text))
            {
                ShowWarning("已经存在" + UserMessageCode.Text + "的通知编号");
                return;
            }

            if (Tools.DALTool.Add<Edge.SVA.BLL.UserMessageSetting_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("UserMessageSetting_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (DataRow dr in dt.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["UserMessageCode"] = UserMessageCode.Text;
                        row["ReceiveUserID"] = Convert.ToInt32(Convert.ToString(dr["ReceiveUserID"]));
                        row["MessageServiceTypeID"] = Convert.ToInt32(Convert.ToString(dr["MessageServiceTypeID"]));
                        row["AccountNumber"] = Convert.ToString(dr["AccountNumber"]);
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "UserMessageSetting_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("UserMessageSetting  {0} Add Success But Detail Failed", UserMessageCode.Text), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("UserMessageSetting  {0} Add Success", item.UserMessageCode));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("UserMessageSetting  {0} Add Failed", item.UserMessageCode));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void Grid1_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindingDataGrid();
        }

        protected void UserMessageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserMessageType.SelectedIndex == 1)
            {
            }
        }

        protected override SVA.Model.UserMessageSetting_H GetPageObject(SVA.Model.UserMessageSetting_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }
    }
}