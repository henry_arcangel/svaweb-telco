﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_H, Edge.SVA.Model.InventoryReplenishRule_H>
    {
        InventoryReplenishRuleAddController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.InventoryReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                controller = new InventoryReplenishRuleAddController();
                Edge.SVA.Model.InventoryReplenishRule_H model = controller.GetModel(id);
                InventoryReplenishCode.Text = model.InventoryReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                if (model.Status == 1)
                {
                    StatusName.Text = "生效";
                }
                else 
                {
                    StatusName.Text = "关闭";
                }
                BrandID.SelectedValue = model.BrandID.ToString();
                Brand.Text = BrandID.SelectedText;
                ControlTool.BindCouponType(CouponTypeID, (int)model.BrandID);
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", (int)model.BrandID));
                CouponTypeID.SelectedValue = model.CouponTypeID.ToString();
                CouponType.Text = CouponTypeID.SelectedText;
                CardTypeID.SelectedValue = model.CardTypeID.ToString();
                CardType.Text = CardTypeID.SelectedText;
                ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
                CardGradeID.SelectedValue = model.CardGradeID.ToString();
                CardGrade.Text = CardGradeID.SelectedText;
                ControlTool.BindDayFlagID(DayFlagID);
                DayFlagID.SelectedValue = model.DayFlagID.ToString();
                lblDayFlagCode.Text = DayFlagID.SelectedText;
                ControlTool.BindWeekFlagID(WeekFlagID);
                WeekFlagID.SelectedValue = model.WeekFlagID.ToString();
                lblWeekFlagCode.Text = WeekFlagID.SelectedText;
                ControlTool.BindMonthFlagID(MonthFlagID);
                MonthFlagID.SelectedValue = model.MonthFlagID.ToString();
                this.lblMonthFlagCode.Text = MonthFlagID.SelectedText;

                MediaType.SelectedValue = model.MediaType.ToString();
                lblMediaType.Text = MediaType.SelectedItem.Text;
                if (model.MediaType == 1)
                {
                    CouponType.Hidden = false;
                    CardType.Hidden = true;
                    CardGrade.Hidden = true;
                }
                else
                {
                    CouponType.Hidden = true;
                    CardType.Hidden = false;
                    CardGrade.Hidden = false;
                }

                if (model.StoreTypeID == 1)
                {
                    StoreType.Text = "总部";
                    GroupPanel6.Title = "总部列表";
                }
                else
                {
                    StoreType.Text = "店铺";
                    GroupPanel6.Title = "店铺列表";
                }
                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.InventoryReplenishRuleAddController = controller;

                //Add by Robin 20140520
                //this.BindDetail();

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            this.InventoryReplenishRuleList.RecordCount = controller.ViewModel.InventoryReplenishRuleViewModelList.Count;
            this.InventoryReplenishRuleList.DataSource = controller.ViewModel.InventoryReplenishRuleViewModelList;
            this.InventoryReplenishRuleList.DataBind();
            this.InventoryReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

	protected void CouponReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            InventoryReplenishRuleList.PageIndex = e.NewPageIndex;

            BindingDataGrid();

        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    InventoryReplenishRuleList.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("InventoryReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinStockQty", typeof(int));
                    dt.Columns.Add("RunningStockQty", typeof(int));
                    dt.Columns.Add("OrderRoundUpQty", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        //private void BindDetail()
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        GroupPanel6.Title = "总部列表";
        //    }
        //    else 
        //    {
        //        GroupPanel6.Title = "店铺列表";
        //    }
        //    this.InventoryReplenishRuleList.RecordCount = this.Detail.Rows.Count;
        //    this.InventoryReplenishRuleList.DataSource = DataTool.GetPaggingTable(this.InventoryReplenishRuleList.PageIndex, this.InventoryReplenishRuleList.PageSize, this.Detail);
        //    this.InventoryReplenishRuleList.DataBind();
        //}

        //private int DetailIndex
        //{
        //    get
        //    {
        //        if (ViewState["DetailIndex"] == null)
        //        {
        //            ViewState["DetailIndex"] = 0;
        //        }
        //        ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
        //        return (int)ViewState["DetailIndex"];
        //    }
        //}
    }
}