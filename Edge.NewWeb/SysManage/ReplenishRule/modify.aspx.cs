﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_H, Edge.SVA.Model.InventoryReplenishRule_H>
    {
        InventoryReplenishRuleAddController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.InventoryReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                btnNew1.OnClientClick = "";

                controller = new InventoryReplenishRuleAddController();
                Edge.SVA.Model.InventoryReplenishRule_H model = controller.GetModel(id);
                InventoryReplenishCode.Text = model.InventoryReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                Status.SelectedValue = model.Status.ToString();
                BrandID.SelectedValue = model.BrandID.ToString();
                ControlTool.BindCouponType(CouponTypeID, (int)model.BrandID);
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", (int)model.BrandID));
                CouponTypeID.SelectedValue = model.CouponTypeID.ToString();
                CardTypeID.SelectedValue = model.CardTypeID.ToString();
                ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
                CardGradeID.SelectedValue = model.CardGradeID.ToString();
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);

                string TypeOrGradeID = "";
                MediaType.SelectedValue = model.MediaType.ToString();
                if (model.MediaType == 1)
                {
                    CouponTypeID.Hidden = false;
                    CardTypeID.Hidden = true;
                    CardGradeID.Hidden = true;
                    TypeOrGradeID = CouponTypeID.SelectedItem.Value;
                }
                else
                {
                    CouponTypeID.Hidden = true;
                    CardTypeID.Hidden = false;
                    CardGradeID.Hidden = false;
                    TypeOrGradeID = CardGradeID.SelectedItem.Value;
                }

                StoreTypeID.SelectedValue = model.StoreTypeID.ToString();
                if (model.StoreTypeID == 1)
                {
                    GroupPanel6.Title = "总部列表";
                    //btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1", "Add");
                    string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }
                else 
                {
                    GroupPanel6.Title = "店铺列表";
                    //btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=2", "Add");
                    string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }

                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.InventoryReplenishRuleAddController = controller;

                //Add by Robin 20140520
                //this.BindDetail();

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            this.InventoryReplenishRuleList.RecordCount = controller.ViewModel.InventoryReplenishRuleViewModelList.Count;
            this.InventoryReplenishRuleList.DataSource = controller.ViewModel.InventoryReplenishRuleViewModelList;
            this.InventoryReplenishRuleList.DataBind();
            this.InventoryReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(CouponTypeID, string.Format("BrandID = {0}  order by CouponTypeCode", brandID));
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        //protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        if (CouponTypeID.SelectedValue != "-1")
        //        {
        //            btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1&CouponTypeID=" + CouponTypeID.SelectedValue, "Add");
        //        }
        //        else
        //        {
        //            btnNew1.OnClientClick = "";
        //        }
        //    }
        //    else
        //    {
        //        btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=2", "Add");
        //    }
        //}

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            List<InventoryReplenishRuleViewModel> modelList = new List<InventoryReplenishRuleViewModel>();
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
                {
                    if (this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[6] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[7] == model.MainTable.OrderTargetID.ToString())
                    {
                        modelList.Add(model);
                        break;
                    }
                }
            }

            foreach (InventoryReplenishRuleViewModel model in modelList)
            {
                controller.ViewModel.InventoryReplenishRuleViewModelList.Remove(model);
            }
            SVASessionInfo.InventoryReplenishRuleAddController = controller;
            this.InventoryReplenishRuleList.SelectedRowIndexArray = new int[0];
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            SVASessionInfo.InventoryReplenishRuleAddController = null;
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            BindingDataGrid();
        }

        protected void InventoryReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            InventoryReplenishRuleList.PageIndex = e.NewPageIndex;

            BindingDataGrid();

        }

        protected void StoreTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            SVASessionInfo.InventoryReplenishRuleAddController = null;
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            BindingDataGrid();
            string TypeOrGradeID = "";
            if (MediaType.SelectedValue == "1")
            {
                TypeOrGradeID = CouponTypeID.SelectedItem.Value;
            }
            else
            {
                TypeOrGradeID = CardGradeID.SelectedItem.Value;
            }
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void MediaType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SVASessionInfo.InventoryReplenishRuleAddController = null;
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            BindingDataGrid();
            string TypeOrGradeID = "";
            if (MediaType.SelectedValue == "1")
            {
                CouponTypeID.Hidden = false;
                CardTypeID.Hidden = true;
                CardGradeID.Hidden = true;
                TypeOrGradeID = CouponTypeID.SelectedItem.Value;
            }
            else
            {
                CouponTypeID.Hidden = true;
                CardTypeID.Hidden = false;
                CardGradeID.Hidden = false;
                TypeOrGradeID = CardGradeID.SelectedItem.Value;
            }
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=" + MediaType.SelectedValue;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.BrandID.SelectedValue == "-1")
            {
                ShowWarning("请选择品牌！");
                return;
            }

            if (MediaType.SelectedValue == "1")
            {
                if (this.CouponTypeID.SelectedValue == "-1")
                {
                    ShowWarning(Resources.MessageTips.CouponCodeNotNull);
                    return;
                }
            }
            else
            {
                if (this.CardGradeID.SelectedValue == "-1")
                {
                    //todo
                    ShowWarning(Resources.MessageTips.CouponCodeNotNull);
                    return;
                }
            }

            Edge.SVA.Model.InventoryReplenishRule_H item = this.GetUpdateObject();
            
            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish  {0} No Data", InventoryReplenishCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            controller = SVASessionInfo.InventoryReplenishRuleAddController;
            if (this.controller.ViewModel.InventoryReplenishRuleViewModelList.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish Form  {0} Detail No Data", InventoryReplenishCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.InventoryReplenishRule_H>(item))
            {
                Edge.SVA.BLL.InventoryReplenishRule_D bll = new SVA.BLL.InventoryReplenishRule_D();
                bll.DeleteByInventoryReplenishCode(this.InventoryReplenishCode.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("InventoryReplenishRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (InventoryReplenishRuleViewModel model in this.controller.ViewModel.InventoryReplenishRuleViewModelList)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["InventoryReplenishCode"] = item.InventoryReplenishCode;
                        row["StoreID"] = model.MainTable.StoreID;
                        row["OrderTargetID"] = model.MainTable.OrderTargetID;
                        row["MinStockQty"] = model.MainTable.MinStockQty;
                        row["RunningStockQty"] = model.MainTable.RunningStockQty;
                        row["OrderRoundUpQty"] = model.MainTable.OrderRoundUpQty;
                        row["Priority"] = model.MainTable.Priority;
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "InventoryReplenishRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Success But Detail Failed", InventoryReplenishCode.Text), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowUpdateFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Success", item.InventoryReplenishCode));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Failed", item.InventoryReplenishCode));
                ShowUpdateFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    InventoryReplenishRuleList.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        //protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        //{
        //    if (e.CommandName == "Delete")
        //    {
        //        object[] keys = InventoryReplenishRuleList.DataKeys[e.RowIndex];
        //        string keyID = keys[0].ToString();
        //        DeleteDetail(keyID);
        //        BindDetail();
        //    }
        //}

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("InventoryReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinStockQty", typeof(int));
                    dt.Columns.Add("RunningStockQty", typeof(int));
                    dt.Columns.Add("OrderRoundUpQty", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        //private void DeleteDetail(string keyID)
        //{
        //    foreach (System.Data.DataRow row in this.Detail.Rows)
        //    {
        //        if (row["KeyID"].ToString().Equals(keyID.ToString()))
        //        {
        //            row.Delete();
        //            break;
        //        }
        //    }
        //    this.Detail.AcceptChanges();
        //    this.BindDetail();
        //}

        //private void BindDetail()
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        GroupPanel6.Title = "总部列表";
        //    }
        //    else 
        //    {
        //        GroupPanel6.Title = "店铺列表";
        //    }
        //    this.InventoryReplenishRuleList.RecordCount = this.Detail.Rows.Count;
        //    this.InventoryReplenishRuleList.DataSource = DataTool.GetPaggingTable(this.InventoryReplenishRuleList.PageIndex, this.InventoryReplenishRuleList.PageSize, this.Detail);
        //    this.InventoryReplenishRuleList.DataBind();
        //}

        //private int DetailIndex
        //{
        //    get
        //    {
        //        if (ViewState["DetailIndex"] == null)
        //        {
        //            ViewState["DetailIndex"] = 0;
        //        }
        //        ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
        //        return (int)ViewState["DetailIndex"];
        //    }
        //}

        protected override SVA.Model.InventoryReplenishRule_H GetPageObject(SVA.Model.InventoryReplenishRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + this.CouponTypeID.SelectedItem.Value + "&MediaType=1";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + this.CouponTypeID.SelectedItem.Value + "&MediaType=1";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }
    }
}