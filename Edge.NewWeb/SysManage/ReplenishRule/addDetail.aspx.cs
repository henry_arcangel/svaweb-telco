﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using System.Data;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class addDetail : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_D, Edge.SVA.Model.InventoryReplenishRule_D>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["StoreTypeID"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    if (Request["StoreTypeID"].ToString() == "1")
                    {
                        //if (Request["CouponTypeID"] == null)
                        //{
                        //    ActiveWindow.GetHidePostBackReference();
                        //    return;
                        //}

                        this.Supplier.Hidden = false;
                        this.Brand1.Hidden = true;
                        this.StoreType1.Hidden = true;
                        this.Desc1.Hidden = true;
                        this.SearchButton1.Hidden = true;
                        this.GroupPanel2.Hidden = true;

                        //ControlTool.BindSupplier(this.Supplier, Convert.ToInt32(Request["CouponTypeID"]));
                        ControlTool.BindAllSupplier(this.Supplier);

                        this.checkBoxList1.Items.Clear();
                        this.Supplier.Items[0].Selected = true;
                        this.checkBoxList1.Items.Add(this.Supplier.SelectedText,this.Supplier.SelectedValue);
                        this.checkBoxList1.Items[0].Selected = true;
                        this.StoreType1.SelectedValue = "1";
                        this.StoreType2.SelectedValue = "1";
                    }
                    else
                    {
                        this.Supplier.Hidden = true;
                        this.Brand1.Hidden = false;
                        this.StoreType1.Hidden = false;
                        this.Desc1.Hidden = false;
                        this.SearchButton1.Hidden = false;

                        this.StoreType1.SelectedValue = "1";
                        this.StoreType2.SelectedValue = "2";
                    }

                    ControlTool.BindBrand(this.Brand1);
                    ControlTool.BindBrand(this.Brand2);
                }

                RegisterCloseEvent(btnClose);
            }
        }

        protected void SearchButton1_Click(object sender, EventArgs e)
        {
            RptBind1("StoreID>0", "StoreCode");
        }

        protected void SearchButton2_Click(object sender, EventArgs e)
        {
            RptBind2("StoreID>0", "StoreCode");
        }

        private void RptBind1(string strWhere, string orderby)
        {
            this.checkBoxList1.Items.Clear();
            try
            {
                StringBuilder sb = new StringBuilder(strWhere);
                int brandid = this.Brand1.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.Brand1.SelectedValue);
                string desc = this.Desc1.Text.Trim();
                if (brandid > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" BrandID =");
                    sb.Append(brandid);
                    sb.Append("");
                }
                string descLan = DALTool.GetStringByCulture("StoreName1", "StoreName2", "StoreName3");
                if (!string.IsNullOrEmpty(desc))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }

                    sb.Append(descLan);
                    sb.Append(" like '%");
                    sb.Append(desc);
                    sb.Append("%'");
                }

                sb.Append(" and storetypeid =");
                sb.Append(this.StoreType1.SelectedValue);
                sb.Append("");

                strWhere = sb.ToString();

                StoreController controller = new StoreController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, 1000, 0, out count, orderby);
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        this.checkBoxList1.Items.Add(Convert.ToString(dr[descLan]), Convert.ToString(dr["StoreID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("Store", "Load Filed", ex);
            }
        }

        private void RptBind2(string strWhere, string orderby)
        {
            this.checkBoxList2.Items.Clear();
            try
            {
                StringBuilder sb = new StringBuilder(strWhere);
                int brandid = this.Brand2.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.Brand2.SelectedValue);
                string desc = this.Desc2.Text.Trim();
                if (brandid > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" BrandID =");
                    sb.Append(brandid);
                    sb.Append("");
                }
                string descLan = DALTool.GetStringByCulture("StoreName1", "StoreName2", "StoreName3");
                if (!string.IsNullOrEmpty(desc))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }

                    sb.Append(descLan);
                    sb.Append(" like '%");
                    sb.Append(desc);
                    sb.Append("%'");
                }

                sb.Append(" and storetypeid =");
                sb.Append(this.StoreType2.SelectedValue);
                sb.Append("");

                strWhere = sb.ToString();

                StoreController controller = new StoreController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, 1000, 0, out count, orderby);
                //Add By Robin 2014-08-08 过滤已经加过的店铺
                InventoryReplenishRuleController controller1 = new InventoryReplenishRuleAddController();
                int MediaType = int.Parse(Request["MediaType"].ToString());
                int TypeOrGradeID;
                if (string.IsNullOrEmpty(Request["TypeOrGradeID"].ToString())) 
                {
                    TypeOrGradeID = -1; 
                }
                else 
                {
                    TypeOrGradeID = int.Parse(Request["TypeOrGradeID"].ToString());
                }
                controller1.GetAllDetailList(int.Parse(this.StoreType2.SelectedValue), TypeOrGradeID, MediaType);
                InventoryReplenishRuleViewModel model;
                int[] arrOrderTargetID = this.checkBoxList1.SelectedIndexArray;
                if (arrOrderTargetID.Length == 0 || this.checkBoxList1.Items[arrOrderTargetID[0]].Value == "-1") 
                {
                    ShowWarning("Stock exit location must have value!");
                    return;
                }
                Boolean CanAdd;
                //End
                
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        //Add By Robin 2014-08-08 过滤已经加过的店铺
                        CanAdd = true;
                        foreach (int i in arrOrderTargetID)
                        {
                            model = new InventoryReplenishRuleViewModel();
                            model.MainTable.OrderTargetID = StringHelper.ConvertToInt(this.checkBoxList1.Items[i].Value);
                            model.MainTable.StoreID = Convert.ToInt16(dr["StoreID"]);
                            if (controller1.Exists(model))
                            {
                                CanAdd = false;
                            }
                        }
                        if (CanAdd)
                        //End
                        { this.checkBoxList2.Items.Add(Convert.ToString(dr[descLan]), Convert.ToString(dr["StoreID"])); }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("Store", "Load Filed", ex);
            }
        }

        protected void Supplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.checkBoxList1.Items.Clear();
            this.checkBoxList1.Items.Add(this.Supplier.SelectedText, this.Supplier.SelectedValue);
            this.checkBoxList1.Items[0].Selected = true;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            InventoryReplenishRuleController controller = SVASessionInfo.InventoryReplenishRuleAddController;

            InventoryReplenishRuleViewModel model;
            int[] arrOrderTargetID = this.checkBoxList1.SelectedIndexArray;
            int[] arrStoreID = this.checkBoxList2.SelectedIndexArray;
            if (arrOrderTargetID.Length == 0 || this.checkBoxList1.Items[arrOrderTargetID[0]].Value == "-1") 
            {
                ShowWarning(" OrderTargetID must has value!");
                return;
            }
            if (arrStoreID.Length == 0)
            {
                ShowWarning(" StoreID must has value!");
                return;
            }
            foreach (int i in arrOrderTargetID)
            {
                foreach (int j in arrStoreID) 
                {
                    model = new InventoryReplenishRuleViewModel();
                    model.MainTable.OrderTargetID = StringHelper.ConvertToInt(this.checkBoxList1.Items[i].Value);
                    model.MainTable.StoreID = StringHelper.ConvertToInt(this.checkBoxList2.Items[j].Value); ;
                    model.MainTable.MinStockQty = StringHelper.ConvertToInt(this.MinStockQty.Text);
                    model.MainTable.RunningStockQty = StringHelper.ConvertToInt(this.RunningStockQty.Text);
                    model.MainTable.OrderRoundUpQty = StringHelper.ConvertToInt(this.OrderRoundUpQty.Text);
                    model.MainTable.Priority = StringHelper.ConvertToInt(this.Priority.Text);
                    model.OrderTargetName = this.checkBoxList1.Items[i].Text;
                    model.StoreName = this.checkBoxList2.Items[j].Text;

                    if (controller.Exists(model))
                    {
                        //ShowWarning("Exist the same coupon replenish rule!");
                        continue;
                    }
                    controller.Add(model);
                }
            }
            CloseAndPostBack();
        }

        protected void cbSelectAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            for (int i = 0; i < checkBoxList2.Items.Count; i++)
            {
                if (cbSelectAll.Checked)
                { checkBoxList2.Items[i].Selected = true; }
                else
                { checkBoxList2.Items[i].Selected = false; }
            }
        }
    }
}