﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="show.aspx.cs" Inherits="Edge.Web.SysManage.AirloadMonitoringRule.show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="InventoryReplenishCode" runat="server" Label="规则编号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Description" runat="server" Label="描述：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StartDate" runat="server" Label="开始日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="EndDate" runat="server" Label="结束日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StatusName" runat="server" Label="状态：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="卡信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Brand" runat="server" Label="品牌：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="CardType" runat="server" Label="卡类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别：" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="CardGrade" runat="server" Label="卡级别：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="时间设置信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="DayFlagID" runat="server" Label="有效天数设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblDayFlagCode" runat="server" Label="有效天数设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="WeekFlagID" runat="server" Label="有效周设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblWeekFlagCode" runat="server" Label="有效周设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="MonthFlagID" runat="server" Label="有效月设置：" Resizable="true" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblMonthFlagCode" runat="server" Label="有效月设置：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="补货地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StoreType" runat="server" Label="地点类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="总部列表">
                <Items>
                    <ext:Grid ID="InventoryReplenishRuleList" ShowBorder="true" ShowHeader="true" Title="自动绑定列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true">
                        <Columns>
                            <ext:BoundField ColumnID="Priority" DataField="MainTable.Priority" HeaderText="优先级编号" />
                            <ext:BoundField ColumnID="StoreName" DataField="StoreName" HeaderText="库存(入)地点" />
                            <ext:BoundField ColumnID="OrderTargetName" DataField="OrderTargetName" HeaderText="库存(出)地点" />
                            <ext:BoundField ColumnID="MinAmtBalance" DataField="MainTable.MinAmtBalance" HeaderText="最低库存"  Hidden=true/>
                            <ext:BoundField ColumnID="RunningAmtBalance" DataField="MainTable.RunningAmtBalance" HeaderText="常规库存" Hidden=true />
                            <ext:TemplateField Width="100px" HeaderText="最低库存">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("MainTable.MinAmtBalance","{0:F2}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="常规库存">
                                <ItemTemplate>
                                    <asp:Label ID="lblStore" runat="server" Text='<%#Eval("MainTable.RunningAmtBalance","{0:F2}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

