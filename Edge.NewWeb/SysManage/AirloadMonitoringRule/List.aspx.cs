﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Controllers;
using System.Data;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.SysManage.AirloadMonitoringRule
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RptBind("");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                ControlTool.BindBrand(this.Brand);
               // ControlTool.BindCouponType(CouponType, -1);
                ControlTool.BindCardType(CardType, string.Format("BrandID = {0}  order by CardTypeCode", -1));
                ControlTool.BindCardGrade(CardGrade, -1);
            }
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.Brand.SelectedValue, out brandID) ? brandID : 0;
            //ControlTool.BindCouponType(CouponType, string.Format("BrandID = {0}  order by CouponTypeCode", brandID));
            ControlTool.BindCardType(CardType, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGrade, Convert.ToInt32(CardType.SelectedValue));
        }

        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string InventoryReplenishCode = this.InventoryReplenishCode.Text.Trim();
                    string description = this.Description.Text.Trim();
                    string brandID = this.Brand.SelectedValue.Trim();
                    string cardType = this.CardType.SelectedValue.Trim();
                    string cardGrade = this.CardGrade.SelectedValue.Trim();
                    string storeTypeID = this.StoreType.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(InventoryReplenishCode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" InventoryReplenishCode like '%");
                        sb.Append(InventoryReplenishCode);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(description))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Description";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(description);
                        sb.Append("%'");
                    }
                    if (brandID != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("BrandID = ");
                        sb.Append(brandID);
                    }
                    if (cardType != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CardTypeID = ");
                        sb.Append(cardType);
                    }
                    if (cardGrade != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CardGradeID = ");
                        sb.Append(cardGrade);
                    }
                    if (!string.IsNullOrEmpty(storeTypeID))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("StoreTypeID = ");
                        sb.Append(storeTypeID);
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                AirloadMonitoringRuleController controller = new AirloadMonitoringRuleController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("InventoryReplenishRule", "Load Field", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            AirloadMonitoringRuleController c = new AirloadMonitoringRuleController();
            int count = 0;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                if (Tools.DALTool.Delete<Edge.SVA.BLL.InventoryReplenishRule_H>(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString())) 
                {
                    Edge.SVA.BLL.InventoryReplenishRule_D bll = new SVA.BLL.InventoryReplenishRule_D();
                    bll.DeleteByInventoryReplenishCode(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString());      
                }
            }
            this.RefreshPage();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("");
        }

        protected void Grid1_RowCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Action1")
            {
                RunAutoGenOrder_New(Grid1.DataKeys[e.RowIndex][0].ToString());
            }
        }

        private void RunAutoGenOrder_New(string InventoryReplenishCode) 
        {
            AirloadMonitoringRuleController controller = new AirloadMonitoringRuleController();
            controller.RunAutoGenOrder_New(InventoryReplenishCode);
        }

        protected void btnExecute_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            RunAutoGenOrder_New(sb.ToString());
        }
    }
}