﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="Edge.Web.SysManage.AirloadMonitoringRule.add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2,sform4" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本设定"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="InventoryReplenishCode" runat="server" Label="规则编号：" Required="true" ShowRedStar="true">
                                    </ext:TextBox>
                                     <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Description" runat="server" Label="描述：" Required="true" ShowRedStar="true">
                                    </ext:TextBox>
                                     <ext:Label ID="Label2" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="开始日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label3" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                                        ToolTipTitle="结束日期" ToolTip="日期格式：YYYY-MM-DD"  Required="true" ShowRedStar="true">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label4" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="Status" runat="server" Label="状态：" Required="true" ShowRedStar="true">
                                        <ext:RadioItem Text="生效" Value="1" Selected="true" />
                                        <ext:RadioItem Text="关闭" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="卡信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" 
                                        OnSelectedIndexChanged="BrandID_SelectedIndexChanged" AutoPostBack="true" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Label6" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型："
                                    OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" AutoPostBack="true" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Label10" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别："
                                    OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged" AutoPostBack="true" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="Label11" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="时间设置信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="DayFlagID" runat="server" Label="有效天数设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblDayFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="WeekFlagID" runat="server" Label="有效周设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblWeekFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="MonthFlagID" runat="server" Label="有效月设置：" Resizable="true" Required="true" ShowRedStar="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="lblMonthFlagCode" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="补货地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="StoreTypeID" runat="server" Label="地点类型：" Required="true" ShowRedStar="true" AutoPostBack="true" OnSelectedIndexChanged="StoreTypeID_SelectedIndexChanged">
                                        <ext:RadioItem Text="总部" Value="1" Selected="true" />
                                        <ext:RadioItem Text="店铺" Value="2" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label8" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="总部列表">
                <Items>
                    <ext:Grid ID="InventoryReplenishRuleList" ShowBorder="true" ShowHeader="true" Title="自动绑定列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="InventoryReplenishRuleList_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew1" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                    <ext:Button ID="btnClear" Text="清空" Icon="Delete" runat="server" OnClick="btnClear_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="Priority" DataField="MainTable.Priority" HeaderText="优先级编号" />
                            <ext:BoundField ColumnID="StoreName" DataField="StoreName" HeaderText="库存(入)地点" />
                            <ext:BoundField ColumnID="OrderTargetName" DataField="OrderTargetName" HeaderText="库存(出)地点" />
                            <ext:BoundField ColumnID="MinAmtBalance" DataField="MainTable.MinAmtBalance" HeaderText="最低库存" />
                            <ext:BoundField ColumnID="RunningAmtBalance" DataField="MainTable.RunningAmtBalance" HeaderText="常规库存" />
                            <ext:BoundField ColumnID="StoreID" DataField="MainTable.StoreID" HeaderText="" Hidden="true" />
                            <ext:BoundField ColumnID="OrderTargetID" DataField="MainTable.OrderTargetID" HeaderText="" Hidden="true" />
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window1" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataIFrameUrlFields="MainTable.OrderTargetID,MainTable.StoreID"
                                DataIFrameUrlFormatString="modifyDetail.aspx?OrderTargetID={0}&StoreID={1}"  Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>


