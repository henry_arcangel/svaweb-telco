﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.AirloadMonitoringRule
{
    public partial class show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_H, Edge.SVA.Model.InventoryReplenishRule_H>
    {
        AirloadMonitoringRuleController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.InventoryReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                controller = new AirloadMonitoringRuleController();
                Edge.SVA.Model.InventoryReplenishRule_H model = controller.GetModel(id);
                InventoryReplenishCode.Text = model.InventoryReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                if (model.Status == 1)
                {
                    StatusName.Text = "生效";
                }
                else 
                {
                    StatusName.Text = "关闭";
                }
                BrandID.SelectedValue = model.BrandID.ToString();
                Brand.Text = BrandID.SelectedText;
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", (int)model.BrandID));
                CardTypeID.SelectedValue = model.CardTypeID.ToString();
                CardType.Text = CardTypeID.SelectedText;
                ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
                CardGradeID.SelectedValue = model.CardGradeID.ToString();
                CardGrade.Text = CardGradeID.SelectedText;
                ControlTool.BindDayFlagID(DayFlagID);
                DayFlagID.SelectedValue = model.DayFlagID.ToString();
                lblDayFlagCode.Text = DayFlagID.SelectedText;
                ControlTool.BindWeekFlagID(WeekFlagID);
                WeekFlagID.SelectedValue = model.WeekFlagID.ToString();
                lblWeekFlagCode.Text = WeekFlagID.SelectedText;
                ControlTool.BindMonthFlagID(MonthFlagID);
                MonthFlagID.SelectedValue = model.MonthFlagID.ToString();
                this.lblMonthFlagCode.Text = MonthFlagID.SelectedText;
                CardType.Hidden = false;
                CardGrade.Hidden = false;                

                if (model.StoreTypeID == 1)
                {
                    StoreType.Text = "总部";
                    GroupPanel6.Title = "总部列表";
                }
                else
                {
                    StoreType.Text = "店铺";
                    GroupPanel6.Title = "店铺列表";
                }
                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.AirloadMonitoringRuleController = controller;

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            this.InventoryReplenishRuleList.RecordCount = controller.ViewModel.InventoryReplenishRuleViewModelList.Count;
            this.InventoryReplenishRuleList.DataSource = controller.ViewModel.InventoryReplenishRuleViewModelList;
            this.InventoryReplenishRuleList.DataBind();
            this.InventoryReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

	protected void CouponReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            InventoryReplenishRuleList.PageIndex = e.NewPageIndex;

            BindingDataGrid();

        }


        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("InventoryReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinAmtBalance", typeof(decimal));
                    dt.Columns.Add("RunningAmtBalance", typeof(decimal));
                    dt.Columns.Add("ReplenishType", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

    }
}