﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using FineUI;

namespace Edge.Web.SysManage.AirloadMonitoringRule
{
    public partial class modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_H, Edge.SVA.Model.InventoryReplenishRule_H>
    {
        AirloadMonitoringRuleController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.InventoryReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                btnNew1.OnClientClick = "";

                controller = new AirloadMonitoringRuleController();
                Edge.SVA.Model.InventoryReplenishRule_H model = controller.GetModel(id);
                InventoryReplenishCode.Text = model.InventoryReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                Status.SelectedValue = model.Status.ToString();
                BrandID.SelectedValue = model.BrandID.ToString();
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", (int)model.BrandID));
                CardTypeID.SelectedValue = model.CardTypeID.ToString();
                ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
                CardGradeID.SelectedValue = model.CardGradeID.ToString();
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);

                string TypeOrGradeID = "";
                CardTypeID.Hidden = false;
                CardGradeID.Hidden = false;
                TypeOrGradeID = CardGradeID.SelectedItem.Value;

                StoreTypeID.SelectedValue = model.StoreTypeID.ToString();
                if (model.StoreTypeID == 1)
                {
                    GroupPanel6.Title = "总部列表";
                    string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2";
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }
                else 
                {
                    GroupPanel6.Title = "店铺列表";
                    string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2" ;
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }

                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.AirloadMonitoringRuleController = controller;

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
        
         
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            this.InventoryReplenishRuleList.RecordCount = controller.ViewModel.InventoryReplenishRuleViewModelList.Count;
            this.InventoryReplenishRuleList.DataSource = controller.ViewModel.InventoryReplenishRuleViewModelList;
            this.InventoryReplenishRuleList.DataBind();
         this.InventoryReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        { 
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            
                                 
            List<InventoryReplenishRuleViewModel> modelList = new List<InventoryReplenishRuleViewModel>();
           // var selectedrow = InventoryReplenishRuleList.Rows[InventoryReplenishRuleList.SelectedRowIndex].DataKeys[0].ToString();
           // FineUI.Alert.Show(string.Format("   ID  recording  {0}  original article  ", selectedrow), FineUI.MessageBoxIcon.Warning);

            //foreach (FineUI.Grid item in this.InventoryReplenishRuleList.RecordCount)
            //{ 
            //    if (item.Rows[0].Values.ToString() == "1") {

            //        string strin = item.Rows[0].Values.ToString();

            //        //foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
            //        //{
            //        //    if (this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[7] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[8] == model.MainTable.OrderTargetID.ToString())
            //        //    {
            //        //        modelList.Add(model);
            //        //        break;
            //        //    }
            //        //}
            //    }
            
            //}





            //int index = 0;
            //if (this.InventoryReplenishRuleList.PageIndex == 0)
            //{
            //    index = 0;
            //}
            //else {

            //    index =1 + (10 * (this.InventoryReplenishRuleList.PageIndex)   );
            //    //10* 1 = 10, 10*2 =20
            //}
            int rows;

            int index = 0;
            if (this.InventoryReplenishRuleList.PageIndex == 0)
            {
                index = 0;
            }
            else
            {

                index =  (10 * (this.InventoryReplenishRuleList.PageIndex));

            } 
            foreach (int row in InventoryReplenishRuleList.SelectedRowIndexArray)
            {


             
                foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
                {
                 
                    string data = (this.InventoryReplenishRuleList.Rows[row].Values[7].ToString());
                    string data2 = (this.InventoryReplenishRuleList.Rows[row].Values[8].ToString());
                    string dat3a = (this.InventoryReplenishRuleList.Rows[row].Values[9].ToString());
              
                    string data5 = (this.InventoryReplenishRuleList.Rows[row].Values[6].ToString());

                    rows = row + index;
                    if (this.InventoryReplenishRuleList.Rows[rows].Values[7] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[rows].Values[8] == model.MainTable.OrderTargetID.ToString())
                    {
                        modelList.Add(model);
                        break;
                    }
                }
            }
              
          



            ////for (int i = 0 ; i < rowIndexs.Length + 0  ; i++)
            ////{   
            ////    foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
            ////    {

            ////        FineUI.Alert.Show(rowIndexs[i].ToString());
            ////   //    if (this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[7] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[8] == model.MainTable.OrderTargetID.ToString())
            ////   if (this.InventoryReplenishRuleList.Rows[i].Values[7] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[i].Values[8] == model.MainTable.OrderTargetID.ToString())
                  
            ////        {
            ////            modelList.Add(model);
            ////            break;
            ////        }
            ////    }
            ////}

            foreach (InventoryReplenishRuleViewModel model in modelList)
            {
             ///// removed temporary by jay
               controller.ViewModel.InventoryReplenishRuleViewModelList.Remove(model);
            }
            SVASessionInfo.AirloadMonitoringRuleController = controller;
            this.InventoryReplenishRuleList.SelectedRowIndexArray = new int[0];
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            SVASessionInfo.AirloadMonitoringRuleController = null;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            BindingDataGrid();
        }

        protected void InventoryReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            InventoryReplenishRuleList.PageIndex = e.NewPageIndex  ;
            
            BindingDataGrid();

        }

        protected void StoreTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            SVASessionInfo.AirloadMonitoringRuleController = null;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            BindingDataGrid();
            string TypeOrGradeID = "";
            TypeOrGradeID = CardGradeID.SelectedItem.Value;           
            if (StoreTypeID.SelectedValue == "1")
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.BrandID.SelectedValue == "-1")
            {
                ShowWarning("请选择品牌！");
                return;
            }

            if (this.CardGradeID.SelectedValue == "-1")
            {
                //todo
                ShowWarning(Resources.MessageTips.CouponCodeNotNull);
                return;
            }

            Edge.SVA.Model.InventoryReplenishRule_H item = this.GetUpdateObject();
            
            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish  {0} No Data", InventoryReplenishCode.Text));
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            if (this.controller.ViewModel.InventoryReplenishRuleViewModelList.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish Form  {0} Detail No Data", InventoryReplenishCode.Text));
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.InventoryReplenishRule_H>(item))
            {
                Edge.SVA.BLL.InventoryReplenishRule_D bll = new SVA.BLL.InventoryReplenishRule_D();
                bll.DeleteByInventoryReplenishCode(this.InventoryReplenishCode.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("InventoryReplenishRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (InventoryReplenishRuleViewModel model in this.controller.ViewModel.InventoryReplenishRuleViewModelList)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["InventoryReplenishCode"] = item.InventoryReplenishCode;
                        row["StoreID"] = model.MainTable.StoreID;
                        row["OrderTargetID"] = model.MainTable.OrderTargetID;
                        row["MinAmtBalance"] = model.MainTable.MinAmtBalance;
                        row["RunningAmtBalance"] = model.MainTable.RunningAmtBalance;
                        row["Priority"] = model.MainTable.Priority;
                        row["OrderRoundUpQty"] = 1;
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "InventoryReplenishRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Success But Detail Failed", InventoryReplenishCode.Text), ex);
                    ShowUpdateFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Success", item.InventoryReplenishCode));
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Update Failed", item.InventoryReplenishCode));
                ShowUpdateFailed();
            }
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("InventoryReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinAmtBalance", typeof(decimal));
                    dt.Columns.Add("RunningAmtBalance", typeof(decimal));
                    dt.Columns.Add("ReplenishType", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        protected override SVA.Model.InventoryReplenishRule_H GetPageObject(SVA.Model.InventoryReplenishRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreTypeID.SelectedValue == "1")
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }
    }
}