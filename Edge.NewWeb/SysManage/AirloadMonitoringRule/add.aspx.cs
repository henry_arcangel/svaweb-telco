﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.AirloadMonitoringRule
{
    public partial class add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_H, Edge.SVA.Model.InventoryReplenishRule_H>
    {
        AirloadMonitoringRuleController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.InventoryReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", -1));
                ControlTool.BindCardGrade(CardGradeID, -1);
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);

                RegisterCloseEvent(btnClose);
                btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1&MediaType=1", "Add");

                SVASessionInfo.AirloadMonitoringRuleController = null;

                
            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            this.InventoryReplenishRuleList.RecordCount = controller.ViewModel.InventoryReplenishRuleViewModelList.Count;
            this.InventoryReplenishRuleList.DataSource = controller.ViewModel.InventoryReplenishRuleViewModelList;
            this.InventoryReplenishRuleList.DataBind();
            this.InventoryReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e) 
        {
            int[] rowIndexs = this.InventoryReplenishRuleList.SelectedRowIndexArray;
            List<InventoryReplenishRuleViewModel> modelList = new List<InventoryReplenishRuleViewModel>();
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList) 
                {
                    if (this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[6] == model.MainTable.StoreID.ToString() && this.InventoryReplenishRuleList.Rows[rowIndexs[i]].Values[7] == model.MainTable.OrderTargetID.ToString()) 
                    {
                        modelList.Add(model);
                        break;
                    }
                }
            }

            foreach (InventoryReplenishRuleViewModel model in modelList)
            {
                controller.ViewModel.InventoryReplenishRuleViewModelList.Remove(model);
            }
            SVASessionInfo.AirloadMonitoringRuleController = controller;
            this.InventoryReplenishRuleList.SelectedRowIndexArray = new int[0];
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e) 
        {
            SVASessionInfo.AirloadMonitoringRuleController = null;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            BindingDataGrid();
        }

        protected void InventoryReplenishRuleList_OnPageIndexChange(object sender, EventArgs e) 
        {
        
        }

        protected void StoreTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            SVASessionInfo.AirloadMonitoringRuleController = null;
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            BindingDataGrid();
            string TypeOrGradeID = "";
            TypeOrGradeID = CardGradeID.SelectedItem.Value;
            if (StoreTypeID.SelectedValue == "1")
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
            else 
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + TypeOrGradeID + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.BrandID.SelectedValue == "-1")
            {
                ShowWarning(Resources.MessageTips.BrandCodeEmpty);
                return;
            }

            if (this.CardGradeID.SelectedValue == "-1")
            {
                //todo
                ShowWarning(Resources.MessageTips.CouponCodeNotNull);
                return;
            }

            Edge.SVA.Model.InventoryReplenishRule_H item = this.GetAddObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish  {0} No Data", InventoryReplenishCode.Text));
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            controller = SVASessionInfo.AirloadMonitoringRuleController;
            if (this.controller.ViewModel.InventoryReplenishRuleViewModelList.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenish Form  {0} Detail No Data", InventoryReplenishCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            item.MediaType = 2;
            item.PurchaseType = 2;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;
            item.CreatedOn = DateTime.Now;

            if (Tools.DALTool.isHasInventoryReplenishRuleCode(InventoryReplenishCode.Text)) 
            {
                ShowWarning(Resources.MessageTips.ExistReplenishRule + InventoryReplenishCode.Text);
                return;
            }

            if (Tools.DALTool.Add<Edge.SVA.BLL.InventoryReplenishRule_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("InventoryReplenishRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (InventoryReplenishRuleViewModel model in this.controller.ViewModel.InventoryReplenishRuleViewModelList)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["InventoryReplenishCode"] = item.InventoryReplenishCode;
                        row["StoreID"] = model.MainTable.StoreID;
                        row["OrderTargetID"] = model.MainTable.OrderTargetID;
                        row["MinAmtBalance"] = model.MainTable.MinAmtBalance;
                        row["RunningAmtBalance"] = model.MainTable.RunningAmtBalance;
                        row["Priority"] = model.MainTable.Priority;
                        row["ReplenishType"] = 2;
                        row["OrderRoundUpQty"] = 1;
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "InventoryReplenishRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("InventoryReplenishRule  {0} Add Success But Detail Failed", InventoryReplenishCode.Text), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Add Success", item.InventoryReplenishCode));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("InventoryReplenishRule  {0} Add Failed", item.InventoryReplenishCode));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("InventoryReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinAmtBalance", typeof(decimal));
                    dt.Columns.Add("RunningAmtBalance", typeof(decimal));
                    dt.Columns.Add("ReplenishType", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        protected override SVA.Model.InventoryReplenishRule_H GetPageObject(SVA.Model.InventoryReplenishRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreTypeID.SelectedValue == "1")
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=1&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&TypeOrGradeID=" + this.CardGradeID.SelectedItem.Value + "&MediaType=2";
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }
    }
}