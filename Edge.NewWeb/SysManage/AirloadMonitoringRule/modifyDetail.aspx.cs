﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using System.Data;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;

namespace Edge.Web.SysManage.AirloadMonitoringRule
{
    public partial class modifyDetail : Edge.Web.Tools.BasePage<Edge.SVA.BLL.InventoryReplenishRule_D, Edge.SVA.Model.InventoryReplenishRule_D>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["OrderTargetID"] == null || Request["StoreID"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    string orderTargetID = Request["OrderTargetID"].ToString();
                    string storeID = Request["StoreID"].ToString();
                    AirloadMonitoringRuleController controller = SVASessionInfo.AirloadMonitoringRuleController;
                    foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
                    {
                        if (storeID == model.MainTable.StoreID.ToString() && orderTargetID == model.MainTable.OrderTargetID.ToString())
                        {
                            this.OrderTargetID.Items.Add(model.OrderTargetName, orderTargetID);
                            this.OrderTargetID.SelectedIndex = 0;
                            this.StoreID.Items.Add(model.StoreName, storeID);
                            this.StoreID.SelectedIndex = 0;
                            this.Priority.Text = model.MainTable.Priority.ToString();
                            this.MinAmtBalance.Text = Convert.ToDecimal(model.MainTable.MinAmtBalance.ToString()).ToString("F2");
                            this.RunningAmtBalance.Text = Convert.ToDecimal(model.MainTable.RunningAmtBalance.ToString()).ToString("F2");
                            break;
                        }
                    }
                }

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            AirloadMonitoringRuleController controller = SVASessionInfo.AirloadMonitoringRuleController;

            foreach (InventoryReplenishRuleViewModel model in controller.ViewModel.InventoryReplenishRuleViewModelList)
            {
                if (this.StoreID.SelectedValue == model.MainTable.StoreID.ToString() && this.OrderTargetID.SelectedValue == model.MainTable.OrderTargetID.ToString())
                {
                    model.MainTable.MinAmtBalance = Convert.ToDecimal(this.MinAmtBalance.Text);
                    model.MainTable.RunningAmtBalance = Convert.ToDecimal(this.RunningAmtBalance.Text);
                    model.MainTable.Priority = StringHelper.ConvertToInt(this.Priority.Text);
                    break;
                }
            }

            CloseAndPostBack();
        }
    }
}