﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Edge.Security.Model;
using System.IO;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.SysManage
{
	/// <summary>
	/// TreeAdd 的摘要说明。
	/// </summary>
    public partial class add : PageBase
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				//得到现有菜单
				BindTree();	           
				//得到所有权限
                BindClassTree();
				//BindImages();		
                btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();
			}
			
		}


        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {
            string orderid = Edge.Common.PageValidate.InputText(txtOrderId.Text, 10);
            string name = txtName.Text;
            string url = Edge.Common.PageValidate.InputText(txtUrl.Text, 100);
            //string imgUrl=Edge.Common.PageValidate.InputText(txtImgUrl.Text,100);
            //string imgUrl = this.ddlImgUrl.SelectedValue;

            string target = this.ddlParent.SelectedValue;
            int parentid = int.Parse(target);

            if (orderid.Trim() == "")
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NumberNotEmpty,FineUI.MessageBoxIcon.Warning);
                return;
            }
            try
            {
                int.Parse(orderid);
            }
            catch
            {

                FineUI.Alert.ShowInTop(Resources.MessageTips.NumberFormatError, FineUI.MessageBoxIcon.Warning);
                return;
            }

            if (name.Trim() == "")
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NameNotEmpty, FineUI.MessageBoxIcon.Warning);
                return;
            }

            int permission_id = -1;
            if (!string.IsNullOrEmpty(this.ddlPermissionList.SelectedValue))
            {
                permission_id = int.Parse(this.ddlPermissionList.SelectedValue);
            }
            int moduleid = -1;
            int keshidm = -1;
            string keshipublic = this.ckbKeshi.Checked ? "true" : "false";
            string comment = Edge.Common.PageValidate.InputText(txtDescription.Text, 100);

            SysNode node = new SysNode();
            node.Text = name;
            node.ParentID = parentid;
            node.Location = parentid + "." + orderid;
            node.OrderID = int.Parse(orderid);
            node.Comment = comment;
            node.Url = url;
            node.PermissionID = permission_id;
            node.ImageUrl = "";
            node.ModuleID = moduleid;
            node.KeShiDM = keshidm;
            node.KeshiPublic = keshipublic;
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();		
            sm.AddTreeNode(node);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Tree Menu : " + node.Text);

            PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
        }

        #region Event
		
        private void BindTree()
        {
            Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();			
            DataTable dt=sm.GetTreeList("").Tables[0];
            this.ddlParent.Items.Clear();
            //加载树
            this.ddlParent.Items.Add(new FineUI.ListItem("#", "0"));
            DataRow [] drs = dt.Select("ParentID= " + 0);	
	
            foreach( DataRow r in drs )
            {
                string nodeid=r["NodeID"].ToString();				
                string text=r["Text"].ToString();					
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text="╋"+text;
                this.ddlParent.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid=int.Parse(nodeid);
                string blank="├";
				
                BindNode( sonparentid, dt,blank);

            }	
            this.ddlParent.DataBind();			

        }

        private void BindNode(int parentid,DataTable dt,string blank)
        {
            DataRow [] drs = dt.Select("ParentID= " + parentid );
			
            foreach( DataRow r in drs )
            {
                string nodeid=r["NodeID"].ToString();				
                string text=r["Text"].ToString();					
                //string permissionid=r["PermissionID"].ToString();
                text=blank+"『"+text+"』";

                this.ddlParent.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid=int.Parse(nodeid);
                string blank2=blank+"─";
			
                BindNode( sonparentid, dt,blank2);
            }
        }

        //private void BindImages()
        //{
        //    string dirpath=Server.MapPath("../Images/MenuImg");
        //    DirectoryInfo di = new DirectoryInfo(dirpath);  
        //    FileInfo[] rgFiles = di.GetFiles("*.gif");  
        //    this.ddlImgUrl.Items.Clear();
        //    foreach(FileInfo fi in rgFiles) 
        //    {
        //        FineUI.ListItem item = new FineUI.ListItem(fi.Name, "Images/MenuImg/" + fi.Name);
        //        this.ddlImgUrl.Items.Add(item);
        //    }
        //    FileInfo[] rgFiles2 = di.GetFiles("*.jpg"); 		
        //    foreach(FileInfo fi in rgFiles2) 
        //    {
        //        FineUI.ListItem item = new FineUI.ListItem(fi.Name, "Images/MenuImg/" + fi.Name);
        //        this.ddlImgUrl.Items.Add(item);
        //    }
        //    this.ddlImgUrl.Items.Insert(0, new FineUI.ListItem("Default", ""));
        //    this.ddlImgUrl.DataBind();
        //}

        private void BindClassTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(SVASessionInfo.SiteLanguage.ToString());


            this.ddlCategoryList.Items.Clear();
            //加载树
            this.ddlCategoryList.Items.Add(new FineUI.ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlCategoryList.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindClassNode(sonparentid, ds.Tables[0], blank);

            }
            this.ddlCategoryList.DataBind();

        }

        private void BindClassNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.ddlCategoryList.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindClassNode(sonparentid, dt, blank2);
            }
        }


        protected void ddlCategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PermissionsDatabind();
        }

        private void PermissionsDatabind()
        {
            int CategoryId = int.Parse(this.ddlCategoryList.SelectedValue);
            DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, SVASessionInfo.SiteLanguage.ToString());//todo: 修改成多语言。
            this.ddlPermissionList.DataSource = PermissionsList;
            this.ddlPermissionList.DataTextField = "Description";
            this.ddlPermissionList.DataValueField = "PermissionID";
            this.ddlPermissionList.DataBind();
        }

        #endregion
    }
}
