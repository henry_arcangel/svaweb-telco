﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.SysManage.CouponAutoPickingRule
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponAutoPickingRule_H, Edge.SVA.Model.CouponAutoPickingRule_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                RegisterCloseEvent(btnClose);
                ControlTool.BindBrand(BrandID);
                ControlTool.BindStore(StoreID,"2");
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                CouponAutoPickingRuleCode.Text = Model.CouponAutoPickingRuleCode;

                CouponAutoPickingRuleController controller = new CouponAutoPickingRuleController();

                DataTable dt = controller.GetDetailList(Model.CouponAutoPickingRuleCode);
                Session["CouponAutoPickingRule_D"] = dt;
            }

            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            this.Grid1.RecordCount = dt.Rows.Count;
            this.Grid1.DataSource = dt;
            this.Grid1.DataBind();
            this.Grid1.SelectedRowIndexArray = rowIndexs;
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["BrandID"] != System.DBNull.Value)
                    {
                        if (this.Grid1.Rows[rowIndexs[i]].Values[0] == dr["BrandCode"].ToString())
                        {
                            dt.Rows.Remove(dr);
                            break;
                        }
                    }
                    else
                    {
                        if (this.Grid1.Rows[rowIndexs[i]].Values[2] == dr["StoreCode"].ToString())
                        {
                            dt.Rows.Remove(dr);
                            break;
                        }
                    }
                }
            }

            this.Grid1.SelectedRowIndexArray = new int[0];
            Session["CouponAutoPickingRule_D"] = dt;
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            dt.Rows.Clear();
            Session["CouponAutoPickingRule_D"] = dt;
            BindingDataGrid();
        }

        protected void ListType_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkBoxList1.Items.Clear();
        }

        protected void SearchButton1_Click(object sender, EventArgs e)
        {
            if (ListType.SelectedValue == "1")
            {
                RptBind1(" order by BrandID ");
            }
            else
            {
                RptBind2(" order by StoreID ");
            }
        }

        private void RptBind1(string orderby)
        {
            this.checkBoxList1.Items.Clear();
            try
            {
                string descLan = DALTool.GetStringByCulture("BrandName1", "BrandName2", "BrandName3");
                string sql = " select BrandID," + descLan + " BrandName from Brand where 1=1 ";
                string brandid = BrandID.SelectedValue;
                string brandname = BrandName.Text;
                if (brandid != "-1")
                {
                    sql += " and BrandID =" + brandid;
                }
                if (!string.IsNullOrEmpty(brandname))
                {
                    sql += " and " + descLan + " like '%" + brandname + "%' ";
                }

                sql += orderby;

                DataSet ds = DBUtility.DbHelperSQL.Query(sql);
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        this.checkBoxList1.Items.Add(Convert.ToString(dr["BrandName"]), Convert.ToString(dr["BrandID"]));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void RptBind2(string orderby)
        {
            this.checkBoxList1.Items.Clear();
            try
            {
                string descLan = DALTool.GetStringByCulture("StoreName1", "StoreName2", "StoreName3");
                string sql = " select StoreID," + descLan + " StoreName from Store where 1=1 ";
                string storeid = StoreID.SelectedValue;
                string storename = StoreName.Text;
                if (storeid != "-1")
                {
                    sql += " and StoreID =" + storeid;
                }
                if (!string.IsNullOrEmpty(storename))
                {
                    sql += " and " + descLan + " like '%" + storename + "%' ";
                }

                sql += orderby;

                DataSet ds = DBUtility.DbHelperSQL.Query(sql);
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        this.checkBoxList1.Items.Add(Convert.ToString(dr["StoreName"]), Convert.ToString(dr["StoreID"]));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnNew1_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            int[] id = this.checkBoxList1.SelectedIndexArray;
            if (id.Length == 0)
            {
                ShowWarning(" Brand or Store must has value!");
                return;
            }
            foreach (int i in id)
            {
                if (ListType.SelectedValue == "1")
                {
                    if (dt.Select("BrandID = " + this.checkBoxList1.Items[i].Value).Length > 0)
                    {
                        continue;
                    }
                    else
                    {
                        DataRow dr = dt.NewRow();
                        dr["BrandID"] = this.checkBoxList1.Items[i].Value;
                        dr["BrandName"] = this.checkBoxList1.Items[i].Text;
                        dr["StoreID"] = DBNull.Value;
                        dr["StoreName"] = DBNull.Value;
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    if (dt.Select("StoreID = " + this.checkBoxList1.Items[i].Value).Length > 0)
                    {
                        continue;
                    }
                    else
                    {
                        DataRow dr = dt.NewRow();
                        dr["BrandID"] = DBNull.Value;
                        dr["BrandName"] = DBNull.Value;
                        dr["StoreID"] = this.checkBoxList1.Items[i].Value;
                        dr["StoreName"] = this.checkBoxList1.Items[i].Text;
                        dt.Rows.Add(dr);
                    }
                }

            }
            Session["CouponAutoPickingRule_D"] = dt;
            BindingDataGrid();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.CouponAutoPickingRule_H item = this.GetUpdateObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponAutoPickingRule  {0} No Data", this.CouponAutoPickingRuleCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            if (dt.Rows.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponAutoPickingRule Form  {0} Detail No Data", CouponAutoPickingRuleCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.CouponAutoPickingRule_H>(item))
            {
                Edge.SVA.BLL.CouponAutoPickingRule_D bll = new SVA.BLL.CouponAutoPickingRule_D();
                bll.DeleteByCode(CouponAutoPickingRuleCode.Text);

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("CouponAutoPickingRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (DataRow dr in dt.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CouponAutoPickingRuleCode"] = CouponAutoPickingRuleCode.Text;
                        if (dr["BrandID"] != DBNull.Value)
                        {
                            row["BrandID"] = Convert.ToInt32(dr["BrandID"]);
                            row["StoreID"] = DBNull.Value;
                        }
                        if (dr["StoreID"] != DBNull.Value)
                        {
                            row["BrandID"] = DBNull.Value;
                            row["StoreID"] = Convert.ToInt32(dr["StoreID"]);
                        }
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "CouponAutoPickingRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("CouponAutoPickingRule  {0} Update Success But Detail Failed", CouponAutoPickingRuleCode.Text), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponAutoPickingRule  {0} Update Success", item.CouponAutoPickingRuleCode));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponAutoPickingRule  {0} Update Failed", item.CouponAutoPickingRuleCode));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void Grid1_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindingDataGrid();
        }

        protected override SVA.Model.CouponAutoPickingRule_H GetPageObject(SVA.Model.CouponAutoPickingRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeID = 0;
            storeID = int.TryParse(this.StoreID.SelectedValue, out storeID) ? storeID : 0;
        }

        protected void cbSelectAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            for (int i = 0; i < checkBoxList1.Items.Count; i++)
            {
                if (cbSelectAll.Checked)
                { checkBoxList1.Items[i].Selected = true; }
                else
                { checkBoxList1.Items[i].Selected = false; }
            }
        }

    }
}