﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using Edge.Messages.Manager;

namespace Edge.Web.Controllers
{
    public class CardController
    {
        public static string ApproveCardForApproveCode(Edge.SVA.Model.Ord_CardBatchCreate model,out bool isSuccess)
        {
            isSuccess = false;

            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            string msg = CanApprove(model,out isSuccess);

            if (!isSuccess) return msg;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardBatchCreate().GetModel(model.CardCreateNumber);
                //need to ask 
                return model.ApprovalCode;
            }
            return "";
        }

        public static class CardRefnoCode
        {
            //todo:need to ask
            public static string OrderBatchCreationOfCard = "CAC";

            //add by Len
            public static string OrderCardChangeDenomination = "COAD";

            //add by Nathan 20140609
            public static string OrderCardIssue = "CAI";

            //add by Nathan 20140609
            public static string OrderBatchImportCards = "BICA";


            //add by Nathan 20140611
            public static string OrderCardActive = "CAA";

            //add by Alex 20140612
            public static string OrderCardVoid = "CAV";


            //add by Alex 20140612
            public static string OrderCardRedeem = "CAR";

            //add by Alex 20140703
            public static string OrderCardChangeStatus = "CCS";


            //add by Alex 20140712 TBC ++
            public static string OrderCardExpiryDate = "CED";
            public static string OrderCardCardTopup = "CD";
            //add by Alex 20140712 TBC -

            //add by Gavin
            public static string CardCreationAutomatic = "BCC";
            public static string OrderCardPickup = "CPO";
            public static string OrderCardDelivery = "CDO";
            public static string CardOrderForm = "COF";
            public static string CardReturnOrder = "RTH";
            //add by Gavin

            /// <summary>
            /// CTR
            /// </summary>
            public static string OrderCardTranfer = "CTR";
        }
        public static string CanApprove(Edge.SVA.Model.Ord_CardBatchCreate model, out bool isSusscess)
        {
            isSusscess = false;
            if (model == null) return "No Data";
            long remainCards = 0;
            string lastCreatedCard = null;
            string msg = null;
            Controllers.CardController.GetCreatedCardInfo(model.CardGradeID, 0, ref remainCards, ref lastCreatedCard, ref msg);

            if ((remainCards - model.CardCount) >= 0)
            {
                isSusscess = true;
                return null;
            }

            return Messages.Manager.MessagesTool.instance.GetMessage("90417");
        }

        public static string BatchVoidCard(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            //Modify by Nathan 20140611 ++
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.BatchCreate)
                {
                    Edge.SVA.Model.Ord_CardBatchCreate model = new Edge.SVA.BLL.Ord_CardBatchCreate().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                else if (type == ApproveType.ImportCard)
                {
                    Edge.SVA.Model.Ord_ImportCardUID_H model = new Edge.SVA.BLL.Ord_ImportCardUID_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                else if (type == ApproveType.CardAdjust)
                {
                    Edge.SVA.Model.Ord_CardAdjust_H model = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                //add by Gavin
                else if (type == ApproveType.CardCreationAutomatic)
                {
                    Edge.SVA.Model.Ord_OrderToSupplier_Card_H model = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                else if (type == ApproveType.CardReceive)
                {
                    Edge.SVA.Model.Ord_CardReceive_H model = new Edge.SVA.BLL.Ord_CardReceive_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                else if (type == ApproveType.CardReturn)
                {
                    Edge.SVA.Model.Ord_CardReturn_H model = new Edge.SVA.BLL.Ord_CardReturn_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                //add by Gavin
                else if (type == ApproveType.CardTransfer)
                {
                    Edge.SVA.Model.Ord_CardTransfer_H model = new Edge.SVA.BLL.Ord_CardTransfer_H().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
            }
            //Modify by Nathan 20140611 --

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
        //Add by Nathan 20140611 ++
        private static bool VoidCard(Edge.SVA.Model.Ord_ImportCardUID_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            model.ApprovalCode = null;


            Edge.SVA.BLL.Ord_ImportCardUID_H bll = new Edge.SVA.BLL.Ord_ImportCardUID_H();

            return bll.Update(model);
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_CardAdjust_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            model.ApprovalCode = null;


            Edge.SVA.BLL.Ord_CardAdjust_H bll = new Edge.SVA.BLL.Ord_CardAdjust_H();

            return bll.Update(model);
        }
        //Add by Nathan 20140611 --
        private static bool VoidCard(Edge.SVA.Model.Ord_CardBatchCreate model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            model.ApprovalCode = null;


            Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

            return bll.Update(model);
        }
        //add by frank
        private static bool VoidCard(Edge.SVA.Model.Ord_CardTransfer_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            model.ApprovalCode = null;


            Edge.SVA.BLL.Ord_CardTransfer_H bll = new Edge.SVA.BLL.Ord_CardTransfer_H();

            return bll.Update(model);
        }

        public enum ApproveType
        {
            BatchCreate,
            ImportCard,
            CardAdjust,
            CardCreationAutomatic,
            CardReceive,
            CardReturn,
            CardTransfer
        }

        public enum CardStatus
        {
            Dormant = 0,
            Issued = 1,
            Active = 2,
            Redeemed = 3,
            Expired = 4,
            Void = 5,
            Recycled = 6,// Add by Gavin
            Inactive=7, // Add by Alex 2014-07-23++
            Deactive = 8, //Added by Robin 2014-11-14 for 7-11用户主动要求停止账户
            Frozen=9 // Add by Alex 2014-07-22++
        }

        // Add by Gavin
        public enum CardStockStatus
        {
            ForPrinting = 1,
            GoodForRelease = 2,
            Damaged = 3,
            Picked = 4,
            InTransit = 5,
            InLocation = 6,
            Returned = 7
        }
        // Add by Gavin

        public enum OprID
        {
            //CardActive = 21,
            //CardVoid = 5,
            //CardChangeDenomination = 22,
            //CardAmountChange = 23
            CardActive = 21,
            CardVoid = 5,
            CardChangeDenomination = 22,
            CardAmountChange = 10,
            CardIssue = 28,//Add by Nathan 20140609
            CardRedeemed = 3,//Add by Nathan 20140610
            CardChangeStatus=29,//Add by Nathan 20140704
            CardChangeExpiryDate = 24, //Add by Alex 2014-07-12 
            CardTopup = 2, //Add by Alex 2014-07-12 
            TelcoCardAdj = 52,  //Add by Gavin 2015-02-27 
            TelcoMobileTopup = 50  //Add by Gavin 2015-02-27 
        }

        //public static int IsCanCreateCard(int cardGradeID, int createCount, ref string msg)
        //{
        //    string rtn = string.Empty;

        //    IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int,6) , 
        //                                    new SqlParameter("@TypeID", SqlDbType.Int,50),
        //                                    new SqlParameter("@GenQty", SqlDbType.Int,6), 
        //                                    new SqlParameter("@ReturnStartNumber", SqlDbType.VarChar,64),
        //                                    new SqlParameter("@ReturnEndNumber", SqlDbType.VarChar,64),
        //                                    new SqlParameter("@ReturnDuplicateNumber", SqlDbType.VarChar,64)
        //                                  };
        //    parameters[0].Value = 0;
        //    parameters[1].Value = cardGradeID;
        //    parameters[2].Value = createCount;
        //    parameters[3].Direction = ParameterDirection.Output;
        //    parameters[4].Direction = ParameterDirection.Output;
        //    parameters[5].Direction = ParameterDirection.Output;

        //    int count = 0;
        //    int result = DBUtility.DbHelperSQL.RunProcedure("CheckGenerateNumber", parameters, out count);
        //    if (parameters[5].Value != null)
        //    {
        //        msg = parameters[5].Value.ToString();
        //    }
        //    return result;

        //}

        //Add by Alex 2014-06-09 ++
        public static SVA.Model.CardType GetImportcardType(string cardTypeCode, Dictionary<string, SVA.Model.CardType> cache)
        {
            if (cache != null && cache.ContainsKey(cardTypeCode)) return cache[cardTypeCode];

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();

            SVA.Model.CardType cardType = bll.GetImportCardType(cardTypeCode);
            cache.Add(cardTypeCode, cardType);

            return cardType;
        }
        //Add by Alex 2014-06-09 --
        //Add by Nathan 201406010 ++
        public static string ApproveCardForApproveCode(Edge.SVA.Model.Ord_CardAdjust_H model, OprID oprID, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            Edge.SVA.BLL.Card cardBll = new Edge.SVA.BLL.Card();
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            //if (oprID == OprID.Expired)
            //{

            //    string[] list = webset.CouponExpiryDateStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            //    int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

            //    if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
            //    if (!couponBll.ValidCouponStauts(model, status))
            //    {
            //        return MessagesTool.instance.GetMessage("90186");
            //    }
            //}
            //else
            if (oprID == OprID.CardIssue)
            {
                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
                if (!cardBll.ValidCardStauts(model, (int)CardController.CardStatus.Dormant)) return MessagesTool.instance.GetMessage("90122");
            }
            else if (oprID == OprID.CardActive)
            {
                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
                if (!cardBll.ValidCardStauts(model, (int)CardController.CardStatus.Issued)) return MessagesTool.instance.GetMessage("90122");
            }
            else if (oprID == OprID.CardVoid)
            {
                string[] list = webset.CardVoidStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
                if (!cardBll.ValidCardStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90122");
                }
            }
            else if (oprID == OprID.CardRedeemed)
            {
                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
                if (!cardBll.ValidCardStauts(model, (int)CardController.CardStatus.Active)) return MessagesTool.instance.GetMessage("90122");
            }
            else if (oprID == OprID.CardChangeStatus)
            {
                string[] list = webset.CardStatusChangeStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90178");

                if (!cardBll.ValidCardStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90190");
                }
            }
            else if (oprID == OprID.CardChangeDenomination)
            {
                string[] list = webset.CardChangeDenominationEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");

                if (!cardBll.ValidCardStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90122");
                }
            }
            else if (oprID == OprID.CardTopup)
            {
                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
                if (!cardBll.ValidCardStauts(model, (int)CardController.CardStatus.Issued)) return MessagesTool.instance.GetMessage("90122");
            }
            else if (oprID == OprID.TelcoMobileTopup)
            {
                if (!cardBll.ExsitCard(model)) return MessagesTool.instance.GetMessage("90493");
            }

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardAdjust_H bll = new Edge.SVA.BLL.Ord_CardAdjust_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(model.CardAdjustNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        //Add by Nathan 201406010 --
        //Add by Alex 2014-06-11 ++
        public static string ApproveCardForApproveCode(Edge.SVA.Model.Ord_ImportCardUID_H model)
        {
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            if (!CanApprove(model)) return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90439");

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCardUID_H bll = new Edge.SVA.BLL.Ord_ImportCardUID_H();

            if (bll.Update(model, 3600))
            {
                model = new Edge.SVA.BLL.Ord_ImportCardUID_H().GetModel(model.ImportCardNumber);
                return model.ApprovalCode;
            }
            return "";
        }
        #region Can Approve
        public static bool CanApprove(Edge.SVA.Model.Ord_ImportCardUID_H model)
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
            if (bll.ExsitCard(model)) return false;
            return true;
        }
        #endregion

        public static string GetPreviousCardStatus(int currentStatus, string cardNumber)
        {
            switch (currentStatus)
            {
                case 0: return "";
                case 1: return DALTool.GetCardStatusName((int)CardStatus.Dormant);
                case 2: return DALTool.GetCardStatusName((int)CardStatus.Issued);
                case 3: return DALTool.GetCardStatusName((int)CardStatus.Active);
                case 4: return DALTool.GetCardStatusName(DALTool.GetPreviousCardStatus(currentStatus, cardNumber));
                case 5: return DALTool.GetCardStatusName(DALTool.GetPreviousCardStatus(currentStatus, cardNumber));
                case 7: return DALTool.GetCardStatusName((int)CardStatus.Inactive);//Update by Alex 2014-07-23 ++
                case 9: return DALTool.GetCardStatusName((int)CardStatus.Frozen);//Update by Alex 2014-07-15 ++
            }

            return "";
        }
        //Add by Alex 2014-06-11 --
        public static int GetCreatedCardInfo(int cardGradeID, int createCount, ref long remainCards, ref string lastCreatedCard, ref string msg)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int,6) , 
                                            new SqlParameter("@TypeID", SqlDbType.Int,50),
                                            new SqlParameter("@GenQty", SqlDbType.Int,6), 
                                            new SqlParameter("@ReturnStartNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnEndNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnDuplicateNumber", SqlDbType.VarChar,64),
                                             new SqlParameter("@RemainQty", SqlDbType.BigInt),
                                            new SqlParameter("@LastNumber", SqlDbType.VarChar,64)
                                          };
            parameters[0].Value = 0;
            parameters[1].Value = cardGradeID;
            parameters[2].Value = createCount;
            parameters[3].Direction = ParameterDirection.Output;
            parameters[4].Direction = ParameterDirection.Output;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;


            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("CheckGenerateNumber", parameters, out count);
            if (parameters[5].Value != null)
            {
                msg = parameters[5].Value.ToString();
            }
            if (parameters[6].Value != null)
            {
                remainCards = Edge.Web.Tools.ConvertTool.ToLong(parameters[6].Value.ToString());
            }
            if (parameters[7].Value != null)
            {
                lastCreatedCard = parameters[7].Value.ToString();
            }
            return result;

        }
        //add by Gavin
        public static string ApproveOrderToSupplier(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_OrderToSupplier_Card_H bll = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H().GetModel(model.OrderSupplierNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }

        public static string ApproveCardReceive(Edge.SVA.Model.Ord_CardReceive_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardReceive_H bll = new Edge.SVA.BLL.Ord_CardReceive_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardReceive_H().GetModel(model.CardReceiveNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }

        public static string ApproveCardReturn(Edge.SVA.Model.Ord_CardReturn_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardReturn_H bll = new Edge.SVA.BLL.Ord_CardReturn_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardReturn_H().GetModel(model.CardReturnNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_OrderToSupplier_Card_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_OrderToSupplier_Card_H bll = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H();

            return bll.Update(model);
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_CardReceive_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardReceive_H bll = new Edge.SVA.BLL.Ord_CardReceive_H();

            return bll.Update(model);
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_CardReturn_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardReturn_H bll = new Edge.SVA.BLL.Ord_CardReturn_H();

            return bll.Update(model);
        }
        //add by Gavin
        //add by frank++
        public static string ApproveCardForApproveCode(Edge.SVA.Model.Ord_CardTransfer_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            //if (!CanApprove(model)) return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90439");

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardTransfer_H bll = new Edge.SVA.BLL.Ord_CardTransfer_H();

            if (bll.Update(model, 3600))
            {
                isSuccess = true;
                model = new Edge.SVA.BLL.Ord_CardTransfer_H().GetModel(model.CardTransferNumber);
                return model.ApprovalCode;
            }
            return "";
        }
        //add by frank--
    }
}
