﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.TelcoVariant
{
    public class TelcoVariantController
{
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.ImportGM bll = new Edge.SVA.BLL.ImportGM();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "sku";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListViews(pageSize, pageIndex, strWhere, orderStr);

           // Tools.DataTool.addi(ds, "CustomerDesc", "CustomerID");
       //     DataSet  ds2;
     //     Edge.SVA.BLL.GrossMargin bll2 = new Edge.SVA.BLL.ImportGM();
    //  ds2 = bll2.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));
           // Tools.DataTool.AddCardTypeNameByID(ds, "CardTypeName1", "cardtypeid");
           // Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName1", "cardGradeID");
        
            
            return ds;
        }


        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr, int order)
        {
            DataSet ds;
            Edge.SVA.BLL.ImportGM bll = new Edge.SVA.BLL.ImportGM();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "sku";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListViews(pageSize, pageIndex, strWhere, orderStr, order);

            // Tools.DataTool.addi(ds, "CustomerDesc", "CustomerID");
            //     DataSet  ds2;
            //     Edge.SVA.BLL.GrossMargin bll2 = new Edge.SVA.BLL.ImportGM();
            //  ds2 = bll2.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));
            // Tools.DataTool.AddCardTypeNameByID(ds, "CardTypeName1", "cardtypeid");
            // Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName1", "cardGradeID");


            return ds;
        }

    }
}