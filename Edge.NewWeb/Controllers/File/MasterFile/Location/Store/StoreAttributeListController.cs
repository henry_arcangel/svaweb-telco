﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain;
using Edge.SVA.BLL.Domain.DataResources;
using System.Text;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model;
using Edge.SVA.BLL.Domain.ViewModelBLL;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public class StoreAttributeController
    {
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Store_Attribute bll = new Edge.SVA.BLL.Store_Attribute();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "SACode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            //Tools.DataTool.AddOrganizationName(ds, "OrganizationName", "OrganizationID");

            return ds;
        }

    }
}