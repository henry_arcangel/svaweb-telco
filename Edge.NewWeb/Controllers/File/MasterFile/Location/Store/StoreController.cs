﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using System.IO;
using Edge.Web.Controllers;
using Edge.Web.Tools;
using System.Data;
using System.Text.RegularExpressions;
using Edge.SVA.Model.Domain.Surpport;
using System.Data.SqlClient;
using System.Text;

namespace Edge.Web.Controllers.File.MasterFile.Location.Store
{
    public class StoreController
    {

        protected StoreViewModel viewModel = new StoreViewModel();

        public StoreViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int StoreID)
        {
            Edge.SVA.BLL.Store bll = new Edge.SVA.BLL.Store();
            Edge.SVA.Model.Store model = bll.GetModel(StoreID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Store bll = new SVA.BLL.Store();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Store bll = new SVA.BLL.Store();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Store bll = new Edge.SVA.BLL.Store();
            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "StoreCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            //ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);
            ds = bll.GetListByPage(strWhere,orderStr,pageIndex*pageSize+1,(pageIndex+1)*pageSize);

            Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
            Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");

            return ds;
        }

        protected bool IsExists(int StoreID)
        {
            Edge.SVA.BLL.Store bllmc = new SVA.BLL.Store();
            string strwhere = " StoreID =" + StoreID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string StoreCode, int BrandID, int StoreID)
        {
            string Errormessage = "";
            if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(StoreCode, BrandID, StoreID))
            {
                Errormessage = Resources.MessageTips.ExistStoreCodeInBrand;
            }
            return Errormessage;
        }

        #region 导入数据的处理
        public ImportModelList list = new ImportModelList();
        private DataTable adt = new DataTable();
        private DataTable udt = new DataTable();
        private DataTable ddt = new DataTable();
        private List<Edge.SVA.Model.Store> addmodellist = new List<SVA.Model.Store>();
        private List<Edge.SVA.Model.Store> updatemodellist = new List<SVA.Model.Store>();
        private List<Edge.SVA.Model.Store> delmodellist = new List<SVA.Model.Store>();
        private List<Edge.SVA.Model.Country> ctrymodellist = new List<SVA.Model.Country>();
        private List<Edge.SVA.Model.Province> prvmodellist = new List<SVA.Model.Province>();
        private List<Edge.SVA.Model.City> citymodellist = new List<SVA.Model.City>();
        private List<Edge.SVA.Model.District> dismodellist = new List<SVA.Model.District>();

        Edge.SVA.BLL.Store strbll = new SVA.BLL.Store();
        Edge.SVA.BLL.Brand brandbll = new SVA.BLL.Brand();
        Edge.SVA.BLL.StoreType storetypebll = new SVA.BLL.StoreType();
        Edge.SVA.BLL.StoreGroup storegroupbll = new SVA.BLL.StoreGroup();
        Edge.SVA.BLL.Bank bankbll = new SVA.BLL.Bank();
        public bool CheckData(DataTable dt)
        {
            //移除占用了格式的空行
            List<DataRow> drlist = new List<System.Data.DataRow>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Action"] == null || string.IsNullOrEmpty(dt.Rows[i]["Action"].ToString()))
                {
                    drlist.Add(dt.Rows[i]);
                }
            }
            foreach (var item in drlist)
            {
                dt.Rows.Remove(item);
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                List<KeyValue> storelist = new List<KeyValue>();
                this.list.Error.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    if (dr["Action"].ToString().ToLower() != "u" && dr["Action"].ToString().ToLower() != "a" && dr["Action"].ToString().ToLower() != "d")
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90447").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreCode"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90449").Replace("{0}", (i + 1).ToString())));
                    }
                    //if (Regex.Matches(dr["StoreCode"].ToString(), "[a-zA-Z]").Count > 0)
                    //{
                    //    this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90450").Replace("{0}", (i + 1).ToString())));
                    //}
                    if (string.IsNullOrEmpty(dr["Brand"].ToString()) || brandbll.ExistsBrand(dr["Brand"].ToString()) == 0)
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90448").Replace("{0}", (i + 1).ToString())));
                    }
                    else
                    {
                        if (dr["Action"].ToString().ToLower() == "u")
                        {
                            int brandid = brandbll.ExistsBrand(dr["Brand"].ToString());
                            if (!strbll.Exists(dr["StoreCode"].ToString(), brandid))
                            {
                                this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90452").Replace("{0}", (i + 1).ToString())));
                            }
                        }
                        if (dr["Action"].ToString().ToLower() == "d")
                        {
                            int brandid = brandbll.ExistsBrand(dr["Brand"].ToString());
                            if (!strbll.Exists(dr["StoreCode"].ToString(), brandid))
                            {
                                this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90453").Replace("{0}", (i + 1).ToString())));
                            }
                        }
                    }
                    if (dr["Action"].ToString().ToLower() == "a")
                    {
                        int brandid = brandbll.ExistsBrand(dr["Brand"].ToString());
                        if (!strbll.Exists(dr["StoreCode"].ToString(), brandid))
                        {
                            storelist.Add(new KeyValue() { Key = (i + 1).ToString(), Value = dr["StoreCode"].ToString() + " " + brandid.ToString() });
                            if (storelist.Count > 1)
                            {
                                for (int j = 0; j < storelist.Count - 1; j++)
                                {
                                    if (storelist[j].Value == dr["StoreCode"].ToString() + " " + brandid.ToString())
                                    {
                                        if (i > 0)//&& i + 1 != Convert.ToInt32(storelist[j].Key)
                                        {
                                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90451").Replace("{0}", (i + 1).ToString())));
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90463").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                    if (string.IsNullOrEmpty(dr["StoreName_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90454").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreAddress_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90455").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreCountry_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90456").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreProvince_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90457").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreCity_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90458").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["StoreDistrict_EN"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90459").Replace("{0}", (i + 1).ToString())));
                    }
                    if (!string.IsNullOrEmpty(dr["StoreType"].ToString()))
                    {
                        if (storetypebll.ExistsStoreType(dr["StoreType"].ToString()) == 0)
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90460").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                    if (!string.IsNullOrEmpty(dr["StoreGroup"].ToString()))
                    {
                        if (storegroupbll.ExistsStoreGroup(dr["StoreGroup"].ToString()) == 0)
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90461").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                    if (!string.IsNullOrEmpty(dr["Bank"].ToString()))
                    {
                        if (bankbll.ExistsBank(dr["Bank"].ToString()) == 0)
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90462").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                }
                if (this.list.Error.Count == 0) return true;
            }

            return false;
        }
        public void AnalysisData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                adt = dt.Clone();
                udt = dt.Clone();
                ddt = dt.Clone();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    switch (dr["Action"].ToString().ToLower())
                    {
                        case "a":
                            adt.Rows.Add(dr.ItemArray);
                            break;
                        case "u":
                            udt.Rows.Add(dr.ItemArray);
                            break;
                        case "d":
                            ddt.Rows.Add(dr.ItemArray);
                            break;
                    }
                    Edge.SVA.Model.Country ctrymodel = new SVA.Model.Country();
                    ctrymodel.CountryCode = dr["StoreCountry_EN"].ToString();
                    ctrymodel.CountryName1 = dr["StoreCountry_EN"].ToString();
                    ctrymodel.CountryName2 = dr["StoreCountry_SC"].ToString();
                    ctrymodel.CountryName3 = dr["StoreCountry_TC"].ToString();
                    ctrymodellist.Add(ctrymodel);

                    Edge.SVA.Model.Province prvmodel = new SVA.Model.Province();
                    prvmodel.ProvinceCode = dr["StoreProvince_EN"].ToString();
                    prvmodel.CountryCode = dr["StoreCountry_EN"].ToString();
                    prvmodel.ProvinceName1 = dr["StoreProvince_EN"].ToString();
                    prvmodel.ProvinceName2 = dr["StoreProvince_SC"].ToString();
                    prvmodel.ProvinceName3 = dr["StoreProvince_TC"].ToString();
                    prvmodellist.Add(prvmodel);

                    Edge.SVA.Model.City citymodel = new SVA.Model.City();
                    citymodel.CityCode = dr["StoreCity_EN"].ToString();
                    citymodel.ProvinceCode = dr["StoreProvince_EN"].ToString();
                    citymodel.CityName1 = dr["StoreCity_EN"].ToString();
                    citymodel.CityName2 = dr["StoreCity_SC"].ToString();
                    citymodel.CityName3 = dr["StoreCity_TC"].ToString();
                    citymodellist.Add(citymodel);

                    Edge.SVA.Model.District dismodel = new SVA.Model.District();
                    dismodel.DistrictCode = dr["StoreDistrict_EN"].ToString();
                    dismodel.CityCode = dr["StoreCity_EN"].ToString();
                    dismodel.DistrictName1 = dr["StoreDistrict_EN"].ToString();
                    dismodel.DistrictName2 = dr["StoreDistrict_SC"].ToString();
                    dismodel.DistrictName3 = dr["StoreDistrict_TC"].ToString();
                    dismodellist.Add(dismodel);
                }
                addmodellist = DataTableToList(adt, "a");
                updatemodellist = DataTableToList(udt, "u");
                delmodellist = DataTableToList(ddt, "");

                //去除重复项
                for (int i = 0; i < ctrymodellist.Count; i++)
                {
                    for (int j = ctrymodellist.Count - 1; j > i; j--)
                    {
                        if (ctrymodellist[i].CountryCode.Equals(ctrymodellist[j].CountryCode))
                        {
                            ctrymodellist.Remove(ctrymodellist[j]);
                        }
                    }
                }
                for (int i = 0; i < prvmodellist.Count; i++)
                {
                    for (int j = prvmodellist.Count - 1; j > i; j--)
                    {
                        if (prvmodellist[i].ProvinceCode.Equals(prvmodellist[j].ProvinceCode))
                        {
                            prvmodellist.Remove(prvmodellist[j]);
                        }
                    }
                }
                for (int i = 0; i < citymodellist.Count; i++)
                {
                    for (int j = citymodellist.Count - 1; j > i; j--)
                    {
                        if (citymodellist[i].CityCode.Equals(citymodellist[j].CityCode))
                        {
                            citymodellist.Remove(citymodellist[j]);
                        }
                    }
                }
                for (int i = 0; i < dismodellist.Count; i++)
                {
                    for (int j = dismodellist.Count - 1; j > i; j--)
                    {
                        if (dismodellist[i].DistrictCode.Equals(dismodellist[j].DistrictCode))
                        {
                            dismodellist.Remove(dismodellist[j]);
                        }
                    }
                }
            }
        }
        public List<Edge.SVA.Model.Store> DataTableToList(DataTable dt, string type)
        {
            List<Edge.SVA.Model.Store> modelList = new List<Edge.SVA.Model.Store>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.Store model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.Store();
                    if (dt.Rows[n]["StoreCode"] != null && dt.Rows[n]["StoreCode"].ToString() != "")
                    {
                        model.StoreCode = dt.Rows[n]["StoreCode"].ToString();
                    }
                    if (dt.Rows[n]["StoreName_EN"] != null && dt.Rows[n]["StoreName_EN"].ToString() != "")
                    {
                        model.StoreName1 = dt.Rows[n]["StoreName_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreName_SC"] != null && dt.Rows[n]["StoreName_SC"].ToString() != "")
                    {
                        model.StoreName2 = dt.Rows[n]["StoreName_SC"].ToString();
                    }
                    if (dt.Rows[n]["StoreName_TC"] != null && dt.Rows[n]["StoreName_TC"].ToString() != "")
                    {
                        model.StoreName3 = dt.Rows[n]["StoreName_TC"].ToString();
                    }
                    if (dt.Rows[n]["StoreType"] != null && dt.Rows[n]["StoreType"].ToString() != "")
                    {
                        model.StoreTypeID = storetypebll.ExistsStoreType(dt.Rows[n]["StoreType"].ToString());
                    }
                    if (dt.Rows[n]["StoreGroup"] != null && dt.Rows[n]["StoreGroup"].ToString() != "")
                    {
                        model.StoreGroupID = storegroupbll.ExistsStoreGroup(dt.Rows[n]["StoreGroup"].ToString());
                    }
                    if (dt.Rows[n]["Bank"] != null && dt.Rows[n]["Bank"].ToString() != "")
                    {
                        model.BankID = bankbll.ExistsBank(dt.Rows[n]["Bank"].ToString());
                    }
                    if (dt.Rows[n]["StoreCountry_EN"] != null && dt.Rows[n]["StoreCountry_EN"].ToString() != "")
                    {
                        model.StoreCountry = dt.Rows[n]["StoreCountry_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreProvince_EN"] != null && dt.Rows[n]["StoreProvince_EN"].ToString() != "")
                    {
                        model.StoreProvince = dt.Rows[n]["StoreProvince_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreCity_EN"] != null && dt.Rows[n]["StoreCity_EN"].ToString() != "")
                    {
                        model.StoreCity = dt.Rows[n]["StoreCity_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreAddress_EN"] != null && dt.Rows[n]["StoreAddress_EN"].ToString() != "")
                    {
                        model.StoreAddressDetail = dt.Rows[n]["StoreAddress_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreTel"] != null && dt.Rows[n]["StoreTel"].ToString() != "")
                    {
                        model.StoreTel = dt.Rows[n]["StoreTel"].ToString();
                    }
                    if (dt.Rows[n]["StoreFax"] != null && dt.Rows[n]["StoreFax"].ToString() != "")
                    {
                        model.StoreFax = dt.Rows[n]["StoreFax"].ToString();
                    }
                    if (dt.Rows[n]["StoreLongitude"] != null && dt.Rows[n]["StoreLongitude"].ToString() != "")
                    {
                        model.StoreLongitude = dt.Rows[n]["StoreLongitude"].ToString();
                    }
                    if (dt.Rows[n]["StoreLatitude"] != null && dt.Rows[n]["StoreLatitude"].ToString() != "")
                    {
                        model.StoreLatitude = dt.Rows[n]["StoreLatitude"].ToString();
                    }
                    if (type.ToLower() == "a")
                    {
                        model.CreatedOn = DateTime.Now;
                        model.CreatedBy = SVASessionInfo.CurrentUser.UserID;
                    }
                    if (type.ToLower() == "u")
                    {
                        model.UpdatedOn = DateTime.Now;
                        model.UpdatedBy = SVASessionInfo.CurrentUser.UserID;
                    }
                    if (dt.Rows[n]["StoreNote"] != null && dt.Rows[n]["StoreNote"].ToString() != "")
                    {
                        model.StoreNote = dt.Rows[n]["StoreNote"].ToString();
                    }
                    if (dt.Rows[n]["StoreOpenTime"] != null && dt.Rows[n]["StoreOpenTime"].ToString() != "")
                    {
                        model.StoreOpenTime = dt.Rows[n]["StoreOpenTime"].ToString();
                    }
                    if (dt.Rows[n]["Brand"] != null && dt.Rows[n]["Brand"].ToString() != "")
                    {
                        model.BrandID = brandbll.ExistsBrand(dt.Rows[n]["Brand"].ToString());
                    }
                    if (dt.Rows[n]["StorePicFile"] != null && dt.Rows[n]["StorePicFile"].ToString() != "")
                    {
                        model.StorePicFile = dt.Rows[n]["StorePicFile"].ToString();
                    }
                    if (dt.Rows[n]["MapsPicFile"] != null && dt.Rows[n]["MapsPicFile"].ToString() != "")
                    {
                        model.MapsPicFile = dt.Rows[n]["MapsPicFile"].ToString();
                    }
                    if (dt.Rows[n]["MapsPicShadowFile"] != null && dt.Rows[n]["MapsPicShadowFile"].ToString() != "")
                    {
                        model.MapsPicShadowFile = dt.Rows[n]["MapsPicShadowFile"].ToString();
                    }
                    if (dt.Rows[n]["Email"] != null && dt.Rows[n]["Email"].ToString() != "")
                    {
                        model.Email = dt.Rows[n]["Email"].ToString();
                    }
                    if (dt.Rows[n]["Contact"] != null && dt.Rows[n]["Contact"].ToString() != "")
                    {
                        model.Contact = dt.Rows[n]["Contact"].ToString();
                    }
                    if (dt.Rows[n]["StoreDistrict_EN"] != null && dt.Rows[n]["StoreDistrict_EN"].ToString() != "")
                    {
                        model.StoreDistrict = dt.Rows[n]["StoreDistrict_EN"].ToString();
                    }
                    if (dt.Rows[n]["StoreAddress_SC"] != null && dt.Rows[n]["StoreAddress_SC"].ToString() != "")
                    {
                        model.StoreAddressDetail2 = dt.Rows[n]["StoreAddress_SC"].ToString();
                    }
                    if (dt.Rows[n]["StoreAddress_TC"] != null && dt.Rows[n]["StoreAddress_TC"].ToString() != "")
                    {
                        model.StoreAddressDetail3 = dt.Rows[n]["StoreAddress_TC"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }
        public ExecResult Operation()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            Edge.SVA.BLL.Store bll = new SVA.BLL.Store();
            Edge.SVA.BLL.Country ctrybll = new SVA.BLL.Country();
            Edge.SVA.BLL.Province prvbll = new SVA.BLL.Province();
            Edge.SVA.BLL.City citybll = new SVA.BLL.City();
            Edge.SVA.BLL.District distbll = new SVA.BLL.District();

            this.list.Addrecords = 0;
            this.list.Updaterecords = 0;
            this.list.Deletedrecords = 0;
            try
            {
                //关闭触发器
                bll.OpenCloseTrigger("disable TRIGGER UpdateStoreCondition_Store ON Store", trans);
                foreach (var item in addmodellist)
                {
                    bll.ImportAdd(item, trans);
                    this.list.Addrecords++;
                }
                foreach (var item in updatemodellist)
                {
                    bll.ImportUpdate(item, trans);
                    this.list.Updaterecords++;
                }
                foreach (var item in delmodellist)
                {
                    bll.ImportDelete(item.StoreCode, Convert.ToInt32(item.BrandID), trans);
                    this.list.Deletedrecords++;
                }
                //操作地址
                foreach (var item in ctrymodellist)
                {
                    if (!ctrybll.Exists(item.CountryCode, trans))
                    {
                        ctrybll.Add(item,trans);
                    }
                }
                foreach (var item in prvmodellist)
                {
                    if (!prvbll.Exists(item.ProvinceCode, trans))
                    {
                        prvbll.Add(item, trans);
                    }
                }
                foreach (var item in citymodellist)
                {
                    if (!citybll.Exists(item.CityCode, trans))
                    {
                        citybll.Add(item, trans);
                    }
                }
                foreach (var item in dismodellist)
                {
                    if (!distbll.Exists(item.DistrictCode, trans))
                    {
                        distbll.Add(item, trans);
                    }
                }
                bll.OpenCloseTrigger("enable TRIGGER UpdateStoreCondition_Store ON Store",trans);
                bll.OpenCloseTrigger("exec UpdateCardGradeStoreCondition 0,0,0,0", trans);
                bll.OpenCloseTrigger("exec UpdateCouponTypeStoreCondition 0,0,0,0", trans);
                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
            }
            return rtn;
        }
        public StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");

            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                }
                html.AppendFormat("</table></td></tr>");
            }
            else
            {
                html.AppendFormat("<tr><td align='right'></td><td>Add {0} records {1}.</td></tr>", this.list.Addrecords, "successfully");
                html.AppendFormat("<tr><td align='right'></td><td>Update {0} records {1}.</td></tr>", this.list.Updaterecords, "successfully");
                html.AppendFormat("<tr><td align='right'></td><td>Delete {0} records {1}.</td></tr>", this.list.Deletedrecords, "successfully");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Import Stores");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            this.list.Addrecords = 0;
            this.list.Updaterecords = 0;
            this.list.Deletedrecords = 0;
            this.list.Error.Clear();

            return html;
        }
        #endregion

        #region 导出数据的处理
        public DataTable GetExportData()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select a.*,b.CountryName1, b.CountryName2,b.CountryName3,");
            sb.Append("c.ProvinceName1, c.ProvinceName2,c.ProvinceName3,");
            sb.Append("d.CityName1, d.CityName2,d.CityName3,");
            sb.Append("e.DistrictName1, e.DistrictName2,e.DistrictName3, ");
            sb.Append("f.BrandName1, g.StoreGroupName1,h.StoreTypeName1 ");
            sb.Append("from Store a ");
            sb.Append("left join Country b on a.StoreCountry=b.CountryCode ");
            sb.Append("left join Province c on a.StoreProvince=c.ProvinceCode ");
            sb.Append("left join City d on a.StoreCity=d.ProvinceCode ");
            sb.Append("left join District e on a.StoreDistrict=e.DistrictCode ");
            sb.Append("left join Brand f on a.BrandID=f.BrandID ");
            sb.Append("left join StoreGroup g on a.StoreGroupID=g.StoreGroupID ");
            sb.Append("left join StoreType h on a.StoreTypeID=h.StoreTypeID ");
            DataSet ds = DBUtility.DbHelperSQL.Query(sb.ToString());

            if (ds.Tables.Count > 0) return ds.Tables[0];
            else return null;
        }
        public string UpLoadFileToServer(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/Store/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/Store/" + "ExportStoreInfo" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder(1000);
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("Action\tStoreCode\tStoreName_EN\tStoreName_SC\tStoreName_TC\tStoreAddress_EN");
                    text.Append("\tStoreAddress_SC\tStoreAddress_TC\tStoreCountry_EN\tStoreCountry_SC\tStoreCountry_TC");
                    text.Append("\tStoreProvince_EN\tStoreProvince_SC\tStoreProvince_TC\tStoreCity_EN\tStoreCity_SC");
                    text.Append("\tStoreCity_TC\tStoreDistrict_EN\tStoreDistrict_SC\tStoreDistrict_TC\tStoreTel");
                    text.Append("\tStoreFax\tEmail\tContact\tStoreLongitude\tStoreLatitude\tStoreOpenTime");
                    text.Append("\tBrand\tStoreType\tStoreGroup\tBank\tStorePicFile\tMapsPicFile");
                    text.Append("\tMapsPicShadowFile\tStoreNote");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.AppendFormat("{0}\t", "U");
                        text.AppendFormat("'{0}\t", dr["StoreCode"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreName1"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreName2"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreName3"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreAddressDetail"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreAddressDetail2"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreAddressDetail3"].ToString());
                        text.AppendFormat("{0}\t", dr["CountryName1"].ToString());
                        text.AppendFormat("{0}\t", dr["CountryName2"].ToString());
                        text.AppendFormat("{0}\t", dr["CountryName3"].ToString());
                        text.AppendFormat("{0}\t", dr["ProvinceName1"].ToString());
                        text.AppendFormat("{0}\t", dr["ProvinceName2"].ToString());
                        text.AppendFormat("{0}\t", dr["ProvinceName3"].ToString());
                        text.AppendFormat("{0}\t", dr["CityName1"].ToString());
                        text.AppendFormat("{0}\t", dr["CityName2"].ToString());
                        text.AppendFormat("{0}\t", dr["CityName3"].ToString());
                        text.AppendFormat("{0}\t", dr["DistrictName1"].ToString());
                        text.AppendFormat("{0}\t", dr["DistrictName2"].ToString());
                        text.AppendFormat("{0}\t", dr["DistrictName3"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["StoreTel"].ToString()) ? "" : "'" + dr["StoreTel"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["StoreFax"].ToString()) ? "" : "'" + dr["StoreFax"].ToString());
                        text.AppendFormat("{0}\t", dr["Email"].ToString());
                        text.AppendFormat("{0}\t", dr["Contact"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreLongitude"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreLatitude"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreOpenTime"].ToString());
                        text.AppendFormat("{0}\t", dr["BrandName1"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreTypeName1"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreGroupName1"].ToString());
                        text.AppendFormat("{0}\t", dr["BankID"].ToString());
                        text.AppendFormat("{0}\t", dr["StorePicFile"].ToString());
                        text.AppendFormat("{0}\t", dr["MapsPicFile"].ToString());
                        text.AppendFormat("{0}\t", dr["MapsPicShadowFile"].ToString());
                        text.AppendFormat("{0}\t", dr["StoreNote"].ToString());
                        text.Append("\r\n");

                        if (text.Length >= 10000)
                        {
                            byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                            fs.Write(buffer, 0, buffer.Length);
                            text.Remove(0, text.Length);
                        }
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }
        #endregion
    }
    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<string> Error { get { return error; } }
        public int Addrecords { get; set; }
        public int Updaterecords { get; set; }
        public int Deletedrecords { get; set; }
        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                if (Addrecords == 0 && Updaterecords == 0 && Deletedrecords == 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}