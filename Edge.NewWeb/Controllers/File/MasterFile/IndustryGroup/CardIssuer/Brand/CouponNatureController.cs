﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.CouponNature
{
    public class CouponNatureController
    {
        protected CouponNatureViewModel viewModel = new CouponNatureViewModel();

        public CouponNatureViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int CouponNatureID)
        {
            Edge.SVA.BLL.CouponNature bll = new Edge.SVA.BLL.CouponNature();
            Edge.SVA.Model.CouponNature model = bll.GetModel(CouponNatureID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CouponNature bll = new SVA.BLL.CouponNature();
                
                //保存
                if (bll.Exists(viewModel.MainTable.CouponNatureID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CouponNature bll = new SVA.BLL.CouponNature();

                //保存
                if (bll.Exists(viewModel.MainTable.CouponNatureID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.StoreType bll = new Edge.SVA.BLL.StoreType();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CouponNatureCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddStoreTypeName(ds, "CouponNatureName", "CouponNatureID");

            return ds;
        }

        protected bool IsExists(int CouponNatureID)
        {
            Edge.SVA.BLL.Company bllmc = new SVA.BLL.Company();
            string strwhere = " CouponNatureID =" + CouponNatureID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string CouponNatureCode, int CouponNatureID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasCouponNatureCode(CouponNatureCode, CouponNatureID))
            {
                Errormessage = Resources.MessageTips.ExistCouponNatureCode;
            }
            return Errormessage;
        }
    }
}