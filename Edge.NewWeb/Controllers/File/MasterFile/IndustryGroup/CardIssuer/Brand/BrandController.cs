﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public class BrandController
    {
        protected BrandViewModel viewModel = new BrandViewModel();

        public BrandViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int BrandID)
        {
            Edge.SVA.BLL.Brand bll = new Edge.SVA.BLL.Brand();
            Edge.SVA.Model.Brand model = bll.GetModel(BrandID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Brand bll = new SVA.BLL.Brand();
                
                //保存
                if (bll.Exists(viewModel.MainTable.BrandID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Brand bll = new SVA.BLL.Brand();

                //保存
                if (bll.Exists(viewModel.MainTable.BrandID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Brand bll = new Edge.SVA.BLL.Brand();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "BrandCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Edge.Web.Tools.DataTool.AddCardIssuerName(ds, "CardIssuerName", "CardIssuerID");
            return ds;
        }

        public string ValidataObject(string BrandCode, int BrandID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasBrandCode(BrandCode, BrandID))
            {
                Errormessage = Resources.MessageTips.ExistBrandCode;
            }
            return Errormessage;
        }
    }
}