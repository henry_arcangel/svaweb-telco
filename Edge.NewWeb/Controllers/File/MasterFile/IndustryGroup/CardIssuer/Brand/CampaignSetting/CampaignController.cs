﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignController
    {
        protected CampaignViewModel viewModel = new CampaignViewModel();

        public CampaignViewModel ViewModel
        {
            get { return viewModel; }
        }
        public bool ValidateIfCanAddCardGrade(KeyValue model)
        {
            return !ExistSameKeyInKeyValueList(model, viewModel.CardGradeList);
        }
        public void AddCardGradeList(List<string> list)
        {
            CardGradeRepostory res =CardGradeRepostory.Singleton;
            foreach (var item in list)
            {
                KeyValue exist = this.viewModel.CardGradeList.Find(m => m.Key == item);
                if (exist==null)
                {
                    KeyValue kv = new KeyValue();
                    kv.Key = item;
                    CardGrade cg = res.GetModelByID(ConvertTool.ConverType<int>(item));
                    switch (SVASessionInfo.SiteLanguage)
                    {
                        case LanguageFlag.ZHCN:
                            kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName2;
                            break;
                        case LanguageFlag.ZHHK:
                            kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName3;
                            break;
                        case LanguageFlag.ENUS:
                        default:
                            kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName1;
                            break;
                    }
                    this.viewModel.CardGradeList.Add(kv);
                }
            }

        }
        public void RemoveCardGrade(KeyValue model)
        {
            if (this.viewModel.CardGradeList.Contains(model))
            {
                this.viewModel.CardGradeList.Remove(model);
            }
        }
        public bool ValidateIfCanAddCouponType(KeyValue model)
        {
            return !ExistSameKeyInKeyValueList(model, viewModel.CouponTypeList);
        }
        public void AddCouponTypeList(List<string> list)
        {
            CouponTypeRepostory res = CouponTypeRepostory.Singleton;
            foreach (var item in list)
            {
                KeyValue exist = this.viewModel.CouponTypeList.Find(m => m.Key == item);
                if (exist == null)
                {
                    KeyValue kv = new KeyValue();
                    kv.Key = item;
                    Edge.SVA.Model.CouponType cg = res.GetModelByID(ConvertTool.ConverType<int>(item));
                    switch (SVASessionInfo.SiteLanguage)
                    {
                        case LanguageFlag.ZHCN:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName2;
                            break;
                        case LanguageFlag.ZHHK:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName3;
                            break;
                        case LanguageFlag.ENUS:
                        default:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName1;
                            break;
                    }
                    this.viewModel.CouponTypeList.Add(kv);
                }
            }
        }
        public void RemoveCouponType(KeyValue model)
        {
            if (this.viewModel.CouponTypeList.Contains(model))
            {
                this.viewModel.CouponTypeList.Remove(model);
            }
        }
        private bool ExistSameKeyInKeyValueList(KeyValue model, List<KeyValue> list)
        {
            bool rtn = false;
            if (model == null)
            {
                return true;
            }
            KeyValue mo = list.Find(m => m.Key == model.Key);
            if (mo != null)
            {
                return true;
            }
            return rtn;
        }

        //List界面绑定(Add by Len)
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Campaign bll = new Edge.SVA.BLL.Campaign();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CampaignCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);
            Tools.DataTool.AddCampaignName(ds, "CampaignName", "CampaignID");
            Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
            return ds;
        }
    }
}