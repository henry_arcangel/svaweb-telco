﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignAddController : CampaignController
    {
        public bool Validate(out List<KeyValue> errorList)
        {
            bool rtn = true;
            errorList = new List<KeyValue>();

            return rtn;
        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                int id = viewModel.MainTable.CampaignID;
                Edge.SVA.BLL.CampaignCardGrade bll1 = new Edge.SVA.BLL.CampaignCardGrade();
                bll1.Delete(id, 0);
                foreach (var item in this.viewModel.CardGradeList)
                {
                    Edge.SVA.Model.CampaignCardGrade model = new Edge.SVA.Model.CampaignCardGrade();
                    model.CampaignID = id;
                    model.CardGradeID = ConvertTool.ConverType<int>(item.Key);
                    bll1.Add(model);
                }

                Edge.SVA.BLL.CampaignCouponType bll2 = new Edge.SVA.BLL.CampaignCouponType();
                bll2.Delete(id, 0);
                foreach (var item in this.viewModel.CouponTypeList)
                {
                    Edge.SVA.Model.CampaignCouponType model = new Edge.SVA.Model.CampaignCouponType();
                    model.CampaignID = id;
                    model.CouponTypeID = ConvertTool.ConverType<int>(item.Key);
                    bll2.Add(model);
                }
                SVASessionInfo.CardGradeAddController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Save()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign bll = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign();
                int keyid = bll.Add(viewModel.MainTable, trans);

                //第一个子表的保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade bllcard = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade();
                Edge.SVA.Model.CampaignCardGrade model = new SVA.Model.CampaignCardGrade();
                bllcard.Delete(keyid, 0, trans);
                model.CampaignID = keyid;
                foreach (var item in this.viewModel.CardGradeList)
                {
                    model.CardGradeID = ConvertTool.ConverType<int>(item.Key);
                    bllcard.Add(model, trans);
                }

                //第二个子表的保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType bllcoupon = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType();
                bllcoupon.Delete(keyid, 0, trans);
                Edge.SVA.Model.CampaignCouponType modelcoupon = new Edge.SVA.Model.CampaignCouponType();
                modelcoupon.CampaignID = keyid;
                foreach (var item in this.viewModel.CouponTypeList)
                {
                    modelcoupon.CouponTypeID = ConvertTool.ConverType<int>(item.Key);
                    bllcoupon.Add(modelcoupon, trans);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
                SVASessionInfo.CardGradeAddController = null;
            }
            return rtn;
        }

    }
}