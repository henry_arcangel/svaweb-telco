﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.ViewModelBLL;
using System.Text;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public class CardGradeModifyController : CardGradeController
    {
        public void LoadViewModel(int cardGradeID, string lan)
        {
            CardGradeViewModelBLL bll = new CardGradeViewModelBLL();
            viewModel = bll.GetModel(cardGradeID, lan);

            viewModel.MemberClause = GetMemberClause(cardGradeID);
        }
        /// <summary>
        /// 是否继承cardtype的规则
        /// </summary>
        public bool IsInheritsOfCardTypeRule
        {
            get {
                Edge.SVA.Model.CardType item = Edge.SVA.BLL.Domain.DataResources.CardTypeRepostory.Singleton.GetModelByID(this.viewModel.MainTable.CardTypeID);
                if (item==null)
                {
                    return false;
                }
                if (item.IsImportUIDNumber==this.viewModel.MainTable.IsImportUIDNumber
                    &&!string.IsNullOrEmpty(item.CardNumMask)
                    &&item.CardNumMask==this.viewModel.MainTable.CardNumMask
                    &&item.CardNumPattern==this.viewModel.MainTable.CardNumPattern
                    )
                {
                    return true;
                }
                return false;
            }
        }
        public bool Validate(out List<KeyValue> errorList)
        {
            errorList = new List<KeyValue>();
            KeyValue desc;
            if (!viewModel.ValidateCanHoldCouponCount())
            {
                desc = new KeyValue();
                desc.Key = "HoldCount";
                desc.Value = " reach max coupon quantity!";
                errorList.Add(desc);
                goto ReturnUnSuccess;
            }
            return true;

        ReturnUnSuccess: return false;

        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                int cardgradeID = viewModel.MainTable.CardGradeID;
                Edge.SVA.BLL.CardGradeHoldCouponRule bll = new Edge.SVA.BLL.CardGradeHoldCouponRule();
                StringBuilder sb = new StringBuilder();
                foreach (var item in viewModel.CardGradeHoldCouponRuleListDelete)
                {
                    sb.Append(",");
                    sb.Append(item.MainTable.KeyID);
                }
                if (sb.Length >= 1)
                {
                    bll.DeleteList(sb.ToString().Substring(1));
                }
                foreach (var item in viewModel.CardGradeHoldCouponRuleListAdd)
                {
                    item.MainTable.CardGradeID = cardgradeID;
                    item.MainTable.UpdatedOn = DateTime.Now;
                    bll.Add(item.MainTable);
                }

                Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
                if (IsExistsMemberClause(cardgradeID))
                {
                    bllmc.Update(viewModel.MemberClause);
                }
                else
                {
                    viewModel.MemberClause.ClauseTypeCode = "CardGrade";
                    viewModel.MemberClause.ClauseSubCode = cardgradeID.ToString();
                    bllmc.Add(viewModel.MemberClause);
                }
                SVASessionInfo.CardGradeModifyController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public Edge.SVA.Model.MemberClause GetMemberClause(int cardGradeID)
        {
            Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
            string strwhere = " MemberClause.ClauseTypeCode='CardGrade' and MemberClause.ClauseSubCode=" + cardGradeID;
            foreach (var item in bllmc.GetModelList(strwhere))
            {
                viewModel.MemberClause = item;
            }
            return viewModel.MemberClause;
        }
        private bool IsExistsMemberClause(int cardGradeID)
        {
            Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
            string strwhere = " MemberClause.ClauseTypeCode='CardGrade' and MemberClause.ClauseSubCode=" + cardGradeID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
    }
}