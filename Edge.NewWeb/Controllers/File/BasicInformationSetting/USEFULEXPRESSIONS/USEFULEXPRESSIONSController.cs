﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public class USEFULEXPRESSIONSController
    {
        protected USEFULEXPRESSIONSViewModel viewModel = new USEFULEXPRESSIONSViewModel();

        public USEFULEXPRESSIONSViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.USEFULEXPRESSIONS bll = new Edge.SVA.BLL.USEFULEXPRESSIONS();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "Code";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddPhraseTitle(ds, "PhraseTitle", "USEFULEXPRESSIONSID");
            return ds;
        }
    }
}