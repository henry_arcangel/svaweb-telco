﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public class USEFULEXPRESSIONSAddController : USEFULEXPRESSIONSController
    {
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.USEFULEXPRESSIONS bll = new SVA.BLL.USEFULEXPRESSIONS();
                
                //保存
                if (bll.Exists(viewModel.MainTable.USEFULEXPRESSIONSID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }
               
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
    }
}