﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.StoreGroup
{
    public class StoreGroupController
    {
        protected StoreGroupViewModel viewModel = new StoreGroupViewModel();

        public StoreGroupViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int StoreGroupID)
        {
            Edge.SVA.BLL.StoreGroup bll = new Edge.SVA.BLL.StoreGroup();
            Edge.SVA.Model.StoreGroup model = bll.GetModel(StoreGroupID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.StoreGroup bll = new SVA.BLL.StoreGroup();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreGroupID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.StoreGroup bll = new SVA.BLL.StoreGroup();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreGroupID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.StoreGroup bll = new Edge.SVA.BLL.StoreGroup();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "StoreGroupCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddStoreGroupName(ds, "StoreGroupName", "StoreGroupID");

            return ds;
        }

        protected bool IsExists(int StoreGroupID)
        {
            Edge.SVA.BLL.StoreGroup bllmc = new SVA.BLL.StoreGroup();
            string strwhere = " StoreGroupID =" + StoreGroupID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string StoreGroupCode, int StoreGroupID)
        {
            string Errormessage = "";

            return Errormessage;
        }
    }
}