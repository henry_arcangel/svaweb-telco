﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Company
{
    public class CompanyController
    {
        protected CompanyViewModel viewModel = new CompanyViewModel();

        public CompanyViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int CompanyID)
        {
            Edge.SVA.BLL.Company bll = new Edge.SVA.BLL.Company();
            Edge.SVA.Model.Company model = bll.GetModel(CompanyID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Company bll = new SVA.BLL.Company();

                //保存
                if (this.IsExists(viewModel.MainTable.CompanyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Company bll = new SVA.BLL.Company();

                //保存
                if (this.IsExists(viewModel.MainTable.CompanyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Company bll = new Edge.SVA.BLL.Company();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "CompanyCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            //Tools.DataTool.AddSupplierDesc(ds, "SupplierDesc", "SupplierID");

            return ds;
        }

        protected bool IsExists(int CompanyID)
        {
            Edge.SVA.BLL.Company bllmc = new SVA.BLL.Company();
            string strwhere = " CompanyID =" + CompanyID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
    }
}