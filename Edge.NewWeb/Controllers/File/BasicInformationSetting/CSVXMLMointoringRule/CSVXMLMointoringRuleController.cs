﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using System.Data.SqlClient;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLMointoringRule
{
    public class CSVXMLMointoringRuleController
    {
        protected CSVXMLMointoringRuleViewModel viewModel = new CSVXMLMointoringRuleViewModel(); 

        public CSVXMLMointoringRuleViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string code)
        {
            Edge.SVA.BLL.CSVXMLMointoringRule bll = new Edge.SVA.BLL.CSVXMLMointoringRule();
            Edge.SVA.Model.CSVXMLMointoringRule model = bll.GetModel(code);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CSVXMLMointoringRule bll = new SVA.BLL.CSVXMLMointoringRule();

                //保存
                if (this.IsExists(viewModel.MainTable.RuleName))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CSVXMLMointoringRule bll = new SVA.BLL.CSVXMLMointoringRule();

                //保存
                if (this.IsExists(viewModel.MainTable.RuleName))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.CSVXMLMointoringRule bll = new Edge.SVA.BLL.CSVXMLMointoringRule();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "RuleName";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);
            Tools.DataTool.AddStatus(ds, "StatusName", "Status");
            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            //Tools.DataTool.AddReasonDesc(ds, "ReasonDesc", "ReasonID");

            return ds;
        }

        protected bool IsExists(string code)
        {
            Edge.SVA.BLL.CSVXMLMointoringRule bllmc = new SVA.BLL.CSVXMLMointoringRule();
            string strwhere = " RuleName =" + code;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string code)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasCSVXMLMointoringRuleCode(code))
            {
                Errormessage = Resources.MessageTips.ExistRecord;
            }
            return Errormessage;
        }
        public void RunRule(string RuleNumber)
        {
            IDataParameter[] iData = new SqlParameter[2];
            iData[0] = new SqlParameter("@UserID", SqlDbType.Int);
            iData[0].Value = DALTool.GetCurrentUser().UserID;
            iData[1] = new SqlParameter("@RuleNumber", SqlDbType.VarChar, 64);
            iData[1].Value = RuleNumber;
            int result = 0;

            //DBUtility.DbHelperSQL.RunProcedure("DoCSVXMLMointoringRule", iData, out result);

            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": FineUI.Alert.ShowInTop("Finish to execute!"); ; break;
                case "zh-cn": FineUI.Alert.ShowInTop("运行完成！"); ; break;
                case "zh-hk": FineUI.Alert.ShowInTop("運行完成！"); break;
            }
        }
    }
}