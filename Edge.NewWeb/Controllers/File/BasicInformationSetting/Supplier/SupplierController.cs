﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Supplier
{
    public class SupplierController
    {
        protected SupplierViewModel viewModel = new SupplierViewModel();

        public SupplierViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int SupplierID)
        {
            Edge.SVA.BLL.Supplier bll = new Edge.SVA.BLL.Supplier();
            Edge.SVA.Model.Supplier model = bll.GetModel(SupplierID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Supplier bll = new SVA.BLL.Supplier();

                //保存
                if (this.IsExists(viewModel.MainTable.SupplierID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Supplier bll = new SVA.BLL.Supplier();

                //保存
                if (this.IsExists(viewModel.MainTable.SupplierID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Supplier bll = new Edge.SVA.BLL.Supplier();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "SupplierCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddSupplierDesc(ds, "SupplierDesc", "SupplierID");

            return ds;
        }

        protected bool IsExists(int SupplierID)
        {
            Edge.SVA.BLL.Supplier bllmc = new SVA.BLL.Supplier();
            string strwhere = " SupplierID =" + SupplierID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
    }
}