﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.CurrencyExchangeRate
{
    public class CurrencyController
    {
        protected CurrencyViewModel viewModel = new CurrencyViewModel();

        public CurrencyViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int CurrencyID)
        {
            Edge.SVA.BLL.Currency bll = new Edge.SVA.BLL.Currency();
            Edge.SVA.Model.Currency model = bll.GetModel(CurrencyID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Currency bll = new SVA.BLL.Currency();

                //保存
                if (bll.Exists(viewModel.MainTable.CurrencyCode, viewModel.MainTable.CurrencyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Currency bll = new SVA.BLL.Currency();

                //保存
                if (bll.Exists(viewModel.MainTable.CurrencyCode, viewModel.MainTable.CurrencyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere,int pageSize, int pageIndex, out int recodeCount)
        {
            DataSet ds;
            Edge.SVA.BLL.Currency bll = new Edge.SVA.BLL.Currency();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            ds = bll.GetListByPage(strWhere, "CurrencyCode", pageSize * pageIndex + 1, pageSize * (pageIndex + 1)); //bll.GetList(pageSize, pageIndex, strWhere, "");

            DataTool.AddCurrencyName(ds, "CurrencyName", "CurrencyID");

            return ds;
        }

        public string ValidataObject(string CurrencyCode, int CurrencyID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasCurrencyCode(CurrencyCode, CurrencyID))
            {
                Errormessage = Resources.MessageTips.ExistCurrencyCode;
            }
            return Errormessage;
        }
    }
}