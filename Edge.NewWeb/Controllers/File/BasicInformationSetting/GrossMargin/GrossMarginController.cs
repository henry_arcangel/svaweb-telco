﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using Edge.Web.Tools;
using Edge.Utils.Tools;

namespace Edge.Web.Controllers.File.BasicInformationSetting.GrossMargin
{
    public class GrossMarginController
    {
        protected GrossMarginViewModel viewModel = new GrossMarginViewModel();

        public GrossMarginViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string GrossMarginCode)
        {
            Edge.SVA.BLL.GrossMargin bll = new Edge.SVA.BLL.GrossMargin();
            Edge.SVA.Model.GrossMargin model = bll.GetModel(GrossMarginCode);
            viewModel.MainTable = model;

            Edge.SVA.BLL.GrossMargin_MobileNo detailbll = new Edge.SVA.BLL.GrossMargin_MobileNo();
            List<Edge.SVA.Model.GrossMargin_MobileNo> dm = detailbll.GetModelList(" GrossMarginCode=" + GrossMarginCode);
            viewModel.DetailTable = dm;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.GrossMargin bll = new SVA.BLL.GrossMargin();
                Edge.SVA.BLL.GrossMargin_MobileNo detailbll = new SVA.BLL.GrossMargin_MobileNo();

                //保存
                if (this.IsExists(viewModel.MainTable.GrossMarginCode))
                {
                    bll.Update(viewModel.MainTable);
                    foreach (Edge.SVA.Model.GrossMargin_MobileNo a in ViewModel.DetailTable)
                    {
                        detailbll.Update(a);
                    }                      
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                    foreach (Edge.SVA.Model.GrossMargin_MobileNo a in ViewModel.DetailTable)
                    {
                        detailbll.Add(a);
                    }                  
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.GrossMargin bll = new SVA.BLL.GrossMargin();
                Edge.SVA.BLL.GrossMargin_MobileNo detailbll = new SVA.BLL.GrossMargin_MobileNo();

                //保存
                if (this.IsExists(viewModel.MainTable.GrossMarginCode))
                {
                    string delkeylist= "";
                    int delkey = 0;
                    List<Edge.SVA.Model.GrossMargin_MobileNo> dm = detailbll.GetModelList(" GrossMarginCode=" + viewModel.MainTable.GrossMarginCode);
                    foreach (SVA.Model.GrossMargin_MobileNo md in dm)
                    {
                        delkey = md.KeyID;
                        foreach (SVA.Model.GrossMargin_MobileNo viewmd in ViewModel.DetailTable)
                        {                           
                            if (md.KeyID == viewmd.KeyID)
                            {
                                delkey = 0;
                                break;
                            }
                        }
                        if (delkey != 0)
                        {
                            if (delkeylist != "")
                                delkeylist = delkeylist + "," + delkey.ToString();
                            else
                                delkeylist =delkey.ToString();
                        }
                    }
                    if (delkeylist != "")
                        detailbll.DeleteList(delkeylist);

                    bll.Update(viewModel.MainTable);
                    foreach (SVA.Model.GrossMargin_MobileNo md in ViewModel.DetailTable)
                    {
                        if (IsDetailExists(md.KeyID))
                            detailbll.Update(md);
                        else 
                            detailbll.Add(md);
                    }
                }

                else
                {
                    bll.Add(viewModel.MainTable);
                    foreach (SVA.Model.GrossMargin_MobileNo md in ViewModel.DetailTable)
                    {
                        if (IsDetailExists(md.KeyID))
                            detailbll.Update(md);
                        else
                            detailbll.Add(md);
                    }
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.GrossMargin bll = new Edge.SVA.BLL.GrossMargin();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "GrossMarginCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));
            Tools.DataTool.AddCardTypeNameByID(ds, "CardTypeName", "CardTypeID");
            Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");
            Tools.DataTool.AddBuyerName(ds, "BuyerName");
            Tools.DataTool.AddVenderName(ds, "VenderName");
            
            return ds;
        }

        protected bool IsExists(string GrossMarginCode)
        {
            Edge.SVA.BLL.GrossMargin bllmc = new SVA.BLL.GrossMargin();
            string strwhere = " GrossMarginCode ='" + GrossMarginCode + "'";
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
        protected bool IsDetailExists(int KeyID)
        {
            Edge.SVA.BLL.GrossMargin_MobileNo bllmn = new SVA.BLL.GrossMargin_MobileNo();
            string strwhere = " KeyID =" + KeyID.ToString();
            return (bllmn.GetModelList(strwhere).Count > 0) ? true : false;
        }


        public void  GetDetailList(int KeyID, ref Edge.SVA.Model.GrossMargin_MobileNo md)
        {
            Edge.SVA.BLL.GrossMargin_MobileNo bll = new SVA.BLL.GrossMargin_MobileNo();
            List<Edge.SVA.Model.GrossMargin_MobileNo> list = bll.GetModelList("KeyID = '" + KeyID.ToString() + "'");
            if (list != null)
                md = list[0];
            else
                md = null;
        }

        public void GetDetailList(string GrossMarginCode)
        {
            Edge.SVA.BLL.GrossMargin_MobileNo bll = new SVA.BLL.GrossMargin_MobileNo();
            List<Edge.SVA.Model.GrossMargin_MobileNo> list = bll.GetModelList("GrossMarginCode = '" + GrossMarginCode + "'");
            this.viewModel.DetailTable.Clear();
            foreach (Edge.SVA.Model.GrossMargin_MobileNo md in  (List<Edge.SVA.Model.GrossMargin_MobileNo>)list)
            {                
               this.viewModel.DetailTable.Add(md);
            }

        }
    }
}