﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Advertisements
{
    public class AdvertisementsAddController : AdvertisementsController
    {
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PromotionMsg bll = new SVA.BLL.PromotionMsg();
                int objctid = 0;
                //Msg表保存
                if (bll.Exists(viewModel.MainTable.KeyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    objctid = bll.Add(viewModel.MainTable);
                }
                //CardCondition表保存
                Edge.SVA.BLL.PromotionCardCondition bll1 = new SVA.BLL.PromotionCardCondition();
                Edge.SVA.Model.PromotionCardCondition model = new SVA.Model.PromotionCardCondition();
                bll1.Delete(objctid, 0);
                model.PromotionMsgID = objctid;
                foreach (var item in viewModel.CardGradeIDList)
                {
                    model.CardGradeID = item;
                    bll1.Add(model);
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Save()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg bll = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg();
                int keyid = bll.Add(viewModel.MainTable, trans);

                //第一个子表的保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition bllcard = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition();
                Edge.SVA.Model.PromotionCardCondition model = new SVA.Model.PromotionCardCondition();
                bllcard.Delete(keyid, 0, trans);
                model.PromotionMsgID = keyid;
                foreach (var item in viewModel.CardGradeIDList)
                {
                    model.CardGradeID = item;
                    bllcard.Add(model,trans);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
            }
            return rtn;
        }
    }
}