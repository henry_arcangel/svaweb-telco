﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Advertisements.NewsPicture
{
    public class NewsPictureController
    {
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.PromotionMsg_Pic bll = new Edge.SVA.BLL.PromotionMsg_Pic();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "PromotionMsgCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            //Tools.DataTool.AddOrganizationName(ds, "OrganizationName", "OrganizationID");

            return ds;
        }
    }
}