﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.Controllers
{
    public class CardOrderController
    {
        #region Approve Order

        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CardOrderForm_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            Edge.SVA.BLL.Ord_CardOrderForm_H bll = new Edge.SVA.BLL.Ord_CardOrderForm_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardOrderForm_H().GetModel(model.CardOrderFormNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CardPicking_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPicked;

            string msg = "";
            if (!CanApprovePicked(model, out msg)) return msg;

            Edge.SVA.BLL.Ord_CardPicking_H bll = new Edge.SVA.BLL.Ord_CardPicking_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardPicking_H().GetModel(model.CardPickingNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CardDelivery_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            Edge.SVA.BLL.Ord_CardDelivery_H bll = new Edge.SVA.BLL.Ord_CardDelivery_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardDelivery_H().GetModel(model.CardDeliveryNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        #endregion

        #region Void Card

        private static bool VoidCard(Edge.SVA.Model.Ord_CardOrderForm_H model)
        {
            if (model == null) return false;

            //if (model.ApproveStatus != "P") return false;
            if (model.ApproveStatus != "P" && model.ApproveStatus != "C" && model.ApproveStatus != "A") return false; //C for Pending. A for Approved Robin-2014-07-22 RRG Request

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardOrderForm_H bll = new Edge.SVA.BLL.Ord_CardOrderForm_H();

            return bll.Update(model);
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_CardPicking_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P" && model.ApproveStatus != "R") return false;//R for Pending, P for Picked.

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardPicking_H bll = new Edge.SVA.BLL.Ord_CardPicking_H();

            return bll.Update(model);
        }

        private static bool VoidCard(Edge.SVA.Model.Ord_CardDelivery_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardDelivery_H bll = new Edge.SVA.BLL.Ord_CardDelivery_H();

            return bll.Update(model);
        }
        #endregion

        #region Picked Card
        //Add By Robin for Picking Order 2014-07-10
        private static bool PickedCard(Edge.SVA.Model.Ord_CardOrderForm_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "A") return false;

            model.ApproveStatus = "C";
            Edge.SVA.BLL.Ord_CardOrderForm_H bll = new Edge.SVA.BLL.Ord_CardOrderForm_H();

            return bll.Update(model);
        }
        //End

        private static bool PickedCard(Edge.SVA.Model.Ord_CardPicking_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "R") return false;

            model.ApproveStatus = "P";//Picked
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardPicking_H bll = new Edge.SVA.BLL.Ord_CardPicking_H();

            return bll.Update(model);
        }
        #endregion


        public static string BatchVoidCard(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.CardOrderForm)
                {
                    Edge.SVA.Model.Ord_CardOrderForm_H model = new Edge.SVA.BLL.Ord_CardOrderForm_H().GetModel(id);
                    if (VoidCard(model)) success++;
                }
                else if (type == ApproveType.CardPicking)
                {
                    Edge.SVA.Model.Ord_CardPicking_H model = new Edge.SVA.BLL.Ord_CardPicking_H().GetModel(id);
                    if (VoidCard(model)) success++;
                }
                else if (type == ApproveType.CardDelivery)
                {
                    Edge.SVA.Model.Ord_CardDelivery_H model = new Edge.SVA.BLL.Ord_CardDelivery_H().GetModel(id);
                    if (VoidCard(model)) success++;
                }
            }

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }

        //Add By Robin for Picking Order 2014-07-10
        public static string BatchPickOrder(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.CardOrderForm)
                {
                    Edge.SVA.Model.Ord_CardOrderForm_H model = new Edge.SVA.BLL.Ord_CardOrderForm_H().GetModel(id);
                    if (PickedCard(model)) success++;
                }
                else if (type == ApproveType.CardPicking)
                {
                    Edge.SVA.Model.Ord_CardPicking_H model = new Edge.SVA.BLL.Ord_CardPicking_H().GetModel(id);
                    if (PickedCard(model)) success++;
                }
            }

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success); //Todo modify MessageTips
        }
        //End

        //Modified By Robin 2015-02-28 增加了Amount和Point
        public static int AddPickupDetailCard(int userID, string CardPickingNumber,int CardTypeID ,int CardGradeID, int QTY,int Amount,int Point, int batchID, string CardUID, string StartCardNumber, int filterType)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@UserID", SqlDbType.Int) , 
                                            new SqlParameter("@CardPickingNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@FilterType", SqlDbType.Int), 
                                            new SqlParameter("@CardTypeID", SqlDbType.Int),
                                            new SqlParameter("@CardGradeID", SqlDbType.Int),
                                            new SqlParameter("@Qty", SqlDbType.Int),
                                            new SqlParameter("@Amount", SqlDbType.Int),
                                            new SqlParameter("@Point", SqlDbType.Int),
                                            new SqlParameter("@BatchID", SqlDbType.Int),
                                            new SqlParameter("@CardUID", SqlDbType.VarChar,512),
                                            new SqlParameter("@StartCardNumber", SqlDbType.VarChar,64)
                                          };
            parameters[0].Value = userID;
            parameters[1].Value = CardPickingNumber;
            parameters[2].Value = filterType;
            parameters[3].Value = CardTypeID;
            parameters[4].Value = CardGradeID;
            parameters[5].Value = QTY;
            parameters[6].Value = Amount;
            parameters[7].Value = Point;
            parameters[8].Value = batchID;
            parameters[9].Value = CardUID;
            parameters[10].Value = StartCardNumber;

            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("AddPickupDetail_Card", parameters, out count);
            return result;
        }

        public static string GetOrderCardGrade(string CardOrderFormNumber)
        {
            string strReturn = "";

            string query = "SELECT [CardGradeID] FROM [Ord_CardOrderForm_D] where [CardOrderFormNumber]='" + CardOrderFormNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CardGradeID"] + ",";
                }

                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }

            return strReturn;
        }

        public static long GetPickupOrderTotalQty(string CardOrderFormNumber)
        {
            long lonReturn = 0;

            string query = "SELECT sum(CardQty) as CardQty FROM [Ord_CardOrderForm_D] where [CardOrderFormNumber]='" + CardOrderFormNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                lonReturn = Tools.ConvertTool.ConverType<long>(ds.Tables[0].Rows[0]["CardQty"].ToString());

                return lonReturn;
            }
            return lonReturn;
        }

        public static long GetDeliveryOrderTotalQty(string CardPickingNumber)
        {
            long lonReturn = 0;

            string query = "SELECT sum(CardQty) as CardQty FROM [Ord_CardOrderForm_D] where CardOrderFormNumber=(select top 1 ReferenceNo from Ord_CardPicking_H where CardPickingNumber=(select top 1 ReferenceNo from Ord_CardDelivery_H where CardDeliveryNumber='" + CardPickingNumber.Trim() + "'))";
            //string query = "SELECT sum(o.CardQty) as CardQty FROM [Ord_CardOrderForm_D] o left join dbo.Ord_CardPicking_H p on o.CardOrderFormNumber=p.ReferenceNo where p.CardPickingNumber='" + CardPickingNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                lonReturn = Tools.ConvertTool.ConverType<long>(ds.Tables[0].Rows[0]["CardQty"].ToString());

                return lonReturn;
            }
            return lonReturn;
        }

        public static bool CanApprovePicked(Edge.SVA.Model.Ord_CardPicking_H model, out string msg)
        {
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            string allowType = webset.CardOrderPickingAllowSetting.Trim();

            string strSql = "SELECT CardGradeID,SUM(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CardPicking_D] where CardPickingNumber='" + model.CardPickingNumber + "' group by CardGradeID ";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
                //long OrderQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
                //long PickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
                foreach (DataRow row in dt.Rows)
                {
                    long OrderQTY = Tools.ConvertTool.ConverType<long>(row["OrderQTY"].ToString());
                    long PickQTY = Tools.ConvertTool.ConverType<long>(row["PickQTY"].ToString());

                    if (allowType == "1")
                    {
                        if (PickQTY == OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet1;
                            return false;
                        }
                    }
                    else if (allowType == "2")
                    {
                        if (PickQTY <= OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet2;
                            return false;
                        }
                    }
                    else if (allowType == "3")
                    {
                        if (PickQTY >= OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet3;
                            return false;
                        }
                    }
                    else
                    {
                        msg = "";
                        return true;
                    }
                }

                msg = "";
                return true;
            }
            else
            {
                msg = msg = Resources.MessageTips.OrderPickSet4;
                return false;
            }

        }


        public static bool IsMeetPickingByType(long OrderQTY, long PickQTY)
        {
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            string allowType = webset.CardOrderPickingAllowSetting.Trim();

            if (allowType == "1")
            {
                if (PickQTY == OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (allowType == "2")
            {
                if (PickQTY <= OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (allowType == "3")
            {
                if (PickQTY >= OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public static void GetApprovePickedTotal(string CardPickingNumber, out long totalOrderQTY, out long totalPickQTY, out long totalOrderAmount, out long totalPickAmount, out long totalOrderPoint, out long totalPickPoint)
        {
            //string strSql = "SELECT CardGradeID,SUM(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CardPicking_D] where CardPickingNumber='" + CardPickingNumber.Trim() + "' group by CardGradeID ";
            string strSql = "select case B.CardGradeID when null then A.CardGradeID else B.CardGradeID end as CardGradeID," +
            "case B.CardGradeID when null then A.OrderQTY else B.OrderQTY end as OrderQTY," +
            "case B.CardGradeID when null then A.PickQTY else B.PickQTY end as PickQTY, " +
            "case B.CardGradeID when null then A.OrderAmount else B.OrderAmount end as OrderAmount," +
            "case B.CardGradeID when null then A.PickAmount else B.PickAmount end as PickAmount, " +
            "case B.CardGradeID when null then A.OrderPoint else B.OrderPoint end as OrderPoint," +
            "case B.CardGradeID when null then A.PickPoint else B.PickPoint end as PickPoint " +
            "from (" +
            "SELECT D.CardGradeID, SUM(D.CardQty) as OrderQTY, 0 as PickQTY, SUM(D.OrderAmount) as OrderAmount, 0 as PickAmount, SUM(D.OrderPoint) as OrderPoint, 0 as PickPoint FROM [Ord_CardOrderForm_D] D " +
            "left join ord_Cardorderform_h H on H.CardOrderFormNumber = D.CardOrderFormNumber " +
            "where D.CardOrderFormNumber in (select ReferenceNo from Ord_CardPicking_H where CardPickingNumber='" + CardPickingNumber.Trim() + "' )" +
            "group by CardGradeID) A left join (" +
            "SELECT D.CardGradeID,SUM(D.OrderQTY) as OrderQTY,Sum(D.PickQTY) as PickQTY,SUM(D.OrderAmount) as OrderAmount,Sum(D.PickAmount) as PickAmount,SUM(D.OrderPoint) as OrderPoint,Sum(D.PickPoint) as PickPoint FROM [Ord_CardPicking_D] D " +
            "where CardPickingNumber='" + CardPickingNumber.Trim() + "'" +
            "group by CardGradeID) B on A.CardGradeID = B.CardGradeID";
            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
                totalOrderQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
                totalPickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
                totalOrderAmount = Convert.ToInt32(Tools.ConvertTool.ConverType<decimal>(dt.Compute("sum(OrderAmount)", "").ToString()));
                totalPickAmount = Convert.ToInt32(Tools.ConvertTool.ConverType<decimal>(dt.Compute("sum(PickAmount)", "").ToString()));
                totalOrderPoint = Convert.ToInt32(Tools.ConvertTool.ConverType<decimal>(dt.Compute("sum(OrderPoint)", "").ToString()));
                totalPickPoint = Convert.ToInt32(Tools.ConvertTool.ConverType<decimal>(dt.Compute("sum(PickPoint)", "").ToString()));
            }
            else
            {
                totalPickQTY = 0;
                totalOrderQTY = 0;
                totalPickAmount = 0;
                totalOrderAmount = 0;
                totalPickPoint = 0;
                totalOrderPoint = 0;
            }


        }

        public static void GetApproveDeliveryTotal(string CardDeliveryNumber, out long totalOrderQTY, out long totalPickQTY)
        {
            string strSql = "SELECT CardGradeID,MAX(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CardDelivery_D] where CardDeliveryNumber='" + CardDeliveryNumber.Trim() + "' group by CardGradeID ";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
                totalOrderQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
                totalPickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
            }
            else
            {
                totalPickQTY = 0;
                totalOrderQTY = 0;
            }


        }

        public static void GenOrderFormPrintPages(List<string> idList, System.Web.UI.HtmlControls.HtmlGenericControl printDiv)
        {
            foreach (string id in idList)
            {
                SVA.Model.Ord_CardPicking_H picking = new SVA.BLL.Ord_CardPicking_H().GetModelByOrderNumber(id);
                if (picking == null) continue;

                #region 构建打印列表
                Table printTable = new Table();
                printTable.CellPadding = 0;  //设置单元格内间距
                printTable.CellSpacing = 0;   //设置单元格之间的距离
                //printTable.BorderWidth = 0;
                printTable.Width = new Unit(100, UnitType.Percentage);
                printTable.CssClass = "msgtable";

                //表头
                TableHeaderRow headerRow = new TableHeaderRow();
                TableHeaderCell headerRowCell = new TableHeaderCell();
                headerRowCell.Text = "捡货列表";
                headerRowCell.ColumnSpan = 4;
                headerRowCell.HorizontalAlign = HorizontalAlign.Center;
                headerRow.Cells.Add(headerRowCell);
                printTable.Rows.Add(headerRow);

                //第一行
                TableRow row1 = new TableRow();
                TableCell row1Cell1 = new TableCell();
                row1Cell1.Text = "捡货单编号：";
                row1Cell1.HorizontalAlign = HorizontalAlign.Right;
                row1Cell1.Width = new Unit(15, UnitType.Percentage);
                row1.Cells.Add(row1Cell1);
                TableCell row1Cell2 = new TableCell();
                row1Cell2.Text = picking.CardPickingNumber;
                row1Cell2.Width = new Unit(35, UnitType.Percentage);
                row1.Cells.Add(row1Cell2);
                TableCell row1Cell3 = new TableCell();
                row1Cell3.Text = "打印时间：";
                row1Cell3.HorizontalAlign = HorizontalAlign.Right;
                row1Cell3.Width = new Unit(15, UnitType.Percentage);
                row1.Cells.Add(row1Cell3);
                TableCell row1Cell4 = new TableCell();
                row1Cell4.Text = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                row1.Cells.Add(row1Cell4);
                printTable.Rows.Add(row1);

                //第二行
                TableRow row2 = new TableRow();
                TableCell row2Cell1 = new TableCell();
                row2Cell1.Text = "参考编号：";
                row2Cell1.HorizontalAlign = HorizontalAlign.Right;
                row2.Cells.Add(row2Cell1);
                TableCell row2Cell2 = new TableCell();
                row2Cell2.Text = picking.ReferenceNo;
                row2.Cells.Add(row2Cell2);
                TableCell row2Cell3 = new TableCell();
                row2Cell3.Text = "状态：";
                row2Cell3.HorizontalAlign = HorizontalAlign.Right;
                row2.Cells.Add(row2Cell3);
                TableCell row2Cell4 = new TableCell();
                row2Cell4.Text = Tools.DALTool.GetOrderPickingApproveStatusString(picking.ApproveStatus);
                row2.Cells.Add(row2Cell4);
                printTable.Rows.Add(row2);

                //第三行
                TableRow row3 = new TableRow();
                TableCell row3Cell1 = new TableCell();
                row3Cell1.Text = "捡货日期：";
                row3Cell1.HorizontalAlign = HorizontalAlign.Right;
                row3.Cells.Add(row3Cell1);
                TableCell row3Cell2 = new TableCell();
                row3Cell2.Text = Tools.ConvertTool.ToStringDate(picking.ApproveOn.GetValueOrDefault());
                row3.Cells.Add(row3Cell2);
                TableCell row3Cell3 = new TableCell();
                row3Cell3.Text = "捡货人：";
                row3Cell3.HorizontalAlign = HorizontalAlign.Right;
                row3.Cells.Add(row3Cell3);
                TableCell row3Cell4 = new TableCell();
                row3Cell4.Text = Tools.DALTool.GetUserName(picking.ApproveBy.GetValueOrDefault());
                row3.Cells.Add(row3Cell4);
                printTable.Rows.Add(row3);


                #region Picking list table
                //构建表
                Table printPickingTable = new Table();
                printPickingTable.CellPadding = 0;  //设置单元格内间距
                printPickingTable.CellSpacing = 0;   //设置单元格之间的距离
                //printTable.BorderWidth = 0;
                printPickingTable.Width = new Unit(100, UnitType.Percentage);
                printPickingTable.CssClass = "msgtablelist";

                //表头
                TableHeaderRow headerPickingRow = new TableHeaderRow();
                TableHeaderCell headerPickingRowCell1 = new TableHeaderCell();
                headerPickingRowCell1.Text = "优惠劵类型编号";
                headerPickingRow.Cells.Add(headerPickingRowCell1);
                TableHeaderCell headerPickingRowCell2 = new TableHeaderCell();
                headerPickingRowCell2.Text = "优惠劵类型";
                headerPickingRow.Cells.Add(headerPickingRowCell2);
                TableHeaderCell headerPickingRowCell3 = new TableHeaderCell();
                headerPickingRowCell3.Text = "订单数量";
                headerPickingRow.Cells.Add(headerPickingRowCell3);
                TableHeaderCell headerPickingRowCell4 = new TableHeaderCell();
                headerPickingRowCell4.Text = "捡货数量";
                headerPickingRow.Cells.Add(headerPickingRowCell4);
                TableHeaderCell headerPickingRowCell5 = new TableHeaderCell();
                headerPickingRowCell5.Text = "优惠券起始编号";
                headerPickingRow.Cells.Add(headerPickingRowCell5);
                TableHeaderCell headerPickingRowCell6 = new TableHeaderCell();
                headerPickingRowCell6.Text = "优惠券结束编号";
                headerPickingRow.Cells.Add(headerPickingRowCell6);
                printPickingTable.Rows.Add(headerPickingRow);

                System.Data.DataSet ds = new SVA.BLL.Ord_CardOrderForm_D().GetList(string.Format("CardOrderFormNumber = '{0}'", id));
                Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    //第二行
                    TableRow printPickingRow = new TableRow();
                    TableCell printPickingRowCell1 = new TableCell();
                    printPickingRowCell1.Text = row["CardGradeCode"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell1);
                    TableCell printPickingRowCell2 = new TableCell();
                    printPickingRowCell2.Text = row["CardGrade"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell2);
                    TableCell printPickingRowCell3 = new TableCell();
                    printPickingRowCell3.Text = row["CardQty"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell3);
                    TableCell printPickingRowCell4 = new TableCell();
                    printPickingRowCell4.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell4);
                    TableCell printPickingRowCell5 = new TableCell();
                    printPickingRowCell5.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell5);
                    TableCell printPickingRowCell6 = new TableCell();
                    printPickingRowCell6.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell6);
                    printPickingTable.Rows.Add(printPickingRow);
                }

                #endregion

                //第四行
                TableRow row4 = new TableRow();
                TableCell row4Cell = new TableCell();
                row4Cell.ColumnSpan = 4;
                row4Cell.Controls.Add(printPickingTable);
                row4.Cells.Add(row4Cell);
                printTable.Rows.Add(row4);

                printDiv.Controls.Add(printTable);
                Panel p = new Panel();
                p.Style.Add("padding-bottom", "10px");
                printDiv.Controls.Add(p);
                #endregion
            }
        }

        public enum ApproveType
        {
            CardOrderForm,
            CardPicking,
            CardDelivery

        }

        public static string GetOrderCardType(string CardOrderFormNumber)
        {
            string strReturn = "";

            string query = "SELECT [CardTypeID] FROM [Ord_CardOrderForm_D] where [CardOrderFormNumber]='" + CardOrderFormNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CardTypeID"] + ",";
                }

                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }

            return strReturn;
        }

        public static class RefnoCode
        {
            public static string OrderCardPickup = "CPO";
            public static string OrderCardDelivery = "CDO";
            public static string OrderCardForm = "COF";

        }

    }
}