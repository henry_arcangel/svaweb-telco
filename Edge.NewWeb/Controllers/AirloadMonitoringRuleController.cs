﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model;

namespace Edge.Web.Controllers
{
    public class AirloadMonitoringRuleController
    {
        protected InventoryReplenishRuleListViewModel viewModel = new InventoryReplenishRuleListViewModel();
        public InventoryReplenishRuleListViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }

        private const string fields = "InventoryReplenishCode,Description,StartDate,EndDate,Status,MediaType,BrandID,CouponTypeID,CardTypeID,CardGradeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy";

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            //add by gavin @2015-02-26
            if (strWhere.Length > 0)
                strWhere = strWhere + " and PurchaseType = 2";
            else strWhere = " PurchaseType = 2";

            Edge.SVA.BLL.InventoryReplenishRule_H bll = new Edge.SVA.BLL.InventoryReplenishRule_H()
            {
                StrWhere = strWhere,
                Order = "InventoryReplenishCode",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;

            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddBrandName(ds, "Brand", "BrandID");
            Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
            Tools.DataTool.AddCouponReplenishStatus(ds, "StatusName", "Status");
            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");


            return ds;
        }

        public bool Exists(InventoryReplenishRuleViewModel model) 
        {
            foreach (InventoryReplenishRuleViewModel mdl in viewModel.InventoryReplenishRuleViewModelList) 
            {
                if (mdl.MainTable.OrderTargetID == model.MainTable.OrderTargetID && mdl.MainTable.StoreID == model.MainTable.StoreID) 
                {
                    return true;
                }
            }
            return false;
        }

        public void Add(InventoryReplenishRuleViewModel model) 
        {
            viewModel.InventoryReplenishRuleViewModelList.Add(model);
        }

        public SVA.Model.InventoryReplenishRule_H GetModel(string id) 
        {
            Edge.SVA.BLL.InventoryReplenishRule_H bll = new SVA.BLL.InventoryReplenishRule_H();
            return bll.GetModel(id);
        }

        //modify by gavin @2015-02-26
        public void GetDetailList(string id, int StoreTypeID)
        {           
            string sql = "";
            string descLan = DALTool.GetStringByCulture("1", "2", "3");
            string supplierDesc = "SupplierDesc" + descLan;
            string storeName = "StoreName" + descLan;
            if (StoreTypeID == 1)
            {
                sql = "select a.KeyID,a.InventoryReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + supplierDesc + " OrderTargetName,c." + storeName + " StoreName, a.ReplenishType, a.MinAmtBalance, a.RunningAmtBalance, a.MinPointBalance, a.RunningPointBalance from InventoryReplenishRule_D a,Supplier b,Store c where a.OrderTargetID = b.SupplierID and a.StoreID = c.StoreID and a.InventoryReplenishCode='" + id + "' order by a.KeyID";
            }
            else
            {
                sql = "select a.KeyID,a.InventoryReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + storeName + " OrderTargetName,c." + storeName + " StoreName, a.ReplenishType, a.MinAmtBalance, a.RunningAmtBalance, a.MinPointBalance, a.RunningPointBalance from InventoryReplenishRule_D a,Store b,Store c where a.OrderTargetID = b.StoreID and a.StoreID = c.StoreID and a.InventoryReplenishCode='" + id + "' order by a.KeyID";
            }
            DataTable dt = DBUtility.DbHelperSQL.Query(sql).Tables[0];
            foreach (DataRow dr in dt.Rows) 
            {
                InventoryReplenishRuleViewModel model = new InventoryReplenishRuleViewModel();
                model.MainTable.KeyID = StringHelper.ConvertToInt(Convert.ToString(dr["KeyID"]));
                model.MainTable.InventoryReplenishCode = Convert.ToString(dr["InventoryReplenishCode"]);
                model.MainTable.StoreID = StringHelper.ConvertToInt(Convert.ToString(dr["StoreID"]));
                model.MainTable.OrderTargetID = StringHelper.ConvertToInt(Convert.ToString(dr["OrderTargetID"]));
                model.MainTable.MinStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["MinStockQty"]));
                model.MainTable.RunningStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["RunningStockQty"]));
                model.MainTable.OrderRoundUpQty = StringHelper.ConvertToInt(Convert.ToString(dr["OrderRoundUpQty"]));
                model.MainTable.Priority = StringHelper.ConvertToInt(Convert.ToString(dr["Priority"]));
                model.MainTable.ReplenishType = StringHelper.ConvertToInt(Convert.ToString(dr["ReplenishType"]));
                model.MainTable.MinAmtBalance = Convert.ToDecimal(dr["MinAmtBalance"]);
                model.MainTable.RunningAmtBalance = Convert.ToDecimal(dr["RunningAmtBalance"]);
                model.MainTable.MinPointBalance = StringHelper.ConvertToInt(Convert.ToString(dr["MinPointBalance"]));
                model.MainTable.RunningPointBalance = StringHelper.ConvertToInt(Convert.ToString(dr["RunningPointBalance"]));
                model.StoreName = Convert.ToString(dr["StoreName"]);
                model.OrderTargetName = Convert.ToString(dr["OrderTargetName"]);
                this.viewModel.InventoryReplenishRuleViewModelList.Add(model);
            }
        }

        //modify by gavin @2015-02-26
        //Add By Robin 2014-08-18 取所有ReplenishRule的店铺 for 过滤已经加过的店铺
        public void GetAllDetailList(int StoreTypeID, int typeOrGradeID, int mediaType)
        {
            string typeOrGrade = "";
            if (mediaType == 1)
            {
                typeOrGrade = "CouponTypeID";
            }
            else
            {
                typeOrGrade = "CardGradeID";
            }
            string sql = "";
            string descLan = DALTool.GetStringByCulture("1", "2", "3");
            string supplierDesc = "SupplierDesc" + descLan;
            string storeName = "StoreName" + descLan;
            if (StoreTypeID == 1)
            {
                sql = "select a.KeyID,a.InventoryReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + supplierDesc + " OrderTargetName,c." + storeName + " StoreName, a.ReplenishType, a.MinAmtBalance, a.RunningAmtBalance, a.MinPointBalance, a.RunningPointBalance from InventoryReplenishRule_D a inner join InventoryReplenishRule_H a1 on a.InventoryReplenishCode=a1.InventoryReplenishCode,Supplier b,Store c where a1.PurchaseType=2 and a.OrderTargetID = b.SupplierID and a.StoreID = c.StoreID and a1.Status=1 and a1." + typeOrGrade + "=" + typeOrGradeID + " order by a.KeyID";
            }
            else
            {
                sql = "select a.KeyID,a.InventoryReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + storeName + " OrderTargetName,c." + storeName + " StoreName, a.ReplenishType, a.MinAmtBalance, a.RunningAmtBalance, a.MinPointBalance, a.RunningPointBalance from InventoryReplenishRule_D a inner join InventoryReplenishRule_H a1 on a.InventoryReplenishCode=a1.InventoryReplenishCode,Store b,Store c where a1.PurchaseType=2 and a.OrderTargetID = b.StoreID and a.StoreID = c.StoreID and a1.Status=1 and a1." + typeOrGrade + "=" + typeOrGradeID + " order by a.KeyID";
            }
            DataTable dt = DBUtility.DbHelperSQL.Query(sql).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                InventoryReplenishRuleViewModel model = new InventoryReplenishRuleViewModel();
                model.MainTable.KeyID = StringHelper.ConvertToInt(Convert.ToString(dr["KeyID"]));
                model.MainTable.InventoryReplenishCode = Convert.ToString(dr["InventoryReplenishCode"]);
                model.MainTable.StoreID = StringHelper.ConvertToInt(Convert.ToString(dr["StoreID"]));
                model.MainTable.OrderTargetID = StringHelper.ConvertToInt(Convert.ToString(dr["OrderTargetID"]));
                model.MainTable.MinStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["MinStockQty"]));
                model.MainTable.RunningStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["RunningStockQty"]));
                model.MainTable.OrderRoundUpQty = StringHelper.ConvertToInt(Convert.ToString(dr["OrderRoundUpQty"]));
                model.MainTable.Priority = StringHelper.ConvertToInt(Convert.ToString(dr["Priority"]));
                model.MainTable.ReplenishType = StringHelper.ConvertToInt(Convert.ToString(dr["ReplenishType"]));
                model.MainTable.MinAmtBalance = Convert.ToDecimal(dr["MinAmtBalance"]);
                model.MainTable.RunningAmtBalance = Convert.ToDecimal(dr["RunningAmtBalance"]);
                model.MainTable.MinPointBalance = StringHelper.ConvertToInt(Convert.ToString(dr["MinPointBalance"]));
                model.MainTable.RunningPointBalance = StringHelper.ConvertToInt(Convert.ToString(dr["RunningPointBalance"]));
                model.StoreName = Convert.ToString(dr["StoreName"]);
                model.OrderTargetName = Convert.ToString(dr["OrderTargetName"]);
                this.viewModel.InventoryReplenishRuleViewModelList.Add(model);
            }
        }

        public void RunAutoGenOrder_New(string InventoryReplenishCode) 
        {
            IDataParameter[] iData = new SqlParameter[5];
            iData[0] = new SqlParameter("@UserID", SqlDbType.Int);
            iData[0].Value = DALTool.GetCurrentUser().UserID;
            iData[1] = new SqlParameter("@InventoryReplenishCode", SqlDbType.VarChar,64);
            iData[1].Value = InventoryReplenishCode;
            iData[2] = new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64);
            iData[2].Value = null;
            iData[3] = new SqlParameter("@BusDate", SqlDbType.DateTime);
            iData[3].Value = null;
            iData[4] = new SqlParameter("@TxnDate", SqlDbType.DateTime);
            iData[4].Value = null;
            int result;
            DBUtility.DbHelperSQL.RunProcedure("AutoTopUpTelcoCard", iData, out result);
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": FineUI.Alert.ShowInTop("Finish to execute!"); ; break;
                case "zh-cn": FineUI.Alert.ShowInTop("运行完成！"); ; break;
                case "zh-hk": FineUI.Alert.ShowInTop("運行完成！"); break;
            }
        }
    }
}