﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.WalletRule
{
    public class WalletRuleController
    {
        private const string fields = " [WalletRuleCode],[Description],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy]";

        Edge.SVA.BLL.BLLCRUD crud = new Edge.SVA.BLL.BLLCRUD();

        private string lan = "zh-cn";

        public string Lan
        {
            get { return lan; }
            set { lan = value; }
        }

        public DataSet GetTransferList(string strWhere, string sortfield)
        {
            string sql = @"select A.WalletRuleCode, A.Description, A.BrandID, A.CardTypeID, A.CardGradeID, " 
                  +  "B.BrandCode as Brand, C.CardTypeCode as CardType, D.CardGradeCode as CardGrade"  
                  +  " from(  select D.WalletRuleCode, max(H.Description) as Description, D.BrandID, D.CardTypeID, D.CardGradeID"
                  +  "   from walletrule_D D "
                  +  "    left join walletRule_h H on D.WalletRuleCode = H.WalletRuleCode"
                  +  "   group by D.WalletRuleCode, D.BrandID, D.CardTypeID, D.CardGradeID" 
               +  " ) A left join Brand B on A.BrandID = B.BrandID"
               +  " left join CardType C on A.CardTypeID = C.CardTypeID"
                +  " left join CardGrade D on A.CardGradeID = D.CardGradeID ";
            sql =  sql + strWhere.ToString();
            if (!string.IsNullOrEmpty(sortfield))
            {
                sql = sql + " order by A." + sortfield;
            }
            DataSet ds = crud.Query(sql);
            return ds;
        }

    }
}