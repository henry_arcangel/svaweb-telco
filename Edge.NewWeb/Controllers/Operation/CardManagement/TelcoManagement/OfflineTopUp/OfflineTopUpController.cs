﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.OfflineTopUp
{
    public class OfflineTopUpController
    {
        protected CardAdjustViewModel viewModel = new CardAdjustViewModel();

        public CardAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void ShowCouponList(DataTable dt)
        {
            viewModel.CardTable = dt;
        }

        public void AddCardList(List<string> list)
        {
            CardRepostory res = CardRepostory.Singleton;
            foreach (var item in list)
            {
                KeyValue exist = this.viewModel.CardList.Find(m => m.Key == item);
                if (exist == null)
                {
                    KeyValue kv = new KeyValue();
                    kv.Key = item;
                    Edge.SVA.Model.Card cg = res.GetModelByID(ConvertTool.ConverType<int>(item));
                    switch (SVASessionInfo.SiteLanguage)
                    {
                        case LanguageFlag.ZHCN:
                            kv.Value = cg.CardNumber;
                            break;
                        case LanguageFlag.ZHHK:
                            kv.Value = cg.CardNumber;
                            break;
                        case LanguageFlag.ENUS:
                        default:
                            kv.Value = cg.CardNumber;
                            break;
                    }
                    this.viewModel.CardList.Add(kv);
                }
            }
        }
        public void RemoveCouponType(KeyValue model)
        {
            if (this.viewModel.CardList.Contains(model))
            {
                this.viewModel.CardList.Remove(model);
            }
        }
        public void AddCardBatchCodeList(KeyValue model)
        {
            BatchCard bc = new BatchCard();
            model.Key = bc.BatchCardID.ToString();
            model.Value = bc.BatchCardCode;
            viewModel.BatchCardIDList.Add(model);
        }
        private bool ExistSameKeyInKeyValueList(KeyValue model, List<KeyValue> list)
        {
            bool rtn = false;
            if (model == null)
            {
                return true;
            }
            KeyValue mo = list.Find(m => m.Key == model.Key);
            if (mo != null)
            {
                return true;
            }
            return rtn;
        }
    }
}