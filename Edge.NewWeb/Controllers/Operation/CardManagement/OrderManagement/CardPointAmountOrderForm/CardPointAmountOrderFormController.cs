﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using System.Data.SqlClient;
using Edge.DBUtility;
using System.Text;

namespace Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardPointAmountOrderForm
{
    public class CardPointAmountOrderFormController
    {
        private const string fields = "CardOrderFormNumber,ApproveStatus,OrderType,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";
        private const string condition = " StoreID in {0} and FromStoreID in {0} ";
        private const string andCondition = " and StoreID in {0} and FromStoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores) + " and PurchaseType=2 ";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1" + " and PurchaseType=2 ";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores) + " and PurchaseType=2 ";
                }
            }
            Edge.SVA.BLL.Ord_CardOrderForm_H bll = new Edge.SVA.BLL.Ord_CardOrderForm_H()
            {
                StrWhere = strWhere,
                //Order = "CardOrderFormNumber", //Modified By Robin 2014-07-17 for order field
                Order = "CreatedOn",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
                ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            this.AddOrderAmount(ds, "OrderAmount", "CardOrderFormNumber");

            return ds;
        }

        public void AddOrderAmount(DataSet ds, string name, string refKey)
        {
            string sqlstr = "select isnull(sum(orderamount),0) OrderAmount from ord_cardorderform_d where ";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DataSet ds1 = DBUtility.DbHelperSQL.Query(sqlstr+" CardOrderFormNumber='" + row[refKey].ToString() + "'");
                if (ds1.Tables.Count > 0)
                {
                    row[name] = Convert.ToDecimal(ds1.Tables[0].Rows[0]["OrderAmount"]).ToString("F2");
                }
                
            }
        }
    }
}