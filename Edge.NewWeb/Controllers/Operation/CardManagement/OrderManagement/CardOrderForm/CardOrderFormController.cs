﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardOrderForm
{
    public class CardOrderFormController
    {
        private const string fields = "CardOrderFormNumber,ApproveStatus,OrderType,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";
        private const string condition = " StoreID in {0} and FromStoreID in {0} ";
        private const string andCondition = " and StoreID in {0} and FromStoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores) + " and PurchaseType=1 ";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1" + " and PurchaseType=1 ";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores) + " and PurchaseType=1 ";
                }
            }
            Edge.SVA.BLL.Ord_CardOrderForm_H bll = new Edge.SVA.BLL.Ord_CardOrderForm_H()
            {
                StrWhere = strWhere,
                //Order = "CardOrderFormNumber", //Modified By Robin 2014-07-17 for order field
                Order = "CreatedOn",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
                ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }
    }
}