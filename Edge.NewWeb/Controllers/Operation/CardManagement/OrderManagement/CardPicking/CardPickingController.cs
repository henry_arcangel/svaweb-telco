﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardPicking
{
    public class CardPickingController
    {
        private const string fields = "CardPickingNumber,ReferenceNo,ApproveStatus,OrderType,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy,UpdatedOn,UpdatedBy";
        private const string condition = " StoreID in {0} ";
        private const string andCondition = " and StoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores) + " and PurchaseType=1 ";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1" + " and PurchaseType=1 ";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores) + " and PurchaseType=1 ";
                }
            }
            Edge.SVA.BLL.Ord_CardPicking_H bll = new Edge.SVA.BLL.Ord_CardPicking_H()
            {
                StrWhere = strWhere,
                Order = "CardPickingNumber",
                Fields = fields,
                Ascending = false
            };

            DataSet ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            Tools.DataTool.AddOrderPickingApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(string id)
        {
            string sql = " select a.KeyID,a.CardPickingNumber,a.CardTypeID,a.PickQTY,a.FirstCardNumber,a.EndCardNumber,b.CardNumPattern,c.CardAmount from Ord_CardPicking_D a,CardType b,Card c where a.CardTypeID = b.CardTypeID and a.CardTypeID = c.CardTypeID and a.FirstCardNumber = c.CardNumber and a.CardPickingNumber = '" + id + "' order by a.KeyID";
            return DBUtility.DbHelperSQL.Query(sql);
        }

        //Added By Robin 2014-08-06 for RRG group Card number
        public DataSet GetDetailListGroup(string id)
        {
            //string sql = " select a.KeyID,a.CardPickingNumber,a.CardTypeID,a.PickQTY,a.FirstCardNumber,a.EndCardNumber,b.CardNumPattern,c.CardAmount from Ord_CardPicking_D a,CardType b,Card c where a.CardTypeID = b.CardTypeID and a.CardTypeID = c.CardTypeID and a.FirstCardNumber = c.CardNumber and a.CardPickingNumber = '" + id + "' order by a.KeyID";
            StringBuilder sql = new StringBuilder();
            string tblUser;
            tblUser = "_" + Tools.DALTool.GetCurrentUser().UserID.ToString();
            sql.Append("if object_id('tempdb..#O" + tblUser + "') is not null Begin drop table #O" + tblUser + " End ");
            sql.Append("if object_id('tempdb..#E" + tblUser + "') is not null Begin drop table #E" + tblUser + " End ");
            sql.Append("if object_id('tempdb..#F" + tblUser + "') is not null Begin drop table #F" + tblUser + " End ");
            sql.Append("select a.KeyID,a.CardPickingNumber,a.CardGradeID,a.PickQTY,a.FirstCardNumber,a.EndCardNumber,b.CardNumPattern,c.TotalAmount CardAmount into #O" + tblUser + " from Ord_CardPicking_D a,CardGrade b,Card c where a.CardGradeID = b.CardGradeID and a.CardGradeID = c.CardGradeID and a.FirstCardNumber = c.CardNumber and a.CardPickingNumber =  '" + id + "' order by a.KeyID ");
            sql.Append("SELECT identity(int, 1,1) id, T2.FirstCardNumber into #F" + tblUser + " FROM #O" + tblUser + " T2 WHERE NOT EXISTS (SELECT FirstCardNumber  FROM #O" + tblUser + " A WHERE cast(A.FirstCardNumber as bigint)+1=cast(T2.FirstCardNumber as bigint)) order by T2.FirstCardNumber ");
            sql.Append("SELECT identity(int, 1,1) id,T2.EndCardNumber into #E" + tblUser + " FROM #O" + tblUser + " T2 WHERE NOT EXISTS (SELECT EndCardNumber  FROM #O" + tblUser + " A WHERE cast(A.EndCardNumber as bigint)=cast(T2.EndCardNumber as bigint)+1) order by T2.EndCardNumber ");
            //sql.Append("select KeyID,CardPickingNumber,CardTypeID,T.PickQTY,replace(T.FirstCardNumber,CardNumPattern,'') FirstCardNumber,replace(T.EndCardNumber,CardNumPattern,'') EndCardNumber,CardNumPattern,CardAmount*T.PickQTY CardAmount from #O" + tblUser + " inner join (select FirstCardNumber,EndCardNumber, cast(EndCardNumber as bigint)-cast(FirstCardNumber as bigint)+1 PickQTY from #E" + tblUser + " right join #F" + tblUser + " on #E" + tblUser + ".id=#F" + tblUser + ".id) T on #O" + tblUser + ".FirstCardNumber=T.FirstCardNumber");
            sql.Append("select KeyID,CardPickingNumber,CardGradeID,T.PickQTY,replace(T.FirstCardNumber,CardNumPattern,'') FirstCardNumber,replace(T.EndCardNumber,CardNumPattern,'') EndCardNumber,CardNumPattern,(select SUM(CardAmount) from #O_1 where #O_1.FirstCardNumber between  T.FirstCardNumber and T.EndCardNumber) CardAmount from #O" + tblUser + " inner join (select FirstCardNumber,EndCardNumber, cast(EndCardNumber as bigint)-cast(FirstCardNumber as bigint)+1 PickQTY from #E" + tblUser + " right join #F" + tblUser + " on #E" + tblUser + ".id=#F" + tblUser + ".id) T on #O" + tblUser + ".FirstCardNumber=T.FirstCardNumber");
           
            return DBUtility.DbHelperSQL.Query(sql.ToString());
            ////Here is SQL Robin 2014-09-02
            //if object_id('tempdb..#O_1') is not null Begin drop table #O_1 End 
            //if object_id('tempdb..#E_1') is not null Begin drop table #E_1 End 
            //if object_id('tempdb..#F_1') is not null Begin drop table #F_1 End 
            //select a.KeyID,a.CardPickingNumber,a.CardTypeID,a.PickQTY,a.FirstCardNumber,a.EndCardNumber,b.CardNumPattern,c.CardAmount into #O_1 from Ord_CardPicking_D a,CardType b,Card c where a.CardTypeID = b.CardTypeID and a.CardTypeID = c.CardTypeID and a.FirstCardNumber = c.CardNumber and a.CardPickingNumber =  'COPO00000000380' order by a.KeyID 
            //select * from #O_1
            //SELECT identity(int, 1,1) id, T2.FirstCardNumber into #F_1 FROM #O_1 T2 WHERE NOT EXISTS (SELECT FirstCardNumber  FROM #O_1 A WHERE cast(A.FirstCardNumber as bigint)+1=cast(T2.FirstCardNumber as bigint)) order by T2.FirstCardNumber
            //select * from #F_1
            //SELECT identity(int, 1,1) id,T2.EndCardNumber into #E_1 FROM #O_1 T2 WHERE NOT EXISTS (SELECT EndCardNumber  FROM #O_1 A WHERE cast(A.EndCardNumber as bigint)=cast(T2.EndCardNumber as bigint)+1) order by T2.EndCardNumber
            //select * from #E_1
            //select KeyID,CardPickingNumber,CardTypeID,T.PickQTY,replace(T.FirstCardNumber,CardNumPattern,'') FirstCardNumber,replace(T.EndCardNumber,CardNumPattern,'') EndCardNumber,CardNumPattern,CardAmount*T.PickQTY CardAmount 
            //from #O_1 inner join (select FirstCardNumber,EndCardNumber, cast(EndCardNumber as bigint)-cast(FirstCardNumber as bigint)+1 PickQTY from #E_1 right join #F_1 on #E_1.id=#F_1.id) T on #O_1.FirstCardNumber=T.FirstCardNumber
        }
        //End

        //Add By Robin 2014-07-24
        public string GetFirstCardNumber(string CardPickingNumber)
        {
            string sql = " select min(a.FirstCardNumber) CardNumber from Ord_CardPicking_D a where a.CardPickingNumber = '" + CardPickingNumber + "' group by CardPickingNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string FirstCardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                return FirstCardNumber;
            }
            else
            {
                return "";
            }
        }

        public string GetEndCardNumber(string CardPickingNumber)
        {
            string sql = " select max(a.EndCardNumber) CardNumber from Ord_CardPicking_D a where a.CardPickingNumber = '" + CardPickingNumber + "' group by CardPickingNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string EndCardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                return EndCardNumber;
            }
            else
            {
                return "";
            }
        }
        //End

        //Add By Robin 2014-09-02
        public DataSet GetCardGradeList(string CardPickingNumber)
        {
            string sql = "select CardGradeID, case CardAmount when 0 then CardAmount1 else CardAmount end as CardAmount from (select D.CardGradeID, SUM(PickQty)*max(CT.CardTypeInitAmount) CardAmount, SUM(C.TotalAmount) CardAmount1 from Ord_CardPicking_D D left join CardGrade CT on D.CardGradeID=CT.CardGradeID left join Card C on D.FirstCardNumber=C.CardNumber where CardPickingNumber = '" + CardPickingNumber + "' group by D.CardGradeID) temp";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }
        //End
    }
}