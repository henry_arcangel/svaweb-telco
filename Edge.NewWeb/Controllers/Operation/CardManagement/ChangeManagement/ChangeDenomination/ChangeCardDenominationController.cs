﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;
using System.Text;
using Edge.DBUtility;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination
{
    public class ChangeCardDenominationController
    {
        protected CardAdjustViewModel viewModel = new CardAdjustViewModel();

        public CardAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataTable GetCardSearchTable(int Top, string strWhere, string filedOrder)
        {
            DataTable dt = new DataTable();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append("CardType.CardTypeID,CardGrade.CardGradeID,Card.CardNumber,CardUIDMap.CardUID,Card.Status,");
            strSql.Append("Member.CountryCode,Member.MemberMobilePhone,Card.TotalPoints,Card.TotalAmount");
            strSql.Append(" from Card ");
            strSql.Append(" left join Member  on Card.MemberID=Member.MemberID");
            strSql.Append(" left join CardUIDMap on Card.CardNumber=CardUIDMap.CardNumber ");
            strSql.Append(" left join CardGrade on CardUIDMap.CardGradeID=CardGrade.CardGradeID ");
            strSql.Append(" left join CardType  on CardGrade.CardTypeID=CardType.CardTypeID ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);

            DataSet ds = DBUtility.DbHelperSQL.Query(strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
                dt.Columns.Add("OrderPoints", typeof(int));
                dt.Columns.Add("OrderAmount",typeof(decimal));
                dt.Columns.Add("AdjustPoints");
                dt.Columns.Add("AdjustAmount");
            }
            return dt;
        }

        public int GetCountWithCard_Movement(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Card_Movement ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }

        public DataSet GetPageListWithCard_Movement(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OpenBal,Amount as OrderAmount,CloseBal,OpenPoint,Points as OrderPoints,ClosePoint,");
            strSql.Append("CardType.CardTypeID,CardGrade.CardGradeID,Card.CardNumber,CardUIDMap.CardUID,Card.Status,");
            strSql.Append("Member.CountryCode,Member.MemberMobilePhone,Card.TotalPoints,Card.TotalAmount,Member.MemberID");
            strSql.Append(" from dbo.Card_Movement");
            strSql.Append(" left join card on Card_Movement.CardNumber = Card.CardNumber");
            strSql.Append(" left join Member  on Card.MemberID=Member.MemberID ");
            strSql.Append(" left join CardUIDMap on Card.CardNumber=CardUIDMap.CardNumber");
            strSql.Append(" left join CardGrade on CardUIDMap.CardGradeID=CardGrade.CardGradeID");
            strSql.Append(" left join CardType  on CardGrade.CardTypeID=CardType.CardTypeID");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);

            DataSet ds = DBUtility.DbHelperSQL.Query(strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
                Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
                Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
                //dt.Columns.Add("OrderPoints");
                //dt.Columns.Add("OrderAmount");
                dt.Columns.Add("AdjustPoints");
                dt.Columns.Add("AdjustAmount");
            }
            return ds;
        }

        public int GetCountWithCard(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }

        public DataSet GetPageListWithCard(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CardType.CardTypeID,CardGrade.CardGradeID,Card.CardNumber,CardUIDMap.CardUID,Card.Status,Member.CountryCode,");
            strSql.Append("Member.MemberMobilePhone,Card.TotalPoints,Card.TotalAmount,Member.MemberID,Ord_CardAdjust_D.OrderAmount,Ord_CardAdjust_D.OrderPoints");
            strSql.Append(" from Card");
            strSql.Append(" left join Ord_CardAdjust_D  on Ord_CardAdjust_D.CardNumber=Card.CardNumber");
            strSql.Append(" left join Member  on Card.MemberID=Member.MemberID");
            strSql.Append(" left join CardUIDMap on Card.CardNumber=CardUIDMap.CardNumber");
            strSql.Append(" left join CardGrade on CardUIDMap.CardGradeID=CardGrade.CardGradeID");
            strSql.Append(" left join CardType  on CardGrade.CardTypeID=CardType.CardTypeID");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);

            DataSet ds = DBUtility.DbHelperSQL.Query(strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
                Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
                Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
                //dt.Columns.Add("OrderPoints");
                //dt.Columns.Add("OrderAmount");
                dt.Columns.Add("AdjustPoints");
                dt.Columns.Add("AdjustAmount");
            }
            return ds;
        }

        public int GetTotalPonitsWithOrd_CardAdjust_D(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Ord_CardAdjust_D.OrderPoints) as OrderPoints ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["OrderPoints"];

                if (result == DBNull.Value) return 0;

                int denomination = 0;

                return Int32.TryParse(result.ToString(), out denomination) ? denomination : 0;
            }
        }
        public decimal GetTotalAmountWithOrd_CardAdjust_D(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Ord_CardAdjust_D.OrderAmount) as OrderAmount ");
            strSql.Append(" FROM Ord_CardAdjust_D left join Card on Ord_CardAdjust_D.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["OrderAmount"];

                if (result == DBNull.Value) return 0.0m;

                decimal denomination = 0.0m;

                return decimal.TryParse(result.ToString(), out denomination) ? denomination : 0.0m;
            }
        }

        public int GetTotalPonitsWithOrd_CardMovent(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Card_Movement.Points) as OrderPoints ");//Card_Movement.ClosePoint
            strSql.Append(" FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["OrderPoints"];

                if (result == DBNull.Value) return 0;

                int denomination = 0;

                return Int32.TryParse(result.ToString(), out denomination) ? denomination : 0;
            }
        }
        public decimal GetTotalAmountWithOrd_CardMovent(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select sum(Card_Movement.Amount) as OrderAmount ");//Card_Movement.CloseBal
            strSql.Append(" FROM Card_Movement left join Card on Card_Movement.CardNumber=Card.CardNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(strSql.ToString()))
            {
                if (!reader.Read()) throw new Exception("No Data");

                object result = reader["OrderAmount"];

                if (result == DBNull.Value) return 0.0m;

                decimal denomination = 0.0m;

                return decimal.TryParse(result.ToString(), out denomination) ? denomination : 0.0m;
            }
        }

        public string ApproveCardForApproveCode(Edge.SVA.Model.Ord_CardAdjust_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardAdjust_H bll = new Edge.SVA.BLL.Ord_CardAdjust_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(model.CardAdjustNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }

        private bool VoidCard(Edge.SVA.Model.Ord_CardAdjust_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardAdjust_H bll = new Edge.SVA.BLL.Ord_CardAdjust_H();

            return bll.Update(model);
        }

        public string BatchVoidCard(List<string> idList)
        {
            int success = 0;
            int count = 0;
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                Edge.SVA.Model.Ord_CardAdjust_H model = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(id);
                if (this.VoidCard(model)) success++;
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
    }
}