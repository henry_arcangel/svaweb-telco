﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardTransfer
{
    public class CardTransferController
    {
        private const string fields = " [CardTransferNumber],[ApprovalCode],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate]";

        Edge.SVA.BLL.BLLCRUD crud = new Edge.SVA.BLL.BLLCRUD();

        CardTransferViewModel viewModel = new CardTransferViewModel();

        public CardTransferViewModel ViewModel
        {
            get { return viewModel; }
            set { viewModel = value; }
        }

        private string lan = "zh-cn";

        public string Lan
        {
            get { return lan; }
            set { lan = value; }
        }
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            if (strWhere.Length > 0)
                strWhere = strWhere + " and OrderType = 0 ";
            else
                strWhere = " OrderType = 0 ";
            string OrderField = "CardTransferNumber";
            bool OrderType = false;
            if (sortFieldStr.ToLower().EndsWith(" asc"))
            {
                OrderType = true;
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" asc"));
            }
            else if (sortFieldStr.ToLower().EndsWith(" desc"))
            {
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" desc"));
            }
            Edge.SVA.BLL.Ord_CardTransfer_H bll = new Edge.SVA.BLL.Ord_CardTransfer_H()
                {
                    StrWhere = strWhere,
                    Order = OrderField,//"CouponCreateNumber",
                    Fields = fields,
                    Ascending = OrderType//false
                }
                ;
            //recodeCount = bll.GetCount(strWhere);
            //System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, strWhere, OrderField);
            System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, out recodeCount);
            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            }
            return ds;
        }
        public DataSet GetTransferList(Dictionary<string, string> dic)
        {
            StringBuilder sb = new StringBuilder(" where 1=1 ");
            foreach (var item in dic.Keys)
            {
                sb.Append(" and " + item + " like '%" + dic[item] + "%'");
            }
            string sql = @"select [CardTransferNumber],[TxnDate],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate],[ApprovalCode]
from Ord_CardTransfer_H " + sb.ToString();
            DataSet ds = crud.Query(sql);
            Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            return ds;
        }
        public DataSet GetFromList(Dictionary<string, string> dic)
        {
            StringBuilder sb = new StringBuilder(" where 1=1 ");
            foreach (var item in dic.Keys)
            {
                sb.Append(" and " + item + " like '%" + dic[item] + "%'");
            }
            string sql = @"select top 50 m.MemberMobilePhone as MobileNO,m.MemberEmail Email,c.CardNumber,c.TotalPoints 
,c.TotalAmount,c.Status,c.CreatedOn,c.CardExpiryDate,c.CardGradeID
from card c
left join Member m on m.MemberID=c.MemberID" + sb.ToString() + " and not exists(select 1 from Ord_CardTransferFrom t where t.cardnumber=c.cardnumber)";
            DataSet ds = crud.Query(sql);
            Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            return ds;
        }
        public DataSet GetToList(string cardGradeID)
        {
            string sql = @"select top 1 cg.CardTypeID,c.CardGradeID,c.CardNumber,c.TotalPoints
,c.TotalAmount,c.Status,c.CreatedOn,c.CardExpiryDate
from card c
left join CardGrade cg on cg.CardGradeID=c.CardGradeID
where c.status=0 and c.CardGradeID=" + cardGradeID + @" and not exists(select 1 from Ord_CardTransferTo t where t.cardnumber=c.cardnumber) order by c.CardNumber";
            DataSet ds = crud.Query(sql);
            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            return ds;
        }
        public bool ExistsFromCard(string from)
        {
            string sql = "select 1 from Ord_CardTransferFrom where CardNumber='" + from.Trim() + "'";
            DataSet ds = crud.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        public bool Add()
        {
            bool success = false;
            Edge.SVA.BLL.Ord_CardTransfer_H bllCardTransfer = new Edge.SVA.BLL.Ord_CardTransfer_H();
            success = bllCardTransfer.Add(viewModel.MainTable);
            if (success)
            {
                viewModel.DetailTable.CardTransferNumber = viewModel.MainTable.CardTransferNumber;
                Edge.SVA.BLL.Ord_CardTransfer_D blld = new SVA.BLL.Ord_CardTransfer_D();
                int rtn = blld.Add(viewModel.DetailTable);
                if (rtn <= 0)
                {
                    success = false;
                }
                if (success)
                {
                    List<string> list = new List<string>();
                    list.Add(string.Format(@"insert into Ord_CardTransferFrom SELECT '{0}',a.* from card a where a.CardNumber='{1}'"
                        ,viewModel.MainTable.CardTransferNumber, viewModel.DetailTable.SourceCardNumber));
                    list.Add(string.Format(@"insert into Ord_CardTransferTo SELECT '{0}',a.* from card a where a.CardNumber='{1}'"
                        , viewModel.MainTable.CardTransferNumber, viewModel.DetailTable.DestCardNumber));
                    int rtnint = crud.ExecuteSqlList(list);
                    if (rtnint <= 0)
                    {
                        success = false;
                    }

                }
            }
            return success;
        }
        public DataSet GetFromCard(string from)
        {
            string sql = @"select top 50 m.MemberMobilePhone as MobileNO,m.MemberEmail Email,c.CardNumber,c.TotalPoints 
,c.TotalAmount,c.Status,c.CreatedOn,c.CardExpiryDate,c.CardGradeID
from card c
left join Member m on m.MemberID=c.MemberID where c.CardNumber='" + from.Trim() + "'";
            DataSet ds = crud.Query(sql);
            Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            return ds;
        }
        public DataSet GetToCard(string to)
        {
            string sql = @"select top 1 cg.CardTypeID,c.CardGradeID,c.CardNumber,c.TotalPoints
,c.TotalAmount,c.Status,c.CreatedOn,c.CardExpiryDate
from card c
left join CardGrade cg on cg.CardGradeID=c.CardGradeID
where c.CardNumber='" + to.Trim() + "'";
            DataSet ds = crud.Query(sql);
            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            return ds;
        }
        public bool Modify()
        {
            bool success = false;
            Edge.SVA.BLL.Ord_CardTransfer_H bllCardTransfer = new Edge.SVA.BLL.Ord_CardTransfer_H();

            success = bllCardTransfer.Update(viewModel.MainTable);
            if (success)
            {
                List<string> list = new List<string>();
                list.Add(string.Format(@"update Ord_CardTransfer_D set SourceCardNumber='{0}',DestCardNumber='{1}',ActPoints={3},ActAmount={4} where CardTransferNumber='{2}'"
                    , viewModel.DetailTable.SourceCardNumber, viewModel.DetailTable.DestCardNumber, viewModel.MainTable.CardTransferNumber
                    ,viewModel.DetailTable.ActPoints,viewModel.DetailTable.ActAmount));
                list.Add(string.Format("delete from Ord_CardTransferFrom where CardTransferNumber='{0}'", viewModel.MainTable.CardTransferNumber));
                list.Add(string.Format("delete from Ord_CardTransferTo where CardTransferNumber='{0}'", viewModel.MainTable.CardTransferNumber));
                list.Add(string.Format("insert into Ord_CardTransferFrom SELECT '{0}',a.* from card a where a.CardNumber='{1}'", viewModel.MainTable.CardTransferNumber, viewModel.DetailTable.SourceCardNumber));
                list.Add(string.Format("insert into Ord_CardTransferTo SELECT '{0}',a.* from card a where a.CardNumber='{1}'", viewModel.MainTable.CardTransferNumber, viewModel.DetailTable.DestCardNumber));
                int rtnint = crud.ExecuteSqlList(list);
                if (rtnint <= 0)
                {
                    success = false;
                }
                else
                {
                    success = true;
                }
            }

            return success;
        }

    }
}