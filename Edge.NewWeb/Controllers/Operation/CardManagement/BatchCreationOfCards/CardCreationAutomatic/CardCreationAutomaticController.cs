﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic
{
    public class CardCreationAutomaticController
    {
        private const string fields = "OrderSupplierNumber,ApproveStatus,OrderType,ApprovalCode,SupplierID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";
        private const string condition = " StoreID in {0} ";
        private const string andCondition = " and StoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores)+ " and PurchaseType=1 ";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1" + " and PurchaseType=1 ";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores) + " and PurchaseType=1 ";
                }
            }
            Edge.SVA.BLL.Ord_OrderToSupplier_Card_H bll = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H()
            {
                StrWhere = strWhere,
                //Order = "OrderSupplierNumber",//Modified By Robin 2014-07-17 for order field
                Order = "CreatedOn",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataTable GetExportList(string orderSupplierNumber) 
        {
            string sql = "select a.FirstCardNumber CardNumber,b.CardUID from Ord_OrderToSupplier_Card_D a,CardUIDMap b where a.FirstCardNumber = b.CardNumber and a.CardGradeID = b.CardGradeID and a.OrderSupplierNumber = '" + orderSupplierNumber + "' order by a.KeyID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds.Tables[0];
        }

        public DataSet GetCardNumberRange(string orderSupplierNumber)
        {
            string sql = "select CardGradeID, SUM(OrderQty) OrderQty, min(FirstCardNumber) FirstCardNumber,max(FirstCardNumber) EndCardNumber from Ord_OrderToSupplier_Card_D where OrderSupplierNumber = '" + orderSupplierNumber + "'group by CardGradeID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }

        public DataSet GetCardGradeList(string orderSupplierNumber)
        {
            string sql = "select CardGradeID, Max(PackageQty) OrderQty from Ord_OrderToSupplier_Card_D where OrderSupplierNumber = '" + orderSupplierNumber + "' group by CardGradeID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }

        public string UpLoadFileToServer(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/GC Delivery List/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/GC Delivery List/" + "GC Delivery List" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder();
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("CardUID\t");
                    text.Append("CardNumber\t");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.AppendFormat("'{0}\t", dr["CardUID"].ToString());
                        text.AppendFormat("'{0}\t", dr["CardNumber"].ToString());
                        text.Append("\r\n");
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }
    }
}