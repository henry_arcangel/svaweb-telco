﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardDeliveryPointAmountConfirmation
{
    public class CardDeliveryPointAmountConfirmationController
    {
        private const string fields = "CardReceiveNumber,ReferenceNo,StoreID,SupplierID,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy, ApproveOn,ApproveBy,OrderType";
        private const string condition = " StoreID in {0} ";
        private const string andCondition = " and StoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores) + " and PurchaseType=2 ";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1" + " and PurchaseType=2 ";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores) + " and PurchaseType=2 ";
                }
            }
            Edge.SVA.BLL.Ord_CardReceive_H bll = new Edge.SVA.BLL.Ord_CardReceive_H()
            {
                StrWhere = strWhere,
                Order = "CardReceiveNumber",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(int type, string CardReceiveNumber, string storeName, string CardGradeID, string para1, string para2,int startIndex, int endIndex, string startCardNumber)
        {
            string sql = "";
            string sql1 = "";
            if (startCardNumber != "")
            {
                para2 = (endIndex - startIndex + 1).ToString();
                para1 = startCardNumber;
                endIndex = endIndex - startIndex + 1;
                startIndex = 1;
            }
            if (CardGradeID != "-1")
            { sql1 = " and a.CardGradeID=" + CardGradeID + " "; }

            if (type == 0)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "'";// order by a.FirstCardNumber ";
            }
            else if (type == 1)
            {
                sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber >= '" + para1 + "'";// order by a.FirstCardNumber ";
            }
            else if (type == 2)
            {
                string CardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    CardNumber = model.CardNumber;
                }
                //sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber >= '" + CardNumber +"' order by a.FirstCardNumber ";
                if (startCardNumber == "")
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber >= '" + CardNumber + "'";// order by a.FirstCardNumber ";
                }
                else
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber > '" + CardNumber + "'";// order by a.FirstCardNumber ";
                }
            }
            else if (type == 3)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber = '" + para1 + "'";// order by a.FirstCardNumber ";
            }
            else
            {
                string CardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    CardNumber = model.CardNumber;
                }
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardTypeID=b.CardTypeID" + sql1 + " and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.FirstCardNumber = '" + CardNumber + "'";// order by a.FirstCardNumber ";
            }

            //Modified By Robin 2014-10-10
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            strSql.Append("order by FirstCardNumber");
            strSql.Append(")AS Row, T.*  from (" + sql + ") T ");
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            DataSet ds3 = DBUtility.DbHelperSQL.Query(strSql.ToString());
            //End
            //DataSet ds3 = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds3.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("CardStockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                //dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(0); //Modified By Robin 2014-11-21
                dr["CardStockStatusName"] = Edge.Web.Tools.DALTool.GetCardStockStatusName(Convert.ToInt32(dr["CardStockStatus"])); //Add By Robin 2014-06-20
            }

            return ds3;
        }

        public DataSet GetDetailList(int type, string CardReceiveNumber, int CardStockStatus, string storeName, string para1, string para2)
        {
            string sql = "";

            if (type == 0)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardGradeID=b.CardGradeID and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.CardStockStatus= " + CardStockStatus + " order by a.FirstCardNumber ";
            }
            else if (type == 1)
            {
                sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardGradeID=b.CardGradeID and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.CardStockStatus= " + CardStockStatus + " and a.FirstCardNumber >= '" + para1 + "' order by a.FirstCardNumber ";
            }
            else if (type == 2)
            {
                string CardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    CardNumber = model.CardNumber;
                }
                sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardGradeID=b.CardGradeID and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.CardStockStatus= " + CardStockStatus + " and a.FirstCardNumber >= '" + CardNumber + "' order by a.FirstCardNumber ";
            }
            else if (type == 3)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardGradeID=b.CardGradeID and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.CardStockStatus= " + CardStockStatus + " and a.FirstCardNumber = '" + para1 + "' order by a.FirstCardNumber ";
            }
            else
            {
                string CardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    CardNumber = model.CardNumber;
                }
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CardReceiveNumber,a.CardTypeID,a.CardGradeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCardNumber,a.EndCardNumber,a.BatchCardCode,a.CardStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CardExpiryDate, 111) CardExpiryDate,b.Status,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,Card b where a.FirstCardNumber=b.CardNumber and a.CardGradeID=b.CardGradeID and a.CardReceiveNumber='" + CardReceiveNumber + "' and a.CardStockStatus= " + CardStockStatus + " and a.FirstCardNumber = '" + CardNumber + "' order by a.FirstCardNumber ";
            }

            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("CardStockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["CardStockStatusName"] = Edge.Web.Tools.DALTool.GetCardStockStatusName(Convert.ToInt32(dr["CardStockStatus"])); //Add By Robin 2014-06-20
            }

            return ds;
        }

        public void UpdateCardStockStatus(DataTable dt, int CardStockStatus) 
        {
            string sql = "";
            foreach (DataRow dr in dt.Rows)
            {
                sql = "update Ord_CardReceive_D set CardStockStatus = " + CardStockStatus + ",Description = '" + Convert.ToString(dr["Description"]) + "' where KeyID = " + Convert.ToString(dr["KeyID"]);
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
        }

        public void UpdateCardStockStatus(DataTable dt)
        {
            string sql = "";
            foreach (DataRow dr in dt.Rows)
            {
                sql = "update Ord_CardReceive_D set CardStockStatus = " + Convert.ToString(dr["CardStockStatus"]) + ",Description = '" + Convert.ToString(dr["Description"]) + "' where KeyID = " + Convert.ToString(dr["KeyID"]);
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
        }

        public void UpdateCardActualAmount(DataTable dt)
        {
            string sql = "";
            foreach (DataRow dr in dt.Rows)
            {
                sql = "update Ord_CardReceive_D set ActualAmount = " + Convert.ToString(dr["ActualAmount"])+ " where KeyID = " + Convert.ToString(dr["KeyID"]);
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
        }

        public DataSet GetDetailList(string CardReceiveNumber, string supplierName)
        {
            string sql = "";

            string CardGradeName = DALTool.GetStringByCulture("CardGradeName1", "CardGradeName2", "CardGradeName3");

            sql = " select '" + supplierName + "' FromStoreName,a.CardGradeID,b.CardGradeCode,b." + CardGradeName + " CardGradeName,a.BatchCardCode,a.FirstCardNumber CardNumber,c.TotalAmount CardAmount,c.Status,c.StockStatus,CONVERT(varchar(100), c.CardIssueDate, 111) CardIssueDate,CONVERT(varchar(100), c.CardExpiryDate, 111) CardExpiryDate,d.CardUID,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,CardGrade b,Card c,CardUIDMap d where a.CardGradeID = b.CardGradeID and a.FirstCardNumber = c.CardNumber and a.CardGradeID = c.CardGradeID and a.FirstCardNumber = d.CardNumber and a.CardGradeID = d.CardGradeID and a.CardReceiveNumber = '" + CardReceiveNumber + "' order by a.FirstCardNumber ";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCardStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
                /*if (Convert.ToString(dr["Status"]) == "0")
                {
                    dr["StatusName"] = "未被领用";
                }
                else if (Convert.ToString(dr["Status"]) == "1")
                {
                    dr["StatusName"] = "已被领取（激活）";
                }
                else if (Convert.ToString(dr["Status"]) == "2")
                {
                    dr["StatusName"] = "已被使用";
                }
                else if (Convert.ToString(dr["Status"]) == "3")
                {
                    dr["StatusName"] = "过期";
                }
                else
                {
                    dr["StatusName"] = "作废";
                }
                if (Convert.ToString(dr["StockStatus"]) == "1")
                {
                    dr["StockStatusName"] = "总部未确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "2")
                {
                    dr["StockStatusName"] = "总部确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "3")
                {
                    dr["StockStatusName"] = "总部发现有优惠券损坏";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "4")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "5")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单，并发货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "6")
                {
                    dr["StockStatusName"] = "店铺确认收货";
                }
                else
                {
                    dr["StockStatusName"] = "店铺退货给总部";
                }*/
            }

            return ds;
        }

        public DataTable GetExportList(string CardReceiveNumber)
        {
            string sql = "select a.FirstCardNumber CardNumber,b.CardUID,OrderPoint,ActualPoint,OrderAmount,ActualAmount from Ord_CardReceive_D a,CardUIDMap b where a.FirstCardNumber = b.CardNumber and a.CardGradeID = b.CardGradeID and a.CardReceiveNumber = '" + CardReceiveNumber + "' order by a.KeyID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds.Tables[0];
        }

        public string UpLoadFileToServer(SVA.Model.Ord_CardReceive_H model,DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/SalesInvoice_PO/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/SalesInvoice_PO/" + "SalesInvoice_PO" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder();
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("eGC Delivery Confirmation\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("SVA Transaction No.:\t");
                    text.Append(model.CardReceiveNumber + "\t");
                    text.Append("Confirmed Date:\t");
                    text.Append(Convert.ToString(model.UpdatedOn) + "\t");
                    text.Append("\r\n");

                    text.Append("Transaction Status:\t");
                    text.Append(DALTool.GetApproveStatusString(Convert.ToString(model.ApproveStatus)) + "\t");
                    text.Append("Confirmed By:\t");
                    text.Append(Tools.DALTool.GetUserName(model.UpdatedBy.GetValueOrDefault()) + "\t");
                    text.Append("\r\n");

                    text.Append("Sales Invoice:\t");
                    text.Append(Convert.ToString(model.Remark1) + "\t");
                    text.Append("Purchase Order No.:\t");
                    text.Append(Convert.ToString(model.Remark2) + "\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("Card Type:\t");
                    text.Append("\t");
                    text.Append("Card QTY:\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("CardUID\t");
                    text.Append("CardNumber\t");
                    text.Append("\t");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.Append("\t");
                        text.AppendFormat("'{0}\t", dr["CardUID"].ToString());
                        text.AppendFormat("'{0}\t", dr["CardNumber"].ToString());
                        text.Append("\t");
                        text.Append("\r\n");
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }

        //Add By Robin 2014-07-24
        public string GetFirstCardNumber(string CardReceiveNumber)
        {
            string sql = "select min(a.FirstCardNumber) CardNumber from Ord_CardReceive_D a,CardUIDMap b where a.FirstCardNumber = b.CardNumber and a.CardGradeID = b.CardGradeID and a.CardReceiveNumber = '" + CardReceiveNumber + "' group by CardReceiveNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string FirstCardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                return FirstCardNumber;
            }
            else
            {
                return "";
            }
        }

        public string GetEndCardNumber(string CardReceiveNumber)
        {
            string sql = "select max(a.EndCardNumber) CardNumber from Ord_CardReceive_D a,CardUIDMap b where a.FirstCardNumber = b.CardNumber and a.CardGradeID = b.CardGradeID and a.CardReceiveNumber = '" + CardReceiveNumber + "' group by CardReceiveNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string EndCardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                return EndCardNumber;
            }
            else
            {
                return "";
            }
        }
        //End
    }
}