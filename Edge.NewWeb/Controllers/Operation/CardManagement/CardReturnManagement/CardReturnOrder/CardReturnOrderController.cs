﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;


namespace Edge.Web.Controllers.Operation.CardManagement.CardReturnManagement.CardReturnOrder
{
    public class CardReturnOrderController
    {
        private const string fields = "CardReturnNumber,ApproveStatus,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";
        private const string condition = " StoreID in {0} and FromStoreID in {0} ";
        private const string andCondition = " and StoreID in {0} and FromStoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores);
                }
            }
            Edge.SVA.BLL.Ord_CardReturn_H bll = new Edge.SVA.BLL.Ord_CardReturn_H()
            {
                StrWhere = strWhere,
                Order = "CardReturnNumber",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(int type, string CardGradeID, string batchCardID, string CardQty, string fromStoreID, string fromStoreName, string para1)
        {
            string sql = "";

            string strWhere = "";
            string CardGradeName = DALTool.GetStringByCulture("CardGradeName1", "CardGradeName2", "CardGradeName3");
            if (CardGradeID != "-1") 
            {
                strWhere += " and a.CardGradeID = " + CardGradeID;
            }
            if (batchCardID != "")
            {
                strWhere += " and a.BatchCardID = " + batchCardID;
            }
            if (type == 0) 
            {
                string topN = "";
                if (!string.IsNullOrEmpty(CardQty)) 
                {
                    topN = "top " + CardQty;
                }
                sql = " select " + topN + " '" + fromStoreName + "' FromStoreName,a.CardTypeID,a.CardGradeID,b.CardGradeCode,b." + CardGradeName + " CardGradeName,c.BatchCardCode,a.CardNumber,a.TotalAmount CardAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CardIssueDate, 111) CardIssueDate,CONVERT(varchar(100), a.CardExpiryDate, 111) CardExpiryDate,d.CardUID from Card a,CardGrade b,BatchCard c,CardUIDMap d where a.CardGradeID = b.CardGradeID and a.BatchCardID = c.BatchCardID and a.CardNumber = d.CardNumber and a.CardGradeID = d.CardGradeID and a.Status = 1 and a.IssueStoreID = " + fromStoreID + strWhere + " order by a.CardNumber ";
            }
            else if (type == 1)
            {
                sql = " select top " + CardQty + " '" + fromStoreName + "' FromStoreName,a.CardTypeID,a.CardGradeID,b.CardGradeCode,b." + CardGradeName + " CardGradeName,c.BatchCardCode,a.CardNumber,a.TotalAmount CardAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CardIssueDate, 111) CardIssueDate,CONVERT(varchar(100), a.CardExpiryDate, 111) CardExpiryDate,d.CardUID from Card a,CardGrade b,BatchCard c,CardUIDMap d where a.CardGradeID = b.CardGradeID and a.BatchCardID = c.BatchCardID and a.CardNumber = d.CardNumber and a.CardGradeID = d.CardGradeID and a.Status = 1 and a.IssueStoreID = " + fromStoreID + strWhere + " and a.CardNumber >= '" + para1 + "' order by a.CardNumber ";
            }
            else
            {
                string CardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    CardNumber = model.CardNumber;
                }
                sql = " select top " + CardQty + " '" + fromStoreName + "' FromStoreName,a.CardTypeID,a.CardGradeID,b.CardGradeCode,b." + CardGradeName + " CardGradeName,c.BatchCardCode,a.CardNumber,a.TotalAmount CardAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CardIssueDate, 111) CardIssueDate,CONVERT(varchar(100), a.CardExpiryDate, 111) CardExpiryDate,d.CardUID from Card a,CardGrade b,BatchCard c,CardUIDMap d where a.CardGradeID = b.CardGradeID and a.BatchCardID = c.BatchCardID and a.CardNumber = d.CardNumber and a.CardGradeID = d.CardGradeID and a.Status = 1 and a.IssueStoreID = " + fromStoreID + strWhere + " and a.CardNumber >= '" + CardNumber + "' order by a.CardNumber ";
            }

            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCardStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
                /*if (Convert.ToString(dr["Status"]) == "0")
                {
                    dr["StatusName"] = "未被领用";
                }
                else if (Convert.ToString(dr["Status"]) == "1")
                {
                    dr["StatusName"] = "已被领取（激活）";
                }
                else if (Convert.ToString(dr["Status"]) == "2")
                {
                    dr["StatusName"] = "已被使用";
                }
                else if (Convert.ToString(dr["Status"]) == "3")
                {
                    dr["StatusName"] = "过期";
                }
                else
                {
                    dr["StatusName"] = "作废";
                }
                if (Convert.ToString(dr["StockStatus"]) == "1")
                {
                    dr["StockStatusName"] = "总部未确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "2")
                {
                    dr["StockStatusName"] = "总部确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "3")
                {
                    dr["StockStatusName"] = "总部发现有优惠券损坏";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "4")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "5")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单，并发货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "6")
                {
                    dr["StockStatusName"] = "店铺确认收货";
                }
                else
                {
                    dr["StockStatusName"] = "店铺退货给总部";
                }*/
            }

            return ds;
        }

        public DataSet GetDetailList(string CardReturnNumber,string fromStoreName)
        {
            string sql = "";

            string CardGradeName = DALTool.GetStringByCulture("CardGradeName1", "CardGradeName2", "CardGradeName3");

            sql = " select '" + fromStoreName + "' FromStoreName,a.CardTypeID,a.CardGradeID,b.CardGradeCode,b." + CardGradeName + " CardGradeName,a.BatchCardCode,a.FirstCardNumber CardNumber,c.TotalAmount CardAmount,c.Status,c.StockStatus,CONVERT(varchar(100), c.CardIssueDate, 111) CardIssueDate,CONVERT(varchar(100), c.CardExpiryDate, 111) CardExpiryDate,d.CardUID from Ord_CardReturn_D a,CardGrade b,Card c,CardUIDMap d where a.CardGradeID = b.CardGradeID and a.FirstCardNumber = c.CardNumber and a.CardGradeID = c.CardGradeID and a.FirstCardNumber = d.CardNumber and a.CardGradeID = d.CardGradeID and a.CardReturnNumber = '" + CardReturnNumber + "' order by a.FirstCardNumber ";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCardTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCardStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
                /*if (Convert.ToString(dr["Status"]) == "0")
                {
                    dr["StatusName"] = "未被领用";
                }
                else if (Convert.ToString(dr["Status"]) == "1")
                {
                    dr["StatusName"] = "已被领取（激活）";
                }
                else if (Convert.ToString(dr["Status"]) == "2")
                {
                    dr["StatusName"] = "已被使用";
                }
                else if (Convert.ToString(dr["Status"]) == "3")
                {
                    dr["StatusName"] = "过期";
                }
                else
                {
                    dr["StatusName"] = "作废";
                }
                if (Convert.ToString(dr["StockStatus"]) == "1")
                {
                    dr["StockStatusName"] = "总部未确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "2")
                {
                    dr["StockStatusName"] = "总部确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "3")
                {
                    dr["StockStatusName"] = "总部发现有优惠券损坏";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "4")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "5")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单，并发货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "6")
                {
                    dr["StockStatusName"] = "店铺确认收货";
                }
                else
                {
                    dr["StockStatusName"] = "店铺退货给总部";
                }*/
            }

            return ds;
        }
    }
}