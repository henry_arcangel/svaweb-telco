﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.Operation.CardManagement
{
    public class ChangeManagementController
    {
        //private const string fields = "[CardAdjustNumber],[TxnDate],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate],[ApprovalCode]";
        //private const string condition = " EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CardAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CardAdjust_H.BrandCode))";
        //private const string andCondition = " and EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CardAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CardAdjust_H.BrandCode))";
              
        Edge.SVA.BLL.BLLCRUD crud = new Edge.SVA.BLL.BLLCRUD();
        
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            //string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            //if (string.IsNullOrEmpty(strWhere))
            //{
            //    if (string.IsNullOrEmpty(stores))
            //    {
            //        strWhere = " 1!=1";
            //    }
            //    else
            //    {
            //        strWhere = string.Format(condition, stores);
            //    }
            //}
            //else
            //{
            //    if (string.IsNullOrEmpty(stores))
            //    {
            //        strWhere = strWhere + " and 1!=1";
            //    }
            //    else
            //    {
            //        strWhere += string.Format(andCondition, stores);
            //    }
            //}

            string OrderField = "CardAdjustNumber";
            //bool OrderType = false;
            if (sortFieldStr.ToLower().EndsWith(" asc"))
            {
                //OrderType = true;
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" asc"));
            }
            else if (sortFieldStr.ToLower().EndsWith(" desc"))
            {
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" desc"));
            }
            Edge.SVA.BLL.Ord_CardAdjust_H bll = new Edge.SVA.BLL.Ord_CardAdjust_H();
            recodeCount = bll.GetCount(strWhere);
            System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, strWhere, OrderField);
            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
                this.AddAdjustAmount(ds, "OrderAmount", "CardAdjustNumber");
            }
            return ds;
        }

        public bool CheckCardBalance(string ordernumber)
        {
            string sql = @"select * from ( "
               + " select (isnull(C.TotalAmount,0) - abs(isnull(A.ActAmount,0))) as CloseBalance from "
               + " ( "
               + " select sum(isnull(ActAmount,0)) as ActAmount, CardNumber from ord_cardadjust_D where CardAdjustNumber = '" + ordernumber + "' "
               + " group by CardNumber "
               + " ) A left join  Card C on A.CardNumber = C.CardNumber "
               + " ) B where B.CloseBalance < 0 ";
            DataSet ds = crud.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
                return false;
            else
                return true;
        }

        public void AddAdjustAmount(DataSet ds, string name, string refKey)
        {
            string sqlstr = "select isnull(sum(orderamount),0) OrderAmount from ord_cardadjust_d where ";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DataSet ds1 = DBUtility.DbHelperSQL.Query(sqlstr + " CardAdjustNumber='" + row[refKey].ToString() + "'");
                if (ds1.Tables.Count > 0)
                {
                    row[name] = Convert.ToDecimal(ds1.Tables[0].Rows[0]["OrderAmount"]).ToString("F2");
                }
            }
        }

        #region 绑定下拉框
        public void BindDropDownList(FineUI.DropDownList ddl)
        {
            
        }
        #endregion
    }
}