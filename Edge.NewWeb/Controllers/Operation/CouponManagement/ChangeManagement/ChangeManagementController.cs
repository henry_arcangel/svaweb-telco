﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement
{
    public class ChangeManagementController
    {
        private const string fields = "[CouponAdjustNumber],[TxnDate],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate],[ApprovalCode]";
        //private const string condition = " EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CouponAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CouponAdjust_H.BrandCode))";
        //private const string andCondition = " and EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CouponAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CouponAdjust_H.BrandCode))";
        private const string condition = " ((isNull(StoreCode,'')='') or EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CouponAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CouponAdjust_H.BrandCode)))";
        private const string andCondition = " and ((isNull(StoreCode,'')='') or EXISTS (SELECT Store.StoreID FROM Brand INNER JOIN Store ON Brand.BrandID = Store.BrandID WHERE  Store.StoreID in {0} and (Store.StoreCode = Ord_CouponAdjust_H.StoreCode) AND (Brand.BrandCode = Ord_CouponAdjust_H.BrandCode)))";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere =" 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores);
                }                
            }

            string OrderField = "CouponAdjustNumber";
            bool OrderType = false;
            if (sortFieldStr.ToLower().EndsWith(" asc"))
            {
                OrderType = true;
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" asc"));
            }
            else if (sortFieldStr.ToLower().EndsWith(" desc"))
            {
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" desc"));
            }
            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H()
            {
                StrWhere = strWhere,
                Order = OrderField,//"CouponAdjustNumber",
                Fields = fields,
                Ascending = OrderType
            };
            //Modified By Robin 2014-10-13 fixed 711 can't show transaction in list problem
            //System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, out recodeCount);
            recodeCount = bll.GetCount(strWhere);
            System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, strWhere, OrderField);
            //End
            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            }
            return ds;
        }

        public DataSet GetTransactionList_NoStore(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores);
                }
            }


            string OrderField = "CouponAdjustNumber";
            bool OrderType = false;
            if (sortFieldStr.ToLower().EndsWith(" asc"))
            {
                OrderType = true;
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" asc"));
            }
            else if (sortFieldStr.ToLower().EndsWith(" desc"))
            {
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" desc"));
            }
            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H()
            {
                StrWhere = strWhere,
                Order = OrderField,//"CouponAdjustNumber",
                Fields = fields,
                Ascending = OrderType
            };
            //Modified By Robin 2014-10-13 fixed 711 can't show transaction in list problem
            //System.Data.DataSet ds = bll.GetList(pageSize, pageIndex, out recodeCount);
            recodeCount = bll.GetCount_NoBrand(strWhere);
            System.Data.DataSet ds = bll.GetList_NoBrand(pageSize, pageIndex, strWhere, OrderField);
            //End
            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            }
            return ds;
        }
    }
}