﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponIssue
{
    public class CouponIssueController
    {
        protected CouponAdjustViewModel viewModel = new CouponAdjustViewModel();

        public CouponAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void ShowCouponList(DataTable dt)
        {
            viewModel.CouponTable = dt;
        }

        public void AddCouponTypeList(List<string> list)
        {
            CouponTypeRepostory res = CouponTypeRepostory.Singleton;
            foreach (var item in list)
            {
                KeyValue exist = this.viewModel.CouponTypeList.Find(m => m.Key == item);
                if (exist == null)
                {
                    KeyValue kv = new KeyValue();
                    kv.Key = item;
                    Edge.SVA.Model.CouponType cg = res.GetModelByID(ConvertTool.ConverType<int>(item));
                    switch (SVASessionInfo.SiteLanguage)
                    {
                        case LanguageFlag.ZHCN:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName2;
                            break;
                        case LanguageFlag.ZHHK:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName3;
                            break;
                        case LanguageFlag.ENUS:
                        default:
                            kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName1;
                            break;
                    }
                    this.viewModel.CouponTypeList.Add(kv);
                }
            }
        }
        public void RemoveCouponType(KeyValue model)
        {
            if (this.viewModel.CouponTypeList.Contains(model))
            {
                this.viewModel.CouponTypeList.Remove(model);
            }
        }
        public void AddCouponBatchCodeList(KeyValue model)
        {
            BatchCoupon bc = new BatchCoupon();
            model.Key = bc.BatchCouponID.ToString();
            model.Value = bc.BatchCouponCode;
            viewModel.BatchCouponIDList.Add(model);
        }
        private bool ExistSameKeyInKeyValueList(KeyValue model, List<KeyValue> list)
        {
            bool rtn = false;
            if (model == null)
            {
                return true;
            }
            KeyValue mo = list.Find(m => m.Key == model.Key);
            if (mo != null)
            {
                return true;
            }
            return rtn;
        }
    }
}