﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;

namespace Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponPushManual
{
    public class CouponPushController
    {
        protected CouponPushViewModel viewModel = new CouponPushViewModel();

        public CouponPushViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Ord_CouponPush_H bll = new Edge.SVA.BLL.Ord_CouponPush_H();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "CouponPushNumber desc";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }
    }
}