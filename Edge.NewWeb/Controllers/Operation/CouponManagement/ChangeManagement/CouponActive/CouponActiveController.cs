﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.CouponActive
{
    public class CouponActiveController
    {
        protected CouponAdjustViewModel viewModel = new CouponAdjustViewModel();

        public CouponAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }
    }
}