﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.Controllers.Operation.CouponManagement.BatchCreationOfCoupons.CouponDeliveryConfirmation
{
    public class CouponDeliveryConfirmationController
    {
        private const string fields = "CouponReceiveNumber,ReferenceNo,StoreID,SupplierID,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy, ApproveOn,ApproveBy,OrderType";
        private const string condition = " StoreID in {0} ";
        private const string andCondition = " and StoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores);
                }
            }
            Edge.SVA.BLL.Ord_CouponReceive_H bll = new Edge.SVA.BLL.Ord_CouponReceive_H()
            {
                StrWhere = strWhere,
                Order = "CouponReceiveNumber",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(int type, string couponReceiveNumber, string storeName, string CouponTypeID, string para1, string para2, int startIndex, int endIndex, string startCouponNumber)
        {
            string sql = "";
            string sql1 = "";
            if (startCouponNumber != "")
            {
                para2 = (endIndex - startIndex + 1).ToString();
                para1 = startCouponNumber;
                endIndex = endIndex - startIndex + 1;
                startIndex = 1;
            }
            if (CouponTypeID != "-1")
            { sql1 = " and a.CouponTypeID=" + CouponTypeID + " "; }

            if (type == 0)
            {
                //sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' order by a.FirstCouponNumber ";
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "'  ";
            }
            else if (type == 1)
            {
                //sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber >= '" + para1 + "' order by a.FirstCouponNumber ";
                if (startCouponNumber == "")
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber >= '" + para1 + "'  ";
                }
                else
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber > '" + para1 + "'  ";
                }

            }
            else if (type == 2)
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                //sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber >= '" + couponNumber + "' order by a.FirstCouponNumber ";
                if (startCouponNumber == "")
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber >= '" + couponNumber + "'  ";
                }
                else
                {
                    sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber > '" + couponNumber + "'  ";
                }
            }
            else if (type == 3)
            {
                //sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber = '" + para1 + "' order by a.FirstCouponNumber ";
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber = '" + para1 + "'  ";
            }
            else
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                //sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber = '" + couponNumber + "' order by a.FirstCouponNumber ";
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID" + sql1 + " and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.FirstCouponNumber = '" + couponNumber + "'  ";
            }
            //Modified By Robin 2014-10-10
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            strSql.Append("order by FirstCouponNumber");
            strSql.Append(")AS Row, T.*  from (" + sql + ") T ");
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            DataSet ds3 = DBUtility.DbHelperSQL.Query(strSql.ToString());
            //End
            //DataSet ds3 = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds3.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("CouponStockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                //dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(0); //Modified By Robin 2014-11-21
                //dr["CouponStockStatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(Convert.ToInt32(dr["CouponStockStatus"])); //Add By Robin 2014-06-20
            }

            return ds3;
        }

        public DataSet GetDetailList(int type, string couponReceiveNumber, int couponStockStatus, string storeName, string para1, string para2)
        {
            string sql = "";

            if (type == 0)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.couponStockStatus= " + couponStockStatus + " order by a.FirstCouponNumber ";
            }
            else if (type == 1)
            {
                sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.couponStockStatus= " + couponStockStatus + " and a.FirstCouponNumber >= '" + para1 + "' order by a.FirstCouponNumber ";
            }
            else if (type == 2)
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                sql = " select top " + para2 + " '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.couponStockStatus= " + couponStockStatus + " and a.FirstCouponNumber >= '" + couponNumber + "' order by a.FirstCouponNumber ";
            }
            else if (type == 3)
            {
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.couponStockStatus= " + couponStockStatus + " and a.FirstCouponNumber = '" + para1 + "' order by a.FirstCouponNumber ";
            }
            else
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                sql = " select '" + storeName + "' StoreName,a.KeyID,a.CouponReceiveNumber,a.CouponTypeID,a.Description,a.OrderQTY,a.ActualQTY,a.FirstCouponNumber,a.EndCouponNumber,a.BatchCouponCode,a.CouponStockStatus,CONVERT(varchar(100), a.ReceiveDateTime, 111) ReceiveDateTime,CONVERT(varchar(100), b.CouponExpiryDate, 111) CouponExpiryDate,b.Status,m.CouponUID,t.CouponTypeName1 CouponTypeName from Ord_CouponReceive_D a left join CouponUIDMap m on a.FirstCouponNumber=m.CouponNumber left join CouponType t on a.CouponTypeID=t.CouponTypeID,Coupon b where a.FirstCouponNumber=b.CouponNumber and a.CouponTypeID=b.CouponTypeID and a.CouponReceiveNumber='" + couponReceiveNumber + "' and a.couponStockStatus= " + couponStockStatus + " and a.FirstCouponNumber = '" + couponNumber + "' order by a.FirstCouponNumber ";
            }

            DataSet ds2 = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds2.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("CouponStockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["CouponStockStatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(Convert.ToInt32(dr["CouponStockStatus"])); //Add By Robin 2014-06-20
            }

            return ds2;
        }

        public void UpdateCouponStockStatus(DataTable dt, int couponStockStatus) 
        {
            string sql = "";
            foreach (DataRow dr in dt.Rows)
            {
                sql = "update Ord_CouponReceive_D set CouponStockStatus = " + couponStockStatus + ",Description = '" + Convert.ToString(dr["Description"]) + "' where KeyID = " + Convert.ToString(dr["KeyID"]);
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
        }

        public void UpdateCouponStockStatus(DataTable dt)
        {
            string sql = "";
            foreach (DataRow dr in dt.Rows)
            {
                sql = "update Ord_CouponReceive_D set CouponStockStatus = " + Convert.ToString(dr["CouponStockStatus"]) + ",Description = '" + Convert.ToString(dr["Description"]) + "' where KeyID = " + Convert.ToString(dr["KeyID"]);
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
        }

        public DataSet GetDetailList(string couponReceiveNumber, string supplierName)
        {
            string sql = "";

            string couponTypeName = DALTool.GetStringByCulture("CouponTypeName1", "CouponTypeName2", "CouponTypeName3");

            sql = " select '" + supplierName + "' FromStoreName,a.CouponTypeID,b.CouponTypeCode,b." + couponTypeName + " CouponTypeName,a.BatchCouponCode,a.FirstCouponNumber CouponNumber,c.CouponAmount,c.Status,c.StockStatus,CONVERT(varchar(100), c.CouponIssueDate, 111) CouponIssueDate,CONVERT(varchar(100), c.CouponExpiryDate, 111) CouponExpiryDate,d.CouponUID from Ord_CouponReceive_D a,CouponType b,Coupon c,CouponUIDMap d where a.CouponTypeID = b.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = d.CouponNumber and a.CouponTypeID = d.CouponTypeID and a.CouponReceiveNumber = '" + couponReceiveNumber + "' order by a.FirstCouponNumber ";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
            }

            return ds;
        }

        public DataTable GetExportList(string couponReceiveNumber)
        {
            string sql = "select a.FirstCouponNumber CouponNumber,b.CouponUID from Ord_CouponReceive_D a,CouponUIDMap b where a.FirstCouponNumber = b.CouponNumber and a.CouponTypeID = b.CouponTypeID and a.CouponReceiveNumber = '" + couponReceiveNumber + "' order by a.KeyID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds.Tables[0];
        }

        public string UpLoadFileToServer(SVA.Model.Ord_CouponReceive_H model,DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/SalesInvoice_PO/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/SalesInvoice_PO/" + "SalesInvoice_PO" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder();
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("GC Delivery Confirmation\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("SVA Transaction No.:\t");
                    text.Append(model.CouponReceiveNumber + "\t");
                    text.Append("Confirmed Date:\t");
                    text.Append(Convert.ToString(model.UpdatedOn) + "\t");
                    text.Append("\r\n");

                    text.Append("Transaction Status:\t");
                    text.Append(DALTool.GetApproveStatusString(Convert.ToString(model.ApproveStatus)) + "\t");
                    text.Append("Confirmed By:\t");
                    text.Append(Tools.DALTool.GetUserName(model.UpdatedBy.GetValueOrDefault()) + "\t");
                    text.Append("\r\n");

                    text.Append("Sales Invoice:\t");
                    text.Append(Convert.ToString(model.Remark1) + "\t");
                    text.Append("Purchase Order No.:\t");
                    text.Append(Convert.ToString(model.Remark2) + "\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("Coupon Type:\t");
                    text.Append("\t");
                    text.Append("Coupon QTY:\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\t");
                    text.Append("\r\n");

                    text.Append("\t");
                    text.Append("CouponUID\t");
                    text.Append("CouponNumber\t");
                    text.Append("\t");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.Append("\t");
                        text.AppendFormat("'{0}\t", dr["CouponUID"].ToString());
                        text.AppendFormat("'{0}\t", dr["CouponNumber"].ToString());
                        text.Append("\t");
                        text.Append("\r\n");
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }

        //Add By Robin 2014-07-24
        public string GetFirstCouponNumber(string couponReceiveNumber)
        {
            string sql = "select min(a.FirstCouponNumber) CouponNumber from Ord_CouponReceive_D a,CouponUIDMap b where a.FirstCouponNumber = b.CouponNumber and a.CouponTypeID = b.CouponTypeID and a.CouponReceiveNumber = '" + couponReceiveNumber + "' group by CouponReceiveNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string FirstCouponNumber = ds.Tables[0].Rows[0]["CouponNumber"].ToString();
                return FirstCouponNumber;
            }
            else
            {
                return "";
            }
        }

        public string GetEndCouponNumber(string couponReceiveNumber)
        {
            string sql = "select max(a.EndCouponNumber) CouponNumber from Ord_CouponReceive_D a,CouponUIDMap b where a.FirstCouponNumber = b.CouponNumber and a.CouponTypeID = b.CouponTypeID and a.CouponReceiveNumber = '" + couponReceiveNumber + "' group by CouponReceiveNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string EndCouponNumber = ds.Tables[0].Rows[0]["CouponNumber"].ToString();
                return EndCouponNumber;
            }
            else
            {
                return "";
            }
        }
        //End
    }
}