﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace Edge.Web.Controllers.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public class CouponPickingController
    {
        private const string fields = "CouponPickingNumber,ReferenceNo,ApproveStatus,OrderType,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy,UpdatedOn,UpdatedBy";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            Edge.SVA.BLL.Ord_CouponPicking_H bll = new Edge.SVA.BLL.Ord_CouponPicking_H()
            {
                StrWhere = strWhere,
                Order = "CouponPickingNumber",
                Fields = fields,
                Ascending = false
            };

            DataSet ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            Tools.DataTool.AddOrderPickingApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(string id)
        {
            string sql = " select a.KeyID,a.CouponPickingNumber,a.CouponTypeID,a.PickQTY,a.FirstCouponNumber,a.EndCouponNumber,b.CouponNumPattern,c.CouponAmount from Ord_CouponPicking_D a,CouponType b,Coupon c where a.CouponTypeID = b.CouponTypeID and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponPickingNumber = '" + id + "' order by a.KeyID";
            return DBUtility.DbHelperSQL.Query(sql);
        }

        //Added By Robin 2014-08-06 for RRG group coupon number
        public DataSet GetDetailListGroup(string id)
        {
            //string sql = " select a.KeyID,a.CouponPickingNumber,a.CouponTypeID,a.PickQTY,a.FirstCouponNumber,a.EndCouponNumber,b.CouponNumPattern,c.CouponAmount from Ord_CouponPicking_D a,CouponType b,Coupon c where a.CouponTypeID = b.CouponTypeID and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponPickingNumber = '" + id + "' order by a.KeyID";
            StringBuilder sql = new StringBuilder();
            string tblUser;
            tblUser = "_" + Tools.DALTool.GetCurrentUser().UserID.ToString();
            sql.Append("if object_id('tempdb..#O" + tblUser + "') is not null Begin drop table #O" + tblUser + " End ");
            sql.Append("if object_id('tempdb..#E" + tblUser + "') is not null Begin drop table #E" + tblUser + " End ");
            sql.Append("if object_id('tempdb..#F" + tblUser + "') is not null Begin drop table #F" + tblUser + " End ");
            sql.Append("select a.KeyID,a.CouponPickingNumber,a.CouponTypeID,a.PickQTY,a.FirstCouponNumber,a.EndCouponNumber,b.CouponNumPattern,c.CouponAmount into #O" + tblUser + " from Ord_CouponPicking_D a,CouponType b,Coupon c where a.CouponTypeID = b.CouponTypeID and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponPickingNumber =  '" + id + "' order by a.KeyID ");
            sql.Append("SELECT identity(int, 1,1) id, T2.FirstCouponNumber into #F" + tblUser + " FROM #O" + tblUser + " T2 WHERE NOT EXISTS (SELECT FirstCouponNumber  FROM #O" + tblUser + " A WHERE cast(A.FirstCouponNumber as bigint)+1=cast(T2.FirstCouponNumber as bigint)) order by T2.FirstCouponNumber ");
            sql.Append("SELECT identity(int, 1,1) id,T2.EndCouponNumber into #E" + tblUser + " FROM #O" + tblUser + " T2 WHERE NOT EXISTS (SELECT EndCouponNumber  FROM #O" + tblUser + " A WHERE cast(A.EndCouponNumber as bigint)=cast(T2.EndCouponNumber as bigint)+1) order by T2.EndCouponNumber ");
            //sql.Append("select KeyID,CouponPickingNumber,CouponTypeID,T.PickQTY,replace(T.FirstCouponNumber,CouponNumPattern,'') FirstCouponNumber,replace(T.EndCouponNumber,CouponNumPattern,'') EndCouponNumber,CouponNumPattern,CouponAmount*T.PickQTY CouponAmount from #O" + tblUser + " inner join (select FirstCouponNumber,EndCouponNumber, cast(EndCouponNumber as bigint)-cast(FirstCouponNumber as bigint)+1 PickQTY from #E" + tblUser + " right join #F" + tblUser + " on #E" + tblUser + ".id=#F" + tblUser + ".id) T on #O" + tblUser + ".FirstCouponNumber=T.FirstCouponNumber");
            sql.Append("select KeyID,CouponPickingNumber,CouponTypeID,T.PickQTY,replace(T.FirstCouponNumber,CouponNumPattern,'') FirstCouponNumber,replace(T.EndCouponNumber,CouponNumPattern,'') EndCouponNumber,CouponNumPattern,(select SUM(CouponAmount) from #O" + tblUser + " where #O" + tblUser + ".FirstCouponNumber between  T.FirstCouponNumber and T.EndCouponNumber) CouponAmount from #O" + tblUser + " inner join (select FirstCouponNumber,EndCouponNumber, cast(EndCouponNumber as bigint)-cast(FirstCouponNumber as bigint)+1 PickQTY from #E" + tblUser + " right join #F" + tblUser + " on #E" + tblUser + ".id=#F" + tblUser + ".id) T on #O" + tblUser + ".FirstCouponNumber=T.FirstCouponNumber");
           
            return DBUtility.DbHelperSQL.Query(sql.ToString());
            ////Here is SQL Robin 2014-09-02
            //if object_id('tempdb..#O_1') is not null Begin drop table #O_1 End 
            //if object_id('tempdb..#E_1') is not null Begin drop table #E_1 End 
            //if object_id('tempdb..#F_1') is not null Begin drop table #F_1 End 
            //select a.KeyID,a.CouponPickingNumber,a.CouponTypeID,a.PickQTY,a.FirstCouponNumber,a.EndCouponNumber,b.CouponNumPattern,c.CouponAmount into #O_1 from Ord_CouponPicking_D a,CouponType b,Coupon c where a.CouponTypeID = b.CouponTypeID and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponPickingNumber =  'COPO00000000380' order by a.KeyID 
            //select * from #O_1
            //SELECT identity(int, 1,1) id, T2.FirstCouponNumber into #F_1 FROM #O_1 T2 WHERE NOT EXISTS (SELECT FirstCouponNumber  FROM #O_1 A WHERE cast(A.FirstCouponNumber as bigint)+1=cast(T2.FirstCouponNumber as bigint)) order by T2.FirstCouponNumber
            //select * from #F_1
            //SELECT identity(int, 1,1) id,T2.EndCouponNumber into #E_1 FROM #O_1 T2 WHERE NOT EXISTS (SELECT EndCouponNumber  FROM #O_1 A WHERE cast(A.EndCouponNumber as bigint)=cast(T2.EndCouponNumber as bigint)+1) order by T2.EndCouponNumber
            //select * from #E_1
            //select KeyID,CouponPickingNumber,CouponTypeID,T.PickQTY,replace(T.FirstCouponNumber,CouponNumPattern,'') FirstCouponNumber,replace(T.EndCouponNumber,CouponNumPattern,'') EndCouponNumber,CouponNumPattern,CouponAmount*T.PickQTY CouponAmount 
            //from #O_1 inner join (select FirstCouponNumber,EndCouponNumber, cast(EndCouponNumber as bigint)-cast(FirstCouponNumber as bigint)+1 PickQTY from #E_1 right join #F_1 on #E_1.id=#F_1.id) T on #O_1.FirstCouponNumber=T.FirstCouponNumber
        }
        //End

        //Add By Robin 2014-07-24
        public string GetFirstCouponNumber(string CouponPickingNumber)
        {
            string sql = " select min(a.FirstCouponNumber) CouponNumber from Ord_CouponPicking_D a where a.CouponPickingNumber = '" + CouponPickingNumber + "' group by CouponPickingNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string FirstCouponNumber = ds.Tables[0].Rows[0]["CouponNumber"].ToString();
                return FirstCouponNumber;
            }
            else
            {
                return "";
            }
        }

        public string GetEndCouponNumber(string CouponPickingNumber)
        {
            string sql = " select max(a.EndCouponNumber) CouponNumber from Ord_CouponPicking_D a where a.CouponPickingNumber = '" + CouponPickingNumber + "' group by CouponPickingNumber";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string EndCouponNumber = ds.Tables[0].Rows[0]["CouponNumber"].ToString();
                return EndCouponNumber;
            }
            else
            {
                return "";
            }
        }
        //End

        //Add By Robin 2014-09-02
        public DataSet GetCouponTypeList(string CouponPickingNumber)
        {
            string sql = "select CouponTypeID, case CouponAmount when 0 then CouponAmount1 else CouponAmount end as CouponAmount from (select D.CouponTypeID, SUM(PickQty)*max(CT.CouponTypeAmount) CouponAmount, SUM(C.CouponAmount) CouponAmount1 from Ord_CouponPicking_D D left join CouponType CT on D.CouponTypeID=CT.CouponTypeID left join Coupon C on D.FirstCouponNumber=C.CouponNumber where CouponPickingNumber = '" + CouponPickingNumber + "' group by D.CouponTypeID) temp";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }
        //End
    }
}