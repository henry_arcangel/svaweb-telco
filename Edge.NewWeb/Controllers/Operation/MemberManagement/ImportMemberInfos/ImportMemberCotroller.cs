﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using System.Data;
using Edge.SVA.Model.Domain.Operation;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using Edge.DBUtility;
using Newtonsoft.Json.Linq;

namespace Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos
{
    public class ImportMemberCotroller
    {
        protected ImportMemberViewModel viewModel = new ImportMemberViewModel();

        public ImportMemberViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string ImportMemberNumber)
        {
            Edge.SVA.BLL.Ord_ImportMember_H bll = new Edge.SVA.BLL.Ord_ImportMember_H();
            Edge.SVA.Model.Ord_ImportMember_H model = bll.GetModel(ImportMemberNumber);
            viewModel.MainTable = model;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Ord_ImportMember_H bll = new Edge.SVA.BLL.Ord_ImportMember_H();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            ShowCSVFileName(ds, "DescriptionName", "Description");

            return ds;
        }

        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Ord_ImportMember_H bll = new SVA.BLL.Ord_ImportMember_H();

                //保存
                if (bll.Exists(viewModel.MainTable.ImportMemberNumber))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public bool ExchangeCSVToXML(string strPath, string ImportMemberNumber,bool Adjustflag)
        {
            try
            {
                //循环读取文件行
                FileStream aFile = new FileStream(strPath, FileMode.Open);
                StreamReader sr = new StreamReader(aFile);
                List<string> storedata = new List<string>();
                int lineindex = 0;
                DataTable dt = new DataTable();
                while (!sr.EndOfStream)
                {
                    List<string> tempListString = new List<string>();
                    String test = sr.ReadLine().Replace("\\", "").Replace("ñ", "n").Replace("Ñ", "N").Trim();
                    //if (lineindex == 0 && !test.StartsWith("RRC_"))
                    //{
                    //    Tools.Logger.Instance.WriteErrorLog(" Imported file  " + strPath, "no headers");
                    //    throw new Exception(" this file no headers ");
                    //}
                    if (test.Contains(","))
                    {
                        string[] tempStringArr = test.Split(',');
                        if (lineindex == 0)
                        {
                            for (int i = 0; i < tempStringArr.Length; i++)
                            {
                                dt.Columns.Add(tempStringArr[i]);
                            }
                        }

                        string ss = "";
                        bool flag = false;
                        for (int i = 0; i < tempStringArr.Length; i++)
                        {
                            //处理数据中包含","的字段值
                            if (lineindex > 0)
                            {
                                //处理数据中包含","的字段值
                                if (!IsValidate(tempStringArr[i]))
                                {
                                    flag = true;
                                }

                                if (flag)
                                {
                                    ss += tempStringArr[i] + ",";
                                    if (IsValidate(ss))
                                    {
                                        flag = false;
                                        string str = ss.TrimEnd(',');
                                        tempListString.Add(str.TrimStart('"').TrimEnd('"').Replace("\"\"", "\""));
                                        ss = "";
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    tempListString.Add(tempStringArr[i]);
                                }
                            }
                        }
                        if (lineindex > 0)//第一行为列头，需排除
                        {
                            dt.Rows.Add(1);
                            storedata = tempListString;

                            for (int i = 0; i < storedata.Count; i++)
                            {
                                dt.Rows[lineindex - 1][i] = storedata[i];
                            }

                        }
                        lineindex++;
                    }
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (!Adjustflag)//(dt.Columns[0].ColumnName.Trim().ToLower() == "rrc_retailerid")
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        XmlDeclaration xmldecl;
                        xmldecl = xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                        xmldoc.AppendChild(xmldecl);

                        //加入一个根元素    
                        XmlElement xmlelem;
                        xmlelem = xmldoc.CreateElement("", "LoyaltyCustomer", "");
                        xmlelem.SetAttribute("Action", "I");
                        xmlelem.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                        xmldoc.AppendChild(xmlelem);

                        XmlNode loyaltycustomer = xmldoc.SelectSingleNode("LoyaltyCustomer");
                        XmlElement retailer = xmldoc.CreateElement("Retailer");
                        retailer.SetAttribute("Id", CutString(dt.Rows[0]["RRC_RetailerID"].ToString(), 24));
                        loyaltycustomer.AppendChild(retailer);
                        DataRow dr;
                        if (dt.Columns.Count < 25)
                        {
                            return false;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dr = dt.Rows[i];

                            //Logic to set datevalue
                            string MemberStartDate = "";
                            string MemberEffectiveDate = "";
                            string CardIssueDate = "";
                            string CardExpirationDate = "";
                            string CardEffectiveDate = "";

                            string HouseHoldExternalId = "";
                            try
                            {
                                string RRC_Type_of_Application = CutString(dr["RRC_Type of Application"].ToString(), 1);
                                if (RRC_Type_of_Application == "1")
                                {
                                    MemberStartDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    MemberEffectiveDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    CardIssueDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    CardExpirationDate = string.IsNullOrEmpty(dr["RRC_Card Expiry Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Card Expiry Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    CardEffectiveDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");

                                    HouseHoldExternalId = CutString(dr["RRC_Cardnumber"].ToString(), 24);
                                }
                                else if (RRC_Type_of_Application == "2")
                                {
                                    MemberStartDate = "remove";
                                    MemberEffectiveDate = "remove";
                                    CardIssueDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    //CardExpirationDate = string.IsNullOrEmpty(dr["RRC_Card Expiry Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Card Expiry Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    CardEffectiveDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");


                                    sqlString = string.Format(sqlString, CutString(dr["RRC_Old Card No."].ToString(), 20));
                                    HouseHoldExternalId = GetHouseHoldExternalId(connString, sqlString);
                                    sqlString2 = string.Format(sqlString2, CutString(dr["RRC_Old Card No."].ToString(), 20));
                                    CardExpirationDate = GetExpirationDate(connString, sqlString2);

                                }
                                else if (RRC_Type_of_Application == "3")
                                {
                                    MemberStartDate = "remove";
                                    MemberEffectiveDate = "remove";
                                    CardExpirationDate = "remove";
                                    CardIssueDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                                    CardEffectiveDate = string.IsNullOrEmpty(dr["RRC_Application Date"].ToString()) ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss");

                                    HouseHoldExternalId = GetHouseHoldExternalId(connString, sqlString);
                                }
                            }
                            catch (System.Exception ex)
                            {
                                Tools.Logger.Instance.WriteErrorLog(" Import Member Datetime error line " + (i + 1).ToString(), " [RRC_Application Date]  " + dr["RRC_Application Date"].ToString(), ex);
                                throw ex;
                            }
                            string MemberExternalId = HouseHoldExternalId;

                            //截取前40位字符
                            string street = dr["RRC_House No."].ToString() + " " + dr["RRC_Street Name"].ToString() + " " + dr["RRC_Brgy/Subd"].ToString();
                            street = street.Length > 40 ? street.Substring(0, 40) : street;

                            string BirthDate = "";


                            try
                            {
                                //获取生日年份
                                DateTime dtBirthDate = Convert.ToDateTime(dr["RRC_Birthdate"]);
                                if (dtBirthDate < DateTime.Parse("1900-01-01"))
                                {
                                    dtBirthDate = DateTime.Parse("1900-01-01");
                                }
                                else if (dtBirthDate > DateTime.Parse("2056-12-31"))
                                {
                                    dtBirthDate = DateTime.Parse("2056-12-31");
                                }
                                BirthDate = dtBirthDate.ToString("yyyy-MM-dd");
                            }
                            catch (System.Exception ex)
                            {
                                Tools.Logger.Instance.WriteErrorLog(" Import Member Datetime error line " + (i + 1).ToString(), " [RRC_Birthdate]  " + dr["RRC_Birthdate"].ToString(), ex);
                                throw ex;
                            }


                            //构建XML
                            XmlElement household = xmldoc.CreateElement("HouseHold");
                            household.SetAttribute("HouseHoldExternalId", HouseHoldExternalId);
                            household.SetAttribute("PostalCode", CutString(dr["RRC_ZipCode"].ToString(), 10));
                            household.SetAttribute("Country", "46");
                            household.SetAttribute("City", CutString(dr["RRC_City/Town"].ToString(), 30));
                            //household.SetAttribute("Street1", dr["RRC_House No."].ToString() + " " + dr["RRC_Street Name"].ToString() + " " + dr["RRC_Brgy/Subd"].ToString());
                            household.SetAttribute("Street1", street);
                            household.SetAttribute("HomePhone", CutString(dr["RRC_HomePhone"].ToString(), 20));
                            household.SetAttribute("EMailAddress", CutString(dr["RRC_Emailadd"].ToString(), 100));
                            household.SetAttribute("SendEmail", "0");
                            retailer.AppendChild(household);

                            //xmlelem = xmldoc.CreateElement("Accounts");
                            //XmlElement account = xmldoc.CreateElement("Account");
                            //account.SetAttribute("Id", "4");
                            //account.SetAttribute("EarnValue", "0.00");
                            //xmlelem.AppendChild(account);
                            //household.AppendChild(xmlelem);

                            XmlElement members = xmldoc.CreateElement("Members");
                            XmlElement member = xmldoc.CreateElement("Member");
                            member.SetAttribute("LastName", CutString(dr["RRC_Last Name"].ToString(), 50));
                            member.SetAttribute("MiddleInitial", CutString(dr["RRC_Middle Initial"].ToString(), 1));
                            member.SetAttribute("IsMainMember", "1");
                            member.SetAttribute("FirstName", CutString(dr["RRC First Name"].ToString(), 50));
                            //member.SetAttribute("BirthDate", dr["RRC_Birthdate"] == null ? "" : Convert.ToDateTime(dr["RRC_Birthdate"]).ToString("yyyy-MM-dd"));
                            member.SetAttribute("BirthDate", BirthDate);
                            member.SetAttribute("MobilePhoneNumber", CutString(dr["RRC_Mobile No."].ToString(), 20));
                            member.SetAttribute("WorkPhoneNumber", CutString(dr["RRC_WorkPhone"].ToString(), 20));
                            member.SetAttribute("MemberExternalId", MemberExternalId);
                            member.SetAttribute("Gender", CutString(dr["RRC_Gender"].ToString(), 1));
                            if (MemberStartDate != "remove")
                            {
                                member.SetAttribute("StartDate", MemberStartDate);
                            }
                            if (MemberEffectiveDate != "remove")
                            {
                                member.SetAttribute("EffectiveDate", MemberEffectiveDate);
                            }
                            member.SetAttribute("MemberStatus", "1");
                            household.AppendChild(members);
                            members.AppendChild(member);

                            xmlelem = xmldoc.CreateElement("Cards");
                            XmlElement card = xmldoc.CreateElement("Card");
                            card.SetAttribute("Id", CutString(dr["RRC_Cardnumber"].ToString(), 24));
                            card.SetAttribute("CardStatus", CutString(dr["RRC_Status"].ToString(), 1));
                            card.SetAttribute("IssueDate", CardIssueDate);
                            if (CardExpirationDate != "remove")
                            {
                                card.SetAttribute("ExpirationDate", CardExpirationDate);
                            }
                            card.SetAttribute("EffectiveDate", CardEffectiveDate);
                            xmlelem.AppendChild(card);
                            member.AppendChild(xmlelem);
                            household.AppendChild(members);

                            xmlelem = xmldoc.CreateElement("Stores");
                            XmlElement store = xmldoc.CreateElement("Store");
                            store.SetAttribute("IsHomeStore", "true");
                            store.SetAttribute("Id", dr["RRC_Store Code"].ToString());
                            store.SetAttribute("StoreTypeId", "1");
                            xmlelem.AppendChild(store);
                            member.AppendChild(xmlelem);
                            household.AppendChild(members);

                            xmlelem = xmldoc.CreateElement("MemberAttributes");
                            XmlElement attribute = xmldoc.CreateElement("Attribute");
                            attribute.SetAttribute("Id", "10005");
                            attribute.SetAttribute("Value", CutString(dr["RRC_Marital Status"].ToString(), 1));
                            xmlelem.AppendChild(attribute);
                            XmlElement attribute1 = xmldoc.CreateElement("Attribute");
                            attribute1.SetAttribute("Id", "10011");
                            attribute1.SetAttribute("Value", CutString(dr["RRC_Type of Application"].ToString(), 1));
                            xmlelem.AppendChild(attribute1);
                            XmlElement attribute2 = xmldoc.CreateElement("Attribute");
                            attribute2.SetAttribute("Id", "10012");
                            attribute2.SetAttribute("Value", CutString(dr["RRC_Old Card No."].ToString(), 20));
                            xmlelem.AppendChild(attribute2);
                            XmlElement attribute3 = xmldoc.CreateElement("Attribute");
                            attribute3.SetAttribute("Id", "10013");
                            attribute3.SetAttribute("Value", dr["RRC_Application Date"] == null ? "" : Convert.ToDateTime(dr["RRC_Application Date"]).ToString("yyyy-MM-ddTHH:mm:ss"));
                            xmlelem.AppendChild(attribute3);
                            XmlElement attribute4 = xmldoc.CreateElement("Attribute");
                            attribute4.SetAttribute("Id", "10014");
                            attribute4.SetAttribute("Value", CutString(CutString(dr["RRC_Batch"].ToString(), 5) + CutString(dr["RRC_Sales Invoice"].ToString(), 15), 15));
                            xmlelem.AppendChild(attribute4);
                            XmlElement attribute5 = xmldoc.CreateElement("Attribute");
                            attribute5.SetAttribute("Id", "10015");
                            attribute5.SetAttribute("Value", CutString(dr["RRC_Business Unit"].ToString(), 25));
                            xmlelem.AppendChild(attribute5);
                            XmlElement attribute6 = xmldoc.CreateElement("Attribute");
                            attribute6.SetAttribute("Id", "10019");
                            attribute6.SetAttribute("Value", CutString(dr["RRC_WorkPhone"].ToString(), 20));
                            xmlelem.AppendChild(attribute6);
                            XmlElement attribute7 = xmldoc.CreateElement("Attribute");
                            attribute7.SetAttribute("Id", "10020");
                            attribute7.SetAttribute("Value", CutString(dr["RRC_Middle Initial"].ToString(), 3));
                            xmlelem.AppendChild(attribute7);
                            member.AppendChild(xmlelem);
                            household.AppendChild(members);

                        }
                        //遍历其中的空值
                        readxml(xmldoc.DocumentElement as XmlNode);

                        xmldoc.Save(strPath.Replace(".csv", ".xml"));

                        xmldoc = null;
                    }
                    else
                    {
                        AdjustmentPoint(strPath, dt, ImportMemberNumber);
                    }
                }
                sr.Close();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog(" Import member error ", "", ex);
                throw ex;
            }
        }

        #region 480 Adjustment Point
        public void AdjustmentPoint(string strPath, DataTable dt, string ImportMemberNumber)
        {
            if (ValidateData(dt))
            {
                InsertDetailTable(dt, ImportMemberNumber);
                Logger.Instance.WriteImportLog("Import Member Detail", "", DateTime.Now, dt.Rows.Count, new List<string> { });
                XmlDocument xmldoc = new XmlDocument();
                XmlDeclaration xmldecl;
                xmldecl = xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                xmldoc.AppendChild(xmldecl);

                //加入一个根元素    
                XmlElement xmlelem;
                xmlelem = xmldoc.CreateElement("", "LoyaltyCustomer", "");
                xmlelem.SetAttribute("Action", "I");
                xmlelem.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                xmldoc.AppendChild(xmlelem);

                XmlNode loyaltycustomer = xmldoc.SelectSingleNode("LoyaltyCustomer");
                XmlElement retailer = xmldoc.CreateElement("Retailer");
                retailer.SetAttribute("Id", CutString(dt.Rows[0]["RetailerID"].ToString(), 24));
                loyaltycustomer.AppendChild(retailer);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    XmlElement household = xmldoc.CreateElement("HouseHold");
                    household.SetAttribute("HouseHoldExternalId", CutString(dr["RRCCRD"].ToString(), 24));
                    retailer.AppendChild(household);

                    xmlelem = xmldoc.CreateElement("Accounts");
                    XmlElement account = xmldoc.CreateElement("Account");
                    account.SetAttribute("Id", "2");
                    account.SetAttribute("EarnValue", CutString(dr["PTS"].ToString(), 24));
                    xmlelem.AppendChild(account);
                    household.AppendChild(xmlelem);

                    XmlElement members = xmldoc.CreateElement("Members");
                    XmlElement member = xmldoc.CreateElement("Member");
                    member.SetAttribute("LastName", CutString(dr["LNAME"].ToString(), 50));
                    member.SetAttribute("IsMainMember", "1");
                    member.SetAttribute("FirstName", CutString(dr["FNAME"].ToString(), 50));
                    member.SetAttribute("MemberExternalId", CutString(dr["RRCCRD"].ToString(), 24));
                    household.AppendChild(members);
                    members.AppendChild(member);

                    //当OLDRRCCRD和CRDStatus同时为空时不需要创建Card节点
                    //if (!string.IsNullOrEmpty(dr["OLDRRCCRD"].ToString()) || !string.IsNullOrEmpty(dr["CRDStatus"].ToString()))
                    //{
                    //    xmlelem = xmldoc.CreateElement("Cards");
                    //    XmlElement card = xmldoc.CreateElement("Card");
                    //    card.SetAttribute("Id", CutString(dr["OLDRRCCRD"].ToString(), 24));
                    //    card.SetAttribute("CardStatus", CutString(dr["CRDStatus"].ToString(), 1));
                    //    xmlelem.AppendChild(card);
                    //    member.AppendChild(xmlelem);
                    //    household.AppendChild(members);
                    //}

                    xmlelem = xmldoc.CreateElement("Stores");
                    XmlElement store = xmldoc.CreateElement("Store");
                    store.SetAttribute("Id", dr["StoreID"].ToString());
                    store.SetAttribute("IsHomeStore", "true");
                    store.SetAttribute("StoreTypeId", "1");
                    xmlelem.AppendChild(store);
                    member.AppendChild(xmlelem);
                    household.AppendChild(members);

                    xmlelem = xmldoc.CreateElement("MemberAttributes");
                    XmlElement attribute = xmldoc.CreateElement("Attribute");
                    attribute.SetAttribute("Id", "10012");
                    attribute.SetAttribute("Value", CutString(dr["OLDCRD"].ToString(), 24));
                    xmlelem.AppendChild(attribute);
                    member.AppendChild(xmlelem);
                    household.AppendChild(members);
                }
                //遍历其中的空值
                RemoveEmptyNode(xmldoc.DocumentElement as XmlNode);
                xmldoc.Save(strPath.Replace(".csv", ".xml"));
                xmldoc = null;
            }
        }
        public bool ValidateData(DataTable dt)
        {
            //先校验文件中是否有相同的两行OLDCRD
            List<KeyValue> storelist = new List<KeyValue>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                storelist.Add(new KeyValue() { Key = (i + 1).ToString(), Value = dt.Rows[i]["OLDCRD"].ToString() });
                if (storelist.Count > 1)
                {
                    for (int j = 0; j < storelist.Count - 1; j++)
                    {
                        if (storelist[j].Value == dt.Rows[i]["OLDCRD"].ToString())
                        {
                            if (i > 0)
                            {
                                Logger.Instance.WriteErrorLog(" Import Member Detail ", string.Format("OLDCRD has the same data in line{0} and line{1}", i, j));
                                throw new Exception(" Import Member Detail :" + string.Format("'OLDCRD' has the same data in line {0} and line {1}", j + 1, i + 1));
                                //return false;
                            }
                        }
                    }
                }
            }
            //再校验数据库中是否存在重复OLDCRD
            Edge.SVA.BLL.Ord_ImportMember_D bll = new SVA.BLL.Ord_ImportMember_D();
            List<Edge.SVA.Model.Ord_ImportMember_D> list = new List<Ord_ImportMember_D>();
            
            foreach (DataRow dr in dt.Rows)
            {
                list = bll.GetModelList("OLDCardNumber='" + dr["OLDCRD"].ToString() + "'");
                if (list.Count > 0)
                {
                    Logger.Instance.WriteErrorLog(" Import Member Detail ", string.Format("OLDCRD: {0}, Data already exists!", dr["OLDCRD"].ToString()));
                    throw new Exception(" Import Member Detail: Data already exists!");
                    //return false;
                }
            }
            return true;
        }
        public void InsertDetailTable(DataTable dt, string ImportMemberNumber)
        {
            Edge.SVA.BLL.Ord_ImportMember_D bll = new SVA.BLL.Ord_ImportMember_D();
            foreach (DataRow dr in dt.Rows)
            {
                Edge.SVA.Model.Ord_ImportMember_D model = new Ord_ImportMember_D();
                model.ImportMemberNumber = ImportMemberNumber;
                model.RRCCardNumber = dr["RRCCRD"].ToString();
                model.OLDCardNumber = dr["OLDCRD"].ToString();
                //model.OLDRRCCardNumber = dr["OLDRRCCRD"].ToString();
                model.LName = dr["LName"].ToString();
                model.FName = dr["FName"].ToString();
                model.VPPTS = dr["PTS"].ToString();
                model.RetailID = dr["RetailerID"].ToString();
                model.StoreID = dr["StoreID"].ToString();
                //if (string.IsNullOrEmpty(dr["CRDStatus"].ToString()))
                //{
                //    model.CardStatus = null;
                //}
                //else
                //{
                //    model.CardStatus = ConvertTool.ToInt(dr["CRDStatus"].ToString());
                //}
                bll.Add(model);
            }
        }
        public void RemoveEmptyNode(XmlNode xn)
        {
            List<XmlAttribute> attlist = new List<XmlAttribute>();
            foreach (XmlAttribute item in xn.Attributes)
            {
                if (string.IsNullOrEmpty(item.Value.Trim()))
                {
                    attlist.Add(item);
                }
            }
            foreach (var item in attlist)
            {
                xn.Attributes.Remove(item);
            }

            if (xn.HasChildNodes)
            {
                foreach (XmlNode item in xn.ChildNodes)
                {
                    RemoveEmptyNode(item);
                }
            }
        }
        #endregion

        //递归遍历XML中的属性,其值为空时要删除相应的属性
        public void readxml(XmlNode xn)
        {
            if (xn.Attributes != null)
            {
                List<XmlAttribute> attlist = new List<XmlAttribute>();
                if (xn.Name != "Attribute")//当节点为Attribute时,保存其属性的完整
                {
                    foreach (XmlAttribute item in xn.Attributes)
                    {
                        if (string.IsNullOrEmpty(item.Value.Trim()))
                        {
                            attlist.Add(item);
                        }
                    }
                }
                foreach (var item in attlist)
                {
                    xn.Attributes.Remove(item);
                }
            }
            if (xn.HasChildNodes)
            {
                foreach (var item in xn.ChildNodes)
                {
                    readxml(xn.FirstChild);
                }
            }
        }

        //从HQDB中读取HouseHoldExternalId的值
        public string connString { get; set; }
        public string sqlString { get; set; }
        public string sqlString2 { get; set; }
        public string GetHouseHoldExternalId(string strcon, string strsql)
        {
            string HouseHoldExternalId = "";
            try
            {
                SqlConnection conn = new SqlConnection(strcon);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = strsql;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Close();
                DataTable dt = ds.Tables[0];


                if (dt != null && dt.Rows.Count > 0)
                {
                    HouseHoldExternalId = dt.Rows[0]["HouseHold ExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                HouseHoldExternalId = "";
                Logger.Instance.WriteErrorLog("GetHouseHoldExternalId", ex.Message);
            }

            return HouseHoldExternalId;
        }
        public string GetExpirationDate(string strcon, string strsql)
        {
            string ExpirationDate = "";
            try
            {
                SqlConnection conn = new SqlConnection(strcon);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = strsql;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Close();
                DataTable dt = ds.Tables[0];


                if (dt != null && dt.Rows.Count > 0)
                {
                    ExpirationDate = dt.Rows[0]["ExpirationDate"].ToString();
                    ExpirationDate = (Convert.ToDateTime(ExpirationDate)).AddYears(2).ToString("yyyy-MM-ddTHH:mm:ss");
                }
            }
            catch (Exception ex)
            {
                ExpirationDate = "";
                Logger.Instance.WriteErrorLog("GetExpirationDate", ex.Message);
            }

            return ExpirationDate;
        }

        //格式列表中显示CSV文件的名称
        public void ShowCSVFileName(DataSet ds, string newColumn, string refKey)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = row[refKey].ToString().Substring(row[refKey].ToString().LastIndexOf('/') + 1);
            }
        }

        //判断字符串中"的个数是否为奇数
        public bool IsValidate(string str)
        {
            int cout = 0;
            for (int n = 0; n < str.Length; n++)
            {
                if (str[n].Equals('\"'))//检索字符串中是否包含多个"
                {
                    cout++;
                }
            }
            if (cout % 2 != 0)
            {
                return false;
            }
            return true;
        }
        public string CutString(string str, int len)
        {
            string rtn;
            if (str.Length <= len)
            {
                rtn = str;
            }
            else
            {
                rtn = str.Substring(0, len);
            }
            return rtn;
        }
    }
}