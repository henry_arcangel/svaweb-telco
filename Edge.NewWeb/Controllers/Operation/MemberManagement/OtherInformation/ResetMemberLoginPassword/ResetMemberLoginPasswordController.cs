﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation.MemberManagement.OtherInformation.ResetMemberLoginPassword;
using System.Data;
using Edge.SVA.Model.Domain.Operation;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Text;
using Edge.DBUtility;

namespace Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.ResetMemberLoginPassword
{
    public class ResetMemberLoginPasswordController
    {
        protected ResetMemberLoginPasswordViewModel viewModel = new ResetMemberLoginPasswordViewModel();

        public ResetMemberLoginPasswordViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int MemberID)
        {
            string strSql = @"select  distinct a.MemberEngFamilyName,a.MemberEngGivenName,a.MemberID,isnull(a.MemberMobilePhone,'') as MemberMobilePhone,a.CountryCode
                                from Member a inner join Card b on a.MemberID = b.MemberID  where MemberID=" + MemberID;
            DataSet ds = DBUtility.DbHelperSQL.Query(strSql) == null ? null : DBUtility.DbHelperSQL.Query(strSql);
            DataTable dt = ds.Tables[0];
            if (dt != null)
            {
                //基本信息
                viewModel.CardNumber = dt.Rows[0]["CardNumber"].ToString();
                viewModel.CardTypeID = Convert.ToInt32(dt.Rows[0]["CardTypeID"].ToString());
                viewModel.CardGradeID = Convert.ToInt32(dt.Rows[0]["CardGradeID"].ToString());
                viewModel.MemberMobilePhone = dt.Rows[0]["MemberMobilePhone"].ToString();
                viewModel.CountryCode = dt.Rows[0]["CountryCode"].ToString();
                //消息形式
                Edge.SVA.BLL.MemberMessageAccount bll = new SVA.BLL.MemberMessageAccount();
                List<Edge.SVA.Model.MemberMessageAccount> list = bll.GetModelList("MemberID=" + dt.Rows[0]["MemberID"].ToString());
                foreach (var item in list)
                {
                    switch (item.MessageServiceTypeID)//4:msn,5:qq,7:facebook,8:sina
                    {
                        case 1:
                            viewModel.Email = item.AccountNumber;
                            break;
                        case 2:
                            viewModel.Message = item.AccountNumber;
                            break;
                        case 4:
                            viewModel.MSNNumber = item.AccountNumber;
                            break;
                        case 5:
                            viewModel.QQNumber = item.AccountNumber;
                            break;
                        case 6:
                            viewModel.APP = item.AccountNumber;
                            break;
                        case 7:
                            viewModel.FacebookNumber = item.AccountNumber;
                            break;
                        case 8:
                            viewModel.SinaNumber = item.AccountNumber;
                            break;
                    }
                }
            }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;

            //获得总条数
            recodeCount = this.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "MemberMobilePhone";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = this.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            return ds;
        }

        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by " + orderby);
            }
            else
            {
                strSql.Append("order by MemberMobilePhone desc");
            }
            strSql.Append(@")AS Row, * from ( select  distinct a.MemberEngFamilyName,a.MemberEngGivenName,a.MemberID,
                            isnull(a.MemberMobilePhone,'') as MemberMobilePhone,a.CountryCode,MemberRegisterMobile 
                        from Member a inner join Card b on a.MemberID = b.MemberID ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) T ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        public int GetRecordCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(@"select count(*) from ( select  distinct a.MemberEngFamilyName,a.MemberEngGivenName,a.MemberID,
                        isnull(a.MemberMobilePhone,'') as MemberMobilePhone,a.CountryCode,MemberRegisterMobile 
                    from Member a inner join Card b on a.MemberID = b.MemberID ");
 
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0} ", strWhere);
            }
            sql.Append(" ) T");

            return DbHelperSQL.GetCount(sql.ToString());
        }


    }
}