﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation.MemberManagement.MemberInformation;
using System.Data;
using System.Text;

namespace Edge.Web.Controllers.Operation.MemberManagement
{
    public class MemberController
    {
        protected MemberViewModel viewModel = new MemberViewModel();

        public MemberViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            Edge.SVA.BLL.Member bll = new Edge.SVA.BLL.Member();

            DataSet ds = new DataSet();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "MemberID";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            return ds;
        }

        public DataSet GetCardList(int MemberID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select BrandID,a.CardTypeID,CardGradeID,CardNumber,TotalPoints,TotalAmount ");
            sb.Append("from Card a ");
            sb.Append("join Member b on a.MemberID = b.MemberID ");
            sb.Append("join CardType c on a.CardTypeID = c.CardTypeID ");
            sb.Append("where b.MemberID = " + MemberID);

            DataSet ds = DBUtility.DbHelperSQL.Query(sb.ToString());
            if (ds != null && ds.Tables.Count > 0)
            {
                Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
                Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            }

            return ds;
        }
    }
}