﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers;
using System.Data;
using Edge.Web.Tools;
using Edge.Common;
using FineUI;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.MobileTopUp
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        //JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    bool isSuccess = false;

                    if (CallSocket(idList) == 0)
                    {
                        foreach (string id in idList)
                        {
                            Edge.SVA.Model.Ord_CardAdjust_H mode = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(id);

                            DataRow dr = dt.NewRow();
                            dr["TxnNo"] = id;
                            dr["ApproveCode"] = CardController.ApproveCardForApproveCode(mode, CardController.OprID.TelcoMobileTopup, out isSuccess);
                            if (isSuccess)
                            {
                                Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Active " + mode.CardAdjustNumber + " " + Resources.MessageTips.ApproveCode);


                                dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                            }
                            else
                            {
                                Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Active " + mode.CardAdjustNumber + " " + Resources.MessageTips.ApproveError);

                                dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                            }
                            dt.Rows.Add(dr);
                        }
                        this.Grid1.DataSource = dt;
                        this.Grid1.DataBind();
                    }
                    else
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve:CallSocket Fail");
                        Alert.ShowInTop("CallSocket Fail", "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        protected int CallSocket(List<string> idlist)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();


                dt.Columns.Add("OpCode", typeof(string));
                dt.Columns.Add("businessType", typeof(string));
                dt.Columns.Add("xmlns", typeof(string));
                dt.Columns.Add("StoreId", typeof(string));
                dt.Columns.Add("PosId", typeof(string));
                dt.Columns.Add("TxnDate", typeof(string));
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("SN", typeof(string));
                dt.Columns.Add("TargetSN", typeof(string));
                dt.Columns.Add("Amount", typeof(string));
                dt.Columns.Add("TotalAmount", typeof(string));

                dt.Columns.Add("ItemUID", typeof(string));  //Added by Roche
                dt.Columns.Add("TransactionType", typeof(string)); //Added by Roche
                dt.Columns.Add("ProdCode", typeof(string)); // Added by Roche

                dt.Columns.Add("MobileNumber", typeof(string));
                dt.Columns.Add("CashierId", typeof(string));
                dt.Columns.Add("TxnType", typeof(string));
                dt.Columns.Add("BrandCode", typeof(string));
                dt.Columns.Add("CardUID", typeof(string));
                dt.Columns.Add("SKU", typeof(string));



                foreach (string id in idlist)
                {
                    Edge.SVA.Model.Ord_CardAdjust_H mode = new Edge.SVA.BLL.Ord_CardAdjust_H().GetModel(id);
                    if (!(mode == null))
                    {
                        string wherestr = "";
                        wherestr = "CardAdjustNumber='" + id + "'";

                        DataSet dsdetail = new Edge.SVA.BLL.Ord_CardAdjust_D().GetList(wherestr);

                        DataTable dataProdCode = new DataTable();

                        foreach (DataRow drdetail in dsdetail.Tables[0].Rows)
                        {
                            string[] temp = drdetail["Additional1"].ToString().Split(',');


                            DataRow dr = dt.NewRow();

                            dr["OpCode"] = "100";
                            dr["businessType"] = "TelecoServer";
                            dr["xmlns"] = "http://www.tap.org/gc/beans";
                            dr["StoreId"] = mode.StoreCode;
                            dr["PosId"] = mode.RegisterCode;
                            dr["TxnDate"] = Convert.ToDateTime(mode.TxnDate).ToString("yyyy-MM-dd HH:mm:ss");
                            dr["TxnNo"] = mode.OriginalTxnNo;
                            dr["SN"] = mode.OriginalTxnNo; 
                            dr["TargetSN"] = mode.OriginalTxnNo;
                            dr["Amount"] = Convert.ToInt16(Math.Abs(Convert.ToDecimal(drdetail["OrderAmount"]))) * 100; 
                            dr["TotalAmount"] = Convert.ToInt16(Math.Abs(Convert.ToDecimal(drdetail["OrderAmount"]))) * 100; 

                            //dataProdCode = GetProdCodeBySKU(temp[0].ToString());
                           // DataRow row = dataProdCode.Rows[0];

                            //dr["ItemUID"] = validatedForItemUID(drdetail["Additional1"].ToString());
                            //dr["TransactionType"] = validatedForTransactionType(drdetail["Additional1"].ToString());
                            ////dr["ItemUID"] = row["ProdCode"]; //
                            //dr["ProdCode"] = temp[0]; //Added by Roche


                            dr["ItemUID"] = temp[1];
                            dr["TransactionType"] = temp[2];
                            dr["ProdCode"] = temp[0]; //Added by Roche


                            dr["MobileNumber"] = temp[1];
                            dr["CashierId"] = mode.CreatedBy;
                            dr["TxnType"] = "Confirm";
                            dr["BrandCode"] = mode.BrandCode;
                            dr["CardUID"] = drdetail["CardNumber"];
                            dr["SKU"] = temp[0];
                            dt.Rows.Add(dr);
                        }
                        string err = "";
                        string sendstr = "";
                        CFSXMLFormat s = new CFSXMLFormat(); //dito yun pre!
                        sendstr = s.GetRequestStr(dt, ref err);
                        if ((err == "") && (sendstr != ""))
                        {
                            string RespStr = "";
                            string TelcoSocketIP = System.Configuration.ConfigurationManager.AppSettings["TelcoSocketIP"].ToString();
                            int TelcoSocketPort = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TelcoSocketPort"]);
                            Tools.Logger.Instance.WriteOperationLog("CallSocket", "send string:" + sendstr);
                            RespStr = GetSocket.SocketSendReceive(TelcoSocketIP, TelcoSocketPort, sendstr);
                            Tools.Logger.Instance.WriteOperationLog("CallSocket", "Response string:" + RespStr);
                            DataTable resultdt = s.AnalyResponseStr(RespStr, ref err);
                            if (err == "")
                            {
                                Tools.Logger.Instance.WriteErrorLog("CallSocket", "Service return ResponseCode:" + resultdt.Rows[0]["ResponseCode"]);
                                if (resultdt.Rows[0]["ResponseCode"].ToString().Trim() != "0")
                                {                                    
                                    Tools.Logger.Instance.WriteErrorLog("CallSocket", "Service return error:" + resultdt.Rows[0]["ErrorMessage"]);
                                    return -1;
                                }
                                else
                                    return 0;
                            }
                            else
                            {
                                Tools.Logger.Instance.WriteErrorLog("CallSocket", "AnalyResponseStr fail:" + err);
                                return -1;
                            }
                        }
                        else
                        {
                            Tools.Logger.Instance.WriteErrorLog("CallSocket", "GetRequestStr fail:" + err);
                            return -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("CallSocket", "error", ex);
                return -1;
            }

            return 0;
        }


        //AddedByRoche for ProdCode in socket

        public DataTable GetProdCodeBySKU(string sku)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    SqlCommand queryForProdCode = new SqlCommand("SELECT ProdCode FROM ImportGM WHERE SKU = @sku", conn);

                    queryForProdCode.Parameters.AddWithValue("@sku", sku);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = queryForProdCode;
                    adapter.Fill(dt);

                    return dt;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }


        public string validatedForItemUID(string additional1)
        {
            if (string.IsNullOrEmpty(additional1))
            {
                return null;
            }
            else
            {
                string item_UID = additional1.Substring(additional1.IndexOf(",") + 1, 11);
                return additional1.Substring(additional1.IndexOf(",") + 1, 11);
            }
        }

        public string validatedForTransactionType(string additional1)
        {
            if (string.IsNullOrEmpty(additional1))
            {
                return null;
            }
            else
            {
                string transactionType = additional1.Substring(additional1.LastIndexOf(",") + 1);
                return transactionType;
            }
        }




        //Until here



        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {

        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            //if (e.DataItem is DataRowView)
            //{
            //    DataRowView drv = e.DataItem as DataRowView;
            //    string approveStatus = drv["ApproveStatus"].ToString().Trim();
            //    FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

            //    if (approveStatus != "")
            //    {
            //        approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
            //        switch (approveStatus)
            //        {
            //            case "A":
            //                editWF.Enabled = false;
            //                break;
            //            case "P":
            //                editWF.Enabled = true;
            //                (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
            //                break;
            //            case "V":
            //                editWF.Enabled = false;
            //                (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
            //                break;
            //        }
            //    }
            //}
        }
    }
}
