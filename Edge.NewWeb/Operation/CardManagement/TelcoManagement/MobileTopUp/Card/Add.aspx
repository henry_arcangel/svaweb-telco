﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.TelcoManagement.MobileTopUp.Card.Add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript">
        function keydown(e) {
            var currKey = 0, e = e || event; currKey = e.keyCode || e.which || e.charCode;
            //支持IE、FF 
            if (currKey == 13) { document.getElementById('SimpleForm1_Toolbar3_BtnAddScan').click(); }
        }
        document.onkeydown = keydown;
        //onkeydown事件调用方式 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
        <ext:SimpleForm ID="SimpleForm1" ShowBorder="true" ShowHeader="false" runat="server"
            BodyPadding="20px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
            LabelAlign="Right">
            <Toolbars>
                    <ext:Toolbar ID="Toolbar3" runat="server">
                        <Items>
                            <ext:Button ID="btnCloseSearch" Icon="SystemClose" runat="server" Text="关闭">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnAddSearchItem" Icon="Add" runat="server" Text="添加" OnClick="btnAddSearchItem_Click">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnSearch" Icon="Find" OnClick="btnSearch_Click" runat="server" Text="搜 索" ValidateForms="Form3">
                            </ext:Button>                            
                            <ext:ToolbarFill ID="ToolbarFill4" runat="server">
                            </ext:ToolbarFill>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="筛选条件"
                        AutoHeight="true" AutoWidth="true">
                        <Items>
                            <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                           <ext:DropDownList ID="CardTypeID" runat="server" AutoPostBack="true" Label="卡类型："
                                                Resizable="true" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" 
                                                ShowRedStar="true" Required="true">
                                            </ext:DropDownList>
                                            <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别：" AutoPostBack="true"
                                                Resizable="true"  OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged"
                                                ShowRedStar="true" Required="true">
                                            </ext:DropDownList>     
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:DropDownList ID="Product" runat="server" AutoPostBack="true" Label="Product："
                                                  Resizable="true" OnSelectedIndexChanged="Product_SelectedIndexChanged"
                                                  ShowRedStar="true" Required="true">
                                            </ext:DropDownList>
                                            <ext:TextBox ID="ActAmount" Hidden="true"/>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:TextBox ID="NumberMask" Hidden="true" />
                                            <ext:TextBox ID="NumberPrefix" Hidden="true" />
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="添加选项"
                        AutoHeight="true" AutoWidth="true">
                        <Items>
                            <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:NumberBox ID="Amount" runat="server" Label="金额：" MaxValue="100000000" Required="true"
                                                ShowRedStar="true" NoDecimal="true" NoNegative="true">
                                            </ext:NumberBox>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:TextBox ID="MobileNumber" runat="server" Label="Mobile Number：" MaxLength="20" 
                                                ShowRedStar="true" Required="true">
                                            </ext:TextBox>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="卡搜索结果列表"
                        AutoHeight="true" AutoWidth="true" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel11" runat="Server" BodyPadding="3px" EnableBackgroundColor="true" ShowHeader="false" ShowBorder="false" Layout="Column">
                                <Items>
                                    <ext:CheckBox ID="cbSearchAll" runat="server" AutoPostBack="true" OnCheckedChanged="cbSearchAll_OnCheckedChanged" Text="添加所有搜索结果">
                                    </ext:CheckBox>
                                </Items>
                               
                            </ext:Panel>
                            <ext:Grid ID="SearchListGrid" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                                PageSize="3" runat="server" EnableCheckBoxSelect="true" DataKeyNames="CardNumber"
                                AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                                ForceFitAllTime="true" OnPageIndexChange="RegisterGrid_OnPageIndexChange" ClearSelectedRowsAfterPaging="false">
                           <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill1" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="lblTotal" runat="server" Text="面额汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalDenomination" runat="server" Text="0.00">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                            </Toolbars>
                                <Columns>
                                    <ext:TemplateField Width="130px" HeaderText="卡号码">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField Width="130px" HeaderText="卡物理编号">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("CardUID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField Width="60px" HeaderText="卡级别">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField Width="60px" HeaderText="面额">
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("TotalAmount","{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                </Items>
        </ext:SimpleForm>
        <ext:HiddenField ID="hfSelectedIDS" runat="server">
        </ext:HiddenField>
        <ext:Window ID="WindowContact" Title="" Popup="false" EnableIFrame="true" runat="server"
            CloseAction="HidePostBack" OnClose="WindowContact_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
            Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
        </ext:Window> 
        <ext:Window ID="WindowEnough" Title="" Popup="false" EnableIFrame="true" runat="server"
            CloseAction="HidePostBack" OnClose="WindowEnough_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
            Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
        </ext:Window>
    </form>
</body>
</html>
