﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardActive;
using Edge.Web.Controllers;
using System.Text.RegularExpressions;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.MobileTopUp;
using System.Data.SqlClient;

using System.Configuration;


namespace Edge.Web.Operation.CardManagement.TelcoManagement.MobileTopUp.Card
{
    public partial class Add : PageBase
    {



        MobileTopUpController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                CardTypeID_SelectedIndexChanged(null, null);

                RegisterCloseEvent(this.btnCloseSearch);
            }
            controller = SVASessionInfo.MobileTopUpController;
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {

            SyncSelectedRowIndexArrayToHiddenField();
            List<string> idList = GetSelectedRowIndexArrayFromHiddenField();

            if (ViewState["SearchResult"] != null)
            {
                if (controller.ViewModel.CardTable == null)
                {
                    controller.ViewModel.CardTable = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = controller.ViewModel.CardTable;
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    for (int i = 0; i < idList.Count; i++)
                    {
                        ids += string.Format("{0},", "'" + idList[i].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    ViewState["SearchResult"] = dtSearch;

                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];

                    //ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();

                    cbSearchAll.Checked = false;
                }

                #region 验证金额是否一致
                DataTable dt = ViewState["SearchResult"] as DataTable;
                if (this.IsOpenAmount && dt.Rows.Count > 0)
                {
                    int j = dt.Rows.Count - 1;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        double startAmount = string.IsNullOrEmpty(dt.Rows[i]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[i]["TotalAmount"]);
                        double endAmount = string.IsNullOrEmpty(dt.Rows[j]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[j]["TotalAmount"]);
                        if (startAmount != endAmount)
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90492"));
                            return;
                        }
                    }

                }
                #endregion

                DataTable addDT = controller.ViewModel.CardTable;

                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");

                controller.ViewModel.CardTable = newSearchDT;

                //返回界面时要清空查询记录
                ViewState["SearchResult"] = null;
            }
            CloseAndPostBack();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);

            #region 检查手机号码
            if (CheckMobileNumber(this.MobileNumber.Text, this.NumberMask.Text, this.NumberPrefix.Text) == false)
            {
                ShowWarning(Resources.MessageTips.AddFailed);
                return;
            }
            #endregion

            if (!ValidaData())
            {
                return;
            }

            Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            Edge.SVA.Model.CardUIDMap modelmap = null;
            DataSet ds = new DataSet();

            //
            if (!IsContinue(GetSearchDataTable(), false))
            {
                return;
            }
            this.SearchListGrid.PageIndex = 0;
            DataTable dt = GetSearchDataTable();

            ViewState["SearchResult"] = dt;
            BindCardGrid();
        }

        private void ChangeControlStatus(bool status)
        {
            this.btnSearch.Enabled = !status;
            this.CardTypeID.Enabled = !status;
            this.CardTypeID.SelectedIndex = 0;
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Edge.SVA.Model.CardType ctype = new Edge.SVA.BLL.CardType().GetModel(int.Parse(CardTypeID.SelectedValue));
                Edge.SVA.Model.CardGrade cgrade = new Edge.SVA.BLL.CardGrade().GetModel(int.Parse(CardGradeID.SelectedValue));
                Tools.ControlTool.BindImportGM(this.Product, ctype.CardTypeCode, cgrade.CardGradeCode); //nag edit ako dito
            }
        }
        protected void Product_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Product.SelectedValue != "-1")
            {
                //removed by jay
                //Edge.SVA.Model.ImportGM im = new Edge.SVA.BLL.ImportGM().GetModel(Convert.ToString(this.Product.SelectedValue));

                //mypart
                Edge.Web.Tools.ControlTool ct = new Edge.Web.Tools.ControlTool();
                Edge.SVA.Model.ImportGM im = new Edge.SVA.Model.ImportGM();

                foreach (DataRow dr in ct.selectedSKU(this.Product.SelectedValue.ToString()).Rows)
                {
                    //im.ProdCode = dr[0].ToString();
                    im.SKUUnitAmount = Convert.ToDecimal(dr[0].ToString());
                    im.GMValue = Convert.ToDecimal(dr[1].ToString());
                    im.NumberMask = dr[2].ToString();
                    im.NumberPrefix = dr[3].ToString();

                    im.ProdCode = dr[4].ToString();

                    Amount.Text = im.SKUUnitAmount.ToString();

                    decimal GMValue = 100 - (decimal)im.GMValue;
                    decimal vat = (decimal)(GMValue * (decimal)0.01);
                    ActAmount.Text = vat.ToString();
                    NumberMask.Text = im.NumberMask;
                    NumberPrefix.Text = im.NumberPrefix;
                }


                //Edge.SVA.Model.ImportGM im = new Edge.SVA.BLL.ImportGM().GetModelProd(Convert.ToString(this.Product.SelectedValue));
                //Amount.Text = im.SKUUnitAmount.ToString(); Done
                //decimal GMValue = 100 - (decimal)im.GMValue; 
                //decimal vat = (decimal)(GMValue * (decimal)0.01);
                //ActAmount.Text = vat.ToString();   //(decimal.Multiply((decimal)im.SKUUnitAmount, vat)).ToString();
                //NumberMask.Text = im.NumberMask;
                //NumberPrefix.Text = im.NumberPrefix;

            }
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindCardGrid();
        }

        private DataTable GetSearchDataTable()
        {
            try
            {
                Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
                string cardTypeID = CardTypeID.SelectedValue;
                string cardGradeID = CardGradeID.SelectedValue;
                string strWhere = string.Format(" Card.Status in ( {0} )", (int)CardController.CardStatus.Active);
                strWhere += " and Card.IssueStoreID=" + Request["StoreID"];
                string filedOrder = " Card.CardNumber ASC ";

                if (cardGradeID == "-1")
                {
                    cardGradeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
                }

                strWhere = GetCardSearchStrWhere(1, 0, "", cardGradeID, "", strWhere);

                //Display message
                int count = bll.GetCount(strWhere);

                if (count <= 0)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return null;
                }

                //add by gavin @2015-03-02 只有一行记录
                DataSet ds = bll.GetListForBatchOperation(1, strWhere, filedOrder);
                DataTable dt = ds.Tables[0];
                dt.Columns.Add("Additional1", typeof(string));
                dt.Columns.Add("ActAmount", typeof(float));
                dt.Columns.Add("NumberMask", typeof(string));
                dt.Columns.Add("NumberPrefix", typeof(string));

                DataRow dr = dt.Rows[0];
                dr["TotalAmount"] = decimal.Parse(this.Amount.Text);

                dr["Additional1"] = getProdCodeFromSelectedSKU(this.Product.SelectedValue) + "," + this.MobileNumber.Text + "," + getTransactionTypeFromSelectedSKU(this.Product.SelectedValue); //ito na yun

                dr["ActAmount"] = (decimal.Multiply((decimal)dr["TotalAmount"], decimal.Parse(this.ActAmount.Text))).ToString();
                dr["NumberMask"] = this.NumberMask.Text;
                dr["NumberPrefix"] = this.NumberPrefix.Text;
                ViewState["SearchResult"] = ds.Tables[0];
                return ds.Tables[0];
            }
            catch
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
        }

        public string getProdCodeFromSelectedSKU(string Thiskey)
        {
            Edge.Web.Tools.ControlTool ct = new Edge.Web.Tools.ControlTool();

            foreach(DataRow dr in ct.selectedSKU(Thiskey).Rows)
            {
                return dr[4].ToString();
            }
            return null;
        }


        public string getTransactionTypeFromSelectedSKU(string ThisKey2)
        {
            Edge.Web.Tools.ControlTool ct = new Edge.Web.Tools.ControlTool();

            foreach(DataRow dr in ct.selectedSKU(ThisKey2).Rows)
            {
                return dr[5].ToString();
            }
            return null;

        }


        private void BindCardGrid()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();

                //判断CardAmount
                if (dt == null) return;

                if (dt.Rows.Count <= 0) return;

                //Add by Nathan 20140820 ++
                int cardGradeID = Tools.ConvertTool.ToInt(this.CardGradeID.SelectedValue);
                Edge.SVA.BLL.CardGrade blltype = new SVA.BLL.CardGrade();
                Edge.SVA.Model.CardGrade modeltype = cardGradeID == 0 ? null : blltype.GetModel(cardGradeID);

                //Add by Nathan 20140820 --
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }
        }

        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            BindCardGrid();

            UpdateSelectedRowIndexArray();
            if (cbSearchAll.Checked)
            {
                SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            }
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            if (SearchListGrid.SelectedRowIndexArray != null && SearchListGrid.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(SearchListGrid.SelectedRowIndexArray);
                for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
                {
                    string id = SearchListGrid.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
            {
                string id = SearchListGrid.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i);
                }
            }
            SearchListGrid.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion

        public bool IsOpenAmount
        {
            get
            {
                if (ViewState["IsOpenAmount"] == null) return false;

                if (ViewState["IsOpenAmount"] is bool) return (bool)ViewState["IsOpenAmount"];

                return false;
            }
            set
            {
                ViewState["IsOpenAmount"] = value;
            }
        }
        protected void cbSearchAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            if (!cbSearchAll.Checked)
            {
                hfSelectedIDS.Text = string.Empty;
            }
        }

        protected bool ValidaData()
        {
            int top = 1;
            string cardGradeID = CardGradeID.SelectedValue;

            if (cardGradeID == "-1")
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }
            if (Amount.Text == "")
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }
            if (MobileNumber.Text == "")
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }
            if (top > webset.MaxSearchNum)
            {

                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                return false;
            }

            return true;
        }

        protected bool IsContinue(DataTable dt, bool IsScan)
        {
            return true;
        }

        protected void WindowContact_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            BindCardGrid();
        }

        protected void WindowEnough_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            if (Convert.ToBoolean(ViewState["IsScan"]))
            {
                ShowTable();
            }
            BindCardGrid();
        }

        protected bool ValidataStore(Edge.SVA.Model.CardUIDMap model)
        {
            if (model == null)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return false;
            }
            Edge.SVA.BLL.CardGradeStoreCondition bll = new SVA.BLL.CardGradeStoreCondition();
            DataTable dt = bll.GetList(" CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 3 ").Tables[0];

            int i = 0;
            while (dt.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dt.Rows[i]["ConditionID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }
            Edge.SVA.BLL.Store bllstore = new SVA.BLL.Store();
            DataTable dtstore = bllstore.GetList(" BrandID in (select ConditionID from CardGradeStoreCondition where CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 1)").Tables[0];
            i = 0;
            while (dtstore.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dtstore.Rows[i]["StoreID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }

            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90404"), "CardNumber"));
            return false;
        }

        public void ShowTable()
        {
            DataTable newdt = ViewState["Newds"] as DataTable;
            DataTable showdt = new DataTable();
            if (ViewState["SearchResult"] != null)
            {
                DataTable dt = ViewState["SearchResult"] as DataTable;
                showdt = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(dt, newdt, "CardUID");
            }
            else
            {
                showdt = newdt;
            }

            ViewState["Newds"] = null;
            ViewState["SearchResult"] = showdt;
        }

        public bool CheckMobileNumber(string MobileNo, string nmask, string nprefix)
        {
            int i = 0;
            string mobileprefix = "";

            if ((nmask == "") || (nprefix == ""))
                return true;

            if (MobileNo.Length != nmask.Length)
                return false;

            while (i < nmask.Length)
            {
                if (nmask.Substring(i, 1) == "A")
                {
                    mobileprefix = mobileprefix + MobileNo.Substring(i, 1);
                }
                i = i + 1;
            }
            if (nprefix.IndexOf(mobileprefix) >= 0)
                return true;
            else
                return false;
        }
    }
}