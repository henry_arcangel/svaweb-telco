﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.WalletEnquiry
{
    public partial class Show : PageBase
    {

        private const string fields = "ServerCode,StoreID,RegisterCode,OprID,RefTxnNo,BusDate,Txndate,ApprovalCode";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.ResultGrid.PageSize = webset.ContentPageNum;

                SetObject();

                string strWhere = string.Format("CardNumber = '{0}'", Request.Params["id"]);
                RptBind(strWhere, "CardNumber", "");

                btnClose.OnClientClick = FineUI.ActiveWindow.GetConfirmHideReference();
            }
        }

        protected void ResultGrid_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.ResultGrid.PageIndex = e.NewPageIndex;
            string strWhere = string.Format("CardNumber = '{0}'", Request.Params["id"]);
            RptBind(strWhere, "KeyID", "");
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby, string fields)
        {

            Edge.SVA.BLL.Card_Movement bll = new Edge.SVA.BLL.Card_Movement();

            //获得总条数
            this.ResultGrid.RecordCount = bll.GetCount(strWhere);

            DataSet ds = bll.GetList(this.ResultGrid.PageSize, ResultGrid.PageIndex, strWhere, orderby);

            Tools.DataTool.AddID(ds, "IDNumber", this.ResultGrid.PageSize, ResultGrid.PageIndex);
            Tools.DataTool.AddTxnTypeName(ds, "OprIDName", "OprID");
            Tools.DataTool.AddColumn(ds, "BrandCode", this.BrandCode);
            Tools.DataTool.AddStoreCode(ds, "StoreCode", "StoreID");

            Tools.DataTool.AddAmount(ds, "AmountPoints", "OprID");

            this.ResultGrid.DataSource = ds.Tables[0].DefaultView;
            this.ResultGrid.DataBind();
        }
        #endregion

        private void SetObject()
        {
            this.CardTypeID.Text = Request.Params["CardTypeName"];// SVASessionInfo.CardTypeCodeName;//Request.Params["CardTypeName"];
            this.CardGradID.Text = Request.Params["CardGradeName"];//SVASessionInfo.CardGradeCodeName;//Request.Params["CardGradeName"];
            this.CardNumber.Text = Request.Params["id"];
            this.BatchCardID.Text = Request.Params["BatchCode"];
            this.VendorCardNumber.Text = Request.Params["VendorCardNumber"];
            this.Status.Text = Request.Params["StatusName"];
            ////Todo Fixed show format
            //this.TotalAmount.Text = Request.Params["TotalAmount"].ToString();
            //this.TotalPoints.Text = Request.Params["TotalPoints"].ToString();
            try
            {
                this.TotalAmount.Text = Convert.ToDecimal(Request.Params["TotalAmount"].ToString()).ToString("F2");
            }
            catch
            {
                this.TotalAmount.Text = "0.00";
            }
            try
            {
                this.TotalPoints.Text = Convert.ToInt32(Request.Params["TotalPoints"].ToString()).ToString();
            }
            catch
            {
                this.TotalPoints.Text = "0";
            }
            this.CardIssueDate.Text = Request.Params["CreatedOn"];
            this.CardExpiryDate.Text = Request.Params["CardExpiryDate"];

            int memberID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["MemberID"]);
            int cardTypetID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["CardTypeID"]);

            Edge.SVA.Model.CardType cardType = new Edge.SVA.BLL.CardType().GetModel(cardTypetID);
            Edge.SVA.Model.Brand brand = cardType == null ? null : new Edge.SVA.BLL.Brand().GetModel(cardType.BrandID);
            this.BrandCode = brand == null ? null : brand.BrandCode;

        }

        private string BrandCode
        {
            get
            {
                if (ViewState["BrandCode"] == null) return "";
                return ViewState["BrandCode"].ToString();
            }
            set
            {
                ViewState["BrandCode"] = value;
            }
        }

        //added by me
        public string MobileNumber(string valve)
        {
            int vald = validateMobileNumber(valve);
            
            if (vald == 0)
            {
                string mobile = valve.Substring(valve.IndexOf(",") + 1, 11);
                return mobile;
            }
            else
            {
                return null;
            }
            
            
        }

        public int validateMobileNumber(string m_number)
        {
            if (String.IsNullOrEmpty(m_number))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


       
        

    }
}
