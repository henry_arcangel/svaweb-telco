﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.TelcoStoreWalletTransfer;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.TelcoStoreWalletTransfer
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardTransfer_H, Edge.SVA.Model.Ord_CardTransfer_H>
    {
        TelcoStoreWalletTransferController controller = new TelcoStoreWalletTransferController();

        #region Basic Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.Lan = SVASessionInfo.SiteLanguage;
                this.Window1.Title = "搜索";
                this.WindowSearch.Title = "搜索";
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindBrand(FromBrandID);
                Edge.Web.Tools.ControlTool.BindBrand(ToBrandID);
                Edge.Web.Tools.ControlTool.BindStore(FromStoreID, "2");
                Edge.Web.Tools.ControlTool.BindStore(ToStoreID, "2");

                btnDeleteResultItem.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDeleteResultItem.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                InitData();
                SVASessionInfo.TelcoStoreWalletTransferController = null;

                this.GridCardTo.EnableTextSelection = true;
            }
            controller = SVASessionInfo.TelcoStoreWalletTransferController;
            controller.Lan = SVASessionInfo.SiteLanguage;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.GridCardTo.RecordCount <= 0)
            {
                ShowWarning("There must be one record in To Card List!");
                return;
            }

            Edge.SVA.Model.Ord_CardTransfer_H item = this.GetAddObject();

            if (item == null)
            {
                ShowWarning("System error");
                return;
            }
            if (item.CardTransferNumber.Equals(string.Empty))
            {
                ShowAddFailed();
                return;
            }
            item.OrderType = 1;
            item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = null;
            item.UpdatedBy = null;
            item.ApproveOn = null;
            item.PurchaseType = 2;

            controller.ViewModel.MainTable = item;
            //controller.Modify()
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardTransfer_H>(item))
            {
                controller.DeleteDetailByOrder(this.CardTransferNumber.Text.Trim());

                if (ViewState["AddResult"] != null)
                {
                    DataTable issuedDT = (DataTable)ViewState["AddResult"];

                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(600);

                    DataTable needInsertDt = database.GetTableSchema("Ord_CardTransfer_D");
                    foreach (DataRow row in issuedDT.Rows)
                    {
                        DataRow dr = needInsertDt.NewRow();
                        dr["CardTransferNumber"] = item.CardTransferNumber;
                        dr["SourceCardTypeID"] = row["CardTypeID"];
                        dr["SourceCardGradeID"] = row["CardGradeID"];

                        //modified by Darwin Pasco: get sourcecardnumber from datarow
                        dr["SourceCardNumber"] = row["SourceCardNumber"];  //FromCardNumber.Text.ToString();
                        dr["DestCardTypeID"] = row["CardTypeID"];
                        dr["DestCardGradeID"] = row["CardGradeID"];
                        dr["DestCardNumber"] = row["CardNumber"];
                        dr["TxnDate"] = DateTime.Now;
                        dr["StoreCode"] = ToStoreID.Text.ToString();
                        dr["ServerCode"] = "";
                        dr["RegisterCode"] = "";
                        dr["BrandCode"] = "";// ToBrand.SelectedValue;
                        dr["ReasonID"] = 0;
                        dr["Note"] = "Telco Store wallet transfer";
                        dr["ActAmount"] = row["ActAmount"];
                        dr["ActPoints"] = 0;
                        dr["ActCouponNumbers"] = "";
                        dr["CopyCardFlag"] = 0;
                        needInsertDt.Rows.Add(dr);
                        // i++;
                    }
                    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CardTransfer_D");
                    if (!es.Success)
                    {
                        ShowAddFailed();
                        return;
                    }
                }

                Logger.Instance.WriteOperationLog(this.PageName, "Telco Store Card Top Up  " + item.CardTransferNumber + " " + Resources.MessageTips.AddSuccess);

                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Telco Store Card Top Up  " + item.CardTransferNumber + " " + Resources.MessageTips.AddFailed);

                ShowAddFailed();
            }
        }


        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());

                lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());

                this.lblApproveStatus.Text = Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }
                DataSet ds = new Edge.SVA.BLL.Ord_CardTransfer_D().GetList(" CardTransferNumber='" + Model.CardTransferNumber + "'");
                DataTool.AddCardGradeName(ds, "CardGradeName", "SourceCardGradeID");
                DataTool.AddCardGradeCode(ds, "CardGradeCode", "SourceCardGradeID");

                DataTable dt = new DataTable();
                dt.Columns.Add("CardTypeID", typeof(int));
                dt.Columns.Add("CardGradeID", typeof(int));
                dt.Columns.Add("CardNumber", typeof(string));
                dt.Columns.Add("CardGradeCode", typeof(string));
                dt.Columns.Add("CardGradeName", typeof(string));
                dt.Columns.Add("ActAmount", typeof(float));
                dt.Columns.Add("DestCardNumber", typeof(string));

                //added by Darwin Pasco
                dt.Columns.Add("SourceCardNumber", typeof(string));

                string sourcecardnumbers = "";

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    DataRow ddr = dt.NewRow();
                    ddr["CardGradeCode"] = dr["CardGradeCode"];
                    ddr["CardGradeName"] = dr["CardGradeName"];
                    ddr["ActAmount"] = dr["ActAmount"];
                    ddr["DestCardNumber"] = dr["DestCardNumber"];
                    ddr["CardNumber"] = dr["DestCardNumber"];
                    ddr["CardTypeID"] = dr["DestCardTypeID"];
                    ddr["CardGradeID"] = dr["DestCardGradeID"];
                    // added by Darwin Pasco
                    ddr["SourceCardNumber"] = dr["SourceCardNumber"];

                    dt.Rows.Add(ddr);


                    // populate variale sourcecardnumbers with concatenated values from datarow
                    if (sourcecardnumbers == "")
                    {
                        sourcecardnumbers = dr["SourceCardNumber"].ToString();
                    }
                    else
                    {
                        sourcecardnumbers = sourcecardnumbers.ToString() + "|" + dr["SourceCardNumber"].ToString();
                    }

                }

                this.GridCardTo.DataSource = dt.DefaultView;
                this.GridCardTo.DataBind();
                ViewState["SearchResult"] = null;
                ViewState["AddResult"] = dt;

                BindResultList(dt);

                int nCardTypeID = Convert.ToInt16(ds.Tables[0].Rows[0]["SourceCardTypeID"]);
                int nCardGradeID = Convert.ToInt16(ds.Tables[0].Rows[0]["SourceCardGradeID"]);
                Edge.SVA.Model.Card fromCard = new Edge.SVA.BLL.Card().GetModel(ds.Tables[0].Rows[0]["SourceCardNumber"].ToString());
                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Convert.ToInt16(fromCard.IssueStoreID));
                Edge.SVA.Model.Brand storebrand = new Edge.SVA.BLL.Brand().GetModel(Convert.ToInt16(store.BrandID));

                FromBrandID.SelectedValue = storebrand.BrandID.ToString().Trim();

                //Added by Darwin Pasco: Bind store id with brand.
                Edge.Web.Tools.ControlTool.BindStoreWithBrand(FromStoreID, Edge.Web.Tools.ConvertTool.ToInt(this.FromBrandID.SelectedValue));

                FromStoreID.SelectedValue = store.StoreID.ToString().Trim();

                // assigned concatenated sourcecardnumbers to FromCardnmber
                FromCardNumber.Text = sourcecardnumbers; //fromCard.CardNumber;
                FillToStoreInfo(ds.Tables[0].Rows[0]["DestCardNumber"].ToString(), float.Parse(ds.Tables[0].Rows[0]["ActAmount"].ToString()));
            }

        }
        #endregion

        private void FillToStoreInfo(string nCardNumber, float actamount)
        {
            Edge.SVA.Model.Card toCard = new Edge.SVA.BLL.Card().GetModel(nCardNumber);
            Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Convert.ToInt16(toCard.IssueStoreID));
            Edge.SVA.Model.CardType cardtype = new Edge.SVA.BLL.CardType().GetModel(Convert.ToInt16(toCard.CardTypeID));
            Edge.SVA.Model.CardGrade cardgrade = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt16(toCard.CardGradeID));
            Edge.SVA.Model.Brand storebrand = new Edge.SVA.BLL.Brand().GetModel(Convert.ToInt16(store.BrandID));
            Edge.SVA.Model.Brand cardbrand = new Edge.SVA.BLL.Brand().GetModel(cardtype.BrandID);
            ToBrandID.SelectedValue = storebrand.BrandID.ToString();
            ToStoreID.SelectedValue = store.StoreID.ToString();
            InitCardTypeByStore();
            CardTypeID.SelectedValue = cardtype.CardTypeID.ToString();
            InitCardGradeByCardType();
            CardGradeID.SelectedValue = cardgrade.CardGradeID.ToString();

            ActAmount.Text = actamount.ToString();
        }
        #region Basic Functions
        private void InitData()
        {
            //this.CardTransferNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderCardTranfer);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

            btnAddSearchItem.Enabled = true;
            btnDeleteResultItem.Enabled = true;

            InitSearchList();
            InitResultList();
        }

        protected override SVA.Model.Ord_CardTransfer_H GetPageObject(SVA.Model.Ord_CardTransfer_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void SummaryAmounts(DataTable table)
        {
            //if (table.Rows.Count > 0)
            //{
            //    decimal totalDenomination = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(TotalAmount) ", "").ToString());
            //    this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
            //}
            //else
            //{
            //    this.lblTotalDenomination.Text = "0.00";
            //}
        }
        #endregion

        #region Search Functions
        private DataTable GetSearchDataTable()
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

            int top = 1;
            int batchCardID = 0;
            string cardNumber = "";
            string cardTypeID = CardTypeID.SelectedValue;
            string cardgradeID = CardGradeID.SelectedValue;
            string strWhere = string.Format(" IssueStoreID = {0}", ToStoreID.SelectedValue.ToString());

            string filedOrder = " Card.CardNumber ASC ";

            strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardgradeID, string.Empty, strWhere);

            //Display message
            int count = bll.GetCount(strWhere);

            if (count <= 0)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }

            DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);

            DataTable dt = new DataTable();
            dt.Columns.Add("CardTypeID", typeof(int));
            dt.Columns.Add("CardGradeID", typeof(int));
            dt.Columns.Add("CardNumber", typeof(string));
            dt.Columns.Add("CardGradeCode", typeof(string));
            dt.Columns.Add("CardGradeName", typeof(string));
            dt.Columns.Add("ActAmount", typeof(float));
            dt.Columns.Add("DestCardNumber", typeof(string));
            //added by Darwin Pasco
            dt.Columns.Add("SourceCardNumber", typeof(string));

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                DataRow ddr = dt.NewRow();
                ddr["CardGradeCode"] = this.CardGradeID.SelectedItem.Text.Substring(0, this.CardGradeID.SelectedItem.Text.IndexOf("-"));
                ddr["CardGradeName"] = this.CardGradeID.SelectedItem.Text.Substring(this.CardGradeID.SelectedItem.Text.IndexOf("-") + 1);
                ddr["ActAmount"] = Tools.ConvertTool.ConverType<decimal>(this.ActAmount.Text.ToString());
                ddr["DestCardNumber"] = dr["CardNumber"];
                ddr["CardNumber"] = dr["CardNumber"];
                ddr["CardTypeID"] = dr["CardTypeID"];
                ddr["CardGradeID"] = dr["CardGradeID"];
                //added by Darwin Pasco
                ddr["SourceCardNumber"] = GetSourceCardNumber(FromCardNumber.Text, dr["CardNumber"].ToString().Substring(0, 4));

                dt.Rows.Add(ddr);
            }

            return dt;
        }


        //Added by Darwin Pasco: Extract appropriate sourcecardnumber from concatenated list.
        private string GetSourceCardNumber(string sourcecardnumberlist, string searchstring)
        {
            string sourcecardnumber = "";

            string value = sourcecardnumberlist;
            string[] parts = value.Split('|');
            int k = 0;

            while (sourcecardnumber == "")
            {
                if (parts[k].Substring(0, 4) == searchstring)
                {
                    sourcecardnumber = parts[k].ToString();
                }
                k++;
            }
            return sourcecardnumber;
        }


        private void GetFromCardNumber()
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

            int top = 1;
            int batchCardID = 0;
            string cardNumber = "";
            string cardTypeID = CardTypeID.SelectedValue;
            string cardgradeID = CardGradeID.SelectedValue;
            string strWhere = string.Format(" IssueStoreID = {0}", FromStoreID.SelectedValue.ToString());

            string filedOrder = " Card.CardNumber ASC ";

            strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardgradeID, string.Empty, strWhere);

            DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);
            if (ds.Tables[0].Rows.Count > 0)
            {
                //modified by Darwin Pasco: Concatenate source card number delimited by "|" for every telco wallet transfer
                #region Concatenate source card number delimited by "|" for every telco wallet transfer
                if (this.FromCardNumber.Text.Trim().Length <= 0)
                {
                    FromCardNumber.Text = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                }
                else
                {
                    FromCardNumber.Text = FromCardNumber.Text + "|" + ds.Tables[0].Rows[0]["CardNumber"].ToString();
                }
                #endregion

            }
            else
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                FromCardNumber.Text = "";
            }
        }

        private void GetToCardNumber()
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

            int top = 1;
            int batchCardID = 0;
            string cardNumber = "";
            string cardTypeID = CardTypeID.SelectedValue;
            string cardgradeID = CardGradeID.SelectedValue;
            string strWhere = string.Format(" IssueStoreID = {0}", ToStoreID.SelectedValue.ToString());

            string filedOrder = " Card.CardNumber ASC ";

            strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardgradeID, string.Empty, strWhere);

            DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ToCardNumber.Text = ds.Tables[0].Rows[0]["CardNumber"].ToString();
            }
            else
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                ToCardNumber.Text = "";
            }
        }

        private void AddItem()
        {

            if (ViewState["SearchResult"] != null)
            {
                if (ViewState["AddResult"] == null)
                {
                    ViewState["AddResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = (DataTable)ViewState["AddResult"];
                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                dtSearch = (DataTable)ViewState["SearchResult"];
                ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();

                DataTable addDT = (DataTable)ViewState["AddResult"];
                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");
                ViewState["AddResult"] = newSearchDT;

                BindResultList(newSearchDT);
            }

        }

        private void DeleteItem()
        {
            //老界面代码
            if (ViewState["AddResult"] != null)
            {
                DataTable addDT = (DataTable)ViewState["AddResult"];

                foreach (int row in GridCardTo.SelectedRowIndexArray)
                {
                    string cardGradeCode = GridCardTo.DataKeys[row][0].ToString().Trim();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["CardGradeCode"].ToString().Trim() == cardGradeCode)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);                          
                        }
                    }
                    addDT.AcceptChanges();
                }

                ViewState["AddResult"] = addDT;
                BindResultList(addDT);
            }
        }


        private void DeleteAllItem()
        {
            //老界面代码
            if (ViewState["AddResult"] != null)
            {
                DataTable addDT = ((DataTable)ViewState["AddResult"]).Clone();
                ViewState["AddResult"] = addDT;
                BindResultList(addDT);
            }
        }

        private void InitSearchList()
        {
            ViewState["SearchResult"] = null;
        }

        private void InitResultList()
        {
            //老界面代码
            ViewState["AddResult"] = null;
            BindResultList(null);
        }
        #endregion

        #region Search Events
        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            DataTable dt = GetSearchDataTable();
            ViewState["SearchResult"] = dt;
            AddItem();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
        }
        #endregion

        protected void FromBrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(FromStoreID, Edge.Web.Tools.ConvertTool.ToInt(this.FromBrandID.SelectedValue));
            //InitCardTypeByStore();
            //InitCardGradeByCardType();
            //InitSearchList();
        }
        protected void ToBrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(ToStoreID, Edge.Web.Tools.ConvertTool.ToInt(this.ToBrandID.SelectedValue));
            InitCardTypeByStore();
            InitCardGradeByCardType();
            InitSearchList();
        }

        protected void FromStoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Convert.ToInt16(FromStoreID.SelectedValue));
        }

        protected void ToStoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Convert.ToInt16(ToStoreID.SelectedValue));
            InitCardTypeByStore();
            InitCardGradeByCardType();
            InitSearchList();
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }
        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetFromCardNumber();
            GetToCardNumber();
        }

        private void InitCardTypeByStore()
        {
            if (!string.IsNullOrEmpty(this.ToStoreID.SelectedValue))
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, " IsDumpCard = 1 and CardTypeID in (" + Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.ToStoreID.SelectedValue), 2) + ") order by CardTypeCode");
            //Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, " CardTypeID in (" + Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.ToStoreID.SelectedValue), 2) + ") order by CardTypeCode");
        }

        private void InitCardGradeByCardType()
        {
            if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue))
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
        }

        //绑定添加结果（新）
        private void BindResultList(DataTable dt)
        {
            if ((dt != null) && (dt.Rows.Count > 0))
            {
                this.GridCardTo.PageSize = webset.ContentPageNum;
                //DataTable dt = (DataTable)ViewState["AddResult"];
                this.GridCardTo.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.GridCardTo.PageIndex, this.GridCardTo.PageSize);
                //this.GridCardTo.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.GridCardTo.DataSource = dt.DefaultView;
                this.GridCardTo.DataBind();

                SummaryAmounts(dt);
            }
            else
            {
                this.GridCardTo.PageSize = webset.ContentPageNum;
                this.GridCardTo.PageIndex = 0;
                this.GridCardTo.RecordCount = 0;
                this.GridCardTo.DataSource = null;
                this.GridCardTo.DataBind();
            }
            //this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = GridCardFrom.RecordCount > 0 ? true : false;
            //this.Brand.Enabled = this.StoreID.Enabled = GridCardFrom.RecordCount > 0 ? false : true;
        }
    }
}
