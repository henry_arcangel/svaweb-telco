﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using System.Data;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.WalletRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.WalletRule_H, Edge.SVA.Model.WalletRule_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                this.WalletRuleCode.Text = DALTool.GetREFNOCode("WALLET");
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;

                ControlTool.BindBrand(BrandID);
                ControlTool.BindStore(StoreID, -1);
                ControlTool.BindBrand(BrandDetail);

                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0} and IsDumpCard =1 order by CardTypeCode", -1));
                ControlTool.BindCardGrade(CardGradeID, -1);

                //ControlTool.AddTitle(BrandID);
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {

            Edge.SVA.Model.WalletRule_H item = this.GetAddObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Wallet Rule Form  {0} No Data", item.WalletRuleCode));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            if (this.Detail.Rows.Count <= 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Wallet Rule Form  {0} Detail No Data", item.WalletRuleCode));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedOn = DateTime.Now;

            if (Tools.DALTool.Add<Edge.SVA.BLL.WalletRule_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("WalletRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["WalletRuleCode"] = item.WalletRuleCode;
                        row["BrandID"] = detail["BrandID"];
                        row["CardTypeID"] = detail["CardTypeID"];
                        row["CardGradeID"] = detail["CardGradeID"];
                        row["StoreID"] = detail["StoreID"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "WalletRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                        item.UpdatedOn = DateTime.Now;
                        item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                        Tools.DALTool.Update<Edge.SVA.BLL.WalletRule_H>(item);
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Edge.SVA.BLL.WalletRule_D bll = new SVA.BLL.WalletRule_D();

                    Edge.SVA.BLL.WalletRule_H hearder = new SVA.BLL.WalletRule_H();
                    hearder.Delete(this.WalletRuleCode.Text.Trim());

                    Logger.Instance.WriteErrorLog(this.PageName, string.Format(" Wallet Rule Form  {0} Add Success But Detail Failed", item.WalletRuleCode), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format(" Wallet Rule Form  {0} Add Success", item.WalletRuleCode));
               // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format(" Wallet Rule Form  {0} Add Failed", item.WalletRuleCode));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            Boolean finded=false;
            if (this.BrandID.SelectedValue == "-1")
            {
                BrandID.MarkInvalid(String.Format("'{0}' Can't Empty！", BrandID.Text));
                return;
                //throw new Exception("Brand Can't Empty");
            }
            if (this.CardGradeID.SelectedValue == "-1")
            {
                CardGradeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CardGradeID.Text));
                return;
                // throw new Exception("Card Grade Can't Empty");
            }
            //if (this.StoreID.SelectedValue == "-1")
            //{
            //    StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
            //    return;
            //    //throw new Exception("Brand Can't Empty");
            //}

            int[] selectedarr = this.checkBoxList1.SelectedIndexArray;
            foreach (int i in selectedarr)
            {
                foreach (System.Data.DataRow detail in this.Detail.Rows)
                {
                    if (detail["CardGradeID"].ToString().Equals(this.CardGradeID.SelectedValue) && detail["StoreID"].ToString().Equals(this.StoreID.SelectedValue))
                    {
                        //JscriptPrint(Resources.MessageTips.ExistCardTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                        ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                        finded = true;
                    }
                }
                if (!finded)
                {
                    System.Data.DataRow row = this.Detail.NewRow();
                    row["ID"] = this.DetailIndex;
                    row["CardTypeID"] = int.Parse(this.CardTypeID.SelectedItem.Value);
                    row["CardTypeCode"] = this.CardTypeID.SelectedItem.Text.Substring(0, this.CardTypeID.SelectedItem.Text.IndexOf("-"));
                    row["CardTypeName"] = this.CardTypeID.SelectedItem.Text.Substring(this.CardTypeID.SelectedItem.Text.IndexOf("-") + 1);
                    row["CardGradeID"] = int.Parse(this.CardGradeID.SelectedItem.Value);
                    row["CardGradeCode"] = this.CardGradeID.SelectedItem.Text.Substring(0, this.CardGradeID.SelectedItem.Text.IndexOf("-"));
                    row["CardGradeName"] = this.CardGradeID.SelectedItem.Text.Substring(this.CardGradeID.SelectedItem.Text.IndexOf("-") + 1);
                    row["StoreID"] = int.Parse(this.checkBoxList1.Items[i].Value);
                    row["StoreCode"] = this.checkBoxList1.Items[i].Text.Substring(0, this.StoreID.SelectedItem.Text.IndexOf("-"));
                    row["StoreName"] = this.checkBoxList1.Items[i].Text;
                    row["BrandID"] = int.Parse(this.BrandID.SelectedItem.Value);
                    row["BrandCode"] = this.BrandID.SelectedItem.Text.Substring(0, this.BrandID.SelectedItem.Text.IndexOf("-"));
                    row["BrandName"] = this.BrandID.SelectedItem.Text;
                    this.Detail.Rows.Add(row);
                }
                finded = false;
            }

            this.BindDetail();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int CardID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(CardID);
                BindDetail();
            }
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        protected void BrandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStoreCodeWithBrand(StoreID, brandID);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("ID", typeof(int));
                    dt.Columns.Add("BrandID", typeof(int));
                    dt.Columns.Add("BrandCode", typeof(string));
                    dt.Columns.Add("BrandName", typeof(string));
                    dt.Columns.Add("CardTypeID", typeof(int));
                    dt.Columns.Add("CardTypeCode", typeof(string));
                    dt.Columns.Add("CardTypeName", typeof(string));
                    dt.Columns.Add("CardGradeID", typeof(int));
                    dt.Columns.Add("CardGradeCode", typeof(string));
                    dt.Columns.Add("CardGradeName", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("StoreCode", typeof(string));
                    dt.Columns.Add("StoreName", typeof(string));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void DeleteDetail(int CardID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(CardID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected override SVA.Model.WalletRule_H GetPageObject(SVA.Model.WalletRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void SearchButton1_Click(object sender, EventArgs e)
        {
            RptBind1("StoreID>0", "StoreCode");
        }

        private void RptBind1(string strWhere, string orderby)
        {
            this.checkBoxList1.Items.Clear();
            try
            {
                StringBuilder sb = new StringBuilder(strWhere);
                int brandid = this.BrandDetail.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.BrandDetail.SelectedValue);
                if (brandid > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" BrandID =");
                    sb.Append(brandid);
                    sb.Append("");
                }

                if (this.StoreID.SelectedIndex > 0)
                {
                    sb.Append(" and storecode ='");
                    sb.Append(this.StoreID.SelectedValue);
                    sb.Append("'");
                }

                strWhere = sb.ToString();
                string descLan = DALTool.GetStringByCulture("StoreName1", "StoreName2", "StoreName3");

                StoreController controller = new StoreController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, 1000, 0, out count, orderby);
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        this.checkBoxList1.Items.Add(Convert.ToString(dr["StoreCode"])+"-"+Convert.ToString(dr[descLan]), Convert.ToString(dr["StoreID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteOperationLog(this.PageName, ex.Message);
            }
        }

        protected void SelectAllButton_Click(object sender, EventArgs e)
        {
            for (int i=0; i < this.checkBoxList1.Items.Count ; i++)
            {
                this.checkBoxList1.Items[i].Selected = true;
            }
        }

        protected void DeleteAllButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.checkBoxList1.Items.Count; i++)
            {
                this.checkBoxList1.Items[i].Selected = false;
            }
        }
    }
}