﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.WalletRule
{
    public partial class Show : Tools.BasePage<SVA.BLL.WalletRule_H, SVA.Model.WalletRule_H>
    {

        private const string fields = "[KeyID] ,[WalletRuleCode],[BrandID],[CardTypeID] ,[CardGradeID],[StoreID]";
        private static string ApproveStatusValue; //Add By Robin 2014-11-25 for static stock status

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid2.PageSize = webset.ContentPageNum;
                RptBind(string.Format("WalletRuleCode='{0}'", Request.Params["id"]), "CardGradeID,KeyID", fields); //Moved from Page_Load() Robin 2014-11-26
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());

                
            }
        }

        #endregion

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            Edge.SVA.BLL.WalletRule_D bll = new Edge.SVA.BLL.WalletRule_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex);
            }

            DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            DataTool.AddBrandName(ds, "BrandName", "BrandID");
            DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
            DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            DataTool.AddStoreCode(ds, "StoreCode", "StoreID");
            DataTool.AddStoreName(ds, "StoreName", "StoreID");

            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();


        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid2.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid2.PageIndex = e.NewPageIndex;

            RptBind(string.Format("WalletRuleCode='{0}'", Request.Params["id"]), "CardGradeID,KeyID", fields);

        }

        protected void Grid2_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {

            if (e.DataItem is DataRowView)
            {
                //显示格式
                //Label lblCardGradeCode = Grid2.Rows[e.RowIndex].FindControl("lblCardGradeCode") as Label;
                //if (lblCardGradeCode != null)
                //{
                //    Label lblCardGrade = (Label)Grid2.Rows[e.RowIndex].FindControl("lblCardGrade");
                //    Label lblOrderQTY = (Label)Grid2.Rows[e.RowIndex].FindControl("lblOrderQTY");
                //    Label lblSeq = Grid2.Rows[e.RowIndex].FindControl("lblSeq") as Label;
                //    HiddenField hfCardGradeID = Grid2.Rows[e.RowIndex].FindControl("hfCardGradeID") as HiddenField;
                //    //重复
                //    if (ViewState["CardGradeCode"] != null && ViewState["CardGradeCode"].ToString().Trim() == lblCardGradeCode.Text.Trim())
                //    {
                //        lblCardGradeCode.Visible = false;
                //        if (lblCardGrade != null) { lblCardGrade.Visible = false; }
                //        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                //        if (lblSeq != null) { lblSeq.Visible = false; }
                //    }
                //    else//不重复
                //    {
                //        ViewState["CardGradeCode"] = lblCardGradeCode.Text.Trim();
                //        if (lblCardGrade != null) { ViewState["CardGrade"] = lblCardGrade.Text.Trim(); }
                //        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                //        if (lblSeq != null) { lblSeq.Text = (this.CardGradeIndex[int.Parse(hfCardGradeID.Value)]).ToString(); }
                //    }
                //}
            }

        }


        #endregion

 
        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

    }
}