﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Messages.Manager;
using Edge.Web.Controllers;
using System.Text;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.CardManagement;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.WalletRule;

namespace Edge.Web.Operation.CardManagement.TelcoManagement.WalletRule
{
    public partial class List : PageBase
    {

        private const string fields = "[WalletRuleCode],[Description],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy]";
        protected static string strWhere = " where 1=1 ";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind(strWhere, "WalletRuleCode");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");

                ControlTool.BindBrand(this.CardBrand);
            }
            string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
        }

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    if (!string.IsNullOrEmpty(this.CardBrand.SelectedValue) && this.CardBrand.SelectedValue != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" A.BrandID =" + CardBrand.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(this.CardType.SelectedValue) && this.CardType.SelectedValue != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" A.CardTypeID =" + CardType.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(this.CardGrade.SelectedValue) && this.CardGrade.SelectedValue != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" A.CardGradeID =" + CardGrade.SelectedValue);
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                WalletRuleController c = new WalletRuleController();
                DataSet ds = c.GetTransferList(strWhere, "");
                this.RecordCount = ds.Tables[0].Rows.Count;
                if (ds != null)
                {
                    DataTable table = ds.Tables[0];

                    DataView view1 = table.DefaultView;
                    view1.Sort = this.SortField.Text;
                    this.Grid1.DataSource = view1;

                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load WalletRule", "error", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            WalletRuleController c = new WalletRuleController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = c.GetTransferList(ViewState["strWhere"].ToString(), this.SortField.Text);
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        #region Event


        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(strWhere, "WalletRuleCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind(strWhere, "WalletRuleCode");
        }
        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {

        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {

        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind(strWhere, "WalletRuleCode");
        }
        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindCardGradeWithoutBrand(CardGrade, "CardTypeID=" + CardType.SelectedValue);    
        }
        protected void CardBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindCardTypeWithoutBrand(CardType, "BrandID=" + CardBrand.SelectedValue);
            Edge.Web.Tools.ControlTool.BindCardGradeWithoutBrand(CardGrade, "CardTypeID=0");   
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                string Code = keys[0].ToString();
                Edge.SVA.BLL.WalletRule_H bll = new SVA.BLL.WalletRule_H();
                bll.Delete(Code);
                Edge.SVA.BLL.WalletRule_D bll1 = new SVA.BLL.WalletRule_D();
                bll1.DeleteByCode(Code);
                RptBind(strWhere, "WalletRuleCode");
            }
        }


    }
}
