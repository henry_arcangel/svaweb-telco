﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardActive;
using Edge.Web.Controllers;
using System.Text.RegularExpressions;
using Edge.Web.Controllers.Operation.CardManagement.TelcoManagement.WalletBalanceAdjustment;


namespace Edge.Web.Operation.CardManagement.TelcoManagement.WalletBalanceAdjustment.Card
{
    public partial class Add : PageBase
    {
        WalletBalanceAdjustmentController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                CardTypeID_SelectedIndexChanged(null, null);

                RegisterCloseEvent(this.btnCloseSearch);
            }
            controller = SVASessionInfo.WalletBalanceAdjustmentController;
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();
            List<string> idList = GetSelectedRowIndexArrayFromHiddenField();

            if (ViewState["SearchResult"] != null)
            {
                if (controller.ViewModel.CardTable == null)
                {
                    controller.ViewModel.CardTable = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = controller.ViewModel.CardTable;
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    for (int i = 0; i < idList.Count; i++)
                    {
                        ids += string.Format("{0},", "'" + idList[i].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    //foreach (DataRowView drv in dvSearch)
                    //{
                    //    drv.Delete();
                    //}
                    //vsDT.AcceptChanges();

                    //ViewState["SearchResult"] = vsDT;
                    ViewState["SearchResult"] = dtSearch;

                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];

                    //ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();

                    cbSearchAll.Checked = false;
                }

                #region 验证金额是否一致
                DataTable dt = ViewState["SearchResult"] as DataTable;
                if (this.IsOpenAmount && dt.Rows.Count > 0)
                {
                    int j = dt.Rows.Count - 1;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        double startAmount = string.IsNullOrEmpty(dt.Rows[i]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[i]["TotalAmount"]);
                        double endAmount = string.IsNullOrEmpty(dt.Rows[j]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[j]["TotalAmount"]);
                        if (startAmount != endAmount)
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90492"));
                            return;
                        }
                    }
                }
                #endregion

                DataTable addDT = controller.ViewModel.CardTable;

                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");

                controller.ViewModel.CardTable = newSearchDT;

                //返回界面时要清空查询记录
                ViewState["SearchResult"] = null;
            }
            CloseAndPostBack();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);
            if (!ValidaData())
            {
                return;
            }
            
            Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            Edge.SVA.Model.CardUIDMap modelmap = null;

            this.SearchListGrid.PageIndex = 0;
            DataTable dt = GetSearchDataTable();

            ViewState["SearchResult"] = dt;
            BindCardGrid();
        }

        private void ChangeControlStatus(bool status)
        {
            this.btnSearch.Enabled = !status;
            this.CardTypeID.Enabled = !status;
            this.CardTypeID.SelectedIndex = 0;
        }
        
        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
                // Tools.ControlTool.BindCardGradeWithoutBrand(CardGradeID, "CardGradeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") order by CardGradeCode");

            }
            else
            {
                Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {  

        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindCardGrid();
        }

        private DataTable GetSearchDataTable()
        {
            try
            {
                DataTable dt = new DataTable();
                Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
                string cardGradeID = CardGradeID.SelectedValue;
                string strWhere = string.Format(" Card.Status in ( {0} )", (int)CardController.CardStatus.Active);
                strWhere += " and Card.IssueStoreID=" + Request["StoreID"];                
                string filedOrder = " Card.CardNumber ASC ";

                if (cardGradeID == "-1")
                {
                    cardGradeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
                }
               
                strWhere = GetCardSearchStrWhere(1, 0, "", cardGradeID, "", strWhere);

                dt = controller.GetCardSearchTable(1, strWhere, filedOrder);

                if (dt.Rows.Count == 0)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return null;
                }

                return dt;
            }
            catch
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
        }

        private void BindCardGrid()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();

                //判断CardAmount
                if (dt == null) return;

                if (dt.Rows.Count <= 0) return;

                //Add by Nathan 20140820 ++
                int cardGradeID = Tools.ConvertTool.ToInt(this.CardGradeID.SelectedValue);
                Edge.SVA.BLL.CardGrade blltype = new SVA.BLL.CardGrade();
                Edge.SVA.Model.CardGrade modeltype = cardGradeID == 0 ? null : blltype.GetModel(cardGradeID);

                //Add by Nathan 20140820 --
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }

           // btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
        }

        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            BindCardGrid();

            UpdateSelectedRowIndexArray();
            if (cbSearchAll.Checked)
            {
                SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            }
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            if (SearchListGrid.SelectedRowIndexArray != null && SearchListGrid.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(SearchListGrid.SelectedRowIndexArray);
                for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
                {
                    string id = SearchListGrid.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
            {
                string id = SearchListGrid.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i);
                }
            }
            SearchListGrid.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion

        public bool IsOpenAmount
        {
            get
            {
                if (ViewState["IsOpenAmount"] == null) return false;

                if (ViewState["IsOpenAmount"] is bool) return (bool)ViewState["IsOpenAmount"];

                return false;
            }
            set
            {
                ViewState["IsOpenAmount"] = value;
            }
        }
        protected void cbSearchAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            if (!cbSearchAll.Checked)
            {
                hfSelectedIDS.Text = string.Empty;
            } 
        }

        protected bool ValidaData()
        {
            return true;
        }


        protected void WindowContact_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            DataTable dt = (DataTable)ViewState["SearchResult"];

            BindCardGrid();
        }

        protected void WindowEnough_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            if (Convert.ToBoolean(ViewState["IsScan"]))
            {
                ShowTable();
            }
            BindCardGrid();
        }

        protected bool ValidataStore(Edge.SVA.Model.CardUIDMap model)
        {
            if (model == null)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return false;
            }
            Edge.SVA.BLL.CardGradeStoreCondition bll = new SVA.BLL.CardGradeStoreCondition();
            DataTable dt = bll.GetList(" CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 3 ").Tables[0];

            int i = 0;
            while (dt.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dt.Rows[i]["ConditionID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }
            Edge.SVA.BLL.Store bllstore = new SVA.BLL.Store();
            DataTable dtstore = bllstore.GetList(" BrandID in (select ConditionID from CardGradeStoreCondition where CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 1)").Tables[0];
            i = 0;
            while (dtstore.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dtstore.Rows[i]["StoreID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }

            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90404"), "CardNumber"));
            return false;
        }

        public void ShowTable()
        {
            DataTable newdt = ViewState["Newds"] as DataTable;
            DataTable showdt = new DataTable();
            if (ViewState["SearchResult"] != null)
            {
                DataTable dt = ViewState["SearchResult"] as DataTable;
                showdt = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(dt, newdt, "CardUID");
            }
            else
            {
                showdt = newdt;
            }

            ViewState["Newds"] = null;
            ViewState["SearchResult"] = showdt;
        }
    }
}