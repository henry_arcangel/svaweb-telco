﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardOrderFromSupplierPointAmount;
using Edge.Web.Controllers.Accounts;
using Edge.Security.Manager;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardOrderFromSupplierPointAmount
{
    public partial class PrintPR : PageBase
    {
        DataSet ds;
        DataSet detail;
        double iTotalAmount=0;
        double iTotalRetailAmount = 0;
        double iTotalNetAmount=0;
        double iTotalDiscountAmount=0;

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Request.Params["id"];

                ds = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H().GetList("OrderSupplierNumber ='" + id + "'");
                ds.Tables[0].Columns.Add("StoreName", typeof(string));
                ds.Tables[0].Columns.Add("SupplierName", typeof(string));
                ds.Tables[0].Columns.Add("StoreAddress", typeof(string));
                ds.Tables[0].Columns.Add("Buyer", typeof(string));
                Tools.DataTool.AddUserName(ds, "CreatedByUserName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApprovedByUserName", "ApproveBy");
                Tools.DataTool.AddCompanyName(ds, "CompanyName", "CompanyID");
                Tools.DataTool.AddCompanyAddress(ds, "CompanyAddress", "CompanyID");
                
                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Convert.ToInt16(ds.Tables[0].Rows[0]["StoreID"]));
                ds.Tables[0].Rows[0]["StoreName"] = string.Format("{0} - {1}", store.StoreCode, store.StoreName1);
                //ds.Tables[0].Rows[0]["StoreAddress"] = store.StoreFullDetail.ToString();
                if (store.StoreAddressDetail!=null)
                { ds.Tables[0].Rows[0]["StoreAddress"] = store.StoreAddressDetail.ToString(); }
                Edge.SVA.Model.Supplier Supplier = new Edge.SVA.BLL.Supplier().GetModel(Convert.ToInt16(ds.Tables[0].Rows[0]["SupplierID"]));
                ds.Tables[0].Rows[0]["SupplierName"] = string.Format("{0} - {1}", Supplier.SupplierCode, Supplier.SupplierDesc1);

                ds.Tables[0].Rows[0]["Buyer"] = "";
                AccountController controller = new AccountController();
                if (ds.Tables[0].Rows[0]["ApproveBy"].ToString() != string.Empty)
                {
                    User us = new User(Convert.ToInt32(ds.Tables[0].Rows[0]["ApproveBy"]));
                    ds.Tables[0].Rows[0]["Buyer"] = string.Format("{0} - {1}", us.UserID.ToString(), us.UserName.ToString());
                }
                
                detail = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_D().GetList("OrderSupplierNumber ='" + id + "'");
                detail.Tables[0].Columns.Add("SKU", typeof(string));
                detail.Tables[0].Columns.Add("SKUDesc", typeof(string));
                detail.Tables[0].Columns.Add("UPC", typeof(string));
                detail.Tables[0].Columns.Add("UnitRetail", typeof(double));
                detail.Tables[0].Columns.Add("UnitCost", typeof(double));
                detail.Tables[0].Columns.Add("GM", typeof(double));
                detail.Tables[0].Columns.Add("NetCost", typeof(double));
                List<Edge.SVA.Model.ImportGM> imlist = (new Edge.SVA.BLL.ImportGM()).GetModelList("Export = 'PR'");
                //List<Edge.SVA.Model.GrossMargin> gmlist = (new Edge.SVA.BLL.GrossMargin()).GetModelList("Export = 2 ");

                foreach (DataRow dr in detail.Tables[0].Rows)
                {
                    Edge.SVA.Model.CardGrade cardgrade = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt32(dr["CardGradeID"]));
                    foreach (Edge.SVA.Model.ImportGM im in imlist)
                    {
                        if (im.CardGradeCode == cardgrade.CardGradeCode)
                        {
                            double UnitCost = (100 - (int)(im.GMValue)) * 0.01;
                            dr["SKU"] = im.SKU;
                            dr["SKUDesc"] = im.SKUDesc;
                            dr["UPC"] = im.UPC;
                            dr["GM"] = im.GMValue;
                            dr["UnitRetail"] = 1.00;
                            dr["UnitCost"] = UnitCost.ToString("F2");
                            dr["NetCost"] = decimal.Parse(dr["OrderAmount"].ToString()) * (100 - (decimal)im.GMValue) * (decimal)0.01;
                            iTotalAmount = iTotalAmount + double.Parse(dr["OrderAmount"].ToString());
                            iTotalRetailAmount = iTotalRetailAmount + double.Parse(dr["OrderAmount"].ToString());
                            iTotalNetAmount = iTotalNetAmount + double.Parse(dr["NetCost"].ToString());
                            break;
                        }
                    }              
                }
                iTotalDiscountAmount = iTotalAmount - iTotalNetAmount;
                this.rptOrders.DataSource = ds.Tables[0];
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                list.DataSource = detail.Tables[0];
                list.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");
                if (lblTotalAmount != null)
                    lblTotalAmount.Text = iTotalNetAmount.ToString("F2");
                
                Label lblTotalDiscountAmount = (Label)e.Item.FindControl("lblTotalDiscountAmount");
                if (lblTotalDiscountAmount != null)
                    lblTotalDiscountAmount.Text = 0.ToString("F2");
               
                Label lblTotalNetAmount = (Label)e.Item.FindControl("lblTotalNetAmount");
                if (lblTotalNetAmount != null)
                    lblTotalNetAmount.Text = iTotalNetAmount.ToString("F2");

                Label lblTotalGrossAmount = (Label)e.Item.FindControl("lblTotalGrossAmount");
                if (lblTotalGrossAmount != null)
                    lblTotalGrossAmount.Text = iTotalAmount.ToString("F2");

                Label lblTotalRetailAmount = (Label)e.Item.FindControl("lblTotalRetailAmount");
                if (lblTotalRetailAmount != null)
                    lblTotalRetailAmount.Text = iTotalRetailAmount.ToString("F2");
            }
        }

        #endregion


        #region 数据列表绑定

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}