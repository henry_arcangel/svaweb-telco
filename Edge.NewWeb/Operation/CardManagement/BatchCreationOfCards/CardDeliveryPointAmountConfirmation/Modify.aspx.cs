﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardDeliveryPointAmountConfirmation;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardDeliveryPointAmountConfirmation
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardReceive_H, Edge.SVA.Model.Ord_CardReceive_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.Grid1.PageSize = webset.ContentPageNum;
                //this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardReceiveNumber.Text = Model.CardReceiveNumber;
                this.lblReferenceNo.Text = Model.ReferenceNo;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.SupplierAddress.Text = Model.SupplierAddress;
                this.SuppliertContactName.Text = Model.SuppliertContactName;
                this.SupplierPhone.Text = Model.SupplierPhone;

                //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));

                this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.StorerAddress.Text = Model.StorerAddress;
                this.StoreContactName.Text = Model.StoreContactName;
                this.StorePhone.Text = Model.StorePhone;

                //this.CardTypeID.SelectedValue = Model.CardTypeID.ToString();
                //this.CardQty.Text = Model.CardQty.ToString();

                this.BindDetail3();
                //this.BindDetail();
                //this.BindDetail2();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardReceive_H item = null;
            Edge.SVA.Model.Ord_CardReceive_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            item = this.GetPageObject(dataItem);

            item.ReceiveType = 1;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardReceive_H>(item))
            {
                try
                {
                    CardDeliveryPointAmountConfirmationController controller = new CardDeliveryPointAmountConfirmationController();
                    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                    //DataTable dt = (DataTable)ViewState["DetailResult"];
                    //DataTable dt2 = (DataTable)ViewState["DetailResult2"];
                    controller.UpdateCardActualAmount(dt3);
                    //controller.UpdateCardStockStatus(dt, 2);
                    //controller.UpdateCardStockStatus(dt2, 3);
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Success But Detail Failed", item.CardReceiveNumber), ex);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Success", item.CardReceiveNumber));
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Failed", item.CardReceiveNumber));
                ShowAddFailed();
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //Search 前先保存 Robin 2014-11-07
            try
            {
                CardDeliveryPointAmountConfirmationController controller = new CardDeliveryPointAmountConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            Grid3.PageIndex = e.NewPageIndex;
            this.BindDetail3();
        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid1.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        //protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid2.PageIndex = e.NewPageIndex;
        //    this.BindDetail2();
        //}

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                StorerAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StorePhone.Text = model.StoreTel;
            }
            else
            {
                StorerAddress.Text = "";
                StoreContactName.Text = "";
                StorePhone.Text = "";
            }

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            if (model != null)
            {
                SupplierAddress.Text = model.SupplierAddress;
                SuppliertContactName.Text = model.Contact;
                SupplierPhone.Text = model.ContactPhone;
            }
            else
            {
                SupplierAddress.Text = "";
                SuppliertContactName.Text = "";
                SupplierPhone.Text = "";
            }

            //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        private System.Data.DataTable Detail3
        {
            get
            {
                if (ViewState["DetailResult3"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CardReceive_D().GetList(string.Format("CardReceiveNumber = '{0}'", Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;
                    ds.Tables[0].Columns.Add("ID", typeof(int));
                    Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
                    Tools.DataTool.AddCardTypeNameByID(ds, "CardTypeName", "CardTypeID");
                    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                    Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");
                    ViewState["DetailResult3"] = ds.Tables[0];
                }
                return ViewState["DetailResult3"] as System.Data.DataTable;
            }
        }

         //Add By Robin 2014-11-07
        private int DetailCount
        {
            get
            {
                int rtn = DALTool.GetCardReceiveDetailCount(CardReceiveNumber.Text, "");
                return rtn;
            }
        }

        private void BindDetail3()
        {
            this.Grid3.RecordCount = this.Detail3.Rows.Count;
            this.Grid3.DataSource = DataTool.GetPaggingTable(this.Grid3.PageIndex, this.Grid3.PageSize, this.Detail3);
            this.Grid3.DataBind();
        }

  
        protected override SVA.Model.Ord_CardReceive_H GetPageObject(SVA.Model.Ord_CardReceive_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            //Search 前先保存 Robin 2014-07-30
            try
            {
                CardDeliveryPointAmountConfirmationController controller = new CardDeliveryPointAmountConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
               return;
            }
            //End

            ViewState["DetailResult3"] = null;
            this.BindDetail3();
        }

  
        protected void btnExport_Click(object sender, EventArgs e)
        {
            CardDeliveryPointAmountConfirmationController controller = new CardDeliveryPointAmountConfirmationController();
            DataTable dt = controller.GetExportList(this.CardReceiveNumber.Text);
            SVA.BLL.Ord_CardReceive_H bll = new SVA.BLL.Ord_CardReceive_H();
            string fileName = controller.UpLoadFileToServer(bll.GetModel(this.CardReceiveNumber.Text), dt);

            try
            {
                string exportname = "SalesInvoice_PO.xls";
                Tools.ExportTool.ExportFile(fileName, exportname);
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
            }

        }

        protected void btnUpdateDetail_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.ActualAmount.Text) || int.Parse(this.ActualAmount.Text.Replace(",", "").Trim()) <= 0)
            {
                return;
            }

            foreach (System.Data.DataRow detail in this.Detail3.Rows)
            {
                if (Convert.ToDecimal(detail["ActualAmount"].ToString()) >= Convert.ToDecimal(this.ActualAmount.Text.Replace(",", "").Trim())) //Add By Robin 避免修改金额大于原来金额
                {
                    detail["ActualAmount"] = Convert.ToDecimal(this.ActualAmount.Text.Replace(",", "").Trim());
                }
            }

            this.BindDetail3();
        }

    }
}