﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic
{
    public partial class Export : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_OrderToSupplier_Card_H, Edge.SVA.Model.Ord_OrderToSupplier_Card_H>
    {
        CardCreationAutomaticController controller = new CardCreationAutomaticController();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                RegisterCloseEvent(btnClose);
                if (Request["OrderSupplierNumber"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    HidID.Text = Convert.ToString(Request["OrderSupplierNumber"]);
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string id = HidID.Text;
            if (ExportType.SelectedValue == "1")
            {
                DataTable dt = controller.GetExportList(id);
                string fileName = controller.UpLoadFileToServer(dt);

                try
                {
                    string exportname = "GC Delivery List.xls";
                    Tools.ExportTool.ExportFile(fileName, exportname);
                }
                catch (Exception ex)
                {
                    ShowWarning(ex.Message);
                }
            }
            else
            {

            }
        }
    }
}