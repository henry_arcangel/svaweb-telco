﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_OrderToSupplier_Card_H, Edge.SVA.Model.Ord_OrderToSupplier_Card_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.OrderSupplierNumber.Text = DALTool.GetREFNOCode(CardController.CardRefnoCode.CardCreationAutomatic);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;
                this.lblOrderType.Text = "手动";

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCardType(CardTypeID, -1);
                ControlTool.BindCardType(CardTypeID);
                ControlTool.BindCardGrade(CardGradeID,-1);
                ControlTool.BindCompanyID(CompanyID);
                CompanyID.SelectedIndex = 1;

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {

            if (this.SupplierID.SelectedValue == "-1")
            {
                this.SupplierID.MarkInvalid(String.Format("'{0}' Can't Empty！", SupplierID.Text));
                return;
            }
            //if (this.StoreID.SelectedValue == "")
            //{
            //    this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
            //    return;
            //}
            if (this.CardTypeID.SelectedValue == "-1")
            {
                this.CardTypeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CardTypeID.Text));
                return;
            }

            Edge.SVA.Model.Ord_OrderToSupplier_Card_H item = this.GetAddObject();

            if (this.Detail.Rows.Count <= 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Detail No Data", item.OrderSupplierNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Creation-Automatic {0} No Data", item.OrderSupplierNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //if (this.Detail.Rows.Count <= 0)
            //{
            //    Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Detail No Data", item.CardOrderFormNumber));
            //    //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
            //    ShowWarning(Resources.MessageTips.NoData);
            //    return;
            //}

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;
            item.CreatedOn = DateTime.Now;
            item.ApproveOn = null;
            item.ApprovalCode = null;
            item.PurchaseType = 1; //Add By Robin 2015-02-09 for RGG增加新的订金额和订数量

            if (Tools.DALTool.Add<Edge.SVA.BLL.Ord_OrderToSupplier_Card_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_OrderToSupplier_Card_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["OrderSupplierNumber"] = item.OrderSupplierNumber;
                        row["CardTypeID"] = detail["CardTypeID"];
                        row["CardGradeID"] = detail["CardGradeID"];
                        row["OrderQty"] = detail["OrderQty"];
                        row["PackageQty"] = detail["PackageQty"];
                        row["OrderRoundUpQty"] = detail["OrderRoundUpQty"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_OrderToSupplier_Card_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Edge.SVA.BLL.Ord_OrderToSupplier_Card_D bll = new SVA.BLL.Ord_OrderToSupplier_Card_D();
                    bll.DeleteByOrder(this.OrderSupplierNumber.Text.Trim());

                    Edge.SVA.BLL.Ord_OrderToSupplier_Card_H hearder = new SVA.BLL.Ord_OrderToSupplier_Card_H();
                    hearder.Delete(this.OrderSupplierNumber.Text.Trim());

                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Card Order to Supplier {0} Add Success But Detail Failed", item.OrderSupplierNumber), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order to Supplier {0} Add Success", item.OrderSupplierNumber));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Add Failed", item.OrderSupplierNumber));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StorePhone.Text = model.StoreTel;
            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StorePhone.Text = "";
            }

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            if (model != null)
            {
                SupplierAddress.Text = model.SupplierAddress;
                SuppliertContactName.Text = model.Contact;
                SupplierPhone.Text = model.ContactPhone;
            }
            else
            {
                SupplierAddress.Text = "";
                SuppliertContactName.Text = "";
                SupplierPhone.Text = "";
            }

            //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected override SVA.Model.Ord_OrderToSupplier_Card_H GetPageObject(SVA.Model.Ord_OrderToSupplier_Card_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }


        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_D().GetList(string.Format("OrderSupplierNumber = '{0}'", Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;
                    ds.Tables[0].Columns.Add("ID", typeof(int));
                    Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
                    Tools.DataTool.AddCardTypeNameByID(ds, "CardTypeName", "CardTypeID");
                    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                    Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");
                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int CardGradeID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(CardGradeID);
                BindDetail();
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue == "-1" || this.CardGradeID.SelectedValue == "-1" || string.IsNullOrEmpty(this.CardQty.Text))
            {
                this.CardGradeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CardGradeID.Text));
                return;
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CardGradeID"].ToString().Equals(this.CardGradeID.SelectedValue))
                {
                    ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["CardTypeID"] = int.Parse(this.CardTypeID.SelectedItem.Value);
            row["CardTypeCode"] = this.CardTypeID.SelectedItem.Text.Substring(0, this.CardTypeID.SelectedItem.Text.IndexOf("-"));
            row["CardTypeName"] = this.CardTypeID.SelectedItem.Text.Substring(this.CardTypeID.SelectedItem.Text.IndexOf("-") + 1);
            row["CardGradeID"] = int.Parse(this.CardGradeID.SelectedItem.Value);
            row["CardGradeCode"] = this.CardGradeID.SelectedItem.Text.Substring(0, this.CardGradeID.SelectedItem.Text.IndexOf("-"));
            row["CardGradeName"] = this.CardGradeID.SelectedItem.Text.Substring(this.CardGradeID.SelectedItem.Text.IndexOf("-") + 1);
            //row["OrderQty"] = int.Parse(this.CardQty.Text.Replace(",", "").Trim());
            int CardRoundUpQty = Tools.DALTool.GetReplenishment(int.Parse(this.StoreID.SelectedItem.Value), int.Parse(this.CardGradeID.SelectedItem.Value),2); //Modified by Robin 2014-11-14
            int PackageQty = int.Parse(this.CardQty.Text.Replace(",", "").Trim()) / CardRoundUpQty;
            row["OrderQty"] = CardRoundUpQty * PackageQty;
            row["PackageQty"] = PackageQty;
            row["OrderRoundUpQty"] = CardRoundUpQty;
            
            this.Detail.Rows.Add(row);

            this.BindDetail();
        }

        private void DeleteDetail(int CardGradeID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["CardGradeID"].ToString().Equals(CardGradeID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

   }
}