﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardCreationAutomatic
{
    public partial class PrintPR : PageBase
    {
        DataSet ds;
        DataSet detail;
        DataSet detail2;
        double iTotalQty;
        SVA.Model.CardGrade model;

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Request.Params["id"];

                ds = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H().GetList("OrderSupplierNumber ='" + id + "'");
                Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
                Tools.DataTool.AddSupplierDesc(ds, "SupplierName", "SupplierID");
                //Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");
                Tools.DataTool.AddCompanyName(ds, "CompanyName", "CompanyID");
                Tools.DataTool.AddUserName(ds, "CreatedByUserName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApprovedByUserName", "ApproveBy");

                //detail = new Edge.SVA.BLL.Ord_OrderToSupplier_Card_H().GetList("OrderSupplierNumber ='" + id + "'");
                //model = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt32(detail.Tables[0].Rows[0]["CardGradeID"]));
                //detail.Tables[0].Columns.Add("CardGradeAmount", typeof(string));
                //detail.Tables[0].Columns.Add("Qty", typeof(string));
                //detail.Tables[0].Rows[0]["CardGradeAmount"] = model.CardGradeAmount.ToString("F2");
                //try
                //{
                //    double iQty = Convert.ToInt32(detail.Tables[0].Rows[0]["CardQty"]) /  Convert.ToInt32(detail.Tables[0].Rows[0]["PackageQty"]);
                //    detail.Tables[0].Rows[0]["Qty"] = iQty.ToString();
                //}
                //catch 
                //{
                //    detail.Tables[0].Rows[0]["Qty"] = "0";
                //}
                //foreach (DataRow dr in detail.Tables[0].Rows)
                //{
                //    iTotalQty += Convert.ToDouble(detail.Tables[0].Rows[0]["Qty"]);
                //}

                CardCreationAutomaticController controller = new CardCreationAutomaticController();
                //Add By Robin 2014-08-13
                detail = controller.GetCardGradeList(id);
                detail.Tables[0].Columns.Add("CardGradeAmount", typeof(string));
                detail.Tables[0].Columns.Add("Qty", typeof(string));
                Tools.DataTool.AddCardGradeNameByID(detail, "CardGradeName", "CardGradeID"); //Add By Robin 2014-09-02
                foreach (DataRow dr in detail.Tables[0].Rows)
                {
                    model = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt32(dr["CardGradeID"]));
                    dr["CardGradeAmount"] = Convert.ToDecimal(model.CardTypeInitAmount).ToString("F2");
                    try
                    {
                        double iQty = Convert.ToInt32(dr["OrderQty"]);
                        dr["Qty"] = iQty.ToString("N0");
                    }
                    catch
                    {
                        dr["Qty"] = "0";
                    }
                    iTotalQty += Convert.ToDouble(dr["Qty"]);
                }

                //End


                detail2 = controller.GetCardNumberRange(id);
                detail2.Tables[0].Columns.Add("CardGradeAmountAddCardQty", typeof(string));
                detail2.Tables[0].Columns.Add("CardNumPattern", typeof(string));
                char[] CCardNumber;
                CCardNumber = new char[64];
                string sCardGradeAmountAddCardQty;
                foreach (DataRow dr in detail2.Tables[0].Rows) 
                {
                    model = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt32(dr["CardGradeID"]));
                    sCardGradeAmountAddCardQty = Convert.ToDecimal(model.CardTypeInitAmount).ToString("F2") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToInt32(dr["OrderQty"]).ToString("N0") + " pcs.";
                    dr["CardGradeAmountAddCardQty"] = sCardGradeAmountAddCardQty;
                    dr["CardNumPattern"] = Convert.ToString(model.CardNumPattern);
                    //Add By Robin 2014-07-18
                    try
                    {
                        Convert.ToString(dr["FirstCardNumber"]).CopyTo(Convert.ToString(dr["CardNumPattern"]).Length, CCardNumber, 0, Convert.ToString(dr["FirstCardNumber"]).Length - Convert.ToString(dr["CardNumPattern"]).Length);
                        dr["FirstCardNumber"] = new string(CCardNumber);
                        Convert.ToString(dr["EndCardNumber"]).CopyTo(Convert.ToString(dr["CardNumPattern"]).Length, CCardNumber, 0, Convert.ToString(dr["EndCardNumber"]).Length - Convert.ToString(dr["CardNumPattern"]).Length);
                        dr["EndCardNumber"] = new string(CCardNumber);
                    }
                    catch
                    { }
                    //End
                }

                this.rptOrders.DataSource = ds.Tables[0];
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalQty = (Label)e.Item.FindControl("lblTotalQty");
                lblTotalQty.Text = iTotalQty.ToString("N0");
            }
        }

        protected void rptOrderList2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                list.DataSource = detail.Tables[0];
                list.DataBind();


                Repeater list2 = e.Item.FindControl("rptOrderList2") as Repeater;
                if (list2 == null) return;

                System.Data.DataRowView drv2 = e.Item.DataItem as System.Data.DataRowView;
                if (drv2 == null) return;

                list2.DataSource = detail2.Tables[0];
                list2.DataBind();

                Repeater list1 = e.Item.FindControl("rptCardGradeList") as Repeater;
                if (list1 == null) return;

                System.Data.DataRowView drv1 = e.Item.DataItem as System.Data.DataRowView;
                if (drv1 == null) return;

                list1.DataSource = detail.Tables[0];
                list1.DataBind();
            }
        }

        #endregion


        #region 数据列表绑定

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}