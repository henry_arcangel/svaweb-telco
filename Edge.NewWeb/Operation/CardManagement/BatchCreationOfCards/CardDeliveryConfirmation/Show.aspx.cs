﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardDeliveryConfirmation;
using System.Data;


namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardDeliveryConfirmation
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardReceive_H, Edge.SVA.Model.Ord_CardReceive_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCardType(CardType, -1);
                //ControlTool.BindCardType(CardTypeID);

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardReceiveNumber.Text = Model.CardReceiveNumber;
                this.lblReferenceNo.Text = Model.ReferenceNo;
                this.ApprovalCode.Text = Model.ApprovalCode;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.ApproveBusDate.Text = ConvertTool.ToStringDate(Model.ApproveBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.lblRemark.Text = Model.Remark;
                this.lblRemark1.Text = Model.Remark1;
                this.lblRemark2.Text = Model.Remark2;
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.lblSupplierID.Text = this.SupplierID.SelectedItem.Text;
                this.lblSupplierAddress.Text = Model.SupplierAddress;
                this.lblSuppliertContactName.Text = Model.SuppliertContactName;
                this.lblSupplierPhone.Text = Model.SupplierPhone;

                 this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.lblStoreID.Text = this.StoreID.SelectedItem.Text;
                this.lblStorerAddress.Text = Model.StorerAddress;
                this.lblStoreContactName.Text = Model.StoreContactName;
                this.lblStorePhone.Text = Model.StorePhone;

                //this.CardTypeID.SelectedValue = Model.CardTypeID.ToString();
                //this.lblCardTypeID.Text = this.CardTypeID.SelectedItem.Text;
                //this.lblCardQty.Text = Model.CardQty.ToString();

                this.BindDetail3();
                //this.BindDetail();
                //this.BindDetail2();
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid3.PageIndex = e.NewPageIndex;
            this.BindDetail3();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
        //    Grid1.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
        //    Grid2.PageIndex = e.NewPageIndex;
        //    this.BindDetail2();
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            //if (model != null)
            //{
            //    StorerAddress.Text = model.StoreAddressDetail;
            //    StoreContactName.Text = model.Contact;
            //    StorePhone.Text = model.StoreTel;
            //}
            //else
            //{
            //    StorerAddress.Text = "";
            //    StoreContactName.Text = "";
            //    StorePhone.Text = "";
            //}

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            //if (model != null)
            //{
            //    SupplierAddress.Text = model.SupplierAddress;
            //    SuppliertContactName.Text = model.Contact;
            //    SupplierPhone.Text = model.ContactPhone;
            //}
            //else
            //{
            //    SupplierAddress.Text = "";
            //    SuppliertContactName.Text = "";
            //    SupplierPhone.Text = "";
            //}

            //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        private System.Data.DataTable Detail3
        {
            get
            {
                //if (ViewState["DetailResult3"] == null) //Removed By Robin 2014-11-07 important to show page by page
                {
                    int type = 0;
                    string para1 = "";
                    string para2 = "";
                    //Add By Robin 2014-10-10
                    int from, to;
                    from = this.Grid3.PageIndex * this.Grid3.PageSize + 1;
                    to = (this.Grid3.PageIndex + 1) * this.Grid3.PageSize;
                    //End
                    CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
                    System.Data.DataSet ds = controller.GetDetailList(type, CardReceiveNumber.Text, StoreID.SelectedItem.Text, "-1", para1, para2, from, to, "");
                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");
                    Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");

                    ViewState["DetailResult3"] = ds.Tables[0];
                }
                return ViewState["DetailResult3"] as System.Data.DataTable;
            }
        }

        //Add By Robin 2014-11-07
        private int DetailCount
        {
            get
            {
                int rtn = DALTool.GetCardReceiveDetailCount(CardReceiveNumber.Text, "");
                return rtn;
            }
        }
        //End

        //private System.Data.DataTable Detail
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult"] == null)
        //        {
        //            //int type = 0;
        //            int type = 1; //Modify By Robin 2014-09-05 To solve RRG approved transactions with huge records
        //            string para1 = "";
        //            //string para2 = "";
        //            string para2 = "1000"; //Modify By Robin 2014-09-05 To solve RRG approved transactions with huge records

        //            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CardReceiveNumber.Text, 2, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");

        //            ViewState["DetailResult"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult"] as System.Data.DataTable;
        //    }
        //}

        //private System.Data.DataTable Detail2
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult2"] == null)
        //        {
        //            int type = 0;
        //            string para1 = "";
        //            string para2 = "";

        //            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CardReceiveNumber.Text, 3, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");

        //            ViewState["DetailResult2"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult2"] as System.Data.DataTable;
        //    }
        //}

        private void BindDetail3()
        {
            //this.Grid3.RecordCount = this.Detail3.Rows.Count;
            this.Grid3.RecordCount = DetailCount;//Modified By Robin 2014-11-07
            //this.Grid3.DataSource = DataTool.GetPaggingTable(this.Grid3.PageIndex, this.Grid3.PageSize, this.Detail3);
            this.Grid3.DataSource = DataTool.GetPaggingTable(0, this.Grid3.PageSize, this.Detail3); //Modified By Robin 2014-11-07
            this.Grid3.DataBind();
        }

        //private void BindDetail()
        //{
        //    this.Grid1.RecordCount = this.Detail.Rows.Count;
        //    this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
        //    this.Grid1.DataBind();
        //}

        //private void BindDetail2()
        //{
        //    this.Grid2.RecordCount = this.Detail2.Rows.Count;
        //    this.Grid2.DataSource = DataTool.GetPaggingTable(this.Grid2.PageIndex, this.Grid2.PageSize, this.Detail2);
        //    this.Grid2.DataBind();
        //}

        protected override SVA.Model.Ord_CardReceive_H GetPageObject(SVA.Model.Ord_CardReceive_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            DataTable dt = controller.GetExportList(this.CardReceiveNumber.Text);
            SVA.BLL.Ord_CardReceive_H bll = new SVA.BLL.Ord_CardReceive_H();
            string fileName = controller.UpLoadFileToServer(bll.GetModel(this.CardReceiveNumber.Text), dt);

            try
            {
                string exportname = "SalesInvoice_PO.xls";
                Tools.ExportTool.ExportFile(fileName, exportname);
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
            }

        }
    }
}