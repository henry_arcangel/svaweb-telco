﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards.CardDeliveryConfirmation;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards.CardDeliveryConfirmation
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardReceive_H, Edge.SVA.Model.Ord_CardReceive_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.Grid1.PageSize = webset.ContentPageNum;
                //this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCardType(CardType, -1);
                ControlTool.BindCardType(CardTypeID1);
                ControlTool.BindCardGrade(CardGradeID1, -1);

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardReceiveNumber.Text = Model.CardReceiveNumber;
                this.lblReferenceNo.Text = Model.ReferenceNo;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.SupplierAddress.Text = Model.SupplierAddress;
                this.SuppliertContactName.Text = Model.SuppliertContactName;
                this.SupplierPhone.Text = Model.SupplierPhone;

                //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));

                this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.StorerAddress.Text = Model.StorerAddress;
                this.StoreContactName.Text = Model.StoreContactName;
                this.StorePhone.Text = Model.StorePhone;

                //this.CardTypeID.SelectedValue = Model.CardTypeID.ToString();
                //this.CardQty.Text = Model.CardQty.ToString();

                this.BindDetail3();
                //this.BindDetail();
                //this.BindDetail2();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardReceive_H item = null;
            Edge.SVA.Model.Ord_CardReceive_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            item = this.GetPageObject(dataItem);

            item.ReceiveType = 1;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardReceive_H>(item))
            {
                try
                {
                    CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
                    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                    //DataTable dt = (DataTable)ViewState["DetailResult"];
                    //DataTable dt2 = (DataTable)ViewState["DetailResult2"];
                    controller.UpdateCardStockStatus(dt3);
                    //controller.UpdateCardStockStatus(dt, 2);
                    //controller.UpdateCardStockStatus(dt2, 3);
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Success But Detail Failed", item.CardReceiveNumber), ex);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Success", item.CardReceiveNumber));
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Delivery Confirmation {0} Update Failed", item.CardReceiveNumber));
                ShowAddFailed();
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //Search 前先保存 Robin 2014-11-07
            try
            {
                CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            Grid3.PageIndex = e.NewPageIndex;
            this.BindDetail3();
        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid1.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        //protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid2.PageIndex = e.NewPageIndex;
        //    this.BindDetail2();
        //}

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                StorerAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StorePhone.Text = model.StoreTel;
            }
            else
            {
                StorerAddress.Text = "";
                StoreContactName.Text = "";
                StorePhone.Text = "";
            }

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            if (model != null)
            {
                SupplierAddress.Text = model.SupplierAddress;
                SuppliertContactName.Text = model.Contact;
                SupplierPhone.Text = model.ContactPhone;
            }
            else
            {
                SupplierAddress.Text = "";
                SuppliertContactName.Text = "";
                SupplierPhone.Text = "";
            }

            //ControlTool.BindCardTypeBySupplierID(this.CardTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID1, Convert.ToInt32(CardTypeID1.SelectedValue));
        }

        private System.Data.DataTable Detail3
        {
            get
            {
                //if (ViewState["DetailResult3"] == null) //Removed By Robin 2014-10-10 important to show page by page
                {
                    int type = 0;
                    string para1 = "";
                    string para2 = "";
                    string ReceiveNumber = ""; //Add By Robin 2014-09-04
                    

                    if (!string.IsNullOrEmpty(sFirstCardNumber.Text))
                    {
                        if (string.IsNullOrEmpty(sCardQty.Text))
                        {
                            this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                            ViewState["DetailResult3"] = new DataTable();
                            return ViewState["DetailResult3"] as System.Data.DataTable;
                        }
                        else
                        {
                            type = 1;
                            para1 = sFirstCardNumber.Text;
                            para2 = sCardQty.Text;
                        }
                    }
                    else if (!string.IsNullOrEmpty(sCardUID1.Text))
                    {
                        if (string.IsNullOrEmpty(sCardQty.Text))
                        {
                            this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                            ViewState["DetailResult3"] = new DataTable();
                            return ViewState["DetailResult3"] as System.Data.DataTable;
                        }
                        else
                        {
                            type = 2;
                            para1 = sCardUID1.Text;
                            para2 = sCardQty.Text;
                        }
                    }
                    else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text))
                    {
                        type = 3;
                        para1 = sCurrentCardNumber.Text;
                        para2 = "";
                    }
                    else if (!string.IsNullOrEmpty(sCardUID2.Text))
                    {
                        type = 4;
                        para1 = sCardUID2.Text;
                        para2 = "";
                    }
                    else
                    {
                        type = 0;
                        para1 = "";
                        para2 = "";
                    }

                    //Add By Robin 2014-10-10
                    int from, to;
                    from = this.Grid3.PageIndex * this.Grid3.PageSize + 1;
                    to = (this.Grid3.PageIndex + 1) * this.Grid3.PageSize;
                    //End

                    CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
                    if (para1 != "" || para2 != "") { ReceiveNumber = CardReceiveNumber.Text; } //Add By Robin 2014-09-04
                    System.Data.DataSet ds = controller.GetDetailList(type, ReceiveNumber, StoreID.SelectedItem.Text, CardGradeID1.SelectedValue, para1, para2,from, to, startCardNumber);
                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");
                    Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");

                    ViewState["DetailResult3"] = ds.Tables[0];
                }
                return ViewState["DetailResult3"] as System.Data.DataTable;
            }
        }

        //private System.Data.DataTable Detail
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult"] == null)
        //        {
        //            int type = 0;
        //            string para1 = "";
        //            string para2 = "";

        //            if (!string.IsNullOrEmpty(sFirstCardNumber.Text)) 
        //            {
        //                if (string.IsNullOrEmpty(sCardQty.Text))
        //                {
        //                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
        //                    ViewState["DetailResult"] = new DataTable();
        //                    return ViewState["DetailResult"] as System.Data.DataTable;
        //                }
        //                else 
        //                {
        //                    type = 1;
        //                    para1 = sFirstCardNumber.Text;
        //                    para2 = sCardQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCardUID1.Text)) 
        //            {
        //                if (string.IsNullOrEmpty(sCardQty.Text))
        //                {
        //                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
        //                    ViewState["DetailResult"] = new DataTable();
        //                    return ViewState["DetailResult"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 2;
        //                    para1 = sCardUID1.Text;
        //                    para2 = sCardQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text)) 
        //            {
        //                type = 3;
        //                para1 = sCurrentCardNumber.Text;
        //                para2 = "";
        //            }
        //            else if (!string.IsNullOrEmpty(sCardUID2.Text))
        //            {
        //                type = 4;
        //                para1 = sCardUID2.Text;
        //                para2 = "";
        //            }
        //            else 
        //            {
        //                type = 0;
        //                para1 = "";
        //                para2 = "";
        //            }

        //            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CardReceiveNumber.Text, 2, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");

        //            ViewState["DetailResult"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult"] as System.Data.DataTable;
        //    }
        //}

        //private System.Data.DataTable Detail2
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult2"] == null)
        //        {
        //            int type = 0;
        //            string para1 = "";
        //            string para2 = "";

        //            if (!string.IsNullOrEmpty(sFirstCardNumber.Text))
        //            {
        //                if (string.IsNullOrEmpty(sCardQty.Text))
        //                {
        //                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
        //                    ViewState["DetailResult2"] = new DataTable();
        //                    return ViewState["DetailResult2"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 1;
        //                    para1 = sFirstCardNumber.Text;
        //                    para2 = sCardQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCardUID1.Text))
        //            {
        //                if (string.IsNullOrEmpty(sCardQty.Text))
        //                {
        //                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
        //                    ViewState["DetailResult2"] = new DataTable();
        //                    return ViewState["DetailResult2"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 2;
        //                    para1 = sCardUID1.Text;
        //                    para2 = sCardQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text))
        //            {
        //                type = 3;
        //                para1 = sCurrentCardNumber.Text;
        //                para2 = "";
        //            }
        //            else if (!string.IsNullOrEmpty(sCardUID2.Text))
        //            {
        //                type = 4;
        //                para1 = sCardUID2.Text;
        //                para2 = "";
        //            }
        //            else
        //            {
        //                type = 0;
        //                para1 = "";
        //                para2 = "";
        //            }

        //            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CardReceiveNumber.Text, 3, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCardUIDByCardNumber(ds, "CardUID", "FirstCardNumber");

        //            ViewState["DetailResult2"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult2"] as System.Data.DataTable;
        //    }
        //}


        //Add By Robin 2014-11-07
        private int DetailCount
        {
            get
            {
                int rtn = DALTool.GetCardReceiveDetailCount(CardReceiveNumber.Text, "");
                return rtn;
            }
        }

        private void BindDetail3()
        {
            //this.Grid3.RecordCount = DetailCount;
            if (!string.IsNullOrEmpty(sCardQty.Text))
            {
                this.Grid3.RecordCount = Int32.Parse(sCardQty.Text.ToString());
            }
            //this.Grid3.RecordCount = this.Detail3.Rows.Count;
            this.Grid3.DataSource = DataTool.GetPaggingTable(0, this.Grid3.PageSize, this.Detail3);
            this.Grid3.DataBind();
        }

        //private void BindDetail()
        //{
        //    this.Grid1.RecordCount = this.Detail.Rows.Count;
        //    this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
        //    this.Grid1.DataBind();
        //}

        //private void BindDetail2()
        //{
        //    this.Grid2.RecordCount = this.Detail2.Rows.Count;
        //    this.Grid2.DataSource = DataTool.GetPaggingTable(this.Grid2.PageIndex, this.Grid2.PageSize, this.Detail2);
        //    this.Grid2.DataBind();
        //}

        protected override SVA.Model.Ord_CardReceive_H GetPageObject(SVA.Model.Ord_CardReceive_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            //Search 前先保存 Robin 2014-07-30
            try
            {
                CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
               return;
            }
            //End

            ViewState["DetailResult3"] = null;
            //ViewState["DetailResult"] = null;
            //ViewState["DetailResult2"] = null;
            this.BindDetail3();
            //this.BindDetail();
            //this.BindDetail2();
        }

        //protected void btnAdd1_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)ViewState["DetailResult"];
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid3.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid3.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt3.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt3.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt.NewRow();
        //            dr["StoreName"] = dt3.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt3.Rows[i]["KeyID"];
        //            dr["CardReceiveNumber"] = dt3.Rows[i]["CardReceiveNumber"];
        //            dr["CardTypeID"] = dt3.Rows[i]["CardTypeID"];
        //            dr["Description"] = dt3.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt3.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt3.Rows[i]["ActualQTY"];
        //            dr["FirstCardNumber"] = dt3.Rows[i]["FirstCardNumber"];
        //            dr["EndCardNumber"] = dt3.Rows[i]["EndCardNumber"];
        //            dr["BatchCardCode"] = dt3.Rows[i]["BatchCardCode"];
        //            dr["CardStockStatus"] = dt3.Rows[i]["CardStockStatus"];
        //            dr["ReceiveDateTime"] = dt3.Rows[i]["ReceiveDateTime"];
        //            dr["CardExpiryDate"] = dt3.Rows[i]["CardExpiryDate"];
        //            dr["StatusName"] = dt3.Rows[i]["StatusName"];
        //            dr["CardStockStatusName"] = dt3.Rows[i]["CardStockStatusName"];
        //            dr["ID"] = dt3.Rows[i]["ID"];
        //            dr["CardUID"] = dt3.Rows[i]["CardUID"];
        //            dt.Rows.InsertAt(dr, 0);
        //            dt3.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult"] = dt;
        //    ViewState["DetailResult3"] = dt3;
        //    this.BindDetail();
        //    this.BindDetail3();
        //}

        //protected void btnDelete1_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)ViewState["DetailResult"];
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid1.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid1.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt.Rows.Count -1;i > -1; i--) 
        //    {
        //        if (keyId.Contains(dt.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt3.NewRow();
        //            dr["StoreName"] = dt.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt.Rows[i]["KeyID"];
        //            dr["CardReceiveNumber"] = dt.Rows[i]["CardReceiveNumber"];
        //            dr["CardTypeID"] = dt.Rows[i]["CardTypeID"];
        //            dr["Description"] = dt.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt.Rows[i]["ActualQTY"];
        //            dr["FirstCardNumber"] = dt.Rows[i]["FirstCardNumber"];
        //            dr["EndCardNumber"] = dt.Rows[i]["EndCardNumber"];
        //            dr["BatchCardCode"] = dt.Rows[i]["BatchCardCode"];
        //            dr["CardStockStatus"] = dt.Rows[i]["CardStockStatus"];
        //            dr["ReceiveDateTime"] = dt.Rows[i]["ReceiveDateTime"];
        //            dr["CardExpiryDate"] = dt.Rows[i]["CardExpiryDate"];
        //            dr["StatusName"] = dt.Rows[i]["StatusName"];
        //            dr["CardStockStatusName"] = dt.Rows[i]["CardStockStatusName"];
        //            dr["ID"] = dt.Rows[i]["ID"];
        //            dr["CardUID"] = dt.Rows[i]["CardUID"];
        //            dt3.Rows.InsertAt(dr,0);
        //            dt.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult"] = dt;
        //    ViewState["DetailResult3"] = dt3;
        //    this.BindDetail();
        //    this.BindDetail3();
        //}

        //protected void btnAdd2_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    DataTable dt2 = (DataTable)ViewState["DetailResult2"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid3.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid3.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt3.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt3.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt2.NewRow();
        //            dr["StoreName"] = dt3.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt3.Rows[i]["KeyID"];
        //            dr["CardReceiveNumber"] = dt3.Rows[i]["CardReceiveNumber"];
        //            dr["CardTypeID"] = dt3.Rows[i]["CardTypeID"];
        //            dr["Description"] = dt3.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt3.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt3.Rows[i]["ActualQTY"];
        //            dr["FirstCardNumber"] = dt3.Rows[i]["FirstCardNumber"];
        //            dr["EndCardNumber"] = dt3.Rows[i]["EndCardNumber"];
        //            dr["BatchCardCode"] = dt3.Rows[i]["BatchCardCode"];
        //            dr["CardStockStatus"] = dt3.Rows[i]["CardStockStatus"];
        //            dr["ReceiveDateTime"] = dt3.Rows[i]["ReceiveDateTime"];
        //            dr["CardExpiryDate"] = dt3.Rows[i]["CardExpiryDate"];
        //            dr["StatusName"] = dt3.Rows[i]["StatusName"];
        //            dr["CardStockStatusName"] = dt3.Rows[i]["CardStockStatusName"];
        //            dr["ID"] = dt3.Rows[i]["ID"];
        //            dr["CardUID"] = dt3.Rows[i]["CardUID"];
        //            dt2.Rows.InsertAt(dr, 0);
        //            dt3.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult3"] = dt3;
        //    ViewState["DetailResult2"] = dt2;
        //    this.BindDetail3();
        //    this.BindDetail2();
        //}

        //protected void btnDelete2_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    DataTable dt2 = (DataTable)ViewState["DetailResult2"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid2.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid2.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt2.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt2.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt3.NewRow();
        //            dr["StoreName"] = dt2.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt2.Rows[i]["KeyID"];
        //            dr["CardReceiveNumber"] = dt2.Rows[i]["CardReceiveNumber"];
        //            dr["CardTypeID"] = dt2.Rows[i]["CardTypeID"];
        //            dr["Description"] = dt2.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt2.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt2.Rows[i]["ActualQTY"];
        //            dr["FirstCardNumber"] = dt2.Rows[i]["FirstCardNumber"];
        //            dr["EndCardNumber"] = dt2.Rows[i]["EndCardNumber"];
        //            dr["BatchCardCode"] = dt2.Rows[i]["BatchCardCode"];
        //            dr["CardStockStatus"] = dt2.Rows[i]["CardStockStatus"];
        //            dr["ReceiveDateTime"] = dt2.Rows[i]["ReceiveDateTime"];
        //            dr["CardExpiryDate"] = dt2.Rows[i]["CardExpiryDate"];
        //            dr["StatusName"] = dt2.Rows[i]["StatusName"];
        //            dr["CardStockStatusName"] = dt2.Rows[i]["CardStockStatusName"];
        //            dr["ID"] = dt2.Rows[i]["ID"];
        //            dr["CardUID"] = dt2.Rows[i]["CardUID"];
        //            dt3.Rows.InsertAt(dr, 0);
        //            dt2.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult3"] = dt3;
        //    ViewState["DetailResult2"] = dt2;
        //    this.BindDetail3();
        //    this.BindDetail2();
        //}

        protected void btnExport_Click(object sender, EventArgs e)
        {
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            DataTable dt = controller.GetExportList(this.CardReceiveNumber.Text);
            SVA.BLL.Ord_CardReceive_H bll = new SVA.BLL.Ord_CardReceive_H();
            string fileName = controller.UpLoadFileToServer(bll.GetModel(this.CardReceiveNumber.Text), dt);

            try
            {
                string exportname = "SalesInvoice_PO.xls";
                Tools.ExportTool.ExportFile(fileName, exportname);
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
            }

        }

        //protected void chkGood_Changed(object sender, System.EventArgs e)
        //{
        //    foreach (GridRow gvr in this.Grid1.Rows)   
        //    {   
        //        Control ctl = gvr.FindControl("chkGood");
        //        System.Web.UI.WebControls.CheckBox cb;
        //        cb = (System.Web.UI.WebControls.CheckBox)ctl;
        //        if (cb.Checked)   
        //        {
        //            Control ctl1 = gvr.FindControl("chkDamaged");
        //            System.Web.UI.WebControls.CheckBox cb1;
        //            cb1 = (System.Web.UI.WebControls.CheckBox)ctl1;
        //            cb1.Checked = false;
        //        }   
        //    }   
        //}

        //protected void chkDamaged_Changed(object sender, System.EventArgs e)
        //{
        //    System.Web.UI.WebControls.CheckBox cb;
        //    cb = (System.Web.UI.WebControls.CheckBox)sender;
        //    if (cb.Checked)
        //    { 
        //        //
        //    }
        //}

        protected void btnAddGood_OnClick(object sender, EventArgs e) 
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows) 
            {
                if (dr3["KeyID"].ToString() == txtKeyID.Text)
                {
                    if (dr3["CardStockStatus"].ToString() == "2")
                    {
                        dr3["CardStockStatus"] = 1;
                    }
                    else
                    {
                        dr3["CardStockStatus"] = 2;
                    }
                    break;
                }
            }
            ViewState["DetailResult3"] = dt3;
            //保存
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            try
            {
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            this.BindDetail3();
        }

        protected void btnAddDamaged_OnClick(object sender, EventArgs e)
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows)
            {
                if (dr3["KeyID"].ToString() == txtKeyID.Text)
                {
                    if (dr3["CardStockStatus"].ToString() == "3")
                    {
                        dr3["CardStockStatus"] = 1;
                    }
                    else
                    {
                        dr3["CardStockStatus"] = 3;
                    }
                }
            }
            ViewState["DetailResult3"] = dt3;
            //保存
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            try
            {
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            this.BindDetail3();
        }

        protected void chkAll_Changed(object sender, System.EventArgs e) 
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows)
            {
                if (chkAll.Checked)
                {
                    dr3["CardStockStatus"] = 2;
                }
                else
                {
                    dr3["CardStockStatus"] = 1;
                }

            }
            ViewState["DetailResult3"] = dt3;
            //保存
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            try
            {
                controller.UpdateCardStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            this.BindDetail3();
        }

        private string startCardNumber = "";

        protected void chkAllGood_Changed(object sender, System.EventArgs e)
        {
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            Grid3.PageIndex = 0;
            DataTable dt3;
            for (int j = 0; j < Grid3.PageCount; j++)
            {
                dt3 = this.Detail3;
                foreach (DataRow dr3 in dt3.Rows)
                {
                    if (chkAllGood.Checked)
                    {
                        dr3["CardStockStatus"] = 2;
                    }
                    else
                    {
                        dr3["CardStockStatus"] = 1;
                    }
                    startCardNumber = dr3["FirstCardNumber"].ToString();
                }
                //保存
                try
                {
                    controller.UpdateCardStockStatus(dt3);
                }
                catch (Exception ex)
                {
                    return;
                }
                //End
                Grid3.PageIndex = j + 1;
                //ViewState["DetailResult3"] = dt3;
            }
            Grid3.PageIndex = 0;
            startCardNumber = "";
            this.BindDetail3();
        }

        protected void chkAllDamage_Changed(object sender, System.EventArgs e)
        {
            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            Grid3.PageIndex = 0;
            DataTable dt3;
            for (int j = 0; j < Grid3.PageCount; j++)
            {
                dt3 = Detail3;
                foreach (DataRow dr3 in dt3.Rows)
                {
                    if (chkAllDamage.Checked)
                    {
                        dr3["CardStockStatus"] = 3;
                    }
                    else
                    {
                        dr3["CardStockStatus"] = 1;
                    }
                    startCardNumber = dr3["FirstCardNumber"].ToString();
                }
                //保存
                try
                {
                    controller.UpdateCardStockStatus(dt3);
                }
                catch (Exception ex)
                {
                    return;
                }
                //End
                Grid3.PageIndex = j + 1;
                //ViewState["DetailResult3"] = dt3;
            }
            Grid3.PageIndex = 0;
            startCardNumber = "";
            this.BindDetail3();
        }
    //    protected void chkAllGood_Changed(object sender, System.EventArgs e)
    //    {
    //        int startIndex = Grid3.PageIndex * Grid3.PageSize + 1;
    //        int endIndex = (Grid3.PageIndex + 1) * Grid3.PageSize;
    //        int index = 1;
    //        DataTable dt3 = this.Detail3;
    //        foreach (DataRow dr3 in dt3.Rows)
    //        {
    //            if (index >= startIndex && index <= endIndex)
    //            {
    //                if (chkAllGood.Checked)
    //                {
    //                    dr3["CardStockStatus"] = 2;
    //                }
    //                else
    //                {
    //                    dr3["CardStockStatus"] = 1;
    //                }
    //                startCardNumber = dr3["FirstCardNumber"].ToString();
    //            }
    //            //保存
    //            try
    //            {
    //                controller.UpdateCouponStockStatus(dt3);
    //            }
    //            catch (Exception ex)
    //            {
    //                return;
    //            }
    //            //End
    //            index++;
    //        }
    //        ViewState["DetailResult3"] = dt3;
    //        startCardNumber = "";
    //        this.BindDetail3();
    //    }

    //    protected void chkAllDamage_Changed(object sender, System.EventArgs e)
    //    {
    //        int startIndex = Grid3.PageIndex * Grid3.PageSize + 1;
    //        int endIndex = (Grid3.PageIndex + 1) * Grid3.PageSize;
    //        int index = 1;
    //        DataTable dt3 = this.Detail3;
    //        foreach (DataRow dr3 in dt3.Rows)
    //        {
    //            if (index >= startIndex && index <= endIndex)
    //            {
    //                if (chkAllDamage.Checked)
    //                {
    //                    dr3["CardStockStatus"] = 3;
    //                }
    //                else
    //                {
    //                    dr3["CardStockStatus"] = 1;
    //                }
    //                startCardNumber = dr3["FirstCardNumber"].ToString();
    //            }
    //            index++;
    //        }
    //        ViewState["DetailResult3"] = dt3;
    //        startCardNumber = "";
    //        this.BindDetail3();
    //    }
    //}

        protected void SaveToGoodButton_Click(object sender, EventArgs e)
        {
            UpdateDetail("2");
            BindDetail3();
        }

        protected void SaveToDamageButton_Click(object sender, EventArgs e)
        {
            UpdateDetail("3");
            BindDetail3();
        }

        public Boolean UpdateDetail(string para3)
        {
            int type = 0;
            string CardGradeID;
            string para1 = "";
            string para2 = "";
            string sql = "";
            string sql1 = "";

            CardGradeID = CardGradeID1.SelectedValue;
            string ReceiveNumber = ""; //Add By Robin 2014-09-04


            if (!string.IsNullOrEmpty(sFirstCardNumber.Text))
            {
                if (string.IsNullOrEmpty(sCardQty.Text))
                {
                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                }
                else
                {
                    type = 1;
                    para1 = sFirstCardNumber.Text;
                    para2 = sCardQty.Text;
                }
            }
            else if (!string.IsNullOrEmpty(sCardUID1.Text))
            {
                if (string.IsNullOrEmpty(sCardQty.Text))
                {
                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                }
                else
                {
                    type = 2;
                    para1 = sCardUID1.Text;
                    para2 = sCardQty.Text;
                }
            }
            else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text))
            {
                type = 3;
                para1 = sCurrentCardNumber.Text;
                para2 = "";
            }
            else if (!string.IsNullOrEmpty(sCardUID2.Text))
            {
                type = 4;
                para1 = sCardUID2.Text;
                para2 = "";
            }
            else
            {
                type = 0;
                para1 = "";
                para2 = "";
            }

            CardDeliveryConfirmationController controller = new CardDeliveryConfirmationController();
            if (para1 != "" || para2 != "") { ReceiveNumber = CardReceiveNumber.Text; } //Add By Robin 2014-09-04

            if (CardGradeID != "-1")
            { sql1 = " and a.CardGradeID=" + CardGradeID + " "; }

            if (type == 0)
            {
                sql = " update Ord_CardReceive_D set CardStockStatus=" + para3 + " where CardReceiveNumber='" + ReceiveNumber + "'  ";
            }
            else if (type == 1)
            {
                sql = " update Ord_CardReceive_D set CardStockStatus=" + para3 + " where KeyID in (select top " + para2 + " KeyID from Ord_CardReceive_D where CardReceiveNumber='" + ReceiveNumber + "' and FirstCardNumber >= '" + para1 + "' order by FirstCardNumber) ";
            }
            else if (type == 2)
            {
                string cardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    cardNumber = model.CardNumber;
                }
                sql = " update Ord_CardReceive_D set CardStockStatus=" + para3 + " where KeyID in (select top " + para2 + " KeyID from Ord_CardReceive_D where CardReceiveNumber='" + ReceiveNumber + "' and FirstCardNumber >= '" + cardNumber + "' order by FirstCardNumber) ";
            }
            else if (type == 3)
            {
                sql = " update Ord_Cardive_D set CardStockStatus=" + para3 + " where CardReceiveNumber='" + ReceiveNumber + "' and FirstCardNumber = '" + para1 + "'  ";
            }
            else
            {
                string cardNumber = "99999999";
                Edge.SVA.Model.CardUIDMap model = new Edge.SVA.BLL.CardUIDMap().GetModel(para1);
                if (model != null)
                {
                    cardNumber = model.CardNumber;
                }
                sql = " update Ord_CardReceive_D set CardStockStatus=" + para3 + " where CardReceiveNumber='" + ReceiveNumber + "' and FirstCardNumber = '" + cardNumber + "'  ";
            }
            DataSet ds3 = DBUtility.DbHelperSQL.Query(sql);
            return true;
        }
    }
}