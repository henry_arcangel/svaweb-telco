﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardPointAmountPicking;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountPicking
{
    public partial class PrintAR : PageBase
    {
        DataSet ds;
        DataSet detail;
        DataSet summary;
        CardPointAmountPickingController controller = new CardPointAmountPickingController();
        int iTotalPickQTY;
        double iTotalAmount;

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Request.Params["id"];

                ds = new Edge.SVA.BLL.Ord_CardPicking_H().GetList("CardPickingNumber ='" + id + "'");
                ds.Tables[0].Columns.Add("CardGradeID", typeof(string));
                ds.Tables[0].Columns.Add("TotalAmount", typeof(string));
                //detail = controller.GetDetailList(id);
                //detail = controller.GetDetailListGroup(id); //Modified By Robin 2014-08-06 for RRG group Card number
                detail = new Edge.SVA.BLL.Ord_CardPicking_D().GetList("CardPickingNumber ='" + id + "'");

                detail.Tables[0].Columns.Add("Amount", typeof(string));
                detail.Tables[0].Columns.Add("SKUCode", typeof(string));
                detail.Tables[0].Columns.Add("SKUDesc", typeof(string));
                detail.Tables[0].Columns.Add("UPC", typeof(string));

                if (detail.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Rows[0]["CardGradeID"] = Convert.ToString(detail.Tables[0].Rows[0]["CardGradeID"]);
                }
                else 
                {
                    ds.Tables[0].Rows[0]["CardGradeID"] = "-1";
                }
                //Add By Robin 2014-09-02
                summary = controller.GetCardGradeList(id);
                Tools.DataTool.AddCardGradeNameByID(summary, "CardGradeName", "CardGradeID");
                summary.Tables[0].Columns.Add("TotalAmount", typeof(string));
                //
                Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
                Tools.DataTool.AddStoreName(ds, "FromStoreName", "FromStoreID");
                Tools.DataTool.AddCardGradeNameByID(ds, "CardGradeName", "CardGradeID");
                Tools.DataTool.AddCustomerDesc(ds, "CustomerName", "CustomerID");

                List<Edge.SVA.Model.ImportGM> imlist = (new Edge.SVA.BLL.ImportGM()).GetModelList("Export = 'PR'");
                int iPickQTY;
                double iCardAmount;
                string CardGradeCode = "";
                string SKUCode = "";
                string SKUDesc = "";
                string UPC = "";
                foreach (DataRow dr in detail.Tables[0].Rows) 
                {
                    try
                    {
                        iPickQTY = 1;  // Convert.ToInt32(dr["PickQTY"]);
                        iCardAmount = Convert.ToDouble(dr["PickAmount"]); //Convert.ToDouble(dr["ActualAmount"]);
                        Edge.SVA.Model.CardGrade cardgrade = new Edge.SVA.BLL.CardGrade().GetModel(Convert.ToInt32(dr["CardGradeID"]));
                        CardGradeCode = cardgrade.CardGradeCode; 
                        foreach(Edge.SVA.Model.ImportGM im in imlist)
                        {
                            if (im.CardGradeCode == CardGradeCode)
                            {
                                SKUCode = im.SKU;
                                SKUDesc = im.SKUDesc;
                                UPC = im.UPC;
                                break;
                            }
                        }
                    }
                    catch 
                    {
                        iPickQTY = 0;
                        iCardAmount = 0;
                        SKUCode ="";
                        SKUDesc = "";
                        UPC ="";
                    }
                    double iAmount = iCardAmount;
                    //dr["CardAmount"] = (iCardAmount/iPickQTY).ToString("F2");
                    //dr["CardAmount"] = iCardAmount.ToString("F2");
                    dr["Amount"] = iCardAmount.ToString("F2");
                    dr["SKUCode"] = SKUCode;
                    dr["SKUDesc"] = SKUDesc;
                    dr["UPC"] = UPC;
                    iTotalPickQTY += iPickQTY;
                    iTotalAmount += iCardAmount;
                }

                ds.Tables[0].Rows[0]["TotalAmount"] = iTotalAmount.ToString("F2");               
                this.rptOrders.DataSource = ds.Tables[0];
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                //Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                //lblTotalPickQTY.Text = iTotalPickQTY.ToString();
                Label lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");
                if (lblTotalAmount != null) 
                    lblTotalAmount.Text = iTotalAmount.ToString("F2");

                Label lblTotalAmt = (Label)e.Item.FindControl("lblTotalAmt");
                if (lblTotalAmt != null)
                    lblTotalAmt.Text = iTotalAmount.ToString("F2");
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                list.DataSource = detail.Tables[0];
                list.DataBind();

                //Add By Robin 2014-09-02
                Repeater list1 = e.Item.FindControl("rptOrderSummary") as Repeater;
                if (list1 == null) return;

                System.Data.DataRowView drv1 = e.Item.DataItem as System.Data.DataRowView;
                if (drv1 == null) return;

                list1.DataSource = summary.Tables[0];
                list1.DataBind();

                Repeater list2 = e.Item.FindControl("rptCardGradeList") as Repeater;
                if (list2 == null) return;

                System.Data.DataRowView drv2 = e.Item.DataItem as System.Data.DataRowView;
                if (drv2 == null) return;

                list2.DataSource = summary.Tables[0];
                list2.DataBind();

                //Label lblTotalAmount1 = (Label)e.Item.FindControl("lblTotalAmount1");
                //lblTotalAmount1.Text = iTotalAmount.ToString("F2");
                //End
            }
        }

        #endregion


        #region 数据列表绑定

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}