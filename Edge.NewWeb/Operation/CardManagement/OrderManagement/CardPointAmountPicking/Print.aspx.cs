﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountPicking
{
    public partial class Print : PageBase
    {
        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["TotalOrderAmount"] = 0;
                ViewState["TotalActAmount"] = 0;

                //RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardPickingNumber");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CardGradeCode"] = null;
                ViewState["CardGrade"] = null;
                ViewState["OrderAmount"] = null;

                //string ids = Request.Params["id"];

                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                DataTable orders = new DataTable();
                orders.Columns.Add("CardPickingNumber", typeof(string));
                orders.Columns.Add("PrintDateTime", typeof(string));
                orders.Columns.Add("ReferenceNo", typeof(string));
                orders.Columns.Add("ApproveStatus", typeof(string));
                orders.Columns.Add("PickingDate", typeof(string));
                orders.Columns.Add("PickedBy", typeof(string));


                //if (Model.ApproveStatus.ToUpper().Trim()!="A")
                //{
                //    JscriptPrint(Resources.MessageTips.YouNotApprove, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                //    return;
                //}

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CardPicking_H mode = new Edge.SVA.BLL.Ord_CardPicking_H().GetModel(id);
                    if (mode == null) continue;
                    DataRow order = orders.NewRow();
                    order["CardPickingNumber"] = mode.CardPickingNumber;
                    order["PrintDateTime"] = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                    order["ReferenceNo"] = mode.ReferenceNo;
                    order["ApproveStatus"] = Tools.DALTool.GetOrderPickingApproveStatusString(mode.ApproveStatus);
                    order["PickingDate"] = Tools.ConvertTool.ToStringDate(mode.ApproveOn.GetValueOrDefault());
                    order["PickedBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());

                    orders.Rows.Add(order);

                    this.rptOrders.DataSource = orders;
                    this.rptOrders.DataBind();
                }
                //this.lblCardPickingNumber.Text = this.Model.CardPickingNumber;
                //this.lblApproveStatus.Text = Tools.DALTool.GetOrderPickingApproveStatusString(Model.ApproveStatus);
                //this.lblPickedBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                //this.lblPickingDate.Text = Tools.ConvertTool.ToStringDate(Model.ApproveOn.GetValueOrDefault());
                //this.lblPrintDateTime.Text = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                //this.lblReferenceNo.Text = Model.ReferenceNo;
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
           // RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardPickingNumber");
        }
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCardGradeCode = (Label)e.Item.FindControl("lblCardGradeCode");
                if (lblCardGradeCode != null)
                {
                    Label lblCardGrade = (Label)e.Item.FindControl("lblCardGrade");
                    Label lblOrderAmount = (Label)e.Item.FindControl("lblOrderAmount");

                    //重复
                    if (ViewState["CardGradeCode"] != null && ViewState["CardGradeCode"].ToString().Trim() == lblCardGradeCode.Text.Trim())
                    {
                        lblCardGradeCode.Visible = false;
                        if (lblCardGrade != null) { lblCardGrade.Visible = false; }
                        if (lblOrderAmount != null) { lblOrderAmount.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CardGradeCode"] = lblCardGradeCode.Text.Trim();
                        if (lblCardGrade != null)
                        {
                            ViewState["CardGrade"] = lblCardGrade.Text.Trim();                         
                        }
                        if (lblOrderAmount != null)
                        {                            
                            ViewState["OrderAmount"] = lblOrderAmount.Text.Trim();                             
                            //统计数量
                            ViewState["TotalOrdeAmount"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderAmount"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderAmount.Text.Trim());
                        }
                        //((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }

                Label lblActAmount = (Label)e.Item.FindControl("lblActAmount");
                if (lblActAmount != null)
                {  
                    //统计数量
                    ViewState["TotalActAmount"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalActAmount"].ToString()) + Tools.ConvertTool.ConverType<long>(lblActAmount.Text.Trim());
                } 
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderAmount = (Label)e.Item.FindControl("lblTotalOrderAmount");
                if (lblTotalOrderAmount != null)
                {
                    lblTotalOrderAmount.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderAmount"].ToString()).ToString();
                }
                Label lblTotalActAmount = (Label)e.Item.FindControl("lblTotalActAmount");
                if (lblTotalActAmount != null)
                {
                    lblTotalActAmount.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalActAmount"].ToString()).ToString();
                }

            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CardGradeCode"] = null;
                ViewState["CardGrade"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderAmount"] = 0;
                ViewState["TotalActAmount"] = 0;

                System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CardPicking_D().GetList(string.Format("CardPickingNumber = '{0}'", drv["CardPickingNumber"].ToString()) + " order by CardGradeID,KeyID");

                Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");

                list.DataSource = ds.Tables[0];
                list.DataBind();
            }
        }

        #endregion


        #region 数据列表绑定
        //private void RptBind(string strWhere, string orderby)
        //{
        //    ViewState["CardGradeCode"] = null;
        //    ViewState["CardGrade"] = null;
        //    ViewState["OrderQTY"] = null;

        //    Edge.SVA.BLL.Ord_CardPicking_D bll = new Edge.SVA.BLL.Ord_CardPicking_D();

        //    System.Data.DataSet ds = null;

        //    ds = bll.GetList(strWhere);

        //    Tools.DataTool.AddCardGradeNameByID(ds, "CardGrade", "CardGradeID");
        //    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();

        //    //统计
        //    long totalOrderQTY = 0;
        //    long totalPickQTY = 0;
        //    Controllers.CardOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
        //    lblTotalOrderQTY.Text = totalOrderQTY.ToString();
        //    lblTotalPickQTY.Text = totalPickQTY.ToString();
        //}

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}