﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountPicking
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    ViewState["TotalOrderQTY"] = 0;
                    ViewState["TotalPickQTY"] = 0;

                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));

                    DataTable orders = new DataTable();
                    orders.Columns.Add("CardPickingNumber", typeof(string));
                    orders.Columns.Add("PrintDateTime", typeof(string));
                    orders.Columns.Add("ReferenceNo", typeof(string));
                    orders.Columns.Add("ApproveStatus", typeof(string));
                    orders.Columns.Add("PickingDate", typeof(string));
                    orders.Columns.Add("PickedBy", typeof(string));

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    bool isSuccess = false;
                    foreach (string id in idList)
                    {
                        Edge.SVA.Model.Ord_CardPicking_H mode = new Edge.SVA.BLL.Ord_CardPicking_H().GetModel(id);

                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = id;
                        dr["ApproveCode"] = CardOrderController.ApproveForApproveCode(mode, out isSuccess);
                        if (isSuccess)
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Order Picking " + mode.CardPickingNumber + " " + Resources.MessageTips.ApproveCode);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                            //打印预览
                            this.div_print.Visible = true;
                            this.btnPrint.Visible = true;

                            DataRow order = orders.NewRow();

                            order["CardPickingNumber"] = mode.CardPickingNumber;
                            order["PrintDateTime"] = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                            order["ReferenceNo"] = mode.ReferenceNo;
                            order["ApproveStatus"] = Tools.DALTool.GetOrderPickingApproveStatusString(mode.ApproveStatus);
                            order["PickingDate"] = Tools.ConvertTool.ToStringDate(mode.ApproveOn.GetValueOrDefault());
                            order["PickedBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());

                            orders.Rows.Add(order);

                            this.rptOrders.DataSource = orders;
                            this.rptOrders.DataBind();
                        }
                        else
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Order Picking " + mode.CardPickingNumber + " " + Resources.MessageTips.ApproveError);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;

                            //打印预览
                            this.div_print.Visible = false;
                            this.btnPrint.Visible = false;
                        }
                        dt.Rows.Add(dr);
                    }
                    this.rptList.DataSource = dt;
                    this.rptList.DataBind();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    //FineUI.Alert.ShowInTop(Resources.MessageTips.SystemError, "", FineUI.MessageBoxIcon.Error, FineUI.ActiveWindow.GetHidePostBackReference());
                    FineUI.Alert.ShowInTop("Approve Card Order Picking fail: "+ex.Message, "", FineUI.MessageBoxIcon.Error, FineUI.ActiveWindow.GetHidePostBackReference()); //Modified By Robin 2014-07-18 
                }
            }
        }


        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            //ViewState["CardGradeCode"] = null;
            //ViewState["CardGrade"] = null;
            //ViewState["OrderQTY"] = null;

            //Edge.SVA.BLL.Ord_CardPicking_D bll = new Edge.SVA.BLL.Ord_CardPicking_D();

            //System.Data.DataSet ds = null;

            //ds = bll.GetList(strWhere);

            //Tools.DataTool.AddCardGradeNameByID(ds, "CardGrade", "CardGradeID");
            //Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");

            //this.rptPrintList.DataSource = ds.Tables[0].DefaultView;
            //this.rptPrintList.DataBind();
        }
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCardGradeCode = (Label)e.Item.FindControl("lblCardGradeCode");
                if (lblCardGradeCode != null)
                {
                    Label lblCardGrade = (Label)e.Item.FindControl("lblCardGrade");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");

                    //重复
                    if (ViewState["CardGradeCode"] != null && ViewState["CardGradeCode"].ToString().Trim() == lblCardGradeCode.Text.Trim())
                    {
                        lblCardGradeCode.Visible = false;
                        if (lblCardGrade != null) { lblCardGrade.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CardGradeCode"] = lblCardGradeCode.Text.Trim();
                        if (lblCardGrade != null)
                        {
                            ViewState["CardGrade"] = lblCardGrade.Text.Trim();
                        }
                        if (lblOrderQTY != null)
                        {
                            ViewState["OrderQTY"] = lblOrderQTY.Text.Trim();
                            //统计数量
                            ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                        }
                        ((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }

                Label lblPickQTY = (Label)e.Item.FindControl("lblPickQTY");
                if (lblPickQTY != null)
                {
                    //统计数量
                    ViewState["TotalPickQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblPickQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()).ToString();
                }
            
            }
            
        }


        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CardGradeCode"] = null;
                ViewState["CardGrade"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                //Removed By Robin 2014-11-13
                //System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CardPicking_D().GetList(string.Format("CardPickingNumber = '{0}'", drv["CardPickingNumber"].ToString()) + " order by CardGradeID,KeyID");

                //Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                //Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");

                //list.DataSource = ds.Tables[0];
                //list.DataBind();
                //End
            }
        }

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}