﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintAR.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountPicking.PrintAR" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div align="center" style="width: 100%; text-align: center;">
        <%--<asp:Button ID="btnPrint" runat="server" Text="打印" OnClientClick="printdiv('div_print')" />--%>
        <%--        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
            class="submit" />--%>
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0">
                    <tr>
                        <th colspan="3" align="center">
                            TELCO ACKNOWLEDGEMENT RECEIPT
                        </th>
                    </tr>
                    <tr>
                        <td width="25%">
                            Date:
                        </td>
                        <td width="25%">
                            <%#Eval("ApproveOn")%>
                        </td>
                        <td width="50%" align="right">
                            A/R#:<%#Eval("CardPickingNumber")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td colspan="2">
                            <%#Eval("StoreName")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td colspan="2">
                            <%#Eval("FromStoreName")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Re:
                        </td>
                        <td colspan="2">
                            <%--<%#Eval("CardGradeName")%>--%>
                            <tr>
                            <td colspan="3">
                                <asp:Repeater ID="rptCardGradeList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblCardGradeList" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                            </tr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Issuance:
                        </td>
                        <td colspan="2">
                            <%#Eval("Remark")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Customer:
                        </td>
                        <td colspan="2">
                            <%#Eval("Remark1")%>
                        </td>
                    </tr>
<%--                    <tr>
                        <td>
                            Please acknowledge the receipt of
                        </td>
                        <td colspan="2">
                            <%#Eval("CardGradeName")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            worth:
                        </td>
                        <td colspan="2">
                            <%#Eval("TotalAmount")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            P <%#Eval("TotalAmount")%> as follows
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptOrderSummary" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                Please acknowledge the receipt of
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            worth:
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("ActualAmount")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td>
                                            Total of:
                                        </td>
                                        <td>
                                            <%--<%#Eval("TotalAmount")%> --%>
                                            <asp:Label ID="lblTotalAmt" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            as follows
                                        </td >
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                STYLE CODE /
                                            </td>
                                            <td>
                                                STYLE DESCRIPTION /SKU
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td>
                                                UPC
                                            </td>
                                            <td align="right">
                                                AMOUNT
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                SKU CODE
                                            </td>
                                            <td>
                                                DESCRIPTION
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblSKUCode" runat="server" Text='<%#Eval("SKUCode")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblSKUDesc" runat="server" Text='<%#Eval("SKUDesc")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblUPC" runat="server" Text='<%#Eval("UPC")%>'></asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            TOTAL
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                        <td align="right">
                                           <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="3">
                            <table width="100%" border="0"
                                runat="server" id="dtEnd">
                                <tr>
                                    <td>
                                        Prepared by:
                                    </td>
                                    <td>
                                        AR:<%#Eval("CardPickingNumber")%>
                                    </td>
                                    <td>
                                        Received by:
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
