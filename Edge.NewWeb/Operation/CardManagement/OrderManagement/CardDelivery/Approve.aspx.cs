﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardDelivery
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    ViewState["TotalOrderQTY"] = 0;
                    ViewState["TotalPickQTY"] = 0;
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));

                    DataTable orders = new DataTable();
                    orders.Columns.Add("CardDeliveryNumber", typeof(string));
                    orders.Columns.Add("CreatedBusDate", typeof(string));
                    orders.Columns.Add("ApprovalCode", typeof(string));
                    orders.Columns.Add("ApproveBusDate", typeof(string));
                    orders.Columns.Add("ApproveStatus", typeof(string));
                    orders.Columns.Add("CreatedOn", typeof(string));
                    orders.Columns.Add("CreatedBy", typeof(string));
                    orders.Columns.Add("ApproveOn", typeof(string));
                    orders.Columns.Add("ApproveBy", typeof(string));
                    orders.Columns.Add("CustomerType", typeof(string));
                    orders.Columns.Add("SendMethod", typeof(string));
                    orders.Columns.Add("NeedActive", typeof(string));
                    orders.Columns.Add("StoreID", typeof(string));
                    orders.Columns.Add("Brand", typeof(string));
                    orders.Columns.Add("CustomerID", typeof(string));
                    orders.Columns.Add("Remark", typeof(string));
                    orders.Columns.Add("OrdersCount", typeof(string));
                    orders.Columns.Add("SendAddress", typeof(string));
                    orders.Columns.Add("ContactName", typeof(string));
                    orders.Columns.Add("Email", typeof(string));
                    orders.Columns.Add("SMSMMS", typeof(string));
                    orders.Columns.Add("ContactNumber", typeof(string));


                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    //Add By Robin 2014-07-14
                    string needActive = Request.Params["NeedActive"];
                    //

                    bool isSuccess = false;
                    foreach (string id in idList)
                    {
                        Edge.SVA.Model.Ord_CardDelivery_H mode = new Edge.SVA.BLL.Ord_CardDelivery_H().GetModel(id);
                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = id;
                        dr["ApproveCode"] = CardOrderController.ApproveForApproveCode(mode, out isSuccess);
                        if (isSuccess)
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Order Delivery " + mode.CardDeliveryNumber + " " + Resources.MessageTips.ApproveCode);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                            #region Print list
                            this.div_print.Visible = true;
                            this.btnPrint.Visible = true;
                            //this.CardDeliveryNumber.Text = mode.CardDeliveryNumber;
                            //this.CreatedBusDate.Text = ConvertTool.ToStringDate(mode.CreatedBusDate.GetValueOrDefault());
                            //this.lblApproveStatus.Text = DALTool.GetApproveStatusString(mode.ApproveStatus);
                            //this.CreatedOn.Text = ConvertTool.ToStringDateTime(mode.CreatedOn.GetValueOrDefault());
                            //this.lblCreatedBy.Text = Tools.DALTool.GetUserName(mode.CreatedBy.GetValueOrDefault());

                            //CustomerType.SelectedValue = mode.CustomerType.GetValueOrDefault().ToString();
                            //this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                            //this.CustomerType.Visible = false;

                            //SendMethod.SelectedValue = mode.SendMethod.GetValueOrDefault().ToString();
                            //this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                            //this.SendMethod.Visible = false;

                            //NeedActive.SelectedValue = mode.NeedActive.GetValueOrDefault().ToString();
                            //this.NeedActiveView.Text = NeedActive.SelectedItem == null ? "" : NeedActive.SelectedItem.Text;
                            //this.NeedActive.Visible = false;

                            //Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(mode.StoreID.GetValueOrDefault());
                            //this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                            //Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                            //this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                            //Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(mode.CustomerID.GetValueOrDefault());
                            //this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                            DataRow order = orders.NewRow();
                            order["CardDeliveryNumber"] = mode.CardDeliveryNumber;
                            order["CreatedBusDate"] = ConvertTool.ToStringDate(mode.CreatedBusDate.GetValueOrDefault());
                            order["ApproveBusDate"] = ConvertTool.ToStringDate(mode.ApproveBusDate.GetValueOrDefault());
                            order["ApprovalCode"] = dr["ApproveCode"].ToString();
                            order["ApproveStatus"] = DALTool.GetApproveStatusString(mode.ApproveStatus);
                            order["CreatedOn"] = ConvertTool.ToStringDateTime(mode.CreatedOn.GetValueOrDefault());
                            order["CreatedBy"] = Tools.DALTool.GetUserName(mode.CreatedBy.GetValueOrDefault());
                            order["ApproveOn"] = ConvertTool.ToStringDateTime(mode.ApproveOn.GetValueOrDefault());
                            order["ApproveBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());
                            CustomerType.SelectedValue = mode.CustomerType.GetValueOrDefault().ToString();
                            order["CustomerType"] = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                            this.CustomerType.Visible = false;
                            //Add By Robin 2014-07-14
                            if (needActive == "1")
                            {
                                SendMethod.SelectedValue = needActive;
                                mode.NeedActive = 1;
                            }
                            else
                            {
                                SendMethod.SelectedValue = mode.SendMethod.GetValueOrDefault().ToString();
                            }
                            //
                            //SendMethod.SelectedValue = mode.SendMethod.GetValueOrDefault().ToString();
                            order["SendMethod"] = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                            this.SendMethod.Visible = false;
                            NeedActive.SelectedValue = mode.NeedActive.GetValueOrDefault().ToString();
                            order["NeedActive"] = NeedActive.SelectedItem == null ? "" : NeedActive.SelectedItem.Text;
                            this.NeedActive.Visible = false;
                            Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(mode.StoreID.GetValueOrDefault());
                            order["StoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                            Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                            order["Brand"] = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);
                            Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(mode.CustomerID.GetValueOrDefault());
                            order["CustomerID"] = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                            order["Remark"] = mode.Remark;
                            order["OrdersCount"] = Controllers.CardOrderController.GetDeliveryOrderTotalQty(mode.CardDeliveryNumber).ToString();
                            order["SendAddress"] = mode.SendAddress;
                            order["ContactName"] = mode.StoreContactName;
                            order["Email"] = mode.StoreContactEmail;
                            //order["SMSMMS"] = mode.SMSMMS;
                            order["ContactNumber"] = mode.StoreContactPhone;

                            orders.Rows.Add(order);

                            this.rptOrders.DataSource = orders;
                            this.rptOrders.DataBind();

                            // RptBind(string.Format("CardDeliveryNumber='{0}'", id));
                            #endregion
                        }
                        else
                        {
                            this.div_print.Visible = false;
                            this.btnPrint.Visible = false;

                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Card Order Delivery " + mode.CardDeliveryNumber + " " + Resources.MessageTips.ApproveError);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }
                        dt.Rows.Add(dr);
                    }
                    this.rptList.DataSource = dt;
                    this.rptList.DataBind();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    FineUI.Alert.ShowInTop(Resources.MessageTips.SystemError, "", FineUI.MessageBoxIcon.Error, FineUI.ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        #region Print List
        //private void RptBind(string strWhere)
        //{
        //    ViewState["CardGradeCode"] = null;
        //    ViewState["CardGrade"] = null;
        //    ViewState["OrderQTY"] = null;

        //    Edge.SVA.BLL.Ord_CardDelivery_D bll = new Edge.SVA.BLL.Ord_CardDelivery_D();

        //    System.Data.DataSet ds = null;

        //    ds = bll.GetList(strWhere);

        //    Tools.DataTool.AddCardGradeNameByID(ds, "CardGrade", "CardGradeID");
        //    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");

        //    this.rptPrintList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptPrintList.DataBind();
        //}
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCardGradeCode = (Label)e.Item.FindControl("lblCardGradeCode");
                if (lblCardGradeCode != null)
                {
                    Label lblCardGrade = (Label)e.Item.FindControl("lblCardGrade");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");

                    //重复
                    if (ViewState["CardGradeCode"] != null && ViewState["CardGradeCode"].ToString().Trim() == lblCardGradeCode.Text.Trim())
                    {
                        lblCardGradeCode.Visible = false;
                        if (lblCardGrade != null) { lblCardGrade.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CardGradeCode"] = lblCardGradeCode.Text.Trim();
                        if (lblCardGrade != null)
                        {
                            ViewState["CardGrade"] = lblCardGrade.Text.Trim();
                        }
                        if (lblOrderQTY != null)
                        {
                            ViewState["OrderQTY"] = lblOrderQTY.Text.Trim();
                            //统计数量
                            ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                        }
                        ((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }

                Label lblPickQTY = (Label)e.Item.FindControl("lblPickQTY");
                if (lblPickQTY != null)
                {
                    //统计数量
                    ViewState["TotalPickQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblPickQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                    //lblOrdersCount.Text = lblTotalOrderQTY.Text;
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()).ToString();
                }

            }
        }


        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CardGradeCode"] = null;
                ViewState["CardGrade"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                //Removed By Robin 2014-11-19
                //System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CardDelivery_D().GetList(string.Format("CardDeliveryNumber = '{0}'", drv["CardDeliveryNumber"].ToString()) + " order by CardGradeID");

                //Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                //Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");

                //list.DataSource = ds.Tables[0];
                //list.DataBind();
                //End
            }
        }
        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}