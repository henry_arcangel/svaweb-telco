﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Messages.Manager;
using System.Data;
using System.Text;
using Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardDelivery;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardDelivery
{
    public partial class List : PageBase
    {
        private const string fields = "CardDeliveryNumber,ReferenceNo,ApproveStatus,ApprovalCode,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy,NeedActive";
        CardDeliveryController controller = new CardDeliveryController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnPrint.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                RptBind("", "CardDeliveryNumber");

                ControlTool.BindBrand(this.Brand);
                ControlTool.BindStore(FromStoreID);
                InitStoreByBrand();
            }
        }

        //protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string CardNumber = (e.Item.FindControl("lb_id") as Label).Text;
        //        string approveStatus = ((Label)e.Item.FindControl("lblApproveStatus")).Text;
        //        DateTime? createTime = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<DateTime>(((Label)e.Item.FindControl("lblCreatedOn")).Text);
        //        string strCreateDate = Edge.Utils.Tools.StringHelper.GetDateString(createTime);

        //        switch (approveStatus.Substring(0, 1).ToUpper().Trim())
        //        {
        //            case "A":
        //                if (webset.CardShipmentConfirmationSwitch == 1) (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                else (e.Item.FindControl("lbkEdit") as LinkButton).Visible = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=A", CardNumber, strCreateDate);
        //                break;
        //            case "P":
        //                if (webset.CardShipmentConfirmationSwitch == 1) (e.Item.FindControl("lbkEdit") as LinkButton).Attributes["href"] = string.Format("Modify.aspx?id={0}&CreatedOn={1}&Status=P", CardNumber, strCreateDate);
        //                else (e.Item.FindControl("lbkEdit") as LinkButton).Visible = false;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=P", CardNumber, strCreateDate);
        //                break;
        //            case "V":
        //                if (webset.CardShipmentConfirmationSwitch == 1) (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                else (e.Item.FindControl("lbkEdit") as LinkButton).Visible = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=V", CardNumber, strCreateDate);
        //                break;

        //        }
        //    }
        //}

        //protected void rptListPager_PageChanged(object sender, EventArgs e)
        //{
        //    RptBind("", "CardDeliveryNumber", fields);
        //}

        //#region  Approve Void

        //protected void lbtnApprove_Click(object sender, EventArgs e)
        //{
        //    string ids = "";
        //    for (int i = 0; i < this.rptList.Items.Count; i++)
        //    {
        //        System.Web.UI.Control item = rptList.Items[i];
        //        CheckBox cb = item.FindControl("cb_id") as CheckBox;

        //        if (cb != null && cb.Checked == true)
        //        {
        //            string CardNumber = (item.FindControl("lb_id") as Label).Text;
        //            ids += string.Format("{0};", CardNumber);
        //        }
        //    }
        //    Response.Redirect("Approve.aspx?ids=" + ids);
        //}

        //protected void lbtnVoid_Click(object sender, EventArgs e)
        //{
        //    string ids = "";
        //    for (int i = 0; i < this.rptList.Items.Count; i++)
        //    {
        //        System.Web.UI.Control item = rptList.Items[i];
        //        CheckBox cb = item.FindControl("cb_id") as CheckBox;

        //        if (cb != null && cb.Checked == true)
        //        {
        //            string CardNumber = (item.FindControl("lb_id") as Label).Text;
        //            ids += string.Format("{0};", CardNumber);
        //        }
        //    }
        //    Response.Redirect("Void.aspx?ids=" + ids);
        //}

        //#endregion

        //#region 数据列表绑定

        //private void RptBind(string strWhere, string orderby, string fields)
        //{
        //    Edge.SVA.BLL.Ord_CardDelivery_H bll = new Edge.SVA.BLL.Ord_CardDelivery_H()
        //    {
        //        StrWhere = strWhere,
        //        Order = orderby,
        //        Fields = fields,
        //        Ascending = false
        //    };

        //    int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;
        //    System.Data.DataSet ds = null;
        //    if (this.RecordCount <= 0)
        //    {
        //        int count = 0;
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
        //        this.RecordCount = count;

        //    }
        //    else
        //    {
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage);
        //    }

        //    Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
        //    Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
        //    Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
        //    Tools.DataTool.AddIsActivated(ds, "NeedActiveView", "NeedActive");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}

        //#endregion

        //private int RecordCount
        //{
        //    get
        //    {
        //        if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
        //        int count = 0;
        //        return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
        //    }
        //    set
        //    {
        //        if (value < 0) value = 0;
        //        if (value > 0)
        //        {
        //            this.lbtnApprove.Attributes.Remove("disabled");
        //            this.lbtnVoid.Attributes.Remove("disabled");
        //        }
        //        else
        //        {
        //            this.lbtnApprove.Attributes["disabled"] = "disabled";
        //            this.lbtnVoid.Attributes["disabled"] = "disabled";
        //        }
        //        this.rptPager.RecordCount = value;
        //        ViewState["RecordCount"] = value;
        //    }
        //}

        //protected void lbtnPrint_Click(object sender, EventArgs e)
        //{
        //    string ids = "";
        //    for (int i = 0; i < this.rptList.Items.Count; i++)
        //    {
        //        System.Web.UI.Control item = rptList.Items[i];
        //        CheckBox cb = item.FindControl("cb_id") as CheckBox;

        //        if (cb != null && cb.Checked == true)
        //        {
        //            string CardNumber = (item.FindControl("lb_id") as Label).Text;
        //            ids += string.Format("{0};", CardNumber);
        //        }
        //    }
        //    Response.Redirect("Print.aspx?ids=" + ids);
        //}

        #region  Approve Void

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            //////FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));

            ////Window2.Title = "Approve";
            ////FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')));
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnApproveActive_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window2.Title = Resources.MessageTips.Approve;
            string okScript = Window2.GetShowReference("Approve.aspx?NeedActive=1&ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////FineUI.PageContext.Redirect("Void.aspx?ids=" + sb.ToString().TrimEnd(','));
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), ""); 
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));

            Window1.Title = "Print";
            FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("Print.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        #endregion

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            try
            {            
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    int brandid = Tools.ConvertTool.ToInt(this.Brand.SelectedValue);
                    int storeid = Tools.ConvertTool.ToInt(this.Store.SelectedValue);
                    string code = this.Code.Text.Trim();
                    string status = this.Status.SelectedValue.Trim();
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string AStatrtDate = this.ApproveStartDate.Text;
                    string AEndDate = this.ApproveEndDate.Text;
                    int fromStoreID = Tools.ConvertTool.ToInt(this.FromStoreID.SelectedValue);
                    if (brandid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" StoreID in (select StoreID from Store where BrandID = ");
                        sb.Append(brandid);
                        sb.Append(")");
                    }
                    if (storeid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" StoreID =");
                        sb.Append(storeid);

                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardDeliveryNumber like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveStatus";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(AStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(AStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(AEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(AEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (fromStoreID > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" FromStoreID =");
                        sb.Append(fromStoreID);

                    }
                    strWhere = sb.ToString();
                }
            #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.Ord_CardDelivery_H bll = new Edge.SVA.BLL.Ord_CardDelivery_H()
                //{
                //    StrWhere = strWhere,
                //    Order = orderby,
                //    Fields = fields,
                //    Ascending = false
                //};

                //System.Data.DataSet ds = null;
                //int count = 0;
                //ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                //this.RecordCount = count;

                //Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
                //Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
                //Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
                //Tools.DataTool.AddIsActivated(ds, "NeedActiveView", "NeedActive");

                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();

                CardDeliveryController con = new CardDeliveryController();
                int count = 0;
                DataSet ds = con.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                ds.Tables[0].Columns.Add(new DataColumn("FullStoreID", typeof(string)));
                ds.Tables[0].Columns.Add(new DataColumn("FullFromStoreID", typeof(string)));
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int storeID = dr["StoreID"] == null ? 0 : Convert.ToInt32(dr["StoreID"]);
                    int fromStoreID = dr["FromStoreID"] == null ? 0 : Convert.ToInt32(dr["FromStoreID"]);
                    Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(storeID);
                    dr["FullStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                    store = new Edge.SVA.BLL.Store().GetModel(fromStoreID);
                    dr["FullFromStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                }
                this.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("CardDelivery", "Load Faild", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CardDeliveryController c = new CardDeliveryController();
            int count = 0;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);
            ds.Tables[0].Columns.Add(new DataColumn("FullStoreID", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("FullFromStoreID", typeof(string)));
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                int storeID = dr["StoreID"] == null ? 0 : Convert.ToInt32(dr["StoreID"]);
                int fromStoreID = dr["FromStoreID"] == null ? 0 : Convert.ToInt32(dr["FromStoreID"]);
                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(storeID);
                dr["FullStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                store = new Edge.SVA.BLL.Store().GetModel(fromStoreID);
                dr["FullFromStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
            }
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "CardDeliveryNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "CardDeliveryNumber");
        }
        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A"://Approve
                            break;
                        case "P"://Pending
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                        case "V"://Voided
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                    }
                }
            }
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                FineUI.WindowField ViewWF = Grid1.FindColumn("ViewWindowField") as FineUI.WindowField;
                FineUI.WindowField EditWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A"://Approve
                            ViewWF.Enabled = true;
                            if (webset.CardShipmentConfirmationSwitch == 1) EditWF.Enabled = false;
                            else EditWF.Hidden = true;
                            break;
                        case "P"://Pending
                            ViewWF.Enabled = true;
                            if (webset.CardShipmentConfirmationSwitch == 1) EditWF.Hidden = false;
                            else EditWF.Hidden = true;
                            break;
                        case "V"://Voided
                            ViewWF.Enabled = true;
                            if (webset.CardShipmentConfirmationSwitch == 1) EditWF.Enabled = false;
                            else EditWF.Hidden = true;
                            break;
                    }
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "CardDeliveryNumber");
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
        }
        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(this.Store, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }
    }
}