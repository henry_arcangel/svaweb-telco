﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardPicking.Print" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <%-- <script language="javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
        function preview() {
            bdhtml = window.document.body.innerHTML;
            sprnstr = "<!--startprint-->";
            eprnstr = "<!--endprint-->";
            prnhtml = bdhtml.substr(bdhtml.indexOf(sprnstr) + 17);
            prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));
            window.document.body.innerHTML = prnhtml;
            window.print();
        }
    </script>--%>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtable">
                    <tr>
                        <th colspan="4" align="center">
                            捡货列表
                        </th>
                    </tr>
                    <tr>
                        <td align="right" width="25%">
                            捡货单编号：
                        </td>
                        <td width="25%">
                            <%#Eval("CardPickingNumber")%>
                        </td>
                        <td align="right">
                            打印时间：
                        </td>
                        <td>
                            <%#Eval("PrintDateTime")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            参考编号：
                        </td>
                        <td>
                            <%#Eval("ReferenceNo")%>
                        </td>
                        <td align="right" width="25%">
                            状态：
                        </td>
                        <td width="25%">
                            <%#Eval("ApproveStatus")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            捡货日期：
                        </td>
                        <td>
                            <%#Eval("PickingDate")%>
                        </td>
                        <td align="right">
                            捡货人：
                        </td>
                        <td>
                            <%#Eval("PickedBy")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtablelist">
                                        <tr>
                                            <th width="6%">
                                                序号
                                            </th>
                                            <th width="10%">
                                                优惠劵类型编号
                                            </th>
                                            <th width="10%">
                                                优惠劵类型
                                            </th>
                                            <th width="10%">
                                                订单数量
                                            </th>
                                            <th width="10%">
                                                捡货数量
                                            </th>
                                            <th width="10%">
                                                卡起始编号
                                            </th>
                                            <th width="10%">
                                                卡结束编号
                                            </th>
                                            <th width="10%">
                                                卡批次编号
                                            </th>
                                            <th width="10%">
                                                捡货时间
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblSeq" runat="server" Text=''></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCardGrade" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblFirstCardNumber" runat="server" Text='<%#Eval("FirstCardNumber")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblEndCardNumber" runat="server" Text='<%#Eval("EndCardNumber")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCardBatchID" runat="server" Text='<%#Eval("BatchCardCode")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblPickupDateTime" runat="server" Text='<%#Eval("PickupDateTime","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="center" colspan="3">
                                            总计：
                                        </td>
                                        <td align="center" colspan="2">
                                           订单数量总计： <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" colspan="2">
                                           捡货数量总计： <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <%-- <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                                runat="server" id="dtTotal">
                                <tr>
                                    <td align="center">
                                        汇总：
                                    </td>
                                    <td align="center">
                                        订单汇总：
                                        <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        捡货汇总：
                                        <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <div align="center" style="width: 100%; text-align: center;">
        <%--<asp:Button ID="btnPrint" runat="server" Text="打印" OnClientClick="printdiv('div_print')" />--%>
        <%--        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
            class="submit" />--%>
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
