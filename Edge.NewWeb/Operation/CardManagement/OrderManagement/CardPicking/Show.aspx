﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardPicking.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:Button ID="btnPrint" runat="server" Text="打印" Icon="Printer" OnClick="btnPrint_Click">
                    </ext:Button>
                    <ext:Button ID="btnPrintAR" runat="server" Text="AR" Icon="Printer" OnClick="btnPrintAR_Click" Hidden="true">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="订单信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Label="交易状态：" Text="P">
                    </ext:HiddenField>
                    <ext:HiddenField ID="OrderType" runat="server" Label="订单类型：" Text="0">
                    </ext:HiddenField>
                    <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardPickingNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="lblOrderType" runat="server" Label="订单类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Remark" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="库存(出)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="FromStoreID" runat="server" Label="总部：">
                                    </ext:Label>
                                    <ext:Label ID="FromAddress" runat="server" Label="地址：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="FromContactName" runat="server" Label="联系人：">
                                    </ext:Label>
                                    <ext:Label ID="FromContactNumber" runat="server" Label="联系电话：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="库存(入)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="CustomerType" runat="server">
                                        <ext:RadioItem Text="客户订货" Value="1" />
                                        <ext:RadioItem Text="店铺订货" Value="2" Selected="true" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="SendMethod" runat="server" Label="Label">
                                        <ext:ListItem Text="直接交付（打印）" Value="1" Selected="true" />
                                        <ext:ListItem Text="SMS" Value="2" />
                                        <ext:ListItem Text="Email" Value="3" />
                                        <ext:ListItem Text="Social Network" Value="4" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CustomerTypeView" runat="server" Label="订单类型：">
                                    </ext:Label>
                                    <ext:Label ID="ddlBrand" runat="server" Label="订货品牌：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StoreID" runat="server" Label="店铺：">
                                    </ext:Label>
                                    <ext:Label ID="CustomerID" runat="server" Label="订货客户：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="SendMethodView" runat="server" Label="送货单发送方式：">
                                    </ext:Label>
                                    <ext:Label ID="SendAddress" runat="server" Label="送货地址：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StoreContactName" runat="server" Label="联系人：">
                                    </ext:Label>
                                    <ext:Label ID="StoreContactEmail" runat="server" Label="邮件发送：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <%--<ext:Label ID="SMSMMS" runat="server" Label="Translate__Special_121_StartSMS/MMS发送：Translate__Special_121_End">
                                    </ext:Label>--%>
                                    <ext:Label ID="StoreContactPhone" runat="server" Label="联系电话：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Remark1" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="捡货汇总列表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="ID" AllowPaging="false"
                        EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true" 
                        OnRowDataBound="Grid1_RowDataBound">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill1" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="Label4" runat="server" Text="订单汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblGrid1TotalOrderQTY" runat="server">
                                    </ext:Label>
                                    <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                    </ext:ToolbarSeparator>
                                    <ext:Label ID="Label6" runat="server" Text="捡货汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblGrid1TotalPickQTY" runat="server">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="序号">
                                <ItemTemplate>
                                    <asp:Label ID="lblID1" runat="server" Text='<%#Eval("ID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGradeCode1" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGrade1" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订单数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderQTY1" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="捡货数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickQTY1" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="实际捡货结果"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="ID" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid2_PageIndexChange" 
                        OnRowDataBound="Grid2_RowDataBound">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill2" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="Label2" runat="server" Text="订单汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalOrderQTY" runat="server">
                                    </ext:Label>
                                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                    </ext:ToolbarSeparator>
                                    <ext:Label ID="Label3" runat="server" Text="捡货汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalPickQTY" runat="server">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="序号">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeq" runat="server" Text=''></asp:Label>
                                    <asp:HiddenField ID="hfCardGradeID" runat="server" Value='<%#Eval("CardGradeID") %>' />
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGrade" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <%--ext:TemplateField Width="60px" HeaderText="订单数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField--%>
                            <%--ext:TemplateField Width="60px" HeaderText="捡货数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField--%>
                            <ext:TemplateField Width="60px" HeaderText="卡编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstCardNumber" runat="server" Text='<%#Eval("FirstCardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡物理编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstCardUID" runat="server" Text='<%#Eval("FirstCardUID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡结束编号" Hidden="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndCardNumber" runat="server" Text='<%#Eval("EndCardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡结束物理编号" Hidden="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndCardUID" runat="server" Text='<%#Eval("EndCardUID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡批次编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardBatchID" runat="server" Text='<%#Eval("BatchCardCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblStockStatusName" runat="server" Text='<%#Eval("StockStatusName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="捡货时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickupDateTime" runat="server" Text='<%#Eval("PickupDateTime","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <%--    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="5" align="left">
                交易信息
            </th>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" width="15%">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CardPickingNumber" runat="server"></asp:Label>
            </td>
            <td align="right" width="15%">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server" Text="P"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易批核工作日期：
            </td>
            <td>
                <asp:Label ID="ApproveBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                批核时间：
            </td>
            <td>
                <asp:Label ID="ApproveOn" runat="server"></asp:Label>
            </td>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="lblApproveBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                授权号：
            </td>
            <td>
                <asp:Label ID="ApprovalCode" runat="server"></asp:Label>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="Remark" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="5" align="left">
                捡货汇总列表
            </th>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Repeater ID="rptTotalList" runat="server" OnItemDataBound="rptTotalList_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="10%">
                                    卡级别编号
                                </th>
                                <th width="10%">
                                    卡级别
                                </th>
                                <th width="10%">
                                    订单数量
                                </th>
                                <th width="6%">
                                    &nbsp;
                                </th>
                                <th width="10%">
                                    捡货数量
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblSeq" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGrade" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td style="text-align: right;">
                                订单汇总：
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                捡货汇总：
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                            </td>
                        </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <th colspan="5" align="left">
                实际捡货结果
            </th>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="10%">
                                    卡级别编号
                                </th>
                                <th width="10%">
                                    卡级别
                                </th>
                                <th width="10%">
                                    订单数量
                                </th>
                                <th width="10%">
                                    捡货数量
                                </th>
                                <th width="10%">
                                    卡起始编号
                                </th>
                                <th width="10%">
                                    卡结束编号
                                </th>
                                <th width="10%">
                                    卡批次编号
                                </th>
                                <th width="10%">
                                    捡货时间
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                             <td align="center">
                                <asp:Label ID="lblSeq" runat="server" Text=''></asp:Label>
                                <asp:HiddenField ID="hfCardGradeID" runat="server" Value='<%#Eval("CardGradeID") %>' />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGrade" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblFirstCardNumber" runat="server" Text='<%#Eval("FirstCardNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblEndCardNumber" runat="server" Text='<%#Eval("EndCardNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardBatchID" runat="server" Text='<%#Eval("BatchCardCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickupDateTime" runat="server" Text='<%#Eval("PickupDateTime","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                    runat="server" id="dtTotal">
                    <tr>
                        <td align="center">
                            汇总：
                        </td>
                        <td align="center">
                            订单汇总：
                            <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            捡货汇总：
                            <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="clear" />
                <div align="center" style="width: 100%; text-align: center;">
                    <asp:Button ID="btnPrint" runat="server" Text="打印" CssClass="submit" OnClick="btnPrint_Click" />
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <%--    <script type="text/javascript">

        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>--%>
</body>
</html>
