﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardPicking.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        function checkSelect(msg, url) {
            var link = url + "?ids=";
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            link += ids;
            link += "&height=460&amp;width=800&amp;TB_iframe=True&amp";

            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.:" + ids)) return false;

            window.top.tb_show("", link, "");
            $("#TB_title", parent.document).hide();

        }

        function hasSelect(msg) {
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.: " + ids)) return false;
            return true;
        }

        function checkPrintSelect(msg, url) {
            var link = url + "?id=";
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()));
                }
            });
            link += ids;
            link += "&height=460&amp;width=800&amp;TB_iframe=True&amp";

            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.:" + ids)) return false;
            window.top.tb_show("", link, "");
            // window.location.href = link;
        }
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelSeparator="0" LabelAlign="Right" LabelWidth="110">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="23% 23% 23% 23% 8%">
                        <Items>
                            <ext:DropDownList ID="Brand" runat="server" Label="品牌：" 
                                OnSelectedIndexChanged="Brand_SelectedIndexChanged" AutoPostBack="true" Resizable="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="Store" runat="server" Label="店铺：" Resizable="true"></ext:DropDownList>
                            <ext:TextBox ID="Code" runat="server" Label="交易编号：" MaxLength="512">
                            </ext:TextBox>
                            <ext:DropDownList ID="Status" runat="server" Label="交易状态：" Resizable="true">
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="PENDING" Value="R" />
                                <ext:ListItem Text="PICKED" Value="P" />
                                <ext:ListItem Text="APPROVED" Value="A" />
                                <ext:ListItem Text="VOID" Value="V" />
                            </ext:DropDownList>
                            <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click" ValidateForms="SearchForm">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="23% 23% 23% 23% 8%">
                        <Items>
                            <ext:DatePicker ID="CreateStartDate" runat="server" Label="创建日期从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="CreateEndDate" runat="server" Label="创建日期到：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="ApproveStartDate" runat="server" Label="批核日期从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="ApproveEndDate" runat="server" Label="批核日期到：" DateFormatString="yyyy-MM-dd"> 
                            </ext:DatePicker>
                            <ext:Label ID="Label2" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow runat="server" ColumnWidths="23% 23% 23% 23% 8%">
                        <Items>
                            <ext:TextBox ID="RefCode" runat="server" Label="参考编号：" MaxLength="512">
                            </ext:TextBox>
                           <ext:DropDownList ID="OrderType" runat="server" Label="订单类型：" Resizable="true">
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="手动" Value="0" />
                                <ext:ListItem Text="自动" Value="1" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="FromStoreID" runat="server" Label="库存(出)地点：" Resizable="true">
                            </ext:DropDownList>
                            <ext:Label ID="Label5" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                            <ext:Label ID="Label6" runat="server" Label="" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="CardPickingNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" OnRowDataBound="Grid1_RowDataBound"
                        OnPreRowDataBound="Grid1_PreRowDataBound" OnSort="Grid1_Sort" AllowSorting="true">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnApprove" Text="批核" Icon="Accept" runat="server" OnClick="btnApprove_Click">
                                    </ext:Button>
                                    <ext:Button ID="btnVoid" Text="作废" Icon="Cross" runat="server" OnClick="btnVoid_Click">
                                    </ext:Button>
                                    <ext:Button ID="btnPrint" Text="打印捡货单" Icon="Printer" runat="server" OnClick="btnPrint_Click">
                                    </ext:Button>
                                    <ext:Button ID="btnPrintAR" Text="AR" Icon="Printer" runat="server" OnClick="btnPrintAR_Click">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="125px" HeaderText="交易编号" SortField="CardPickingNumber">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%# Eval("CardPickingNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="120px" HeaderText="参考编号" SortField="ReferenceNo">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ReferenceNo") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订单类型" SortField="OrderTypeName">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderType" runat="server" Text='<%# Eval("OrderTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="交易状态" SortField="ApproveStatusName">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%# Eval("ApproveStatusName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
<%--                            <ext:TemplateField Width="60px" HeaderText="库存(出)地点">
                                <ItemTemplate>
                                    <asp:Label ID="lblFormStoreID" runat="server" Text='<%# Eval("FullFromStoreID") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存(入)地点">
                                <ItemTemplate>
                                    <asp:Label ID="lblStoreID" runat="server" Text='<%# Eval("FullStoreID") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
<%--                            <ext:TemplateField Width="60px" HeaderText="交易创建工作日期" SortField="CreatedBusDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CreatedBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="交易更新日期" SortField="UpdatedOn">
                                <ItemTemplate>
                                    <asp:Label ID="lblUpdatedOn" runat="server" Text='<%#Eval("UpdatedOn","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="批核人" SortField="ApproveByName">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBy" runat="server" Text='<%# Eval("ApproveByName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="95px" HeaderText="开始卡" SortField="FirstCardNumber">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("FirstCardNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="95px" HeaderText="结束卡" SortField="EndCardNumber">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("EndCardNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
<%--                            <ext:TemplateField Width="60px" HeaderText="交易批核工作日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("ApproveBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="交易创建时间" SortField="CreatedOn">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="创建人" SortField="CreatedByName">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateBy" runat="server" Text='<%# Eval("CreatedByName") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
<%--                            <ext:TemplateField Width="60px" HeaderText="批核时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveOn" runat="server" Text='<%#Eval("ApproveOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="授权号" SortField="ApprovalCode">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%# Eval("ApprovalCode") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="CardPickingNumber"
                                DataIFrameUrlFormatString="Show.aspx?id={0}" Title="查看" />
                            <ext:WindowField ColumnID="PickedWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                                Text="捡货回应" ToolTip="捡货回应" DataTextFormatString="{0}" DataIFrameUrlFields="CardPickingNumber"
                                DataIFrameUrlFormatString="Modify.aspx?id={0}" Title="捡货回应" />
                            <ext:WindowField ColumnID="RePickedWindowField" Width="60px" WindowID="Window2" Icon="PagePaintbrush"
                                Text="重新捡货" ToolTip="重新捡货" DataTextFormatString="{0}" DataIFrameUrlFields="CardPickingNumber"
                                DataIFrameUrlFormatString="Modify.aspx?id={0}" Title="重新捡货" />
                        </Columns>
                    </ext:Grid>
                    </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="900px"
        Height="580px">
    </ext:Window>
    <ext:Window ID="Window2" runat="server" Popup="false" IsModal="True" Title="" EnableMaximize="true"
        EnableResize="true" Target="Top" EnableIFrame="true" IFrameUrl="about:blank"
        Width="900px" CloseAction="HidePostBack" OnClose="WindowEdit_Close" EnableClose="true" Height="550px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField>
    <%--    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
        <HeaderTemplate>
            <table id="msgtablelist" width="100%" border="0" cellspacing="0" cellpadding="0"
                class="msgtablelist">
                <tr>
                    <th width="6%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                    <th width="10%">
                        交易编号
                    </th>
                    <th width="10%">
                        参考编号
                    </th>
                    <th width="5%">
                        交易状态
                    </th>
                    <th width="6%">
                        授权号
                    </th>
                    <th width="8%">
                        交易创建工作日期
                    </th>
                    <th width="8%">
                        交易批核工作日期
                    </th>
                    <th width="8%">
                        交易创建时间
                    </th>
                    <th width="6%">
                        创建人
                    </th>
                    <th width="8%">
                        批核时间
                    </th>
                    <th width="6%">
                        批核人
                    </th>
                    <th width="20%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall"  runat="server" />
                </td>
                <td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("CardPickingNumber")%>'></asp:Label></a>
                </td>
                <td align="center">
                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Eval("ReferenceNo")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("ApproveStatusName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("ApprovalCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CreatedBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("ApproveBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedByName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveOn" runat="server" Text='<%#Eval("ApproveOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBy" runat="server" Text='<%#Eval("ApproveByName")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg">
                        <asp:LinkButton ID="lkbView" runat="server">查看</asp:LinkButton>
                        <asp:LinkButton ID="lbkEdit" runat="server">捡货回应</asp:LinkButton>
                        <asp:LinkButton ID="lbkReEdit" runat="server">重新捡货</asp:LinkButton>
                    </span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear" style="padding-top: 12px;" />
    <table width="100%">
        <tr>
            <td>
                <div class="left">
                    <span class="btn_bg"><a id="lbtnApprove" runat="server" href="#" onclick="return checkSelect();">
                        批核</a>
                        <asp:LinkButton ID="lbtnVoid" runat="server" OnClick="lbtnVoid_Click">作废</asp:LinkButton>
                        <asp:LinkButton ID="lbtnPrint" runat="server" OnClick="lbtnPrint_Click">打印捡货单</asp:LinkButton>
                        <%--<a id="lbtnPrint" runat="server" href="#" onclick="return checkPrintSelect();">打印捡货单</a>
                    </span>
                </div>
            </td>
            <td class="right">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
