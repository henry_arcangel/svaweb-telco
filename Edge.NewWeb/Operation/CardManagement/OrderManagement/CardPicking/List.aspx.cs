﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Messages.Manager;
using System.Text;
using System.Data;
using Edge.Web.Controllers.Operation.CardManagement.OrderManagement.CardPicking;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPicking
{
    public partial class List : PageBase
    {
        private const string fields = "CardPickingNumber,ReferenceNo,ApproveStatus,OrderType,ApprovalCode,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy,UpdatedOn,UpdatedBy";
        CardPickingController controller = new CardPickingController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
                //this.lbtnApprove.Attributes["onclick"] = string.Format("return checkSelect( '{0}','{1}');", MessagesTool.instance.GetMessage("10017"), url + "Approve.aspx");
                ////this.lbtnPrint.Attributes["onclick"] = string.Format("return checkPrintSelect( '{0}','{1}');", "Are you sure?", url + "Print.aspx");
                //this.lbtnVoid.OnClientClick = "return hasSelect( '" + Resources.MessageTips.ConfirmVoid + " ');";

                this.Grid1.PageSize = webset.ContentPageNum;

                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnPrint.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnPrintAR.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

                RptBind("", "CardPickingNumber");

                ControlTool.BindBrand(this.Brand);
                ControlTool.BindStore(FromStoreID);
                InitStoreByBrand();
            }
        }

        //protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string CardNumber = (e.Item.FindControl("lb_id") as Label).Text;
        //        string approveStatus = DataBinder.Eval(e.Item.DataItem, "ApproveStatus").ToString();
        //        DateTime? createTime = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<DateTime>(((Label)e.Item.FindControl("lblCreatedOn")).Text);
        //        string strCreateDate = Edge.Utils.Tools.StringHelper.GetDateString(createTime);

        //        switch (approveStatus.ToUpper().Trim())
        //        {
        //            case "A"://Approve
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("lbkReEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=A", CardNumber, strCreateDate);
        //                break;
        //            case "R"://Pending
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = true;
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Attributes["href"] = string.Format("Modify.aspx?id={0}&CreatedOn={1}&Status=R", CardNumber, strCreateDate);
        //                (e.Item.FindControl("lbkReEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=R", CardNumber, strCreateDate);
        //                break;
        //            case "P"://Picked
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("lbkReEdit") as LinkButton).Enabled = true;
        //                (e.Item.FindControl("lbkReEdit") as LinkButton).Attributes["href"] = string.Format("Modify.aspx?id={0}&CreatedOn={1}&Status=P", CardNumber, strCreateDate);
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=P", CardNumber, strCreateDate);
        //                break;
        //            case "V"://Voided
        //                 (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("lbkReEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=V", CardNumber, strCreateDate);
        //                break;
        //        }
        //    }
        //}


        #region  Approve Void

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            //////FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));

            ////Window2.Title = "Approve";
            ////FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')));
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////FineUI.PageContext.Redirect("Void.aspx?ids=" + sb.ToString().TrimEnd(','));

            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));

            Window1.Title = "Print";
            FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("Print.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void btnPrintAR_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            if (Grid1.SelectedRowIndexArray.Length > 1) 
            {
                ShowWarning("Too many records!");
                return;
            }
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
            }
            //FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));

            Window1.Title = "AR";
            FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("PrintAR.aspx?id=" + sb.ToString()));
        }

        #endregion

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            #region for search
            if (SearchFlag.Text == "1")
            {
                StringBuilder sb = new StringBuilder(strWhere);

                int brandid = Tools.ConvertTool.ToInt(this.Brand.SelectedValue);
                int storeid = Tools.ConvertTool.ToInt(this.Store.SelectedValue);
                string code = this.Code.Text.Trim();
                string status = this.Status.SelectedValue.Trim();
                string ordertype = this.OrderType.SelectedValue;
                string refcode = this.RefCode.Text;
                string CStatrtDate = this.CreateStartDate.Text;
                string CEndDate = this.CreateEndDate.Text;
                string AStatrtDate = this.ApproveStartDate.Text;
                string AEndDate = this.ApproveEndDate.Text;
                int fromStoreID = Tools.ConvertTool.ToInt(this.FromStoreID.SelectedValue);
                if (brandid > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" StoreID in (select StoreID from Store where BrandID = ");
                    sb.Append(brandid);
                    sb.Append(")");
                }
                if (storeid > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" StoreID =");
                    sb.Append(storeid);

                }
                if (!string.IsNullOrEmpty(code))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" CardPickingNumber like '%");
                    sb.Append(code);
                    sb.Append("%'");
                }
                if (!string.IsNullOrEmpty(refcode))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" ReferenceNo like '%");
                    sb.Append(refcode);
                    sb.Append("%'");
                }
                if (!string.IsNullOrEmpty(status))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("ApproveStatus = '");
                    sb.Append(status);
                    sb.Append("'");
                }
                if (!string.IsNullOrEmpty(ordertype))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" OrderType =");
                    sb.Append(ordertype);

                }
                if (!string.IsNullOrEmpty(CStatrtDate))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    string descLan = "CreatedOn";
                    sb.Append(descLan);
                    sb.Append(" >= Cast('");
                    sb.Append(CStatrtDate);
                    sb.Append("' as DateTime)");
                }
                if (!string.IsNullOrEmpty(CEndDate))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    string descLan = "CreatedOn";
                    sb.Append(descLan);
                    sb.Append(" < Cast('");
                    sb.Append(CEndDate);
                    sb.Append("' as DateTime) + 1");
                }
                if (!string.IsNullOrEmpty(AStatrtDate))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    string descLan = "ApproveOn";
                    sb.Append(descLan);
                    sb.Append(" >= Cast('");
                    sb.Append(AStatrtDate);
                    sb.Append("' as DateTime)");
                }
                if (!string.IsNullOrEmpty(AEndDate))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    string descLan = "ApproveOn";
                    sb.Append(descLan);
                    sb.Append(" < Cast('");
                    sb.Append(AEndDate);
                    sb.Append("' as DateTime) + 1");
                }
                if (fromStoreID > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" FromStoreID =");
                    sb.Append(fromStoreID);

                }
                strWhere = sb.ToString();
            }
            #endregion
            //记录查询条件用于排序
            ViewState["strWhere"] = strWhere;


            //Edge.SVA.BLL.Ord_CardPicking_H bll = new Edge.SVA.BLL.Ord_CardPicking_H()
            //{
            //    StrWhere = strWhere,
            //    Order = orderby,
            //    Fields = fields,
            //    Ascending = false
            //};

            //System.Data.DataSet ds = null;
            //int count = 0;
            //ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
            //this.RecordCount = count;

            //Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            //Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            //Tools.DataTool.AddOrderPickingApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            //this.Grid1.DataSource = ds.Tables[0].DefaultView;
            //this.Grid1.DataBind();
            CardPickingController con = new CardPickingController();
            int count = 0;
            DataSet ds = con.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);
            ds.Tables[0].Columns.Add(new DataColumn("FullStoreID", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("FullFromStoreID", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("OrderTypeName", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("FirstCardNumber", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("EndCardNumber", typeof(string)));
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                int storeID = dr["StoreID"] == null ? 0 : Convert.ToInt32(dr["StoreID"]);
                int fromStoreID = dr["FromStoreID"] == null ? 0 : Convert.ToInt32(dr["FromStoreID"]);
                int orderType = dr["OrderType"] == null ? 0 : Convert.ToInt32(dr["OrderType"]);
                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(storeID);
                dr["FullStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                store = new Edge.SVA.BLL.Store().GetModel(fromStoreID);
                dr["FullFromStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                if (orderType == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": dr["OrderTypeName"] = "Manually"; break;
                        case "zh-cn": dr["OrderTypeName"] = "手动"; break;
                        case "zh-hk": dr["OrderTypeName"] = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": dr["OrderTypeName"] = "Auto"; break;
                        case "zh-cn": dr["OrderTypeName"] = "自动"; break;
                        case "zh-hk": dr["OrderTypeName"] = "自動"; break;
                    }
                }
                dr["FirstCardNumber"] = controller.GetFirstCardNumber(dr["CardPickingNumber"].ToString());
                dr["EndCardNumber"] = controller.GetEndCardNumber(dr["CardPickingNumber"].ToString());
            }
            this.RecordCount = count;
            if (ds != null)
            {
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.Reset();
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CardPickingController c = new CardPickingController();
            int count = 0;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);
            ds.Tables[0].Columns.Add(new DataColumn("FullStoreID", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("FullFromStoreID", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("OrderTypeName", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("FirstCardNumber", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("EndCardNumber", typeof(string)));
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                int storeID = dr["StoreID"] == null ? 0 : Convert.ToInt32(dr["StoreID"]);
                int fromStoreID = dr["FromStoreID"] == null ? 0 : Convert.ToInt32(dr["FromStoreID"]);
                int orderType = dr["OrderType"] == null ? 0 : Convert.ToInt32(dr["OrderType"]);
                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(storeID);
                dr["FullStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                store = new Edge.SVA.BLL.Store().GetModel(fromStoreID);
                dr["FullFromStoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                if (orderType == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": dr["OrderTypeName"] = "Manually"; break;
                        case "zh-cn": dr["OrderTypeName"] = "手动"; break;
                        case "zh-hk": dr["OrderTypeName"] = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": dr["OrderTypeName"] = "Auto"; break;
                        case "zh-cn": dr["OrderTypeName"] = "自动"; break;
                        case "zh-hk": dr["OrderTypeName"] = "自動"; break;
                    }
                }
                dr["FirstCardNumber"] = controller.GetFirstCardNumber(dr["CardPickingNumber"].ToString());
                dr["EndCardNumber"] = controller.GetEndCardNumber(dr["CardPickingNumber"].ToString());
            }
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "CardPickingNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "CardPickingNumber");
        }
        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A"://Approve
                            break;
                        case "R"://Pending
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                        case "P"://Picked
                              (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                        case "V"://Voided
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                    }
                }
            }
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                FineUI.WindowField PickedWF = Grid1.FindColumn("PickedWindowField") as FineUI.WindowField;
                FineUI.WindowField RePickedWF = Grid1.FindColumn("RePickedWindowField") as FineUI.WindowField;

                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A"://Approve
                            PickedWF.Enabled = false;
                            RePickedWF.Enabled = false;
                            break;
                        case "R"://Pending
                            PickedWF.Enabled = true;
                            RePickedWF.Enabled = false;
                            PickedWF.DataIFrameUrlFormatString = "Modify.aspx?id={0}&Status=R";
                            break;
                        case "P"://Picked
                            PickedWF.Enabled = false;
                            RePickedWF.Enabled = true;
                            RePickedWF.DataIFrameUrlFormatString = "Modify.aspx?id={0}&Status=P";
                            break;
                        case "V"://Voided
                            PickedWF.Enabled = false;
                            RePickedWF.Enabled = false;
                            break;
                    }
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "CardPickingNumber");
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
        }
        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(this.Store, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }
    }
}