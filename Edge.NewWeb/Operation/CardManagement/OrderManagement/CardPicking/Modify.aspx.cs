﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Messages.Manager;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPicking
{
    public partial class Modify : Tools.BasePage<SVA.BLL.Ord_CardPicking_H, SVA.Model.Ord_CardPicking_H>
    {
        private const string fields = "[KeyID],[CardPickingNumber],[CardTypeID],[CardGradeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCardNumber],[EndCardNumber],[BatchCardCode],[PickupDateTime]";


        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["flg"] = 0;
                this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);
                RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardGradeID,KeyID", fields);
                RptTotalBind();

                BindBatchList();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                ViewState["CardGradeCode"] = null;
                ViewState["CardGrade"] = null;
                ViewState["OrderQTY"] = null;

                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Hidden = true;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Hidden = true;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID);
                this.lblStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                store = new Edge.SVA.BLL.Store().GetModel(Model.FromStoreID);
                this.lblFromStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.lblCustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetOrderPickingApproveStatusString(this.Model.ApproveStatus);
                //this.Remark.Text = Model.Remark.ToString();
                ///this.Remark1.Text = Model.Remark1.ToString();
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手动"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自动"; break;
                    }
                }

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                    this.btnPrint.Visible = false;
                }
                else
                {
                    this.btnPrint.Visible = false;
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }

                string CardTypeList = Controllers.CardOrderController.GetOrderCardType(Model.ReferenceNo);
                if (!string.IsNullOrEmpty(CardTypeList))
                {
                    string strWhre = " CardTypeID in (" + CardTypeList + ")";
                    Edge.Web.Tools.ControlTool.BindCardType(this.CardTypeID, strWhre);
                }
                else
                {

                    Edge.Web.Tools.ControlTool.BindCardType(this.CardTypeID, " 1>2");
                }
            }
        }


        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            int userID = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            string CardPickingNumber = string.Empty;
            int CardTypeID = 0;
            int CardGradeID = 0;
            int QTY = 0;
            int batchID = 0;
            string CardUID = string.Empty;
            string StartCardNumber = string.Empty;
            int filterType = 0;

            if (string.IsNullOrEmpty(this.CardGradeID.SelectedValue))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return;
            }
            CardTypeID = ConvertTool.ToInt(this.CardTypeID.SelectedValue);
            CardGradeID = ConvertTool.ToInt(this.CardGradeID.SelectedValue);
            CardPickingNumber = this.CardPickingNumber.Text.Trim();

            if (cb1.Checked)
            {
                filterType = 1;
                batchID = Tools.ConvertTool.ConverType<int>(this.BatchCardID.SelectedValue);

                if (batchID <= 0)
                {
                    ShowWarning(Resources.MessageTips.NoSearchCondition);
                    return;
                }
            }
            else if (cb2.Checked)
            {
                filterType = 3;
                StartCardNumber = this.CardNumberFirst.Text.Trim();
                QTY = ConvertTool.ToInt(this.CardCount.Text);

                //if (String.IsNullOrEmpty(StartCardNumber))
                //{
                //    ShowWarning(Resources.MessageTips.NoSearchCondition);
                //    return;
                //}
            }
            else if (cb3.Checked)
            {
                filterType = 2;
                CardUID = this.CardUID.Text.Trim();

                if (String.IsNullOrEmpty(CardUID))
                {
                    ShowWarning(Resources.MessageTips.NoSearchCondition);
                    return;
                }
            }


            if (Controllers.CardOrderController.AddPickupDetailCard(userID, CardPickingNumber, CardTypeID,CardGradeID, QTY,0,0, batchID, CardUID, StartCardNumber, filterType) == 0)
            {

                string condition = string.Format("CardPickingNumber = '{0}' and CardGradeID = {1} and PickQTY = 0", CardPickingNumber, CardGradeID);
                if (new SVA.BLL.Ord_CardPicking_D().GetRecordCount(condition) == 1)
                {
                    System.Data.DataSet ds = new SVA.BLL.Ord_CardPicking_D().GetList(condition);
                    new SVA.BLL.Ord_CardPicking_D().Delete(Tools.ConvertTool.ConverType<int>(ds.Tables[0].Rows[0]["KeyID"].ToString()));
                }
                ReBindingGrid();
                //JscriptPrint(Resources.MessageTips.AddSuccess, "", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
            }
            RptTotalBind();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardPicking_H item = null;
            Edge.SVA.Model.Ord_CardPicking_H dataItem= this.GetDataObject();

            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P" && dataItem.ApproveStatus.ToUpper().Trim() != "R")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPendingOrNotPicked);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);


            //If repick(status is "R") then set the status to "P";
            if (Request.Params["Status"] != null && Request.Params["Status"] == "R")
                item.ApproveStatus = "P";
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardPicking_H>(item))
            {
                string msg = "";
                if (!Controllers.CardOrderController.CanApprovePicked(item, out msg))
                {
                    //JscriptPrint(msg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                    //JscriptPrint(msg, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(msg);
                }
                else
                {
                    CloseAndPostBack();
                    //JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                }
            }
            else
            {
                ShowUpdateFailed();
                //JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnSaveAndApprove_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardPicking_H item = null;
            Edge.SVA.Model.Ord_CardPicking_H dataItem= this.GetDataObject();

            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P" && dataItem.ApproveStatus.ToUpper().Trim() != "R")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPendingOrNotPicked);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);


            //If repick(status is "R") then set the status to "P";
            if (Request.Params["Status"] != null && Request.Params["Status"] == "R")
                item.ApproveStatus = "P";
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardPicking_H>(item))
            {
                string msg = "";
                if (!Controllers.CardOrderController.CanApprovePicked(item, out msg))
                {
                    ShowWarning(msg);
                }
                else
                {
                    String sb = item.CardPickingNumber;
                    Window1.Title = Resources.MessageTips.Approve;
                    string okScript = Window1.GetShowReference("Approve.aspx?ids=" + sb);
                    string cancelScript = "";
                    ShowConfirmDialog(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + sb.Replace(",", ";\n"), Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
                    //CloseAndPostBack();
                }
            }
            else
            {
                ShowUpdateFailed();
            }
        }
        //End

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Print.aspx?id={0}", Request.Params["id"]));
        }

        protected void cb1_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1.Checked)
            {
                this.BatchCardID.Enabled = true;
                this.CardCount.Enabled = false;
                this.CardCount.Text = "";
                this.CardNumberFirst.Enabled = false;
                this.CardNumberFirst.Text = "";
                this.CardUID.Enabled = false;
                this.CardUID.Text = "";

                this.cb2.Checked = false;
                this.cb3.Checked = false;

                BindBatchList();
            }
        }

        protected void cb2_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2.Checked)
            {
                this.BatchCardID.Enabled = false;
                this.BatchCardID.Text = "";
                this.CardCount.Enabled = true;
                this.CardNumberFirst.Enabled = true;
                this.CardUID.Enabled = false;
                this.CardUID.Text = "";

                this.cb1.Checked = false;
                this.cb3.Checked = false;

                BindBatchList();
            }
        }

        protected void cb3_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3.Checked)
            {
                this.BatchCardID.Enabled = false;
                this.BatchCardID.Text = "";
                this.CardCount.Enabled = false;
                this.CardCount.Text = "";
                this.CardNumberFirst.Enabled = false;
                this.CardNumberFirst.Text = "";
                this.CardUID.Enabled = true;

                this.cb1.Checked = false;
                this.cb2.Checked = false;

                BindBatchList();
            }
        }

        //protected void Grid2_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        //{
        //    if (e.CommandName == "Delete")
        //    {
        //        object[] keys = Grid2.DataKeys[e.RowIndex];
        //        int ordPickingNumber = Tools.ConvertTool.ConverType<int>(keys[0].ToString());

        //        Edge.SVA.BLL.Ord_CardPicking_D bll = new SVA.BLL.Ord_CardPicking_D();
        //        Edge.SVA.Model.Ord_CardPicking_D model = bll.GetModel(ordPickingNumber);
        //        if (model == null) return;

        //        if (bll.Delete(ordPickingNumber))
        //        {
        //            ReBindingGrid();
        //        }
        //        else
        //        {
        //            ShowWarning(Resources.MessageTips.DeleteFailed);
        //            // JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
        //        }

        //        if (bll.GetRecordCount(string.Format("CardPickingNumber='{0}' and CardTypeID = {1}", Request.Params["id"], model.CardTypeID)) <= 0)
        //        {
        //            model.PickQTY = 0;
        //            model.FirstCardNumber = null;
        //            model.EndCardNumber = null;
        //            model.PickupDateTime = null;
        //            model.BatchCardCode = null;

        //            if (bll.Add(model) > 0)
        //            {
        //                ReBindingGrid();
        //            }
        //            else
        //            {
        //                ShowWarning(Resources.MessageTips.DeleteFailed);
        //                //JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
        //            }
        //        }

        //        RptTotalBind();
        //    }
        //}
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFirstCardNumber.Text))
            {
                if (string.IsNullOrEmpty(sCardQty.Text))
                {
                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(sCardUID1.Text))
            {
                if (string.IsNullOrEmpty(sCardQty.Text))
                {
                    this.sCardQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCardQty.Text));
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text))
            {

            }
            else if (!string.IsNullOrEmpty(sCardUID2.Text))
            {

            }
            else
            {
                this.sFirstCardNumber.MarkInvalid("Please select the query conditions!");
                return;
            }

            ViewState["flg"] = 2;
            RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardGradeID,KeyID", fields);
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            Edge.SVA.BLL.Ord_CardPicking_D bll = new Edge.SVA.BLL.Ord_CardPicking_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            System.Data.DataSet ds1 = null;
            if (this.RecordCount <=0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex);
            }


            Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
            Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
            Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
            Tools.DataTool.AddCardStockStatusByID(ds, "StockStatus", "FirstCardNumber");
            DataTool.AddCardStockStatus(ds, "StockStatusName", "StockStatus");
            DataTool.AddCardUIDByCardNumber(ds, "FirstCardUID", "FirstCardNumber");
            DataTool.AddCardUIDByCardNumber(ds, "EndCardUID", "EndCardNumber");

            //add start
            //if (Convert.ToInt32(ViewState["flg"]) != 0)
            //{
            //    DataTable dt3 = (DataTable)ViewState["dt3"];
            //    foreach (DataRow dr3 in dt3.Rows)
            //    {
            //        DataRow[] dr = ds.Tables[0].Select("FirstCardNumber='" + dr3["FirstCardNumber"].ToString() + "'");
            //        if (dr.Length > 0)
            //        {
            //            ds.Tables[0].Rows.Remove(dr[0]);
            //        }
            //    }
            //}
            //add end

            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            long totalOrderAmount = 0;
            long totalPickAmount = 0;
            long totalOrderPoint = 0;
            long totalPickPoint = 0;
            Controllers.CardOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY, out totalOrderAmount, out totalPickAmount, out totalOrderPoint, out totalPickPoint);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();

            //add start
            if (Convert.ToInt32(ViewState["flg"]) == 0)
            {
                DataTable dt2 = ds.Tables[0].Clone();
                this.Grid3.DataSource = dt2;
                this.Grid3.DataBind();
                //DataTable dt3 = ds.Tables[0].Clone();
                ViewState["dt2"] = dt2;
                //ViewState["dt3"] = dt3;
            }
            else if (Convert.ToInt32(ViewState["flg"]) == 2)
            {
                string strWhere1="";
                string endCardNumber;
                string endCardUID;
                if (!string.IsNullOrEmpty(sFirstCardNumber.Text))
                {
                    endCardNumber = (Convert.ToInt64(sFirstCardNumber.Text) + (Convert.ToInt32(sCardQty.Text) - 1)).ToString();
                    strWhere1 = " (FirstCardNumber >= '" + sFirstCardNumber.Text + "' " + " and FirstCardNumber<='" + endCardNumber + "') ";
                }
                else if (!string.IsNullOrEmpty(sCardUID1.Text))
                {
                    string CardNumbers = "";
                    endCardUID = (Convert.ToInt64(sCardUID1.Text) + (Convert.ToInt32(sCardQty.Text) - 1)).ToString();
                    DataSet dsCardUIDMap = new Edge.SVA.BLL.CardUIDMap().GetList("CardUID >= '" + sCardUID1.Text + "' " + " and CardUID<='" + endCardUID + "'");
                    foreach (DataRow drCardUIDMap in dsCardUIDMap.Tables[0].Rows)
                    {
                        CardNumbers += "'" + Convert.ToString(drCardUIDMap["CardNumber"]) + "',";
                    }
                    if (CardNumbers == "")
                    {
                        CardNumbers = "''";
                    }
                    else 
                    {
                       CardNumbers = CardNumbers.TrimEnd(',');
                    }
                    strWhere1 = " FirstCardNumber in (" + CardNumbers + ") ";
                }
                else if (!string.IsNullOrEmpty(sCurrentCardNumber.Text))
                {
                    strWhere1 = " FirstCardNumber = '" + sCurrentCardNumber.Text + "' ";

                }
                else if (!string.IsNullOrEmpty(sCardUID2.Text))
                {
                    string CardNumber = "";
                    DataSet dsCardUIDMap = new Edge.SVA.BLL.CardUIDMap().GetList("CardUID ='" + sCardUID2.Text + "'");
                    if (dsCardUIDMap.Tables[0].Rows.Count > 0)
                    {
                        CardNumber = Convert.ToString(dsCardUIDMap.Tables[0].Rows[0]["CardNumber"]);
                    }
                    strWhere1 = " FirstCardNumber = '" + CardNumber + "' ";
                }
                bll.StrWhere = bll.StrWhere + " and " + strWhere1; 
                ds1 = bll.GetList(this.Grid3.PageSize, this.Grid3.PageIndex);
                Tools.DataTool.AddCardTypeName(ds1, "CardType", "CardTypeID");
                Tools.DataTool.AddCardTypeCode(ds1, "CardTypeCode", "CardTypeID");
                Tools.DataTool.AddCardGradeName(ds1, "CardGrade", "CardGradeID");
                Tools.DataTool.AddCardGradeCode(ds1, "CardGradeCode", "CardGradeID");
                Tools.DataTool.AddCardStockStatusByID(ds1, "StockStatus", "FirstCardNumber");
                DataTool.AddCardStockStatus(ds1, "StockStatusName", "StockStatus");
                DataTool.AddCardUIDByCardNumber(ds1, "FirstCardUID", "FirstCardNumber");
                DataTool.AddCardUIDByCardNumber(ds1, "EndCardUID", "EndCardNumber");
                this.Grid3.DataSource = ds1.Tables[0].DefaultView;
                this.Grid3.DataBind();
                ViewState["dt2"] = ds1.Tables[0];
            }
            //add end
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return 0;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : 0;
            }
            set
            {
                if (value < 0) return;
                this.Grid2.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid3.PageIndex = e.NewPageIndex;

            DataTable dt2 = (DataTable)ViewState["dt2"];
            this.Grid3.DataSource = dt2;
            this.Grid3.DataBind();
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            ViewState["CardGradeCode"] = null;
            ViewState["CardGrade"] = null;
            ViewState["OrderQTY"] = null;
            ViewState["flg"] = 1;

            this.Grid2.PageIndex = e.NewPageIndex;

            RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardGradeID,KeyID", fields);

        }

        protected void Grid2_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            //string CardTypeCode = "";
            //string CardType = "";
            //string OrderQTY = "";

            if (e.DataItem is DataRowView)
            {
                //显示格式
                Label lblCardGradeCode = Grid2.Rows[e.RowIndex].FindControl("lblCardGradeCode") as Label;
                if (lblCardGradeCode != null)
                {
                    Label lblCardGrade = (Label)Grid2.Rows[e.RowIndex].FindControl("lblCardGrade");
                    Label lblOrderQTY = (Label)Grid2.Rows[e.RowIndex].FindControl("lblOrderQTY");
                    Label lblSeq = Grid2.Rows[e.RowIndex].FindControl("lblSeq") as Label;
                    HiddenField hfCardGradeID = Grid2.Rows[e.RowIndex].FindControl("hfCardGradeID") as HiddenField;
                    //重复
                    if (ViewState["CardGradeCode"] != null && ViewState["CardGradeCode"].ToString().Trim() == lblCardGradeCode.Text.Trim())
                    {
                        lblCardGradeCode.Visible = false;
                        if (lblCardGrade != null) { lblCardGrade.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CardGradeCode"] = lblCardGradeCode.Text.Trim();
                        if (lblCardGrade != null) { ViewState["CardGrade"] = lblCardGrade.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CardGradeIndex[int.Parse(hfCardGradeID.Value)]).ToString(); }
                    }
                }
            }

        }

        private Dictionary<int, int> CardGradeIndex
        {
            get
            {
                if (ViewState["CardTypeIndex"] == null)
                {
                    ViewState["CardGradeIndex"] = new SVA.BLL.Ord_CardPicking_D().GetCardGradeIndex(Request.Params["id"]);
                }
                return ViewState["CardGradeIndex"] as Dictionary<int, int>;
            }
        }

        #endregion

        #region 捡回汇总列表
        private void RptTotalBind()
        {
            Edge.SVA.BLL.Ord_CardPicking_D bll = new Edge.SVA.BLL.Ord_CardPicking_D();

            System.Data.DataSet ds = bll.GetListGroupByCardGrade(string.Format("CardPickingNumber='{0}'", Request.Params["id"].Trim()));

            //Tools.DataTool.AddCardTypeNameByID(ds, "CardType", "CardTypeID");
            //Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            Tools.DataTool.AddCardGradeNameByID(ds, "CardGrade", "CardGradeID");
            Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
            Tools.DataTool.AddID(ds, "ID", this.Grid2.PageSize, this.Grid2.PageIndex);
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();

            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            long totalOrderAmount = 0;
            long totalPickAmount = 0;
            long totalOrderPoint = 0;
            long totalPickPoint = 0;
            Controllers.CardOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY, out totalOrderAmount, out totalPickAmount, out totalOrderPoint, out totalPickPoint);

            lblGrid1TotalOrderQTY.Text = totalOrderQTY.ToString();
            lblGrid1TotalPickQTY.Text = totalPickQTY.ToString();

            if (!Controllers.CardOrderController.IsMeetPickingByType(totalOrderQTY, totalPickQTY))
            {
                lblGrid1TotalPickQTY.CssStyle = "color:red;font-weight:bold;";
            }
        }

        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                Label orderQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblOrderQTY1");
                Label pickQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblPickQTY1");

                long longOrderQTY = Tools.ConvertTool.ConverType<long>(orderQTY.Text);
                long longPickQTY = Tools.ConvertTool.ConverType<long>(pickQTY.Text);

                if (!Controllers.CardOrderController.IsMeetPickingByType(longOrderQTY, longPickQTY))
                {
                    pickQTY.ForeColor = System.Drawing.Color.Red;
                    pickQTY.Font.Bold = true;
                }
            }
        }
        #endregion

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected override SVA.Model.Ord_CardPicking_H GetPageObject(SVA.Model.Ord_CardPicking_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void BindBatchList()
        {
            if (cb1.Checked)
            {
                if (CardTypeID.SelectedValue != "")
                {
                    Tools.ControlTool.BindBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
                }
                else
                {
                    Tools.ControlTool.BindBatchID(BatchCardID);
                }
            }
            else
            {
                BatchCardID.Items.Clear();
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
            BindBatchList();
        }

        private void ReBindingGrid()
        {
            ViewState["CardGradeCode"] = null;
            ViewState["CardGrade"] = null;
            ViewState["OrderQTY"] = null;
            ViewState["flg"] = 1;

            this.RecordCount = 0;
            this.Grid2.PageIndex = 0;
            RptBind(string.Format("CardPickingNumber='{0}'", Request.Params["id"]), "CardGradeID", fields);
        }

        protected void btnSelectAll_OnClick(object sender, EventArgs e)
        {
            Grid3.SelectAllRows();
        }


        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            DataTable dt2 = (DataTable)ViewState["dt2"];
            foreach (int i in Grid3.SelectedRowIndexArray)
            {
                string keyID = Grid3.DataKeys[i][0].ToString();
                DataRow[] dr = dt2.Select("KeyID=" + keyID);
                if (dr.Length > 0)
                {
                    dt2.Rows.Remove(dr[0]);
                }
                Edge.SVA.BLL.Ord_CardPicking_D bll = new SVA.BLL.Ord_CardPicking_D();

                if (bll.Delete(Convert.ToInt32(keyID)))
                {
                    ReBindingGrid();
                }
                else
                {
                    ShowWarning(Resources.MessageTips.DeleteFailed);
                }
            }
            this.Grid3.DataSource = dt2;
            this.Grid3.DataBind();
            ViewState["dt2"] = dt2;
        }

    }
}