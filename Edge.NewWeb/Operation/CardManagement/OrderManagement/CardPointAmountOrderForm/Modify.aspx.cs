﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountOrderForm
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardOrderForm_H, Edge.SVA.Model.Ord_CardOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);

                this.CardOrderFormNumber.Text = DALTool.GetREFNOCode(CardController.CardRefnoCode.CardOrderForm);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;

                ControlTool.BindStore(FromStoreID);
                ControlTool.BindBrand(ddlBrand);
                ControlTool.BindBrand(brandDetail);
                ControlTool.BindStore(StoreID, -1);
                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", -1));
                ControlTool.BindCardGrade(CardGradeID, -1);

                //ControlTool.AddTitle(ddlBrand);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardOrderFormNumber.Text = Model.CardOrderFormNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }


                Edge.SVA.Model.Store store = new SVA.BLL.Store().GetModel(Model.StoreID);
                int brandID = store == null ? -1 : store.BrandID.GetValueOrDefault();
                this.ddlBrand.SelectedValue = brandID < 0 ? "" : brandID.ToString();

                ControlTool.BindStore(StoreID, brandID);

                string storeID = Model.StoreID.ToString();
                this.StoreID.SelectedValue = this.StoreID.Items.FindByValue(storeID) == null ? "" : storeID;

                this.BindDetail();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteErrorLog(this.PageName, "Update");

            if (this.ddlBrand.SelectedValue == "")
            {
                this.ddlBrand.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlBrand.Text));
                return;
            }
            if (this.StoreID.SelectedValue == "")
            {
                this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
                return;
            }

            Edge.SVA.Model.Ord_CardOrderForm_H item = null;
            Edge.SVA.Model.Ord_CardOrderForm_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            if (this.Detail.Rows.Count <= 0)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            //Update model
            item = this.GetPageObject(dataItem);

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardOrderForm_H>(item))
            {
                Edge.SVA.BLL.Ord_CardOrderForm_D bll = new SVA.BLL.Ord_CardOrderForm_D();
                bll.DeleteByOrder(this.CardOrderFormNumber.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CardOrderForm_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CardOrderFormNumber"] = item.CardOrderFormNumber;
                        //row["BrandID"] = detail["BrandID"];
                        row["CardTypeID"] = detail["CardTypeID"];
                        row["CardGradeID"] = detail["CardGradeID"];
                        row["OrderAmount"] = detail["OrderAmount"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_CardOrderForm_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, "Update", ex);
                    ShowAddFailed();
                    return;
                }
                CloseAndPostBack();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.brandDetail.SelectedItem.Value))
            {
                brandDetail.MarkInvalid(String.Format("'{0}' Can't Empty！", brandDetail.Text));
                return;
                //throw new Exception("Brand Can't Empty");
            }
            if (string.IsNullOrEmpty(this.CardGradeID.SelectedItem.Value))
            {
                CardGradeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CardGradeID.Text));
                return;
                // throw new Exception("Card Grade Can't Empty");
            }
            if (string.IsNullOrEmpty(this.txtOrderCount.Text))
            {
                txtOrderCount.MarkInvalid(String.Format("'{0}' Can't Empty！", txtOrderCount.Text));
                return;
                //throw new Exception("Card Qty Can't Empty");
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CardGradeID"].ToString().Equals(this.CardGradeID.SelectedValue))
                {
                    //JscriptPrint(Resources.MessageTips.ExistCardTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["ID"] = this.DetailIndex;
            //row["BrandID"] = int.Parse(this.brandDetail.SelectedItem.Value);
            //row["BrandCode"] = this.brandDetail.SelectedItem.Text.Substring(0, this.brandDetail.SelectedItem.Text.IndexOf("-")).Trim();
            //row["BrandName"] = this.brandDetail.SelectedItem.Text.Substring(this.brandDetail.SelectedItem.Text.IndexOf("-") + 1).Trim();
            row["CardTypeID"] = int.Parse(this.CardTypeID.SelectedItem.Value);
            row["CardTypeCode"] = this.CardTypeID.SelectedItem.Text.Substring(0, this.CardTypeID.SelectedItem.Text.IndexOf("-"));
            row["CardTypeName"] = this.CardTypeID.SelectedItem.Text.Substring(this.CardTypeID.SelectedItem.Text.IndexOf("-") + 1);
            row["CardGradeID"] = int.Parse(this.CardGradeID.SelectedItem.Value);
            row["CardGradeCode"] = this.CardGradeID.SelectedItem.Text.Substring(0, this.CardGradeID.SelectedItem.Text.IndexOf("-"));
            row["CardGradeName"] = this.CardGradeID.SelectedItem.Text.Substring(this.CardGradeID.SelectedItem.Text.IndexOf("-") + 1);
            row["OrderAmount"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
            this.Detail.Rows.Add(row);

            this.BindDetail();

            // this.AnimateRoll("tranctionDetial");
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int CardID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(CardID);
                BindDetail();
            }
        }

        protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStore(StoreID, brandID);
        }

        protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0} order by CardTypeCode", brandID));
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                StoreAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.StoreTel;
            }
            else
            {
                StoreAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        protected void StoreID_SelectedIndexChanged2(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(FromStoreID.SelectedValue.ToString()));
            if (model != null)
            {
                FromAddress.Text = model.StoreAddressDetail;
                FromContactName.Text = model.Contact;
                FromContactNumber.Text = model.StoreTel;
            }
            else
            {
                FromAddress.Text = "";
                FromContactName.Text = "";
                FromContactNumber.Text = "";
            }

        }

         private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CardOrderForm_D().GetList(string.Format("CardOrderFormNumber = '{0}'", Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;
                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
                    Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
                    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                    Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");

                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void DeleteDetail(int CardID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(CardID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected override SVA.Model.Ord_CardOrderForm_H GetPageObject(SVA.Model.Ord_CardOrderForm_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }
    }
}