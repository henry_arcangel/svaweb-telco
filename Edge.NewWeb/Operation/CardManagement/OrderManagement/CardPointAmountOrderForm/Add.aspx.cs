﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardPointAmountOrderForm
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardOrderForm_H, Edge.SVA.Model.Ord_CardOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                this.CardOrderFormNumber.Text = DALTool.GetREFNOCode(CardController.CardRefnoCode.CardOrderForm);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;
                this.lblOrderType.Text = "手动";

                ControlTool.BindStore(FromStoreID);
                ControlTool.BindBrand(ddlBrand);
                ControlTool.BindBrand(brandDetail);
                ControlTool.BindStore(StoreID, -1);

                ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0} and IsDumpCard =1 order by CardTypeCode", -1));
                ControlTool.BindCardGrade(CardGradeID, -1);

                //ControlTool.AddTitle(ddlBrand);
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.ddlBrand.SelectedValue == "")
            {
                this.ddlBrand.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlBrand.Text));
                return;
            }
            if (this.StoreID.SelectedValue == "")
            {
                this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
                return;
            }

            Edge.SVA.Model.Ord_CardOrderForm_H item = this.GetAddObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} No Data", item.CardOrderFormNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            if (this.Detail.Rows.Count <= 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Detail No Data", item.CardOrderFormNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.PurchaseType = 2;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;
            item.CreatedOn = DateTime.Now;
            item.ApproveOn = null;
            item.ApprovalCode = null;

            if (Tools.DALTool.Add<Edge.SVA.BLL.Ord_CardOrderForm_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CardOrderForm_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CardOrderFormNumber"] = item.CardOrderFormNumber;
                        //row["BrandID"] = detail["BrandID"];
                        row["CardTypeID"] = detail["CardTypeID"];
                        row["CardGradeID"] = detail["CardGradeID"];
                        row["OrderAmount"] = detail["OrderAmount"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_CardOrderForm_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Edge.SVA.BLL.Ord_CardOrderForm_D bll = new SVA.BLL.Ord_CardOrderForm_D();
                    bll.DeleteByOrder(this.CardOrderFormNumber.Text.Trim());

                    Edge.SVA.BLL.Ord_CardOrderForm_H hearder = new SVA.BLL.Ord_CardOrderForm_H();
                    hearder.Delete(this.CardOrderFormNumber.Text.Trim());

                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Card Order Form  {0} Add Success But Detail Failed", item.CardOrderFormNumber), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Add Success", item.CardOrderFormNumber));
               // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Card Order Form  {0} Add Failed", item.CardOrderFormNumber));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (this.brandDetail.SelectedValue == "-1")
            {
                brandDetail.MarkInvalid(String.Format("'{0}' Can't Empty！", brandDetail.Text));
                return;
                //throw new Exception("Brand Can't Empty");
            }
            if (this.CardGradeID.SelectedValue == "-1")
            {
                CardGradeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CardGradeID.Text));
                return;
                // throw new Exception("Card Grade Can't Empty");
            }
            if (string.IsNullOrEmpty(this.txtOrderCount.Text))
            {
                txtOrderCount.MarkInvalid(String.Format("'{0}' Can't Empty！", txtOrderCount.Text));
                return;
                //throw new Exception("Card Qty Can't Empty");
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CardGradeID"].ToString().Equals(this.CardGradeID.SelectedValue))
                {
                    //JscriptPrint(Resources.MessageTips.ExistCardTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["ID"] = this.DetailIndex;
            row["CardTypeID"] = int.Parse(this.CardTypeID.SelectedItem.Value);
            row["CardTypeCode"] = this.CardTypeID.SelectedItem.Text.Substring(0, this.CardTypeID.SelectedItem.Text.IndexOf("-"));
            row["CardTypeName"] = this.CardTypeID.SelectedItem.Text.Substring(this.CardTypeID.SelectedItem.Text.IndexOf("-") + 1);
            row["CardGradeID"] = int.Parse(this.CardGradeID.SelectedItem.Value);
            row["CardGradeCode"] = this.CardGradeID.SelectedItem.Text.Substring(0, this.CardGradeID.SelectedItem.Text.IndexOf("-"));
            row["CardGradeName"] = this.CardGradeID.SelectedItem.Text.Substring(this.CardGradeID.SelectedItem.Text.IndexOf("-") + 1);
            row["OrderAmount"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
            this.Detail.Rows.Add(row);

            this.BindDetail();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int CardID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(CardID);
                BindDetail();
            }
        }

        protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStore(StoreID, brandID);
        }

        protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCardType(CardTypeID, string.Format("BrandID = {0}  order by CardTypeCode", brandID));
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.BindCardGrade(CardGradeID, Convert.ToInt32(CardTypeID.SelectedValue));
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                StoreAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.StoreTel;
            }
            else
            {
                StoreAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        protected void StoreID_SelectedIndexChanged2(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(FromStoreID.SelectedValue.ToString()));
            if (model != null)
            {
                FromAddress.Text = model.StoreAddressDetail;
                FromContactName.Text = model.Contact;
                FromContactNumber.Text = model.StoreTel;
            }
            else
            {
                FromAddress.Text = "";
                FromContactName.Text = "";
                FromContactNumber.Text = "";
            }

        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("ID", typeof(int));
                    //dt.Columns.Add("BrandID", typeof(int));
                    //dt.Columns.Add("BrandCode", typeof(string));
                    //dt.Columns.Add("BrandName", typeof(string));
                    dt.Columns.Add("CardTypeID", typeof(int));
                    dt.Columns.Add("CardTypeCode", typeof(string));
                    dt.Columns.Add("CardTypeName", typeof(string));
                    dt.Columns.Add("CardGradeID", typeof(int));
                    dt.Columns.Add("CardGradeCode", typeof(string));
                    dt.Columns.Add("CardGradeName", typeof(string));
                    dt.Columns.Add("OrderAmount", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void DeleteDetail(int CardID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(CardID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected override SVA.Model.Ord_CardOrderForm_H GetPageObject(SVA.Model.Ord_CardOrderForm_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

    }
}