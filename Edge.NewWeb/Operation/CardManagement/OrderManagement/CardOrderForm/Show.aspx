﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.OrderManagement.CardOrderForm.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--  <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="订单信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Label="交易状态：" Text="P">
                    </ext:HiddenField>
                    <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CardOrderFormNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="lblOrderType" runat="server" Label="订单类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Remark" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="库存(出)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="FromStoreID" runat="server" Label="总部：">
                                    </ext:Label>
                                    <ext:Label ID="FromAddress" runat="server" Label="地址：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="FromContactName" runat="server" Label="联系人：">
                                    </ext:Label>
                                    <ext:Label ID="FromContactNumber" runat="server" Label="联系电话：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="订单内容"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="CustomerType" runat="server">
                                        <ext:RadioItem Text="客户订货" Value="1" />
                                        <ext:RadioItem Text="店铺订货" Value="2" Selected="true" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="SendMethod" runat="server" Label="Label">
                                        <ext:ListItem Text="直接交付（打印）" Value="1" Selected="true" />
                                        <ext:ListItem Text="SMS" Value="2" />
                                        <ext:ListItem Text="Email" Value="3" />
                                        <ext:ListItem Text="Social Network" Value="4" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CustomerTypeView" runat="server" Label="订单类型：">
                                    </ext:Label>
                                    <ext:Label ID="ddlBrand" runat="server" Label="订货品牌：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StoreID" runat="server" Label="店铺：">
                                    </ext:Label>
                                    <ext:Label ID="CustomerID" runat="server" Label="订货客户：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="SendMethodView" runat="server" Label="送货单发送方式：">
                                    </ext:Label>
                                    <ext:Label ID="SendAddress" runat="server" Label="送货地址：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="StoreContactName" runat="server" Label="联系人：">
                                    </ext:Label>
                                    <ext:Label ID="StoreContactEmail" runat="server" Label="邮件发送：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <%--<ext:Label ID="SMSMMS" runat="server" Label="Translate__Special_121_StartSMS/MMS发送：Translate__Special_121_End">
                                    </ext:Label>--%>
                                    <ext:Label ID="StoreContactPhone" runat="server" Label="联系电话：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="Remark1" runat="server" Label="备注：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="订单明细"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="CardGradeCode" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
<%--                            <ext:TemplateField Width="60px" HeaderText="品牌编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrandCode" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="品牌">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="卡级别编号">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGradeCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardGradeName" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="订货数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardQty" runat="server" Text='<%#Eval("CardQty","{0:N00}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <%--    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="4" align="left">
                订单信息
            </th>
        </tr>
        <tr>
            <td width="15%" align="right">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CardOrderFormNumber" runat="server" MaxLength="512"></asp:Label>
            </td>
            <td width="15%" align="right">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td colspan="3">
                <asp:Label ID="CreatedByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                订单内容
            </th>
        </tr>
        <tr>
            <td align="right" colspan="1">
                订单类型：
            </td>
            <td colspan="3">
                <asp:Label ID="CustomerTypeView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CustomerType" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Text="客户订货" Value="1"></asp:ListItem>
                    <asp:ListItem Text="店铺订货" Value="2" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货品牌：
            </td>
            <td>
                <asp:Label ID="ddlBrand" runat="server"></asp:Label>
            </td>
            <td align="right">
                店铺：
            </td>
            <td>
                <asp:Label ID="StoreID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货客户：
            </td>
            <td>
                <asp:Label ID="CustomerID" runat="server"></asp:Label>
            </td>
            <td align="right">
                送货单发送方式：
            </td>
            <td>
                <asp:Label ID="SendMethodView" runat="server"></asp:Label>
                <asp:DropDownList ID="SendMethod" runat="server" CssClass="dropdownlist">
                    <asp:ListItem Text="直接交付（打印）" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="SMS" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Email" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Social Network" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货地址：
            </td>
            <td>
                <asp:Label ID="SendAddress" runat="server"></asp:Label>
            </td>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:Label ID="ContactName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮件发送：
            </td>
            <td>
                <asp:Label ID="Email" runat="server"></asp:Label>
            </td>
            <td align="right">
                SMS/MMS发送：
            </td>
            <td>
                <asp:Label ID="SMSMMS" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:Label ID="ContactNumber" runat="server"></asp:Label>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="Remark" runat="server"></asp:Label>
            </td>
        </tr>
        <tr id="tranctionDetial">
            <td colspan="4">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist" id="msgtablelist">
                            <tr>
                                <th width="10%">
                                    品牌编号
                                </th>
                                <th width="35%">
                                    品牌
                                </th>
                                <th width="10%">
                                    优惠劵类型编号
                                </th>
                                <th width="35%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    订货数量
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#Eval("BrandCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("BrandName")%>
                            </td>
                            <td align="center">
                                <%#Eval("CardTypeCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("CardTypeName")%>
                            </td>
                            <td align="center">
                                <%#Eval("CardQty","{0:N00}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

