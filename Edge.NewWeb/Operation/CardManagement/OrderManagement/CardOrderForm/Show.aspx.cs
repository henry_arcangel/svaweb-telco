﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardOrderForm
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_CardOrderForm_H, SVA.Model.Ord_CardOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                this.CardOrderFormNumber.Text = Model.CardOrderFormNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Hidden = true;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Hidden = true;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID);
                this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                store = new Edge.SVA.BLL.Store().GetModel(Model.FromStoreID);
                this.FromStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);
                DataBind(string.Format("CardOrderFormNumber = '{0}'", Request.Params["id"]), "KeyID");
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            DataBind(string.Format("CardOrderFormNumber = '{0}'", Request.Params["id"]), "KeyID");
            this.AnimateRoll("tranctionDetial");
        }

        protected void DataBind(string strWhere, string orderby)
        {

            Edge.SVA.BLL.Ord_CardOrderForm_D bll = new Edge.SVA.BLL.Ord_CardOrderForm_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                this.RecordCount = count;
            }
            else
            {
                ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex);
            }
            Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            //Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            DataBind(string.Format("CardOrderFormNumber = '{0}'", Request.Params["id"]), "KeyID");
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}

        //protected override void OnLoadComplete(EventArgs e)
        //{
        //    base.OnLoadComplete(e);
            
        //    if (!this.IsPostBack)
        //    {
        //        this.rptPager.PageSize = webset.ContentPageNum;

        //        this.CardOrderFormNumber.Text = Model.CardOrderFormNumber;
        //        this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
        //        this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
        //        this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
        //        this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

        //        this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
        //        this.CustomerType.Visible = false;

        //        this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
        //        this.SendMethod.Visible = false;

        //        Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
        //        this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

        //        Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
        //        this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

        //        Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
        //        this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);
        //        DataBind(string.Format("CardOrderFormNumber = '{0}'", Request.Params["id"]), "KeyID");
        //    }
        //}

        //protected void rptListPager_PageChanged(object sender, EventArgs e)
        //{
        //     DataBind(string.Format("CardOrderFormNumber = '{0}'",Request.Params["id"]), "KeyID");
        //     this.AnimateRoll("tranctionDetial");
        //}

        //protected void DataBind(string strWhere, string orderby)
        //{
        //    int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

        //    Edge.SVA.BLL.Ord_CardOrderForm_D bll = new Edge.SVA.BLL.Ord_CardOrderForm_D()
        //    {
        //        StrWhere = strWhere,
        //        Order = orderby,
        //        Timeout = 60
        //    };

        //    System.Data.DataSet ds = null;
        //    if (this.RecordCount < 0)
        //    {
        //        int count = 0;
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
        //        this.RecordCount = count;
        //    }
        //    else
        //    {
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage);
        //    }
        //    Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
        //    Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
        //    Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
        //    Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
          
        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}

        //private int RecordCount
        //{
        //    get
        //    {
        //        if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
        //        int count = 0;
        //        return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
        //    }
        //    set
        //    {
        //        if (value < 0) return;
        //        this.rptPager.RecordCount = value;
        //        ViewState["RecordCount"] = value;
        //    }
        //}
    }
}