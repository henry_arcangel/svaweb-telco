﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.OrderManagement.CardOrderForm
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    ViewState["TotalOrderQTY"] = 0;
                    ViewState["TotalPickQTY"] = 0;

                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));


                    DataTable orders = new DataTable();
                    orders.Columns.Add("CardPickingNumber", typeof(string));
                    orders.Columns.Add("PrintDateTime", typeof(string));
                    orders.Columns.Add("ReferenceNo", typeof(string));
                    orders.Columns.Add("ApproveStatus", typeof(string));
                    orders.Columns.Add("PickingDate", typeof(string));
                    orders.Columns.Add("PickedBy", typeof(string));

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    foreach (string id in idList)
                    {
                        Edge.SVA.Model.Ord_CardOrderForm_H mode = new Edge.SVA.BLL.Ord_CardOrderForm_H().GetModel(id);
                        bool isSuccess = false;
                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = id;
                        dr["ApproveCode"] = CardOrderController.ApproveForApproveCode(mode, out isSuccess);
                        if (isSuccess)
                        {
                            this.div_print.Visible = true;
                            this.btnPrint.Visible = true;
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Order Form " + id + " " + Resources.MessageTips.ApproveCode);
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                            SVA.Model.Ord_CardPicking_H picking = new SVA.BLL.Ord_CardPicking_H().GetModelByOrderNumber(id);
                            if (picking == null) continue;

                            DataRow order = orders.NewRow();

                            order["CardPickingNumber"] = picking.CardPickingNumber;
                            order["PrintDateTime"] = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                            order["ReferenceNo"] = picking.ReferenceNo;
                            order["ApproveStatus"] = Tools.DALTool.GetOrderPickingApproveStatusString(picking.ApproveStatus);
                            order["PickingDate"] = Tools.ConvertTool.ToStringDate(picking.ApproveOn.GetValueOrDefault());
                            order["PickedBy"] = Tools.DALTool.GetUserName(picking.ApproveBy.GetValueOrDefault());

                            orders.Rows.Add(order);

                            this.rptOrders.DataSource = orders;
                            this.rptOrders.DataBind();
                        }
                        else
                        {
                            this.div_print.Visible = false;
                            this.btnPrint.Visible = false;
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Order Form " + id + " " + Resources.MessageTips.ApproveError);
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }
                        dt.Rows.Add(dr);

                    }
                    this.rptList.DataSource = dt;
                    this.rptList.DataBind();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    FineUI.Alert.ShowInTop(Resources.MessageTips.SystemError, "", FineUI.MessageBoxIcon.Error, FineUI.ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;


                System.Data.DataSet ds = new SVA.BLL.Ord_CardOrderForm_D().GetList(string.Format("CardOrderFormNumber = '{0}'", drv["ReferenceNo"].ToString()));

                Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
                Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");

                list.DataSource = ds.Tables[0];
                list.DataBind();
            }
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");
                if (lblOrderQTY != null)
                {                        //统计数量
                    ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                ViewState["TotalOrderQTY"] = 0;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = "0";
                }
            }

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}