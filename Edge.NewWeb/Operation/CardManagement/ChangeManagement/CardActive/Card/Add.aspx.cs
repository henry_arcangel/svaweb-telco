﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardActive;
using Edge.Web.Controllers;
using System.Text.RegularExpressions;


namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardActive.Card
{
    public partial class Add : PageBase
    {
        CardActiveController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                CardTypeID_SelectedIndexChanged(null, null);

                //if (!string.IsNullOrEmpty(Request["StoreID"]))
                //{
                //   // Edge.Web.Tools.ControlTool.BindCardTypeWithoutBrand(CardTypeID, "CardTypeID in (" + Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") order by CardTypeCode");
                //}
                //else
                //{
                //    Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                //}

                Tools.ControlTool.BindBatchID(BatchCardID);

                RegisterCloseEvent(this.btnCloseSearch);
            }
            controller = SVASessionInfo.CardActiveController;
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            //if (this.IsOpenAmount)
            //{
            //    if (string.IsNullOrEmpty(this.CardTypeAmount.Text))
            //    {
            //        ShowWarning(Resources.MessageTips.EntryDenomination);
            //        return;
            //    }
            //    double amount = 0.00;
            //    if (!double.TryParse(this.CardTypeAmount.Text.Trim(), out amount))
            //    {
            //        ShowWarning(Resources.MessageTips.EntryTwoDecimal);
            //        return;
            //    }
            //}

            SyncSelectedRowIndexArrayToHiddenField();
            List<string> idList = GetSelectedRowIndexArrayFromHiddenField();

            if (ViewState["SearchResult"] != null)
            {
                if (controller.ViewModel.CardTable == null)
                {
                    controller.ViewModel.CardTable = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = controller.ViewModel.CardTable;
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    for (int i = 0; i < idList.Count; i++)
                    {
                        ids += string.Format("{0},", "'" + idList[i].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    //foreach (DataRowView drv in dvSearch)
                    //{
                    //    drv.Delete();
                    //}
                    //vsDT.AcceptChanges();

                    //ViewState["SearchResult"] = vsDT;
                    ViewState["SearchResult"] = dtSearch;

                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];

                    //ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();

                    cbSearchAll.Checked = false;
                }

                #region 验证金额是否一致
                DataTable dt = ViewState["SearchResult"] as DataTable;
                if (this.IsOpenAmount && dt.Rows.Count > 0)
                {
                    int j = dt.Rows.Count - 1;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        double startAmount = string.IsNullOrEmpty(dt.Rows[i]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[i]["TotalAmount"]);
                        double endAmount = string.IsNullOrEmpty(dt.Rows[j]["TotalAmount"].ToString()) ? 0.00 : Convert.ToDouble(dt.Rows[j]["TotalAmount"]);
                        if (startAmount != endAmount)
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90492"));
                            return;
                        }
                    }
                    //Remark by Nathan 20140820 ++
                    //if (Convert.ToDouble(dt.Rows[0]["CardAmount"]) != Convert.ToDouble(this.CardTypeAmount.Text))
                    //{
                    //    if (Convert.ToDouble(dt.Rows[0]["CardAmount"]) != Convert.ToDouble(0))
                    //    {
                    //        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90443"));
                    //        return;
                    //    }
                    //    for (int i = 0; i < dt.Rows.Count; i++)
                    //    {
                    //        dt.Rows[i]["CardAmount"] = this.CardTypeAmount.Text;
                    //    }
                    //}
                    //Remark by Nathan 20140820 --
                }
                #endregion

                DataTable addDT = controller.ViewModel.CardTable;

                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");

                controller.ViewModel.CardTable = newSearchDT;

                //返回界面时要清空查询记录
                ViewState["SearchResult"] = null;
            }
            CloseAndPostBack();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);
            if (!ValidaData())
            {
                return;
            }
            
            Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            Edge.SVA.Model.CardUIDMap modelmap = null;
            DataSet ds = new DataSet();
            //校验不能在此店铺激活的CardNumber
            if (this.CardNumber.Text != "" && this.CardUID.Text == "")
            {
                ds = bll1.GetList(" CardNumber = '" + this.CardNumber.Text + "'");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {

                    modelmap = bll1.GetModel(ds.Tables[0].Rows[0]["CardUID"].ToString());
                    if (!ValidataStore(modelmap)) return;
                }
            }
            //校验不能在此店铺激活的CardUID
            if (this.CardNumber.Text == "" && this.CardUID.Text != "")
            {
                modelmap = bll1.GetModel(this.CardUID.Text);

                if (modelmap != null)
                {
                    if (!ValidataStore(modelmap)) return;
                }
            }

            //
            if (!IsContinue(GetSearchDataTable(),false))
            {
                return;
            }
            this.SearchListGrid.PageIndex = 0;
            DataTable dt = GetSearchDataTable();

            //Add by Nathan 20140820 ++
            //if (dt != null)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        dt.Rows[i]["CardAmount"] = this.CardTypeAmount.Text;
            //    }
            //}
            //Add by Nathan 20140820 --

            ViewState["SearchResult"] = dt;
            BindCardGrid();
        }

        protected void BtnScanStart_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);
            ViewState["SearchResult"] = null;

            ChangeControlStatus(true);
            //this.CardNumber.Focus();
            this.CardUID.Focus();
        }
        protected void BtnScanStop_Click(object sender, EventArgs e)
        {
            ChangeControlStatus(false);
        }
        private void ChangeControlStatus(bool status)
        {
            this.BtnScanStart.Hidden = status;
            this.BtnScanStop.Hidden = !status;
            this.BtnAddScan.Hidden = !status;

            this.btnSearch.Enabled = !status;
            this.CardTypeID.Enabled = !status;
            this.BatchCardID.Enabled = !status;
            this.CardCount.Enabled = !status;
            //this.CardUID.Enabled = !status;
            this.CardTypeID.SelectedIndex = 0;

            this.CardNumber.Enabled = !status;

            this.CardUID.Text = string.Empty;
            this.CardNumber.Text = string.Empty;
        }
        protected void BtnAddScan_Click(object sender, EventArgs e)
        {
            if (this.BtnAddScan.Hidden)
            {
                return;
            }

            //Scan标记
            ViewState["IsScan"] = true;

            string cardUID = this.CardUID.Text.Trim();
            int top = string.IsNullOrEmpty(this.CardCount.Text) ? 1 : Convert.ToInt32(this.CardCount.Text);
            if (string.IsNullOrEmpty(cardUID))
            {
                ShowWarning(Resources.MessageTips.CardUIDCannotBeEmpty);//TODO:
                return;
            }
            //校验UID中不能包含字母
            if (Regex.Matches(cardUID, "[a-zA-Z]").Count > 0)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90312"));
                return;
            }
            this.SearchListGrid.PageIndex = 0;
            Edge.SVA.BLL.Card bll = new SVA.BLL.Card();
            Edge.SVA.BLL.CardUIDMap bll1 = new Edge.SVA.BLL.CardUIDMap();
            Edge.SVA.Model.CardUIDMap model = bll1.GetModel(cardUID);

            //不需要补齐GC所做的逻辑
            int cardGradeID = model == null ? 0 : Convert.ToInt32(model.CardGradeID);
            Edge.SVA.BLL.CardGrade blltype = new SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade modeltype = cardGradeID == 0 ? null : blltype.GetModel(cardGradeID);
            //string endCarduid = CardUID;
            string endCarduid = (Convert.ToInt64(cardUID) + (top -1)).ToString();
            if (modeltype == null)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90312"));
                return;
            }
            if (modeltype.UIDCheckDigit == 1)
            {
                endCarduid = (Convert.ToInt64(cardUID.Substring(0, cardUID.Length - 1)) + (top - 1)).ToString() + "9";
            }

            if (!ValidataStore(model)) return;

            DataTable dt = null;

            DataSet newds = null;
            if (top > 1)
            {

                //newds = bll.GetListForBatchOperation(top, string.Format(@" Card.Status in ( {0} )", (int)CardController.CardStatus.Issued) + " and CardUIDMap.CardUID >='" + CardUID
                //           + "' and Card.CardTypeID =( select top 1 CardTypeID from Card where Card.CardNumber >=( select CardNumber from CardUIDMap where CardUID='" + CardUID + "'))"
                //           , "CardUIDMap.CardUID ASC ");

                newds = bll.GetListForBatchOperation(top, string.Format(@" Card.Status in ( {0} )", (int)CardController.CardStatus.Issued) + " and CardUIDMap.CardUID >='" + cardUID
                                        + "' and CardUIDMap.CardUID <='" + endCarduid + "' and Card.CardGradeID =" + cardGradeID, "CardUIDMap.CardUID ASC ");
            }
            else
            {
                newds = bll.GetListForBatchOperation(top, string.Format(@" Card.Status in ( {0} )", (int)CardController.CardStatus.Issued) + " and CardUIDMap.CardUID ='" + cardUID + "'"
                          , "CardUIDMap.CardUID ASC ");
            }

            if (newds.Tables[0].Rows.Count <= 0)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return;
            }

            ViewState["Newds"] = newds.Tables[0];


            if (!IsContinue(newds.Tables[0], true)) return;

            //if (ViewState["SearchResult"] != null)
            //{
            //    dt = ViewState["SearchResult"] as DataTable;
            //    dt = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(newds.Tables[0], dt, "CardUID");
            //}
            //else
            //{
            //    dt = newds.Tables[0];
            //}

            //ViewState["SearchResult"] = dt;

            ShowTable();

            dt = ViewState["SearchResult"] as DataTable;

            //查出数据后要将UID清空
            if (dt != null && dt.Rows.Count > 0)
            {
                this.CardUID.Text = string.Empty;
            }
            BindCardGrid();
        }
        
        //protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (CardTypeID.SelectedValue != "-1")
        //    {
        //        Tools.ControlTool.BindBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
        //    }
        //    else
        //    {
        //        Tools.ControlTool.BindBatchID(BatchCardID);
        //    }
        //}
        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
                // Tools.ControlTool.BindCardGradeWithoutBrand(CardGradeID, "CardGradeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") order by CardGradeCode");

            }
            else
            {
                Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardGradeID.SelectedValue != "-1")
            {
              //  Tools.ControlTool.BindCardGradeWithoutBrand(CardGradeID, "CardGradeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") and CardTypeID =" + Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue) + " order by CardGradeCode");
                Tools.ControlTool.BindCardBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardGradeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindCardBatchID(BatchCardID);
            }
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindCardGrid();
        }

        private DataTable GetSearchDataTable()
        {
            try
            {
                Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

                int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
                int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.SelectedValue);
                string cardNumber = CardNumber.Text.Trim();
                //string cardTypeID = CardTypeID.SelectedValue;
                string cardGradeID = CardGradeID.SelectedValue;
                string strWhere = string.Format(" Card.Status in ( {0} )", (int)CardController.CardStatus.Issued);
                if (webset.IsActiveSameStore != 1)
                {
                    strWhere += " and Card.LocateStoreID=" + Request["StoreID"];
                }
                string filedOrder = " Card.CardNumber ASC ";

                if (cardGradeID == "-1")
                {
                    cardGradeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
                }

                //strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardTypeID, this.CardUID.Text.Trim(), strWhere);
                if (top > 0 && cardNumber == "" && this.CardUID.Text != "")
                {
                    Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                    Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                    cardNumber = model.CardNumber;
                    cardGradeID = model.CardGradeID.ToString();
                    strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardGradeID, "", strWhere);
                }
                else if (top > 0 && cardNumber != "" && this.CardUID.Text != "")
                {
                    strWhere = GetCardSearchStrWhere(top, batchCardID, this.CardNumber.Text, cardGradeID, "", strWhere);
                }
                else
                {
                    if (this.CardNumber.Text != "" && this.CardGradeID.SelectedValue == "-1")
                    {
                        Edge.SVA.Model.Card model = bll.GetModel(this.CardNumber.Text);
                        cardGradeID = model == null ? "-1" : model.CardGradeID.ToString();
                    }
                    strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardGradeID, this.CardUID.Text.Trim(), strWhere);
                }

                //Display message
                int count = bll.GetCount(strWhere);

                if (count <= 0)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return null;
                }
                if ((top > webset.MaxSearchNum) || ((count > webset.MaxSearchNum) && top <= 0))
                {
                    top = webset.MaxSearchNum;
                    ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                }

                DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);
                ViewState["SearchResult"] = ds.Tables[0];
                return ds.Tables[0];
            }
            catch
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
        }

        private void BindCardGrid()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();

                //判断CardAmount
                if (dt == null) return;

                if (dt.Rows.Count <= 0) return;

                //int cardTypeID = Tools.ConvertTool.ToInt(dt.Rows[0]["CardTypeID"].ToString());
                //Edge.SVA.Model.CardGrade cardGrade = new Edge.SVA.BLL.CardGrade().GetModel(cardTypeID);

                //if (cardGrade == null) return;
               //this.IsOpenAmount = cardGrade.CardtypeFixedAmount.GetValueOrDefault() == 1 ? false : true;  //TODO: Add by Nathan  ?????
                //this.CardTypeAmount.Enabled = this.IsOpenAmount ? true : false;
                //this.CardTypeAmount.Text = this.IsOpenAmount ? "" : cardGrade.CardTypeInitAmount.ToString();


                //Add by Nathan 20140820 ++
                int cardGradeID = Tools.ConvertTool.ToInt(this.CardGradeID.SelectedValue);
                Edge.SVA.BLL.CardGrade blltype = new SVA.BLL.CardGrade();
                Edge.SVA.Model.CardGrade modeltype = cardGradeID == 0 ? null : blltype.GetModel(cardGradeID);

                if (modeltype != null)
                {
                    this.CardTypeAmount.Text = modeltype.CardTypeInitAmount.Value.ToString("N2");
                }
                //Add by Nathan 20140820 --
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }

           // btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
        }

        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            BindCardGrid();

            UpdateSelectedRowIndexArray();
            if (cbSearchAll.Checked)
            {
                SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            }
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            if (SearchListGrid.SelectedRowIndexArray != null && SearchListGrid.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(SearchListGrid.SelectedRowIndexArray);
                for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
                {
                    string id = SearchListGrid.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
            {
                string id = SearchListGrid.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i);
                }
            }
            SearchListGrid.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion

        public bool IsOpenAmount
        {
            get
            {
                if (ViewState["IsOpenAmount"] == null) return false;

                if (ViewState["IsOpenAmount"] is bool) return (bool)ViewState["IsOpenAmount"];

                return false;
            }
            set
            {
                ViewState["IsOpenAmount"] = value;
            }
        }
        protected void cbSearchAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            if (!cbSearchAll.Checked)
            {
                hfSelectedIDS.Text = string.Empty;
            } 
        }

        protected bool ValidaData()
        {
            int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
            int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.SelectedValue);
            string cardNumber = CardNumber.Text.Trim();
            string cardGradeID = CardGradeID.SelectedValue;

            if ((top <= 0) && (batchCardID <= 0) && string.IsNullOrEmpty(cardNumber) && cardGradeID == "-1" && string.IsNullOrEmpty(this.CardUID.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }

            if (top > webset.MaxSearchNum)
            {

                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                return false;
            }
            if (top > 0 && cardNumber != "" && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
                else
                {
                    if (cardNumber != model.CardNumber)
                    {
                        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                        return false;
                    }
                }
            }
            //if (top > 0 && this.CardUID.Text != "" && cardTypeID == "-1")
            //{
            //    string cardTypeids = Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
            //    List<string> idlist = new List<string>();
            //    foreach (var item in cardTypeids.Split(','))
            //    {
            //        idlist.Add(item);
            //    }

            //    Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            //    Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
            //    if (model == null)
            //    {
            //        return false;
            //    }
            //    int i = 0;
            //    while (idlist.Count > i)
            //    {
            //        if (idlist[i] != model.CardTypeID.ToString())
            //        {
            //            i++;
            //        }
            //        else
            //        {
            //            return true;
            //        }
            //    }
            //    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
            //    return false;

            //}
            //if (top > 0 && this.CardNumber.Text != "" && cardTypeID == "-1")
            //{
            //    string cardTypeids = Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
            //    List<string> idlist = new List<string>();
            //    foreach (var item in cardTypeids.Split(','))
            //    {
            //        idlist.Add(item);
            //    }

            //    Edge.SVA.BLL.Card bll1 = new SVA.BLL.Card();
            //    Edge.SVA.Model.Card model = bll1.GetModel(this.CardNumber.Text);
            //    if (model == null)
            //    {
            //        return false;
            //    }
            //    int i = 0;
            //    while (idlist.Count > i)
            //    {
            //        if (idlist[i] != model.CardTypeID.ToString())
            //        {
            //            i++;
            //        }
            //        else
            //        {
            //            return true;
            //        }
            //    }
            //    if (model == null)
            //    {
            //        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
            //        return false;
            //    }
            //}
            if (top > 0 && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            if (top > 0 && this.CardNumber.Text != "")
            {
                Edge.SVA.BLL.Card bll1 = new SVA.BLL.Card();
                Edge.SVA.Model.Card model = bll1.GetModel(this.CardNumber.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            return true;
        }

        protected bool IsContinue(DataTable dt,bool IsScan)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                Edge.SVA.BLL.CardGrade cardGrade = new SVA.BLL.CardGrade();
                int j = dt.Rows.Count - 1;
                string firstnumber = "";
                string lastnumber = "";
                string cardGradeID = dt.Rows[0]["CardGradeID"].ToString();
                Edge.SVA.Model.CardGrade model = cardGrade.GetModel(Convert.ToInt32(cardGradeID));
                int startindex = model.CardNumPattern == null ? 0 : model.CardNumPattern.Length;
                int digit = 0;


                if (!IsScan)
                {
                    if (string.IsNullOrEmpty(this.CardNumber.Text))
                    {
                        firstnumber = dt.Rows[0]["CardNumber"].ToString();
                    }
                    else
                    {
                        firstnumber = this.CardNumber.Text.Trim();
                    }
                    lastnumber = dt.Rows[j]["CardNumber"].ToString();

                    if (model.CardCheckdigit.GetValueOrDefault() == 1)
                    {
                        digit = 1;
                    }

                }
                else
                {
                    firstnumber = this.CardUID.Text.Trim();
                    lastnumber = dt.Rows[j]["CardUID"].ToString();

                    if (model.UIDCheckDigit == 1)
                    {
                        digit = 1;
                    }
                }

                long startcard = Convert.ToInt64(firstnumber.Substring(startindex, firstnumber.Length - startindex - digit));
                long endcard = Convert.ToInt64(lastnumber.Substring(startindex, lastnumber.Length - startindex - digit));
                long cardqty = string.IsNullOrEmpty(this.CardCount.Text) ? 0 : Convert.ToInt64(this.CardCount.Text);

                if (endcard - startcard != j)
                {
                    //if (!IsScan)
                    //{
                    //    ViewState["SearchResult"] = dt;
                    //}
                    //else
                    //{
                        
                    //}
                    //if (!IsScan)
                    //{
                    //    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("10030"), "", MessageBoxIcon.Question, WindowContact.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    //}
                    //else
                    //{
                    //    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90445"));
                    //}
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("10030"), "", MessageBoxIcon.Question, WindowContact.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    
                    return false;
                }
                if (dt.Rows.Count < cardqty)
                {
                    //ViewState["SearchResult"] = dt;
                    //if (!IsScan)
                    //{
                    //    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("90444"), "", MessageBoxIcon.Question, WindowEnough.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    //}
                    //else
                    //{
                    //    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90446"));
                    //}
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("90464"), "", MessageBoxIcon.Question, WindowEnough.GetShowReference("~/PublicForms/Confirm.aspx"), "");

                    return false;
                }
            }

            return true;
        }

        protected void WindowContact_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            DataTable dt = (DataTable)ViewState["SearchResult"];
            if (dt != null && dt.Rows.Count > 0)
            {
                long cardqty = string.IsNullOrEmpty(this.CardCount.Text) ? 0 : Convert.ToInt64(this.CardCount.Text);
                if (dt.Rows.Count < cardqty)
                {
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("90464"), "", MessageBoxIcon.Question, WindowEnough.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    return;
                }
                
            }
            if (Convert.ToBoolean(ViewState["IsScan"]))
            {
                ShowTable();

                //查出数据后要将UID清空
                this.CardUID.Text = string.Empty;
            }
            BindCardGrid();

        }

        protected void WindowEnough_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            if (Convert.ToBoolean(ViewState["IsScan"]))
            {
                ShowTable();
            }
            BindCardGrid();

            if (Convert.ToBoolean(ViewState["IsScan"]) == true)
            {
                //查出数据后要将UID清空
                this.CardUID.Text = string.Empty;
            }
        }

        protected bool ValidataStore(Edge.SVA.Model.CardUIDMap model)
        {
            if (model == null)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return false;
            }
            Edge.SVA.BLL.CardGradeStoreCondition bll = new SVA.BLL.CardGradeStoreCondition();
            DataTable dt = bll.GetList(" CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 3 ").Tables[0];

            int i = 0;
            while (dt.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dt.Rows[i]["ConditionID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }
            Edge.SVA.BLL.Store bllstore = new SVA.BLL.Store();
            DataTable dtstore = bllstore.GetList(" BrandID in (select ConditionID from CardGradeStoreCondition where CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 1)").Tables[0];
            i = 0;
            while (dtstore.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dtstore.Rows[i]["StoreID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }

            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90404"), "CardNumber"));
            return false;
        }

        public void ShowTable()
        {
            DataTable newdt = ViewState["Newds"] as DataTable;
            DataTable showdt = new DataTable();
            if (ViewState["SearchResult"] != null)
            {
                DataTable dt = ViewState["SearchResult"] as DataTable;
                showdt = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(dt, newdt, "CardUID");
            }
            else
            {
                showdt = newdt;
            }

            ViewState["Newds"] = null;
            ViewState["SearchResult"] = showdt;
        }
    }
}