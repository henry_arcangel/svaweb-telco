﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Common;
using System.Data.SqlClient;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardIssue.ChangeDenomination
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardAdjust_H, Edge.SVA.Model.Ord_CardAdjust_H>
    {
        ChangeCardDenominationController controller = new ChangeCardDenominationController();
        //public string id = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.id = Request.Params["id"];
            this.Grid1.PageSize = webset.ContentPageNum;
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
                controller = SVASessionInfo.ChangeCardDenominationController;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                
                lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());

                lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());

                this.ApproveStatus.Text = Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                string strWhere = string.Format("Ord_CardAdjust_D.CardAdjustNumber = '{0}'", WebCommon.No_SqlHack(Model.CardAdjustNumber));

                if (Model.ApproveStatus.ToUpper().Trim() == "A") strWhere = string.Format(" Card_Movement.RefTxnNo ='{0}' and Card_Movement.OprID = '{1}' ", WebCommon.No_SqlHack(Model.CardAdjustNumber), WebCommon.No_SqlHack(Model.OprID.ToString()));

                ViewState["StrWhere"] = strWhere;
                ViewState["ApproveStatus"] = Model.ApproveStatus;

                if (this.OprID.SelectedValue == "23")
                {
                    this.OrderAmount.Enabled = false;
                    this.OrderPoints.Enabled = true;
                    this.OrderAmount.Text = string.Empty;
                }
                else
                {
                    this.OrderPoints.Enabled = false;
                    this.OrderAmount.Enabled = true;
                    this.OrderPoints.Text = string.Empty;
                }

                RptBind();

                //汇总金额
                Edge.SVA.BLL.Ord_CardAdjust_D bll = new SVA.BLL.Ord_CardAdjust_D();
                if (Model.ApproveStatus.ToUpper().Trim() == "A")
                {
                    this.lblTotalPoints.Text = controller.GetTotalPonitsWithOrd_CardMovent(strWhere).ToString();
                    this.lblTotalAmount.Text = controller.GetTotalAmountWithOrd_CardMovent(strWhere).ToString("N02");
                }
                else
                {
                    this.lblTotalPoints.Text = controller.GetTotalPonitsWithOrd_CardAdjust_D(strWhere).ToString();
                    this.lblTotalAmount.Text = controller.GetTotalAmountWithOrd_CardAdjust_D(strWhere).ToString("N2");
                }
            }
        }

        protected override SVA.Model.Ord_CardAdjust_H GetPageObject(SVA.Model.Ord_CardAdjust_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        private void RptBind()
        {
            if (ViewState["StrWhere"] != null && ViewState["ApproveStatus"] != null)
            {
                string strWhere = ViewState["StrWhere"].ToString();
                string status = ViewState["ApproveStatus"].ToString();

                DataSet ds=new DataSet();

                if (status.ToUpper().Trim() == "A")
                {
                    this.Grid1.RecordCount = controller.GetCountWithCard_Movement(strWhere);

                    ds = controller.GetPageListWithCard_Movement((this.Grid1.PageSize * (this.Grid1.PageIndex + 1)), strWhere, "Card_Movement.CardNumber");

                    ViewState["ViewTable"] = ds.Tables[0];

                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.RecordCount = controller.GetCountWithCard(strWhere);

                     ds = controller.GetPageListWithCard((this.Grid1.PageSize * (this.Grid1.PageIndex + 1)), strWhere, "Ord_CardAdjust_D.CardNumber");
                    
                    ViewState["ViewTable"] = ds.Tables[0];

                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();

                }

                this.OrderPoints.Text = ds.Tables[0].Rows[0]["OrderPoints"].ToString();
                this.OrderAmount.Text = ds.Tables[0].Rows[0]["OrderAmount"].ToString();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            RptBind();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.Ord_CardAdjust_H item = null;
            Edge.SVA.Model.Ord_CardAdjust_H dataItem = this.GetDataObject();

            //Check model
            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);

            if (this.OprID.SelectedValue == "23" && string.IsNullOrEmpty(this.OrderPoints.Text))
            {
                ShowWarning(Resources.MessageTips.YouMustInputPoints);
                return;
            }
            if (this.OprID.SelectedValue == "22" && string.IsNullOrEmpty(this.OrderAmount.Text))
            {
                ShowWarning(Resources.MessageTips.YouMustInputAmount);
                return;
            }

            if (item != null)
            {
                if (item.CardAdjustNumber.Equals(string.Empty))
                {
                    ShowUpdateFailed();
                    return;
                }
                item.UpdatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            }
            bool count = Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardAdjust_H>(item);
            if (count)
            {
                //更新Amount
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CardAdjustNumber", item.CardAdjustNumber));
                paramList.Add(new SqlParameter("@OrderAmount", this.OrderAmount.Text));
                paramList.Add(new SqlParameter("@OrderPoints", this.OrderPoints.Text));
                Edge.DBUtility.DbHelperSQL.ExecuteSql("UPDATE [Ord_CardAdjust_D] SET [OrderAmount] =@OrderAmount,[OrderPoints]=@OrderPoints WHERE [CardAdjustNumber] =@CardAdjustNumber", paramList.ToArray());

                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        
        //protected void OrderPoints_TextChanged(object sender, EventArgs e)
        //{
        //    int orderpoints = ConvertTool.ToInt(this.OrderPoints.Text);
        //    controller.ViewModel.CardTable = (DataTable)ViewState["ViewTable"];
        //    if (controller.ViewModel.CardTable != null && controller.ViewModel.CardTable.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
        //        {
        //            DataRow dr = controller.ViewModel.CardTable.Rows[i];
        //            if (orderpoints < -99999999 || orderpoints > 99999999)
        //            {
        //                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90465"));
        //                return;
        //            }
        //            if (ConvertTool.ToInt(dr["TotalPoints"].ToString()) < orderpoints)
        //            {
        //                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90467"));
        //                return;
        //            }
        //            //dr["OrderPoints"] = orderpoints;
        //            //dr["AdjustPoints"] = ConvertTool.ToInt(dr["TotalPoints"].ToString()) - orderpoints;
        //        }
        //        //RptBind(controller.ViewModel.CardTable);
        //    }
        //}
        //protected override void ConvertTextboxToDecimal(object sender, EventArgs e)
        //{
        //    base.ConvertTextboxToDecimal(sender, e);
        //    decimal orderamount = ConvertTool.ToDecimal(this.OrderAmount.Text);
        //    controller.ViewModel.CardTable = (DataTable)ViewState["ViewTable"];
        //    if (controller.ViewModel.CardTable != null && controller.ViewModel.CardTable.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
        //        {
        //            DataRow dr = controller.ViewModel.CardTable.Rows[i];
        //            if (orderamount < -99999999 || orderamount > 99999999)
        //            {
        //                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90466"));
        //                return;
        //            }
        //            if (ConvertTool.ToDecimal(dr["TotalAmount"].ToString()) < orderamount)
        //            {
        //                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90468"));
        //                return;
        //            }
        //            //dr["OrderAmount"] = orderamount;
        //            //dr["AdjustAmount"] = ConvertTool.ToDecimal(dr["TotalPoints"].ToString()) - orderamount;
        //        }
        //        //RptBind(controller.ViewModel.CardTable);
        //    }
        //}

    }
}