﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.ChangeManagement.ChangePoints.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
        <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
            EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
            <Toolbars>
                <ext:Toolbar ID="Toolbar2" runat="server">
                    <Items>
                        <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                            Text="关闭">
                        </ext:Button>
                        <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                        </ext:ToolbarSeparator>
                        <ext:Button ID="btnSaveClose" ValidateForms="from1,form2,form5" Icon="SystemSaveClose"
                            OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                        </ext:Button>
                        <ext:ToolbarFill ID="ToolbarFill3" runat="server">
                        </ext:ToolbarFill>
                    </Items>
                </ext:Toolbar>
            </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="交易信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="CreatedBy" runat="server" />
                    <ext:HiddenField ID="ApproveBy" runat="server" />
                    <ext:HiddenField ID="ApproveStatus" runat="server" Text="P" />
                    <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="TradeManuallyCode" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="BrandID" runat="server" Label="商城：" Resizable="true" 
                                        OnSelectedIndexChanged="BrandID_SelectedChanged" AutoPostBack="true"
                                        Required="true" ShowRedStar="true" CompareOperator="GreaterThan" CompareValue="-1" 
                                        CompareMessage="请选择有效的商城"></ext:DropDownList>
                                    <ext:DropDownList ID="TenderID" runat="server" Label="货币：" Resizable="true"></ext:DropDownList>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:DatePicker ID="Busdate" runat="server" Label="交易日期：" DateFormatString="yyyy-MM-dd">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label1" runat="server" HideMode="Offsets" Hidden="true"></ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="会员信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="MobileNo" runat="server" Label="手机号码：" Required="true" ShowRedStar="true"
                                        OnTextChanged="MobileNo_TextChanged" AutoPostBack="true" Enabled="false"></ext:TextBox>
                                    <ext:TextBox ID="CardNumber" runat="server" Label="卡号码：" Required="true" ShowRedStar="true"
                                        OnTextChanged="CardNumber_TextChanged" AutoPostBack="true" Enabled="false"></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="MemberRegisterMobile" runat="server" Label="完整手机号码：" Required="true" ShowRedStar="true"
                                        OnTextChanged="MemberRegisterMobile_TextChanged" AutoPostBack="true" Enabled="false"></ext:TextBox>
                                    <ext:Label ID="lbl1" runat="server" HideMode="Offsets" Hidden="true"></ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="店铺信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="form3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="form4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:TextBox ID="StoreName" runat="server" Label="店铺名称：">
                                            </ext:TextBox>
                                            <ext:Button ID="btnSearch" runat="server" Text="查找" Icon="Find" OnClick="btnSearch_Click"/>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <%--<ext:Panel ID="panel2" runat="server" Title="可选店铺" AutoScroll="true">
                                <Items>
                                    <ext:CheckBoxList ID="ckbStoreID" runat="server" ColumnNumber="3" OnSelectedIndexChanged="ckbStoreID_SelectedIndexChanged" AutoPostBack="true"/>
                                </Items>
                            </ext:Panel>--%>
                            <ext:Panel ID="panel3" runat="server" Title="可选店铺" AutoScroll="true">
                                <Items>
                                    <ext:RadioButtonList ID="rblStore" runat="server" ColumnNumber="3"></ext:RadioButtonList>
                                </Items>
                            </ext:Panel>
                            <ext:Form ID="form5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:NumberBox ID="TraderAmount" runat="server" Label="发票金额：" ShowRedStar="true"
                                                NoDecimal="false" DecimalPrecision="2">
                                            </ext:NumberBox>
                                            <ext:Button ID="btnConfirm" runat="server" Text="确定" OnClick="btnConfirm_Click">
                                            </ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="交易列表"
                AutoHeight="true" AutoWidth="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar5" runat="server" Position="Top">
                        <Items>
                            <ext:Button ID="btnDeleteResultItem" Icon="Delete" OnClick="btnDeleteResultItem_Click"
                                runat="server" Text="删除">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Grid ID="AddResultListGrid" ShowBorder="false" ShowHeader="false" AutoHeight="true"
                        PageSize="3" runat="server" EnableCheckBoxSelect="true" DataKeyNames="KeyID"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="AddResultListGrid_PageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill1" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="lblTotal" runat="server" Text="总额：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalAmount" runat="server" Text="0.00">
                                    </ext:Label>
                                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                    </ext:ToolbarSeparator>
                                    <ext:Label ID="Label4" runat="server" Text="积分：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalPoints" runat="server" Text="0">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <%--<ext:TemplateField Width="60px" HeaderText="商城">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="货币">
                                <ItemTemplate>
                                    <asp:Label ID="glblApproveStatus" runat="server" Text='<%#Eval("TendName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="交易日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("Busdate")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="店铺名称">
                                <ItemTemplate>
                                    <asp:Label ID="lblBatchCouponID" runat="server" Text='<%#Eval("StoreName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("TraderAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="积分">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("EarnPoint")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:HiddenField ID="PointsValue" runat="server"></ext:HiddenField>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="Window1_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window> 
    <ext:Window ID="Window2" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window> 
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
