﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangePoints;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using FineUI;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.ChangePoints
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_TradeManually_H, Edge.SVA.Model.Ord_TradeManually_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CardChangePointsController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                InitData();

                SVASessionInfo.CardChangePointsController = null;
            }
            controller = SVASessionInfo.CardChangePointsController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                controller.LoadViewModel(Model.TradeManuallyCode);

                if (controller.ViewModel.MainTable != null)
                {
                    this.lblCreatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                    this.ApproveStatus.Text = DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);
                    DataTable dt = controller.GetCardNumberByCase("cardno", ConvertTool.ToInt(this.BrandID.SelectedValue), controller.ViewModel.MainTable.CardNumber);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        this.MobileNo.Text = dt.Rows[0]["MemberMobilePhone"].ToString();
                        this.MemberRegisterMobile.Text = dt.Rows[0]["MemberRegisterMobile"].ToString();
                    }
                    this.BrandView.Text = this.BrandID.SelectedText;
                    this.TenderView.Text = this.TenderID.SelectedText;
                }

                if (controller.ViewModel.SubTable != null)
                {
                    BindTradeList(controller.ViewModel.SubTable);
                }
            }
        }

        protected void InitData()
        {
            ControlTool.BindBrand(this.BrandID);
            ControlTool.BindCurrency(this.TenderID);
        }

        protected void BindTradeList(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = viewDT;
                this.AddResultListGrid.DataBind();

                SummaryTrade(dt);
            }
            else
            {
                ClearGird(AddResultListGrid);
            }
        }

        private void SummaryTrade(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                decimal totalamount = Tools.ConvertTool.ConverType<decimal>(dt.Compute(" sum(TraderAmount) ", "").ToString());
                this.lblTotalAmount.Text = totalamount.ToString("N2");

                int totalpoints = Tools.ConvertTool.ConverType<int>(dt.Compute(" sum(EarnPoint) ", "").ToString());
                this.lblTotalPoints.Text = totalpoints.ToString();
            }
        }

        protected void AddResultListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            AddResultListGrid.PageIndex = e.NewPageIndex;
            BindTradeList(controller.ViewModel.SubTable);
        }
    }
}