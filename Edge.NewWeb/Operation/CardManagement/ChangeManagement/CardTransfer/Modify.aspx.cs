﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardTransfer;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardTransfer
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardTransfer_H, Edge.SVA.Model.Ord_CardTransfer_H>
    {
        CardTransferController controller = new CardTransferController();

        #region Basic Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.Lan = SVASessionInfo.SiteLanguage;
                this.Window1.Title = "搜索";
                this.WindowSearch.Title = "搜索";
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
                //Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                //Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, "CardTypeID =-1");
                //Edge.Web.Tools.ControlTool.BindBrand(Brand);
                ////ControlTool.BindCardActStatus(ActStatus);

                //btnAddSearchItem.OnClientClick = this.SearchListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                //btnDeleteResultItem.OnClientClick = this.GridCardFrom.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                // btnDeleteResultItem.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                //btnDeleteResultItem.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                InitData();
                SVASessionInfo.CardTransferController = null;
                this.GridCardFrom.EnableTextSelection = true;
                this.GridCardFrom.EnableMultiSelect = false;
                this.GridCardTo.EnableTextSelection = true;
                this.GridCardTo.EnableCheckBoxSelect = false;
            }
            controller = SVASessionInfo.CardTransferController;
            controller.Lan = SVASessionInfo.SiteLanguage;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());

                lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());

                this.lblApproveStatus.Text = Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);
                
                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                List<Edge.SVA.Model.Ord_CardTransfer_D> list = new Edge.SVA.BLL.Ord_CardTransfer_D().GetModelList(" CardTransferNumber='"+Model.CardTransferNumber+"'");
                if (list.Count>0)
                {
                    DataSet ds = controller.GetFromCard(list[0].SourceCardNumber);
                    this.GridCardFrom.PageSize = webset.ContentPageNum;
                    this.GridCardFrom.RecordCount = ds.Tables[0].Rows.Count;
                    this.GridCardFrom.DataSource = ds;
                    this.GridCardFrom.DataBind();
                    DataSet ds1 = controller.GetToCard(list[0].DestCardNumber);
                    this.GridCardTo.PageSize = webset.ContentPageNum;
                    this.GridCardTo.RecordCount = ds1.Tables[0].Rows.Count;
                    this.GridCardTo.DataSource = ds1;
                    this.GridCardTo.DataBind();                    
                }

                this.GridCardFrom.SelectedRowIndexArray = new int[] { 0 };
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.GridCardFrom.SelectedRowIndex<0)
            {
                ShowWarning("You must select one record in From Card List!");
                return;
            }
            if (this.GridCardTo.RecordCount<=0)
            {
                ShowWarning("There must be one record in To Card List!");
                return;
            }

            Edge.SVA.Model.Ord_CardTransfer_H item = this.GetAddObject();

            if (item == null)
            {
                ShowWarning("System error");
                return;
            }
            if (item.CardTransferNumber.Equals(string.Empty))
            {
                ShowAddFailed();
                return;
            }
            item.OrderType = 0;
            item.UpdatedOn = DateTime.Now;
            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.ApproveOn = null;

            controller.ViewModel.MainTable = item;
            StringBuilder sb = new StringBuilder();
            string fromCardNO = this.GridCardFrom.DataKeys[this.GridCardFrom.SelectedRowIndex][0].ToString();
            int TotalPoints = int.Parse(this.GridCardFrom.DataKeys[this.GridCardFrom.SelectedRowIndex][2].ToString());
            decimal TotalAmount = decimal.Parse(this.GridCardFrom.DataKeys[this.GridCardFrom.SelectedRowIndex][3].ToString());
            string toCardNO = this.GridCardTo.DataKeys[0][0].ToString();

            controller.ViewModel.DetailTable.SourceCardNumber = fromCardNO;
            controller.ViewModel.DetailTable.DestCardNumber = toCardNO;
            controller.ViewModel.DetailTable.ActPoints = TotalPoints;
            controller.ViewModel.DetailTable.ActAmount = TotalAmount;
            
            if (controller.Modify())
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Transfer Status  " + item.CardTransferNumber + " " + Resources.MessageTips.AddSuccess);

                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Transfer Status  " + item.CardTransferNumber + " " + Resources.MessageTips.AddFailed);

                ShowAddFailed();
            }
        }

        #endregion

        #region Basic Functions
        private void InitData()
        {
            //this.CardTransferNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderCardTranfer);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

            //btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
            //btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = GridCardFrom.RecordCount > 0 ? true : false;

            //Tools.ControlTool.BindBatchID(BatchCardID);
            InitStoreByBrand();
        }

        protected override SVA.Model.Ord_CardTransfer_H GetPageObject(SVA.Model.Ord_CardTransfer_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void SummaryAmounts(DataTable table)
        {
            //if (table.Rows.Count > 0)
            //{
            //    decimal totalDenomination = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(TotalAmount) ", "").ToString());
            //    this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
            //}
            //else
            //{
            //    this.lblTotalDenomination.Text = "0.00";
            //}
        }
        #endregion

        #region Search Functions

        //private DataTable GetSearchDataTable()
        //{
        //    Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

        //    int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
        //    int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.Text.ToString().Trim());
        //    string cardNumber = CardNumber.Text.Trim();
        //    string cardTypeID = CardTypeID.SelectedValue;
        //    //string strWhere = string.Format(" Status != {0}", (int)CardController.CardStatus.Dormant);
        //    string strWhere = string.Empty;

        //    if (!string.IsNullOrEmpty(webset.CardStatusChangeStatusEnable))
        //    {

        //        strWhere = string.Format(" Card.Status in ( {0} )", webset.CardStatusChangeStatusEnable);
        //    }
        //    else
        //    {
        //        strWhere = string.Format(" Card.Status in ( {0} )", "-1");
        //    }
        //    string filedOrder = " Card.CardNumber ASC ";

        //    if (string.IsNullOrEmpty(cardTypeID))
        //    {
        //        //cardTypeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 1);
        //    }

        //    strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardTypeID, this.CardUID.Text.Trim(), strWhere);

        //    //Display message
        //    int count = bll.GetCount(strWhere);

        //    if (count <= 0)
        //    {
        //        //this.JscriptPrint(Messages.Manager.MessagesTool.instance.GetMessage("90180"), "", Resources.MessageTips.WARNING_TITLE);
        //        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
        //        return null;
        //    }


        //    if ((top > webset.MaxSearchNum) || ((count > webset.MaxSearchNum) && top <= 0))
        //    {
        //        top = webset.MaxSearchNum;
        //        ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
        //        // this.JscriptPrint(Resources.MessageTips.IsMaxSearchLimit, "", Resources.MessageTips.WARNING_TITLE);
        //    }

        //    DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);

        //    //Edge.Web.Tools.DataTool.AddCardUID(ds, "CardUID", "CardNumber");
        //    //Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
        //    //Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
        //    //Tools.DataTool.AddBatchCode(ds, "BatchCode", "BatchCardID");

        //    return ds.Tables[0];
        //}

        ////绑定搜索结果列表
        //private void BindSearchList()
        //{
        //    if (ViewState["SearchResult"] != null)
        //    {
        //        this.SearchListGrid.PageSize = webset.ContentPageNum;
        //        DataTable dt = (DataTable)ViewState["SearchResult"];
        //        this.SearchListGrid.RecordCount = dt.Rows.Count;
        //        DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
        //        this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
        //        this.SearchListGrid.DataBind();
        //    }
        //    else
        //    {
        //        this.SearchListGrid.PageSize = webset.ContentPageNum;
        //        this.SearchListGrid.PageIndex = 0;
        //        this.SearchListGrid.RecordCount = 0;
        //        this.SearchListGrid.DataSource = null;
        //        this.SearchListGrid.DataBind();
        //    }

        //    btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
        //}

        ////绑定待添加结果列表
        //private void BindResultList()
        //{
        //    if (ViewState["AddResult"] != null)
        //    {
        //        this.GridCardFrom.PageSize = webset.ContentPageNum;
        //        DataTable dt = (DataTable)ViewState["AddResult"];
        //        this.GridCardFrom.RecordCount = dt.Rows.Count;
        //        DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.GridCardFrom.PageIndex + 1, this.GridCardFrom.PageSize);
        //        this.GridCardFrom.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
        //        this.GridCardFrom.DataBind();

        //        SummaryAmounts(dt);

        //    }
        //    else
        //    {
        //        this.GridCardFrom.PageSize = webset.ContentPageNum;
        //        this.GridCardFrom.PageIndex = 0;
        //        this.GridCardFrom.RecordCount = 0;
        //        this.GridCardFrom.DataSource = null;
        //        this.GridCardFrom.DataBind();
        //    }

        //    //btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = GridCardFrom.RecordCount > 0 ? true : false;

        //    //this.Brand.Enabled = this.StoreID.Enabled = GridCardFrom.RecordCount > 0 ? false : true;
        //}

        //private void AddItem()
        //{
        //    if (ViewState["SearchResult"] != null)
        //    {
        //        if (ViewState["AddResult"] == null)
        //        {
        //            ViewState["AddResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
        //        }
        //        DataTable addDTView = (DataTable)ViewState["AddResult"];
        //        if (addDTView.DefaultView.Count >= webset.MaxShowNum)
        //        {
        //            ShowWarning(Resources.MessageTips.IsMaximumLimit);
        //            return;
        //        }

        //        DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

        //        if (!cbSearchAll.Checked)
        //        {
        //            string ids = "";

        //            foreach (int row in SearchListGrid.SelectedRowIndexArray)
        //            {
        //                ids += string.Format("{0},", "'" + SearchListGrid.DataKeys[row][0].ToString() + "'");
        //            }

        //            ids = ids.TrimEnd(',');

        //            if (string.IsNullOrEmpty(ids.Trim()))
        //            {
        //                ShowWarning(Resources.MessageTips.NotSelected);
        //                return;
        //            }
        //            DataTable vsDT = (DataTable)ViewState["SearchResult"];
        //            DataView dvSearch = vsDT.DefaultView;
        //            dvSearch.RowFilter = "CardNumber in (" + ids + ")";
        //            dtSearch = dvSearch.ToTable();
        //            foreach (DataRowView drv in dvSearch)
        //            {
        //                drv.Delete();
        //            }
        //            vsDT.AcceptChanges();
        //            ViewState["SearchResult"] = vsDT;
        //        }
        //        else
        //        {
        //            dtSearch = (DataTable)ViewState["SearchResult"];
        //            ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
        //            cbSearchAll.Checked = false;
        //        }

        //        DataTable addDT = (DataTable)ViewState["AddResult"];
        //        DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");
        //        ViewState["AddResult"] = newSearchDT;

        //        BindResultList();
        //        BindSearchList();
        //    }
        //}

        private void DeleteItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = (DataTable)ViewState["AddResult"];

            //    foreach (int row in GridCardFrom.SelectedRowIndexArray)
            //    {
            //        string cardNumber = GridCardFrom.DataKeys[row][0].ToString();
            //        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
            //        {
            //            if (addDT.Rows[j]["CardNumber"].ToString().Trim() == cardNumber)
            //            {
            //                addDT.Rows.Remove(addDT.Rows[j]);
            //            }
            //        }
            //        addDT.AcceptChanges();
            //    }

            //    ViewState["AddResult"] = addDT;
            //    BindResultList();
            //}
            //转移界面删除代码
            //if (controller.ViewModel.CardTable != null)
            //{
            //    DataTable addDT = controller.ViewModel.CardTable;

            //    foreach (int row in GridCardFrom.SelectedRowIndexArray)
            //    {
            //        string cardNumber = GridCardFrom.DataKeys[row][0].ToString();
            //        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
            //        {
            //            if (addDT.Rows[j]["CardNumber"].ToString().Trim() == cardNumber)
            //            {
            //                addDT.Rows.Remove(addDT.Rows[j]);
            //            }
            //        }
            //        addDT.AcceptChanges();
            //    }

            //    controller.ViewModel.CardTable = addDT;
            //    BindResultList(controller.ViewModel.CardTable);
            //}
        }

        private void DeleteAllItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = ((DataTable)ViewState["AddResult"]).Clone();
            //    ViewState["AddResult"] = addDT;
            //    BindResultList();
            //}
            //删除界面代码
            //if (controller.ViewModel.CardTable != null)
            //{
            //    DataTable addDT = controller.ViewModel.CardTable.Clone();
            //    controller.ViewModel.CardTable = addDT;
            //    BindResultList(controller.ViewModel.CardTable);
            //}
        }

        private void InitSearchList()
        {
            ViewState["SearchResult"] = null;
            //BindSearchList();
        }

        private void InitResultList()
        {
            //老界面代码
            //ViewState["AddResult"] = null;
            //BindResultList();
            //删除界面代码
            //controller.ViewModel.CardTable = null;
            //BindResultList(controller.ViewModel.CardTable);
        }
        #endregion

        #region Search Events

        protected void btnViewSearch_Click(object sender, EventArgs e)
        {
            //InitSearchList();
            //this.Window1.Hidden = false;

            //转移界面
            //ExecuteJS(WindowSearch.GetShowReference(string.Format("Card/Add.aspx?BrandID={0}&StoreID={1}", this.Brand.SelectedValue, this.StoreID.SelectedValue)));

        }

        protected void btnSearchCardInfo_Click(object sender, EventArgs e)
        {
            string mobileno = this.txtMobileNO.Text.Trim();
            string email = this.txtEmail.Text.Trim();
            string cardNO = this.txtCardNumber.Text.Trim();
            if (string.IsNullOrEmpty(mobileno + email + cardNO))
            {
                ShowWarning("Please input one of MobileNO/Email/CardNumber at least!");
                return;
            }
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(mobileno))
            {
                dic.Add("m.MemberMobilePhone", mobileno);
            }
            if (!string.IsNullOrEmpty(email))
            {
                dic.Add("m.MemberEmail", email);
            }
            if (!string.IsNullOrEmpty(cardNO))
            {
                dic.Add("c.CardNumber", cardNO);
            }
            DataSet ds = controller.GetFromList(dic);
            this.GridCardFrom.PageSize = webset.ContentPageNum;
            this.GridCardFrom.RecordCount = ds.Tables[0].Rows.Count;
            DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(ds.Tables[0], this.GridCardFrom.PageIndex + 1, this.GridCardFrom.PageSize);
            this.GridCardFrom.DataSource = viewDT;
            this.GridCardFrom.DataBind();
        }

        protected void btnCloseSearch_Click(object sender, EventArgs e)
        {
            InitSearchList();
            this.Window1.Hidden = true;
        }

        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            //AddItem();
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            //SearchListGrid.PageIndex = e.NewPageIndex;
            //BindSearchList();
        }

        protected void GridCardFrom_PageIndexChange(object sender, GridPageEventArgs e)
        {
            GridCardFrom.PageIndex = e.NewPageIndex;
            btnSearchCardInfo_Click(null, null);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (CardTypeID.SelectedValue != "-1")
            //{
            //    Tools.ControlTool.BindBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
            //}
            //else
            //{
            //    Tools.ControlTool.BindBatchID(BatchCardID);
            //}

        }

        protected void cbSearchAll_CheckedChanged(object sender, EventArgs e)
        {
            //if ((!this.cbSearchAll.Checked) && this.rptSearchList.Items.Count > 0)
            //{
            //    this.btnAddItem.Visible = true;
            //}
            //else
            //{
            //    this.btnAddItem.Visible = false;
            //}
            //this.lbtnDeleteIssued.Visible = !this.cbSearchAll.Checked;
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            //老界面
            //base.WindowEdit_Close(sender, e);
            //InitSearchList();

            base.WindowEdit_Close(sender, e);
            //BindResultList(controller.ViewModel.CardTable);
        }
        #endregion

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
            InitCardTypeByStore();
            InitSearchList();
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitCardTypeByStore();
            InitSearchList();
        }
        private void InitStoreByBrand()
        {
            //Edge.Web.Tools.ControlTool.BindStoreWithBrand(StoreID, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }
        private void InitCardTypeByStore()
        {
            //0703
            //if (!string.IsNullOrEmpty(this.StoreID.SelectedValue))
            //    Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, "CardTypeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 2) + ") order by CardTypeCode");
            //else
            //    Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, " 1>2");
        }

        //绑定添加结果（新）
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.GridCardFrom.PageSize = webset.ContentPageNum;
                //DataTable dt = (DataTable)ViewState["AddResult"];
                this.GridCardFrom.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.GridCardFrom.PageIndex + 1, this.GridCardFrom.PageSize);
                this.GridCardFrom.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.GridCardFrom.DataBind();

                SummaryAmounts(dt);
            }
            else
            {
                this.GridCardFrom.PageSize = webset.ContentPageNum;
                this.GridCardFrom.PageIndex = 0;
                this.GridCardFrom.RecordCount = 0;
                this.GridCardFrom.DataSource = null;
                this.GridCardFrom.DataBind();
            }

            //this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = GridCardFrom.RecordCount > 0 ? true : false;
            //this.Brand.Enabled = this.StoreID.Enabled = GridCardFrom.RecordCount > 0 ? false : true;
        }
        protected void GridCardFrom_RowClick(object sender, FineUI.GridRowClickEventArgs e)
        {
            if (this.GridCardTo.RecordCount>0)
            {
                return;
            }
            if (this.GridCardFrom.SelectedRowIndex < 0)
            {
                return;
            }
            string cardGradeID = this.GridCardFrom.DataKeys[this.GridCardFrom.SelectedRowIndex][1].ToString();
            DataSet ds = controller.GetToList(cardGradeID);
            this.GridCardTo.PageSize = webset.ContentPageNum;
            this.GridCardTo.RecordCount = ds.Tables[0].Rows.Count;
            this.GridCardTo.DataSource = ds;
            this.GridCardTo.DataBind();
        }
    }
}
