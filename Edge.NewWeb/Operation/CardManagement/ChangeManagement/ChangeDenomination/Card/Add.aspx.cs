﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination;
using Edge.Web.Tools;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using FineUI;
using System.Text;
using System.IO;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.ChangeDenomination.Card
{
    public partial class Add : PageBase
    {
        ChangeCardDenominationController controller = new ChangeCardDenominationController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                InitData();
                RegisterCloseEvent(this.btnCloseSearch);
            }
            controller = SVASessionInfo.ChangeCardDenominationController;
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();
            List<string> idList = GetSelectedRowIndexArrayFromHiddenField();

            if (ViewState["SearchResult"] != null)
            {
                if (controller.ViewModel.CardTable == null)
                {
                    controller.ViewModel.CardTable = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = controller.ViewModel.CardTable;
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }
               
                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    for (int i = 0; i < idList.Count; i++)
                    {
                        ids += string.Format("{0},", "'" + idList[i].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    foreach (DataRowView drv in dvSearch)
                    {
                        drv.Delete();
                    }
                    vsDT.AcceptChanges();
                    ViewState["SearchResult"] = vsDT;
                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];
                    ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                    cbSearchAll.Checked = false;
                }

                DataTable addDT = controller.ViewModel.CardTable;
                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");

                //校验选好后的卡状态，卡积分，卡余额
                if (newSearchDT != null)
                {
                    for (int i = 0; i < newSearchDT.Rows.Count; i++)
                    {
                        DataRow dr = newSearchDT.Rows[i];
                        if (ConvertTool.ToInt(dr["Status"].ToString()) != 1 && ConvertTool.ToInt(dr["Status"].ToString()) != 2)
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90478"));
                            return;
                        }
                    }
                }
                
                controller.ViewModel.CardTable = newSearchDT;

                //返回界面时要清空查询记录
                ViewState["SearchResult"] = null;
            }
            CloseAndPostBack();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);
            DataTable dt = null;
            if (this.SearchType.SelectedValue == "0")
            {
                if (!ValidaData())
                {
                    return;
                }
                dt = GetSearchDataTable();
            }
            else
            {
                dt = GetImportBindTable();
            }
            ViewState["SearchResult"] = dt;
            this.SearchListGrid.PageIndex = 0;
            BindCardGrid();
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardGradeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardGradeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindBatchID(BatchCardID);
            }
        }

        protected void SearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SearchType.SelectedValue == "1")
            {
                this.CardTypeID.Enabled = false;
                this.CardGradeID.Enabled = false;
                this.BatchCardID.Enabled = false;
                this.CardUID.Enabled = false;
                this.CardCount.Enabled = false;
                this.CardNumber.Enabled = false;
                this.CountryCode.Enabled = false;
                this.MemberMobilePhone.Enabled = false;
                this.MemberEmail.Enabled = false;
                this.ImportFile.Enabled = true;
                ClearGird(this.SearchListGrid);
            }
            else
            {
                this.CardTypeID.Enabled = true;
                this.CardGradeID.Enabled = true;
                this.BatchCardID.Enabled = true;
                this.CardUID.Enabled = true;
                this.CardCount.Enabled = true;
                this.CardNumber.Enabled = true;
                this.CountryCode.Enabled = true;
                this.MemberMobilePhone.Enabled = true;
                this.MemberEmail.Enabled = true;
                this.ImportFile.Enabled = false;
            }
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindCardGrid();
        }

        private DataTable GetSearchDataTable()
        {
            try
            {
                DataTable dt = new DataTable();
                int cardtypeid = ConvertTool.ConverType<int>(CardTypeID.SelectedValue.Trim());
                int cardgradeid = ConvertTool.ConverType<int>(CardGradeID.SelectedValue == null ? "-1" : CardGradeID.SelectedValue.Trim());
                int batchCouponID = ConvertTool.ConverType<int>(BatchCardID.Text.ToString().Trim());
                string carduid = this.CardUID.Text;
                int top = ConvertTool.ConverType<int>(CardCount.Text.Trim());
                string cardnumber = this.CardNumber.Text;
                string countrycode = this.CountryCode.SelectedValue;
                string mobile = this.MemberMobilePhone.Text;
                string email = this.MemberEmail.Text;

                StringBuilder sb = new StringBuilder();
                sb.Append(" (Card.Status=1 or Card.Status=2)");
                if (cardtypeid != -1)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" Card.CardTypeID =" + cardtypeid);
                }

                if (cardgradeid != -1)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("CardGrade.CardGradeID=" + cardgradeid);
                }
                if (batchCouponID != 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("Card.BatchCardID=" + batchCouponID);
                }
                if (carduid != "")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    if (top == 0)
                    {
                        sb.Append("CardUIDMap.CardUID='" + carduid + "'");
                    }
                    else
                    {
                        sb.Append("CardUIDMap.CardUID >='" + carduid + "'");
                    }
                }
                if (cardnumber != "")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    if (top == 0)
                    {
                        sb.Append("Card.CardNumber='" + cardnumber + "'");
                    }
                    else
                    {
                        sb.Append("Card.CardNumber>='" + cardnumber + "'");
                    }
                }
                if (countrycode != "-1")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("Member.CountryCode='" + countrycode + "'");
                }
                if (mobile != "")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("Member.MemberMobilePhone='" + mobile + "'");
                }
                if (email != "")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append("Member.MemberEmail='" + email + "'");
                }

                string filedOrder = " Card.CardNumber ASC ";

                dt = controller.GetCardSearchTable(top, sb.ToString(), filedOrder);

                if (dt.Rows.Count == 0)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return null;
                }

                if ((top > webset.MaxSearchNum) || ((dt.Rows.Count > webset.MaxSearchNum) && top <= 0))
                {
                    top = webset.MaxSearchNum;
                    ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                }
                return dt;
            }
            catch
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
        }

        private void BindCardGrid()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }

            btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
        }

        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            BindCardGrid();

            UpdateSelectedRowIndexArray();
            if (cbSearchAll.Checked)
            {
                SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            }
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            if (SearchListGrid.SelectedRowIndexArray != null && SearchListGrid.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(SearchListGrid.SelectedRowIndexArray);
                for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
                {
                    string id = SearchListGrid.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
            {
                string id = SearchListGrid.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i);
                }
            }
            SearchListGrid.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion
        protected void cbSearchAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            if (!cbSearchAll.Checked)
            {
                hfSelectedIDS.Text = string.Empty;
            }
        }

        protected bool ValidaData()
        {
            int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
            int BatchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(this.BatchCardID.SelectedValue);
            string CardNumber = this.CardNumber.Text.Trim();
            string CardTypeID = this.CardTypeID.SelectedValue;

            if ((top <= 0) && (BatchCardID <= 0) && string.IsNullOrEmpty(CardNumber) && CardTypeID == "-1" && string.IsNullOrEmpty(this.CardUID.Text.Trim()) && this.CountryCode.SelectedValue == "-1" && string.IsNullOrEmpty(this.MemberMobilePhone.Text) && string.IsNullOrEmpty(this.MemberEmail.Text))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }

            if (top > webset.MaxSearchNum)
            {

                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                return false;
            }
            if (top > 0 && CardNumber != "" && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
                else
                {
                    if (CardNumber != model.CardNumber)
                    {
                        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                        return false;
                    }
                }
            }
            if (top > 0 && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            if (top > 0 && this.CardNumber.Text != "")
            {
                Edge.SVA.BLL.Card bll1 = new SVA.BLL.Card();
                Edge.SVA.Model.Card model = bll1.GetModel(this.CardNumber.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            return true;
        }

        public void InitData()
        {
            ControlTool.BindCardType(CardTypeID);
            BindCountry(CountryCode);

            this.CardTypeID.Enabled = true;
            this.CardGradeID.Enabled = true;
            this.BatchCardID.Enabled = true;
            this.CardUID.Enabled = true;
            this.CardCount.Enabled = true;
            this.CardNumber.Enabled = true;
            this.CountryCode.Enabled = true;
            this.MemberMobilePhone.Enabled = true;
            this.MemberEmail.Enabled = true;
            this.ImportFile.Enabled = false;
        }

        public void BindCountry(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = ds = new Edge.SVA.BLL.Nation().GetAllList();
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    FineUI.ListItem li = new FineUI.ListItem() { Value = dr["CountryCode"].ToString().Trim(), Text = dr["CountryCode"].ToString().Trim() };
                    ddl.Items.Add(li);
                }
            }
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }

        #region 导入数据
        public DataTable GetImportData()
        {
            string pathFile = this.ImportFile.SaveToServer("ImportCardFile");
            string path = Server.MapPath("~" + pathFile);
            DataTable dt = ExcelTool.GetFirstSheet(path);
            ViewState["ImportTable"] = dt;
            return dt;
        }
        public bool ValidaImportData()
        {
            if (string.IsNullOrEmpty(this.ImportFile.ShortFileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            string filename = Path.GetExtension(this.ImportFile.ShortFileName).TrimStart('.');
            if (filename.ToLower() != "xls")
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90446"));
                return false;
            }
            DataTable dt = GetImportData();
            if (dt != null)
            {
                string strwhere = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (string.IsNullOrEmpty(dr["CountryCode"].ToString()))
                    {
                        ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90469"), i + 1));
                        return false;
                    }
                    if (string.IsNullOrEmpty(dr["MobileNumber"].ToString()))
                    {
                        ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90470"), i + 1));
                        return false;
                    }
                    strwhere = @" Member.CountryCode='" + dr["CountryCode"].ToString() + "' and Member.MemberMobilePhone='" + dr["MobileNumber"].ToString() + "'";
                    DataTable searchdt = controller.GetCardSearchTable(0, strwhere, " Card.CardNumber ASC ");
                    if (searchdt != null && searchdt.Rows.Count > 0)
                    {
                        DataRow newdr = searchdt.Rows[0];
                        if (ConvertTool.ToInt(newdr["Status"].ToString()) != 1 && ConvertTool.ToInt(newdr["Status"].ToString()) != 2)
                        {
                            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90472"), i + 1));
                            return false;
                        }
                        if (newdr["CardNumber"] == null || newdr["CardNumber"].ToString() == string.Empty)
                        {
                            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90180")));
                            return false;
                        }

                    }
                    else
                    {
                        ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90180")));
                        return false;
                    }
                }
            }
            else
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return false;
            }
            
            return true;
        }

        public DataTable GetImportBindTable()
        {
            DataTable ViewTable = new DataTable();
            if (ValidaImportData())
            {
                DataTable dt = (DataTable)ViewState["ImportTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string strwhere = @" Member.CountryCode='" + dr["CountryCode"].ToString() + "' and Member.MemberMobilePhone='" + dr["MobileNumber"].ToString() + "'";
                    DataTable searchdt = controller.GetCardSearchTable(0, strwhere, " Card.CardNumber ASC ");
                    DataRow searchdr = searchdt.Rows[0];
                    if (i == 0)
                    {
                        ViewTable = searchdt.Clone();
                    }
                    ViewTable.Rows.Add(searchdr.ItemArray);
                }
            }
            return ViewTable;
        }
        #endregion
    }
}