﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Edge.Web.Controllers;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeExpiryDate;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.ChangeExpiryDate.Card
{
    public partial class Add : PageBase
    {
        ChangeCardExpiryDateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterCloseEvent(this.btnCloseSearch);
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                CardTypeID_SelectedIndexChanged(null, null);
                //CardGradeID_SelectedIndexChanged(null, null);
                //if (!string.IsNullOrEmpty(Request["StoreID"]))
                //{
                //    Edge.Web.Tools.ControlTool.BindCardTypeWithoutBrand(CardTypeID, "CardTypeID in (" + Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") order by CardTypeCode");
                //}
                //else
                //{
                //    Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, " 1>2");
                //}

                Tools.ControlTool.BindCardBatchID(BatchCardID);

            }
            controller = SVASessionInfo.ChangeCardExpiryDateController;
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();
            List<string> idList = GetSelectedRowIndexArrayFromHiddenField();

            if (ViewState["SearchResult"] != null)
            {
                if (controller.ViewModel.CardTable == null)
                {
                    controller.ViewModel.CardTable = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = controller.ViewModel.CardTable;
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    for (int i = 0; i < idList.Count; i++)
                    {
                        ids += string.Format("{0},", "'" + idList[i].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    foreach (DataRowView drv in dvSearch)
                    {
                        drv.Delete();
                    }
                    vsDT.AcceptChanges();
                    ViewState["SearchResult"] = vsDT;
                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];
                    ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                    cbSearchAll.Checked = false;
                }

                DataTable addDT = controller.ViewModel.CardTable;
                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");
                controller.ViewModel.CardTable = newSearchDT;

                //返回界面时要清空查询记录
                ViewState["SearchResult"] = null;
            }
            CloseAndPostBack();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearGird(this.SearchListGrid);
            if (!ValidaData())
            {
                return;
            }

            Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            Edge.SVA.Model.CardUIDMap modelmap = null;
            DataSet ds = new DataSet();
            //校验不能在此店铺激活的CardNumber
            if (this.CardNumber.Text != "" && this.CardUID.Text == "")
            {
                ds = bll1.GetList(" CardNumber = '" + this.CardNumber.Text + "'");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {

                    modelmap = bll1.GetModel(ds.Tables[0].Rows[0]["CardUID"].ToString());
                    if (!ValidataStore(modelmap)) return;
                }
            }
            //校验不能在此店铺激活的CardUID
            if (this.CardNumber.Text == "" && this.CardUID.Text != "")
            {
                modelmap = bll1.GetModel(this.CardUID.Text);

                if (modelmap != null)
                {
                    if (!ValidataStore(modelmap)) return;
                }
            }

            DataTable dt = GetSearchDataTable();
            ViewState["SearchResult"] = dt;

            if (!IsContinue(GetSearchDataTable()))
            {
                return;
            }
            this.SearchListGrid.PageIndex = 0;
            //DataTable dt = GetSearchDataTable();
            //ViewState["SearchResult"] = dt;
            BindCardGrid();
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                //Tools.ControlTool.BindCardGrade(CardGradeID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
                Tools.ControlTool.BindCardGradeWithoutBrand(CardGradeID, "CardGradeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") and CardTypeID='" + CardTypeID.SelectedValue + "' order by CardGradeCode");

            }
            else
            {
                //Tools.ControlTool.BindCardGrade(CardGradeID);
                Tools.ControlTool.BindCardGradeWithoutBrand(CardGradeID, "CardGradeID in (" + Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1) + ") order by CardGradeCode");
            }
        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardGradeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardGradeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindCardBatchID(BatchCardID);
            }
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindCardGrid();
        }

        private DataTable GetSearchDataTable()
        {
            try
            {
                Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

                int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
                int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.SelectedValue.ToString().Trim());
                string cardNumber = CardNumber.Text.Trim();
                string cardGradeID = CardGradeID.SelectedValue;
                string strWhere = string.Empty;
                if (!string.IsNullOrEmpty(webset.CardExpiryDateStatusEnable))
                {

                    strWhere = string.Format(" Card.Status in ( {0} )", webset.CardExpiryDateStatusEnable);
                }
                else
                {
                    strWhere = string.Format(" Card.Status in ( {0} )", "-1");
                }
                string filedOrder = " Card.CardNumber ASC ";

                if (cardGradeID == "-1")
                {
                    cardGradeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
                }

                //strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardTypeID, this.CardUID.Text.Trim(), strWhere);
                if (top > 0 && cardNumber == "" && this.CardUID.Text != "")
                {
                    Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                    Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                    cardNumber = model.CardNumber;
                    cardGradeID = model.CardGradeID.ToString();
                    strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardGradeID, "", strWhere);
                }
                else if (top > 0 && cardNumber != "" && this.CardUID.Text != "")
                {
                    strWhere = GetCardSearchStrWhere(top, batchCardID, this.CardNumber.Text, cardGradeID, "", strWhere);
                }
                else
                {
                    if (this.CardNumber.Text != "" && this.CardGradeID.SelectedValue == "-1")
                    {
                        Edge.SVA.Model.Card model = bll.GetModel(this.CardNumber.Text);
                        cardGradeID = model == null ? "-1" : model.CardGradeID.ToString();
                    }
                    strWhere = GetCardSearchStrWhere(top, batchCardID, cardNumber, cardGradeID, this.CardUID.Text.Trim(), strWhere);
                }
                //Display message
                int count = bll.GetCount(strWhere);

                if (count <= 0)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return null;
                }


                if ((top > webset.MaxSearchNum) || ((count > webset.MaxSearchNum) && top <= 0))
                {
                    top = webset.MaxSearchNum;
                    ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                }

                DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);

                return ds.Tables[0];
            }
            catch
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
        }

        private void BindCardGrid()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }

            btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
        }

        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            BindCardGrid();

            UpdateSelectedRowIndexArray();
            if (cbSearchAll.Checked)
            {
                SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            }
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            if (SearchListGrid.SelectedRowIndexArray != null && SearchListGrid.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(SearchListGrid.SelectedRowIndexArray);
                for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
                {
                    string id = SearchListGrid.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            for (int i = 0, count = Math.Min(SearchListGrid.PageSize, (SearchListGrid.RecordCount - SearchListGrid.PageIndex * SearchListGrid.PageSize)); i < count; i++)
            {
                string id = SearchListGrid.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i);
                }
            }
            SearchListGrid.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion
        protected void cbSearchAll_OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetGirdSelectAll(SearchListGrid, cbSearchAll.Checked);
            if (!cbSearchAll.Checked)
            {
                hfSelectedIDS.Text = string.Empty;
            } 
        }

        protected bool ValidaData()
        {
            int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
            int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.SelectedValue);
            string cardNumber = CardNumber.Text.Trim();
            string cardGradeID = CardGradeID.SelectedValue;

            if ((top <= 0) && (batchCardID <= 0) && string.IsNullOrEmpty(cardNumber) && cardGradeID == "-1" && string.IsNullOrEmpty(this.CardUID.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return false;
            }

            if (top > webset.MaxSearchNum)
            {

                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                return false;
            }

            if (top > 0 && cardNumber != "" && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
                else
                {
                    if (cardNumber != model.CardNumber)
                    {
                        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                        return false;
                    }
                }
            }
            //if (top > 0 && this.CardUID.Text != "" && cardTypeID == "-1")
            //{
            //    string cardTypeids = Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
            //    List<string> idlist = new List<string>();
            //    foreach (var item in cardTypeids.Split(','))
            //    {
            //        idlist.Add(item);
            //    }

            //    Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
            //    Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
            //    if (model == null)
            //    {
            //        return false;
            //    }
            //    int i = 0;
            //    while (idlist.Count > i)
            //    {
            //        if (idlist[i] != model.CardTypeID.ToString())
            //        {
            //            i++;
            //        }
            //        else
            //        {
            //            return true;
            //        }
            //    }
            //    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
            //    return false;

            //}
            //if (top > 0 && this.CardNumber.Text != "" && cardTypeID == "-1")
            //{
            //    string cardTypeids = Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(Request["StoreID"]), 1);
            //    List<string> idlist = new List<string>();
            //    foreach (var item in cardTypeids.Split(','))
            //    {
            //        idlist.Add(item);
            //    }

            //    Edge.SVA.BLL.Card bll1 = new SVA.BLL.Card();
            //    Edge.SVA.Model.Card model = bll1.GetModel(this.CardNumber.Text);
            //    if (model == null)
            //    {
            //        return false;
            //    }
            //    int i = 0;
            //    while (idlist.Count > i)
            //    {
            //        if (idlist[i] != model.CardTypeID.ToString())
            //        {
            //            i++;
            //        }
            //        else
            //        {
            //            return true;
            //        }
            //    }
            //    if (model == null)
            //    {
            //        ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
            //        return false;
            //    }
            //}
            if (top > 0 && this.CardUID.Text != "")
            {
                Edge.SVA.BLL.CardUIDMap bll1 = new SVA.BLL.CardUIDMap();
                Edge.SVA.Model.CardUIDMap model = bll1.GetModel(this.CardUID.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            if (top > 0 && this.CardNumber.Text != "")
            {
                Edge.SVA.BLL.Card bll1 = new SVA.BLL.Card();
                Edge.SVA.Model.Card model = bll1.GetModel(this.CardNumber.Text);
                if (model == null)
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                    return false;
                }
            }
            return true;
        }

        protected bool IsContinue(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                //ViewState["SearchResult"] = dt;

                Edge.SVA.BLL.CardGrade cardtype = new SVA.BLL.CardGrade();
                int j = dt.Rows.Count - 1;
                string firstnumber = dt.Rows[0]["CardNumber"].ToString();
                string lastnumber = dt.Rows[j]["CardNumber"].ToString();
                string cardGradeID = dt.Rows[0]["CardGradeID"].ToString();
                Edge.SVA.Model.CardGrade model = cardtype.GetModel(Convert.ToInt32(cardGradeID));
                int startindex = model.CardNumPattern == null ? 0 : model.CardNumPattern.Length;
                int digit = 0;
                if (model.CardCheckdigit == 1)
                {
                    digit = 1;
                }

                long startcard = Convert.ToInt64(firstnumber.Substring(startindex, firstnumber.Length - startindex - digit));
                long endcard = Convert.ToInt64(lastnumber.Substring(startindex, lastnumber.Length - startindex - digit));

                if (endcard - startcard != j)
                {
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("10030"), "", MessageBoxIcon.Question, WindowContact.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    return false;
                }

                long cardqty = string.IsNullOrEmpty(this.CardCount.Text) ? 0 : Convert.ToInt64(this.CardCount.Text);
                if (dt.Rows.Count < cardqty)
                {
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("90444"), "", MessageBoxIcon.Question, WindowEnough.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    return false;
                }
            }

            return true;
        }

        protected void WindowContact_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            DataTable dt = (DataTable)ViewState["SearchResult"];
            if (dt != null && dt.Rows.Count > 0)
            {
                long cardqty = string.IsNullOrEmpty(this.CardCount.Text) ? 0 : Convert.ToInt64(this.CardCount.Text);
                if (dt.Rows.Count < cardqty)
                {
                    ShowConfirmDialog(Messages.Manager.MessagesTool.instance.GetMessage("90444"), "", MessageBoxIcon.Question, WindowEnough.GetShowReference("~/PublicForms/Confirm.aspx"), "");
                    return;
                }
                BindCardGrid();
            }
        }

        protected void WindowEnough_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            BindCardGrid();
        }

        protected bool ValidataStore(Edge.SVA.Model.CardUIDMap model)
        {
            if (model == null)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return false;
            }
            Edge.SVA.BLL.CardGradeStoreCondition bll = new SVA.BLL.CardGradeStoreCondition();
            DataTable dt = bll.GetList(" CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 3 ").Tables[0];

            int i = 0;
            while (dt.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dt.Rows[i]["ConditionID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }
            Edge.SVA.BLL.Store bllstore = new SVA.BLL.Store();
            DataTable dtstore = bllstore.GetList(" BrandID in (select ConditionID from CardGradeStoreCondition where CardGradeID = " + model.CardGradeID + " and StoreConditionType = 1 and ConditionType = 1)").Tables[0];
            i = 0;
            while (dtstore.Rows.Count > i)
            {
                if (Request["StoreID"].ToString() != dtstore.Rows[i]["StoreID"].ToString())
                {
                    i++;
                }
                else
                {
                    return true;
                }
            }

            ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90404"), "CardNumber"));
            return false;
        }

    }
}