﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery
{
    public partial class AddParentChild : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardParentChild, Edge.SVA.Model.CardParentChild>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                InitData();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            Model = this.GetAddObject();

            Edge.SVA.BLL.CardParentChild bll = new SVA.BLL.CardParentChild();
            if (Model != null)
            {
                if (!bll.Exists(Model.ParentCardNumber, Model.ChildCardNumber))
                {
                    bll.Add(Model);
                    CloseAndRefresh();
                }
                else
                {
                    ShowAddFailed();
                    return;
                }
            }
            else
            {
                ShowAddFailed();
                return;
            }
        }

        protected void InitData()
        {
            this.ParentCardNumber.Text = Request.Params["id"].ToString();
        }
    }
}