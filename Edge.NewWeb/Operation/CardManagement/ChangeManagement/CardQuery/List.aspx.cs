﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery
{
    public partial class List : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.ResultGrid.PageSize = webset.ContentPageNum;

                ControlTool.BindCardType(CardTypeID);
                ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindCardStatus(Status);
                ControlTool.BindCardStockStatus(StockStatus);
                this.BindBatchID(CardBatchID);
                Edge.Web.Tools.ControlTool.BindBrand(Brand);
                InitStoreByBrand();
            }

        }

        public void BindBatchID(FineUI.DropDownList ddl, int CardGradeType)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCard().GetBatchIDByType(CardGradeType);
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        public void BindBatchID(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.BatchCard().GetBatchID();
            ddl.DataSource = ds.Tables[0];
            ddl.DataTextField = "BatchCardCode";
            ddl.DataValueField = "BatchCardID";
            ddl.DataBind();
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
            ddl.SelectedIndex = 0;
        }
        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindAllStoreWithBrand(StoreID, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //不填内容直接搜索时要弹出提示消息
            if (!this.ValidaInput())
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                RptBind(null);
                return;
            }

            string strWhere = GetStrWhere();

            this.ResultGrid.PageIndex = 0;

            //if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue) && this.CardTypeID.SelectedValue != "-1")
            //{
            //    SVASessionInfo.CardTypeCodeName = this.CardTypeID.SelectedText;
            //}
            //if (!string.IsNullOrEmpty(this.CardGradeID.SelectedValue) && this.CardGradeID.SelectedValue != "-1")
            //{
            //    SVASessionInfo.CardGradeCodeName = this.CardGradeID.SelectedText;
            //}

            RptBind(strWhere);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardTypeID.SelectedValue))
            {
                Edge.Web.Tools.ControlTool.BindDataSet(this.CardGradeID, null, null, null, null, null, null);
                return;
            }

            int cardTypeID = ConvertTool.ConverType<int>(this.CardTypeID.SelectedValue);

            Edge.Web.Tools.ControlTool.BindCardGrade(this.CardGradeID, cardTypeID);

        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardGradeID.SelectedValue))
            {
                this.CardBatchID.DataSource = Controllers.CardBatchController.GetInstance().GetBatch(10);
                return;
            }
            int cardGradeID = ConvertTool.ConverType<int>(this.CardGradeID.SelectedValue);

            this.CardBatchID.DataSource = Controllers.CardBatchController.GetInstance().GetBatch(10, cardGradeID);
        }


        protected void ResultGrid_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.ResultGrid.PageIndex = e.NewPageIndex;

            RptBind(GetStrWhere());
        }


        private void RptBind(string strWhere)
        {
            if (string.IsNullOrEmpty(strWhere))
            {
                this.ResultGrid.DataSource = null; ;
                this.ResultGrid.DataBind();
                this.ResultGrid.RecordCount = 0;

                return;
            }


            Edge.SVA.BLL.Card card = new Edge.SVA.BLL.Card();

            this.ResultGrid.RecordCount = card.GetCount(strWhere);

            DataSet ds = card.GetList(this.ResultGrid.PageSize, ResultGrid.PageIndex, strWhere, "CardNumber");

            DataTool.AddID(ds, "ID", this.ResultGrid.PageSize, ResultGrid.PageIndex);
            DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            DataTool.AddCardUID(ds, "VendorCardNumber", "CardNumber");
            DataTool.AddCardBatchCode(ds, "BatchCode", "BatchCardID");
            DataTool.AddCardStatus(ds, "StatusName", "Status");
            DataTool.AddDateFormat(ds, "CreatedOnText", "CreatedOn");
            DataTool.AddDateFormat(ds, "CardExpiryDateText", "CardExpiryDate");
            DataTool.AddCouponStockStatus(ds, "StockStatusName", "StockStatus");

            if (ds.Tables[0].DefaultView.Count <= 0)
            {
                ShowWarning(Resources.MessageTips.NoData);
                this.ResultGrid.DataSource = null;
                this.ResultGrid.DataBind();
                return;
            }
            this.ResultGrid.DataSource = ds.Tables[0].DefaultView;
            this.ResultGrid.DataBind();


        }

        private string GetStrWhere()
        {
            string strWhere = " 1=1 ";

            if (this.Brand.SelectedValue != "-1")
            {
                if (this.StoreID.SelectedValue != "-1")
                {
                    strWhere += string.Format(" and IssueStoreID = '{0}' ", this.StoreID.SelectedValue.Trim());
                }
                else
                {
                    string storeids = SVASessionInfo.CurrentUser.SqlConditionAllStoresIDsByBrandID(this.Brand.SelectedValue);
                    if (!string.IsNullOrEmpty(storeids))
                    {
                        strWhere += string.Format(" and IssueStoreID in {0} ", storeids);
                    }
                }
            }
            if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue) && this.CardTypeID.SelectedValue != "-1")
            {
                strWhere += string.Format(" and CardTypeID = {0} ", this.CardTypeID.SelectedValue);
            }

            if (!string.IsNullOrEmpty(this.CardGradeID.SelectedValue) && this.CardGradeID.SelectedValue != "-1")
            {
                strWhere += string.Format(" and CardGradeID = {0} ", this.CardGradeID.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.CardBatchID.SelectedValue))
            {
                strWhere += string.Format(" and BatchCardID = {0} ", this.CardBatchID.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.CardNumber.Text))
            {
                strWhere += string.Format(" and CardNumber = '{0}'  ", this.CardNumber.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.CardUID.Text))
            {
                strWhere += string.Format(" and CardNumber in (select CardNumber from CardUIDMap where CardUID =  '{0}')  ", this.CardUID.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.Amount.Text))
            {
                strWhere += string.Format(" and TotalAmount = '{0}'  ", this.Amount.Text.Replace(",", "").Trim());
            }
            if (!string.IsNullOrEmpty(this.Point.Text))
            {
                strWhere += string.Format(" and TotalPoints = '{0}' ", this.Point.Text.Replace(",", "").Trim());
            }
            if (!string.IsNullOrEmpty(this.Status.SelectedValue))
            {
                strWhere += string.Format(" and Status = '{0}'  ", this.Status.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.StockStatus.SelectedValue.Trim()))
            {
                strWhere += string.Format(" and StockStatus = '{0}' ", this.StockStatus.SelectedValue.Trim());
            }
            if (!string.IsNullOrEmpty(this.CreatedOn.Text))
            {
                strWhere += string.Format(" and Convert(char(10),CreatedOn,120) = '{0}'  ", this.CreatedOn.Text);
            }
            if (!string.IsNullOrEmpty(this.ExpiryDate.Text))
            {
                strWhere += string.Format("and Convert(char(10),CardExpiryDate,120) = '{0}'  ", this.ExpiryDate.Text);
            }
            //Add by Nathan 20140706 ++
            if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text) || !string.IsNullOrEmpty(this.MemberEmail.Text))
            {
                string memberWhere = " 1=1 ";

                if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text))
                {
                    memberWhere += string.Format(" and MemberMobilePhone ='{0}'", this.MemberMobilePhone.Text);
                }

                if (!string.IsNullOrEmpty(this.MemberEmail.Text))
                {
                    memberWhere += string.Format(" and MemberEmail ='{0}'", this.MemberEmail.Text);
                }


                strWhere += "and MemberID in (select MemberID from [Member] where " + memberWhere + ")";
            }
            //Add by Nathan 20140706 --

            return strWhere;
        }

        private bool ValidaInput()
        {
            //Add By Robin 2014-11-14 copy from Coupon Query
            if (this.Brand.SelectedValue != "-1")
            {
                return true;
            }
            //
            //Add by Nathan 20140706 ++
            if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text)) return true;

            if (!string.IsNullOrEmpty(this.MemberEmail.Text)) return true;
            //Add by Nathan 20140706 --
            if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue) && this.CardTypeID.SelectedValue != "-1") return true;

            if (!string.IsNullOrEmpty(this.CardGradeID.SelectedValue) && this.CardGradeID.SelectedValue != "-1") return true;

            if (!string.IsNullOrEmpty(this.CardBatchID.Text)) return true;

            if (!string.IsNullOrEmpty(this.CardNumber.Text)) return true;

            if (!string.IsNullOrEmpty(this.CardUID.Text)) return true;

            if (!string.IsNullOrEmpty(this.Status.SelectedValue) && this.Status.SelectedValue != "") return true;

            if (!string.IsNullOrEmpty(this.StockStatus.SelectedValue)  && this.StockStatus.SelectedValue != "") return true;

            if (!string.IsNullOrEmpty(this.Amount.Text)) return true;

            if (!string.IsNullOrEmpty(this.Point.Text)) return true;

            if (!string.IsNullOrEmpty(this.CreatedOn.Text)) return true;

            if (!string.IsNullOrEmpty(this.ExpiryDate.Text)) return true;

            return false;
        }
    }
}
