﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using System.Text;
using FineUI;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardRedeem;


namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardRedeem
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardAdjust_H, Edge.SVA.Model.Ord_CardAdjust_H>
    {

        #region Basic Event
        CardRedeemController controller = new  CardRedeemController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Window1.Title = "搜索";
                this.WindowSearch.Title = "搜索";
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, "CardTypeID =-1");
                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
                Edge.Web.Tools.ControlTool.BindBrand(Brand);

                //this.CardStatus.Text = Tools.DALTool.GetCardStatusName((int)Controllers.CardController.CardStatus.Redeemed);

                //btnAddSearchItem.OnClientClick = this.SearchListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDeleteResultItem.OnClientClick = this.AddResultListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDeleteResultItem.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDeleteResultItem.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                InitData();
                SVASessionInfo.CardRedeemController = null;
            }
            controller = SVASessionInfo.CardRedeemController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardAdjust_H item = this.GetAddObject();

            if (item != null)
            {
                if (item.CardAdjustNumber.Equals(string.Empty))
                {
                    ShowAddFailed();
                    return;
                }
                item.BrandCode = Tools.DALTool.GetBrandCode(Tools.ConvertTool.ToInt(this.Brand.SelectedValue), null);//Add Brand Code
                item.StoreCode = Tools.DALTool.GetStoreCode(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), null);//Add Store Code
                item.OprID = Convert.ToInt32(Enum.Parse(typeof(CardController.OprID), CardController.OprID.CardRedeemed.ToString()));
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
            }
            //老界面代码
            //if (ViewState["AddResult"] == null || ((DataTable)ViewState["AddResult"]).Rows.Count <= 0)
            //{
            //    ShowWarning(Resources.MessageTips.SelectCards);
            //    return;
            //}
            //新界面代码
            if (controller.ViewModel.CardTable == null || (controller.ViewModel.CardTable.Rows.Count <= 0))
            {
                ShowWarning(Resources.MessageTips.SelectCards);
                return;
            }


            int count = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CardAdjust_H>(item);
            if (count > 0)
            {
                //老界面代码
                //if (ViewState["AddResult"] != null)
                //{
                //    DataTable issuedDT = (DataTable)ViewState["AddResult"];
                //    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                //    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                //    database.SetExecuteTimeout(600);

                //    DataTable needInsertDt = database.GetTableSchema("Ord_CardAdjust_D");
                //    foreach (DataRow row in issuedDT.Rows)
                //    {
                //        DataRow dr = needInsertDt.NewRow();
                //        dr["CardAdjustNumber"] = this.CardAdjustNumber.Text;
                //        dr["CardNumber"] = row["CardNumber"];
                //        dr["CardAmount"] = row["ActualTxnAmount"];
                //        needInsertDt.Rows.Add(dr);
                //    }
                //    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CardAdjust_D");
                //    if (!es.Success)
                //    {
                //        ShowAddFailed();
                //        return;
                //    }
                //}
                if (controller.ViewModel.CardTable != null)
                {
                    DataTable issuedDT = controller.ViewModel.CardTable;
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(600);

                    DataTable needInsertDt = database.GetTableSchema("Ord_CardAdjust_D");
                    foreach (DataRow row in issuedDT.Rows)
                    {
                        DataRow dr = needInsertDt.NewRow();
                        dr["CardAdjustNumber"] = this.CardAdjustNumber.Text;
                        dr["CardNumber"] = row["CardNumber"];
                        //Modify by Alex 2014-10-14 ++
                        //dr["OrderAmount"] = row["ActualTxnAmount"];
                        decimal decTxnAmount = 0;
                        decimal.TryParse(this.ActAmount.Text.ToString(), out decTxnAmount);
                        dr["OrderAmount"] = decTxnAmount;
                        //Modify by Alex 2014-10-14 --
                        needInsertDt.Rows.Add(dr);
                    }
                    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CardAdjust_D");
                    if (!es.Success)
                    {
                        ShowAddFailed();
                        return;
                    }
                }
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Redeem  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddSuccess);

                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Redeem  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddFailed);

                ShowAddFailed();
            }
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
            InitCardTypeByStore();
            InitSearchList();
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitCardTypeByStore();
            InitSearchList();
        }


        #endregion

        #region Basic Functions
        private void InitData()
        {
            this.CardAdjustNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderCardRedeem);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
            TxnDate.Text = Tools.DALTool.GetSystemDate();

            btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
            btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;

            Tools.ControlTool.BindBatchID(BatchCardID);
            InitStoreByBrand();
            InitCardTypeByStore();
            //    this.btnAddItem.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            //    this.btnDeleteItem.Visible = this.btnDeleteAllItem.Visible = this.rptAddList.Items.Count > 0 ? true : false;
            //    this.Amount.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            //    this.CardTypeAmount.Visible = this.rptSearchList.Items.Count > 0 ? true : false; 

        }

        protected override SVA.Model.Ord_CardAdjust_H GetPageObject(SVA.Model.Ord_CardAdjust_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void CheckBrandStore(DataTable addDT)
        {
            if (addDT.Rows.Count > 0)
            {
                Brand.Enabled = false;
                StoreID.Enabled = false;
            }
            else
            {
                Brand.Enabled = true;
                StoreID.Enabled = true;
            }
        }

        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(StoreID, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }

        private void InitCardTypeByStore()
        {
            if (!string.IsNullOrEmpty(this.StoreID.SelectedValue))
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, "CardTypeID in (" + Tools.DALTool.GetCardTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 2) + ") order by CardTypeCode");
            else
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID, " 1>2");
        }

        private void SummaryAmounts(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                for (int i = 0; i <= table.Rows.Count - 1; i++)
                {
                    table.Rows[i]["ForfeitAmount"] = Tools.ConvertTool.ConverType<decimal>(table.Rows[i]["TotalAmount"].ToString()) - Tools.ConvertTool.ToDecimal(table.Rows[i]["ActualTxnAmount"].ToString());

                }
                table.AcceptChanges();

                decimal totalDenomination = Tools.ConvertTool.ToDecimal(table.Compute(" sum(TotalAmount) ", "").ToString());
                decimal totalActualTxnAmount = Tools.ConvertTool.ToDecimal(table.Compute(" sum(ActualTxnAmount) ", "").ToString());
                decimal totalForfeitAmount = Tools.ConvertTool.ToDecimal(table.Compute(" sum(ForfeitAmount) ", "").ToString());
                this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
                this.lblTotalActualTxnAmount.Text = totalActualTxnAmount.ToString("N2");
                this.lblTotalForfeitAmount.Text = totalForfeitAmount.ToString("N2");
            }
            else
            {
                decimal TotalDenomination = 0.0m;
                decimal TotalActualTxnAmount = 0.0m;
                decimal TotalForfeitAmount = 0.0m;

                this.lblTotalDenomination.Text = TotalDenomination.ToString("N02");
                this.lblTotalActualTxnAmount.Text = TotalActualTxnAmount.ToString("N02");
                this.lblTotalForfeitAmount.Text = TotalForfeitAmount.ToString("N02");
            }

        }
        #endregion

        #region Search Functions

        private DataTable GetSearchDataTable()
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

            int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
            int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.Text.Trim());
            string couponNumber = CardNumber.Text.Trim();
            string couponTypeID = CardTypeID.SelectedValue;
            string strWhere = string.Format(" Card.Status in ( {0} )", (int)CardController.CardStatus.Active);
            string filedOrder = " Card.CardNumber ASC ";

            if (string.IsNullOrEmpty(couponTypeID))
            {
                couponTypeID = Tools.DALTool.GetCardGradeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 2);
            }

            strWhere = GetCardSearchStrWhere(top, batchCardID, couponNumber, couponTypeID, this.CardUID.Text.Trim(), strWhere);

            //Display message
            int count = bll.GetCount(strWhere);

            if (count <= 0)
            {
               // this.JscriptPrint(Messages.Manager.MessagesTool.instance.GetMessage("90180"), "", Resources.MessageTips.WARNING_TITLE);
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90180"));
                return null;
            }
            if ((top > webset.MaxSearchNum) || ((count > webset.MaxSearchNum) && top <= 0))
            {
                top = webset.MaxSearchNum;
               // this.JscriptPrint(Resources.MessageTips.IsMaxSearchLimit, "", Resources.MessageTips.WARNING_TITLE);
                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
            }

            DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);

            //Edge.Web.Tools.DataTool.AddCardUID(ds, "CardUID", "CardNumber");
            //Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
            //Edge.Web.Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            //Tools.DataTool.AddBatchCode(ds, "BatchCode", "BatchCardID");
            return ds.Tables[0];
        }

        //绑定搜索结果列表
        private void BindSearchList()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.SearchListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.SearchListGrid.PageIndex + 1, this.SearchListGrid.PageSize);
                this.SearchListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.SearchListGrid.DataBind();
            }
            else
            {
                this.SearchListGrid.PageSize = webset.ContentPageNum;
                this.SearchListGrid.PageIndex = 0;
                this.SearchListGrid.RecordCount = 0;
                this.SearchListGrid.DataSource = null;
                this.SearchListGrid.DataBind();
            }

            btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;

            if (ViewState["SearchResult"] != null)
            {
                DataTable dt = ViewState["SearchResult"] as DataTable;
                if (dt == null) return;

                if (dt.Rows.Count <= 0) return;

                int couponTypeID = Tools.ConvertTool.ToInt(dt.Rows[0]["CardTypeID"].ToString());
                Edge.SVA.Model.CardType couponType = new Edge.SVA.BLL.CardType().GetModel(couponTypeID);

                if (couponType == null) return;
            }
        }

        //绑定待添加结果列表
        private void BindResultList()
        {
            if (ViewState["AddResult"] != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["AddResult"];
                SummaryAmounts(dt);
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);             
                this.AddResultListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.AddResultListGrid.DataBind();
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }

            btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;

            this.Brand.Enabled = this.StoreID.Enabled = AddResultListGrid.RecordCount > 0 ? false : true;
        }

        private void AddItem()
        {
            if (string.IsNullOrEmpty(this.txtActualTxnAmount.Text))
            {
                ShowWarning(Resources.MessageTips.EntryActualTxnAmount);
                return;
            }

            if (ViewState["SearchResult"] != null)
            {
                if (ViewState["AddResult"] == null)
                {
                    ViewState["AddResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = (DataTable)ViewState["AddResult"];
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    ShowWarning(Resources.MessageTips.IsMaximumLimit);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";

                    foreach (int row in SearchListGrid.SelectedRowIndexArray)
                    {
                        ids += string.Format("{0},", "'" + SearchListGrid.DataKeys[row][0].ToString() + "'");
                    }

                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }

                    foreach (int row in SearchListGrid.SelectedRowIndexArray)
                    {
                        decimal inputAmount = 0;
                        decimal couponAmount = 0;

                        inputAmount = Tools.ConvertTool.ToDecimal(this.txtActualTxnAmount.Text.Trim());
                        couponAmount = Tools.ConvertTool.ToDecimal(SearchListGrid.DataKeys[row][1].ToString());

                        if (inputAmount < 0)
                        {
                            ShowWarning(Resources.MessageTips.EntryActualTxnAmount);
                            return;
                        }
                        if (inputAmount > couponAmount)
                        {
                            ShowWarning(Resources.MessageTips.EntryActualTxnAmountTooLarge);
                            return;
                        }
                    }

                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CardNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    foreach (DataRowView drv in dvSearch)
                    {
                        drv.Delete();
                    }
                    vsDT.AcceptChanges();

                    ViewState["SearchResult"] = vsDT;

                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];
                    decimal minCardAmount = Tools.ConvertTool.ToDecimal(dtSearch.Compute("min(CardAmount)", "true").ToString());

                    decimal inputAmount = 0;
                    decimal couponAmount = 0;

                    inputAmount = Tools.ConvertTool.ToDecimal(this.txtActualTxnAmount.Text.Trim());
                    couponAmount = minCardAmount;

                    if (inputAmount < 0)
                    {
                        ShowWarning(Resources.MessageTips.EntryActualTxnAmount);
                        return;
                    }
                    if (inputAmount > couponAmount)
                    {
                        ShowWarning(Resources.MessageTips.EntryActualTxnAmountTooLarge);
                        return;
                    }

                    ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                    cbSearchAll.Checked = false;
                }

                DataTable addDT = (DataTable)ViewState["AddResult"];

                //Add ActualTxnAmount
                AddActualTxnAmount(dtSearch);

                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CardNumber");

                this.txtActualTxnAmount.Text = string.Empty;
                ViewState["AddResult"] = newSearchDT;
                CheckBrandStore(newSearchDT);

                BindResultList();
                BindSearchList();
            }
        }

        private void DeleteItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = (DataTable)ViewState["AddResult"];

            //    foreach (int row in AddResultListGrid.SelectedRowIndexArray)
            //    {
            //        string couponNumber = AddResultListGrid.DataKeys[row][0].ToString();
            //        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
            //        {
            //            if (addDT.Rows[j]["CardNumber"].ToString().Trim() == couponNumber)
            //            {
            //                addDT.Rows.Remove(addDT.Rows[j]);
            //            }
            //        }
            //        addDT.AcceptChanges();
            //    }

            //    ViewState["AddResult"] = addDT;
            //    CheckBrandStore(addDT);
            //    BindResultList();
            //}
            //新界面代码
            //转移界面删除代码
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable;

                foreach (int row in AddResultListGrid.SelectedRowIndexArray)
                {
                    string couponNumber = AddResultListGrid.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["CardNumber"].ToString().Trim() == couponNumber)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);

            }

        }

        private void DeleteAllItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = ((DataTable)ViewState["AddResult"]).Clone();
            //    ViewState["AddResult"] = addDT;
            //    BindResultList();
            //}
            //删除界面代码
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable.Clone();
                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);
            }
        }

        private void InitSearchList()
        {
            ViewState["SearchResult"] = null;
            BindSearchList();
        }

        private void InitResultList()
        {
            //老界面代码
            //ViewState["AddResult"] = null;
            //BindResultList();
            //删除界面代码
            controller.ViewModel.CardTable = null;
            BindResultList(controller.ViewModel.CardTable);
        }

        private void AddActualTxnAmount(DataTable table)
        {
            for (int i = 0; i <= table.Rows.Count - 1; i++)
            {
                decimal inputAmount = Tools.ConvertTool.ConverType<decimal>(this.txtActualTxnAmount.Text.Trim());
                table.Rows[i]["ActualTxnAmount"] = inputAmount;
            }
            table.AcceptChanges();
        }

        //private void AddActualTxnAmount(DataTable table)
        //{
        //    if (ViewState["ids"] != null)
        //    {
        //        string[] idsList = ViewState["ids"].ToString().Split(',');
        //        foreach (string strID in idsList)
        //        {
        //            for (int i = 0; i <= table.Rows.Count - 1; i++)
        //            {
        //                if (strID.Replace("'", "").Trim() == table.Rows[i]["CardNumber"].ToString().Trim())
        //                {
        //                    decimal inputAmount = Tools.ConvertTool.ConverType<decimal>(this.txtActualTxnAmount.Text.Trim());
        //                    table.Rows[i]["ActualTxnAmount"] = inputAmount;
        //                }
        //            }
        //            table.AcceptChanges();
        //        }
        //    }
        //}


        #endregion

        #region Search Events

        protected void btnViewSearch_Click(object sender, EventArgs e)
        {
            //InitSearchList();
            //this.Window1.Hidden = false;

            //转移界面
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Card/Add.aspx?BrandID={0}&StoreID={1}", this.Brand.SelectedValue, this.StoreID.SelectedValue)));

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SearchListGrid.PageIndex = 0;

            int top = Edge.Web.Tools.ConvertTool.ConverType<int>(CardCount.Text.Trim());
            int batchCardID = Edge.Web.Tools.ConvertTool.ConverType<int>(BatchCardID.SelectedValue);
            string couponNumber = CardNumber.Text.Trim();
            string couponTypeID = CardTypeID.SelectedValue;

            if ((top <= 0) && (batchCardID <= 0) && string.IsNullOrEmpty(couponNumber) && string.IsNullOrEmpty(couponTypeID) && string.IsNullOrEmpty(this.CardUID.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return;
            }

            if (top > webset.MaxSearchNum)
            {

                ShowWarning(Resources.MessageTips.IsMaxSearchLimit);
                return;
            }

            DataTable dt = GetSearchDataTable();

            //Add new column
            if (dt != null && dt.Rows.Count > 0)
            {
                Tools.DataTool.AddColumn(dt, "ActualTxnAmount", 0.00);
                Tools.DataTool.AddColumn(dt, "ForfeitAmount", 0.00);
            }

            ViewState["SearchResult"] = dt;
            BindSearchList();
        }

        protected void btnCloseSearch_Click(object sender, EventArgs e)
        {
            InitSearchList();
            this.Window1.Hidden = true;
        }

        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }

        protected void btnAddSearchItem_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        protected void SearchListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            SearchListGrid.PageIndex = e.NewPageIndex;
            BindSearchList();
        }

        protected void AddResultListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            //老界面代码
            //AddResultListGrid.PageIndex = e.NewPageIndex;
            //BindResultList();
            //删除界面代码
            AddResultListGrid.PageIndex = e.NewPageIndex;
            BindResultList(controller.ViewModel.CardTable);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindBatchID(BatchCardID, Tools.ConvertTool.ConverType<int>(CardTypeID.SelectedValue));
            }
            else
            {
                Tools.ControlTool.BindBatchID(BatchCardID);
            }

        }

        protected void cbSearchAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.cbSearchAll.Checked)
            {
                btnAddSearchItem.OnClientClick ="";
            }
            else
            {
                btnAddSearchItem.OnClientClick = this.SearchListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            }
           // this.lbtnDeleteIssued.Visible = !this.cbSearchAll.Checked;
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            //老界面
            //base.WindowEdit_Close(sender, e);
            //InitSearchList();

            base.WindowEdit_Close(sender, e);
            BindResultList(controller.ViewModel.CardTable);
        }
        #endregion

        //绑定添加结果（新）
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                //DataTable dt = (DataTable)ViewState["AddResult"];
                SummaryAmounts(dt);
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.AddResultListGrid.DataBind();
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }

            btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;

            this.Brand.Enabled = this.StoreID.Enabled = AddResultListGrid.RecordCount > 0 ? false : true;
        }
    }
}
