﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CardManagement.BatchImportCards
{
    public partial class show : Tools.BasePage<Edge.SVA.BLL.Ord_ImportCardUID_H, Edge.SVA.Model.Ord_ImportCardUID_H>
    {
        //TBC ++
        private const string fields = "KeyID,ImportCardNumber,CardGradeID,CardUID,ExpiryDate,BatchCode,Denomination";
        //TBC --
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
             
                this.RecordCount = -1;
                RptBind(string.Format("ImportCardNumber = '{0}'", Request.Params["id"]), "KeyID");

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveByName.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
       
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                    this.btnExport.Visible = false;
                }
                else if (Model.ApproveStatus == "A")
                {
                    this.btnExport.Visible = true;
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault()); 
                }
                else
                {
                    this.btnExport.Visible = false;
                }


            }
        }


        protected override void SetObject()
        {
            base.SetObject(Model, this.extForm.Controls.GetEnumerator());
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(string.Format("ImportCardNumber = '{0}'", Request.Params["id"]), "KeyID");
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            string fileName = "";
            int records = 0;

            Edge.SVA.BLL.Ord_ImportCardUID_H bll = new SVA.BLL.Ord_ImportCardUID_H();
           
            try
            {
                records = new SVA.BLL.Ord_ImportCardUID_D().GetCount(this.ImportCardNumber.Text.Trim(), 120);
                fileName = bll.ExportCSV(this.ImportCardNumber.Text, records);
                if (string.IsNullOrEmpty(fileName))
                {
                    JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Card - Import", fn, start, records, null);

            }
            catch (Exception ex)
            {

                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Card - Import", fn, start, records, ex.Message);
                JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
            }

           
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {


            Edge.SVA.BLL.Ord_ImportCardUID_D bll = new Edge.SVA.BLL.Ord_ImportCardUID_D();

            DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetListForTotal(this.Grid1.PageSize, this.Grid1.PageIndex, strWhere, orderby, fields, 120);
                this.RecordCount = ds != null && ds.Tables.Count > 1 ? int.TryParse(ds.Tables[1].Rows[0]["Total"].ToString(), out count) ? count : 0 : 0;
            }
            else
            {
                ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, strWhere, orderby, fields, 120);
            }

            this.Grid1.RecordCount = this.RecordCount < 0 ? 0 : this.RecordCount;

            //Modify by Nathan 20140610 ++
            //Tools.DataTool.AddCardTypeName(ds, "CardTypeIDName", "CardTypeID");
            //   Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            //Tools.DataTool.AddCardTypeName(ds, "CardGradeIDName", "CardGradeID");
            //Tools.DataTool.AddCardTypeCode(ds, "CardGradeCode", "CardGradeID");

            Tools.DataTool.AddCardGradeName(ds, "CardGradeIDName", "CardGradeID");
            Tools.DataTool.AddCardGradeCode(ds, "CardGradeCode", "CardGradeID");
            //Modify by Nathan 20140610 --

            Tools.DataTool.AddColumn(ds, "CreatedDate", Convert.ToDateTime(Request.Params["CreatedOn"].ToString()));
            Tools.DataTool.AddColumn(ds, "CardNumber", "");

            if (Request.Params["Status"] == "A")
            {
                Tools.DataTool.AddColumn(ds, "Status", "");
                Tools.DataTool.AddCardStatus(ds, "StatusName", "Status"); //Add by Nathan 20140610 
                Dictionary<string, Edge.SVA.Model.Card> cache = new Dictionary<string, SVA.Model.Card>();

                Tools.DataTool.UpdateCardStatus(ds, "Status", "CardUID", cache);
                Tools.DataTool.UpdateCardBatch(ds, "BatchCode", "CardUID", cache);
                Tools.DataTool.UpdateCardExpiryDate(ds, "ExpiryDate", "CardUID", cache);
                Tools.DataTool.UpdateCardDenomination(ds, "Denomination", "CardUID", cache);
                Tools.DataTool.UpdateCardNumber(ds, "CardNumber", "CardUID", cache);

            }
            else
            {
                //Tools.DataTool.AddColumn(ds, "Status", CouponController.CouponStatus.Dormant.ToString());
//TBC Remove by Alex 2014-06-10 ++
                //Tools.DataTool.UpdateCouponAmout(ds, "Denomination", "CardUID");
//TBC Remove by Alex 2014-06-10 --
                Tools.DataTool.AddColumn(ds, "Status", 0);
                Tools.DataTool.AddCardStatus(ds, "StatusName", "Status");
            }
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                ViewState["RecordCount"] = value;
            }
        }
    }
}
