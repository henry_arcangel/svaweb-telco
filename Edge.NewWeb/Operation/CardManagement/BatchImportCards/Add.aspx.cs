﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers;
using System.Data.SqlClient;
using System.Text;
using Edge.Messages.Manager;
using FineUI;
using System.IO;

namespace Edge.Web.Operation.CardManagement.BatchImportCards
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_ImportCardUID_H, Edge.SVA.Model.Ord_ImportCardUID_H>
    {
        private ImportModelList list = new ImportModelList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //TBC++
                this.ImportCardNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderBatchImportCards);
                //TBC--
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;

            if (string.IsNullOrEmpty(this.ImportFile.FileName))
            {
                this.ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //校验文件类型是否正确
            if (!ValidateFile(this.ImportFile.FileName))
            {
                return;
            }
            string fileName = this.ImportFile.SaveToServer("Files\\BatchImport");
            DataTable dt = ExcelTool.GetFirstSheet(Server.MapPath("~" + fileName));
            Dictionary<string, SVA.Model.CardGrade> cache = new Dictionary<string, SVA.Model.CardGrade>();
            //Dictionary<string, SVA.Model.CardType> cache = new Dictionary<string, SVA.Model.CardType>();
            Edge.SVA.Model.Ord_ImportCardUID_H item = this.GetAddObject();

            this.ExcuteReslut.Text = true.ToString();

            if (!ValidData(dt, cache))
            {
                this.ExcuteReslut.Text = this.list.Success.ToString();

                Tools.Logger.Instance.WriteImportLog("Batch Creation of Card - Import", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                StringBuilder html = GetHtml(begin);

                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));

                return;
            }

            if (item != null)
            {
                item.ApproveOn = null;
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_ImportCardUID_H>(item) > 0)
            {
                DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                database.SetExecuteTimeout(6000);
                DataTable sourceTable = database.GetTableSchema("Ord_ImportCardUID_D");
                DatabaseUtil.Interface.IExecStatus es = null;

                int success = 0;
                int total = this.list.Count;


                try
                {
                    ImportModel detail = null;
                    Edge.Utils.Tools.CheckDigitUtil checkDigit = Utils.Tools.CheckDigitUtil.GetSingleton();
                    for (int index = 0; index < this.list.importDetails.Count; index++)
                    {
                        detail = this.list.importDetails[index];
                        //Edge.SVA.Model.CardType cardType = CardController.GetImportcardType(detail.CardTypeCode, cache);
                        Edge.SVA.Model.CardGrade cardGrade = CardGradeController.GetImportCardTGradeType(detail.CardGradeCode, cache);

                        #region input data to datatable

                        //一张一张导入
                        if (!this.list.IsRange)
                        {
                            DataRow drr = sourceTable.NewRow();
                            drr["ImportCardNumber"] = item.ImportCardNumber;
                            //drr["CardGradeID"] = cardType.CardTypeID;
                            drr["CardGradeID"] = cardGrade.CardGradeID;
                            drr["CardUID"] = detail.CardUID.Value.ToString();
                            drr["BatchCode"] = detail.BatchCode;

                            if (detail.ExpiryDate == null)
                            {
                                drr["ExpiryDate"] = DBNull.Value;
                            }
                            else
                            {
                                drr["ExpiryDate"] = detail.ExpiryDate.Value;
                            }

                            sourceTable.Rows.Add(drr);
                        }
                        //批量导入
                        else if (this.list.IsRange)
                        {
                            if (detail.NeedToAddZero)
                            {
                                for (long i = detail.CardUIDBegin.Value; i <= detail.CardUIDEnd.Value; i++)
                                {
                                    DataRow drr = sourceTable.NewRow();
                                    drr["ImportCardNumber"] = item.ImportCardNumber;
                                    drr["CardGradeID"] = cardGrade.CardGradeID;
                                    //drr["CardGradeID"] = cardType.CardTypeID;

                                    if (cardGrade.UIDCheckDigit.GetValueOrDefault() == 1)
                                    {
                                        drr["CardUID"] = checkDigit.AppendCheckDigitEAN13(i.ToString().PadLeft(detail.NumLength,'0'));
                                    }
                                    else if (cardGrade.UIDCheckDigit.GetValueOrDefault() == 2)
                                    {
                                        drr["CardUID"] = checkDigit.AppendCheckDigitMOD10(i.ToString().PadLeft(detail.NumLength,'0'));
                                    }
                                    else
                                    {
                                        drr["CardUID"] = i.ToString().PadLeft(detail.NumLength,'0');
                                    }
                                    drr["BatchID"] = detail.BatchCode;

                                    if (detail.ExpiryDate == null)
                                    {
                                        drr["ExpiryDate"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        drr["ExpiryDate"] = detail.ExpiryDate.Value;
                                    }

                                    sourceTable.Rows.Add(drr);
                                }
                            }
                            else
                            {
                                for (long i = detail.CardUIDBegin.Value; i <= detail.CardUIDEnd.Value; i++)
                                {
                                    DataRow drr = sourceTable.NewRow();
                                    drr["ImportCardNumber"] = item.ImportCardNumber;
                                    drr["CardGradeID"] = cardGrade.CardGradeID;
                                    //drr["CardGradeID"] = cardType.CardTypeID;
                                    if (cardGrade.UIDCheckDigit.GetValueOrDefault() == 1)
                                    {
                                        drr["CardUID"] = checkDigit.AppendCheckDigitEAN13(i.ToString());
                                    }
                                    else if (cardGrade.UIDCheckDigit.GetValueOrDefault() == 2)
                                    {
                                        drr["CardUID"] = checkDigit.AppendCheckDigitMOD10(i.ToString());
                                    }
                                    else
                                    {
                                        drr["CardUID"] = i.ToString();
                                    }
                                    drr["BatchCode"] = detail.BatchCode;

                                    if (detail.ExpiryDate == null)
                                    {
                                        drr["ExpiryDate"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        drr["ExpiryDate"] = detail.ExpiryDate.Value;
                                    }

                                    sourceTable.Rows.Add(drr);
                                }
                            }

                        }

                        if (sourceTable.Rows.Count >= 100000)
                        {
                            es = database.InsertBigData(sourceTable, "Ord_ImportCardUID_D");
                            if (es.Success)
                            {
                                success += sourceTable.Rows.Count;
                                sourceTable.Rows.Clear();
                            }
                            else
                            {
                                throw es.Ex;
                            }
                        }
                        #endregion
                    }
                    #region 最后清除缓存

                    if (sourceTable.Rows.Count > 0)
                    {
                        es = database.InsertBigData(sourceTable, "Ord_ImportCardUID_D");
                        if (es.Success)
                        {
                            success += sourceTable.Rows.Count;
                            sourceTable.Rows.Clear();
                        }
                        else
                        {
                            throw es.Ex;
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    this.list.Error.Add(ex.Message);
                }
                finally
                {
                    sourceTable.Clear();
                    sourceTable.Dispose();

                    Tools.Logger.Instance.WriteImportLog("Batch Creation of Card - Import", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                    StringBuilder html = GetHtml(begin);
                    FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
                }
            }
            else
            {
                this.ShowAddFailed();
            }
        }


        protected override SVA.Model.Ord_ImportCardUID_H GetPageObject(SVA.Model.Ord_ImportCardUID_H obj)
        {
            return base.GetPageObject(obj, extForm.Controls.GetEnumerator());
        }

        private bool ValidData(DataTable dt, Dictionary<string, SVA.Model.CardGrade> cache)
        //private bool ValidData(DataTable dt, Dictionary<string, SVA.Model.CardType> cache)
        {
            //检查数据是否合法
            if (!CheckData(dt)) return false;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            Dictionary<long, bool> tempList = null;

            ImportModel item = null;
            if (!this.list.IsRange) tempList = new Dictionary<long, bool>(this.list.importDetails.Count);

            #region 检查Excel是否存在相同UID ，Batch ID 是否有多个CardType Code

            for (int i = 0; i < this.list.importDetails.Count; i++)
            {
                //Batch Code 是否有多个CardType Code
                item = this.list.importDetails[i];

                if (!string.IsNullOrEmpty(item.BatchCode))
                {
                    if (!dic.ContainsKey(item.BatchCode))
                    {
                       // dic.Add(item.BatchCode, item.CardTypeCode);
                        dic.Add(item.BatchCode, item.CardGradeCode);
                    }
                    if (dic[item.BatchCode] != item.CardGradeCode)
                    //if (dic[item.BatchCode] != item.CardTypeCode)
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(MessagesTool.instance.GetMessage("90338"), item.BatchCode)));
                        return false;
                    }
                }

                //检查Excel内UID是否重复
                if (!this.list.IsRange)
                {
                    try
                    {
                        tempList.Add(item.CardUID.Value, true);
                    }
                    catch
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90182")));
                        return false;
                    }
                }
                else if (this.list.IsRange)
                {
                    //检查Excel是否存在相同UID 
                    for (int j = i + 1; j < this.list.importDetails.Count; j++)
                    {
                        ImportModel temp = this.list.importDetails[j];
                        if (temp.CardUIDBegin.Value >= item.CardUIDBegin.Value && temp.CardUIDBegin.Value <= item.CardUIDEnd.Value)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90182")));
                            return false;
                        }

                        if (temp.CardUIDEnd.Value >= item.CardUIDBegin.Value && temp.CardUIDEnd.Value <= item.CardUIDEnd.Value)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90182")));
                            return false;
                        }
                    }
                }

            }

            #endregion

            #region 检查是否存在 CardType Code , Batch ID , CoupnUID 是否存在

            Edge.SVA.BLL.Card card = new Edge.SVA.BLL.Card();
            Edge.SVA.BLL.Ord_ImportCardUID_H bllImportH = new Edge.SVA.BLL.Ord_ImportCardUID_H();

            Edge.SVA.BLL.BatchCard batchCard = new SVA.BLL.BatchCard();

            List<string> cardNumbers = new List<string>();
            for (int i = 0; i < this.list.importDetails.Count; i++)
            {
                item = this.list.importDetails[i];
                //检查Card Type Code 是否存在
                //Edge.SVA.Model.CardType cardType = CardController.GetImportcardType(item.CardTypeCode, cache);
                Edge.SVA.Model.CardGrade cardGrade = CardGradeController.GetImportCardTGradeType(item.CardGradeCode, cache);
                if (cardGrade == null)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90500"), item.CardGradeCode)));
                    return false;
                }
                else if (cardGrade.IsImportUIDNumber.GetValueOrDefault() != 1)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90503"), item.CardGradeCode)));
                    return false;
                }
                //检查批次是否存在
                if (!string.IsNullOrEmpty(item.BatchCode) && batchCard.ExistBatchCode(item.BatchCode))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(MessagesTool.instance.GetMessage("90336"), item.BatchCode)));
                    return false;
                }
                //每张导入
                if (!this.list.IsRange)
                {
                    cardNumbers.Add(item.CardUID.Value.ToString());
                    if (i % 1000 == 0)
                    {
                        if (bllImportH.ExistCardUID(cardNumbers))
                        {
                            this.list.Error.Add(string.Format("Line {0} - {1}:{2}\r\n", i >= 1000 ? i - 999 : 1, i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90182")));

                            return false;
                        }
                        cardNumbers.Clear();
                    }
                }
                else if (this.list.IsRange)
                {
                    //批次导入
                    if (bllImportH.ExistCardUID(item.CardUIDBegin.Value.ToString(), item.CardUIDEnd.Value.ToString(), cardGrade.UIDCheckDigit.GetValueOrDefault() == 1))
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90182")));

                        return false;
                    }
                }

            }
            //最后检查
            if (cardNumbers.Count > 0)
            {
                if (bllImportH.ExistCardUID(cardNumbers))
                {
                    this.list.Error.Add(string.Format("Excel Exist CardUID Last:{0}", Messages.Manager.MessagesTool.instance.GetMessage("90182")));
                    return false;
                }
                cardNumbers.Clear();
            }
            #endregion

            return true;                       
        }

        private bool CheckData(DataTable dt)
        {
            DataTool.ClearEndRow(dt);

            if (dt == null || dt.Rows.Count <= 0)
            {
                this.list.Error.Add(Resources.MessageTips.NoData);
                return false;
            }

            ImportModel model = null;
            DateTime expiryDate;

            bool isRange = false;
            bool isList = false;
            
            #region check columns
            List<string> columnList = new List<string>();
            //columnList.Add("Card Type Code");
            columnList.Add("Card Grade Code");
            columnList.Add("Card UID By Range (Beginning)");
            columnList.Add("Card UID By Range (Ending)");
            columnList.Add("Card UID (One by One)");
            columnList.Add("Batch Code");
            columnList.Add("Card UID By Range (Beginning)");
            StringBuilder sb = new StringBuilder(Resources.MessageTips.Lackofcolumn);
            bool existColumn = true;
            foreach (var item in columnList)
            {
                if (!dt.Columns.Contains(item))
                {
                    sb.Append(item);
                    sb.Append(",");
                    existColumn = false;
                }
            }
            if (!existColumn)
            {
                this.list.Error.Add(sb.ToString().TrimEnd(','));
                return false;
            }
            #endregion

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                #region 将DataTable转换为List
                model = new ImportModel();
                //model.CardTypeCode = row["Card Crade Code"].ToString();
                model.CardGradeCode = row["Card Grade Code"].ToString();
                model.CardUIDBegin = ConvertTool.ConverNullable<long>(row["Card UID By Range (Beginning)"].ToString());
                model.CardUIDEnd = ConvertTool.ConverNullable<long>(row["Card UID By Range (Ending)"].ToString());
                model.CardUID = ConvertTool.ConverNullable<long>(row["Card UID (One by One)"].ToString());
                model.BatchCode = row["Batch Code"].ToString();
                if (model.CardUIDBegin.HasValue && model.CardUIDEnd.HasValue && row["Card UID By Range (Beginning)"].ToString().Length > model.CardUIDBegin.Value.ToString().Length)
                {
                    model.NumLength = row["Card UID By Range (Beginning)"].ToString().Length;
                    model.NeedToAddZero = true;
                }

                if (!string.IsNullOrEmpty(row["Expiry Date"].ToString()))
                {
                    if (DateTime.TryParse(row["Expiry Date"].ToString(), out expiryDate))
                    {
                        model.ExpiryDate = expiryDate;
                        if (expiryDate < DateTime.Today)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90141")));
                        }
                    }
                    else
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90369")));
                    }
                }


                #endregion

                #region 检查数据是否合法

                if (string.IsNullOrEmpty(model.CardGradeCode))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.CouponCodeNotNull));
                }
                //判断每一行是否有多种格式
                if (!(model.CardUID.HasValue ^ model.CardUIDBegin.HasValue) || !(model.CardUID.HasValue ^ model.CardUIDEnd.HasValue))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.ImportFileError));
                }

                if (model.CardUIDBegin.HasValue && model.CardUIDEnd.HasValue && model.CardUIDEnd.Value < model.CardUIDBegin.Value)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.EndingNoLessThanBeginNo));
                }

                if (!isList && model.CardUID.HasValue) isList = true;
                if (!isRange && model.CardUIDBegin.HasValue && model.CardUIDEnd.HasValue) isRange = true;

                this.list.importDetails.Add(model);

                #endregion

                if (this.list.Error.Count >= 10) return false;
            }
            //判断整个Excel是否有多种格式
            if (!(isList ^ isRange))
            {
                this.list.Error.Add(string.Format("Excel :{0}\r\n", Resources.MessageTips.ImportFileError));

                return false;
            }

            this.list.IsRange = isRange ? true : false;

            if (this.list.Error.Count <= 0) return true;

            return false;
        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");
            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            html.AppendFormat("<tr><td align='right'></td><td>Import {0} records {1}.</td></tr>", this.list.Count, this.list.Success ? "successfully" : "failed");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                    //html.AppendFormat("<td>{0}</td>", error);
                }
                //html.AppendFormat("<td></td></tr>");
                html.AppendFormat("</table></td></tr>");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import File Name:", this.ImportFile.FileName);
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Card Creation - Import");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.CardCreateFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CardCreateFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }

    public class ImportModel
    {
        //public string CardTypeCode { get; set; }
        public string CardGradeCode { get; set; }
        public int NumLength { get; set; }
        private bool needToAddZero = false;
        public bool NeedToAddZero
        {
            get { return needToAddZero; }
            set { needToAddZero = value; }
        }
        public long? CardUIDBegin { get; set; }
        public long? CardUIDEnd { get; set; }
        public long? CardUID { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string BatchCode { get; set; }
    }

    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<ImportModel> importDetails = new List<ImportModel>();
        public bool IsRange { get; set; }
        public int Count
        {
            get
            {
                if (!this.IsRange) return importDetails.Count;
                int total = 0;
                for (int i = 0; i < this.importDetails.Count; i++)
                {
                    total += (int)(this.importDetails[i].CardUIDEnd - this.importDetails[i].CardUIDBegin);
                    total++;
                }
                return total;
            }
        }

        public List<string> Error { get { return error; } }

        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }

    }
}
