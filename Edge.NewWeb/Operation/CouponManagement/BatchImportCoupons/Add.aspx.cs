﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers;
using System.Data.SqlClient;
using System.Text;
using Edge.Messages.Manager;
using FineUI;
using System.IO;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_ImportCouponUID_H, Edge.SVA.Model.Ord_ImportCouponUID_H>
    {
        private ImportModelList list = new ImportModelList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.ImportCouponNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.OrderBatchImportCoupons);

                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;

            if (string.IsNullOrEmpty(this.ImportFile.FileName))
            {
                this.ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //校验文件类型是否正确
            if (!ValidateFile(this.ImportFile.FileName))
            {
                return;
            }
            string fileName = this.ImportFile.SaveToServer("Files\\BatchImport");
            DataTable dt = ExcelTool.GetFirstSheet(Server.MapPath("~" + fileName));
            Dictionary<string, SVA.Model.CouponType> cache = new Dictionary<string, SVA.Model.CouponType>();
            Edge.SVA.Model.Ord_ImportCouponUID_H item = this.GetAddObject();

            this.ExcuteReslut.Text = true.ToString();

            if (!ValidData(dt, cache))
            {
                this.ExcuteReslut.Text = this.list.Success.ToString();

                Tools.Logger.Instance.WriteImportLog("Batch Creation of Coupons - Import", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                StringBuilder html = GetHtml(begin);

                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));

                return;
            }

            if (item != null)
            {
                item.ApproveOn = null;
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_ImportCouponUID_H>(item) > 0)
            {
                DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                database.SetExecuteTimeout(6000);
                DataTable sourceTable = database.GetTableSchema("Ord_ImportCouponUID_D");
                DatabaseUtil.Interface.IExecStatus es = null;

                int success = 0;
                int total = this.list.Count;


                try
                {
                    ImportModel detail = null;
                    Edge.Utils.Tools.CheckDigitUtil checkDigit = Utils.Tools.CheckDigitUtil.GetSingleton();
                    for (int index = 0; index < this.list.importDetails.Count; index++)
                    {
                        detail = this.list.importDetails[index];
                        Edge.SVA.Model.CouponType couponType = CouponController.GetImportCouponType(detail.CouponTypeCode, cache);

                        #region input data to datatable

                        //一张一张导入
                        if (!this.list.IsRange)
                        {
                            DataRow drr = sourceTable.NewRow();
                            drr["ImportCouponNumber"] = item.ImportCouponNumber;
                            drr["CouponTypeID"] = couponType.CouponTypeID;
                            drr["CouponUID"] = detail.CouponUID.Value.ToString();
                            drr["BatchID"] = detail.BatchCode;

                            if (detail.ExpiryDate == null)
                            {
                                drr["ExpiryDate"] = DBNull.Value;
                            }
                            else
                            {
                                drr["ExpiryDate"] = detail.ExpiryDate.Value;
                            }

                            sourceTable.Rows.Add(drr);
                        }
                        //批量导入
                        else if (this.list.IsRange)
                        {
                            if (detail.NeedToAddZero)
                            {
                                for (long i = detail.CouponUIDBegin.Value; i <= detail.CouponUIDEnd.Value; i++)
                                {
                                    DataRow drr = sourceTable.NewRow();
                                    drr["ImportCouponNumber"] = item.ImportCouponNumber;
                                    drr["CouponTypeID"] = couponType.CouponTypeID;
                                    if (couponType.UIDCheckDigit.GetValueOrDefault() == 1)
                                    {
                                        drr["CouponUID"] = checkDigit.AppendCheckDigitEAN13(i.ToString().PadLeft(detail.NumLength,'0'));
                                    }
                                    else if (couponType.UIDCheckDigit.GetValueOrDefault() == 2)
                                    {
                                        drr["CouponUID"] = checkDigit.AppendCheckDigitMOD10(i.ToString().PadLeft(detail.NumLength,'0'));
                                    }
                                    else
                                    {
                                        drr["CouponUID"] = i.ToString().PadLeft(detail.NumLength,'0');
                                    }
                                    drr["BatchID"] = detail.BatchCode;

                                    if (detail.ExpiryDate == null)
                                    {
                                        drr["ExpiryDate"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        drr["ExpiryDate"] = detail.ExpiryDate.Value;
                                    }

                                    sourceTable.Rows.Add(drr);
                                }
                            }
                            else
                            {
                                for (long i = detail.CouponUIDBegin.Value; i <= detail.CouponUIDEnd.Value; i++)
                                {
                                    DataRow drr = sourceTable.NewRow();
                                    drr["ImportCouponNumber"] = item.ImportCouponNumber;
                                    drr["CouponTypeID"] = couponType.CouponTypeID;
                                    if (couponType.UIDCheckDigit.GetValueOrDefault() == 1)
                                    {
                                        drr["CouponUID"] = checkDigit.AppendCheckDigitEAN13(i.ToString());
                                    }
                                    else if (couponType.UIDCheckDigit.GetValueOrDefault() == 2)
                                    {
                                        drr["CouponUID"] = checkDigit.AppendCheckDigitMOD10(i.ToString());
                                    }
                                    else
                                    {
                                        drr["CouponUID"] = i.ToString();
                                    }
                                    drr["BatchID"] = detail.BatchCode;

                                    if (detail.ExpiryDate == null)
                                    {
                                        drr["ExpiryDate"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        drr["ExpiryDate"] = detail.ExpiryDate.Value;
                                    }

                                    sourceTable.Rows.Add(drr);
                                }
                            }

                        }

                        if (sourceTable.Rows.Count >= 100000)
                        {
                            es = database.InsertBigData(sourceTable, "Ord_ImportCouponUID_D");
                            if (es.Success)
                            {
                                success += sourceTable.Rows.Count;
                                sourceTable.Rows.Clear();
                            }
                            else
                            {
                                throw es.Ex;
                            }
                        }
                        #endregion
                    }
                    #region 最后清除缓存

                    if (sourceTable.Rows.Count > 0)
                    {
                        es = database.InsertBigData(sourceTable, "Ord_ImportCouponUID_D");
                        if (es.Success)
                        {
                            success += sourceTable.Rows.Count;
                            sourceTable.Rows.Clear();
                        }
                        else
                        {
                            throw es.Ex;
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    this.list.Error.Add(ex.Message);
                }
                finally
                {
                    sourceTable.Clear();
                    sourceTable.Dispose();

                    Tools.Logger.Instance.WriteImportLog("Batch Creation of Coupons - Import", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                    StringBuilder html = GetHtml(begin);
                    FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
                }
            }
            else
            {
                this.ShowAddFailed();
            }
        }


        protected override SVA.Model.Ord_ImportCouponUID_H GetPageObject(SVA.Model.Ord_ImportCouponUID_H obj)
        {
            return base.GetPageObject(obj, extForm.Controls.GetEnumerator());
        }

        private bool ValidData(DataTable dt, Dictionary<string, SVA.Model.CouponType> cache)
        {
            //检查数据是否合法
            if (!CheckData(dt)) return false;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            Dictionary<long, bool> tempList = null;

            ImportModel item = null;
            if (!this.list.IsRange) tempList = new Dictionary<long, bool>(this.list.importDetails.Count);

            #region 检查Excel是否存在相同UID ，Batch ID 是否有多个CouponType Code

            for (int i = 0; i < this.list.importDetails.Count; i++)
            {
                //Batch Code 是否有多个CouponType Code
                item = this.list.importDetails[i];

                if (!string.IsNullOrEmpty(item.BatchCode))
                {
                    if (!dic.ContainsKey(item.BatchCode))
                    {
                        dic.Add(item.BatchCode, item.CouponTypeCode);
                    }
                    if (dic[item.BatchCode] != item.CouponTypeCode)
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(MessagesTool.instance.GetMessage("90338"), item.BatchCode)));
                        return false;
                    }
                }

                //检查Excel内UID是否重复
                if (!this.list.IsRange)
                {
                    try
                    {
                        tempList.Add(item.CouponUID.Value, true);
                    }
                    catch
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90181")));
                        return false;
                    }
                }
                else if (this.list.IsRange)
                {
                    //检查Excel是否存在相同UID 
                    for (int j = i + 1; j < this.list.importDetails.Count; j++)
                    {
                        ImportModel temp = this.list.importDetails[j];
                        if (temp.CouponUIDBegin.Value >= item.CouponUIDBegin.Value && temp.CouponUIDBegin.Value <= item.CouponUIDEnd.Value)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90181")));
                            return false;
                        }

                        if (temp.CouponUIDEnd.Value >= item.CouponUIDBegin.Value && temp.CouponUIDEnd.Value <= item.CouponUIDEnd.Value)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90181")));
                            return false;
                        }
                    }
                }

            }

            #endregion

            #region 检查是否存在 CouponType Code , Batch ID , CoupnUID 是否存在

            Edge.SVA.BLL.Coupon coupon = new Edge.SVA.BLL.Coupon();
            Edge.SVA.BLL.Ord_ImportCouponUID_H bllImportH = new Edge.SVA.BLL.Ord_ImportCouponUID_H();

            Edge.SVA.BLL.BatchCoupon batchCoupon = new SVA.BLL.BatchCoupon();

            List<string> couponNumbers = new List<string>();
            for (int i = 0; i < this.list.importDetails.Count; i++)
            {
                item = this.list.importDetails[i];
                //检查Coupon Type Code 是否存在
                Edge.SVA.Model.CouponType couponType = CouponController.GetImportCouponType(item.CouponTypeCode, cache);
                if (couponType == null)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90337"), item.CouponTypeCode)));
                    return false;
                }
                else if (couponType.IsImportCouponNumber.GetValueOrDefault() != 1)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90415"), item.CouponTypeCode)));
                    return false;
                }
                //检查批次是否存在
                if (!string.IsNullOrEmpty(item.BatchCode) && batchCoupon.ExistBatchCode(item.BatchCode))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, string.Format(MessagesTool.instance.GetMessage("90336"), item.BatchCode)));
                    return false;
                }
                //每张导入
                if (!this.list.IsRange)
                {
                    couponNumbers.Add(item.CouponUID.Value.ToString());
                    if (i % 1000 == 0)
                    {
                        if (bllImportH.ExistCouponUID(couponNumbers))
                        {
                            this.list.Error.Add(string.Format("Line {0} - {1}:{2}\r\n", i >= 1000 ? i - 999 : 1, i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90181")));

                            return false;
                        }
                        couponNumbers.Clear();
                    }
                }
                else if (this.list.IsRange)
                {
                    //批次导入
                    if (bllImportH.ExistCouponUID(item.CouponUIDBegin.Value.ToString(), item.CouponUIDEnd.Value.ToString(), couponType.UIDCheckDigit.GetValueOrDefault() == 1))
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Messages.Manager.MessagesTool.instance.GetMessage("90181")));

                        return false;
                    }
                }

            }
            //最后检查
            if (couponNumbers.Count > 0)
            {
                if (bllImportH.ExistCouponUID(couponNumbers))
                {
                    this.list.Error.Add(string.Format("Excel Exist CouponUID Last:{0}", Messages.Manager.MessagesTool.instance.GetMessage("90181")));
                    return false;
                }
                couponNumbers.Clear();
            }
            #endregion

            return true;                       
        }

        private bool CheckData(DataTable dt)
        {
            DataTool.ClearEndRow(dt);

            if (dt == null || dt.Rows.Count <= 0)
            {
                this.list.Error.Add(Resources.MessageTips.NoData);
                return false;
            }

            ImportModel model = null;
            DateTime expiryDate;

            bool isRange = false;
            bool isList = false;
            
            #region check columns
            List<string> columnList = new List<string>();
            columnList.Add("Coupon Type Code");
            columnList.Add("Coupon UID By Range (Beginning)");
            columnList.Add("Coupon UID By Range (Ending)");
            columnList.Add("Coupon UID (One by One)");
            columnList.Add("Batch Code");
            columnList.Add("Coupon UID By Range (Beginning)");
            StringBuilder sb = new StringBuilder(Resources.MessageTips.Lackofcolumn);
            bool existColumn = true;
            foreach (var item in columnList)
            {
                if (!dt.Columns.Contains(item))
                {
                    sb.Append(item);
                    sb.Append(",");
                    existColumn = false;
                }
            }
            if (!existColumn)
            {
                this.list.Error.Add(sb.ToString().TrimEnd(','));
                return false;
            }
            #endregion

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                #region 将DataTable转换为List
                model = new ImportModel();
                model.CouponTypeCode = row["Coupon Type Code"].ToString();
                model.CouponUIDBegin = ConvertTool.ConverNullable<long>(row["Coupon UID By Range (Beginning)"].ToString());
                model.CouponUIDEnd = ConvertTool.ConverNullable<long>(row["Coupon UID By Range (Ending)"].ToString());
                model.CouponUID = ConvertTool.ConverNullable<long>(row["Coupon UID (One by One)"].ToString());
                model.BatchCode = row["Batch Code"].ToString();
                if (model.CouponUIDBegin.HasValue && model.CouponUIDEnd.HasValue && row["Coupon UID By Range (Beginning)"].ToString().Length > model.CouponUIDBegin.Value.ToString().Length)
                {
                    model.NumLength = row["Coupon UID By Range (Beginning)"].ToString().Length;
                    model.NeedToAddZero = true;
                }

                if (!string.IsNullOrEmpty(row["Expiry Date"].ToString()))
                {
                    if (DateTime.TryParse(row["Expiry Date"].ToString(), out expiryDate))
                    {
                        model.ExpiryDate = expiryDate;
                        if (expiryDate < DateTime.Today)
                        {
                            this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90141")));
                        }
                    }
                    else
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90369")));
                    }
                }


                #endregion

                #region 检查数据是否合法

                if (string.IsNullOrEmpty(model.CouponTypeCode))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.CouponCodeNotNull));
                }
                //判断每一行是否有多种格式
                if (!(model.CouponUID.HasValue ^ model.CouponUIDBegin.HasValue) || !(model.CouponUID.HasValue ^ model.CouponUIDEnd.HasValue))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.ImportFileError));
                }

                if (model.CouponUIDBegin.HasValue && model.CouponUIDEnd.HasValue && model.CouponUIDEnd.Value < model.CouponUIDBegin.Value)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.EndingNoLessThanBeginNo));
                }

                if (!isList && model.CouponUID.HasValue) isList = true;
                if (!isRange && model.CouponUIDBegin.HasValue && model.CouponUIDEnd.HasValue) isRange = true;

                this.list.importDetails.Add(model);

                #endregion

                if (this.list.Error.Count >= 10) return false;
            }
            //判断整个Excel是否有多种格式
            if (!(isList ^ isRange))
            {
                this.list.Error.Add(string.Format("Excel :{0}\r\n", Resources.MessageTips.ImportFileError));

                return false;
            }

            this.list.IsRange = isRange ? true : false;

            if (this.list.Error.Count <= 0) return true;

            return false;
        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");
            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            html.AppendFormat("<tr><td align='right'></td><td>Import {0} records {1}.</td></tr>", this.list.Count, this.list.Success ? "successfully" : "failed");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                    //html.AppendFormat("<td>{0}</td>", error);
                }
                //html.AppendFormat("<td></td></tr>");
                html.AppendFormat("</table></td></tr>");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import File Name:", this.ImportFile.FileName);
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Coupon Creation - Import");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.CouponCreateFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CouponCreateFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }

    public class ImportModel
    {
        public string CouponTypeCode { get; set; }
        public int NumLength { get; set; }
        private bool needToAddZero = false;
        public bool NeedToAddZero
        {
            get { return needToAddZero; }
            set { needToAddZero = value; }
        }
        public long? CouponUIDBegin { get; set; }
        public long? CouponUIDEnd { get; set; }
        public long? CouponUID { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string BatchCode { get; set; }
    }

    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<ImportModel> importDetails = new List<ImportModel>();
        public bool IsRange { get; set; }
        public int Count
        {
            get
            {
                if (!this.IsRange) return importDetails.Count;
                int total = 0;
                for (int i = 0; i < this.importDetails.Count; i++)
                {
                    total += (int)(this.importDetails[i].CouponUIDEnd - this.importDetails[i].CouponUIDBegin);
                    total++;
                }
                return total;
            }
        }

        public List<string> Error { get { return error; } }

        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }

    }
}
