﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.Messages.Manager;
using System.Data;
using FineUI;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));


                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    foreach (string id in idList)
                    {
                        Edge.SVA.Model.Ord_ImportCouponUID_H mode = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(id);

                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = id;
                        if (CouponController.CanApprove(mode))
                        {
                            dr["ApproveCode"] = CouponController.ApproveCouponForApproveCode(mode);
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                        }
                        else
                        {
                            dr["ApproveCode"] = Edge.Messages.Manager.MessagesTool.instance.GetMessage("90190");
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }


                        dt.Rows.Add(dr);
                    }
                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}
