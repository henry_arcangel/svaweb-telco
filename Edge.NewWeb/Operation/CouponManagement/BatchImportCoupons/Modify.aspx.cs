﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.Ord_ImportCouponUID_H, Edge.SVA.Model.Ord_ImportCouponUID_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Grid1.PageSize = webset.ContentPageNum;

                RptBind(string.Format("ImportCouponNumber = '{0}'", Request.Params["id"]), "KeyID");

                RegisterCloseEvent(btnClose);
            }
        }
        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {


            Edge.SVA.BLL.Ord_ImportCouponUID_D bll = new Edge.SVA.BLL.Ord_ImportCouponUID_D();
            Grid1.RecordCount = bll.GetCount(strWhere);

            DataSet ds = new DataSet();
            ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

            Edge.SVA.Model.Ord_ImportCouponUID_H item = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(Request.Params["id"]);
            string createDate = item.CreatedOn.HasValue ? item.CreatedOn.Value.ToString("yyyy-MM-dd") : null;

            Tools.DataTool.AddColumn(ds, "Status", CouponController.CouponStatus.Dormant.ToString());          
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeIDName", "CouponTypeID");         
            Tools.DataTool.AddColumn(ds, "CreatedDate", createDate);
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.UpdateCouponAmout(ds, "Denomination", "CouponTypeID");
            Tools.DataTool.AddColumn(ds, "CouponNumber", "");
          
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveByName.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.ApproveStatus.Text = Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }
                else 
                {
                    this.ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }

            }

        }

        protected override void SetObject()
        {
            base.SetObject(Model, this.extForm.Controls.GetEnumerator());
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.Ord_ImportCouponUID_H item = null;
            Edge.SVA.Model.Ord_ImportCouponUID_H dataItem = this.GetDataObject();

            //Check model
            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_ImportCouponUID_H>(item))
            {
                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(string.Format("ImportCouponNumber = '{0}'", Request.Params["id"]), "KeyID");
        }
    
    }
}
