﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponOrderForm_H, Edge.SVA.Model.Ord_CouponOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);

                //this.CouponOrderFormNumber.Text = DALTool.GetREFNOCode(CouponController.CouponRefnoCode.CouponOrderForm);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;

                ControlTool.BindStore(FromStoreID);
                ControlTool.BindBrand(ddlBrand);
                ControlTool.BindBrand(brandDetail);
                ControlTool.BindStore(StoreID, -1);
                ControlTool.BindCustomer(CustomerID);

                ControlTool.BindCouponType(ddlCouponType, -1);

                CustomerType_SelectedIndexChanged(null, null);

                //ControlTool.AddTitle(ddlBrand);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CouponOrderFormNumber.Text = Model.CouponOrderFormNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }


                Edge.SVA.Model.Store store = new SVA.BLL.Store().GetModel(Model.StoreID);
                int brandID = store == null ? -1 : store.BrandID.GetValueOrDefault();
                this.ddlBrand.SelectedValue = brandID < 0 ? "" : brandID.ToString();

                ControlTool.BindStore(StoreID, brandID);

                string storeID = Model.StoreID.ToString();
                this.StoreID.SelectedValue = this.StoreID.Items.FindByValue(storeID) == null ? "" : storeID;

                this.BindDetail();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteErrorLog(this.PageName, "Update");

            string type = this.CustomerType.SelectedValue.Trim();
            if (type == "1") //客户
            {
                if (this.CustomerID.SelectedValue == "")
                {
                    this.CustomerID.MarkInvalid(String.Format("'{0}' Can't Empty！", CustomerID.Text));
                    return;
                }
            }
            else//店铺
            {
                if (this.ddlBrand.SelectedValue == "")
                {
                    this.ddlBrand.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlBrand.Text));
                    return;
                }
                if (this.StoreID.SelectedValue == "")
                {
                    this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
                    return;
                }
            }

            Edge.SVA.Model.Ord_CouponOrderForm_H item = null;
            Edge.SVA.Model.Ord_CouponOrderForm_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            if (this.Detail.Rows.Count <= 0)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            //Update model
            item = this.GetPageObject(dataItem);

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponOrderForm_H>(item))
            {
                Edge.SVA.BLL.Ord_CouponOrderForm_D bll = new SVA.BLL.Ord_CouponOrderForm_D();
                bll.DeleteByOrder(this.CouponOrderFormNumber.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CouponOrderForm_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CouponOrderFormNumber"] = item.CouponOrderFormNumber;
                        //row["BrandID"] = detail["BrandID"];
                        row["CouponTypeID"] = detail["CouponTypeID"];
                        row["CouponQty"] = detail["CouponQty"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_CouponOrderForm_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, "Update", ex);
                    ShowAddFailed();
                    return;
                }
                CloseAndPostBack();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.brandDetail.SelectedItem.Value))
            {
                brandDetail.MarkInvalid(String.Format("'{0}' Can't Empty！", brandDetail.Text));
                return;
                //throw new Exception("Brand Can't Empty");
            }
            if (string.IsNullOrEmpty(this.ddlCouponType.SelectedItem.Value))
            {
                ddlCouponType.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlCouponType.Text));
                return;
                // throw new Exception("Coupon Type Can't Empty");
            }
            if (string.IsNullOrEmpty(this.txtOrderCount.Text))
            {
                txtOrderCount.MarkInvalid(String.Format("'{0}' Can't Empty！", txtOrderCount.Text));
                return;
                //throw new Exception("Coupon Qty Can't Empty");
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CouponTypeID"].ToString().Equals(this.ddlCouponType.SelectedValue))
                {
                    //JscriptPrint(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(Resources.MessageTips.ExistCouponTypeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["ID"] = this.DetailIndex;
            row["BrandID"] = int.Parse(this.brandDetail.SelectedItem.Value);
            row["BrandCode"] = this.brandDetail.SelectedItem.Text.Substring(0, this.brandDetail.SelectedItem.Text.IndexOf("-")).Trim();
            row["BrandName"] = this.brandDetail.SelectedItem.Text.Substring(this.brandDetail.SelectedItem.Text.IndexOf("-") + 1).Trim();
            row["CouponTypeID"] = int.Parse(this.ddlCouponType.SelectedItem.Value);
            row["CouponTypeCode"] = this.ddlCouponType.SelectedItem.Text.Substring(0, this.ddlCouponType.SelectedItem.Text.IndexOf("-"));
            row["CouponTypeName"] = this.ddlCouponType.SelectedItem.Text.Substring(this.ddlCouponType.SelectedItem.Text.IndexOf("-") + 1);
            row["CouponQty"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
            this.Detail.Rows.Add(row);

            this.BindDetail();

            // this.AnimateRoll("tranctionDetial");
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int couponID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(couponID);
                BindDetail();
            }
        }

        protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStore(StoreID, brandID);
        }

        protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(ddlCouponType, string.Format("BrandID = {0} order by CouponTypeCode", brandID));
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.StoreTel;
            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }

        }

        protected void StoreID_SelectedIndexChanged2(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(FromStoreID.SelectedValue.ToString()));
            if (model != null)
            {
                FromAddress.Text = model.StoreAddressDetail;
                FromContactName.Text = model.Contact;
                FromContactNumber.Text = model.StoreTel;
            }
            else
            {
                FromAddress.Text = "";
                FromContactName.Text = "";
                FromContactNumber.Text = "";
            }

        }

        protected void CustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Customer model = new Edge.SVA.BLL.Customer().GetModel(Tools.ConvertTool.ConverType<int>(CustomerID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.CustomerAddress;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.ContactPhone;

            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        protected void CustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = this.CustomerType.SelectedValue.Trim();
            if (type == "1") //客户
            {
                ddlBrand.SelectedIndex = 0;
                StoreID.SelectedIndex = 0;
                CustomerID.Enabled = true;
                StoreID.Enabled = false;
                ddlBrand.Enabled = false;
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
            else
            {//店铺
                CustomerID.SelectedIndex = 0;
                CustomerID.Enabled = false;
                StoreID.Enabled = true;
                ddlBrand.Enabled = true;
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponOrderForm_D().GetList(string.Format("CouponOrderFormNumber = '{0}'", Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddCouponTypeBrandID(ds, "BrandID", "CouponTypeID");
                    Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
                    Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                    Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
                    Tools.DataTool.AddColumn(ds, "CouponGradeID", "");
                    Tools.DataTool.AddColumn(ds, "CouponGradeCode", "");
                    Tools.DataTool.AddColumn(ds, "CouponGradeName", "");

                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void DeleteDetail(int couponID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(couponID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected override SVA.Model.Ord_CouponOrderForm_H GetPageObject(SVA.Model.Ord_CouponOrderForm_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!this.IsPostBack)
        //    {
        //        this.rptPager.PageSize = webset.ContentPageNum;

        //        ControlTool.BindBrand(ddlBrand);
        //        ControlTool.BindBrand(brandDetail);
        //        ControlTool.BindCustomer(CustomerID);
        //        ControlTool.BindCouponType(ddlCouponType, -1);

        //        ControlTool.AddTitle(ddlBrand);
        //    }
        //}

        //protected override void OnLoadComplete(EventArgs e)
        //{
        //    base.OnLoadComplete(e);
        //    if (!this.IsPostBack)
        //    {
        //        this.CouponOrderFormNumber.Text = Model.CouponOrderFormNumber;
        //        this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
        //        this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
        //        this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
        //        this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

        //        Edge.SVA.Model.Store store = new SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
        //        int brandID = store == null ? -1 : store.BrandID.GetValueOrDefault();
        //        this.ddlBrand.SelectedValue = brandID < 0 ? "" : brandID.ToString();

        //        ControlTool.BindStore(StoreID, brandID);

        //        string storeID = Model.StoreID.GetValueOrDefault().ToString();
        //        this.StoreID.SelectedValue = this.StoreID.Items.FindByValue(storeID) == null ? "" : storeID;

        //        this.BindDetail();
        //    }
        //}

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    Edge.SVA.Model.Ord_CouponOrderForm_H item = this.GetUpdateObject();

        //    if (item == null)
        //    {
        //        Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} No Data", item.CouponOrderFormNumber));
        //        JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
        //        return;
        //    }

        //    item.UpdatedBy = DALTool.GetCurrentUser().UserID;
        //    item.UpdatedOn = System.DateTime.Now;

        //    if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponOrderForm_H>(item))
        //    {
        //        Edge.SVA.BLL.Ord_CouponOrderForm_D bll = new SVA.BLL.Ord_CouponOrderForm_D();
        //        bll.DeleteByOrder(this.CouponOrderFormNumber.Text.Trim());

        //        try
        //        {
        //            DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
        //            DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
        //            database.SetExecuteTimeout(6000);
        //            System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CouponOrderForm_D");
        //            DatabaseUtil.Interface.IExecStatus es = null;
        //            foreach (System.Data.DataRow detail in this.Detail.Rows)
        //            {
        //                System.Data.DataRow row = sourceTable.NewRow();
        //                row["CouponOrderFormNumber"] = item.CouponOrderFormNumber;
        //                row["BrandID"] = detail["BrandID"];
        //                row["CouponTypeID"] = detail["CouponTypeID"];
        //                row["CouponQty"] = detail["CouponQty"];
        //                sourceTable.Rows.Add(row);
        //            }
        //            es = database.InsertBigData(sourceTable, "Ord_CouponOrderForm_D");
        //            if (es.Success)
        //            {
        //                sourceTable.Rows.Clear();
        //            }
        //            else
        //            {
        //                throw es.Ex;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Instance.WriteErrorLog(this.PageName, string.Format("Coupon Order Form  {0} Add Success But Detail Failed", item.CouponOrderFormNumber), ex);
        //            JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
        //            return;
        //        }

        //        Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Update Success", item.CouponOrderFormNumber));
        //        JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
        //    }
        //    else
        //    {
        //        Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Update Failed", item.CouponOrderFormNumber));
        //        JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
        //    }
        //}

        //protected void btnAddDetail_Click(object sender, EventArgs e)
        //{
        //    this.AddDetail();

        //    this.AnimateRoll("tranctionDetial");
        //}

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
        //    LinkButton btn = sender as LinkButton;
        //    if (btn == null) return;

        //    int couponID = -1;
        //    couponID = int.TryParse(btn.Attributes["couponid"], out couponID) ? couponID : -1;
        //    this.DeleteDetail(couponID);

        //    this.AnimateRoll("tranctionDetial");
        //}

        //protected void rptListPager_PageChanged(object sender, EventArgs e)
        //{
        //    this.BindDetail();
        //    this.AnimateRoll("tranctionDetial");
        //}

        //protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int brandID = 0;
        //    brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
        //    ControlTool.BindStore(StoreID, brandID);

        //    ControlTool.AddTitle(ddlBrand);
        //    ControlTool.AddTitle(StoreID);
        //}

        //protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int brandID = 0;
        //    brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
        //    ControlTool.BindCouponType(ddlCouponType, string.Format("BrandID = {0}", brandID));

        //    ControlTool.AddTitle(ddlCouponType);
        //    ControlTool.AddTitle(brandDetail);
        //}

        //private System.Data.DataTable Detail
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult"] == null)
        //        {
        //            System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponOrderForm_D().GetList(string.Format("CouponOrderFormNumber = '{0}'",Request.Params["id"]));
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
        //            Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
        //            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
        //            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
        //            Tools.DataTool.AddColumn(ds, "CouponGradeID", "");
        //            Tools.DataTool.AddColumn(ds, "CouponGradeCode", "");
        //            Tools.DataTool.AddColumn(ds, "CouponGradeName", "");
                  
        //            ViewState["DetailResult"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult"] as System.Data.DataTable;
        //    }
        //}

        //private void AddDetail()
        //{
        //    if (string.IsNullOrEmpty(this.brandDetail.SelectedItem.Value)) throw new Exception("Brand Can't Empty");
        //    if (string.IsNullOrEmpty(this.ddlCouponType.SelectedItem.Value)) throw new Exception("Coupon Type Can't Empty");
        //    if (string.IsNullOrEmpty(this.txtOrderCount.Text)) throw new Exception("Coupon Qty Can't Empty");

        //    foreach (System.Data.DataRow detail in this.Detail.Rows)
        //    {
        //        if (detail["CouponTypeID"].ToString().Equals(this.ddlCouponType.SelectedValue))
        //        {
        //            JscriptPrint(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE);
        //            return;
        //        }
        //    }

        //    System.Data.DataRow row = this.Detail.NewRow();
        //    row["ID"] = this.DetailIndex;
        //    row["BrandID"] = int.Parse(this.brandDetail.SelectedItem.Value);
        //    row["BrandCode"] = this.brandDetail.SelectedItem.Text.Substring(0, this.brandDetail.SelectedItem.Text.IndexOf("-")).Trim();
        //    row["BrandName"] = this.brandDetail.SelectedItem.Text.Substring(this.brandDetail.SelectedItem.Text.IndexOf("-") + 1).Trim();
        //    row["CouponTypeID"] = int.Parse(this.ddlCouponType.SelectedItem.Value);
        //    row["CouponTypeCode"] = this.ddlCouponType.SelectedItem.Text.Substring(0, this.ddlCouponType.SelectedItem.Text.IndexOf("-"));
        //    row["CouponTypeName"] = this.ddlCouponType.SelectedItem.Text.Substring(this.ddlCouponType.SelectedItem.Text.IndexOf("-") + 1);
        //    row["CouponQty"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
        //    this.Detail.Rows.Add(row);

        //    this.BindDetail();
        //}

        //private void DeleteDetail(int couponID)
        //{
        //    foreach (System.Data.DataRow row in this.Detail.Rows)
        //    {
        //        if (row["ID"].ToString().Equals(couponID.ToString()))
        //        {
        //            row.Delete();
        //            break;
        //        }
        //    }
        //    this.Detail.AcceptChanges();
        //    this.BindDetail();
        //}

        //private void BindDetail()
        //{
        //    this.rptPager.RecordCount = this.Detail.Rows.Count;

        //    int index = this.rptPager.CurrentPageIndex > 0 ? this.rptPager.CurrentPageIndex - 1 : 0;

        //    this.rptList.DataSource = DataTool.GetPaggingTable(index, this.rptPager.PageSize, this.Detail);
        //    this.rptList.DataBind();

        //}

        //private int DetailIndex
        //{
        //    get
        //    {
        //        if (ViewState["DetailIndex"] == null)
        //        {
        //            ViewState["DetailIndex"] = 0;
        //        }
        //        ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
        //        return (int)ViewState["DetailIndex"];
        //    }
        //}

        //protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
        //    if (model != null)
        //    {
        //        SendAddress.Text = model.StoreAddressDetail;
        //        ContactName.Text = model.Contact;
        //        ContactNumber.Text = model.StoreTel;

        //    }
        //    else
        //    {
        //        SendAddress.Text = "";
        //        ContactName.Text = "";
        //        ContactNumber.Text = "";
        //    }

        //}

        //protected void CustomerID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Edge.SVA.Model.Customer model = new Edge.SVA.BLL.Customer().GetModel(Tools.ConvertTool.ConverType<int>(CustomerID.SelectedValue.ToString()));
        //    if (model != null)
        //    {
        //        SendAddress.Text = model.CustomerAddress;
        //        ContactName.Text = model.Contact;
        //        ContactNumber.Text = model.ContactPhone;

        //    }
        //    else
        //    {
        //        SendAddress.Text = "";
        //        ContactName.Text = "";
        //        ContactNumber.Text = "";
        //    }

        //}
    }
}