﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponOrderForm_H, Edge.SVA.Model.Ord_CouponOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                this.CouponOrderFormNumber.Text = DALTool.GetREFNOCode(CouponController.CouponRefnoCode.CouponOrderForm);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.lblCreatedBy.Text = Tools.DALTool.GetCurrentUser().UserName;
                this.lblOrderType.Text = "手动";

                ControlTool.BindStore(FromStoreID);
                ControlTool.BindBrand(ddlBrand);
                ControlTool.BindBrand(brandDetail);
                ControlTool.BindStore(StoreID, -1);
                ControlTool.BindCustomer(CustomerID);

                ControlTool.BindCouponType(ddlCouponType, -1);

                CustomerType_SelectedIndexChanged(null, null);

                //ControlTool.AddTitle(ddlBrand);
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            string type = this.CustomerType.SelectedValue.Trim();
            if (type == "1") //客户
            {
                if (this.CustomerID.SelectedValue == "")
                {
                    this.CustomerID.MarkInvalid(String.Format("'{0}' Can't Empty！", CustomerID.Text));
                    return;
                }
            }
            else//店铺
            {
                if (this.ddlBrand.SelectedValue == "")
                {
                    this.ddlBrand.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlBrand.Text));
                    return;
                }
                if (this.StoreID.SelectedValue == "")
                {
                    this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
                    return;
                }
            }

            Edge.SVA.Model.Ord_CouponOrderForm_H item = this.GetAddObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} No Data", item.CouponOrderFormNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            if (this.Detail.Rows.Count <= 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Detail No Data", item.CouponOrderFormNumber));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;
            item.CreatedOn = DateTime.Now;
            item.ApproveOn = null;
            item.ApprovalCode = null;

            if (Tools.DALTool.Add<Edge.SVA.BLL.Ord_CouponOrderForm_H>(item) > 0)
            {
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CouponOrderForm_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CouponOrderFormNumber"] = item.CouponOrderFormNumber;
                        //row["BrandID"] = detail["BrandID"];
                        row["CouponTypeID"] = detail["CouponTypeID"];
                        row["CouponQty"] = detail["CouponQty"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_CouponOrderForm_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Edge.SVA.BLL.Ord_CouponOrderForm_D bll = new SVA.BLL.Ord_CouponOrderForm_D();
                    bll.DeleteByOrder(this.CouponOrderFormNumber.Text.Trim());

                    Edge.SVA.BLL.Ord_CouponOrderForm_H hearder = new SVA.BLL.Ord_CouponOrderForm_H();
                    hearder.Delete(this.CouponOrderFormNumber.Text.Trim());

                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Coupon Order Form  {0} Add Success But Detail Failed", item.CouponOrderFormNumber), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Add Success", item.CouponOrderFormNumber));
               // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Add Failed", item.CouponOrderFormNumber));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (this.brandDetail.SelectedValue == "-1")
            {
                brandDetail.MarkInvalid(String.Format("'{0}' Can't Empty！", brandDetail.Text));
                return;
                //throw new Exception("Brand Can't Empty");
            }
            if (this.ddlCouponType.SelectedValue == "-1")
            {
                ddlCouponType.MarkInvalid(String.Format("'{0}' Can't Empty！", ddlCouponType.Text));
                return;
                // throw new Exception("Coupon Type Can't Empty");
            }
            if (string.IsNullOrEmpty(this.txtOrderCount.Text))
            {
                txtOrderCount.MarkInvalid(String.Format("'{0}' Can't Empty！", txtOrderCount.Text));
                return;
                //throw new Exception("Coupon Qty Can't Empty");
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CouponTypeID"].ToString().Equals(this.ddlCouponType.SelectedValue))
                {
                    //JscriptPrint(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(Resources.MessageTips.ExistCouponTypeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["ID"] = this.DetailIndex;
            row["BrandID"] = int.Parse(this.brandDetail.SelectedItem.Value);
            row["BrandCode"] = this.brandDetail.SelectedItem.Text.Substring(0, this.brandDetail.SelectedItem.Text.IndexOf("-")).Trim();
            row["BrandName"] = this.brandDetail.SelectedItem.Text.Substring(this.brandDetail.SelectedItem.Text.IndexOf("-") + 1).Trim();
            row["CouponTypeID"] = int.Parse(this.ddlCouponType.SelectedItem.Value);
            row["CouponTypeCode"] = this.ddlCouponType.SelectedItem.Text.Substring(0, this.ddlCouponType.SelectedItem.Text.IndexOf("-"));
            row["CouponTypeName"] = this.ddlCouponType.SelectedItem.Text.Substring(this.ddlCouponType.SelectedItem.Text.IndexOf("-") + 1);
            row["CouponQty"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
            this.Detail.Rows.Add(row);

            this.BindDetail();

           // this.AnimateRoll("tranctionDetial");
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int couponID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(couponID);
                BindDetail();
            }
        }

        protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStore(StoreID, brandID);
        }

        protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(ddlCouponType, string.Format("BrandID = {0}  order by CouponTypeCode", brandID));
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.StoreTel;
            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }

        }

        protected void StoreID_SelectedIndexChanged2(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(FromStoreID.SelectedValue.ToString()));
            if (model != null)
            {
                FromAddress.Text = model.StoreAddressDetail;
                FromContactName.Text = model.Contact;
                FromContactNumber.Text = model.StoreTel;
            }
            else
            {
                FromAddress.Text = "";
                FromContactName.Text = "";
                FromContactNumber.Text = "";
            }

        }

        protected void CustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Customer model = new Edge.SVA.BLL.Customer().GetModel(Tools.ConvertTool.ConverType<int>(CustomerID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.CustomerAddress;
                StoreContactName.Text = model.Contact;
                StoreContactPhone.Text = model.ContactPhone;

            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        protected void CustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = this.CustomerType.SelectedValue.Trim();
            if (type == "1") //客户
            {
                ddlBrand.SelectedIndex = 0;
                StoreID.SelectedIndex = 0;
                CustomerID.Enabled = true;
                StoreID.Enabled = false;
                ddlBrand.Enabled = false;
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
            else
            {//店铺
                CustomerID.SelectedIndex = 0;
                CustomerID.Enabled = false;
                StoreID.Enabled = true;
                ddlBrand.Enabled = true;
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StoreContactPhone.Text = "";
            }
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("ID", typeof(int));
                    dt.Columns.Add("BrandID", typeof(int));
                    dt.Columns.Add("BrandCode", typeof(string));
                    dt.Columns.Add("BrandName", typeof(string));
                    dt.Columns.Add("CouponTypeID", typeof(int));
                    dt.Columns.Add("CouponTypeCode", typeof(string));
                    dt.Columns.Add("CouponTypeName", typeof(string));
                    dt.Columns.Add("CouponGradeID", typeof(int));
                    dt.Columns.Add("CouponGradeCode", typeof(string));
                    dt.Columns.Add("CouponGradeName", typeof(string));
                    dt.Columns.Add("CouponQty", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void DeleteDetail(int couponID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(couponID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected override SVA.Model.Ord_CouponOrderForm_H GetPageObject(SVA.Model.Ord_CouponOrderForm_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

    }
}