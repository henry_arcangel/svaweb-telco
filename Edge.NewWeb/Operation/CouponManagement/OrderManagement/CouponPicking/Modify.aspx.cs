﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Messages.Manager;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Modify : Tools.BasePage<SVA.BLL.Ord_CouponPicking_H, SVA.Model.Ord_CouponPicking_H>
    {
        private const string fields = "[KeyID],[CouponPickingNumber],[CouponTypeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCouponNumber],[EndCouponNumber],[BatchCouponCode],[PickupDateTime]";


        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["flg"] = 0;
                this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);
                RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
                RptTotalBind();

                BindBatchList();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;

                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Hidden = true;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Hidden = true;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID);
                this.lblStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                store = new Edge.SVA.BLL.Store().GetModel(Model.FromStoreID);
                this.lblFromStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.lblCustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetOrderPickingApproveStatusString(this.Model.ApproveStatus);
                //this.Remark.Text = Model.Remark.ToString();
                ///this.Remark1.Text = Model.Remark1.ToString();
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手动"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自动"; break;
                    }
                }

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                    this.btnPrint.Visible = false;
                }
                else
                {
                    this.btnPrint.Visible = false;
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }

                string couponTypeList = Controllers.CouponOrderController.GetOrderCouponType(Model.ReferenceNo);
                if (!string.IsNullOrEmpty(couponTypeList))
                {
                    string strWhre = " CouponTypeID in (" + couponTypeList + ")";
                    Edge.Web.Tools.ControlTool.BindCouponType(this.CouponTypeID, strWhre);
                }
                else
                {

                    Edge.Web.Tools.ControlTool.BindCouponType(this.CouponTypeID, " 1>2");
                }
            }
        }


        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            int userID = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            string couponPickingNumber = string.Empty;
            int couponTypeID = 0;
            int QTY = 0;
            int batchID = 0;
            string couponUID = string.Empty;
            string StartCouponNumber = string.Empty;
            int filterType = 0;

            if (string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            {
                ShowWarning(Resources.MessageTips.NoSearchCondition);
                return;
            }

            couponTypeID = ConvertTool.ToInt(this.CouponTypeID.SelectedValue);
            couponPickingNumber = this.CouponPickingNumber.Text.Trim();

            if (cb1.Checked)
            {
                filterType = 1;
                batchID = Tools.ConvertTool.ConverType<int>(this.BatchCouponID.SelectedValue);

                if (batchID <= 0)
                {
                    ShowWarning(Resources.MessageTips.NoSearchCondition);
                    return;
                }
            }
            else if (cb2.Checked)
            {
                filterType = 3;
                StartCouponNumber = this.CouponNumberFirst.Text.Trim();
                QTY = ConvertTool.ToInt(this.CouponCount.Text);

                //if (String.IsNullOrEmpty(StartCouponNumber))
                //{
                //    ShowWarning(Resources.MessageTips.NoSearchCondition);
                //    return;
                //}
            }
            else if (cb3.Checked)
            {
                filterType = 2;
                couponUID = this.CouponUID.Text.Trim();

                if (String.IsNullOrEmpty(couponUID))
                {
                    ShowWarning(Resources.MessageTips.NoSearchCondition);
                    return;
                }
            }


            if (Controllers.CouponOrderController.AddPickupDetailCoupon(userID, couponPickingNumber, couponTypeID, QTY, batchID, couponUID, StartCouponNumber, filterType) == 0)
            {

                string condition = string.Format("CouponPickingNumber = '{0}' and CouponTypeID = {1} and PickQTY = 0", couponPickingNumber, couponTypeID);
                if (new SVA.BLL.Ord_CouponPicking_D().GetRecordCount(condition) == 1)
                {
                    System.Data.DataSet ds = new SVA.BLL.Ord_CouponPicking_D().GetList(condition);
                    new SVA.BLL.Ord_CouponPicking_D().Delete(Tools.ConvertTool.ConverType<int>(ds.Tables[0].Rows[0]["KeyID"].ToString()));
                }
                ReBindingGrid();
                //JscriptPrint(Resources.MessageTips.AddSuccess, "", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
            }
            RptTotalBind();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponPicking_H item = null;
            Edge.SVA.Model.Ord_CouponPicking_H dataItem= this.GetDataObject();

            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P" && dataItem.ApproveStatus.ToUpper().Trim() != "R")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPendingOrNotPicked);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);


            //If repick(status is "R") then set the status to "P";
            if (Request.Params["Status"] != null && Request.Params["Status"] == "R")
                item.ApproveStatus = "P";
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponPicking_H>(item))
            {
                string msg = "";
                if (!Controllers.CouponOrderController.CanApprovePicked(item, out msg))
                {
                    //JscriptPrint(msg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                    //JscriptPrint(msg, "", Resources.MessageTips.WARNING_TITLE);
                    ShowWarning(msg);
                }
                else
                {
                    CloseAndPostBack();
                    //JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                }
            }
            else
            {
                ShowUpdateFailed();
                //JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnSaveAndApprove_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponPicking_H item = null;
            Edge.SVA.Model.Ord_CouponPicking_H dataItem= this.GetDataObject();

            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P" && dataItem.ApproveStatus.ToUpper().Trim() != "R")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPendingOrNotPicked);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);


            //If repick(status is "R") then set the status to "P";
            if (Request.Params["Status"] != null && Request.Params["Status"] == "R")
                item.ApproveStatus = "P";
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponPicking_H>(item))
            {
                string msg = "";
                if (!Controllers.CouponOrderController.CanApprovePicked(item, out msg))
                {
                    ShowWarning(msg);
                }
                else
                {
                    String sb = item.CouponPickingNumber;
                    Window1.Title = Resources.MessageTips.Approve;
                    string okScript = Window1.GetShowReference("Approve.aspx?ids=" + sb);
                    string cancelScript = "";
                    ShowConfirmDialog(MessagesTool.instance.GetMessage("10017") + "\n TXN NO.: \n" + sb.Replace(",", ";\n"), Resources.MessageTips.Approve, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
                    //CloseAndPostBack();
                }
            }
            else
            {
                ShowUpdateFailed();
            }
        }
        //End

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Print.aspx?id={0}", Request.Params["id"]));
        }

        protected void cb1_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1.Checked)
            {
                this.BatchCouponID.Enabled = true;
                this.CouponCount.Enabled = false;
                this.CouponCount.Text = "";
                this.CouponNumberFirst.Enabled = false;
                this.CouponNumberFirst.Text = "";
                this.CouponUID.Enabled = false;
                this.CouponUID.Text = "";

                this.cb2.Checked = false;
                this.cb3.Checked = false;

                BindBatchList();
            }
        }

        protected void cb2_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2.Checked)
            {
                this.BatchCouponID.Enabled = false;
                this.BatchCouponID.Text = "";
                this.CouponCount.Enabled = true;
                this.CouponNumberFirst.Enabled = true;
                this.CouponUID.Enabled = false;
                this.CouponUID.Text = "";

                this.cb1.Checked = false;
                this.cb3.Checked = false;

                BindBatchList();
            }
        }

        protected void cb3_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3.Checked)
            {
                this.BatchCouponID.Enabled = false;
                this.BatchCouponID.Text = "";
                this.CouponCount.Enabled = false;
                this.CouponCount.Text = "";
                this.CouponNumberFirst.Enabled = false;
                this.CouponNumberFirst.Text = "";
                this.CouponUID.Enabled = true;

                this.cb1.Checked = false;
                this.cb2.Checked = false;

                BindBatchList();
            }
        }

        //protected void Grid2_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        //{
        //    if (e.CommandName == "Delete")
        //    {
        //        object[] keys = Grid2.DataKeys[e.RowIndex];
        //        int ordPickingNumber = Tools.ConvertTool.ConverType<int>(keys[0].ToString());

        //        Edge.SVA.BLL.Ord_CouponPicking_D bll = new SVA.BLL.Ord_CouponPicking_D();
        //        Edge.SVA.Model.Ord_CouponPicking_D model = bll.GetModel(ordPickingNumber);
        //        if (model == null) return;

        //        if (bll.Delete(ordPickingNumber))
        //        {
        //            ReBindingGrid();
        //        }
        //        else
        //        {
        //            ShowWarning(Resources.MessageTips.DeleteFailed);
        //            // JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
        //        }

        //        if (bll.GetRecordCount(string.Format("CouponPickingNumber='{0}' and CouponTypeID = {1}", Request.Params["id"], model.CouponTypeID)) <= 0)
        //        {
        //            model.PickQTY = 0;
        //            model.FirstCouponNumber = null;
        //            model.EndCouponNumber = null;
        //            model.PickupDateTime = null;
        //            model.BatchCouponCode = null;

        //            if (bll.Add(model) > 0)
        //            {
        //                ReBindingGrid();
        //            }
        //            else
        //            {
        //                ShowWarning(Resources.MessageTips.DeleteFailed);
        //                //JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
        //            }
        //        }

        //        RptTotalBind();
        //    }
        //}
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFirstCouponNumber.Text))
            {
                if (string.IsNullOrEmpty(sCouponQty.Text))
                {
                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(sCouponUID1.Text))
            {
                if (string.IsNullOrEmpty(sCouponQty.Text))
                {
                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text))
            {

            }
            else if (!string.IsNullOrEmpty(sCouponUID2.Text))
            {

            }
            else
            {
                this.sFirstCouponNumber.MarkInvalid("Please select the query conditions!");
                return;
            }

            ViewState["flg"] = 2;
            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            System.Data.DataSet ds1 = null;
            if (this.RecordCount <=0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex);
            }


            Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddCouponStockStatusByID(ds, "StockStatus", "FirstCouponNumber");
            DataTool.AddCouponStockStatus(ds, "StockStatusName", "StockStatus");
            DataTool.AddCouponUIDByCouponNumber(ds, "FirstCouponUID", "FirstCouponNumber");
            DataTool.AddCouponUIDByCouponNumber(ds, "EndCouponUID", "EndCouponNumber");

            //add start
            //if (Convert.ToInt32(ViewState["flg"]) != 0)
            //{
            //    DataTable dt3 = (DataTable)ViewState["dt3"];
            //    foreach (DataRow dr3 in dt3.Rows)
            //    {
            //        DataRow[] dr = ds.Tables[0].Select("FirstCouponNumber='" + dr3["FirstCouponNumber"].ToString() + "'");
            //        if (dr.Length > 0)
            //        {
            //            ds.Tables[0].Rows.Remove(dr[0]);
            //        }
            //    }
            //}
            //add end

            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();

            //add start
            if (Convert.ToInt32(ViewState["flg"]) == 0)
            {
                DataTable dt2 = ds.Tables[0].Clone();
                this.Grid3.DataSource = dt2;
                this.Grid3.DataBind();
                //DataTable dt3 = ds.Tables[0].Clone();
                ViewState["dt2"] = dt2;
                //ViewState["dt3"] = dt3;
            }
            else if (Convert.ToInt32(ViewState["flg"]) == 2)
            {
                string strWhere1="";
                string endCouponNumber;
                string endCouponUID;
                if (!string.IsNullOrEmpty(sFirstCouponNumber.Text))
                {
                    endCouponNumber = (Convert.ToInt64(sFirstCouponNumber.Text) + (Convert.ToInt32(sCouponQty.Text) - 1)).ToString();
                    strWhere1 = " (FirstCouponNumber >= '" + sFirstCouponNumber.Text + "' " + " and FirstCouponNumber<='" + endCouponNumber + "') ";
                }
                else if (!string.IsNullOrEmpty(sCouponUID1.Text))
                {
                    string couponNumbers = "";
                    endCouponUID = (Convert.ToInt64(sCouponUID1.Text) + (Convert.ToInt32(sCouponQty.Text) - 1)).ToString();
                    DataSet dsCouponUIDMap = new Edge.SVA.BLL.CouponUIDMap().GetList("CouponUID >= '" + sCouponUID1.Text + "' " + " and CouponUID<='" + endCouponUID + "'");
                    foreach (DataRow drCouponUIDMap in dsCouponUIDMap.Tables[0].Rows)
                    {
                        couponNumbers += "'" + Convert.ToString(drCouponUIDMap["CouponNumber"]) + "',";
                    }
                    if (couponNumbers == "")
                    {
                        couponNumbers = "''";
                    }
                    else 
                    {
                       couponNumbers = couponNumbers.TrimEnd(',');
                    }
                    strWhere1 = " FirstCouponNumber in (" + couponNumbers + ") ";
                }
                else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text))
                {
                    strWhere1 = " FirstCouponNumber = '" + sCurrentCouponNumber.Text + "' ";

                }
                else if (!string.IsNullOrEmpty(sCouponUID2.Text))
                {
                    string couponNumber = "";
                    DataSet dsCouponUIDMap = new Edge.SVA.BLL.CouponUIDMap().GetList("CouponUID ='" + sCouponUID2.Text + "'");
                    if (dsCouponUIDMap.Tables[0].Rows.Count > 0)
                    {
                        couponNumber = Convert.ToString(dsCouponUIDMap.Tables[0].Rows[0]["CouponNumber"]);
                    }
                    strWhere1 = " FirstCouponNumber = '" + couponNumber + "' ";
                }
                bll.StrWhere = bll.StrWhere + " and " + strWhere1; 
                ds1 = bll.GetList(this.Grid3.PageSize, this.Grid3.PageIndex);
                Tools.DataTool.AddCouponTypeName(ds1, "CouponType", "CouponTypeID");
                Tools.DataTool.AddCouponTypeCode(ds1, "CouponTypeCode", "CouponTypeID");
                Tools.DataTool.AddCouponStockStatusByID(ds1, "StockStatus", "FirstCouponNumber");
                DataTool.AddCouponStockStatus(ds1, "StockStatusName", "StockStatus");
                DataTool.AddCouponUIDByCouponNumber(ds1, "FirstCouponUID", "FirstCouponNumber");
                DataTool.AddCouponUIDByCouponNumber(ds1, "EndCouponUID", "EndCouponNumber");
                this.Grid3.DataSource = ds1.Tables[0].DefaultView;
                this.Grid3.DataBind();
                ViewState["dt2"] = ds1.Tables[0];
            }
            //add end
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return 0;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : 0;
            }
            set
            {
                if (value < 0) return;
                this.Grid2.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid3.PageIndex = e.NewPageIndex;

            DataTable dt2 = (DataTable)ViewState["dt2"];
            this.Grid3.DataSource = dt2;
            this.Grid3.DataBind();
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;
            ViewState["flg"] = 1;

            this.Grid2.PageIndex = e.NewPageIndex;

            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);

        }

        protected void Grid2_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            //string CouponTypeCode = "";
            //string CouponType = "";
            //string OrderQTY = "";

            if (e.DataItem is DataRowView)
            {
                //显示格式
                Label lblCouponTypeCode = Grid2.Rows[e.RowIndex].FindControl("lblCouponTypeCode") as Label;
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)Grid2.Rows[e.RowIndex].FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)Grid2.Rows[e.RowIndex].FindControl("lblOrderQTY");
                    Label lblSeq = Grid2.Rows[e.RowIndex].FindControl("lblSeq") as Label;
                    HiddenField hfCouponTypeID = Grid2.Rows[e.RowIndex].FindControl("hfCouponTypeID") as HiddenField;
                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
                    }
                }
            }

        }

        private Dictionary<int, int> CouponTypeIndex
        {
            get
            {
                if (ViewState["CouponTypeIndex"] == null)
                {
                    ViewState["CouponTypeIndex"] = new SVA.BLL.Ord_CouponPicking_D().GetCouponTypeIndex(Request.Params["id"]);
                }
                return ViewState["CouponTypeIndex"] as Dictionary<int, int>;
            }
        }

        #endregion

        #region 捡回汇总列表
        private void RptTotalBind()
        {
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

            System.Data.DataSet ds = bll.GetListGroupByCouponType(string.Format("CouponPickingNumber='{0}'", Request.Params["id"].Trim()));

            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddID(ds, "ID", this.Grid2.PageSize, this.Grid2.PageIndex);
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();

            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);

            lblGrid1TotalOrderQTY.Text = totalOrderQTY.ToString();
            lblGrid1TotalPickQTY.Text = totalPickQTY.ToString();

            if (!Controllers.CouponOrderController.IsMeetPickingByType(totalOrderQTY, totalPickQTY))
            {
                lblGrid1TotalPickQTY.CssStyle = "color:red;font-weight:bold;";
            }
        }

        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                Label orderQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblOrderQTY1");
                Label pickQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblPickQTY1");

                long longOrderQTY = Tools.ConvertTool.ConverType<long>(orderQTY.Text);
                long longPickQTY = Tools.ConvertTool.ConverType<long>(pickQTY.Text);

                if (!Controllers.CouponOrderController.IsMeetPickingByType(longOrderQTY, longPickQTY))
                {
                    pickQTY.ForeColor = System.Drawing.Color.Red;
                    pickQTY.Font.Bold = true;
                }
            }
        }
        #endregion

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected override SVA.Model.Ord_CouponPicking_H GetPageObject(SVA.Model.Ord_CouponPicking_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void BindBatchList()
        {
            if (cb1.Checked)
            {
                if (CouponTypeID.SelectedValue != "")
                {
                    Tools.ControlTool.BindBatchID(BatchCouponID, Tools.ConvertTool.ConverType<int>(CouponTypeID.SelectedValue));
                }
                else
                {
                    Tools.ControlTool.BindBatchID(BatchCouponID);
                }
            }
            else
            {
                BatchCouponID.Items.Clear();
            }
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindBatchList();
        }

        private void ReBindingGrid()
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;
            ViewState["flg"] = 1;

            this.RecordCount = 0;
            this.Grid2.PageIndex = 0;
            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID", fields);
        }

        protected void btnSelectAll_OnClick(object sender, EventArgs e)
        {
            Grid3.SelectAllRows();
        }


        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            DataTable dt2 = (DataTable)ViewState["dt2"];
            foreach (int i in Grid3.SelectedRowIndexArray)
            {
                string keyID = Grid3.DataKeys[i][0].ToString();
                DataRow[] dr = dt2.Select("KeyID=" + keyID);
                if (dr.Length > 0)
                {
                    dt2.Rows.Remove(dr[0]);
                }
                Edge.SVA.BLL.Ord_CouponPicking_D bll = new SVA.BLL.Ord_CouponPicking_D();

                if (bll.Delete(Convert.ToInt32(keyID)))
                {
                    ReBindingGrid();
                }
                else
                {
                    ShowWarning(Resources.MessageTips.DeleteFailed);
                }
            }
            this.Grid3.DataSource = dt2;
            this.Grid3.DataBind();
            ViewState["dt2"] = dt2;
        }

    }
}