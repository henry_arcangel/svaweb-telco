﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintAR.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking.PrintAR" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div align="center" style="width: 100%; text-align: center;">
        <%--<asp:Button ID="btnPrint" runat="server" Text="打印" OnClientClick="printdiv('div_print')" />--%>
        <%--        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
            class="submit" />--%>
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table  width="100%" border="0">
                    <tr>
                        <th colspan="6" align="center">
                            <b>GIFT CERTIFICATE ACKNOWLEDGEMENT RECEIPT<b>
                        </th>
                    </tr>
					<tr> 
						<td width="16.6%">
							&nbsp;
						</td>
						<td width="16.6%">
							&nbsp;
						</td> 
						<td width="16.6%">
							&nbsp;
						</td> 
						<td width="16.6%">
							&nbsp;
						</td> 
						<td width="16.6%">
							&nbsp;
						</td> 
						<td width="16.6%">
							&nbsp;
						</td> 
					</tr>
                    <tr>
                        <td>
                            Date:
                        </td>
                        <td colspan="3">
                            <b><%#Eval("ApproveOn")%></b>
                        </td>
						<td>
                            A/R#:
                        </td>
                        <td align="center">
                            <b><%#Eval("CouponPickingNumber")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td colspan="2">
                           <b><%#Eval("StoreName")%></b>
                        </td>
                    </tr>
					<tr> 
						<td colspan="6">
							&nbsp;
						</td>
					</tr>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td colspan="5">
                            <b><%#Eval("FromStoreName")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Re:
                        </td>
                        <td colspan="5">
                            <%--<%#Eval("CouponTypeName")%>--%>
                            <tr>
                            <td>
                                <asp:Repeater ID="rptCouponTypeList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left" colspan="5">
                                                <asp:Label ID="lblCouponTypeList" runat="server" Text='<%#Eval("CouponTypeName")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
							<td>
							</td>
                            </td>
                            </tr>
                        </td>
                    </tr>
					<tr>
						<td colspan="6">
							&nbsp;
						</td>
					</tr>
                    <tr>
                        <td>
                            Issuance:
                        </td>
                        <td style="border-bottom:1px solid black;" colspan="4">
                            <%#Eval("Remark")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Customer:
                        </td>
                        <td style="border-bottom:1px solid black;" colspan="4">
                            <%#Eval("Remark1")%>
                        </td>
                    </tr>
					
					
                    <%--<tr>
                        <td colspan="6">
                            Please acknowledge the receipt of:
                        </td>
                        <td colspan="2">
                            <%#Eval("CouponTypeName")%>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            worth:
                        </td>
                        <td colspan="2">
                            <%#Eval("TotalAmount")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            P <%#Eval("TotalAmount")%> as follows
                        </td>
                    </tr>
					
					
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Repeater ID="rptOrderSummary" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td width="33.2%">
                                                Please acknowledge the receipt of
                                            </td>
											<td width="16.6%">
                                                &nbsp;
                                            </td>
											<td width="16.6%">
                                                &nbsp;
                                            </td>
											<td width="16.6%">
                                                &nbsp;
                                            </td>
											<td width="16.6%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td width="16.6%" align="left">
                                            &nbsp;
                                        </td>
                                        <td colspan="2" style="border-bottom:1px solid black;" align="left">
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("CouponTypeName")%>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            worth:
                                        </td>
                                        <td colspan="2" style="border-bottom:1px solid black;" align="right" colspan="2">
                                          <b>  <asp:Label ID="Label1" runat="server" Text='<%#Eval("CouponAmount")%>'></asp:Label> </b>
                                        </td>
                                    </tr>
                                </ItemTemplate>
								
								
								
<%--                                <FooterTemplate>
                                    <tr>
                                        <td>
                                            Total of:
                                        </td>
                                        <td>
                                           <%#Eval("TotalAmount")%>
                                        </td>
                                        <td>
                                            as follows
                                        </td>
                                    </tr>
                                </FooterTemplate>--%>
								
								
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
					<tr>
						<td width="16.6%">
                            &nbsp;
                        </td>
                        <td colspan="5">
                            GC Series
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                    <table id="prefix" width="100%" border="0">
                                        <tr>
                                            <td width="16.6%" align="center">
                                                <b><i><u>Prefix</u></i></b>
                                            </td>
                                            <td width="16.6%" align="center">
                                                <b><i><u>From</u></i></b>
                                            </td>
                                            <td width="16.6%" align="center">
                                                <b><i><u>To</u></i></b>
                                            </td>
                                            <td width="16.6%" align="center">
                                                <b><i><u>Quantity</u></i></b>
                                            </td>
                                            <td width="16.6%" align="center">
                                                <b><i><u>Denomination</u></i></b>
                                            </td>
                                            <td width="16.6%" align="center">
                                                <b><i><u>Amount</u></i></b>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblCouponNumPattern" runat="server" Text='<%#Eval("CouponNumPattern")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblFirstCouponNumber" runat="server" Text='<%#Eval("FirstCouponNumber")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblEndCouponNumber" runat="server" Text='<%#Eval("EndCouponNumber")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCouponAmount" runat="server" Text='<%#Eval("CouponAmount")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
									<tr>
										<td colspan="6">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td colspan="6">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td colspan="6">
											&nbsp;
										</td>
									</tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            TOTAL
                                        </td>
                                        <td align="center">
                                           <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="center">
                                           <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
					<tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                     <tr>
                        <td colspan="6">
                            <table width="100%" border="0"
                                runat="server" id="dtEnd">
                                <tr>
                                    <td width="33.2%">
                                        Prepared by:
                                    </td>
                                    <td width="33.2%">
                                        A/R#:   <b><%#Eval("CouponPickingNumber")%></b>
                                    </td>
                                    <td width="33.2%">
                                        Received by:
                                    </td>
                                </tr>
								<tr>
									<td colspan="6">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="6">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="6">
										&nbsp;
									</td>
								</tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
