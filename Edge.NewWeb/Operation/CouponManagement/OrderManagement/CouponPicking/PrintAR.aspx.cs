﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.Operation.CouponManagement.OrderManagement.CouponPicking;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class PrintAR : PageBase
    {
        DataSet ds;
        DataSet detail;
        DataSet summary;
        CouponPickingController controller = new CouponPickingController();
        int iTotalPickQTY;
        double iTotalAmount;

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Request.Params["id"];

                ds = new Edge.SVA.BLL.Ord_CouponPicking_H().GetList("CouponPickingNumber ='" + id + "'");
                ds.Tables[0].Columns.Add("CouponTypeID", typeof(string));
                ds.Tables[0].Columns.Add("TotalAmount", typeof(string));
                //detail = controller.GetDetailList(id);
                detail = controller.GetDetailListGroup(id); //Modified By Robin 2014-08-06 for RRG group coupon number
                detail.Tables[0].Columns.Add("Amount", typeof(string));
                if (detail.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Rows[0]["CouponTypeID"] = Convert.ToString(detail.Tables[0].Rows[0]["CouponTypeID"]);
                }
                else 
                {
                    ds.Tables[0].Rows[0]["CouponTypeID"] = "-1";
                }
                //Add By Robin 2014-09-02
                summary = controller.GetCouponTypeList(id);
                Tools.DataTool.AddCouponTypeNameByID(summary, "CouponTypeName", "CouponTypeID");
                //
                Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
                Tools.DataTool.AddStoreName(ds, "FromStoreName", "FromStoreID");
                Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");
                Tools.DataTool.AddCustomerDesc(ds, "CustomerName", "CustomerID");
                foreach (DataRow dr in detail.Tables[0].Rows) 
                {
                    int iPickQTY;
                    double iCouponAmount;
                    try
                    {
                        iPickQTY = Convert.ToInt32(dr["PickQTY"]);
                        iCouponAmount = Convert.ToDouble(dr["CouponAmount"]);
                    }
                    catch 
                    {
                        iPickQTY = 0;
                        iCouponAmount = 0;
                    }
                    double iAmount = iCouponAmount;
                    dr["CouponAmount"] = (iCouponAmount/iPickQTY).ToString("F2");
                    dr["Amount"] = iCouponAmount.ToString("F2");
                    iTotalPickQTY += iPickQTY;
                    iTotalAmount += iCouponAmount;
                }

                ds.Tables[0].Rows[0]["TotalAmount"] = iTotalAmount.ToString("F2");

                this.rptOrders.DataSource = ds.Tables[0];
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                lblTotalPickQTY.Text = iTotalPickQTY.ToString();
                Label lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");
                lblTotalAmount.Text = iTotalAmount.ToString("F2");
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                list.DataSource = detail.Tables[0];
                list.DataBind();

                //Add By Robin 2014-09-02
                Repeater list1 = e.Item.FindControl("rptOrderSummary") as Repeater;
                if (list1 == null) return;

                System.Data.DataRowView drv1 = e.Item.DataItem as System.Data.DataRowView;
                if (drv1 == null) return;

                list1.DataSource = summary.Tables[0];
                list1.DataBind();

                Repeater list2 = e.Item.FindControl("rptCouponTypeList") as Repeater;
                if (list2 == null) return;

                System.Data.DataRowView drv2 = e.Item.DataItem as System.Data.DataRowView;
                if (drv2 == null) return;

                list2.DataSource = summary.Tables[0];
                list2.DataBind();

                //Label lblTotalAmount1 = (Label)e.Item.FindControl("lblTotalAmount1");
                //lblTotalAmount1.Text = iTotalAmount.ToString("F2");
                //End
            }
        }

        #endregion


        #region 数据列表绑定

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}