﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_OrderToSupplier_H, Edge.SVA.Model.Ord_OrderToSupplier_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCouponType(CouponType, -1);
                ControlTool.BindCouponType(CouponTypeID);
                ControlTool.BindCompanyID(CompanyID);

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.OrderSupplierNumber.Text = Model.OrderSupplierNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.SupplierAddress.Text = Model.SupplierAddress;
                this.SuppliertContactName.Text = Model.SuppliertContactName;
                this.SupplierPhone.Text = Model.SupplierPhone;

                //ControlTool.BindCouponTypeBySupplierID(this.CouponTypeID, Convert.ToInt32(SupplierID.SelectedValue));

                this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.SendAddress.Text = Model.SendAddress;
                this.StoreContactName.Text = Model.StoreContactName;
                this.StorePhone.Text = Model.StorePhone;

                this.CouponTypeID.SelectedValue = Model.CouponTypeID.ToString();
                this.CouponQty.Text = Model.CouponQty.ToString();

                this.IsProvideNumber.SelectedValue = Model.IsProvideNumber.ToString();

                this.BindDetail();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {

            if (this.SupplierID.SelectedValue == "-1")
            {
                this.SupplierID.MarkInvalid(String.Format("'{0}' Can't Empty！", SupplierID.Text));
                return;
            }
            //if (this.StoreID.SelectedValue == "")
            //{
            //    this.StoreID.MarkInvalid(String.Format("'{0}' Can't Empty！", StoreID.Text));
            //    return;
            //}

            //if (this.CouponTypeID.SelectedValue == "-1")
            //{
            //    this.CouponTypeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CouponTypeID.Text));
            //    return;
            //}

            Edge.SVA.Model.Ord_OrderToSupplier_H item = null;
            Edge.SVA.Model.Ord_OrderToSupplier_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            item = this.GetPageObject(dataItem);

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_OrderToSupplier_H>(item))
            {
                Edge.SVA.BLL.Ord_OrderToSupplier_D bll = new SVA.BLL.Ord_OrderToSupplier_D();
                bll.DeleteByOrder(this.OrderSupplierNumber.Text.Trim());
                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_OrderToSupplier_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["OrderSupplierNumber"] = item.OrderSupplierNumber;
                        row["CouponTypeID"] = detail["CouponTypeID"];
                        row["OrderQty"] = detail["OrderQty"];
                        row["PackageQty"] = detail["PackageQty"];
                        row["OrderRoundUpQty"] = detail["OrderRoundUpQty"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_OrderToSupplier_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, "Update", ex);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order to Supplier {0} Update Success", item.OrderSupplierNumber));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order to Supplier {0} Update Failed", item.OrderSupplierNumber));
                ShowAddFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StorePhone.Text = model.StoreTel;
            }
            else
            {
                SendAddress.Text = "";
                StoreContactName.Text = "";
                StorePhone.Text = "";
            }

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            if (model != null)
            {
                SupplierAddress.Text = model.SupplierAddress;
                SuppliertContactName.Text = model.Contact;
                SupplierPhone.Text = model.ContactPhone;
            }
            else
            {
                SupplierAddress.Text = "";
                SuppliertContactName.Text = "";
                SupplierPhone.Text = "";
            }

            //ControlTool.BindCouponTypeBySupplierID(this.CouponTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_OrderToSupplier_D().GetList(string.Format("OrderSupplierNumber = '{0}'", Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;
                    ds.Tables[0].Columns.Add("ID", typeof(int));
                    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                    Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");
                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void BindDetail()
        {
            this.Grid1.RecordCount = this.Detail.Rows.Count;
            this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            this.Grid1.DataBind();
        }

        protected override SVA.Model.Ord_OrderToSupplier_H GetPageObject(SVA.Model.Ord_OrderToSupplier_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                object[] keys = Grid1.DataKeys[e.RowIndex];
                int couponTypeID = Tools.ConvertTool.ConverType<int>(keys[0].ToString());
                DeleteDetail(couponTypeID);
                BindDetail();
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (this.CouponTypeID.SelectedValue == "-1" || string.IsNullOrEmpty(this.CouponQty.Text))
            {
                this.CouponTypeID.MarkInvalid(String.Format("'{0}' Can't Empty！", CouponTypeID.Text));
                return;
            }

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CouponTypeID"].ToString().Equals(this.CouponTypeID.SelectedValue))
                {
                    ShowWarning(Resources.MessageTips.ExistCouponTypeCode);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["CouponTypeID"] = int.Parse(this.CouponTypeID.SelectedItem.Value);
            row["CouponTypeCode"] = this.CouponTypeID.SelectedItem.Text.Substring(0, this.CouponTypeID.SelectedItem.Text.IndexOf("-"));
            row["CouponTypeName"] = this.CouponTypeID.SelectedItem.Text.Substring(this.CouponTypeID.SelectedItem.Text.IndexOf("-") + 1);
            //row["OrderQty"] = int.Parse(this.CouponQty.Text.Replace(",", "").Trim());
            int CouponRoundUpQty = Tools.DALTool.GetReplenishment(int.Parse(this.StoreID.SelectedItem.Value), int.Parse(this.CouponTypeID.SelectedItem.Value), 1); //Modified by Robin 2014-11-14
            int PackageQty = int.Parse(this.CouponQty.Text.Replace(",", "").Trim()) / CouponRoundUpQty;
            row["OrderQty"] = CouponRoundUpQty * PackageQty;
            row["PackageQty"] = PackageQty;
            row["OrderRoundUpQty"] = CouponRoundUpQty;
            this.Detail.Rows.Add(row);

            this.BindDetail();
        }

        private void DeleteDetail(int couponTypeID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["CouponTypeID"].ToString().Equals(couponTypeID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

    }
}