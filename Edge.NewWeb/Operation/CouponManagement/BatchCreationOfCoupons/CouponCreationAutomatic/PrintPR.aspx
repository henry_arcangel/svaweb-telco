﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPR.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic.PrintPR" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Style/default.css" rel="stylesheet" type="text/css" />
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>--%>
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div id="toolsbar" align="center" style="width: 100%; text-align: center;">
            <table align="center">
                <tr align="center">
                    <td>
                        <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭" OnClick="btnClose_Click"></ext:Button>
                    </td>
                </tr>
            </table>
    </div>
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" cellspacing="0">
                    <tr>
                        <th width="16.6%" align="center">
                            <b>MEMO</b>
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
                        <th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Date:
                        </td>
                        <td colspan="5">
                            <b><%#Eval("ApproveOn")%></b>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td colspan="5">
                            <b><%#Eval("SupplierName")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td colspan="5">
                            <b><%#Eval("CompanyName")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Re:
                        </td>
                        <td colspan="5">
                            <b><%#Eval("Subject")%></b>
                        </td>
                    </tr>
                    <tr style="border-bottom:1px solid black;">
						<td colspan="7">
							&nbsp;
						</td>
                    </tr>		 
                    <tr>
                        <td colspan="6">
                            This is to request the preparation of a Purchase Order for the following item/s:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td align="right">
                            Item/s:
                        </td> 
						<td colspan="4">
							<asp:Repeater ID="rptCouponTypeList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
								<ItemTemplate>
									<tr>
										<td align="left">
											&nbsp;
										</td>
										<td align="left">
											&nbsp;
										</td>
										<td colspan="2"  align="left">
											<b><asp:Label ID="lblQty" runat="server" Text='<%#Eval("CouponTypeName")%>'></asp:Label></b>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</td>
                            
                        
                        <%--<td colspan="2">
                            <%#Eval("CouponTypeName")%>
                        </td>--%>
                    </tr>
					<tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Details as follows:
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td align="center">
                            <b>(Special Instructions)<b>					
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>					
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                                <HeaderTemplate>
                                        <tr>
                                            <th>
                                                <center><b><u>GC Denomination</u></b></center>
                                            </th>
                                            <th>
                                                <center><b><u>Qty</u></b></center>
                                            </th>
                                            <th>
                                                <center><b><u>Unit</u></b></center>
                                            </th>
                                            <th>
                                                <center><b><u>Unit Cost<br />(vat inclusive)</u></b></center>
                                            </th>
                                            <th>
                                                <center><b><u>Amount<br />(vat inclusive)</u></b></center>
                                            </th>
                                            <th>
                                                <center><b><u>Expiry Date</u></b></center>
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <b>P<asp:Label ID="lblCouponTypeAmount" runat="server" Text='<%#Eval("CouponTypeAmount")%>'></asp:Label>s</b>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblQty" runat="server" Text='<%#Eval("Qty")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblUnit" runat="server" Text="bundles"></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblUnitCost" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblAmount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblExpiryDate" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                    </tr>
                                    <table width="83.3%" cellspacing=0 frame=border>
									<tr>
										&nbsp;									
									</tr>
                                    <tr>
                                        <td width="16.6%" align="center">
                                            <b>TOTAL</b>
                                        </td>
                                        <td width="16.6%"  align="center">
                                           <asp:Label ID="lblTotalQty" runat="server"></asp:Label>
                                        </td>
                                        <td width="16.6%" align="center">
                                            <b>bundles</b>
                                        </td>
                                        <td width="16.6%" align="center">
                                            &nbsp;
                                        </td>
                                        <td width="16.6%" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                    <tr class="top_border">
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <table width="83.3%" frame=box>
                    <tr>
                        <td width="16.6%" align="left">
                            GC Series -
                        </td>
						<td width="16.6%">
                            &nbsp;
                        </td>
						<td width="16.6%">
                            &nbsp;
                        </td>
						<td width="16.6%">
                            &nbsp;
                        </td>
						<td width="16.6%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Repeater ID="rptOrderList2" runat="server" OnItemDataBound="rptOrderList2_ItemDataBound">
                                <HeaderTemplate>
                                        <tr>
                                            <th colspan="2">
                                                <u><center>Denomination</center></u>
                                            </th>
                                            <th>
                                                <u><center>Prefix</center></u>
                                            </th>
                                            <th>
                                                <u><center>From</center></u>
                                            </th>
                                            <th>
                                               <u><center>To</center></u>
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <b><asp:Label ID="lblCouponTypeAmountAddCouponQty" runat="server" Text='<%#Eval("CouponTypeAmountAddCouponQty")%>'></asp:Label></b>
                                        </td>
                                        <td align="center">
                                            <b><asp:Label ID="lblCouponNumPattern" runat="server" Text='<%#Eval("CouponNumPattern")%>'></asp:Label></b>
                                        </td>
                                        <td align="center">
                                            <b><asp:Label ID="lblFirstCouponNumber" runat="server" Text='<%#Eval("FirstCouponNumber")%>'></asp:Label></b>
                                        </td>
                                        <td align="center">
                                            <b><asp:Label ID="lblEndCouponNumber" runat="server" Text='<%#Eval("EndCouponNumber")%>'></asp:Label></b>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    </table>
                    <table width="100%" border=0>
                    <tr>
                        <th width="16.6%">
                            &nbsp;
                        </th>
                        <th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
						<th width="16.6%">
                            &nbsp;
                        </th>
                    </tr>
					<tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
						<td colspan="3">
                            Thank you.
                        </td>
						<td colspan="3">
                            <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            ____________________
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedByUserName")%>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>					
                    <tr>
                        <td colspan="2">
                            Approved by:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            ____________________
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Eval("ApprovedByUserName")%>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
