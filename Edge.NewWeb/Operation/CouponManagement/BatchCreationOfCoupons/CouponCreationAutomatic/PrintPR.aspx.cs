﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic
{
    public partial class PrintPR : PageBase
    {
        DataSet ds;
        DataSet detail;
        DataSet detail2;
        double iTotalQty;
        SVA.Model.CouponType model;

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Request.Params["id"];

                ds = new Edge.SVA.BLL.Ord_OrderToSupplier_H().GetList("OrderSupplierNumber ='" + id + "'");
                Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
                Tools.DataTool.AddSupplierDesc(ds, "SupplierName", "SupplierID");
                Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");
                Tools.DataTool.AddCompanyName(ds, "CompanyName", "CompanyID");
                Tools.DataTool.AddUserName(ds, "CreatedByUserName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApprovedByUserName", "ApproveBy");

                //detail = new Edge.SVA.BLL.Ord_OrderToSupplier_H().GetList("OrderSupplierNumber ='" + id + "'");
                //model = new Edge.SVA.BLL.CouponType().GetModel(Convert.ToInt32(detail.Tables[0].Rows[0]["CouponTypeID"]));
                //detail.Tables[0].Columns.Add("CouponTypeAmount", typeof(string));
                //detail.Tables[0].Columns.Add("Qty", typeof(string));
                //detail.Tables[0].Rows[0]["CouponTypeAmount"] = model.CouponTypeAmount.ToString("F2");
                //try
                //{
                //    double iQty = Convert.ToInt32(detail.Tables[0].Rows[0]["CouponQty"]) /  Convert.ToInt32(detail.Tables[0].Rows[0]["PackageQty"]);
                //    detail.Tables[0].Rows[0]["Qty"] = iQty.ToString();
                //}
                //catch 
                //{
                //    detail.Tables[0].Rows[0]["Qty"] = "0";
                //}
                //foreach (DataRow dr in detail.Tables[0].Rows)
                //{
                //    iTotalQty += Convert.ToDouble(detail.Tables[0].Rows[0]["Qty"]);
                //}

                CouponCreationAutomaticController controller = new CouponCreationAutomaticController();
                //Add By Robin 2014-08-13
                detail = controller.GetCouponTypeList(id);
                detail.Tables[0].Columns.Add("CouponTypeAmount", typeof(string));
                detail.Tables[0].Columns.Add("Qty", typeof(string));
                Tools.DataTool.AddCouponTypeNameByID(detail, "CouponTypeName", "CouponTypeID"); //Add By Robin 2014-09-02
                foreach (DataRow dr in detail.Tables[0].Rows)
                {
                    model = new Edge.SVA.BLL.CouponType().GetModel(Convert.ToInt32(dr["CouponTypeID"]));
                    dr["CouponTypeAmount"] = model.CouponTypeAmount.ToString("F2");
                    try
                    {
                        double iQty = Convert.ToInt32(dr["OrderQty"]);
                        dr["Qty"] = iQty.ToString("N0");
                    }
                    catch
                    {
                        dr["Qty"] = "0";
                    }
                    iTotalQty += Convert.ToDouble(dr["Qty"]);
                }

                //End


                detail2 = controller.GetCouponNumberRange(id);
                detail2.Tables[0].Columns.Add("CouponTypeAmountAddCouponQty", typeof(string));
                detail2.Tables[0].Columns.Add("CouponNumPattern", typeof(string));
                char[] CCouponNumber;
                CCouponNumber = new char[64];
                string sCouponTypeAmountAddCouponQty;
                foreach (DataRow dr in detail2.Tables[0].Rows) 
                {
                    model = new Edge.SVA.BLL.CouponType().GetModel(Convert.ToInt32(dr["CouponTypeID"]));
                    sCouponTypeAmountAddCouponQty = model.CouponTypeAmount.ToString("F2") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToInt32(dr["OrderQty"]).ToString("N0") + " pcs.";
                    dr["CouponTypeAmountAddCouponQty"] = sCouponTypeAmountAddCouponQty;
                    dr["CouponNumPattern"] = Convert.ToString(model.CouponNumPattern);
                    //Add By Robin 2014-07-18
                    try
                    {
                        Convert.ToString(dr["FirstCouponNumber"]).CopyTo(Convert.ToString(dr["CouponNumPattern"]).Length, CCouponNumber, 0, Convert.ToString(dr["FirstCouponNumber"]).Length - Convert.ToString(dr["CouponNumPattern"]).Length);
                        dr["FirstCouponNumber"] = new string(CCouponNumber);
                        Convert.ToString(dr["EndCouponNumber"]).CopyTo(Convert.ToString(dr["CouponNumPattern"]).Length, CCouponNumber, 0, Convert.ToString(dr["EndCouponNumber"]).Length - Convert.ToString(dr["CouponNumPattern"]).Length);
                        dr["EndCouponNumber"] = new string(CCouponNumber);
                    }
                    catch
                    { }
                    //End
                }

                this.rptOrders.DataSource = ds.Tables[0];
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalQty = (Label)e.Item.FindControl("lblTotalQty");
                lblTotalQty.Text = iTotalQty.ToString("N0");
            }
        }

        protected void rptOrderList2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                list.DataSource = detail.Tables[0];
                list.DataBind();


                Repeater list2 = e.Item.FindControl("rptOrderList2") as Repeater;
                if (list2 == null) return;

                System.Data.DataRowView drv2 = e.Item.DataItem as System.Data.DataRowView;
                if (drv2 == null) return;

                list2.DataSource = detail2.Tables[0];
                list2.DataBind();

                Repeater list1 = e.Item.FindControl("rptCouponTypeList") as Repeater;
                if (list1 == null) return;

                System.Data.DataRowView drv1 = e.Item.DataItem as System.Data.DataRowView;
                if (drv1 == null) return;

                list1.DataSource = detail.Tables[0];
                list1.DataBind();
            }
        }

        #endregion


        #region 数据列表绑定

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}