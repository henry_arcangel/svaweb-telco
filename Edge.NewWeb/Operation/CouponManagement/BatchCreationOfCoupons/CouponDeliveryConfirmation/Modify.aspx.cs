﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.Controllers.Operation.CouponManagement.BatchCreationOfCoupons.CouponDeliveryConfirmation;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponDeliveryConfirmation
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponReceive_H, Edge.SVA.Model.Ord_CouponReceive_H>
    {
        private static string ApproveStatusValue; //Add By Robin 2014-11-25 for static stock status

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.Grid1.PageSize = webset.ContentPageNum;
                //this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid3.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCouponType(CouponType, -1);
                ControlTool.BindCouponType(CouponTypeID1);

                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CouponReceiveNumber.Text = Model.CouponReceiveNumber;
                this.lblReferenceNo.Text = Model.ReferenceNo;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.SupplierAddress.Text = Model.SupplierAddress;
                this.SuppliertContactName.Text = Model.SuppliertContactName;
                this.SupplierPhone.Text = Model.SupplierPhone;

                //ControlTool.BindCouponTypeBySupplierID(this.CouponTypeID, Convert.ToInt32(SupplierID.SelectedValue));

                this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.StorerAddress.Text = Model.StorerAddress;
                this.StoreContactName.Text = Model.StoreContactName;
                this.StorePhone.Text = Model.StorePhone;

                //this.CouponTypeID.SelectedValue = Model.CouponTypeID.ToString();
                //this.CouponQty.Text = Model.CouponQty.ToString();

                ApproveStatusValue = this.Model.ApproveStatus; //Add By Robin 2014-11-25 for static stock status
                this.BindDetail3();
                //this.BindDetail();
                //this.BindDetail2();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponReceive_H item = null;
            Edge.SVA.Model.Ord_CouponReceive_H dataItem = this.GetDataObject();

            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }

            item = this.GetPageObject(dataItem);

            item.ReceiveType = 1;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponReceive_H>(item))
            {
                try
                {
                    CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
                    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                    //DataTable dt = (DataTable)ViewState["DetailResult"];
                    //DataTable dt2 = (DataTable)ViewState["DetailResult2"];
                    controller.UpdateCouponStockStatus(dt3);
                    //controller.UpdateCouponStockStatus(dt, 2);
                    //controller.UpdateCouponStockStatus(dt2, 3);
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Coupon Delivery Confirmation {0} Update Success But Detail Failed", item.CouponReceiveNumber), ex);
                    ShowAddFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Delivery Confirmation {0} Update Success", item.CouponReceiveNumber));
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Delivery Confirmation {0} Update Failed", item.CouponReceiveNumber));
                ShowAddFailed();
            }
        }

        protected void Grid3_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //Search 前先保存 Robin 2014-10-11
            try
            {
                CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCouponStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            Grid3.PageIndex = e.NewPageIndex;
            this.BindDetail3();
        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid1.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        //protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    Grid2.PageIndex = e.NewPageIndex;
        //    this.BindDetail2();
        //}

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                StorerAddress.Text = model.StoreAddressDetail;
                StoreContactName.Text = model.Contact;
                StorePhone.Text = model.StoreTel;
            }
            else
            {
                StorerAddress.Text = "";
                StoreContactName.Text = "";
                StorePhone.Text = "";
            }

        }

        protected void SupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Supplier model = new Edge.SVA.BLL.Supplier().GetModel(Tools.ConvertTool.ConverType<int>(SupplierID.SelectedValue.ToString()));
            if (model != null)
            {
                SupplierAddress.Text = model.SupplierAddress;
                SuppliertContactName.Text = model.Contact;
                SupplierPhone.Text = model.ContactPhone;
            }
            else
            {
                SupplierAddress.Text = "";
                SuppliertContactName.Text = "";
                SupplierPhone.Text = "";
            }

            //ControlTool.BindCouponTypeBySupplierID(this.CouponTypeID, Convert.ToInt32(SupplierID.SelectedValue));
        }


        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        private System.Data.DataTable Detail3
        {
            get
            {
                //if (ViewState["DetailResult3"] == null) //Removed By Robin 2014-10-10 important to show page by page
                {
                    int type = 0;
                    string para1 = "";
                    string para2 = "";
                    string ReceiveNumber = ""; //Add By Robin 2014-09-04
                    

                    if (!string.IsNullOrEmpty(sFirstCouponNumber.Text))
                    {
                        if (string.IsNullOrEmpty(sCouponQty.Text))
                        {
                            this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                            ViewState["DetailResult3"] = new DataTable();
                            return ViewState["DetailResult3"] as System.Data.DataTable;
                        }
                        else
                        {
                            type = 1;
                            para1 = sFirstCouponNumber.Text;
                            para2 = sCouponQty.Text;
                        }
                    }
                    else if (!string.IsNullOrEmpty(sCouponUID1.Text))
                    {
                        if (string.IsNullOrEmpty(sCouponQty.Text))
                        {
                            this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                            ViewState["DetailResult3"] = new DataTable();
                            return ViewState["DetailResult3"] as System.Data.DataTable;
                        }
                        else
                        {
                            type = 2;
                            para1 = sCouponUID1.Text;
                            para2 = sCouponQty.Text;
                        }
                    }
                    else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text))
                    {
                        type = 3;
                        para1 = sCurrentCouponNumber.Text;
                        para2 = "";
                    }
                    else if (!string.IsNullOrEmpty(sCouponUID2.Text))
                    {
                        type = 4;
                        para1 = sCouponUID2.Text;
                        para2 = "";
                    }
                    else
                    {
                        type = 0;
                        para1 = "";
                        para2 = "";
                    }

                    //Add By Robin 2014-10-10
                    int from, to;
                    from = this.Grid3.PageIndex * this.Grid3.PageSize + 1;
                    to = (this.Grid3.PageIndex + 1) * this.Grid3.PageSize;
                    //End

                    CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
                    if (para1 != "" || para2 != "") { ReceiveNumber = CouponReceiveNumber.Text; } //Add By Robin 2014-09-04
                    System.Data.DataSet ds = controller.GetDetailList(type, ReceiveNumber, StoreID.SelectedItem.Text, CouponTypeID1.SelectedValue, para1, para2,from,to,startCouponNumber);
                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    //Tools.DataTool.AddCouponUIDByCouponNumber(ds, "CouponUID", "FirstCouponNumber");
                    //Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");
                    //Add By Robin 2014-11-25 for static card status
                    int StockStatus = 0;
                    int Status = 0;
                    if (ApproveStatusValue == "A") { StockStatus = 2; }
                    else { StockStatus = 1; }
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        row["StatusName"] = Tools.DALTool.GetCouponTypeStatusName(Status);
                        row["CouponStockStatusName"] = Tools.DALTool.GetCouponStockStatusName(StockStatus);
                    }
                    //End

                    ViewState["DetailResult3"] = ds.Tables[0];
                }
                return ViewState["DetailResult3"] as System.Data.DataTable;
            }
        }

        private int DetailCount
        {
            get
            {
                int rtn = DALTool.GetCouponReceiveDetailCount(CouponReceiveNumber.Text,"");
                return rtn;
            }
        }

        //private System.Data.DataTable Detail
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult"] == null)
        //        {
        //            int type = 0;
        //            string para1 = "";
        //            string para2 = "";

        //            if (!string.IsNullOrEmpty(sFirstCouponNumber.Text)) 
        //            {
        //                if (string.IsNullOrEmpty(sCouponQty.Text))
        //                {
        //                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
        //                    ViewState["DetailResult"] = new DataTable();
        //                    return ViewState["DetailResult"] as System.Data.DataTable;
        //                }
        //                else 
        //                {
        //                    type = 1;
        //                    para1 = sFirstCouponNumber.Text;
        //                    para2 = sCouponQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCouponUID1.Text)) 
        //            {
        //                if (string.IsNullOrEmpty(sCouponQty.Text))
        //                {
        //                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
        //                    ViewState["DetailResult"] = new DataTable();
        //                    return ViewState["DetailResult"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 2;
        //                    para1 = sCouponUID1.Text;
        //                    para2 = sCouponQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text)) 
        //            {
        //                type = 3;
        //                para1 = sCurrentCouponNumber.Text;
        //                para2 = "";
        //            }
        //            else if (!string.IsNullOrEmpty(sCouponUID2.Text))
        //            {
        //                type = 4;
        //                para1 = sCouponUID2.Text;
        //                para2 = "";
        //            }
        //            else 
        //            {
        //                type = 0;
        //                para1 = "";
        //                para2 = "";
        //            }

        //            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CouponReceiveNumber.Text, 2, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCouponUIDByCouponNumber(ds, "CouponUID", "FirstCouponNumber");

        //            ViewState["DetailResult"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult"] as System.Data.DataTable;
        //    }
        //}

        //private System.Data.DataTable Detail2
        //{
        //    get
        //    {
        //        if (ViewState["DetailResult2"] == null)
        //        {
        //            int type = 0;
        //            string para1 = "";
        //            string para2 = "";

        //            if (!string.IsNullOrEmpty(sFirstCouponNumber.Text))
        //            {
        //                if (string.IsNullOrEmpty(sCouponQty.Text))
        //                {
        //                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
        //                    ViewState["DetailResult2"] = new DataTable();
        //                    return ViewState["DetailResult2"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 1;
        //                    para1 = sFirstCouponNumber.Text;
        //                    para2 = sCouponQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCouponUID1.Text))
        //            {
        //                if (string.IsNullOrEmpty(sCouponQty.Text))
        //                {
        //                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
        //                    ViewState["DetailResult2"] = new DataTable();
        //                    return ViewState["DetailResult2"] as System.Data.DataTable;
        //                }
        //                else
        //                {
        //                    type = 2;
        //                    para1 = sCouponUID1.Text;
        //                    para2 = sCouponQty.Text;
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text))
        //            {
        //                type = 3;
        //                para1 = sCurrentCouponNumber.Text;
        //                para2 = "";
        //            }
        //            else if (!string.IsNullOrEmpty(sCouponUID2.Text))
        //            {
        //                type = 4;
        //                para1 = sCouponUID2.Text;
        //                para2 = "";
        //            }
        //            else
        //            {
        //                type = 0;
        //                para1 = "";
        //                para2 = "";
        //            }

        //            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
        //            System.Data.DataSet ds = controller.GetDetailList(type, CouponReceiveNumber.Text, 3, StoreID.SelectedItem.Text, para1, para2);
        //            if (ds == null || ds.Tables.Count <= 0) return null;

        //            Tools.DataTool.AddID(ds, "ID", 0, 0);
        //            Tools.DataTool.AddCouponUIDByCouponNumber(ds, "CouponUID", "FirstCouponNumber");

        //            ViewState["DetailResult2"] = ds.Tables[0];
        //        }
        //        return ViewState["DetailResult2"] as System.Data.DataTable;
        //    }
        //}

        private void BindDetail3()
        {
            //this.Grid3.RecordCount = DetailCount;
            if (!string.IsNullOrEmpty(sCouponQty.Text))
            {
                this.Grid3.RecordCount = Int32.Parse(sCouponQty.Text.ToString());
            }
            //this.Grid3.RecordCount = this.Detail3.Rows.Count;
            this.Grid3.DataSource = DataTool.GetPaggingTable(0, this.Grid3.PageSize, this.Detail3);
            this.Grid3.DataBind();
        }

        //private void BindDetail()
        //{
        //    this.Grid1.RecordCount = this.Detail.Rows.Count;
        //    this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
        //    this.Grid1.DataBind();
        //}

        //private void BindDetail2()
        //{
        //    this.Grid2.RecordCount = this.Detail2.Rows.Count;
        //    this.Grid2.DataSource = DataTool.GetPaggingTable(this.Grid2.PageIndex, this.Grid2.PageSize, this.Detail2);
        //    this.Grid2.DataBind();
        //}

        protected override SVA.Model.Ord_CouponReceive_H GetPageObject(SVA.Model.Ord_CouponReceive_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
	    //Add By Robin 2014-12-24 to Fix 无法显示1条记录问题
            string sCouponQtyText;
            sCouponQtyText = this.sCouponQty.Text;
            if (this.sCouponQty.Text == "") { this.sCouponQty.Text = "2"; }
            //End
            //Search 前先保存 Robin 2014-07-30
            try
            {
                CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
                DataTable dt3 = (DataTable)ViewState["DetailResult3"];
                controller.UpdateCouponStockStatus(dt3);
            }
            catch (Exception ex)
            {
               return;
            }
            //End

            ViewState["DetailResult3"] = null;
            //ViewState["DetailResult"] = null;
            //ViewState["DetailResult2"] = null;
            this.BindDetail3();

            this.sCouponQty.Text = sCouponQtyText;//Add By Robin 2014-12-24 to Fix 无法显示1条记录问题

            //this.BindDetail();
            //this.BindDetail2();
        }

        //protected void btnAdd1_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)ViewState["DetailResult"];
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid3.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid3.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt3.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt3.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt.NewRow();
        //            dr["StoreName"] = dt3.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt3.Rows[i]["KeyID"];
        //            dr["CouponReceiveNumber"] = dt3.Rows[i]["CouponReceiveNumber"];
        //            dr["CouponTypeID"] = dt3.Rows[i]["CouponTypeID"];
        //            dr["Description"] = dt3.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt3.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt3.Rows[i]["ActualQTY"];
        //            dr["FirstCouponNumber"] = dt3.Rows[i]["FirstCouponNumber"];
        //            dr["EndCouponNumber"] = dt3.Rows[i]["EndCouponNumber"];
        //            dr["BatchCouponCode"] = dt3.Rows[i]["BatchCouponCode"];
        //            dr["CouponStockStatus"] = dt3.Rows[i]["CouponStockStatus"];
        //            dr["ReceiveDateTime"] = dt3.Rows[i]["ReceiveDateTime"];
        //            dr["CouponExpiryDate"] = dt3.Rows[i]["CouponExpiryDate"];
        //            dr["StatusName"] = dt3.Rows[i]["StatusName"];
        //            dr["CouponStockStatusName"] = dt3.Rows[i]["CouponStockStatusName"];
        //            dr["ID"] = dt3.Rows[i]["ID"];
        //            dr["CouponUID"] = dt3.Rows[i]["CouponUID"];
        //            dt.Rows.InsertAt(dr, 0);
        //            dt3.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult"] = dt;
        //    ViewState["DetailResult3"] = dt3;
        //    this.BindDetail();
        //    this.BindDetail3();
        //}

        //protected void btnDelete1_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)ViewState["DetailResult"];
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid1.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid1.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt.Rows.Count -1;i > -1; i--) 
        //    {
        //        if (keyId.Contains(dt.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt3.NewRow();
        //            dr["StoreName"] = dt.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt.Rows[i]["KeyID"];
        //            dr["CouponReceiveNumber"] = dt.Rows[i]["CouponReceiveNumber"];
        //            dr["CouponTypeID"] = dt.Rows[i]["CouponTypeID"];
        //            dr["Description"] = dt.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt.Rows[i]["ActualQTY"];
        //            dr["FirstCouponNumber"] = dt.Rows[i]["FirstCouponNumber"];
        //            dr["EndCouponNumber"] = dt.Rows[i]["EndCouponNumber"];
        //            dr["BatchCouponCode"] = dt.Rows[i]["BatchCouponCode"];
        //            dr["CouponStockStatus"] = dt.Rows[i]["CouponStockStatus"];
        //            dr["ReceiveDateTime"] = dt.Rows[i]["ReceiveDateTime"];
        //            dr["CouponExpiryDate"] = dt.Rows[i]["CouponExpiryDate"];
        //            dr["StatusName"] = dt.Rows[i]["StatusName"];
        //            dr["CouponStockStatusName"] = dt.Rows[i]["CouponStockStatusName"];
        //            dr["ID"] = dt.Rows[i]["ID"];
        //            dr["CouponUID"] = dt.Rows[i]["CouponUID"];
        //            dt3.Rows.InsertAt(dr,0);
        //            dt.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult"] = dt;
        //    ViewState["DetailResult3"] = dt3;
        //    this.BindDetail();
        //    this.BindDetail3();
        //}

        //protected void btnAdd2_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    DataTable dt2 = (DataTable)ViewState["DetailResult2"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid3.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid3.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt3.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt3.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt2.NewRow();
        //            dr["StoreName"] = dt3.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt3.Rows[i]["KeyID"];
        //            dr["CouponReceiveNumber"] = dt3.Rows[i]["CouponReceiveNumber"];
        //            dr["CouponTypeID"] = dt3.Rows[i]["CouponTypeID"];
        //            dr["Description"] = dt3.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt3.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt3.Rows[i]["ActualQTY"];
        //            dr["FirstCouponNumber"] = dt3.Rows[i]["FirstCouponNumber"];
        //            dr["EndCouponNumber"] = dt3.Rows[i]["EndCouponNumber"];
        //            dr["BatchCouponCode"] = dt3.Rows[i]["BatchCouponCode"];
        //            dr["CouponStockStatus"] = dt3.Rows[i]["CouponStockStatus"];
        //            dr["ReceiveDateTime"] = dt3.Rows[i]["ReceiveDateTime"];
        //            dr["CouponExpiryDate"] = dt3.Rows[i]["CouponExpiryDate"];
        //            dr["StatusName"] = dt3.Rows[i]["StatusName"];
        //            dr["CouponStockStatusName"] = dt3.Rows[i]["CouponStockStatusName"];
        //            dr["ID"] = dt3.Rows[i]["ID"];
        //            dr["CouponUID"] = dt3.Rows[i]["CouponUID"];
        //            dt2.Rows.InsertAt(dr, 0);
        //            dt3.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult3"] = dt3;
        //    ViewState["DetailResult2"] = dt2;
        //    this.BindDetail3();
        //    this.BindDetail2();
        //}

        //protected void btnDelete2_OnClick(object sender, EventArgs e)
        //{
        //    DataTable dt3 = (DataTable)ViewState["DetailResult3"];
        //    DataTable dt2 = (DataTable)ViewState["DetailResult2"];
        //    List<string> keyId = new List<string>();
        //    foreach (int i in Grid2.SelectedRowIndexArray)
        //    {
        //        keyId.Insert(0, Grid2.DataKeys[i][0].ToString());
        //    }
        //    for (int i = dt2.Rows.Count - 1; i > -1; i--)
        //    {
        //        if (keyId.Contains(dt2.Rows[i]["KeyID"].ToString()))
        //        {
        //            DataRow dr = dt3.NewRow();
        //            dr["StoreName"] = dt2.Rows[i]["StoreName"];
        //            dr["KeyID"] = dt2.Rows[i]["KeyID"];
        //            dr["CouponReceiveNumber"] = dt2.Rows[i]["CouponReceiveNumber"];
        //            dr["CouponTypeID"] = dt2.Rows[i]["CouponTypeID"];
        //            dr["Description"] = dt2.Rows[i]["Description"];
        //            dr["OrderQTY"] = dt2.Rows[i]["OrderQTY"];
        //            dr["ActualQTY"] = dt2.Rows[i]["ActualQTY"];
        //            dr["FirstCouponNumber"] = dt2.Rows[i]["FirstCouponNumber"];
        //            dr["EndCouponNumber"] = dt2.Rows[i]["EndCouponNumber"];
        //            dr["BatchCouponCode"] = dt2.Rows[i]["BatchCouponCode"];
        //            dr["CouponStockStatus"] = dt2.Rows[i]["CouponStockStatus"];
        //            dr["ReceiveDateTime"] = dt2.Rows[i]["ReceiveDateTime"];
        //            dr["CouponExpiryDate"] = dt2.Rows[i]["CouponExpiryDate"];
        //            dr["StatusName"] = dt2.Rows[i]["StatusName"];
        //            dr["CouponStockStatusName"] = dt2.Rows[i]["CouponStockStatusName"];
        //            dr["ID"] = dt2.Rows[i]["ID"];
        //            dr["CouponUID"] = dt2.Rows[i]["CouponUID"];
        //            dt3.Rows.InsertAt(dr, 0);
        //            dt2.Rows.RemoveAt(i);
        //        }
        //    }
        //    ViewState["DetailResult3"] = dt3;
        //    ViewState["DetailResult2"] = dt2;
        //    this.BindDetail3();
        //    this.BindDetail2();
        //}

        protected void btnExport_Click(object sender, EventArgs e)
        {
            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            DataTable dt = controller.GetExportList(this.CouponReceiveNumber.Text);
            SVA.BLL.Ord_CouponReceive_H bll = new SVA.BLL.Ord_CouponReceive_H();
            string fileName = controller.UpLoadFileToServer(bll.GetModel(this.CouponReceiveNumber.Text), dt);

            try
            {
                string exportname = "SalesInvoice_PO.xls";
                Tools.ExportTool.ExportFile(fileName, exportname);
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
            }

        }

        //protected void chkGood_Changed(object sender, System.EventArgs e)
        //{
        //    foreach (GridRow gvr in this.Grid1.Rows)   
        //    {   
        //        Control ctl = gvr.FindControl("chkGood");
        //        System.Web.UI.WebControls.CheckBox cb;
        //        cb = (System.Web.UI.WebControls.CheckBox)ctl;
        //        if (cb.Checked)   
        //        {
        //            Control ctl1 = gvr.FindControl("chkDamaged");
        //            System.Web.UI.WebControls.CheckBox cb1;
        //            cb1 = (System.Web.UI.WebControls.CheckBox)ctl1;
        //            cb1.Checked = false;
        //        }   
        //    }   
        //}

        //protected void chkDamaged_Changed(object sender, System.EventArgs e)
        //{
        //    System.Web.UI.WebControls.CheckBox cb;
        //    cb = (System.Web.UI.WebControls.CheckBox)sender;
        //    if (cb.Checked)
        //    { 
        //        //
        //    }
        //}

        protected void btnAddGood_OnClick(object sender, EventArgs e) 
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows) 
            {
                if (dr3["KeyID"].ToString() == txtKeyID.Text)
                {
                    if (dr3["CouponStockStatus"].ToString() == "2")
                    {
                        dr3["CouponStockStatus"] = 1;
                    }
                    else
                    {
                        dr3["CouponStockStatus"] = 2;
                    }
                    break;
                }
            }
            ViewState["DetailResult3"] = dt3;
            //保存
            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            try
            {
                controller.UpdateCouponStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            this.BindDetail3();
        }

        protected void btnAddDamaged_OnClick(object sender, EventArgs e)
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows)
            {
                if (dr3["KeyID"].ToString() == txtKeyID.Text)
                {
                    if (dr3["CouponStockStatus"].ToString() == "3")
                    {
                        dr3["CouponStockStatus"] = 1;
                    }
                    else
                    {
                        dr3["CouponStockStatus"] = 3;
                    }
                }
            }
            ViewState["DetailResult3"] = dt3;
            //保存
            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            try
            {
                controller.UpdateCouponStockStatus(dt3);
            }
            catch (Exception ex)
            {
                return;
            }
            //End
            this.BindDetail3();
        }

        protected void chkAll_Changed(object sender, System.EventArgs e) 
        {
            DataTable dt3 = this.Detail3;
            foreach (DataRow dr3 in dt3.Rows)
            {
                if (chkAll.Checked)
                {
                    dr3["CouponStockStatus"] = 2;
                }
                else
                {
                    dr3["CouponStockStatus"] = 1;
                }

            }
            ViewState["DetailResult3"] = dt3;
            this.BindDetail3();
        }

        private string startCouponNumber="";

        protected void chkAllGood_Changed(object sender, System.EventArgs e)
        {
            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            Grid3.PageIndex = 0;
            DataTable dt3;
            for (int j = 0; j < Grid3.PageCount; j++)
            {
                dt3 = this.Detail3;
                foreach (DataRow dr3 in dt3.Rows)
                {
                    if (chkAllGood.Checked)
                    {
                        dr3["CouponStockStatus"] = 2;
                    }
                    else
                    {
                        dr3["CouponStockStatus"] = 1;
                    }
                    startCouponNumber = dr3["FirstCouponNumber"].ToString();
                }
                //保存
                try
                {
                    controller.UpdateCouponStockStatus(dt3);
                }
                catch (Exception ex)
                {
                    return;
                }
                //End
                Grid3.PageIndex = j + 1;
                //ViewState["DetailResult3"] = dt3;
            }
            Grid3.PageIndex = 0;
            startCouponNumber = "";
            this.BindDetail3();
        }

        protected void chkAllDamage_Changed(object sender, System.EventArgs e)
        {
            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            Grid3.PageIndex = 0;
            DataTable dt3;
            for (int j = 0; j < Grid3.PageCount; j++)
            {
                dt3 = Detail3;
                foreach (DataRow dr3 in dt3.Rows)
                {
                    if (chkAllDamage.Checked)
                    {
                        dr3["CouponStockStatus"] = 3;
                    }
                    else
                    {
                        dr3["CouponStockStatus"] = 1;
                    }
                    startCouponNumber = dr3["FirstCouponNumber"].ToString();
                }
                //保存
                try
                {
                    controller.UpdateCouponStockStatus(dt3);
                }
                catch (Exception ex)
                {
                    return;
                }
                //End
                Grid3.PageIndex = j + 1;
                //ViewState["DetailResult3"] = dt3;
            }
            Grid3.PageIndex = 0;
            startCouponNumber = "";
            this.BindDetail3();
            //int startIndex = Grid3.PageIndex * Grid3.PageSize + 1;
            //int endIndex = (Grid3.PageIndex + 1) * Grid3.PageSize;
            //int index = 1;
            //DataTable dt3 = this.Detail3;
            //foreach (DataRow dr3 in dt3.Rows)
            //{
            //    if (index >= startIndex && index <= endIndex)
            //    {
            //        if (chkAllDamage.Checked)
            //        {
            //            dr3["CouponStockStatus"] = 3;
            //        }
            //        else
            //        {
            //            dr3["CouponStockStatus"] = 1;
            //        }
            //    }
            //    index++;
            //}
            //ViewState["DetailResult3"] = dt3;
            //this.BindDetail3();
        }

        protected void SaveToGoodButton_Click(object sender, EventArgs e)
        {
            UpdateDetail("2");
            BindDetail3();
        }

        protected void SaveToDamageButton_Click(object sender, EventArgs e)
        {
            UpdateDetail("3");
            BindDetail3();
        }

        public Boolean UpdateDetail(string para3)
        {
            int type=0;
            string CouponTypeID;
            string para1="";
            string para2="";
            string sql = "";
            string sql1 = "";

            CouponTypeID = CouponTypeID1.SelectedValue;
            string ReceiveNumber = ""; //Add By Robin 2014-09-04


            if (!string.IsNullOrEmpty(sFirstCouponNumber.Text))
            {
                if (string.IsNullOrEmpty(sCouponQty.Text))
                {
                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                }
                else
                {
                    type = 1;
                    para1 = sFirstCouponNumber.Text;
                    para2 = sCouponQty.Text;
                }
            }
            else if (!string.IsNullOrEmpty(sCouponUID1.Text))
            {
                if (string.IsNullOrEmpty(sCouponQty.Text))
                {
                    this.sCouponQty.MarkInvalid(String.Format("'{0}' Can't Empty！", sCouponQty.Text));
                }
                else
                {
                    type = 2;
                    para1 = sCouponUID1.Text;
                    para2 = sCouponQty.Text;
                }
            }
            else if (!string.IsNullOrEmpty(sCurrentCouponNumber.Text))
            {
                type = 3;
                para1 = sCurrentCouponNumber.Text;
                para2 = "";
            }
            else if (!string.IsNullOrEmpty(sCouponUID2.Text))
            {
                type = 4;
                para1 = sCouponUID2.Text;
                para2 = "";
            }
            else
            {
                type = 0;
                para1 = "";
                para2 = "";
            }

            CouponDeliveryConfirmationController controller = new CouponDeliveryConfirmationController();
            if (para1 != "" || para2 != "") { ReceiveNumber = CouponReceiveNumber.Text; } //Add By Robin 2014-09-04

            if (CouponTypeID != "-1")
            { sql1 = " and a.CouponTypeID=" + CouponTypeID + " "; }

            if (type == 0)
            {
                sql = " update Ord_CouponReceive_D set CouponStockStatus=" + para3 + " where CouponReceiveNumber='" + ReceiveNumber + "'  ";
            }
            else if (type == 1)
            {
                sql = " update top (" + para2 + ")  Ord_CouponReceive_D set CouponStockStatus=" + para3 + " where CouponReceiveNumber='" + ReceiveNumber + "' and FirstCouponNumber >= '" + para1 + "'  ";
            }
            else if (type == 2)
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                sql = " update top (" + para2 + ")  Ord_CouponReceive_D set CouponStockStatus=" + para3 + " where CouponReceiveNumber='" + ReceiveNumber + "' and FirstCouponNumber >= '" + couponNumber + "'  ";
            }
            else if (type == 3)
            {
                sql = " update Ord_CouponReceive_D set CouponStockStatus=" + para3 + " where CouponReceiveNumber='" + ReceiveNumber + "' and FirstCouponNumber = '" + para1 + "'  ";
            }
            else
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                sql = " update Ord_CouponReceive_D set CouponStockStatus=" + para3 + " where CouponReceiveNumber='" + ReceiveNumber + "' and FirstCouponNumber = '" + couponNumber + "'  ";
            }
            DataSet ds3 = DBUtility.DbHelperSQL.Query(sql);
            return true;
        }
    }
}