﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponDeliveryConfirmation.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function SetGood(chk) {
            document.getElementById("Panel1_GroupPanel5_sForm5_ctl00_txtKeyID").value = chk.title;
            document.getElementById("Panel1_GroupPanel5_Grid3_Toolbar4_btnAddGood").click(); 
        }
        function SetDamaged(chk) {
            document.getElementById("Panel1_GroupPanel5_sForm5_ctl00_txtKeyID").value = chk.title;
            document.getElementById("Panel1_GroupPanel5_Grid3_Toolbar4_btnAddDamaged").click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                <%--    <ext:Button ID="btnExport" Icon="PageExcel" OnClick="btnExport_Click" runat="server" Text="导出报表">
                    </ext:Button>--%>
                      <ext:Button ID="Button1" runat="server" Text="导出报表"
                        OnClick="btnExport_Click" Icon="PageExcel" EnableAjax="false" DisableControlBeforePostBack="false">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="交易信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Label="交易状态：" Text="P">
                    </ext:HiddenField>
                    <ext:HiddenField ID="OrderType" runat="server" Label="订单类型：" Text="0">
                    </ext:HiddenField>
                    <ext:Form ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CouponReceiveNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="lblOrderType" runat="server" Label="订单类型：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="lblReferenceNo" runat="server" Label="参考编号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:Label ID="lblCreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Remark" runat="server" Label="备注：">
                                    </ext:TextBox>
                                    <ext:Label ID="lblApproveBy" runat="server" Label="批核人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="Remark1" runat="server" Label="备注2：">
                                    </ext:TextBox>
                                    <ext:TextBox ID="Remark2" runat="server" Label="备注3：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="库存(出)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="SupplierID" runat="server" Label="供货商：" OnSelectedIndexChanged="SupplierID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" Enabled="false">
                                    </ext:DropDownList>
                                    <ext:TextBox ID="SupplierAddress" runat="server" Label="地址：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="SuppliertContactName" runat="server" Label="联系人：" Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="SupplierPhone" runat="server" Label="联系电话：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="库存(入)地点信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="StoreID" runat="server" Label="总部：" OnSelectedIndexChanged="StoreID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" Enabled="false" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <ext:TextBox ID="StorerAddress" runat="server" Label="地址：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="StoreContactName" runat="server" Label="联系人：" Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="StorePhone" runat="server" Label="联系电话：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
<%--            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="优惠券信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sform3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="CouponTypeID" runat="server" Label="优惠券类型：" OnSelectedIndexChanged="CouponTypeID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Required="true" ShowRedStar="true" Enabled="false" 
                                        CompareType="String" CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <ext:NumberBox ID="CouponQty" runat="server" Label="优惠券的数量：" MaxValue="10000" 
                                        NoDecimal="true" NoNegative="true" Required="true" ShowRedStar="true" Enabled="false">
                                    </ext:NumberBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>--%>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="优惠券列表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow  ColumnWidths="45% 45% 10%">
                                <Items>
                                    <ext:DropDownList ID="CouponTypeID1" runat="server" Label="优惠券类型：" OnSelectedIndexChanged="CouponTypeID_SelectedIndexChanged"
                                        AutoPostBack="True" Resizable="true" Enabled="true" >
                                    </ext:DropDownList>
                                    <ext:TextBox ID="sCouponUID1" runat="server" Label="第一张优惠券物理编号：">
                                    </ext:TextBox>
                                    <ext:TextBox ID="txtKeyID" runat="server" Label="" Hidden="true">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow  ColumnWidths="45% 45% 10%">
                                <Items>
                                    <ext:TextBox ID="sFirstCouponNumber" runat="server" Label="第一张优惠券号码：">
                                    </ext:TextBox>
                                    <ext:TextBox ID="sCouponUID2" runat="server" Label="优惠券物理编号：">
                                    </ext:TextBox>
                                    <ext:TextBox ID="TextBox2" runat="server" Label="" Hidden="true">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow  ColumnWidths="45% 45% 10%">
                                <Items>
                                    <ext:TextBox ID="sCurrentCouponNumber" runat="server" Label="优惠券号码：">
                                    </ext:TextBox>
                                    <ext:NumberBox ID="sCouponQty" runat="server" Label="优惠券数量：" MaxValue="99999999" 
                                        NoDecimal="true" NoNegative="true">
                                    </ext:NumberBox>
                                    <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click" ValidateForms="SearchForm">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow  ColumnWidths="45% 45% 10%">
                                <Items>
                                    <ext:Button ID="SaveToGoodButton" Text="添加为部分收货" Icon="Accept" runat="server" OnClick="SaveToGoodButton_Click">
                                    </ext:Button>
                                    <ext:Button ID="SaveToDamageButton" Text="添加为损坏" Icon="Cancel" runat="server" OnClick="SaveToDamageButton_Click">
                                    </ext:Button>
                                    <ext:Label ID="Label1" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140" Hidden=true>
                        <Rows>
                        <ext:FormRow  ColumnWidths="20% 20% 60%">
                            <Items>
                                <ext:CheckBox ID="chkAll" runat="server" Label="全选" OnCheckedChanged="chkAll_Changed" AutoPostBack="true"/>
                            </Items>
                        </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Grid ID="Grid3" Title="待收优惠券列表" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="5"
                        runat="server" DataKeyNames="KeyID" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid3_PageIndexChange">
                        <Columns>
                            <ext:TemplateField HeaderText="好货" ExpandUnusedSpace="true" Width="50px">
                                <ItemTemplate>
                                    <input type="checkbox" id="chkGood" runat="server" title='<%#((System.Data.DataRow)Container.DataItem)["KeyID"]%>' checked='<%#((System.Data.DataRow)Container.DataItem)["CouponStockStatus"].ToString()=="2"%>' onclick="SetGood(this);" />
                                    <%--<ext:CheckBox ID="chkGood" runat="server"  checked='<%#((System.Data.DataRow)Container.DataItem)["CouponStockStatus"].ToString()=="2"%>'/>--%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="坏货" ExpandUnusedSpace="true" Width="50px">
                                <ItemTemplate>
                                    <input type="checkbox" id="chkDamaged" runat="server" title='<%#((System.Data.DataRow)Container.DataItem)["KeyID"]%>' checked='<%#((System.Data.DataRow)Container.DataItem)["CouponStockStatus"].ToString()=="3"%>' onclick="SetDamaged(this);" />
                                    <%--<ext:CheckBox ID="chkDamage" runat="server"  checked='<%#((System.Data.DataRow)Container.DataItem)["CouponStockStatus"].ToString()=="3"%>'/>--%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券号码">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["FirstCouponNumber"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券物理编号">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponUID"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券类型">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponTypeName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponStockStatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存所在地点">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StoreName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="描述">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["Description"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="创建日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["ReceiveDateTime"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="过期日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponExpiryDate"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar4" runat="server" Hidden=true >
                                <Items>
                                    <ext:Button ID="btnAdd1" Text="添加为部分收货" Icon="Add" runat="server" EnablePress="false">
                                    </ext:Button>
                                    <ext:CheckBox ID="chkAllGood" runat="server" Label="选择好货" OnCheckedChanged="chkAllGood_Changed" AutoPostBack="true"/>
                                    <ext:Button ID="btnAdd2" Text="添加为损坏" Icon="Add" runat="server" EnablePress="false">
                                    </ext:Button>
                                    <ext:CheckBox ID="chkAllDamage" runat="server" Label="选择坏货" OnCheckedChanged="chkAllDamage_Changed" AutoPostBack="true"/>
                                    <ext:Button ID="btnAddGood" Text="选择好货" Icon="Add" runat="server" OnClick="btnAddGood_OnClick" Hidden="true">
                                    </ext:Button>
                                    <ext:Button ID="btnAddDamaged" Text="选择损坏" Icon="Add" runat="server" OnClick="btnAddDamaged_OnClick" Hidden="true">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                    </ext:Grid>
<%--                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="5"
                        runat="server" EnableCheckBoxSelect="true" DataKeyNames="KeyID" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="优惠券号码">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["FirstCouponNumber"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券物理编号">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponUID"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponStockStatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存所在地点">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StoreName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="描述">
                                <ItemTemplate>
                                    <ext:TextBox ID="Description" runat="server" Label="" Text='<%#((System.Data.DataRow)Container.DataItem)["Description"]%>'>
                                    </ext:TextBox>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="创建日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["ReceiveDateTime"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="过期日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponExpiryDate"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnDelete1" Text="添加到待收优惠券表" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                    </ext:Grid>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="5"
                        runat="server" EnableCheckBoxSelect="true" DataKeyNames="KeyID" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid2_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="优惠券号码">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["FirstCouponNumber"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券物理编号">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponUID"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="优惠券状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存状态">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponStockStatusName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="库存所在地点">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["StoreName"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="描述">
                                <ItemTemplate>
                                    <ext:TextBox ID="Description2" runat="server" Label="" Text='<%#((System.Data.DataRow)Container.DataItem)["Description"]%>'>
                                    </ext:TextBox>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="创建日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["ReceiveDateTime"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="过期日期">
                                <ItemTemplate>
                                    <%#((System.Data.DataRow)Container.DataItem)["CouponExpiryDate"]%>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnDelete2" Text="添加到待收优惠券表" Icon="Delete" runat="server" OnClick="btnDelete2_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                    </ext:Grid>--%>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>




