﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.Ord_ImportMember_H, Edge.SVA.Model.Ord_ImportMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberCotroller controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //初始化默认值
                InitData();
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            if (!ValidData()) return;

            controller.ViewModel.MainTable = this.GetAddObject();

            if (controller.ViewModel.MainTable != null)
            {                
                //校验文件类型是否正确
                if (!ValidateFile(this.Description.FileName))
                {
                    return;
                }

                //保存附件到数据库
                controller.ViewModel.MainTable.Description = Description.SaveAttachFileToServer("ImportMemberInfos");

            }

            ExecResult er = controller.Submit();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Success Code:" + controller.ViewModel.MainTable.ImportMemberNumber);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber.ToString());
                ShowAddFailed();
            }          
        }

        private void InitData()
        {
            this.ImportMemberNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.MemberImportInfo);
            this.CreatedBusDate.Text = DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
            this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;
        }

        private bool ValidData()
        {
            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            return true;
        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}