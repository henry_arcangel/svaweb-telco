﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.PointAdjustment.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:HiddenField ID="ApproveStatus" runat="server" Text="P" />
                    <ext:Label ID="ImportMemberNumber" runat="server" Label="交易编号：">
                    </ext:Label>
   
                    <ext:Label ID="lblApproveStatus" runat="server" Label="交易状态：">
                    </ext:Label>

                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                    </ext:Label>
    
                    <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                    </ext:Label>

                    <ext:Label ID="CreatedByName" runat="server" Label="创建人：">
                    </ext:Label>
  
                    <ext:TextBox ID="Note" runat="server" Label="备注：" MaxLength="512">
                    </ext:TextBox>

                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="85% 15%">
                                <Items>
                                    <ext:FileUpload ID="Description" Label="导入文件：" runat="server"></ext:FileUpload>
                                    <ext:Label ID="label" runat="server" HideMode="Display" Hidden="true"></ext:Label>
                                </Items>   
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:Panel>
    <ext:HiddenField ID="ExcuteReslut" runat="server"></ext:HiddenField>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        Target="Top" IsModal="True" Width="500px" Height="350px"> 
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
