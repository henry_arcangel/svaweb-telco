﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using System.IO;

namespace Edge.Web.Operation.MemberManagement.PointAdjustment
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_ImportMember_H, SVA.Model.Ord_ImportMember_H>
    {
        ImportMemberCotroller controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.ImportMemberNumber);
                this.CreatedBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);

                if (controller.ViewModel.MainTable.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());
                }
                else if (controller.ViewModel.MainTable.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.Description;

                    this.btnExport.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.Description) ? true : false;//没有文件时不显示查看按钮(Len)

                    if (!string.IsNullOrEmpty(Request.Params["filename"]))
                    {
                        this.Description.Text = Request.Params["filename"];
                    }
                }
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                //以XML的形式呈现
                if (controller.ViewModel.MainTable.ApproveStatus == "A")
                {
                    string xmlfilename = "CustomerImport-" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + "-01.xml";
                    Tools.ExportTool.ExportFile(fileName.Replace(".csv", ".xml"), xmlfilename);
                }
                else
                {
                    Tools.ExportTool.ExportFile(fileName);
                }
                Tools.Logger.Instance.WriteOperationLog("ImportMemberInfos download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("ImportMemberInfos download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }
        }
    }
}