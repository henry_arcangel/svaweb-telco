﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.PointAdjustment.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:Form ID="Form13" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server">
                                <Items>
                                    <ext:Label ID="ImportMemberNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow2" runat="server">
                                <Items>
                                    <ext:Label ID="ApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow3" runat="server">
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form4" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow4" runat="server">
                            <Items>
                                <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                </ext:Label>
                                <ext:Label ID="ApproveBy" runat="server" Label="批核人：">
                                </ext:Label>
                            </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow5" runat="server">
                            <Items>
                                <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                </ext:Label>
                                <ext:Label ID="Note" runat="server" Label="备注：">
                                </ext:Label>
                            </Items>                   
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form6" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow6" runat="server" ColumnWidths="0% 80% 20%">
                                <Items>
                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                    </ext:Label>
                                    <ext:Label ID="Description" Text="" runat="server" Label="导入文件：">
                                    </ext:Label>
                                    <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
