﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ResetMemberLoginPassword.List" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1"/>
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelAlign="Right" LabelWidth="100px">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server">
                        <Items>
                            <ext:DropDownList ID="CardType" runat="server" Label="卡类型：" Resizable="true" AutoPostBack="true" OnSelectedIndexChanged="CardType_SelectedIndexChanged"></ext:DropDownList>
                            <ext:DropDownList ID="CardGrade" runat="server" Label="卡级别：" Resizable="true"></ext:DropDownList>
                            <ext:TextBox ID="CardNumber" runat="server" Label="卡号：" MaxLength="512">
                            </ext:TextBox>
                            <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server">
                        <Items>
                            <ext:DropDownList ID="CountryCode" runat="server" Label="国家码：" Resizable="true"></ext:DropDownList>
                            <ext:TextBox ID="txtmobile" runat="server" Label="手机号码：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MemberMobilePhone" runat="server" Label="注册号码：" MaxLength="512">
                            </ext:TextBox>
                            <ext:Label runat="server" Hidden="true" HideMode="Offsets"></ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                    runat="server" EnableCheckBoxSelect="True" DataKeyNames="MemberID,CountryCode,MemberMobilePhone,MemberRegisterMobile"
                    AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                    ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange" OnSort="Grid1_Sort" AllowSorting="true">
                    <Toolbars>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnReset" Text="重设密码" runat="server" OnClick="btnReset_Click" Icon="FilmEdit">
                            </ext:Button>
                        </Items>
                        </ext:Toolbar>
                    </Toolbars>
                    <Columns>
                        <ext:TemplateField Width="60px" HeaderText="英文名称" SortField="MemberEngGivenName">
                            <ItemTemplate>
                                <asp:Label ID="lblEngName1" runat="server" Text='<%# Eval("MemberEngGivenName") %>'></asp:Label>.<asp:Label
                                    ID="lblEngName2" runat="server" Text='<%#Eval("MemberEngFamilyName")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="国家码">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("CountryCode")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="手机号码">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("MemberMobilePhone")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" HeaderText="注册号码">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("MemberRegisterMobile")%>'></asp:Label>
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:TemplateField Width="60px" Hidden="true">
                            <ItemTemplate>
                                <asp:HiddenField ID="hidmemberid" runat="server" Value='<%#Eval("MemberID")%>' />
                            </ItemTemplate>
                        </ext:TemplateField>
                        <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                            Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="MemberID"
                            DataIFrameUrlFormatString="../../Basic/Show.aspx?id={0}" Title="查看" />
                    </Columns>
                </ext:Grid>
                </Items>
            </ext:Panel>
            </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="850px"
        Height="490px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>   
    <ext:Window ID="Window2" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField> 
    <ext:HiddenField ID="SortField" Text="" runat="server"></ext:HiddenField>
    </form>
    <uc2:checkright ID="Checkright1" runat="server" />
</body>
</html>
