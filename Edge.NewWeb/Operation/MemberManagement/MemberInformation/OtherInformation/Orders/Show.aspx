﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnPrint" Icon="PrinterGo"
                        OnClick="btnPrint_Click" runat="server" Text="打印预览">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="订单信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="from1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="TxnNo" runat="server" Label="订单编号：">
                                            </ext:Label>
                                            <ext:Label ID="SalesTypeView" runat="server" Label="订单类型：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="StatusView" runat="server" Label="订单状态：">
                                            </ext:Label>
                                            <ext:Label ID="CreatedOn" runat="server" Label="确认时间：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="PaymentDoneOn" runat="server" Label="付款时间：">
                                            </ext:Label>
                                            <ext:Label ID="DeliverStartOn" runat="server" Label="发货时间：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="会员信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="CardNumber" runat="server" Label="卡号码：">
                                            </ext:Label>
                                            <ext:Label ID="MemberRegisterMobile" runat="server" Label="注册号码：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="ChineseName" runat="server" Label="中文名称：">
                                            </ext:Label>
                                            <ext:Label ID="EnglishName" runat="server" Label="英文名称：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="MemberMobilePhone" runat="server" Label="会员电话：">
                                            </ext:Label>
                                            <ext:Label ID="MemberEmail" runat="server" Label="会员邮箱：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="MemberAddress" runat="server" Label="会员地址：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="送货信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="DeliveryAddress" runat="server" Label="送货地址：">
                                            </ext:Label>
                                            <ext:Label ID="Contact" runat="server" Label="联系人：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="ContactPhone" runat="server" Label="联系电话：">
                                            </ext:Label>
                                            <ext:Label ID="PickupTypeView" runat="server" Label="提取方式：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="卖家信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="SalesName" runat="server" Label="卖家：">
                                            </ext:Label>
                                            <ext:Label ID="SalesAddress" runat="server" Label="地址：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="SalesContactPhone" runat="server" Label="联系电话：">
                                            </ext:Label>
                                            <ext:Label ID="SalesContact" runat="server" Label="联系人：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="物流信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                            EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Form ID="Form4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                                EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                                <Rows>
                                    <ext:FormRow>
                                        <Items>
                                            <ext:Label ID="LogisticsProviderIDView" runat="server" Label="物流公司：">
                                            </ext:Label>
                                            <ext:Label ID="DeliveryNumber" runat="server" Label="运单号码：">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                    <ext:FormRow ColumnWidths="18% 12% 70%">
                                        <Items>
                                            <ext:Label ID="Label10" runat="server" Label="物流跟踪：" Text="物流跟踪：" Height="20px">
                                            </ext:Label>
                                            <ext:Button ID="Track" runat="server" Text="查看物流"
                                                OnClick="btnTrack_Click" >
                                            </ext:Button>
                                            <ext:Label ID="Label9" runat="server" Hidden="true" HideMode="Offsets">
                                            </ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:DropDownList ID="LogisticsProviderID" runat="server" Label="物流公司：" Hidden="true">
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="支付信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="TenderCode"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill1" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="Label3" runat="server" Text="汇总：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotal" runat="server" Text="0.00">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="支付方式">
                                <ItemTemplate>
                                    <asp:Label ID="glblApproveStatus" runat="server" Text='<%#Eval("TenderNewName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("TenderAmount","{0:N2}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="货品明细"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid2" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="ProdCode"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid2_PageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server" Position="Top">
                                <Items>
                                    <ext:ToolbarFill ID="ToolbarFill2" runat="server">
                                    </ext:ToolbarFill>
                                    <ext:Label ID="Label1" runat="server" Text="数量：">
                                    </ext:Label>
                                    <ext:Label ID="lblQty" runat="server" Text="0">
                                    </ext:Label>
                                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                    </ext:ToolbarSeparator>
                                    <ext:Label ID="Label2" runat="server" Text="金额：">
                                    </ext:Label>
                                    <ext:Label ID="lblTotalAmount" runat="server" Text="0.00">
                                    </ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="货品编号">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("ProdCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="100px" HeaderText="货品描述">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("ProdName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="尺寸">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ProductSizeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="100px" HeaderText="颜色">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("ColorName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="数量">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("Qty")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="金额">
                                <ItemTemplate>
                                    <asp:Label ID="Label18" runat="server" Text='<%#Eval("NetAmount","{0:N2}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:RadioButtonList ID="PickupType" runat="server" Width="200" Hidden="true">
                <ext:RadioItem Text="门店自提" Value="1" Selected="true"/>
                <ext:RadioItem Text="送货上门" Value="2"/>
            </ext:RadioButtonList>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        Target="Top" IsModal="True" Width="900px" Height="500px"> 
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
