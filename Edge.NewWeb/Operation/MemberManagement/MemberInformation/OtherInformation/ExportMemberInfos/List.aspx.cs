﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using System.Text;
using Edge.Web.Controllers.Operation.MemberManagement;
using Edge.Web.Controllers.Operation.MemberManagement.ExportMemberInfos;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ExportMemberInfos
{
    public partial class List : PageBase
    {
        ExportMemberInfoController controller = new ExportMemberInfoController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                InitData();

                RptBind("", "MemberID");
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    string englishname = this.EngLishName.Text.Trim();
                    string chinesename = this.ChineseName.Text.Trim();
                    int membersex = ConvertTool.ToInt(this.MemberSex.SelectedValue);
                    int membermarital = ConvertTool.ToInt(this.MemberMarital.SelectedValue);
                    int educationid = ConvertTool.ToInt(this.EducationID.SelectedValue);
                    int professionid = ConvertTool.ToInt(this.ProfessionID.SelectedValue);
                    string schooladdress = this.OfficeAddress.SelectedValue == "-1" ? "" : this.OfficeAddress.SelectedText;
                    int schoolid = ConvertTool.ToInt(this.CompanyDesc.SelectedValue);
                    string membedateofbirthstart = this.MemberDateOfBirthStart.Text;
                    string membedateofbirthend = this.MemberDateOfBirthEnd.Text;
                    string statecode = this.StateCode.SelectedValue == "-1" ? "" : this.StateCode.SelectedValue;
                    string membermobilephone = this.MemberMobilePhone.Text;
                    string email = this.Email.Text;
                    string facebook = this.FaceBook.Text;
                    string qq = this.QQ.Text;
                    string msn = this.MSN.Text;
                    string sina = this.Sina.Text;
                    string othercontact = this.OtherContact.Text;
                    string countrycode = this.CountryCode.SelectedValue == "-1" ? "" : this.CountryCode.SelectedValue;
                    string provincecode = this.ProvinceCode.SelectedValue == "-1" ? "" : this.ProvinceCode.SelectedValue;
                    string citycode = this.CityCode.SelectedValue == "-1" ? "" : this.CityCode.SelectedValue;
                    string districtcode = this.DistrictCode.SelectedValue == "-1" ? "" : this.DistrictCode.SelectedValue;
                    string addressfulldetail = this.AddressFullDetail.Text;
                    int cardtypeid = ConvertTool.ToInt(this.CardTypeID.SelectedValue);
                    int cardgradeid = ConvertTool.ToInt(this.CardGradeID.SelectedValue);
                    string cardnumber = this.CardNumber.Text;
                    int cardstatus = this.CardStatus.SelectedValue == "" ? -1 : ConvertTool.ToInt(this.CardStatus.SelectedValue);//绑定时未绑定-1为第一项
                    string registdatestart = this.CreatedOnStart.Text;
                    string registdateend = this.CreatedOnEnd.Text;
                    if (!string.IsNullOrEmpty(englishname))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" MemberEngGivenName+'.'+MemberEngFamilyName like '%");
                        sb.Append(englishname);
                        sb.Append("%'");
                    }

                    if (!string.IsNullOrEmpty(chinesename))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "MemberChiFamilyName+'.'+MemberChiGivenName";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(chinesename);
                        sb.Append("%'");
                    }
                    if (membersex != -1)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberSex";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(membersex);
                    }
                    if (membermarital > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberMarital";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(membermarital);
                    }
                    if (educationid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.EducationID";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(educationid);
                    }
                    if (professionid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.ProfessionID";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(professionid);
                    }
                    if (!string.IsNullOrEmpty(schooladdress))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.OfficeAddress";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(schooladdress);
                        sb.Append("%'");
                    }
                    if (schoolid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.CompanyDesc";
                        sb.Append(descLan);
                        sb.Append(" = '");
                        sb.Append(schoolid);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(membedateofbirthstart))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberDateOfBirth";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(membedateofbirthstart);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(membedateofbirthend))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberDateOfBirth";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(membedateofbirthend);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(statecode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.CountryCode";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(statecode);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(membermobilephone))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberMobilePhone";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(membermobilephone);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(email))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberEmail";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(email);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(facebook))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "FaceBook"; //"MemberMessageAccount.AccountNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(facebook);
                        sb.Append("%'");
                        //sb.Append("and MemberMessageAccount.MessageServiceTypeID=7");
                    }
                    if (!string.IsNullOrEmpty(qq))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "QQ"; //"MemberMessageAccount.AccountNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(qq);
                        sb.Append("%'");
                        //sb.Append("and MemberMessageAccount.MessageServiceTypeID=5");
                    }
                    if (!string.IsNullOrEmpty(msn))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "MSN"; //"MemberMessageAccount.AccountNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(msn);
                        sb.Append("%'");
                        //sb.Append("and MemberMessageAccount.MessageServiceTypeID=4");
                    }
                    if (!string.IsNullOrEmpty(sina))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Weibo"; //"MemberMessageAccount.AccountNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(sina);
                        sb.Append("%'");
                        //sb.Append("and MemberMessageAccount.MessageServiceTypeID=8");
                    }
                    if (!string.IsNullOrEmpty(othercontact))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "MemberMessageAccount.AccountNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(othercontact);
                        sb.Append("%'");
                        sb.Append("and MemberMessageAccount.MessageServiceTypeID=99");
                    }
                    if (!string.IsNullOrEmpty(countrycode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberID in (select MemberID from MemberAddress where AddressCountry";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(countrycode);
                        sb.Append("%')");
                    }
                    if (!string.IsNullOrEmpty(provincecode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberID in (select MemberID from MemberAddress where AddressProvince";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(provincecode);
                        sb.Append("%')");
                    }
                    if (!string.IsNullOrEmpty(citycode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberID in (select MemberID from MemberAddress where AddressCity";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(citycode);
                        sb.Append("%')");
                    }
                    if (!string.IsNullOrEmpty(districtcode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberID in (select MemberID from MemberAddress where AddressDistrict";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(districtcode);
                        sb.Append("%')");
                    }
                    if (!string.IsNullOrEmpty(addressfulldetail))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.MemberID in (select MemberID from MemberAddress where AddressFullDetail";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(addressfulldetail);
                        sb.Append("%')");
                    }
                    if (cardtypeid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Card.CardTypeID";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(cardtypeid);
                    }
                    if (cardgradeid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Card.CardGradeID";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(cardgradeid);
                    }
                    if (!string.IsNullOrEmpty(cardnumber))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Card.CardNumber";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(cardnumber);
                        sb.Append("%'");
                    }
                    if (cardstatus > -1)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Card.Status";
                        sb.Append(descLan);
                        sb.Append(" = ");
                        sb.Append(cardstatus);
                    }
                    if (!string.IsNullOrEmpty(registdatestart))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(registdatestart);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(registdateend))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Member.CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(registdateend);
                        sb.Append("' as DateTime) + 1");
                    }
                    strWhere = sb.ToString();
                }

                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                int count = 0;

                int startindex = this.Grid1.PageSize * this.Grid1.PageIndex + 1;
                int endindex = this.Grid1.PageSize * (this.Grid1.PageIndex + 1);
                DataSet ds = controller.GetTransList(strWhere, startindex, endindex, out count, this.SortField.Text);

                ViewState["Search"] = controller.GetTransList(strWhere, 0, webset.MaxSearchNum, out count, "MemberID").Tables[0];

                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("Member", "Load Faild", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;

            int startindex = this.Grid1.PageSize * this.Grid1.PageIndex + 1;
            int endindex = this.Grid1.PageSize * (this.Grid1.PageIndex + 1);

            DataSet ds = controller.GetTransList(ViewState["strWhere"].ToString(), startindex, endindex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion


        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ViewState["Search"] == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            DateTime start = DateTime.Now;
            string fileName = controller.UpLoadFileToServer(ViewState["Search"] as DataTable);
            int records = 0;
            
            try
            {
                string exportname = "ExportMemberInfo.xls";

                Tools.ExportTool.ExportFile(fileName, exportname);

                Tools.Logger.Instance.WriteExportLog("Batch Export Member", exportname, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Logger.Instance.WriteExportLog("Batch Export Member", fn, start, records, ex.Message);
                ShowWarning(ex.Message);
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "MemberID");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "MemberID");
        }

        protected void InitData()
        {
            controller.BindMemberSex(this.MemberSex);
            controller.BindMemberMarital(this.MemberMarital);
            controller.BindEducation(this.EducationID);
            controller.BindProfession(this.ProfessionID);
            controller.BindSchoolDistrict(this.OfficeAddress, SVASessionInfo.SiteLanguage);
            controller.BindStateCode(this.StateCode);
            controller.BindCountry(this.CountryCode);
            controller.BindCardType(this.CardTypeID);
            controller.BindCardStatus(this.CardStatus);
        }

        protected void CountryCode_SelectedChanged(object sender, EventArgs e)
        {
            if (this.CountryCode.SelectedValue != "-1")
            {
                controller.BindProvince(this.ProvinceCode, this.CountryCode.SelectedValue);
            }
            else
            {
                this.ProvinceCode.Items.Clear();
                this.CityCode.Items.Clear();
                this.DistrictCode.Items.Clear();
            }
        }
        protected void ProvinceCode_SelectedChanged(object sender, EventArgs e)
        {
            if (this.ProvinceCode.SelectedValue != "-1")
            {
                controller.BindCity(this.CityCode, this.ProvinceCode.SelectedValue);
            }
            else
            {
                this.CityCode.Items.Clear();
                this.DistrictCode.Items.Clear();
            }
        }
        protected void CityCode_SelectedChanged(object sender, EventArgs e)
        {
            if (this.CityCode.SelectedValue != "-1")
            {
                controller.BindDistrict(this.DistrictCode, this.CityCode.SelectedValue);
            }
            else
            {
                this.DistrictCode.Items.Clear();
            }
        }
        protected void CardTypeID_SelectedChanged(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue != "-1")
            {
                controller.BindCardGrade(this.CardGradeID, ConvertTool.ToInt(this.CardTypeID.SelectedValue));
            }
            else
            {
                this.CardGradeID.Items.Clear();
            }
        }

        protected void OfficeAddress_SelectedChanged(object sender, EventArgs e)
        {
            if (this.OfficeAddress.SelectedValue != "-1")
            {
                controller.BindSchoolName(this.CompanyDesc, this.OfficeAddress.SelectedText, SVASessionInfo.SiteLanguage);
            }
            else
            {
                this.CompanyDesc.Items.Clear();
            }
        }
    }
}