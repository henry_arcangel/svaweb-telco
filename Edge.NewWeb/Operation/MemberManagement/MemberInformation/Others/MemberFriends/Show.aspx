﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Others.MemberFriends.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：查看会员好友信息</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                好友信息
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                好友编号：
            </td>
            <td width="75%">
                <asp:Label ID="FriendMemberID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                好友名称：
            </td>
            <td>
                <asp:Label ID="FriendName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                电话号码：
            </td>
            <td>
                <asp:Label ID="MobileNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮箱地址：
            </td>
            <td>
                <asp:Label ID="EMail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系方式：
            </td>
            <td>
                <asp:Label ID="SNSTypeID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系方式号码 ：
            </td>
            <td>
                <asp:Label ID="SNSAccountNo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
