﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others.MemberOrder
{
    public partial class List : PageBase
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            if (!Page.IsPostBack)
            {
                RptBind("MemberID=" + Request.Params["id"], "TxnNo");
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        protected void lbtnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../List.aspx?id=" + Request.Params["id"]);
        }
    }
}
