﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Others.MemberOrder.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：查看会员订单</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                会员订单信息
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                订单编号：
            </td>
            <td width="75%">
                <asp:Label ID="TxnNo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                订单日期：
            </td>
            <td>
                <asp:Label ID="TxnDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                收货人名字：
            </td>
            <td>
                <asp:Label ID="Contact" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                收货人地址：
            </td>
            <td>
                <asp:Label ID="DeliveryAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货人员 ：
            </td>
            <td>
                <asp:Label ID="DeliverBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货联系方式 ：
            </td>
            <td>
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <%--<tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" ></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
