﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others.OnlineShippingAddress
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberAddress, Edge.SVA.Model.MemberAddress>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.MemberFirstAddr.SelectedValue =="1")
            {
                int isHas = new Edge.SVA.BLL.MemberAddress().GetCount("MemberFirstAddr=1 and MemberID=" + Request.Params["memberid"]);
                if (isHas > 0)
                {
                    this.lblMsg.Text = Resources.MessageTips.ExistDefaultAddress;
                    this.lblMsg.Visible = true;
                    return;
                }
            }

            Edge.SVA.Model.MemberAddress item = this.GetAddObject();

            if (item != null)
            {
                item.MemberID = Edge.Web.Tools.ConvertTool.ToInt(Request.Params["memberid"]);
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }

            if (item.MemberID <= 0)
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "#", Resources.MessageTips.FAILED_TITLE);
                return;
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.MemberAddress>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0&id=" + Request.Params["memberid"], Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0&id=" + Request.Params["memberid"], Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
