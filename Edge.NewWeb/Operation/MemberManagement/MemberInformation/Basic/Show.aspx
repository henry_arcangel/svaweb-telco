﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Basic.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        LabelAlign="Right">
                        <Rows>
                            <ext:FormRow ID="FormRow17" runat="server">
                                <Items>
                                    <ext:Label ID="MemberRegisterMobile" runat="server" Label="注册号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow runat="server">
                                <Items>
                                    <ext:Label ID="MemberAppellation" runat="server" Label="称呼：">
                                    </ext:Label>
                                    <ext:Label ID="MemberEngFamilyName" runat="server" Label="英文名（姓）：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow1" runat="server">
                                <Items>
                                    <ext:Label ID="MemberEngGivenName" runat="server" Label="英文名（名）：">
                                    </ext:Label>
                                    <ext:Label ID="MemberChiFamilyName" runat="server" Label="中文名（姓）：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow2" runat="server">
                                <Items>
                                    <ext:Label ID="MemberChiGivenName" runat="server" Label="中文名（名）：">
                                    </ext:Label>
                                    <ext:Label ID="NickName" runat="server" Label="昵称：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow3" runat="server">
                                <Items>
                                    <ext:Label ID="MemberSexView" runat="server" Label="性别：">
                                    </ext:Label>
                                    <ext:Label ID="MemberDateOfBirth" runat="server" Label="生日 ：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow4" runat="server">
                                <Items>
                                    <ext:Label ID="MemberMobilePhone" runat="server" Label="手机号码：">
                                    </ext:Label>
                                    <ext:Label ID="MemberMaritalView" runat="server" Label="婚姻情况：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow5" runat="server">
                                <Items>
                                    <ext:Label ID="MemberIdentityTypeView" runat="server" Label="证件类别：">
                                    </ext:Label>
                                    <ext:Label ID="MemberIdentityRef" runat="server" Label="证件号码：" />
                                </Items>
                            </ext:FormRow>
                             <ext:FormRow ID="FormRow18" runat="server">
                                <Items>
                                    <ext:Label ID="EducationIDView" runat="server" Label="学历：" />
                                    <ext:Label ID="MemberDefLanguageView" runat="server" Label="会员默认语言：" />
                                </Items>
                            </ext:FormRow>

                             <ext:FormRow ID="FormRow19" runat="server">
                                <Items>
                                    <ext:Label ID="ReceiveAllAdvertisingView" runat="server" Label="接收促销讯息： " />
                                    <ext:Label ID="ReceiveSMSPromotionMessageView" runat="server" Label="接收短信促销讯息：" />
                                </Items>
                            </ext:FormRow>
                             <ext:FormRow ID="FormRow20" runat="server">
                                <Items>
                                    <ext:Label ID="ReceiveEmailPromotionMessageView" runat="server" Label="接收电子邮件促销讯息： " />
                                    <ext:Label ID="AcceptPhoneAdvertisingView" runat="server" Label="接收电话呼叫促销讯息：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow6" runat="server">
                                <Items>
                                    <ext:Label ID="ProfessionIDView" runat="server" Label="专业：" />
                                    <ext:Label ID="CountryCode" runat="server" Label="国家码：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow7" runat="server">
                                <Items>
                                    <ext:Label ID="MemberPosition" runat="server" Label="职位：" />
                                    <ext:Label ID="HomeTelNum" runat="server" Label="固定电话：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow8" runat="server">
                                <Items>
                                    <ext:Label ID="MemberEmail" runat="server" Label="邮箱：" />
                                    <ext:Label ID="AddressCountry" runat="server" Label="国家编码：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow9" runat="server">
                                <Items>
                                    <ext:Label ID="AddressProvince" runat="server" Label="省（州）编码：">
                                    </ext:Label>
                                    <ext:Label ID="AddressCity" runat="server" Label="城市编码：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow10" runat="server">
                                <Items>
                                    <ext:Label ID="AddressDistrict" runat="server" Label="区县编码：">
                                    </ext:Label>
                                    <ext:Label ID="AddressDetail" runat="server" Label="详细地址：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow11" runat="server">
                                <Items>
                                    <ext:Label ID="AddressFullDetail" runat="server" Label="完整地址：" />
                                    <ext:Label ID="lblFaceBook" runat="server" Label="Translate__Special_121_StartFaceBook：Translate__Special_121_End" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow12" runat="server">
                                <Items>
                                    <ext:Label ID="lblQQ" runat="server" Label="Translate__Special_121_StartQQ：Translate__Special_121_End" />
                                    <ext:Label ID="lblMSN" runat="server" Label="Translate__Special_121_StartMSN：Translate__Special_121_End" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow13" runat="server">
                                <Items>
                                    <ext:Label ID="lblSina" runat="server" Label="新浪微博：" />
                                    <ext:Label ID="OtherContact" runat="server" Label="其他联系方式：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow14" runat="server">
                                <Items>
                                    <ext:Label ID="Hobbies" runat="server" Label="兴趣爱好：" />
                                    <ext:Label ID="SpRemark" runat="server" Label="备注：" />
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow15" runat="server">
                                <Items>
                                    <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                                    </ext:Label>
                                    <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow ID="FormRow16" runat="server">
                                <Items>
                                    <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                                    </ext:Label>
                                    <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:RadioButtonList ID="MemberSex" runat="server" Label="性别：" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="保密" Value="0" Selected="true" />
                        <ext:RadioItem Text="男性" Value="1" />
                        <ext:RadioItem Text="女性" Value="2" />
                    </ext:RadioButtonList>
                    <ext:RadioButtonList ID="MemberMarital" runat="server" Label="婚姻情况：" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="保密" Value="0" Selected="true" />
                        <ext:RadioItem Text="未婚" Value="1" />
                        <ext:RadioItem Text="已婚" Value="2" />
                    </ext:RadioButtonList>
                    <ext:DropDownList ID="MemberIdentityType" runat="server" Label="证件类别：" Enabled="false" Hidden="true">
                        <ext:ListItem Value="0" Text="----------" />
                        <ext:ListItem Value="1" Text="手机" />
                        <ext:ListItem Value="2" Text="邮箱" />
                        <ext:ListItem Value="3" Text="身份证" />
                    </ext:DropDownList>
                    <ext:DropDownList ID="EducationID" runat="server" Label="学历：" Enabled="false" Hidden="true">
                    </ext:DropDownList>
                    <ext:DropDownList ID="ProfessionID" runat="server" Label="专业：" Enabled="false" Hidden="true">
                    </ext:DropDownList>
                    <ext:Form ID="Form2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                        ShowBorder="false" runat="server">
                        <Rows>
                            <ext:FormRow ColumnWidths="0% 40% 60%">
                                <Items>
                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                    </ext:Label>
                                    <ext:Label ID="Label1" Text="" runat="server" Label="会员照片：">
                                    </ext:Label>
                                    <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                     <%--Add by Nathan 2014-07-07 ++ --%>
                    <ext:DropDownList ID="MemberDefLanguage" runat="server" Label="会员默认语言：" MaxLength="512"
                        ToolTipTitle="会员默认语言" ToolTip="不能超過512個字符" Resizable="true" Enabled="false" Hidden="true">
                    </ext:DropDownList>
                    <ext:RadioButtonList ID="ReceiveAllAdvertising" runat="server" Label="接收所有会员优惠讯息："
                        Width="100px" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                        <ext:RadioItem Text="否" Value="0" />
                    </ext:RadioButtonList>
                    <ext:RadioButtonList ID="ReceiveSMSPromotionMessage" runat="server" Label="接收短信优惠讯息："
                        Width="100px" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                        <ext:RadioItem Text="否" Value="0" />
                    </ext:RadioButtonList>
                    <ext:RadioButtonList ID="ReceiveEmailPromotionMessage" runat="server" Label="接收邮箱优惠讯息："
                        Width="100px" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                        <ext:RadioItem Text="否" Value="0" />
                    </ext:RadioButtonList>
                    <ext:RadioButtonList ID="AcceptPhoneAdvertising" runat="server" Label="接收手机优惠讯息："
                        Width="100px" Enabled="false" Hidden="true">
                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                        <ext:RadioItem Text="否" Value="0" />
                    </ext:RadioButtonList>
                    <%--Add by Nathan 2014-07-07 -- --%>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="所绑定的卡的列表"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="false" DataKeyNames="CardNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="品牌">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("CardTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("CardGradeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="现有金额">
                                <ItemTemplate>
                                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("TotalAmount","{0:0.00}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="现有积分">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("TotalPoints")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    </form>
</body>
</html>
