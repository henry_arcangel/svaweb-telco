﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.Web.Controllers.Operation.MemberManagement;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MemberModifyController controller = new MemberModifyController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
                //ds = new Edge.SVA.BLL.Nation().GetAllList();
                //Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3", "NationCode");
                RegisterCloseEvent(btnClose);

                //地址
                ControlTool.BindCountry(AddressCountry);
                InitProvinceByCountry();
                InitCityByProvince();
                InitDistrictByCity();
                //Add by Nathan 20140707++
                Tools.ControlTool.BindLanguageMap(this.MemberDefLanguage);
                //Add by Nathan 20140707--
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                List<Edge.SVA.Model.Member> models = new Edge.SVA.BLL.Member().GetModelList("");
                if (models == null || models.Count <= 0) return;
                if (Model != null)
                {
                    //存在会员相片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.MemberPictureFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.MemberPictureFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    controller.LoadViewModel(this.Model.MemberID);
                    this.txtFaceBook.Text = controller.ViewModel.FacebookNumber;
                    this.txtQQ.Text = controller.ViewModel.QQNumber;
                    this.txtMSN.Text = controller.ViewModel.MSNNumber;
                    this.txtSina.Text = controller.ViewModel.SinaNumber;

                    //地址
                    if (controller.ViewModel.SubTable != null)
                    {
                        AddressCountry.SelectedValue = controller.ViewModel.SubTable.AddressCountry == null ? "-1" : controller.ViewModel.SubTable.AddressCountry.ToString().Trim();
                        InitProvinceByCountry();
                        AddressProvince.SelectedValue = controller.ViewModel.SubTable.AddressProvince == null ? "-1" : controller.ViewModel.SubTable.AddressProvince.ToString().Trim();
                        InitCityByProvince();
                        AddressCity.SelectedValue = controller.ViewModel.SubTable.AddressCity == null ? "-1" : controller.ViewModel.SubTable.AddressCity.ToString().Trim();
                        InitDistrictByCity();
                        AddressDistrict.SelectedValue = controller.ViewModel.SubTable.AddressDistrict == null ? "-1" : controller.ViewModel.SubTable.AddressDistrict.ToString().Trim();
                        AddressDetail.Text = controller.ViewModel.SubTable.AddressDetail;
                        AppendFullAddress();

                        //Add by Nathan 20140707 ++
                        this.ReceiveEmailPromotionMessage.SelectedValue = controller.ViewModel.ReceiveEmailPromotionMessage.ToString();
                        this.ReceiveSMSPromotionMessage.SelectedValue = controller.ViewModel.ReceiveSMSPromotionMessage.ToString();
                        //Add by Nathan 20140707 --
                    }

                    //卡列表
                    DataSet ds = controller.GetCardList(controller.ViewModel.MainObject.MemberID);
                    ViewState["table"] = ds;
                    BindCardList(ds.Tables[0]);
                }

            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");

            //Edge.SVA.Model.Member item = this.GetUpdateObject();

            controller.ViewModel.MainObject = this.GetUpdateObject();

            if (controller.ViewModel.MainObject != null) //item != null)
            {
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainObject.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainObject.UpdatedOn = System.DateTime.Now;
                if (!string.IsNullOrEmpty(this.MemberPictureFile.ShortFileName) && this.FormLoad.Hidden == false)
                {                
                    //校验图片类型是否正确
                    if (!ValidateImg(this.MemberPictureFile.FileName))
                    {
                        return;
                    }
                    //item.MemberPictureFile = this.MemberPictureFile.SaveToServer("Basic");
                    controller.ViewModel.MainObject.MemberPictureFile = this.MemberPictureFile.SaveToServer("Basic");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateImg(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    //item.MemberPictureFile = this.uploadFilePath.Text;
                    controller.ViewModel.MainObject.MemberPictureFile = this.uploadFilePath.Text;
                }


                //item.HomeAddress = this.AddressFullDetail.Text;
                controller.ViewModel.MainObject.HomeAddress = this.AddressFullDetail.Text;

                //修改MemberMessageAccount数据,MemberMessageTypeID有4种（4:msn,5:qq,7:facebook,8:sina）
                //controller.ViewModel.MainObject.MemberID = item.MemberID;
                if (!string.IsNullOrEmpty(this.txtFaceBook.Text))
                {
                    controller.ViewModel.FacebookNumber = this.txtFaceBook.Text;
                    controller.ViewModel.MemberMessageTypeIDList.Add(4);
                }
                if (!string.IsNullOrEmpty(this.txtQQ.Text))
                {
                    controller.ViewModel.QQNumber = this.txtQQ.Text;
                    controller.ViewModel.MemberMessageTypeIDList.Add(5);
                }
                if (!string.IsNullOrEmpty(this.txtMSN.Text))
                {
                    controller.ViewModel.MSNNumber = this.txtMSN.Text;
                    controller.ViewModel.MemberMessageTypeIDList.Add(7);
                }
                if (!string.IsNullOrEmpty(this.txtSina.Text))
                {
                    controller.ViewModel.SinaNumber = this.txtSina.Text;
                    controller.ViewModel.MemberMessageTypeIDList.Add(8);
                }
                if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text))
                {
                    controller.ViewModel.MemberMessageTypeIDList.Add(2);
                }
                // add by Alex 20141016 ++
                if (!string.IsNullOrEmpty(this.MemberRegisterMobile.Text))
                {
                    controller.ViewModel.MemberMessageTypeIDList.Add(1);
                }
                // add by Alex 20141016 --

                //Add by Nathan 20140707 ++
                controller.ViewModel.ReceiveEmailPromotionMessage = Convert.ToInt32(this.ReceiveEmailPromotionMessage.SelectedValue);
                controller.ViewModel.ReceiveSMSPromotionMessage = Convert.ToInt32(this.ReceiveSMSPromotionMessage.SelectedValue);
                controller.ViewModel.MemberMessageTypeIDList.Add(1);
                controller.ViewModel.MemberMessageTypeIDList.Add(2);
                //Add by Nathan 20140707 ---

                //地址保存字段
                controller.ViewModel.SubTable.AddressCountry = this.AddressCountry.SelectedValue == "-1" ? "" : this.AddressCountry.SelectedValue;
                controller.ViewModel.SubTable.AddressProvince = this.AddressProvince.SelectedValue == "-1" ? "" : this.AddressProvince.SelectedValue;
                controller.ViewModel.SubTable.AddressCity = this.AddressCity.SelectedValue == "-1" ? "" : this.AddressCity.SelectedValue;
                controller.ViewModel.SubTable.AddressDistrict = this.AddressDistrict.SelectedValue == "-1" ? "" : this.AddressDistrict.SelectedValue;
                controller.ViewModel.SubTable.AddressDetail = this.AddressDetail.Text;
                controller.ViewModel.SubTable.AddressFullDetail = this.AddressFullDetail.Text;

                //controller.ViewModel.MainObject.MemberRegisterMobile = this.MemberMobilePhone.Text;

                //controller.Submit();
                ExecResult er = controller.Save();
                if (er.Success)//(Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Member>(item))
                {
                    if (this.FormReLoad.Hidden == true)
                    {
                        DeleteFile(this.uploadFilePath.Text);
                    }
                    CloseAndPostBack();
                }
                else
                {
                    ShowUpdateFailed();
                }
            }

        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.MemberPictureFile.ShortFileName))
            {
                this.PicturePath.Text = this.MemberPictureFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);
        }

        protected void AddressCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressCountry.SelectedValue != "-1")
            {
                ControlTool.BindProvince(this.AddressProvince, this.AddressCountry.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindProvince(AddressProvince, "-1");
            }
            AddressProvince_SelectedIndexChanged(null, null);
            AddressCity_SelectedIndexChanged(null, null);
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressProvince.SelectedValue != "-1")
            {
                ControlTool.BindCity(this.AddressCity, this.AddressProvince.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindCity(AddressCity, "-1");
            }
            AddressCity_SelectedIndexChanged(null, null);
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressCity.SelectedValue != "-1")
            {
                ControlTool.BindDistrict(this.AddressDistrict, this.AddressCity.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindDistrict(AddressDistrict, "-1");
            }
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AddressFullDetail_OnTextChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AppendFullAddress()
        {
            string country = this.AddressCountry.SelectedValue == "-1" ? "" : this.AddressCountry.SelectedText;
            string province = this.AddressProvince.SelectedValue == "-1" ? "" : this.AddressProvince.SelectedText;
            string city = this.AddressCity.SelectedValue == "-1" ? "" : this.AddressCity.SelectedText;
            string district = this.AddressDistrict.SelectedValue == "-1" ? "" : this.AddressDistrict.SelectedText;
            string detail = this.AddressDetail.Text;
            AddressFullDetail.Text = country + province + city + district + detail;
        }

        private void InitProvinceByCountry()
        {
            if (this.AddressCountry.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindProvince(AddressProvince, this.AddressCountry.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindProvince(AddressProvince, " -1");
        }

        private void InitCityByProvince()
        {
            if (this.AddressProvince.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindCity(AddressCity, this.AddressProvince.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindCity(AddressCity, " -1");
        }

        private void InitDistrictByCity()
        {
            if (this.AddressCity.SelectedValue != "-1")
                Edge.Web.Tools.ControlTool.BindDistrict(AddressDistrict, this.AddressCity.SelectedValue);
            else
                Edge.Web.Tools.ControlTool.BindDistrict(AddressDistrict, " -1");
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        public void BindCardList(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            DataSet ds = (DataSet)ViewState["table"];
            BindCardList(ds.Tables[0]);
        }
    }
}
