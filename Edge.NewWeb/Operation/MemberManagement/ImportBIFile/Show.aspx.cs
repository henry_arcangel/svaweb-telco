﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_ImportCouponDispense_H, SVA.Model.Ord_ImportCouponDispense_H>
    {
        private const string fields = "CouponDispenseNumber,CampaignCode,CouponTypeCode,MemberRegisterMobile,ExportDatetime,CardNumber";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind(string.Format("CouponDispenseNumber='{0}'", Request.Params["id"]), "CouponDispenseNumber");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus == "A")
                {
                    this.btnExport.Visible = true;
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }
                else if (Model.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                    this.btnExport.Visible = false;
                }
                else
                {
                    this.btnExport.Visible = false;
                }
            }

        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(string.Format("CouponDispenseNumber='{0}'", Request.Params["id"]), "CouponDispenseNumber");
            //RptBind("CouponDispenseNumber<>''", "CouponDispenseNumber");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            DateTime start = DateTime.Now;
            string fileName = "";
            int records = 0;

            Edge.SVA.BLL.Ord_ImportCouponDispense_H bll = new SVA.BLL.Ord_ImportCouponDispense_H();

            try
            {
                Edge.SVA.Model.Ord_ImportCouponDispense_H model = bll.GetModel(this.CouponDispenseNumber.Text.Trim());
                fileName = bll.ExportCSV(model);
                if (string.IsNullOrEmpty(fileName))
                {
                    JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteExportLog("BI Import Coupons", fn, start, records, null);

                //记录下导出时间(Len)
                //Session["exporttime"] = DateTime.Now;
                //Edge.SVA.BLL.Ord_ImportCouponDispense_D subbll = new Edge.SVA.BLL.Ord_ImportCouponDispense_D();
                ////subbll.GetModelList("CouponDispenseNumber=" + Request.Params["id"]);
                //foreach (Edge.SVA.Model.Ord_ImportCouponDispense_D item in subbll.GetModelList("CouponDispenseNumber=" + Request.Params["id"]))
                //{
                //    item.ExportDatetime = DateTime.Now;
                //    subbll.Update(item);
                //}

            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteExportLog("BI Import Coupons", fn, start, records, ex.Message);
                JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
            }

        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.Ord_ImportCouponDispense_D bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_D();

            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);

            DataSet ds = new DataSet();
            ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

            Tools.DataTool.AddCouponTypeNameByCode(ds, "CouponTypeIDName", "CouponTypeCode");
            Tools.DataTool.AddColumn(ds, "CreatedDate", Request.Params["date"]);

            if (SVASessionInfo.ExportTime != "")
            {
                Edge.SVA.Model.Ord_ImportCouponDispense_D model = new SVA.Model.Ord_ImportCouponDispense_D();
                model = bll.GetModel(0);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //ds.Tables[0].Rows[i]["ExportDatetime"] = Session["exporttime"];
                    model.ExportDatetime = ConvertTool.ToDateTime(SVASessionInfo.ExportTime);
                    
                }
                bll.Update(model);
            }

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }
    }
}