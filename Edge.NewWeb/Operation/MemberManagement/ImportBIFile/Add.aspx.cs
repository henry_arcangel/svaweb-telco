﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Messages.Manager;
using Edge.Web.Controllers;
using System.IO;
using System.Text;

namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.Ord_ImportCouponDispense_H, Edge.SVA.Model.Ord_ImportCouponDispense_H>
    {
        private ImportModelList list = new ImportModelList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                this.CouponDispenseNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.OrderImportBICoupons);
                this.CreatedBusDate.Text = DALTool.GetBusinessDate();
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
                this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;
            Edge.SVA.Model.Ord_ImportCouponDispense_H item = this.GetAddObject();
            Dictionary<string, SVA.Model.CouponType> cache = new Dictionary<string, SVA.Model.CouponType>();

            if (string.IsNullOrEmpty(this.ImportFile.FileName) || item == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //校验文件类型是否正确
            if (!ValidateFile(this.ImportFile.FileName))
            {
                return;
            }
            string pathFile = this.ImportFile.SaveToServer("ImportBIFile");
            string path = Server.MapPath("~" + pathFile);
            System.Data.DataTable dt = ExcelTool.GetFirstSheet(path);

            this.ExcuteReslut.Text = true.ToString();

            if (!ValidData(dt, cache))
            {
                this.ExcuteReslut.Text = this.list.Success.ToString();

                Tools.Logger.Instance.WriteImportLog("BI Import Coupon", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                System.Text.StringBuilder html = GetHtml(begin);

                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));

                return;                
            }

            item.ApproveOn = null;
            item.CreatedBy = DALTool.GetCurrentUser().UserID;
            item.CreatedOn = DateTime.Now;
            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;

            //保存文件路径到数据库
            item.Description = this.Description.Text;

            if (Tools.DALTool.Add<Edge.SVA.BLL.Ord_ImportCouponDispense_H>(item) > 0)
            {
                DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                database.SetExecuteTimeout(6000);
                System.Data.DataTable sourceTable = database.GetTableSchema("Ord_ImportCouponDispense_D");
                DatabaseUtil.Interface.IExecStatus es = null;

                try
                {
                    for (int i = 0; i < this.list.importDetails.Count; i++)
                    {
                        ImportModel model = this.list.importDetails[i];

                        #region Insert To Database
                        if (model.CardNumbers.Count == 1)
                        {
                            System.Data.DataRow row = sourceTable.NewRow();
                            row["CouponDispenseNumber"] = item.CouponDispenseNumber;
                            row["CampaignCode"] = model.CampaignCode;
                            row["CouponTypeCode"] = model.CouponTypeCode;
                            row["MemberRegisterMobile"] = model.MemberMobileNumber;
                            if (model.ExportDatetime.HasValue)
                            {
                                row["ExportDatetime"] = model.ExportDatetime.Value;
                            }
                            row["CardNumber"] = model.CardNumbers[0];
                            sourceTable.Rows.Add(row);
                        }
                        else if (model.CardNumbers.Count > 1)
                        {
                            foreach (string cardNumber in model.CardNumbers)
                            {
                                System.Data.DataRow row = sourceTable.NewRow();
                                row["CouponDispenseNumber"] = item.CouponDispenseNumber;
                                row["CampaignCode"] = model.CampaignCode;
                                row["CouponTypeCode"] = model.CouponTypeCode;
                                row["MemberRegisterMobile"] = model.MemberMobileNumber;
                                if (model.ExportDatetime.HasValue)
                                {
                                    row["ExportDatetime"] = model.ExportDatetime.Value;
                                }

                                row["CardNumber"] = cardNumber;
                                sourceTable.Rows.Add(row);
                            }
                        }
                        else
                        {
                            continue;
                        }
                        if (sourceTable.Rows.Count >= 100000)
                        {
                            es = database.InsertBigData(sourceTable, "Ord_ImportCouponDispense_D");
                            if (es.Success)
                            {
                                sourceTable.Rows.Clear();
                            }
                            else
                            {
                                throw es.Ex;
                            }
                        }
                        #endregion
                    }
                    #region 最后清除缓存

                    if (sourceTable.Rows.Count > 0)
                    {
                        es = database.InsertBigData(sourceTable, "Ord_ImportCouponDispense_D");
                        if (es.Success)
                        {
                            sourceTable.Rows.Clear();
                        }
                        else
                        {
                            throw es.Ex;
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    this.list.Error.Add(ex.Message);
                }
                finally
                {
                    sourceTable.Clear();
                    sourceTable.Dispose();

                    Tools.Logger.Instance.WriteImportLog("BI Import Coupon", this.ImportFile.FileName, begin, this.list.Count, this.list.Error);

                    System.Text.StringBuilder html = GetHtml(begin);

                    FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
                    //CloseAndPostBack();
                }
            }
            else
            {
                ShowAddFailed();
            }

        }

        private bool ValidData(System.Data.DataTable dt, Dictionary<string, SVA.Model.CouponType> cache)
        {
            DataTool.ClearEndRow(dt);
            if (dt == null || dt.Rows.Count <= 0)
            {
                this.list.Error.Add(Resources.MessageTips.NoData);
                return false;
            }

            SVA.BLL.Ord_ImportCouponDispense_H bll = new SVA.BLL.Ord_ImportCouponDispense_H();

            #region check columns
            List<string> columnList = new List<string>();
            columnList.Add("Campaign Code");
            columnList.Add("Coupon Type Code");
            columnList.Add("Member Mobile Number");
            columnList.Add("Export Datetime");
            StringBuilder sb = new StringBuilder(Resources.MessageTips.Lackofcolumn);
            bool existColumn = true;
            foreach (var item in columnList)
            {
                if (!dt.Columns.Contains(item))
                {
                    
                    sb.Append(item);
                    sb.Append(",");
                    existColumn = false;
                }
            }
            if (!existColumn)
            {
                this.list.Error.Add(sb.ToString().TrimEnd(','));
                return false;
            }
            #endregion

            #region 检查数据合法性 and DataTable  -> List
            DateTime exportTime;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                System.Data.DataRow row = dt.Rows[i];

                ImportModel model = new ImportModel();
                model.CampaignCode = row["Campaign Code"].ToString();
                model.CouponTypeCode = row["Coupon Type Code"].ToString();
                model.MemberMobileNumber = row["Member Mobile Number"].ToString();

                if (!string.IsNullOrEmpty(row["Export Datetime"].ToString()))
                {
                    if (DateTime.TryParse(row["Export Datetime"].ToString(), out exportTime))
                    {
                        model.ExportDatetime = exportTime;
                    }
                    else
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90369")));
                    }
                }
                if (string.IsNullOrEmpty(model.CouponTypeCode))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90423")));
                }

                if (string.IsNullOrEmpty(model.MemberMobileNumber))
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90424")));
                }
                else
                {
                    //Removed By Robin 2014-12-31 for 711 MemberMobileNumber stored email address
                    //if (!Controllers.RegexController.GetInstance().IsMobileNumber(model.MemberMobileNumber))
                    //{
                    //    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.MobileFormatError));
                    //}
                }

                list.importDetails.Add(model);

                if (list.Error.Count > 10) break;
            }


            #endregion

            if (this.list.Error.Count > 0) return false;
                
            #region 检查CouponTypeCode是否存在，Mobile Number是否存在   Mobile Number是否绑定卡

            Edge.SVA.BLL.Ord_ImportCouponDispense_H importDispense = new SVA.BLL.Ord_ImportCouponDispense_H();
            Edge.SVA.BLL.Member member = new SVA.BLL.Member();
            Edge.SVA.BLL.CardType cardType = new SVA.BLL.CardType();
            Edge.SVA.BLL.Campaign campaign = new SVA.BLL.Campaign();
            Dictionary<string, bool> campaignCache = new Dictionary<string, bool>();

            for (int i = 0; i < this.list.importDetails.Count; i++)
            {
                #region 检查数据合法性
                ImportModel item = this.list.importDetails[i];
                //检查是否存在CouponType
                Edge.SVA.Model.CouponType couponType = CouponController.GetImportCouponType(item.CouponTypeCode, cache);
                if (couponType == null)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90419")));
                    return false;
                }
                //检查是否存在会员手机号码
                List<int> members = null;
                if (!this.list.mobileMap.ContainsKey(item.MemberMobileNumber))
                {
                    members = member.GetMembers(item.MemberMobileNumber);
                    if (members.Count <= 0)
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90420")));
                        return false;
                    }
                    this.list.mobileMap.Add(item.MemberMobileNumber, members);
                }
                else
                {
                    members = this.list.mobileMap[item.MemberMobileNumber];
                }
                //检查是否存在此CouponType Brand下面是否存在CardType
                List<int> cardTypes = null;
                if (!this.list.bandMap.ContainsKey(couponType.BrandID))
                {
                    cardTypes = cardType.GetCardTypes(couponType.BrandID);
                    if (cardTypes.Count <= 0)
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90421")));
                        return false;
                    }
                    this.list.bandMap.Add(couponType.BrandID, cardTypes);
                }
                else
                {
                    cardTypes = this.list.bandMap[couponType.BrandID];
                }
                //检查是否绑定卡
                List<string> cardNumbers = importDispense.GetCardNumbers(cardTypes, members);
                if (cardNumbers.Count <= 0)
                {
                    this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, MessagesTool.instance.GetMessage("90421")));
                    return false;
                }

                if (!string.IsNullOrEmpty(item.CampaignCode) && !campaignCache.ContainsKey(item.CampaignCode))
                {
                    if (!DALTool.isHasCampaignCode(item.CampaignCode, 0))
                    {
                        this.list.Error.Add(string.Format("Line {0}:{1}\r\n", i + 1, Resources.MessageTips.CampaignCodeNotExist));
                        return false;
                    }
                    campaignCache.Add(item.CampaignCode, true);
                }
                item.CardNumbers = cardNumbers;

                #endregion
            }

            #endregion
               
            return true;

        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");
            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            html.AppendFormat("<tr><td align='right'></td><td>Import {0} records {1}.</td></tr>", this.list.Count, this.list.Success ? "successfully" : "failed");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                    //html.AppendFormat("<td>{0}</td>", error);
                }
                //html.AppendFormat("<td></td></tr>");
                html.AppendFormat("</table></td></tr>");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import File Name:", this.ImportFile.FileName);
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "BI Import Coupons");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        protected void ImportFile_FileSelected(object sender, EventArgs e)
        {
            if (ImportFile.HasFile)
            {
                this.Description.Text = ImportFile.ShortFileName;
            }

        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.ImporBIFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.ImporBIFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }

    public class ImportModel
    {
        public string CouponTypeCode { get; set; }
        public string MemberMobileNumber { get; set; }
        public string CampaignCode { get; set; }
        public DateTime? ExportDatetime { get; set; }
        public List<string> CardNumbers { get; set; }
    }

    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<string> Error { get { return error; } }

        public List<ImportModel> importDetails = new List<ImportModel>();
        public Dictionary<string, List<int>> mobileMap = new Dictionary<string, List<int>>();
        public Dictionary<int, List<int>> bandMap = new Dictionary<int, List<int>>();
        private int count = -1;
        public int Count
        {
            get
            {
                if (count >= 0) return count;

                int total = 0;
                foreach (ImportModel item in this.importDetails)
                {
                    total += item.CardNumbers == null ? 1 : item.CardNumbers.Count;
                }
                count = total;
                return total;
            }
        }

        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }

    }
}