﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers;
using Edge.SVA.DomainUtils;
using System.Data;
using System.Data.SqlClient;
using AtiveMQ.NetUtil;
using Edge.SVA.MessageCenter.Domain;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class Approve : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        //JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                        return;
                    }
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));


                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    string messageServerEnabled = System.Configuration.ConfigurationManager.AppSettings["MessageServerEnabled"].ToString();
                    if (messageServerEnabled.ToLower().Trim() == "true")
                    {
                        #region For Notify
                        try
                        {
                            string server = System.Configuration.ConfigurationManager.AppSettings["MessageServer"].ToString();

                            IAtiveMQProductor productor = Factory.CreateIAtiveMQProductor(server, string.Empty, string.Empty, "mc.incoming");

                            foreach (string id in idList)
                            {
                                IDataParameter[] parameters = 
                        { 

                 new SqlParameter("@CouponDispenseNumber", SqlDbType.VarChar,64) , 
                 new SqlParameter("@NeedActive", SqlDbType.Int) ,
                 new SqlParameter("@NeedNewBatch", SqlDbType.Int) ,
                 new SqlParameter("@CreatedBy", SqlDbType.Int) ,

                        };
                                parameters[0].Value = id;
                                parameters[1].Value = 1;
                                parameters[2].Value = 1;
                                parameters[3].Value = 1;

                                DataSet ds = DBUtility.DbHelperSQL.RunProcedure("ImportCouponDispense", parameters, "ds");

                                List<MessageInfoBase> list = new List<MessageInfoBase>();

                                ConvertToPDFUtil util = new ConvertToPDFUtil();
                                foreach (DataRow item in ds.Tables[0].Rows)
                                {
                                    Dictionary<string, string> dic = new Dictionary<string, string>();
                                    dic.Add("[MemberID]", GetValue(item, "MemberID"));
                                    dic.Add("[CouponNumber]", GetValue(item, "CouponNumber"));
                                    dic.Add("[CouponTypeCode]", GetValue(item, "CouponTypeCode"));
                                    dic.Add("[CouponTypeName1]", GetValue(item, "CouponTypeName1"));
                                    dic.Add("[CouponTypeName2]", GetValue(item, "CouponTypeName2"));
                                    dic.Add("[CouponTypeName3]", GetValue(item, "CouponTypeName3"));
                                    dic.Add("[HomeAddress]", GetValue(item, "HomeAddress"));
                                    dic.Add("[MemberEngFamilyName]", GetValue(item, "MemberEngFamilyName"));
                                    dic.Add("[MemberEngGivenName]", GetValue(item, "MemberEngGivenName"));
                                    dic.Add("[MemberChiFamilyName]", GetValue(item, "MemberChiFamilyName"));
                                    dic.Add("[MemberChiGivenName]", GetValue(item, "MemberChiGivenName"));

                                    StringBuilder sb = new StringBuilder();
                                    foreach (string item11 in dic.Keys)
                                    {
                                        sb.Append(dic[item11]);
                                        sb.Append(" ");
                                    }
                                    logger.WriteOperationLog(" Notifiy Infomation ", sb.ToString() + GetValue(item, "EmailAccount"));

                                    string pdfPathFile = Server.MapPath("~/Template/" + dic["[CouponNumber]"] + ".pdf");
                                    util.Convert(Server.MapPath("~/Template/Template.docx"), dic, dic["[CouponNumber]"], pdfPathFile);

                                    MessageInfoEMAIL email = new MessageInfoEMAIL();
                                    email.MessageId = int.Parse(GetValue(item, "MemberID"));
                                    email.MessageType = MessageType.Email.ToString();
                                    email.Subject = " Coupon ";
                                    email.MessageContent = "";
                                    email.Receivers = GetValue(item, "EmailAccount");
                                    using (FileStream fs = new FileStream(pdfPathFile, FileMode.Open))
                                    {
                                        long len = fs.Length;
                                        using (BinaryReader br = new BinaryReader(fs))
                                        {
                                            byte[] bts = br.ReadBytes((int)len);
                                            Attachment att = new Attachment();
                                            att.Name = dic["[CouponNumber]"] + ".pdf";
                                            att.BinaryStream = byteToHexStr(bts);
                                            att.Signature = string.Empty;
                                            att.SignatureType = string.Empty;
                                            email.Attachments.Add(att);
                                            list.Add(email);

                                            br.Close();
                                            fs.Close();
                                            fs.Dispose();
                                        }
                                    }

                                }

                                logger.WriteOperationLog(" Notifiy Infomation ", " Need to Send Count " + list.Count.ToString() + " messages this time");
                                foreach (var item in list)
                                {
                                    string jsonStr = JsonConvert.SerializeObject(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                                    //logger.WriteOperationLog(" Notifiy Infomation", " jsonStr  " + jsonStr);
                                    productor.SendMessage(jsonStr);
                                }
                            }

                        }
                        catch (System.Exception ex)
                        {
                            logger.WriteErrorLog("CreatePDF ", " Error ", ex);
                        }
                        #endregion
                    }
                    else
                    {
                        #region For not Notify
                        try
                        {
                            foreach (string id in idList)
                            {
                                IDataParameter[] parameters = 
                        { 

                 new SqlParameter("@CouponDispenseNumber", SqlDbType.VarChar,64) , 
                 new SqlParameter("@NeedActive", SqlDbType.Int) ,
                 new SqlParameter("@NeedNewBatch", SqlDbType.Int) ,
                 new SqlParameter("@CreatedBy", SqlDbType.Int) ,

                        };
                                parameters[0].Value = id;
                                parameters[1].Value = 1;
                                parameters[2].Value = 1;
                                parameters[3].Value = 1;

                                DataSet ds = DBUtility.DbHelperSQL.RunProcedure("ImportCouponDispense", parameters, "ds");
                            }

                        }
                        catch (System.Exception ex)
                        {
                            logger.WriteErrorLog("Exec pro  ", " Error ", ex);
                        }
                        #endregion
                    }

                    foreach (string id in idList)
                    {
                        Edge.SVA.Model.Ord_ImportCouponDispense_H mode = new Edge.SVA.BLL.Ord_ImportCouponDispense_H().GetModel(id);
                        bool isSuccess = false;

                        System.Data.DataRow dr = dt.NewRow();
                        dr["TxnNo"] = id;
                        string msg = CouponController.ApproveCouponForApproveCode(mode, out isSuccess);
                        if (isSuccess)
                        {
                            dr["ApproveCode"] = msg;
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                        }
                        else
                        {
                            dr["ApproveCode"] = msg;
                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }


                        dt.Rows.Add(dr);
                    }
                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        protected void btnClose11_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
        public static string byteToHexStr(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    sb.Append(bytes[i].ToString("X2"));
                }
            }
            return sb.ToString();
        }

        private static string GetValue(DataRow item, string feildName)
        {
            return item[feildName] == null ? string.Empty : item[feildName].ToString();
        }
    }
}