﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1;
using FineUI;
using Edge.SVA.Model.Domain.WebService.Request;
using Edge.SVA.Model.Domain.WebService.Response;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos1
{
    public partial class Approve : PageBase
    {
        ImportMemberInfos1Controller controller = new ImportMemberInfos1Controller();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                   
                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    ResetPasswordUtil rpu = new ResetPasswordUtil();

                    DataTable dtTrans = new DataTable();
                    List<DataSet> dsList = controller.ApproveImportMember(idList, out dtTrans);

                    if (dtTrans == null || dtTrans.Rows.Count == 0)
                    {
                        Logger.Instance.WriteOperationLog("Approve ImportMember ", "No Data!");
                    }

                    this.Grid1.DataSource = dtTrans;
                    this.Grid1.DataBind();

                    foreach (DataSet ds in dsList)
                    {
                        BatchSendMessageRequest request = new BatchSendMessageRequest();
                        List<MemberAccountInfo> mList = new List<MemberAccountInfo>();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                MemberAccountInfo _mInfo = new MemberAccountInfo();
                                _mInfo.MPASSWORD = item["MemberPassword"].ToString();
                                _mInfo.MEMBERID = item["MemberID"].ToString();
                                _mInfo.MSGACCOUNT = item["MemberRegisterMobile"].ToString();
                                _mInfo.MSGACCOUNTTYPE = "1";

                                mList.Add(_mInfo);
                            }
                        }
                        request.MemberAccountInfo = mList.ToArray();
                        ResultInfo[] ris = rpu.BatchSendMessage(request, SVASessionInfo.SiteLanguage);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}