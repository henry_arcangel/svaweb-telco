﻿using System;
using System.Reflection;
using System.Configuration;
using Edge.SVA.IDAL;
using Edge.Common;
namespace Edge.SVA.DALFactory
{
    /// <summary>
    /// 抽象工厂模式创建DAL。
    /// web.config 需要加入配置：(利用工厂模式+反射机制+缓存机制,实现动态创建不同的数据层对象接口)  
    /// DataCache类在导出代码的文件夹里
    /// <appSettings>  
    /// <add key="DAL" value="Edge.SVA.SQLServerDAL" /> (这里的命名空间根据实际情况更改为自己项目的命名空间)
    /// </appSettings> 
    /// </summary>
    public sealed class DataAccess
    {
        private static readonly string AssemblyPath = ConfigurationManager.AppSettings["DAL"];
        /// <summary>
        /// 创建对象或从缓存获取
        /// </summary>
        public static object CreateObject(string AssemblyPath, string ClassNamespace)
        {
            object objType = DataCache.GetCache(ClassNamespace);//从缓存读取
            if (objType == null)
            {
                try
                {
                    objType = Assembly.Load(AssemblyPath).CreateInstance(ClassNamespace);//反射创建
                    DataCache.SetCache(ClassNamespace, objType);// 写入缓存
                }
                catch
                { }
            }
            return objType;
        }


        /// <summary>
        /// 创建Address数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IAddress CreateAddress()
        {

            string ClassNamespace = AssemblyPath + ".Address";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IAddress)objType;
        }


        /// <summary>
        /// 创建Bank数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IBank CreateBank()
        {

            string ClassNamespace = AssemblyPath + ".Bank";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBank)objType;
        }


        /// <summary>
        /// 创建Batch数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IBatch CreateBatch()
        {

            string ClassNamespace = AssemblyPath + ".Batch";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBatch)objType;
        }


        /// <summary>
        /// 创建BatchActive数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IBatchActive CreateBatchActive()
        {

            string ClassNamespace = AssemblyPath + ".BatchActive";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBatchActive)objType;
        }


        /// <summary>
        /// 创建Brand数据层接口。品牌表
        /// </summary>
        public static Edge.SVA.IDAL.IBrand CreateBrand()
        {

            string ClassNamespace = AssemblyPath + ".Brand";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBrand)objType;
        }


        /// <summary>
        /// 创建Campaign数据层接口。活动表
        /// </summary>
        public static Edge.SVA.IDAL.ICampaign CreateCampaign()
        {

            string ClassNamespace = AssemblyPath + ".Campaign";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICampaign)objType;
        }


        /// <summary>
        /// 创建Card数据层接口。会员卡表
        /// </summary>
        public static Edge.SVA.IDAL.ICard CreateCard()
        {

            string ClassNamespace = AssemblyPath + ".Card";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICard)objType;
        }


        /// <summary>
        /// 创建Card_Movement数据层接口。Card操作的流水表。通过insert触发器
        /// </summary>
        public static Edge.SVA.IDAL.ICard_Movement CreateCard_Movement()
        {

            string ClassNamespace = AssemblyPath + ".Card_Movement";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICard_Movement)objType;
        }


        /// <summary>
        /// 创建CardAutoAddValueRule数据层接口。Card操作的流水表。通过insert触发器
        /// </summary>
        public static Edge.SVA.IDAL.ICardAutoAddValueRule CreateCardAutoAddValueRule()
        {

            string ClassNamespace = AssemblyPath + ".CardAutoAddValueRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardAutoAddValueRule)objType;
        }


        /// <summary>
        /// 创建CardCashDetail数据层接口。卡金额记录明细表。（记录每一笔增加的记录
        /// </summary>
        public static Edge.SVA.IDAL.ICardCashDetail CreateCardCashDetail()
        {

            string ClassNamespace = AssemblyPath + ".CardCashDetail";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardCashDetail)objType;
        }


        /// <summary>
        /// 创建CardExtensionRule数据层接口。卡有效期延长规则
        //增值规则阶梯设置：
        //增值满     延长时间
        //100           90 天
        //200           180天
        //500           360天
        //匹配时按照金额排序
        /// </summary>
        public static Edge.SVA.IDAL.ICardExtensionRule CreateCardExtensionRule()
        {

            string ClassNamespace = AssemblyPath + ".CardExtensionRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardExtensionRule)objType;
        }


        /// <summary>
        /// 创建CardGrade数据层接口。卡等级表
        /// </summary>
        public static Edge.SVA.IDAL.ICardGrade CreateCardGrade()
        {

            string ClassNamespace = AssemblyPath + ".CardGrade";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardGrade)objType;
        }


        /// <summary>
        /// 创建CardIssueHistory数据层接口。卡等级表
        /// </summary>
        public static Edge.SVA.IDAL.ICardIssueHistory CreateCardIssueHistory()
        {

            string ClassNamespace = AssemblyPath + ".CardIssueHistory";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardIssueHistory)objType;
        }

        /// <summary>
        /// 创建CardPointDetail数据层接口。卡积分记录明细表。（记录每一笔增加的记录
        /// </summary>
        public static Edge.SVA.IDAL.ICardPointDetail CreateCardPointDetail()
        {

            string ClassNamespace = AssemblyPath + ".CardPointDetail";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardPointDetail)objType;
        }


        /// <summary>
        /// 创建CardType数据层接口。卡类型表
        /// </summary>
        public static Edge.SVA.IDAL.ICardType CreateCardType()
        {

            string ClassNamespace = AssemblyPath + ".CardType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardType)objType;
        }


        /// <summary>
        /// 创建CardTypeNature数据层接口。店铺类型表。固定数据：1：member card。 2：staff card。 3：store value card。 4：cash card（含有初始金额
        /// </summary>
        public static Edge.SVA.IDAL.ICardTypeNature CreateCardTypeNature()
        {

            string ClassNamespace = AssemblyPath + ".CardTypeNature";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardTypeNature)objType;
        }


        /// <summary>
        /// 创建Company数据层接口。公司表
        /// </summary>
        public static Edge.SVA.IDAL.ICompany CreateCompany()
        {

            string ClassNamespace = AssemblyPath + ".Company";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICompany)objType;
        }


        /// <summary>
        /// 创建Coupon数据层接口。优惠劵表
        /// </summary>
        public static Edge.SVA.IDAL.ICoupon CreateCoupon()
        {

            string ClassNamespace = AssemblyPath + ".Coupon";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICoupon)objType;
        }


        /// <summary>
        /// 创建Coupon_Movement数据层接口。优惠劵操作流水记录表
        /// </summary>
        public static Edge.SVA.IDAL.ICoupon_Movement CreateCoupon_Movement()
        {

            string ClassNamespace = AssemblyPath + ".Coupon_Movement";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICoupon_Movement)objType;
        }


        /// <summary>
        /// 创建CouponType数据层接口。优惠劵类型表
        /// </summary>
        public static Edge.SVA.IDAL.ICouponType CreateCouponType()
        {

            string ClassNamespace = AssemblyPath + ".CouponType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponType)objType;
        }

        /// <summary>
        /// 创建CouponTypeNature数据层接口。优惠劵大类表。
        //1
        /// </summary>
        public static Edge.SVA.IDAL.ICouponTypeNature CreateCouponTypeNature()
        {

            string ClassNamespace = AssemblyPath + ".CouponTypeNature";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponTypeNature)objType;
        }

        /// <summary>
        /// 创建CourierPosition数据层接口。送货员位置表
        /// </summary>
        public static Edge.SVA.IDAL.ICourierPosition CreateCourierPosition()
        {

            string ClassNamespace = AssemblyPath + ".CourierPosition";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICourierPosition)objType;
        }


        /// <summary>
        /// 创建DataModels数据层接口。送货员位置表
        /// </summary>
        public static Edge.SVA.IDAL.IDataModels CreateDataModels()
        {

            string ClassNamespace = AssemblyPath + ".DataModels";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDataModels)objType;
        }


        /// <summary>
        /// 创建DEPARTMENT数据层接口。送货员位置表
        /// </summary>
        public static Edge.SVA.IDAL.IDepartment CreateDepartment()
        {

            string ClassNamespace = AssemblyPath + ".Department";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDepartment)objType;
        }


        /// <summary>
        /// 创建EarnCouponRule数据层接口。获取优惠劵的规则表
        /// </summary>
        public static Edge.SVA.IDAL.IEarnCouponRule CreateEarnCouponRule()
        {

            string ClassNamespace = AssemblyPath + ".EarnCouponRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IEarnCouponRule)objType;
        }


        /// <summary>
        /// 创建Education数据层接口。学历表
        /// </summary>
        public static Edge.SVA.IDAL.IEducation CreateEducation()
        {

            string ClassNamespace = AssemblyPath + ".Education";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IEducation)objType;
        }


        /// <summary>
        /// 创建Industry数据层接口。行业表
        /// </summary>
        public static Edge.SVA.IDAL.IIndustry CreateIndustry()
        {

            string ClassNamespace = AssemblyPath + ".Industry";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IIndustry)objType;
        }


        /// <summary>
        /// 创建LanguageMap数据层接口。kiosk调用
        /// </summary>
        public static Edge.SVA.IDAL.ILanguageMap CreateLanguageMap()
        {

            string ClassNamespace = AssemblyPath + ".LanguageMap";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ILanguageMap)objType;
        }


        /// <summary>
        /// 创建Member数据层接口。会员表
        /// </summary>
        public static Edge.SVA.IDAL.IMember CreateMember()
        {

            string ClassNamespace = AssemblyPath + ".Member";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMember)objType;
        }


        /// <summary>
        /// 创建MemberAddress数据层接口。会员地址表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberAddress CreateMemberAddress()
        {

            string ClassNamespace = AssemblyPath + ".MemberAddress";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberAddress)objType;
        }


        /// <summary>
        /// 创建MemberCompanyStore数据层接口。会员地址表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberCompanyStore CreateMemberCompanyStore()
        {

            string ClassNamespace = AssemblyPath + ".MemberCompanyStore";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberCompanyStore)objType;
        }


        /// <summary>
        /// 创建MemberDepartment数据层接口。会员部门表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberDepartment CreateMemberDepartment()
        {

            string ClassNamespace = AssemblyPath + ".MemberDepartment";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberDepartment)objType;
        }


        /// <summary>
        /// 创建Nation数据层接口。国家表
        /// </summary>
        public static Edge.SVA.IDAL.INation CreateNation()
        {

            string ClassNamespace = AssemblyPath + ".Nation";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.INation)objType;
        }


        /// <summary>
        /// 创建OtherAccount数据层接口。其他账号
        /// </summary>
        public static Edge.SVA.IDAL.IOtherAccount CreateOtherAccount()
        {

            string ClassNamespace = AssemblyPath + ".OtherAccount";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOtherAccount)objType;
        }


        /// <summary>
        /// 创建OtherExchangeRate数据层接口。与第三方的兑换比率设
        /// </summary>
        public static Edge.SVA.IDAL.IOtherExchangeRate CreateOtherExchangeRate()
        {

            string ClassNamespace = AssemblyPath + ".OtherExchangeRate";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOtherExchangeRate)objType;
        }


        /// <summary>
        /// 创建PointRule数据层接口。卡消费赚积分规则表
        /// </summary>
        public static Edge.SVA.IDAL.IPointRule CreatePointRule()
        {

            string ClassNamespace = AssemblyPath + ".PointRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPointRule)objType;
        }


        /// <summary>
        /// 创建Position数据层接口。卡消费赚积分规则表
        /// </summary>
        public static Edge.SVA.IDAL.IPosition CreatePosition()
        {

            string ClassNamespace = AssemblyPath + ".Position";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPosition)objType;
        }


        /// <summary>
        /// 创建POSSalesData数据层接口。卡消费赚积分规则表
        /// </summary>
        public static Edge.SVA.IDAL.IPOSSalesData CreatePOSSalesData()
        {

            string ClassNamespace = AssemblyPath + ".POSSalesData";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPOSSalesData)objType;
        }


        /// <summary>
        /// 创建Profession数据层接口。专业表
        /// </summary>
        public static Edge.SVA.IDAL.IProfession CreateProfession()
        {

            string ClassNamespace = AssemblyPath + ".Profession";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IProfession)objType;
        }


        /// <summary>
        /// 创建PromotionMsg数据层接口。优惠信息表
        /// </summary>
        public static Edge.SVA.IDAL.IPromotionMsg CreatePromotionMsg()
        {

            string ClassNamespace = AssemblyPath + ".PromotionMsg";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotionMsg)objType;
        }


        /// <summary>
        /// 创建QuerySets数据层接口。优惠信息表
        /// </summary>
        public static Edge.SVA.IDAL.IQuerySets CreateQuerySets()
        {

            string ClassNamespace = AssemblyPath + ".QuerySets";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IQuerySets)objType;
        }


        /// <summary>
        /// 创建Reason数据层接口。原因列表
        /// </summary>
        public static Edge.SVA.IDAL.IReason CreateReason()
        {

            string ClassNamespace = AssemblyPath + ".Reason";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IReason)objType;
        }


        /// <summary>
        /// 创建ReceiveTxn数据层接口。接收到的操作指令记录
        /// </summary>
        public static Edge.SVA.IDAL.IReceiveTxn CreateReceiveTxn()
        {

            string ClassNamespace = AssemblyPath + ".ReceiveTxn";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IReceiveTxn)objType;
        }


        /// <summary>
        /// 创建REFNO数据层接口。交易单单号维护表。
        /// </summary>
        public static Edge.SVA.IDAL.IREFNO CreateREFNO()
        {

            string ClassNamespace = AssemblyPath + ".REFNO";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IREFNO)objType;
        }


        /// <summary>
        /// 创建RSAKeyTable数据层接口。交易单单号维护表。
        /// </summary>
        public static Edge.SVA.IDAL.IRSAKeyTable CreateRSAKeyTable()
        {

            string ClassNamespace = AssemblyPath + ".RSAKeyTable";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IRSAKeyTable)objType;
        }


        /// <summary>
        /// 创建Sales_H数据层接口。销售单主表（字段暂定）表中会员部分
        /// </summary>
        public static Edge.SVA.IDAL.ISales_H CreateSales_H()
        {

            string ClassNamespace = AssemblyPath + ".Sales_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISales_H)objType;
        }


        /// <summary>
        /// 创建Schedule数据层接口。销售单主表（字段暂定）表中会员部分
        /// </summary>
        public static Edge.SVA.IDAL.ISchedule CreateSchedule()
        {

            string ClassNamespace = AssemblyPath + ".Schedule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISchedule)objType;
        }


        /// <summary>
        /// 创建ScheduleWeek数据层接口。销售单主表（字段暂定）表中会员部分
        /// </summary>
        public static Edge.SVA.IDAL.IScheduleWeek CreateScheduleWeek()
        {

            string ClassNamespace = AssemblyPath + ".ScheduleWeek";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IScheduleWeek)objType;
        }


        /// <summary>
        /// 创建StaffUpdateXLS_D数据层接口。销售单主表（字段暂定）表中会员部分
        /// </summary>
        public static Edge.SVA.IDAL.IStaffUpdateXLS_D CreateStaffUpdateXLS_D()
        {

            string ClassNamespace = AssemblyPath + ".StaffUpdateXLS_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStaffUpdateXLS_D)objType;
        }


        /// <summary>
        /// 创建StaffUpdateXLS_M数据层接口。销售单主表（字段暂定）表中会员部分
        /// </summary>
        public static Edge.SVA.IDAL.IStaffUpdateXLS_M CreateStaffUpdateXLS_M()
        {

            string ClassNamespace = AssemblyPath + ".StaffUpdateXLS_M";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStaffUpdateXLS_M)objType;
        }


        /// <summary>
        /// 创建Store数据层接口。店铺表
        /// </summary>
        public static Edge.SVA.IDAL.IStore CreateStore()
        {

            string ClassNamespace = AssemblyPath + ".Store";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStore)objType;
        }


        /// <summary>
        /// 创建StoreGroup数据层接口。店铺组
        /// </summary>
        public static Edge.SVA.IDAL.IStoreGroup CreateStoreGroup()
        {

            string ClassNamespace = AssemblyPath + ".StoreGroup";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStoreGroup)objType;
        }


        /// <summary>
        /// 创建StoreType数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.IStoreType CreateStoreType()
        {

            string ClassNamespace = AssemblyPath + ".StoreType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStoreType)objType;
        }


        /// <summary>
        /// 创建SVA_Transfer_D数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_Transfer_D CreateSVA_Transfer_D()
        {

            string ClassNamespace = AssemblyPath + ".SVA_Transfer_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_Transfer_D)objType;
        }


        /// <summary>
        /// 创建SVA_Transfer_H数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_Transfer_H CreateSVA_Transfer_H()
        {

            string ClassNamespace = AssemblyPath + ".SVA_Transfer_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_Transfer_H)objType;
        }


        /// <summary>
        /// 创建SVA_ValueAdjust_D数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_ValueAdjust_D CreateSVA_ValueAdjust_D()
        {

            string ClassNamespace = AssemblyPath + ".SVA_ValueAdjust_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_ValueAdjust_D)objType;
        }


        /// <summary>
        /// 创建SVA_ValueAdjust_H数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_ValueAdjust_H CreateSVA_ValueAdjust_H()
        {

            string ClassNamespace = AssemblyPath + ".SVA_ValueAdjust_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_ValueAdjust_H)objType;
        }


        /// <summary>
        /// 创建SVA_VoidCard_D数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_VoidCard_D CreateSVA_VoidCard_D()
        {

            string ClassNamespace = AssemblyPath + ".SVA_VoidCard_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_VoidCard_D)objType;
        }


        /// <summary>
        /// 创建SVA_VoidCard_H数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISVA_VoidCard_H CreateSVA_VoidCard_H()
        {

            string ClassNamespace = AssemblyPath + ".SVA_VoidCard_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVA_VoidCard_H)objType;
        }


        /// <summary>
        /// 创建TENDER数据层接口。店铺类型表
        /// </summary>
        public static Edge.SVA.IDAL.ITENDER CreateTENDER()
        {

            string ClassNamespace = AssemblyPath + ".TENDER";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ITENDER)objType;
        }


        /// <summary>
        /// 创建VenCardIDMap数据层接口。实体卡绑定表
        /// </summary>
        public static Edge.SVA.IDAL.IVenCardIDMap CreateVenCardIDMap()
        {

            string ClassNamespace = AssemblyPath + ".VenCardIDMap";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IVenCardIDMap)objType;
        }


        /// <summary>
        /// 创建VENDOR数据层接口。实体卡绑定表
        /// </summary>
        public static Edge.SVA.IDAL.IVENDOR CreateVENDOR()
        {

            string ClassNamespace = AssemblyPath + ".VENDOR";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IVENDOR)objType;
        }

        /// <summary>
        /// 创建MemberClause数据层接口。会员条款
        /// </summary>
        public static Edge.SVA.IDAL.IMemberClause CreateMemberClause()
        {

            string ClassNamespace = AssemblyPath + ".MemberClause";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberClause)objType;
        }


        /// <summary>
        /// 创建PasswordRule数据层接口。密码规则
        /// </summary>
        public static Edge.SVA.IDAL.IPasswordRule CreatePasswordRule()
        {

            string ClassNamespace = AssemblyPath + ".PasswordRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPasswordRule)objType;
        }
        /// <summary>
        /// 创建RelationRoleIssuer数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IRelationRoleIssuer CreateRelationRoleIssuer()
        {

            string ClassNamespace = AssemblyPath + ".RelationRoleIssuer";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IRelationRoleIssuer)objType;
        }

        /// <summary>
        /// 创建MemberSNSAccount数据层接口。交友系统账号
        /// </summary>
        public static Edge.SVA.IDAL.IMemberSNSAccount CreateMemberSNSAccount()
        {

            string ClassNamespace = AssemblyPath + ".MemberSNSAccount";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberSNSAccount)objType;
        }


        /// <summary>
        /// 创建SNSType数据层接口。交友系统类型表
        /// </summary>
        public static Edge.SVA.IDAL.ISNSType CreateSNSType()
        {

            string ClassNamespace = AssemblyPath + ".SNSType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISNSType)objType;
        }

        /// <summary>
        /// 创建Location数据层接口。区域
        /// </summary>
        public static Edge.SVA.IDAL.ILocation CreateLocation()
        {

            string ClassNamespace = AssemblyPath + ".Location";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ILocation)objType;
        }

        /// <summary>
        /// 创建BrandStoreRelation数据层接口。品牌店铺关系表
        /// </summary>
        public static Edge.SVA.IDAL.IBrandStoreRelation CreateBrandStoreRelation()
        {

            string ClassNamespace = AssemblyPath + ".BrandStoreRelation";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBrandStoreRelation)objType;
        }

        /// <summary>
        /// 创建MemberFriend数据层接口。好友表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberFriend CreateMemberFriend()
        {

            string ClassNamespace = AssemblyPath + ".MemberFriend";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberFriend)objType;
        }

        /// <summary>
        /// 创建CouponTypeExchangeBinding数据层接口。优惠劵绑定的兑换列表
        /// </summary>
        public static Edge.SVA.IDAL.ICouponTypeExchangeBinding CreateCouponTypeExchangeBinding()
        {

            string ClassNamespace = AssemblyPath + ".CouponTypeExchangeBinding";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponTypeExchangeBinding)objType;
        }


        /// <summary>
        /// 创建CouponTypeStoreCondition数据层接口。优惠劵的店铺条件
        /// </summary>
        public static Edge.SVA.IDAL.ICouponTypeStoreCondition CreateCouponTypeStoreCondition()
        {

            string ClassNamespace = AssemblyPath + ".CouponTypeStoreCondition";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponTypeStoreCondition)objType;
        }

        /// <summary>
        /// 创建CardGradeStoreCondition数据层接口。卡级别的店铺条件
        /// </summary>
        public static Edge.SVA.IDAL.ICardGradeStoreCondition CreateCardGradeStoreCondition()
        {

            string ClassNamespace = AssemblyPath + ".CardGradeStoreCondition";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardGradeStoreCondition)objType;
        }


        /// <summary>
        /// 创建CardGradeStoreCondition_List数据层接口。店铺条件子表（店铺列表）根据店铺查询条件子表（CardGradeStoreCondition）
        /// </summary>
        public static Edge.SVA.IDAL.ICardGradeStoreCondition_List CreateCardGradeStoreCondition_List()
        {

            string ClassNamespace = AssemblyPath + ".CardGradeStoreCondition_List";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardGradeStoreCondition_List)objType;
        }

        /// <summary>
        /// 创建Ord_CardBatchCreate数据层接口。卡批量创建表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardBatchCreate CreateOrd_CardBatchCreate()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardBatchCreate";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardBatchCreate)objType;
        }

        /// <summary>
        /// 创建Ord_CouponBatchCreate数据层接口。优惠劵批量创建表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponBatchCreate CreateOrd_CouponBatchCreate()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponBatchCreate";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponBatchCreate)objType;
        }

        /// <summary>
        /// 创建Ord_ImportCouponUID_D数据层接口。导入Coupon的U
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCouponUID_D CreateOrd_ImportCouponUID_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCouponUID_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCouponUID_D)objType;
        }

        /// <summary>
        /// 创建Ord_ImportCouponUID_H数据层接口。导入Coupon的UID。（销售实体coupon时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCouponUID_H CreateOrd_ImportCouponUID_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCouponUID_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCouponUID_H)objType;
        }

        /// <summary>
        /// 创建Ord_CardAdjust_D数据层接口。卡调整子表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardAdjust_D CreateOrd_CardAdjust_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardAdjust_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardAdjust_D)objType;
        }

        /// <summary>
        /// 创建Ord_CardAdjust_H数据层接口。卡调整单据主表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardAdjust_H CreateOrd_CardAdjust_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardAdjust_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardAdjust_H)objType;
        }


        /// <summary>
        /// 创建Ord_CardTransfer数据层接口。转赠单据表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardTransfer CreateOrd_CardTransfer()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardTransfer";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardTransfer)objType;
        }


        /// <summary>
        /// 创建Ord_CouponAdjust_D数据层接口。优惠劵调整子表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponAdjust_D CreateOrd_CouponAdjust_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponAdjust_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponAdjust_D)objType;
        }


        /// <summary>
        /// 创建Ord_CouponAdjust_H数据层接口。优惠劵调整单据主表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponAdjust_H CreateOrd_CouponAdjust_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponAdjust_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponAdjust_H)objType;
        }

        /// <summary>
        /// 创建CouponTypeStoreCondition_List数据层接口。店铺条件子表（店铺列表）根据店铺查询条件子表（CouponTypeStoreCondition）
        /// </summary>
        public static Edge.SVA.IDAL.ICouponTypeStoreCondition_List CreateCouponTypeStoreCondition_List()
        {

            string ClassNamespace = AssemblyPath + ".CouponTypeStoreCondition_List";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponTypeStoreCondition_List)objType;
        }

        /// <summary>
        /// 创建Product数据层接口。货品表
        /// </summary>
        public static Edge.SVA.IDAL.IProduct CreateProduct()
        {

            string ClassNamespace = AssemblyPath + ".Product";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IProduct)objType;
        }


        /// <summary>
        /// 创建Product_Price数据层接口。货品价格表。（目前阶段
        /// </summary>
        public static Edge.SVA.IDAL.IProduct_Price CreateProduct_Price()
        {

            string ClassNamespace = AssemblyPath + ".Product_Price";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IProduct_Price)objType;
        }

        /// <summary>
        /// 创建CouponUIDMap数据层接口。优惠劵号码与UID对
        /// </summary>
        public static Edge.SVA.IDAL.ICouponUIDMap CreateCouponUIDMap()
        {

            string ClassNamespace = AssemblyPath + ".CouponUIDMap";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponUIDMap)objType;
        }

        /// <summary>
        /// 创建BatchCoupon数据层接口。Coupon批次表
        /// </summary>
        public static Edge.SVA.IDAL.IBatchCoupon CreateBatchCoupon()
        {

            string ClassNamespace = AssemblyPath + ".BatchCoupon";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBatchCoupon)objType;
        }

        /// <summary>
        /// 创建RelationRoleBrand数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IRelationRoleBrand CreateRelationRoleBrand()
        {

            string ClassNamespace = AssemblyPath + ".RelationRoleBrand";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IRelationRoleBrand)objType;
        }

        /// <summary>
        /// 创建Customer数据层接口。客户表
        /// </summary>
        public static Edge.SVA.IDAL.ICustomer CreateCustomer()
        {

            string ClassNamespace = AssemblyPath + ".Customer";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICustomer)objType;
        }

        /// <summary>
        /// 创建DistributeTemplate数据层接口。分发模板发布方法
        /// </summary>
        public static Edge.SVA.IDAL.IDistributeTemplate CreateDistributeTemplate()
        {

            string ClassNamespace = AssemblyPath + ".DistributeTemplate";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDistributeTemplate)objType;
        }

        /// <summary>
        /// 创建BatchCard数据层接口。批次表
        /// </summary>
        public static Edge.SVA.IDAL.IBatchCard CreateBatchCard()
        {

            string ClassNamespace = AssemblyPath + ".BatchCard";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBatchCard)objType;
        }

        /// <summary>
        /// 创建Organization数据层接口。机构表donate
        /// </summary>
        public static Edge.SVA.IDAL.IOrganization CreateOrganization()
        {

            string ClassNamespace = AssemblyPath + ".Organization";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrganization)objType;
        }

        /// <summary>
        /// 创建Ord_ImportCouponDispense_D数据层接口。分发优惠劵明细表CardNumber：在填写数据时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCouponDispense_D CreateOrd_ImportCouponDispense_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCouponDispense_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCouponDispense_D)objType;
        }

        /// <summary>
        /// 创建Ord_ImportCouponDispense_H数据层接口。优惠劵分发单据表（主
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCouponDispense_H CreateOrd_ImportCouponDispense_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCouponDispense_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCouponDispense_H)objType;
        }

        /// <summary>
        /// 创建CardUIDMap数据层接口。Card号码和UID
        /// </summary>
        public static Edge.SVA.IDAL.ICardUIDMap CreateCardUIDMap()
        {

            string ClassNamespace = AssemblyPath + ".CardUIDMap";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardUIDMap)objType;
        }

        /// <summary>
        /// 创建Ord_CardDelivery_D数据层接口。卡送货单子表 根据 Ord_CardPicking_D产生
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardDelivery_D CreateOrd_CardDelivery_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardDelivery_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardDelivery_D)objType;
        }

        /// <summary>
        /// 创建Ord_CardDelivery_H数据层接口。卡送货单主表。（从批核后的拣货单复制过来
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardDelivery_H CreateOrd_CardDelivery_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardDelivery_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardDelivery_H)objType;
        }


        /// <summary>
        /// 创建Ord_CardOrderForm_D数据层接口。卡订货单明细。表中brandID
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardOrderForm_D CreateOrd_CardOrderForm_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardOrderForm_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardOrderForm_D)objType;
        }


        /// <summary>
        /// 创建Ord_CardOrderForm_H数据层接口。卡订货单
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardOrderForm_H CreateOrd_CardOrderForm_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardOrderForm_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardOrderForm_H)objType;
        }


        /// <summary>
        /// 创建Ord_CardPicking_D数据层接口。卡拣货单子表  表中brandID
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardPicking_D CreateOrd_CardPicking_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardPicking_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardPicking_D)objType;
        }


        /// <summary>
        /// 创建Ord_CardPicking_H数据层接口。卡拣货单主表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardPicking_H CreateOrd_CardPicking_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardPicking_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardPicking_H)objType;
        }


        /// <summary>
        /// 创建Ord_CouponDelivery_D数据层接口。优惠劵送货单子表 根据 Ord_CouponPicking_D产生
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponDelivery_D CreateOrd_CouponDelivery_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponDelivery_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponDelivery_D)objType;
        }


        /// <summary>
        /// 创建Ord_CouponDelivery_H数据层接口。优惠劵送货单主表（从批核后的拣货单复制过来
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponDelivery_H CreateOrd_CouponDelivery_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponDelivery_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponDelivery_H)objType;
        }


        /// <summary>
        /// 创建Ord_CouponOrderForm_D数据层接口。优惠劵订货单明细。表中brandID
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponOrderForm_D CreateOrd_CouponOrderForm_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponOrderForm_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponOrderForm_D)objType;
        }


        /// <summary>
        /// 创建Ord_CouponOrderForm_H数据层接口。优惠劵订货单
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponOrderForm_H CreateOrd_CouponOrderForm_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponOrderForm_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponOrderForm_H)objType;
        }


        /// <summary>
        /// 创建Ord_CouponPicking_D数据层接口。优惠劵拣货单子表  表中brandID
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponPicking_D CreateOrd_CouponPicking_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponPicking_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponPicking_D)objType;
        }


        /// <summary>
        /// 创建Ord_CouponPicking_H数据层接口。优惠劵拣货单主表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponPicking_H CreateOrd_CouponPicking_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponPicking_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponPicking_H)objType;
        }

        /// <summary>
        /// 创建MemberNotice数据层接口。会员通知发送设置表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberNotice CreateMemberNotice()
        {

            string ClassNamespace = AssemblyPath + ".MemberNotice";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberNotice)objType;
        }

        /// <summary>
        /// 创建MemberNotice_Filter数据层接口。会员消息发送对象过滤
        /// </summary>
        public static Edge.SVA.IDAL.IMemberNotice_Filter CreateMemberNotice_Filter()
        {

            string ClassNamespace = AssemblyPath + ".MemberNotice_Filter";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberNotice_Filter)objType;
        }


        /// <summary>
        /// 创建MemberNotice_MessageType数据层接口。会员消息 发送类型表
        /// </summary>
        public static Edge.SVA.IDAL.IMemberNotice_MessageType CreateMemberNotice_MessageType()
        {

            string ClassNamespace = AssemblyPath + ".MemberNotice_MessageType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberNotice_MessageType)objType;
        }


        /// <summary>
        /// 创建MessageTemplate数据层接口。消息内容模板。
        /// </summary>
        public static Edge.SVA.IDAL.IMessageTemplate CreateMessageTemplate()
        {

            string ClassNamespace = AssemblyPath + ".MessageTemplate";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMessageTemplate)objType;
        }

        /// <summary>
        /// 创建CouponNature数据层接口。优惠劵类别级别
        /// </summary>
        public static Edge.SVA.IDAL.ICouponNature CreateCouponNature()
        {

            string ClassNamespace = AssemblyPath + ".CouponNature";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponNature)objType;
        }

        /// <summary>
        /// 创建CouponNatureList数据层接口。优惠券类别列表 //Add By Robin 2015-01-07
        /// </summary>
        public static Edge.SVA.IDAL.ICouponNatureList CreateCouponNatureList()
        {

            string ClassNamespace = AssemblyPath + ".CouponNatureList";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponNatureList)objType;
        }

        /// <summary>
        /// 创建CardGradeHoldCouponRule数据层接口。每个CardGrad
        /// </summary>
        public static Edge.SVA.IDAL.ICardGradeHoldCouponRule CreateCardGradeHoldCouponRule()
        {

            string ClassNamespace = AssemblyPath + ".CardGradeHoldCouponRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardGradeHoldCouponRule)objType;
        }


        /// <summary>
        /// 创建CouponReplenishDaily数据层接口。Coupon每日自动
        /// </summary>
        public static Edge.SVA.IDAL.ICouponReplenishDaily CreateCouponReplenishDaily()
        {

            string ClassNamespace = AssemblyPath + ".CouponReplenishDaily";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponReplenishDaily)objType;
        }

        /// <summary>
        /// 创建CampaignCardGrade数据层接口。Campaign和C
        /// </summary>
        public static Edge.SVA.IDAL.ICampaignCardGrade CreateCampaignCardGrade()
        {

            string ClassNamespace = AssemblyPath + ".CampaignCardGrade";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICampaignCardGrade)objType;
        }

        /// <summary>
        /// 创建CampaignCouponType数据层接口。Campaign和C
        /// </summary>
        public static Edge.SVA.IDAL.ICampaignCouponType CreateCampaignCouponType()
        {

            string ClassNamespace = AssemblyPath + ".CampaignCouponType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICampaignCouponType)objType;
        }
        /// <summary>
        /// 创建City数据层接口。市资料表。（省下一级
        /// </summary>
        public static Edge.SVA.IDAL.ICity CreateCity()
        {

            string ClassNamespace = AssemblyPath + ".City";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICity)objType;
        }


        /// <summary>
        /// 创建Country数据层接口。国家资料表
        /// </summary>
        public static Edge.SVA.IDAL.ICountry CreateCountry()
        {

            string ClassNamespace = AssemblyPath + ".Country";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICountry)objType;
        }


        /// <summary>
        /// 创建Province数据层接口。省资料表。（中国一级行政区
        /// </summary>
        public static Edge.SVA.IDAL.IProvince CreateProvince()
        {

            string ClassNamespace = AssemblyPath + ".Province";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IProvince)objType;
        }


        /// <summary>
        /// 创建District数据层接口。区县资料表。（city下一级。中国第三级行政区
        /// </summary>
        public static Edge.SVA.IDAL.IDistrict CreateDistrict()
        {

            string ClassNamespace = AssemblyPath + ".District";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDistrict)objType;
        }
        /// <summary>
        /// 创建MemberMessageAccount数据层接口。会员消息服务账号
        /// </summary>
        public static Edge.SVA.IDAL.IMemberMessageAccount CreateMemberMessageAccount()
        {

            string ClassNamespace = AssemblyPath + ".MemberMessageAccount";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMemberMessageAccount)objType;
        }
        /// <summary>
        /// 创建USEFULEXPRESSIONS数据层接口。常用语表
        /// </summary>
        public static Edge.SVA.IDAL.IUSEFULEXPRESSIONS CreateUSEFULEXPRESSIONS()
        {

            string ClassNamespace = AssemblyPath + ".USEFULEXPRESSIONS";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IUSEFULEXPRESSIONS)objType;
        }
        /// <summary>
        /// 创建PromotionCardCondition数据层接口。促销信息（广告）的卡
        /// </summary>
        public static Edge.SVA.IDAL.IPromotionCardCondition CreatePromotionCardCondition()
        {

            string ClassNamespace = AssemblyPath + ".PromotionCardCondition";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotionCardCondition)objType;
        }
        /// <summary>
        /// 创建OperationTable数据层接口。OprID表。
        /// </summary>
        public static Edge.SVA.IDAL.IOperationTable CreateOperationTable()
        {

            string ClassNamespace = AssemblyPath + ".OperationTable";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOperationTable)objType;
        }
        /// <summary>
        /// 创建MessageTemplateDetail数据层接口。消息内容模板明细。
        /// </summary>
        public static Edge.SVA.IDAL.IMessageTemplateDetail CreateMessageTemplateDetail()
        {

            string ClassNamespace = AssemblyPath + ".MessageTemplateDetail";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMessageTemplateDetail)objType;
        }
        /// <summary>
        /// 创建PromotionMsgType数据层接口。优惠消息类型表
        /// </summary>
        public static Edge.SVA.IDAL.IPromotionMsgType CreatePromotionMsgType()
        {

            string ClassNamespace = AssemblyPath + ".PromotionMsgType";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotionMsgType)objType;
        }
        /// <summary>
        /// 创建Ord_CouponPush_D数据层接口。推送优惠劵（子表）（
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponPush_D CreateOrd_CouponPush_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponPush_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponPush_D)objType;
        }


        /// <summary>
        /// 创建Ord_CouponPush_H数据层接口。推送优惠劵（主表）（注：根据提供的UI需求
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponPush_H CreateOrd_CouponPush_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponPush_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponPush_H)objType;
        }
        /// <summary>
        /// 创建Ord_ImportMember_H数据层接口。导入Member数据（主表） （MII）（只是存储记录
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportMember_H CreateOrd_ImportMember_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportMember_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportMember_H)objType;
        }
        /// <summary>
        /// 创建SVARewardRules数据层接口。会员推荐新会员奖励规
        /// </summary>
        public static Edge.SVA.IDAL.ISVARewardRules CreateSVARewardRules()
        {

            string ClassNamespace = AssemblyPath + ".SVARewardRules";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISVARewardRules)objType;
        }
        /// <summary>
        /// 创建Ord_CreateMember_D数据层接口。创建Member记录
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CreateMember_D CreateOrd_CreateMember_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CreateMember_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CreateMember_D)objType;
        }


        /// <summary>
        /// 创建Ord_CreateMember_H数据层接口。创建产生Member
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CreateMember_H CreateOrd_CreateMember_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CreateMember_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CreateMember_H)objType;
        }


        /// <summary>
        /// 创建Sales_T数据层接口。?úê?μ￥×ó±í
        /// </summary>
        public static Edge.SVA.IDAL.ISales_T CreateSales_T()
        {

            string ClassNamespace = AssemblyPath + ".Sales_T";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISales_T)objType;
        }
        /// <summary>
        /// 创建Sales_D数据层接口。?úê?μ￥×ó±í
        /// </summary>
        public static Edge.SVA.IDAL.ISales_D CreateSales_D()
        {

            string ClassNamespace = AssemblyPath + ".Sales_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISales_D)objType;
        }
        /// <summary>
        /// 创建LogisticsProvider数据层接口。物流供应商
        /// </summary>
        public static Edge.SVA.IDAL.ILogisticsProvider CreateLogisticsProvider()
        {

            string ClassNamespace = AssemblyPath + ".LogisticsProvider";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ILogisticsProvider)objType;
        }

        /// <summary>
        /// 创建Product_Size数据层接口。产品尺寸属性。
        /// </summary>
        public static Edge.SVA.IDAL.IProduct_Size CreateProduct_Size()
        {

            string ClassNamespace = AssemblyPath + ".Product_Size";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IProduct_Size)objType;
        }
        /// <summary>
        /// 创建Color数据层接口。颜色表
        /// </summary>
        public static Edge.SVA.IDAL.IColor CreateColor()
        {

            string ClassNamespace = AssemblyPath + ".Color";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IColor)objType;
        }

        /// <summary>
        /// 创建School数据层接口。学校表
        /// </summary>
        public static Edge.SVA.IDAL.ISchool CreateSchool()
        {

            string ClassNamespace = AssemblyPath + ".School";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISchool)objType;
        }

        /// <summary>
        /// 创建CardIssuer数据层接口。卡发行方
        /// </summary>
        public static Edge.SVA.IDAL.ICardIssuer CreateCardIssuer()
        {

            string ClassNamespace = AssemblyPath + ".CardIssuer";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardIssuer)objType;
        }

        /// <summary>
        /// 创建Currency数据层接口。币种表
        /// </summary>
        public static Edge.SVA.IDAL.ICurrency CreateCurrency()
        {

            string ClassNamespace = AssemblyPath + ".Currency";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICurrency)objType;
        }

        /// <summary>
        /// 创建Sponsor数据层接口。赞助商
        /// </summary>
        public static Edge.SVA.IDAL.ISponsor CreateSponsor()
        {

            string ClassNamespace = AssemblyPath + ".Sponsor";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISponsor)objType;
        }

        /// <summary>
        /// 创建Supplier数据层接口。供货商
        /// </summary>
        public static Edge.SVA.IDAL.ISupplier CreateSupplier()
        {

            string ClassNamespace = AssemblyPath + ".Supplier";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ISupplier)objType;
        }

        /// <summary>
        /// 创建CardParentChild数据层接口。卡父子关系表。（目前为方便svaweb的设置UI)
        /// </summary>
        public static Edge.SVA.IDAL.ICardParentChild CreateCardParentChild()
        {

            string ClassNamespace = AssemblyPath + ".CardParentChild";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardParentChild)objType;
        }

        /// <summary>
        /// 创建Ord_ImportMember_D数据层接口。Ord_ImportMember_H的子表。 当导入Member数据时（MII）
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportMember_D CreateOrd_ImportMember_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportMember_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportMember_D)objType;
        }

        /// <summary>
        /// 创建CouponTypeRedeemCondition数据层接口。优惠劵使用条件设置表
        /// </summary>
        public static Edge.SVA.IDAL.ICouponTypeRedeemCondition CreateCouponTypeRedeemCondition()
        {

            string ClassNamespace = AssemblyPath + ".CouponTypeRedeemCondition";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponTypeRedeemCondition)objType;
        }

        /// <summary>
        /// 创建PointRule_Item数据层接口。积分计算规则的货品条
        /// </summary>
        public static Edge.SVA.IDAL.IPointRule_Item CreatePointRule_Item()
        {

            string ClassNamespace = AssemblyPath + ".PointRule_Item";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPointRule_Item)objType;
        }

        /// <summary>
        /// 创建DAYFLAG数据层接口。天标志表
        /// </summary>
        public static Edge.SVA.IDAL.IDAYFLAG CreateDAYFLAG()
        {

            string ClassNamespace = AssemblyPath + ".DAYFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDAYFLAG)objType;
        }


        /// <summary>
        /// 创建WEEKFLAG数据层接口。周标志表
        /// </summary>
        public static Edge.SVA.IDAL.IWEEKFLAG CreateWEEKFLAG()
        {

            string ClassNamespace = AssemblyPath + ".WEEKFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IWEEKFLAG)objType;
        }


        /// <summary>
        /// 创建MONTHFLAG数据层接口。月标志表
        /// </summary>
        public static Edge.SVA.IDAL.IMONTHFLAG CreateMONTHFLAG()
        {

            string ClassNamespace = AssemblyPath + ".MONTHFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IMONTHFLAG)objType;
        }

        /// <summary>
        /// 创建数据层接口
        /// </summary>
        //public static t Create(string ClassName)
        //{
        //string ClassNamespace = AssemblyPath +"."+ ClassName;
        //object objType = CreateObject(AssemblyPath, ClassNamespace);
        //return (t)objType;
        //}
        /// <summary>
        /// 创建PointRule_Store数据层接口。积分规则适用店铺表。
        //（规则： 如果一个店铺有指定规则
        /// </summary>
        public static Edge.SVA.IDAL.IPointRule_Store CreatePointRule_Store()
        {

            string ClassNamespace = AssemblyPath + ".PointRule_Store";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPointRule_Store)objType;
        }

        /// <summary>
        /// 创建Ord_TradeManually_D数据层接口。手工交易单。 明细表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_TradeManually_D CreateOrd_TradeManually_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_TradeManually_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_TradeManually_D)objType;
        }


        /// <summary>
        /// 创建Ord_TradeManually_H数据层接口。手工交易单。 头表。 为指定会员添加交易
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_TradeManually_H CreateOrd_TradeManually_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_TradeManually_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_TradeManually_H)objType;
        }

        /// <summary>
        /// 创建CouponReplenishRule_H数据层接口。Coupon自动补货规则设置表.  根据设置
        /// </summary>
        public static Edge.SVA.IDAL.ICouponReplenishRule_H CreateCouponReplenishRule_H()
        {

            string ClassNamespace = AssemblyPath + ".CouponReplenishRule_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponReplenishRule_H)objType;
        }

        /// <summary>
        /// 创建CouponReplenishRule_D数据层接口。Coupon自动补货
        /// </summary>
        public static Edge.SVA.IDAL.ICouponReplenishRule_D CreateCouponReplenishRule_D()
        {

            string ClassNamespace = AssemblyPath + ".CouponReplenishRule_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponReplenishRule_D)objType;
        }

        /// <summary>
        /// 创建CouponReplenishRule_EMail数据层接口。Coupon自动补货
        /// </summary>
        public static Edge.SVA.IDAL.ICouponReplenishRule_EMail CreateCouponReplenishRule_EMail()
        {

            string ClassNamespace = AssemblyPath + ".CouponReplenishRule_EMail";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponReplenishRule_EMail)objType;
        }

        /// <summary>
        /// 创建InventoryReplenishRule_H数据层接口。Coupon自动补货规则设置表.  根据设置
        /// </summary>
        public static Edge.SVA.IDAL.IInventoryReplenishRule_H CreateInventoryReplenishRule_H()
        {

            string ClassNamespace = AssemblyPath + ".InventoryReplenishRule_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IInventoryReplenishRule_H)objType;
        }

        /// <summary>
        /// 创建InventoryReplenishRule_D数据层接口。Coupon自动补货
        /// </summary>
        public static Edge.SVA.IDAL.IInventoryReplenishRule_D CreateInventoryReplenishRule_D()
        {

            string ClassNamespace = AssemblyPath + ".InventoryReplenishRule_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IInventoryReplenishRule_D)objType;
        }

        /// <summary>
        /// 创建Ord_CouponReceive_H数据层接口。收货确认单。主表 （后台对供应商收货确认）工作流程： 根据订货单或者手动输入。 根据订货单时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponReceive_H CreateOrd_CouponReceive_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponReceive_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponReceive_H)objType;
        }

        /// <summary>
        /// 创建Ord_CouponReceive_D数据层接口。收货确认单子表（需要逐个Coupon收货时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponReceive_D CreateOrd_CouponReceive_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponReceive_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponReceive_D)objType;
        }

        /// <summary>
        /// 创建Ord_CardReceive_H数据层接口。收货确认单。主表 （后台对供应商收货确认）工作流程： 根据订货单或者手动输入。 根据订货单时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardReceive_H CreateOrd_CardReceive_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardReceive_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardReceive_H)objType;
        }

        /// <summary>
        /// 创建Ord_CardReceive_D数据层接口。收货确认单子表（需要逐个Card收货时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardReceive_D CreateOrd_CardReceive_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardReceive_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardReceive_D)objType;
        }

        /// <summary>
        /// 创建Ord_OrderToSupplier_H数据层接口。面向供应商的订货单（订货实体Coupon）注：工作流程：总部向供应商下订单时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_OrderToSupplier_H CreateOrd_OrderToSupplier_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_OrderToSupplier_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_OrderToSupplier_H)objType;
        }

        /// <summary>
        /// 创建Ord_OrderToSupplier_D数据层接口。面向供应商的订货单
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_OrderToSupplier_D CreateOrd_OrderToSupplier_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_OrderToSupplier_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_OrderToSupplier_D)objType;
        }

        /// <summary>
        /// 创建Ord_OrderToSupplier_Card_H数据层接口。面向供应商的订货单（订货实体Card）注：工作流程：总部向供应商下订单时
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_OrderToSupplier_Card_H CreateOrd_OrderToSupplier_Card_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_OrderToSupplier_Card_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_OrderToSupplier_Card_H)objType;
        }

        /// <summary>
        /// 创建Ord_OrderToSupplier_Card_D数据层接口。面向供应商的订货单
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_OrderToSupplier_Card_D CreateOrd_OrderToSupplier_Card_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_OrderToSupplier_Card_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_OrderToSupplier_Card_D)objType;
        }

        /// <summary>
        /// 创建Ord_CouponReturn_H数据层接口。退货确认单。主表 （
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponReturn_H CreateOrd_CouponReturn_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponReturn_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponReturn_H)objType;
        }

        /// <summary>
        /// 创建Ord_CouponReturn_D数据层接口。t退货单子表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CouponReturn_D CreateOrd_CouponReturn_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CouponReturn_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CouponReturn_D)objType;
        }

        /// <summary>
        /// 创建Ord_CardReturn_H数据层接口。退货确认单。主表 （
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardReturn_H CreateOrd_CardReturn_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardReturn_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardReturn_H)objType;
        }

        /// <summary>
        /// 创建Ord_CardReturn_D数据层接口。t退货单子表
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardReturn_D CreateOrd_CardReturn_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardReturn_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardReturn_D)objType;
        }

        /// <summary>
        /// 创建UserMessageSetting_H数据层接口。系统用户消息通知设定
        /// </summary>
        public static Edge.SVA.IDAL.IUserMessageSetting_H CreateUserMessageSetting_H()
        {

            string ClassNamespace = AssemblyPath + ".UserMessageSetting_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IUserMessageSetting_H)objType;
        }

        /// <summary>
        /// 创建UserMessageSetting_D数据层接口。系统用户消息通知设定
        /// </summary>
        public static Edge.SVA.IDAL.IUserMessageSetting_D CreateUserMessageSetting_D()
        {

            string ClassNamespace = AssemblyPath + ".UserMessageSetting_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IUserMessageSetting_D)objType;
        }

        /// <summary>
        /// 创建BUY_WEEKFLAG数据层接口。周标志表
        /// </summary>
        public static Edge.SVA.IDAL.IBUY_WEEKFLAG CreateBUY_WEEKFLAG()
        {

            string ClassNamespace = AssemblyPath + ".BUY_WEEKFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBUY_WEEKFLAG)objType;
        }

        /// <summary>
        /// 创建BUY_MONTHFLAG数据层接口。月标志表
        /// </summary>
        public static Edge.SVA.IDAL.IBUY_MONTHFLAG CreateBUY_MONTHFLAG()
        {

            string ClassNamespace = AssemblyPath + ".BUY_MONTHFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBUY_MONTHFLAG)objType;
        }

        /// <summary>
        /// 创建BUY_DAYFLAG数据层接口。天标志表
        /// </summary>
        public static Edge.SVA.IDAL.IBUY_DAYFLAG CreateBUY_DAYFLAG()
        {

            string ClassNamespace = AssemblyPath + ".BUY_DAYFLAG";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IBUY_DAYFLAG)objType;
        }

        /// <summary>
        /// 创建CouponAutoPickingRule_H数据层接口。Coupon订单（店
        /// </summary>
        public static Edge.SVA.IDAL.ICouponAutoPickingRule_H CreateCouponAutoPickingRule_H()
        {

            string ClassNamespace = AssemblyPath + ".CouponAutoPickingRule_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponAutoPickingRule_H)objType;
        }

        /// <summary>
        /// 创建CouponAutoPickingRule_D数据层接口。Coupon订单（店铺给总部的）自动产生pickup单的规则设置表   子表（BrandID和storeID 
        /// </summary>
        public static Edge.SVA.IDAL.ICouponAutoPickingRule_D CreateCouponAutoPickingRule_D()
        {

            string ClassNamespace = AssemblyPath + ".CouponAutoPickingRule_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICouponAutoPickingRule_D)objType;
        }
        //Add by Nathan 20140609 ++
        /// <summary>
        /// 创建Ord_ImportCardUID_H数据层接口。絳?Card腔UID
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCardUID_H CreateOrd_ImportCardUID_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCardUID_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCardUID_H)objType;
        }


        /// <summary>
        /// 创建Ord_ImportCardUID_D数据层接口。絳?Coupon腔U
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_ImportCardUID_D CreateOrd_ImportCardUID_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_ImportCardUID_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_ImportCardUID_D)objType;
        }

        /// <summary>
        /// 创建CardGradeExchangeBinding数据层接口。卡绑定的兑换列表
        /// </summary>
        public static Edge.SVA.IDAL.ICardGradeExchangeBinding CreateCardGradeExchangeBinding()
        {

            string ClassNamespace = AssemblyPath + ".CardGradeExchangeBinding";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICardGradeExchangeBinding)objType;
        }

        //Add by Nathan 20140609 --

        //Add by Nathan 20140630++

        /// <summary>
        /// 创建PromotionMsg_Pic数据层接口。促销消息图片表
        /// </summary>
        public static Edge.SVA.IDAL.IPromotionMsg_Pic CreatePromotionMsg_Pic()
        {

            string ClassNamespace = AssemblyPath + ".PromotionMsg_Pic";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotionMsg_Pic)objType;
        }


        /// <summary>
        /// 创建Store_Attribute数据层接口。店铺属性表。
        /// </summary>
        public static Edge.SVA.IDAL.IStore_Attribute CreateStore_Attribute()
        {

            string ClassNamespace = AssemblyPath + ".Store_Attribute";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStore_Attribute)objType;
        }


        //Add by Nathan 20140630--


        //Add by Nathan 20140702++
        /// <summary>
        /// 创建StoreAttributeList数据层接口。店铺的属性列表 （s
        /// </summary>
        public static Edge.SVA.IDAL.IStoreAttributeList CreateStoreAttributeList()
        {

            string ClassNamespace = AssemblyPath + ".StoreAttributeList";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IStoreAttributeList)objType;
        }
        //Add by Nathan 20140702--

        //add by frank 20140721
        /// <summary>
        /// 创建Promotion_H数据层接口。促销头表。 （包含所有被动促销设置.  促销会根据具体的销售单
        /// </summary>
        public static Edge.SVA.IDAL.IPromotion_H CreatePromotion_H()
        {

            string ClassNamespace = AssemblyPath + ".Promotion_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotion_H)objType;
        }


        /// <summary>
        /// 创建Promotion_Hit数据层接口。Promotion_H的子表。 促销命中条件表
        ///（多个货品固定搭配的情况
        /// </summary>
        public static Edge.SVA.IDAL.IPromotion_Hit CreatePromotion_Hit()
        {

            string ClassNamespace = AssemblyPath + ".Promotion_Hit";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotion_Hit)objType;
        }


        /// <summary>
        /// 创建Promotion_Hit_PLU数据层接口。促销命中表的指定货品
        /// </summary>
        public static Edge.SVA.IDAL.IPromotion_Hit_PLU CreatePromotion_Hit_PLU()
        {

            string ClassNamespace = AssemblyPath + ".Promotion_Hit_PLU";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotion_Hit_PLU)objType;
        }


        /// <summary>
        /// 创建Promotion_Gift数据层接口。促销礼品表。
        /// </summary>
        public static Edge.SVA.IDAL.IPromotion_Gift CreatePromotion_Gift()
        {

            string ClassNamespace = AssemblyPath + ".Promotion_Gift";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotion_Gift)objType;
        }


        /// <summary>
        /// 创建Promotion_Member数据层接口。Promotion_H的子表
        /// </summary>
        public static Edge.SVA.IDAL.IPromotion_Member CreatePromotion_Member()
        {

            string ClassNamespace = AssemblyPath + ".Promotion_Member";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IPromotion_Member)objType;
        }
        /// <summary>
        /// 创建DropdownList数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IDropdownList CreateDropdownList()
        {

            string ClassNamespace = AssemblyPath + ".DropdownList";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDropdownList)objType;
        }
        //add--

        //add++ by frank 20141012
        /// <summary>
        /// 创建Address数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IDALCRUD CreateDALCRUD()
        {

            string ClassNamespace = AssemblyPath + ".DALCRUD";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IDALCRUD)objType;
        }
        /// <summary>
        /// 创建Ord_CardTransferFrom数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardTransferFrom CreateOrd_CardTransferFrom()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardTransferFrom";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardTransferFrom)objType;
        }

        /// <summary>
        /// 创建Ord_CardTransferTo数据层接口。
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardTransferTo CreateOrd_CardTransferTo()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardTransferTo";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardTransferTo)objType;
        }
        //add--

        /// <summary>
        /// 创建Ord_CardTransfer_D数据层接口。转赠单据表. 子表
        //@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardTransfer_D CreateOrd_CardTransfer_D()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardTransfer_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardTransfer_D)objType;
        }


        /// <summary>
        /// 创建Ord_CardTransfer_H数据层接口。转赠单据表. 主表
        //@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D
        /// </summary>
        public static Edge.SVA.IDAL.IOrd_CardTransfer_H CreateOrd_CardTransfer_H()
        {

            string ClassNamespace = AssemblyPath + ".Ord_CardTransfer_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IOrd_CardTransfer_H)objType;
        }

        /// <summary>
        /// 创建GrossMargin数据层接口.  @2015-03-03
        /// </summary>
        public static Edge.SVA.IDAL.IGrossMargin CreateGrossMargin()
        {

            string ClassNamespace = AssemblyPath + ".GrossMargin";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IGrossMargin)objType;
        }

        public static Edge.SVA.IDAL.IGrossMargin_MobileNo CreateGrossMargin_MobileNo()
        {

            string ClassNamespace = AssemblyPath + ".GrossMargin_MobileNo";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IGrossMargin_MobileNo)objType;
        }

        public static Edge.SVA.IDAL.IImportGM CreateImportGM()
        {

            string ClassNamespace = AssemblyPath + ".ImportGM";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IImportGM)objType;
        }

        /// <summary>
        /// 创建WalletRule_D数据层接口。转赠单据表. 子表
        /// </summary>
        public static Edge.SVA.IDAL.IWalletRule_D CreateWalletRule_D()
        {

            string ClassNamespace = AssemblyPath + ".WalletRule_D";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IWalletRule_D)objType;
        }


        /// <summary>
        /// 创建WalletRule_H数据层接口。转赠单据表. 主表
        /// </summary>
        public static Edge.SVA.IDAL.IWalletRule_H CreateWalletRule_H()
        {

            string ClassNamespace = AssemblyPath + ".WalletRule_H";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.IWalletRule_H)objType;
        }

        public static Edge.SVA.IDAL.ICSVXMLBinding CreateCSVXMLBinding()
        {

            string ClassNamespace = AssemblyPath + ".CSVXMLBinding";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICSVXMLBinding)objType;
        }

        public static Edge.SVA.IDAL.ICSVXMLMointoringRule CreateCSVXMLMointoringRule()
        {

            string ClassNamespace = AssemblyPath + ".CSVXMLMointoringRule";
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return (Edge.SVA.IDAL.ICSVXMLMointoringRule)objType;
        }
        
    }
}