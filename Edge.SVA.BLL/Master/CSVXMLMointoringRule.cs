﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 实体卡绑定表
	/// </summary>
	public partial class CSVXMLMointoringRule
	{
        private readonly ICSVXMLMointoringRule dal = DataAccess.CreateCSVXMLMointoringRule();
		public CSVXMLMointoringRule()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.CSVXMLMointoringRule model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.CSVXMLMointoringRule model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete(code);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.CSVXMLMointoringRule GetModel(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel(code);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        public Edge.SVA.Model.CSVXMLMointoringRule GetModelByCache(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
            string CacheKey = "CSVXMLMointoringRuleModel-";
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(code);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
            return (Edge.SVA.Model.CSVXMLMointoringRule)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.CSVXMLMointoringRule> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.CSVXMLMointoringRule> DataTableToList(DataTable dt)
		{
            List<Edge.SVA.Model.CSVXMLMointoringRule> modelList = new List<Edge.SVA.Model.CSVXMLMointoringRule>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
                Edge.SVA.Model.CSVXMLMointoringRule model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = new Edge.SVA.Model.CSVXMLMointoringRule();
                    if (dt.Rows[n]["RuleName"] != null && dt.Rows[n]["RuleName"].ToString() != "")
					{
                        model.RuleName = dt.Rows[n]["RuleName"].ToString();
					}
                    if (dt.Rows[n]["Description"] != null && dt.Rows[n]["Description"].ToString() != "")
					{
                        model.Description = dt.Rows[n]["Description"].ToString();
					}
                    if (dt.Rows[n]["StartDate"] != null && dt.Rows[n]["StartDate"].ToString() != "")
					{
                        model.StartDate = DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
					}
                    if (dt.Rows[n]["EndDate"] != null && dt.Rows[n]["EndDate"].ToString() != "")
					{
                        model.EndDate = DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
					}
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
					{
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
					}
                    if (dt.Rows[n]["Func"] != null && dt.Rows[n]["Func"].ToString() != "")
					{
                        model.Func = dt.Rows[n]["Func"].ToString();
					}
                    if (dt.Rows[n]["DownloadPath"] != null && dt.Rows[n]["DownloadPath"].ToString() != "")
					{
                        model.DownloadPath = dt.Rows[n]["DownloadPath"].ToString();
					}
                    if (dt.Rows[n]["DayFlagID"] != null && dt.Rows[n]["DayFlagID"].ToString() != "")
					{
                        model.DayFlagID = int.Parse(dt.Rows[n]["DayFlagID"].ToString());
					}
                    if (dt.Rows[n]["MonthFlagID"] != null && dt.Rows[n]["MonthFlagID"].ToString() != "")
					{
                        model.MonthFlagID = int.Parse(dt.Rows[n]["MonthFlagID"].ToString());
					}
                    if (dt.Rows[n]["WeekFlagID"] != null && dt.Rows[n]["WeekFlagID"].ToString() != "")
					{
                        model.WeekFlagID = int.Parse(dt.Rows[n]["WeekFlagID"].ToString());
					}
                    if (dt.Rows[n]["ActiveTime"] != null && dt.Rows[n]["ActiveTime"].ToString() != "")
					{
                        model.ActiveTime = DateTime.Parse(dt.Rows[n]["ActiveTime"].ToString());
					}
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

