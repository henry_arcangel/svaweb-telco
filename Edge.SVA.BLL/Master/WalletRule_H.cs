﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 面向供应商的订货单（订货实体Card）
    ///注：工作流程：总部向供应商下订单时
    /// </summary>
    public partial class WalletRule_H
    {
        private readonly IWalletRule_H dal = DataAccess.CreateWalletRule_H();
        public WalletRule_H()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string OrderSupplierNumber)
        {
            return dal.Exists(OrderSupplierNumber);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.WalletRule_H model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.WalletRule_H model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string OrderSupplierNumber)
        {

            return dal.Delete(OrderSupplierNumber);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OrderSupplierNumberlist)
        {
            return dal.DeleteList(OrderSupplierNumberlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.WalletRule_H GetModel(string OrderSupplierNumber)
        {

            return dal.GetModel(OrderSupplierNumber);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.WalletRule_H GetModelByCache(string OrderSupplierNumber)
        {

            string CacheKey = "WalletRule_HModel-" + OrderSupplierNumber;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OrderSupplierNumber);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.WalletRule_H)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.WalletRule_H> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.WalletRule_H> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.WalletRule_H> modelList = new List<Edge.SVA.Model.WalletRule_H>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.WalletRule_H model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.WalletRule_H();
                    if (dt.Rows[0]["WalletRuleCode"] != null && dt.Rows[0]["WalletRuleCode"].ToString() != "")
                    {
                        model.WalletRuleCode = dt.Rows[0]["WalletRuleCode"].ToString();
                    }
                    if (dt.Rows[0]["Description"] != null && dt.Rows[0]["Description"].ToString() != "")
                    {
                        model.Description = dt.Rows[0]["Description"].ToString();
                    }
                    if (dt.Rows[0]["CreatedOn"] != null && dt.Rows[0]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[0]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[0]["CreatedBy"] != null && dt.Rows[0]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[0]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[0]["UpdatedOn"] != null && dt.Rows[0]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[0]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[0]["UpdatedBy"] != null && dt.Rows[0]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[0]["UpdatedBy"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

