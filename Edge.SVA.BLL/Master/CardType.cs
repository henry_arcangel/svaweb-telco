﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 卡类型表
	/// </summary>
	public partial class CardType
	{
		private readonly ICardType dal=DataAccess.CreateCardType();
		public CardType()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardTypeCode,int CardTypeID)
		{
			return dal.Exists(CardTypeCode,CardTypeID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CardType model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardType model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardTypeID)
		{
			
			return dal.Delete(CardTypeID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardTypeCode,int CardTypeID)
		{
			
			return dal.Delete(CardTypeCode,CardTypeID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CardTypeIDlist )
		{
			return dal.DeleteList(CardTypeIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardType GetModel(int CardTypeID)
		{
			
			return dal.GetModel(CardTypeID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CardType GetModelByCache(int CardTypeID)
		{
			
			string CacheKey = "CardTypeModel-" + CardTypeID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CardTypeID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CardType)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardType> GetModelList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere);
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.CardType> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.CardType> modelList = new List<Edge.SVA.Model.CardType>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.CardType model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.CardType();
                    if (dt.Rows[n]["CardTypeID"] != null && dt.Rows[n]["CardTypeID"].ToString() != "")
                    {
                        model.CardTypeID = int.Parse(dt.Rows[n]["CardTypeID"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeCode"] != null && dt.Rows[n]["CardTypeCode"].ToString() != "")
                    {
                        model.CardTypeCode = dt.Rows[n]["CardTypeCode"].ToString();
                    }
                    if (dt.Rows[n]["CardTypeName1"] != null && dt.Rows[n]["CardTypeName1"].ToString() != "")
                    {
                        model.CardTypeName1 = dt.Rows[n]["CardTypeName1"].ToString();
                    }
                    if (dt.Rows[n]["CardTypeName2"] != null && dt.Rows[n]["CardTypeName2"].ToString() != "")
                    {
                        model.CardTypeName2 = dt.Rows[n]["CardTypeName2"].ToString();
                    }
                    if (dt.Rows[n]["CardTypeName3"] != null && dt.Rows[n]["CardTypeName3"].ToString() != "")
                    {
                        model.CardTypeName3 = dt.Rows[n]["CardTypeName3"].ToString();
                    }
                    if (dt.Rows[n]["BrandID"] != null && dt.Rows[n]["BrandID"].ToString() != "")
                    {
                        model.BrandID = int.Parse(dt.Rows[n]["BrandID"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeNotes"] != null && dt.Rows[n]["CardTypeNotes"].ToString() != "")
                    {
                        model.CardTypeNotes = dt.Rows[n]["CardTypeNotes"].ToString();
                    }
                    if (dt.Rows[n]["CardNumMask"] != null && dt.Rows[n]["CardNumMask"].ToString() != "")
                    {
                        model.CardNumMask = dt.Rows[n]["CardNumMask"].ToString();
                    }
                    if (dt.Rows[n]["CardNumPattern"] != null && dt.Rows[n]["CardNumPattern"].ToString() != "")
                    {
                        model.CardNumPattern = dt.Rows[n]["CardNumPattern"].ToString();
                    }
                    if (dt.Rows[n]["CardMustHasOwner"] != null && dt.Rows[n]["CardMustHasOwner"].ToString() != "")
                    {
                        model.CardMustHasOwner = int.Parse(dt.Rows[n]["CardMustHasOwner"].ToString());
                    }
                    if (dt.Rows[n]["CardVerifyMethod"] != null && dt.Rows[n]["CardVerifyMethod"].ToString() != "")
                    {
                        model.CardVerifyMethod = int.Parse(dt.Rows[n]["CardVerifyMethod"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeStartDate"] != null && dt.Rows[n]["CardTypeStartDate"].ToString() != "")
                    {
                        model.CardTypeStartDate = DateTime.Parse(dt.Rows[n]["CardTypeStartDate"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeEndDate"] != null && dt.Rows[n]["CardTypeEndDate"].ToString() != "")
                    {
                        model.CardTypeEndDate = DateTime.Parse(dt.Rows[n]["CardTypeEndDate"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeNatureID"] != null && dt.Rows[n]["CardTypeNatureID"].ToString() != "")
                    {
                        model.CardTypeNatureID = int.Parse(dt.Rows[n]["CardTypeNatureID"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
                    }
                    if (dt.Rows[n]["CardExtendCode"] != null && dt.Rows[n]["CardExtendCode"].ToString() != "")
                    {
                        model.CardExtendCode = dt.Rows[n]["CardExtendCode"].ToString();
                    }
                    if (dt.Rows[n]["CardCheckdigit"] != null && dt.Rows[n]["CardCheckdigit"].ToString() != "")
                    {
                        model.CardCheckdigit = int.Parse(dt.Rows[n]["CardCheckdigit"].ToString());
                    }
                    if (dt.Rows[n]["CheckDigitModeID"] != null && dt.Rows[n]["CheckDigitModeID"].ToString() != "")
                    {
                        model.CheckDigitModeID = int.Parse(dt.Rows[n]["CheckDigitModeID"].ToString());
                    }
                    if (dt.Rows[n]["CashExpiredate"] != null && dt.Rows[n]["CashExpiredate"].ToString() != "")
                    {
                        model.CashExpiredate = int.Parse(dt.Rows[n]["CashExpiredate"].ToString());
                    }
                    if (dt.Rows[n]["PointExpiredate"] != null && dt.Rows[n]["PointExpiredate"].ToString() != "")
                    {
                        model.PointExpiredate = int.Parse(dt.Rows[n]["PointExpiredate"].ToString());
                    }
                    if (dt.Rows[n]["CurrencyID"] != null && dt.Rows[n]["CurrencyID"].ToString() != "")
                    {
                        model.CurrencyID = int.Parse(dt.Rows[n]["CurrencyID"].ToString());
                    }
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
                    if (dt.Rows[n]["PasswordRuleID"] != null && dt.Rows[n]["PasswordRuleID"].ToString() != "")
                    {
                        model.PasswordRuleID = int.Parse(dt.Rows[n]["PasswordRuleID"].ToString());
                    }
                    if (dt.Rows[n]["IsPhysicalCard"] != null && dt.Rows[n]["IsPhysicalCard"].ToString() != "")
                    {
                        model.IsPhysicalCard = int.Parse(dt.Rows[n]["IsPhysicalCard"].ToString());
                    }
                    if (dt.Rows[n]["IsImportUIDNumber"] != null && dt.Rows[n]["IsImportUIDNumber"].ToString() != "")
                    {
                        model.IsImportUIDNumber = int.Parse(dt.Rows[n]["IsImportUIDNumber"].ToString());
                    }
                    if (dt.Rows[n]["IsConsecutiveUID"] != null && dt.Rows[n]["IsConsecutiveUID"].ToString() != "")
                    {
                        model.IsConsecutiveUID = int.Parse(dt.Rows[n]["IsConsecutiveUID"].ToString());
                    }
                    if (dt.Rows[n]["UIDToCardNumber"] != null && dt.Rows[n]["UIDToCardNumber"].ToString() != "")
                    {
                        model.UIDToCardNumber = int.Parse(dt.Rows[n]["UIDToCardNumber"].ToString());
                    }
                    if (dt.Rows[n]["UIDCheckDigit"] != null && dt.Rows[n]["UIDCheckDigit"].ToString() != "")
                    {
                        model.UIDCheckDigit = int.Parse(dt.Rows[n]["UIDCheckDigit"].ToString());
                    }
                    if (dt.Rows[n]["CardNumberToUID"] != null && dt.Rows[n]["CardNumberToUID"].ToString() != "")
                    {
                        model.CardNumberToUID = int.Parse(dt.Rows[n]["CardNumberToUID"].ToString());
                    }
                    if (dt.Rows[0]["IsDumpCard"] != null && dt.Rows[0]["IsDumpCard"].ToString() != "")
                    {
                        model.IsDumpCard = int.Parse(dt.Rows[0]["IsDumpCard"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

