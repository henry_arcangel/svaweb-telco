﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 卡等级表
	/// </summary>
	public partial class CardGrade
	{
		private readonly ICardGrade dal=DataAccess.CreateCardGrade();
		public CardGrade()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardGradeCode,int CardGradeID)
		{
			return dal.Exists(CardGradeCode,CardGradeID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CardGrade model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardGrade model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardGradeID)
		{
			
			return dal.Delete(CardGradeID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardGradeCode,int CardGradeID)
		{
			
			return dal.Delete(CardGradeCode,CardGradeID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CardGradeIDlist )
		{
			return dal.DeleteList(CardGradeIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardGrade GetModel(int CardGradeID)
		{
			
			return dal.GetModel(CardGradeID);
		}
 
		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CardGrade GetModelByCache(int CardGradeID)
		{
			
			string CacheKey = "CardGradeModel-" + CardGradeID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CardGradeID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CardGrade)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(strWhere);
		}

        /// <summary>
        /// added By jay
        /// </summary>
        public DataSet GetListView(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetListView(strWhere);

        }
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardGrade> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardGrade> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CardGrade> modelList = new List<Edge.SVA.Model.CardGrade>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CardGrade model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetCount(strWhere);
        }


		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

