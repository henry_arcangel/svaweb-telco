﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 创建Member记录
	/// </summary>
	public partial class Ord_CreateMember_D
	{
		private readonly IOrd_CreateMember_D dal=DataAccess.CreateOrd_CreateMember_D();
		public Ord_CreateMember_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			return dal.Exists(KeyID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Ord_CreateMember_D model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CreateMember_D model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			return dal.Delete(KeyID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			return dal.DeleteList(KeyIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CreateMember_D GetModel(int KeyID)
		{
			
			return dal.GetModel(KeyID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Ord_CreateMember_D GetModelByCache(int KeyID)
		{
			
			string CacheKey = "Ord_CreateMember_DModel-" + KeyID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(KeyID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Ord_CreateMember_D)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CreateMember_D> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CreateMember_D> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Ord_CreateMember_D> modelList = new List<Edge.SVA.Model.Ord_CreateMember_D>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Ord_CreateMember_D model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Ord_CreateMember_D();
					if(dt.Rows[n]["KeyID"]!=null && dt.Rows[n]["KeyID"].ToString()!="")
					{
						model.KeyID=int.Parse(dt.Rows[n]["KeyID"].ToString());
					}
					if(dt.Rows[n]["CreateMemberNumber"]!=null && dt.Rows[n]["CreateMemberNumber"].ToString()!="")
					{
					model.CreateMemberNumber=dt.Rows[n]["CreateMemberNumber"].ToString();
					}
					if(dt.Rows[n]["CountryCode"]!=null && dt.Rows[n]["CountryCode"].ToString()!="")
					{
					model.CountryCode=dt.Rows[n]["CountryCode"].ToString();
					}
					if(dt.Rows[n]["MobileNumber"]!=null && dt.Rows[n]["MobileNumber"].ToString()!="")
					{
					model.MobileNumber=dt.Rows[n]["MobileNumber"].ToString();
					}
					if(dt.Rows[n]["EngFamilyName"]!=null && dt.Rows[n]["EngFamilyName"].ToString()!="")
					{
					model.EngFamilyName=dt.Rows[n]["EngFamilyName"].ToString();
					}
					if(dt.Rows[n]["EngGivenName"]!=null && dt.Rows[n]["EngGivenName"].ToString()!="")
					{
					model.EngGivenName=dt.Rows[n]["EngGivenName"].ToString();
					}
					if(dt.Rows[n]["ChiFamilyName"]!=null && dt.Rows[n]["ChiFamilyName"].ToString()!="")
					{
					model.ChiFamilyName=dt.Rows[n]["ChiFamilyName"].ToString();
					}
					if(dt.Rows[n]["ChiGivenName"]!=null && dt.Rows[n]["ChiGivenName"].ToString()!="")
					{
					model.ChiGivenName=dt.Rows[n]["ChiGivenName"].ToString();
					}
					if(dt.Rows[n]["Birthday"]!=null && dt.Rows[n]["Birthday"].ToString()!="")
					{
						model.Birthday=DateTime.Parse(dt.Rows[n]["Birthday"].ToString());
					}
					if(dt.Rows[n]["Gender"]!=null && dt.Rows[n]["Gender"].ToString()!="")
					{
						model.Gender=int.Parse(dt.Rows[n]["Gender"].ToString());
					}
					if(dt.Rows[n]["HomeAddress"]!=null && dt.Rows[n]["HomeAddress"].ToString()!="")
					{
					model.HomeAddress=dt.Rows[n]["HomeAddress"].ToString();
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["Facebook"]!=null && dt.Rows[n]["Facebook"].ToString()!="")
					{
					model.Facebook=dt.Rows[n]["Facebook"].ToString();
					}
					if(dt.Rows[n]["QQ"]!=null && dt.Rows[n]["QQ"].ToString()!="")
					{
					model.QQ=dt.Rows[n]["QQ"].ToString();
					}
					if(dt.Rows[n]["MSN"]!=null && dt.Rows[n]["MSN"].ToString()!="")
					{
					model.MSN=dt.Rows[n]["MSN"].ToString();
					}
					if(dt.Rows[n]["Weibo"]!=null && dt.Rows[n]["Weibo"].ToString()!="")
					{
					model.Weibo=dt.Rows[n]["Weibo"].ToString();
					}
					if(dt.Rows[n]["OtherContact"]!=null && dt.Rows[n]["OtherContact"].ToString()!="")
					{
					model.OtherContact=dt.Rows[n]["OtherContact"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

