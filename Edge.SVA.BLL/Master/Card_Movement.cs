﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Card2ù×÷μ?
	/// </summary>
	public partial class Card_Movement
	{
		private readonly ICard_Movement dal=DataAccess.CreateCard_Movement();
		public Card_Movement()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			return dal.Exists(KeyID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Card_Movement model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Card_Movement model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			return dal.Delete(KeyID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			return dal.DeleteList(KeyIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Card_Movement GetModel(int KeyID)
		{
			
			return dal.GetModel(KeyID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Card_Movement GetModelByCache(int KeyID)
		{
			
			string CacheKey = "Card_MovementModel-" + KeyID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(KeyID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Card_Movement)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Card_Movement> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Card_Movement> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Card_Movement> modelList = new List<Edge.SVA.Model.Card_Movement>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Card_Movement model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Card_Movement();
					if(dt.Rows[n]["KeyID"]!=null && dt.Rows[n]["KeyID"].ToString()!="")
					{
						model.KeyID=int.Parse(dt.Rows[n]["KeyID"].ToString());
					}
					if(dt.Rows[n]["OprID"]!=null && dt.Rows[n]["OprID"].ToString()!="")
					{
						model.OprID=int.Parse(dt.Rows[n]["OprID"].ToString());
					}
					if(dt.Rows[n]["CardNumber"]!=null && dt.Rows[n]["CardNumber"].ToString()!="")
					{
					model.CardNumber=dt.Rows[n]["CardNumber"].ToString();
					}
					if(dt.Rows[n]["RefKeyID"]!=null && dt.Rows[n]["RefKeyID"].ToString()!="")
					{
						model.RefKeyID=int.Parse(dt.Rows[n]["RefKeyID"].ToString());
					}
					if(dt.Rows[n]["RefReceiveKeyID"]!=null && dt.Rows[n]["RefReceiveKeyID"].ToString()!="")
					{
						model.RefReceiveKeyID=int.Parse(dt.Rows[n]["RefReceiveKeyID"].ToString());
					}
					if(dt.Rows[n]["RefTxnNo"]!=null && dt.Rows[n]["RefTxnNo"].ToString()!="")
					{
					model.RefTxnNo=dt.Rows[n]["RefTxnNo"].ToString();
					}
					if(dt.Rows[n]["OpenBal"]!=null && dt.Rows[n]["OpenBal"].ToString()!="")
					{
						model.OpenBal=decimal.Parse(dt.Rows[n]["OpenBal"].ToString());
					}
					if(dt.Rows[n]["Amount"]!=null && dt.Rows[n]["Amount"].ToString()!="")
					{
						model.Amount=decimal.Parse(dt.Rows[n]["Amount"].ToString());
					}
					if(dt.Rows[n]["CloseBal"]!=null && dt.Rows[n]["CloseBal"].ToString()!="")
					{
						model.CloseBal=decimal.Parse(dt.Rows[n]["CloseBal"].ToString());
					}
					if(dt.Rows[n]["Points"]!=null && dt.Rows[n]["Points"].ToString()!="")
					{
						model.Points=int.Parse(dt.Rows[n]["Points"].ToString());
					}
					if(dt.Rows[n]["BusDate"]!=null && dt.Rows[n]["BusDate"].ToString()!="")
					{
						model.BusDate=DateTime.Parse(dt.Rows[n]["BusDate"].ToString());
					}
					if(dt.Rows[n]["Txndate"]!=null && dt.Rows[n]["Txndate"].ToString()!="")
					{
						model.Txndate=DateTime.Parse(dt.Rows[n]["Txndate"].ToString());
					}
					if(dt.Rows[n]["OrgExpiryDate"]!=null && dt.Rows[n]["OrgExpiryDate"].ToString()!="")
					{
						model.OrgExpiryDate=DateTime.Parse(dt.Rows[n]["OrgExpiryDate"].ToString());
					}
					if(dt.Rows[n]["NewExpiryDate"]!=null && dt.Rows[n]["NewExpiryDate"].ToString()!="")
					{
						model.NewExpiryDate=DateTime.Parse(dt.Rows[n]["NewExpiryDate"].ToString());
					}
					if(dt.Rows[n]["OrgStatus"]!=null && dt.Rows[n]["OrgStatus"].ToString()!="")
					{
						model.OrgStatus=int.Parse(dt.Rows[n]["OrgStatus"].ToString());
					}
					if(dt.Rows[n]["NewStatus"]!=null && dt.Rows[n]["NewStatus"].ToString()!="")
					{
						model.NewStatus=int.Parse(dt.Rows[n]["NewStatus"].ToString());
					}
					if(dt.Rows[n]["CardCashDetailID"]!=null && dt.Rows[n]["CardCashDetailID"].ToString()!="")
					{
						model.CardCashDetailID=int.Parse(dt.Rows[n]["CardCashDetailID"].ToString());
					}
					if(dt.Rows[n]["CardPointDetailID"]!=null && dt.Rows[n]["CardPointDetailID"].ToString()!="")
					{
						model.CardPointDetailID=int.Parse(dt.Rows[n]["CardPointDetailID"].ToString());
					}
					if(dt.Rows[n]["TenderID"]!=null && dt.Rows[n]["TenderID"].ToString()!="")
					{
						model.TenderID=int.Parse(dt.Rows[n]["TenderID"].ToString());
					}
					if(dt.Rows[n]["Additional"]!=null && dt.Rows[n]["Additional"].ToString()!="")
					{
					model.Additional=dt.Rows[n]["Additional"].ToString();
					}
					if(dt.Rows[n]["Remark"]!=null && dt.Rows[n]["Remark"].ToString()!="")
					{
					model.Remark=dt.Rows[n]["Remark"].ToString();
					}
					if(dt.Rows[n]["ApprovalCode"]!=null && dt.Rows[n]["ApprovalCode"].ToString()!="")
					{
					model.ApprovalCode=dt.Rows[n]["ApprovalCode"].ToString();
					}
					if(dt.Rows[n]["SecurityCode"]!=null && dt.Rows[n]["SecurityCode"].ToString()!="")
					{
					model.SecurityCode=dt.Rows[n]["SecurityCode"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
					model.CreatedBy=dt.Rows[n]["CreatedBy"].ToString();
					}
					if(dt.Rows[n]["StoreID"]!=null && dt.Rows[n]["StoreID"].ToString()!="")
					{
						model.StoreID=int.Parse(dt.Rows[n]["StoreID"].ToString());
					}
					if(dt.Rows[n]["ServerCode"]!=null && dt.Rows[n]["ServerCode"].ToString()!="")
					{
					model.ServerCode=dt.Rows[n]["ServerCode"].ToString();
					}
					if(dt.Rows[n]["RegisterCode"]!=null && dt.Rows[n]["RegisterCode"].ToString()!="")
					{
					model.RegisterCode=dt.Rows[n]["RegisterCode"].ToString();
					}
					if(dt.Rows[n]["OpenPoint"]!=null && dt.Rows[n]["OpenPoint"].ToString()!="")
					{
						model.OpenPoint=int.Parse(dt.Rows[n]["OpenPoint"].ToString());
					}
					if(dt.Rows[n]["ClosePoint"]!=null && dt.Rows[n]["ClosePoint"].ToString()!="")
					{
						model.ClosePoint=int.Parse(dt.Rows[n]["ClosePoint"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

