﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠劵的店铺条件
	/// </summary>
	public partial class CouponTypeStoreCondition
	{
		private readonly ICouponTypeStoreCondition dal=DataAccess.CreateCouponTypeStoreCondition();
		public CouponTypeStoreCondition()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CouponTypeStoreConditionID)
		{
			return dal.Exists(CouponTypeStoreConditionID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CouponTypeStoreCondition model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponTypeStoreCondition model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CouponTypeStoreConditionID)
		{
			
			return dal.Delete(CouponTypeStoreConditionID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CouponTypeStoreConditionIDlist )
		{
			return dal.DeleteList(CouponTypeStoreConditionIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponTypeStoreCondition GetModel(int CouponTypeStoreConditionID)
		{
			
			return dal.GetModel(CouponTypeStoreConditionID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CouponTypeStoreCondition GetModelByCache(int CouponTypeStoreConditionID)
		{
			
			string CacheKey = "CouponTypeStoreConditionModel-" + CouponTypeStoreConditionID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CouponTypeStoreConditionID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CouponTypeStoreCondition)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponTypeStoreCondition> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponTypeStoreCondition> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CouponTypeStoreCondition> modelList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CouponTypeStoreCondition model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CouponTypeStoreCondition();
					if(dt.Rows[n]["CouponTypeStoreConditionID"]!=null && dt.Rows[n]["CouponTypeStoreConditionID"].ToString()!="")
					{
						model.CouponTypeStoreConditionID=int.Parse(dt.Rows[n]["CouponTypeStoreConditionID"].ToString());
					}
					if(dt.Rows[n]["CouponTypeID"]!=null && dt.Rows[n]["CouponTypeID"].ToString()!="")
					{
						model.CouponTypeID=int.Parse(dt.Rows[n]["CouponTypeID"].ToString());
					}
					if(dt.Rows[n]["StoreConditionType"]!=null && dt.Rows[n]["StoreConditionType"].ToString()!="")
					{
						model.StoreConditionType=int.Parse(dt.Rows[n]["StoreConditionType"].ToString());
					}
					if(dt.Rows[n]["ConditionType"]!=null && dt.Rows[n]["ConditionType"].ToString()!="")
					{
						model.ConditionType=int.Parse(dt.Rows[n]["ConditionType"].ToString());
					}
					if(dt.Rows[n]["ConditionID"]!=null && dt.Rows[n]["ConditionID"].ToString()!="")
					{
						model.ConditionID=int.Parse(dt.Rows[n]["ConditionID"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }


		#endregion  Method
	}
}

