﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 销售单主表（字段暂定）
	///表中会员部分
	/// </summary>
	public partial class Sales_H
	{
		private readonly ISales_H dal=DataAccess.CreateSales_H();
		public Sales_H()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string TxnNo)
		{
			return dal.Exists(TxnNo);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Sales_H model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sales_H model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TxnNo)
		{
			
			return dal.Delete(TxnNo);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string TxnNolist )
		{
			return dal.DeleteList(TxnNolist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sales_H GetModel(string TxnNo)
		{
			
			return dal.GetModel(TxnNo);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Sales_H GetModelByCache(string TxnNo)
		{
			
			string CacheKey = "Sales_HModel-" + TxnNo;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(TxnNo);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Sales_H)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sales_H> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sales_H> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Sales_H> modelList = new List<Edge.SVA.Model.Sales_H>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Sales_H model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Sales_H();
					if(dt.Rows[n]["TxnNo"]!=null && dt.Rows[n]["TxnNo"].ToString()!="")
					{
					model.TxnNo=dt.Rows[n]["TxnNo"].ToString();
					}
					if(dt.Rows[n]["StoreCode"]!=null && dt.Rows[n]["StoreCode"].ToString()!="")
					{
					model.StoreCode=dt.Rows[n]["StoreCode"].ToString();
					}
					if(dt.Rows[n]["RegisterCode"]!=null && dt.Rows[n]["RegisterCode"].ToString()!="")
					{
					model.RegisterCode=dt.Rows[n]["RegisterCode"].ToString();
					}
					if(dt.Rows[n]["ServerCode"]!=null && dt.Rows[n]["ServerCode"].ToString()!="")
					{
					model.ServerCode=dt.Rows[n]["ServerCode"].ToString();
					}
					if(dt.Rows[n]["BusDate"]!=null && dt.Rows[n]["BusDate"].ToString()!="")
					{
						model.BusDate=DateTime.Parse(dt.Rows[n]["BusDate"].ToString());
					}
					if(dt.Rows[n]["TxnDate"]!=null && dt.Rows[n]["TxnDate"].ToString()!="")
					{
						model.TxnDate=DateTime.Parse(dt.Rows[n]["TxnDate"].ToString());
					}
					if(dt.Rows[n]["SalesType"]!=null && dt.Rows[n]["SalesType"].ToString()!="")
					{
						model.SalesType=int.Parse(dt.Rows[n]["SalesType"].ToString());
					}
					if(dt.Rows[n]["CashierID"]!=null && dt.Rows[n]["CashierID"].ToString()!="")
					{
					model.CashierID=dt.Rows[n]["CashierID"].ToString();
					}
					if(dt.Rows[n]["SalesManID"]!=null && dt.Rows[n]["SalesManID"].ToString()!="")
					{
					model.SalesManID=dt.Rows[n]["SalesManID"].ToString();
					}
					if(dt.Rows[n]["Channel"]!=null && dt.Rows[n]["Channel"].ToString()!="")
					{
					model.Channel=dt.Rows[n]["Channel"].ToString();
					}
					if(dt.Rows[n]["TotalAmount"]!=null && dt.Rows[n]["TotalAmount"].ToString()!="")
					{
						model.TotalAmount=decimal.Parse(dt.Rows[n]["TotalAmount"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["MemberID"]!=null && dt.Rows[n]["MemberID"].ToString()!="")
					{
						model.MemberID=int.Parse(dt.Rows[n]["MemberID"].ToString());
					}
					if(dt.Rows[n]["MemberName"]!=null && dt.Rows[n]["MemberName"].ToString()!="")
					{
					model.MemberName=dt.Rows[n]["MemberName"].ToString();
					}
					if(dt.Rows[n]["MemberMobilePhone"]!=null && dt.Rows[n]["MemberMobilePhone"].ToString()!="")
					{
					model.MemberMobilePhone=dt.Rows[n]["MemberMobilePhone"].ToString();
					}
					if(dt.Rows[n]["CardNumber"]!=null && dt.Rows[n]["CardNumber"].ToString()!="")
					{
					model.CardNumber=dt.Rows[n]["CardNumber"].ToString();
					}
					if(dt.Rows[n]["MemberAddress"]!=null && dt.Rows[n]["MemberAddress"].ToString()!="")
					{
					model.MemberAddress=dt.Rows[n]["MemberAddress"].ToString();
					}
					if(dt.Rows[n]["DeliverBy"]!=null && dt.Rows[n]["DeliverBy"].ToString()!="")
					{
					model.DeliverBy=dt.Rows[n]["DeliverBy"].ToString();
					}
					if(dt.Rows[n]["DeliveryAddress"]!=null && dt.Rows[n]["DeliveryAddress"].ToString()!="")
					{
					model.DeliveryAddress=dt.Rows[n]["DeliveryAddress"].ToString();
					}
					if(dt.Rows[n]["DeliverStartOn"]!=null && dt.Rows[n]["DeliverStartOn"].ToString()!="")
					{
						model.DeliverStartOn=DateTime.Parse(dt.Rows[n]["DeliverStartOn"].ToString());
					}
					if(dt.Rows[n]["DeliverEndOn"]!=null && dt.Rows[n]["DeliverEndOn"].ToString()!="")
					{
						model.DeliverEndOn=DateTime.Parse(dt.Rows[n]["DeliverEndOn"].ToString());
					}
					if(dt.Rows[n]["DeliveryLongitude"]!=null && dt.Rows[n]["DeliveryLongitude"].ToString()!="")
					{
					model.DeliveryLongitude=dt.Rows[n]["DeliveryLongitude"].ToString();
					}
					if(dt.Rows[n]["DeliveryLatitude"]!=null && dt.Rows[n]["DeliveryLatitude"].ToString()!="")
					{
					model.DeliveryLatitude=dt.Rows[n]["DeliveryLatitude"].ToString();
					}
					if(dt.Rows[n]["Contact"]!=null && dt.Rows[n]["Contact"].ToString()!="")
					{
					model.Contact=dt.Rows[n]["Contact"].ToString();
					}
					if(dt.Rows[n]["ContactPhone"]!=null && dt.Rows[n]["ContactPhone"].ToString()!="")
					{
					model.ContactPhone=dt.Rows[n]["ContactPhone"].ToString();
					}
					if(dt.Rows[n]["Approvedby"]!=null && dt.Rows[n]["Approvedby"].ToString()!="")
					{
					model.Approvedby=dt.Rows[n]["Approvedby"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
					model.CreatedBy=dt.Rows[n]["CreatedBy"].ToString();
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
					model.UpdatedBy=dt.Rows[n]["UpdatedBy"].ToString();
					}
					if(dt.Rows[n]["ApprovalCode"]!=null && dt.Rows[n]["ApprovalCode"].ToString()!="")
					{
					model.ApprovalCode=dt.Rows[n]["ApprovalCode"].ToString();
					}
					if(dt.Rows[n]["SalesReceipt"]!=null && dt.Rows[n]["SalesReceipt"].ToString()!="")
					{
					model.SalesReceipt=dt.Rows[n]["SalesReceipt"].ToString();
					}
					if(dt.Rows[n]["SalesReceiptBIN"]!=null && dt.Rows[n]["SalesReceiptBIN"].ToString()!="")
					{
						model.SalesReceiptBIN=(byte[])dt.Rows[n]["SalesReceiptBIN"];
					}
					if(dt.Rows[n]["BrandCode"]!=null && dt.Rows[n]["BrandCode"].ToString()!="")
					{
					model.BrandCode=dt.Rows[n]["BrandCode"].ToString();
					}
					if(dt.Rows[n]["SalesTag"]!=null && dt.Rows[n]["SalesTag"].ToString()!="")
					{
					model.SalesTag=dt.Rows[n]["SalesTag"].ToString();
					}
					if(dt.Rows[n]["Additional"]!=null && dt.Rows[n]["Additional"].ToString()!="")
					{
					model.Additional=dt.Rows[n]["Additional"].ToString();
					}
					if(dt.Rows[n]["LogisticsProviderID"]!=null && dt.Rows[n]["LogisticsProviderID"].ToString()!="")
					{
						model.LogisticsProviderID=int.Parse(dt.Rows[n]["LogisticsProviderID"].ToString());
					}
					if(dt.Rows[n]["PaymentDoneOn"]!=null && dt.Rows[n]["PaymentDoneOn"].ToString()!="")
					{
						model.PaymentDoneOn=DateTime.Parse(dt.Rows[n]["PaymentDoneOn"].ToString());
					}
					if(dt.Rows[n]["DeliveryNumber"]!=null && dt.Rows[n]["DeliveryNumber"].ToString()!="")
					{
					model.DeliveryNumber=dt.Rows[n]["DeliveryNumber"].ToString();
					}
					if(dt.Rows[n]["PickupType"]!=null && dt.Rows[n]["PickupType"].ToString()!="")
					{
						model.PickupType=int.Parse(dt.Rows[n]["PickupType"].ToString());
					}
					if(dt.Rows[n]["PickupStoreCode"]!=null && dt.Rows[n]["PickupStoreCode"].ToString()!="")
					{
					model.PickupStoreCode=dt.Rows[n]["PickupStoreCode"].ToString();
					}
					if(dt.Rows[n]["CODFlag"]!=null && dt.Rows[n]["CODFlag"].ToString()!="")
					{
						model.CODFlag=int.Parse(dt.Rows[n]["CODFlag"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

