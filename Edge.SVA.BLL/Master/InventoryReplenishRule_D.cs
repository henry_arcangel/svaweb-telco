﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// Coupon
    /// </summary>
    public partial class InventoryReplenishRule_D
    {
        private readonly IInventoryReplenishRule_D dal = DataAccess.CreateInventoryReplenishRule_D();
        public InventoryReplenishRule_D()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            return dal.Exists(KeyID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.InventoryReplenishRule_D model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.InventoryReplenishRule_D model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            return dal.Delete(KeyID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            return dal.DeleteList(KeyIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.InventoryReplenishRule_D GetModel(int KeyID)
        {

            return dal.GetModel(KeyID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.InventoryReplenishRule_D GetModelByCache(int KeyID)
        {

            string CacheKey = "InventoryReplenishRule_DModel-" + KeyID;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(KeyID);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.InventoryReplenishRule_D)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.InventoryReplenishRule_D> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.InventoryReplenishRule_D> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.InventoryReplenishRule_D> modelList = new List<Edge.SVA.Model.InventoryReplenishRule_D>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.InventoryReplenishRule_D model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.InventoryReplenishRule_D();
                    if (dt.Rows[n]["KeyID"] != null && dt.Rows[n]["KeyID"].ToString() != "")
                    {
                        model.KeyID = int.Parse(dt.Rows[n]["KeyID"].ToString());
                    }
                    if (dt.Rows[n]["InventoryReplenishCode"] != null && dt.Rows[n]["InventoryReplenishCode"].ToString() != "")
                    {
                        model.InventoryReplenishCode = dt.Rows[n]["InventoryReplenishCode"].ToString();
                    }
                    if (dt.Rows[n]["StoreID"] != null && dt.Rows[n]["StoreID"].ToString() != "")
                    {
                        model.StoreID = int.Parse(dt.Rows[n]["StoreID"].ToString());
                    }
                    if (dt.Rows[n]["OrderTargetID"] != null && dt.Rows[n]["OrderTargetID"].ToString() != "")
                    {
                        model.OrderTargetID = int.Parse(dt.Rows[n]["OrderTargetID"].ToString());
                    }
                    if (dt.Rows[n]["MinStockQty"] != null && dt.Rows[n]["MinStockQty"].ToString() != "")
                    {
                        model.MinStockQty = int.Parse(dt.Rows[n]["MinStockQty"].ToString());
                    }
                    if (dt.Rows[n]["RunningStockQty"] != null && dt.Rows[n]["RunningStockQty"].ToString() != "")
                    {
                        model.RunningStockQty = int.Parse(dt.Rows[n]["RunningStockQty"].ToString());
                    }
                    if (dt.Rows[n]["OrderRoundUpQty"] != null && dt.Rows[n]["OrderRoundUpQty"].ToString() != "")
                    {
                        model.OrderRoundUpQty = int.Parse(dt.Rows[n]["OrderRoundUpQty"].ToString());
                    }
                    if (dt.Rows[n]["Priority"] != null && dt.Rows[n]["Priority"].ToString() != "")
                    {
                        model.Priority = int.Parse(dt.Rows[n]["Priority"].ToString());
                    }
                    if (dt.Rows[n]["ReplenishType"] != null && dt.Rows[n]["ReplenishType"].ToString() != "")
                    {
                        model.ReplenishType = int.Parse(dt.Rows[n]["ReplenishType"].ToString());
                    }
                    if (dt.Rows[n]["MinAmtBalance"] != null && dt.Rows[n]["MinAmtBalance"].ToString() != "")
                    {
                        model.MinAmtBalance = decimal.Parse(dt.Rows[n]["MinAmtBalance"].ToString());
                    }
                    if (dt.Rows[n]["RunningAmtBalance"] != null && dt.Rows[n]["RunningAmtBalance"].ToString() != "")
                    {
                        model.RunningAmtBalance = decimal.Parse(dt.Rows[n]["RunningAmtBalance"].ToString());
                    }
                    if (dt.Rows[n]["MinPointBalance"] != null && dt.Rows[n]["MinPointBalance"].ToString() != "")
                    {
                        model.MinPointBalance = int.Parse(dt.Rows[n]["MinPointBalance"].ToString());
                    }
                    if (dt.Rows[n]["RunningPointBalance"] != null && dt.Rows[n]["RunningPointBalance"].ToString() != "")
                    {
                        model.RunningPointBalance = int.Parse(dt.Rows[n]["RunningPointBalance"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method

        public bool DeleteByInventoryReplenishCode(string inventoryReplenishCode)
        {
            return dal.DeleteByInventoryReplenishCode(inventoryReplenishCode);
        }
    }
}

