﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠劵号码与UID对
	/// </summary>
	public partial class CouponUIDMap
	{
		private readonly ICouponUIDMap dal=DataAccess.CreateCouponUIDMap();
		public CouponUIDMap()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CouponUID)
		{
			return dal.Exists(CouponUID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CouponUIDMap model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponUIDMap model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CouponUID)
		{
			
			return dal.Delete(CouponUID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CouponUIDlist )
		{
			return dal.DeleteList(CouponUIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponUIDMap GetModel(string CouponUID)
		{
			
			return dal.GetModel(CouponUID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CouponUIDMap GetModelByCache(string CouponUID)
		{
			
			string CacheKey = "CouponUIDMapModel-" + CouponUID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CouponUID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CouponUIDMap)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponUIDMap> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponUIDMap> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CouponUIDMap> modelList = new List<Edge.SVA.Model.CouponUIDMap>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CouponUIDMap model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CouponUIDMap();
					if(dt.Rows[n]["CouponUID"]!=null && dt.Rows[n]["CouponUID"].ToString()!="")
					{
					model.CouponUID=dt.Rows[n]["CouponUID"].ToString();
					}
					if(dt.Rows[n]["ImportCouponNumber"]!=null && dt.Rows[n]["ImportCouponNumber"].ToString()!="")
					{
					model.ImportCouponNumber=dt.Rows[n]["ImportCouponNumber"].ToString();
					}
					if(dt.Rows[n]["BatchCouponID"]!=null && dt.Rows[n]["BatchCouponID"].ToString()!="")
					{
						model.BatchCouponID=int.Parse(dt.Rows[n]["BatchCouponID"].ToString());
					}
					if(dt.Rows[n]["CouponTypeID"]!=null && dt.Rows[n]["CouponTypeID"].ToString()!="")
					{
						model.CouponTypeID=int.Parse(dt.Rows[n]["CouponTypeID"].ToString());
					}
					if(dt.Rows[n]["CouponNumber"]!=null && dt.Rows[n]["CouponNumber"].ToString()!="")
					{
					model.CouponNumber=dt.Rows[n]["CouponNumber"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

