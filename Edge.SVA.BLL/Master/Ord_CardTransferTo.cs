﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Ord_CardTransferTo
	/// </summary>
	public partial class Ord_CardTransferTo
	{
		private readonly IOrd_CardTransferTo dal=DataAccess.CreateOrd_CardTransferTo();
		public Ord_CardTransferTo()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardNumber)
		{
			return dal.Exists(CardNumber);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Ord_CardTransferTo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CardTransferTo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardNumber)
		{
			
			return dal.Delete(CardNumber);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CardNumberlist )
		{
			return dal.DeleteList(CardNumberlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardTransferTo GetModel(string CardNumber)
		{
			
			return dal.GetModel(CardNumber);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Ord_CardTransferTo GetModelByCache(string CardNumber)
		{
			
			string CacheKey = "Ord_CardTransferToModel-" + CardNumber;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CardNumber);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Ord_CardTransferTo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardTransferTo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardTransferTo> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Ord_CardTransferTo> modelList = new List<Edge.SVA.Model.Ord_CardTransferTo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Ord_CardTransferTo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Ord_CardTransferTo();
					if(dt.Rows[n]["CardTransferNumber"]!=null && dt.Rows[n]["CardTransferNumber"].ToString()!="")
					{
					model.CardTransferNumber=dt.Rows[n]["CardTransferNumber"].ToString();
					}
					if(dt.Rows[n]["CardNumber"]!=null && dt.Rows[n]["CardNumber"].ToString()!="")
					{
					model.CardNumber=dt.Rows[n]["CardNumber"].ToString();
					}
					if(dt.Rows[n]["CardTypeID"]!=null && dt.Rows[n]["CardTypeID"].ToString()!="")
					{
						model.CardTypeID=int.Parse(dt.Rows[n]["CardTypeID"].ToString());
					}
					if(dt.Rows[n]["CardIssueDate"]!=null && dt.Rows[n]["CardIssueDate"].ToString()!="")
					{
						model.CardIssueDate=DateTime.Parse(dt.Rows[n]["CardIssueDate"].ToString());
					}
					if(dt.Rows[n]["CardExpiryDate"]!=null && dt.Rows[n]["CardExpiryDate"].ToString()!="")
					{
						model.CardExpiryDate=DateTime.Parse(dt.Rows[n]["CardExpiryDate"].ToString());
					}
					if(dt.Rows[n]["CardGradeID"]!=null && dt.Rows[n]["CardGradeID"].ToString()!="")
					{
						model.CardGradeID=int.Parse(dt.Rows[n]["CardGradeID"].ToString());
					}
					if(dt.Rows[n]["MemberID"]!=null && dt.Rows[n]["MemberID"].ToString()!="")
					{
						model.MemberID=int.Parse(dt.Rows[n]["MemberID"].ToString());
					}
					if(dt.Rows[n]["BatchCardID"]!=null && dt.Rows[n]["BatchCardID"].ToString()!="")
					{
						model.BatchCardID=int.Parse(dt.Rows[n]["BatchCardID"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["UsedCount"]!=null && dt.Rows[n]["UsedCount"].ToString()!="")
					{
						model.UsedCount=int.Parse(dt.Rows[n]["UsedCount"].ToString());
					}
					if(dt.Rows[n]["CardPassword"]!=null && dt.Rows[n]["CardPassword"].ToString()!="")
					{
					model.CardPassword=dt.Rows[n]["CardPassword"].ToString();
					}
					if(dt.Rows[n]["TotalPoints"]!=null && dt.Rows[n]["TotalPoints"].ToString()!="")
					{
						model.TotalPoints=int.Parse(dt.Rows[n]["TotalPoints"].ToString());
					}
					if(dt.Rows[n]["TotalAmount"]!=null && dt.Rows[n]["TotalAmount"].ToString()!="")
					{
						model.TotalAmount=decimal.Parse(dt.Rows[n]["TotalAmount"].ToString());
					}
					if(dt.Rows[n]["ParentCardNumber"]!=null && dt.Rows[n]["ParentCardNumber"].ToString()!="")
					{
					model.ParentCardNumber=dt.Rows[n]["ParentCardNumber"].ToString();
					}
					if(dt.Rows[n]["CardForfeitPoints"]!=null && dt.Rows[n]["CardForfeitPoints"].ToString()!="")
					{
						model.CardForfeitPoints=int.Parse(dt.Rows[n]["CardForfeitPoints"].ToString());
					}
					if(dt.Rows[n]["CardForfeitAmount"]!=null && dt.Rows[n]["CardForfeitAmount"].ToString()!="")
					{
						model.CardForfeitAmount=decimal.Parse(dt.Rows[n]["CardForfeitAmount"].ToString());
					}
					if(dt.Rows[n]["ResetPassword"]!=null && dt.Rows[n]["ResetPassword"].ToString()!="")
					{
						model.ResetPassword=int.Parse(dt.Rows[n]["ResetPassword"].ToString());
					}
					if(dt.Rows[n]["CardAmountExpiryDate"]!=null && dt.Rows[n]["CardAmountExpiryDate"].ToString()!="")
					{
						model.CardAmountExpiryDate=DateTime.Parse(dt.Rows[n]["CardAmountExpiryDate"].ToString());
					}
					if(dt.Rows[n]["CardPointExpiryDate"]!=null && dt.Rows[n]["CardPointExpiryDate"].ToString()!="")
					{
						model.CardPointExpiryDate=DateTime.Parse(dt.Rows[n]["CardPointExpiryDate"].ToString());
					}
					if(dt.Rows[n]["CumulativeEarnPoints"]!=null && dt.Rows[n]["CumulativeEarnPoints"].ToString()!="")
					{
						model.CumulativeEarnPoints=int.Parse(dt.Rows[n]["CumulativeEarnPoints"].ToString());
					}
					if(dt.Rows[n]["CumulativeConsumptionAmt"]!=null && dt.Rows[n]["CumulativeConsumptionAmt"].ToString()!="")
					{
						model.CumulativeConsumptionAmt=decimal.Parse(dt.Rows[n]["CumulativeConsumptionAmt"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					if(dt.Rows[n]["PasswordExpiryDate"]!=null && dt.Rows[n]["PasswordExpiryDate"].ToString()!="")
					{
						model.PasswordExpiryDate=DateTime.Parse(dt.Rows[n]["PasswordExpiryDate"].ToString());
					}
					if(dt.Rows[n]["PWDExpiryPromptDays"]!=null && dt.Rows[n]["PWDExpiryPromptDays"].ToString()!="")
					{
						model.PWDExpiryPromptDays=int.Parse(dt.Rows[n]["PWDExpiryPromptDays"].ToString());
					}
					if(dt.Rows[n]["ApprovedCode"]!=null && dt.Rows[n]["ApprovedCode"].ToString()!="")
					{
					model.ApprovedCode=dt.Rows[n]["ApprovedCode"].ToString();
					}
					if(dt.Rows[n]["StockStatus"]!=null && dt.Rows[n]["StockStatus"].ToString()!="")
					{
						model.StockStatus=int.Parse(dt.Rows[n]["StockStatus"].ToString());
					}
					if(dt.Rows[n]["IssueStoreID"]!=null && dt.Rows[n]["IssueStoreID"].ToString()!="")
					{
						model.IssueStoreID=int.Parse(dt.Rows[n]["IssueStoreID"].ToString());
					}
					if(dt.Rows[n]["ActiveStoreID"]!=null && dt.Rows[n]["ActiveStoreID"].ToString()!="")
					{
						model.ActiveStoreID=int.Parse(dt.Rows[n]["ActiveStoreID"].ToString());
					}
					if(dt.Rows[n]["ActiveDate"]!=null && dt.Rows[n]["ActiveDate"].ToString()!="")
					{
						model.ActiveDate=DateTime.Parse(dt.Rows[n]["ActiveDate"].ToString());
					}
					if(dt.Rows[n]["PickupFlag"]!=null && dt.Rows[n]["PickupFlag"].ToString()!="")
					{
						model.PickupFlag=DateTime.Parse(dt.Rows[n]["PickupFlag"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

