﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠劵类型级别
	/// </summary>
	public partial class CouponNature
	{
		private readonly ICouponNature dal=DataAccess.CreateCouponNature();
		public CouponNature()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CouponNatureID)
		{
			return dal.Exists(CouponNatureID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CouponNature model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponNature model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CouponNatureID)
		{
			
			return dal.Delete(CouponNatureID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CouponNatureIDlist )
		{
			return dal.DeleteList(CouponNatureIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponNature GetModel(int CouponNatureID)
		{
			
			return dal.GetModel(CouponNatureID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CouponNature GetModelByCache(int CouponNatureID)
		{
			
			string CacheKey = "CouponNatureModel-" + CouponNatureID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CouponNatureID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CouponNature)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponNature> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponNature> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CouponNature> modelList = new List<Edge.SVA.Model.CouponNature>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CouponNature model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CouponNature();
					if(dt.Rows[n]["CouponNatureID"]!=null && dt.Rows[n]["CouponNatureID"].ToString()!="")
					{
						model.CouponNatureID=int.Parse(dt.Rows[n]["CouponNatureID"].ToString());
					}
					if(dt.Rows[n]["CouponNatureCode"]!=null && dt.Rows[n]["CouponNatureCode"].ToString()!="")
					{
					model.CouponNatureCode=dt.Rows[n]["CouponNatureCode"].ToString();
					}
					if(dt.Rows[n]["CouponNatureName1"]!=null && dt.Rows[n]["CouponNatureName1"].ToString()!="")
					{
					model.CouponNatureName1=dt.Rows[n]["CouponNatureName1"].ToString();
					}
					if(dt.Rows[n]["CouponNatureName2"]!=null && dt.Rows[n]["CouponNatureName2"].ToString()!="")
					{
					model.CouponNatureName2=dt.Rows[n]["CouponNatureName2"].ToString();
					}
					if(dt.Rows[n]["CouponNatureName3"]!=null && dt.Rows[n]["CouponNatureName3"].ToString()!="")
					{
					model.CouponNatureName3=dt.Rows[n]["CouponNatureName3"].ToString();
					}
					if(dt.Rows[n]["BrandID"]!=null && dt.Rows[n]["BrandID"].ToString()!="")
					{
						model.BrandID=int.Parse(dt.Rows[n]["BrandID"].ToString());
					}
                    if (dt.Rows[n]["ParentID"] != null && dt.Rows[n]["ParentID"].ToString() != "")
                    {
                        model.ParentID = int.Parse(dt.Rows[n]["ParentID"].ToString());
                    }
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere);
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere);
            return dal.GetCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
		#endregion  Method
	}
}

