﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 转赠单据表. 子表
	///@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D
	/// </summary>
	public partial class Ord_CardTransfer_D
	{
		private readonly IOrd_CardTransfer_D dal=DataAccess.CreateOrd_CardTransfer_D();
		public Ord_CardTransfer_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			return dal.Exists(KeyID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Ord_CardTransfer_D model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CardTransfer_D model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			return dal.Delete(KeyID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			return dal.DeleteList(KeyIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardTransfer_D GetModel(int KeyID)
		{
			
			return dal.GetModel(KeyID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Ord_CardTransfer_D GetModelByCache(int KeyID)
		{
			
			string CacheKey = "Ord_CardTransfer_DModel-" + KeyID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(KeyID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Ord_CardTransfer_D)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardTransfer_D> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardTransfer_D> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Ord_CardTransfer_D> modelList = new List<Edge.SVA.Model.Ord_CardTransfer_D>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Ord_CardTransfer_D model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Ord_CardTransfer_D();
					if(dt.Rows[n]["KeyID"]!=null && dt.Rows[n]["KeyID"].ToString()!="")
					{
						model.KeyID=int.Parse(dt.Rows[n]["KeyID"].ToString());
					}
					if(dt.Rows[n]["CardTransferNumber"]!=null && dt.Rows[n]["CardTransferNumber"].ToString()!="")
					{
					model.CardTransferNumber=dt.Rows[n]["CardTransferNumber"].ToString();
					}
					if(dt.Rows[n]["SourceCardTypeID"]!=null && dt.Rows[n]["SourceCardTypeID"].ToString()!="")
					{
						model.SourceCardTypeID=int.Parse(dt.Rows[n]["SourceCardTypeID"].ToString());
					}
					if(dt.Rows[n]["SourceCardGradeID"]!=null && dt.Rows[n]["SourceCardGradeID"].ToString()!="")
					{
						model.SourceCardGradeID=int.Parse(dt.Rows[n]["SourceCardGradeID"].ToString());
					}
					if(dt.Rows[n]["SourceCardNumber"]!=null && dt.Rows[n]["SourceCardNumber"].ToString()!="")
					{
					model.SourceCardNumber=dt.Rows[n]["SourceCardNumber"].ToString();
					}
					if(dt.Rows[n]["DestCardTypeID"]!=null && dt.Rows[n]["DestCardTypeID"].ToString()!="")
					{
						model.DestCardTypeID=int.Parse(dt.Rows[n]["DestCardTypeID"].ToString());
					}
					if(dt.Rows[n]["DestCardGradeID"]!=null && dt.Rows[n]["DestCardGradeID"].ToString()!="")
					{
						model.DestCardGradeID=int.Parse(dt.Rows[n]["DestCardGradeID"].ToString());
					}
					if(dt.Rows[n]["DestCardNumber"]!=null && dt.Rows[n]["DestCardNumber"].ToString()!="")
					{
					model.DestCardNumber=dt.Rows[n]["DestCardNumber"].ToString();
					}
					if(dt.Rows[n]["OriginalTxnNo"]!=null && dt.Rows[n]["OriginalTxnNo"].ToString()!="")
					{
					model.OriginalTxnNo=dt.Rows[n]["OriginalTxnNo"].ToString();
					}
					if(dt.Rows[n]["TxnDate"]!=null && dt.Rows[n]["TxnDate"].ToString()!="")
					{
						model.TxnDate=DateTime.Parse(dt.Rows[n]["TxnDate"].ToString());
					}
					if(dt.Rows[n]["StoreCode"]!=null && dt.Rows[n]["StoreCode"].ToString()!="")
					{
					model.StoreCode=dt.Rows[n]["StoreCode"].ToString();
					}
					if(dt.Rows[n]["ServerCode"]!=null && dt.Rows[n]["ServerCode"].ToString()!="")
					{
					model.ServerCode=dt.Rows[n]["ServerCode"].ToString();
					}
					if(dt.Rows[n]["RegisterCode"]!=null && dt.Rows[n]["RegisterCode"].ToString()!="")
					{
					model.RegisterCode=dt.Rows[n]["RegisterCode"].ToString();
					}
					if(dt.Rows[n]["BrandCode"]!=null && dt.Rows[n]["BrandCode"].ToString()!="")
					{
					model.BrandCode=dt.Rows[n]["BrandCode"].ToString();
					}
					if(dt.Rows[n]["ReasonID"]!=null && dt.Rows[n]["ReasonID"].ToString()!="")
					{
						model.ReasonID=int.Parse(dt.Rows[n]["ReasonID"].ToString());
					}
					if(dt.Rows[n]["Note"]!=null && dt.Rows[n]["Note"].ToString()!="")
					{
					model.Note=dt.Rows[n]["Note"].ToString();
					}
					if(dt.Rows[n]["ActAmount"]!=null && dt.Rows[n]["ActAmount"].ToString()!="")
					{
						model.ActAmount=decimal.Parse(dt.Rows[n]["ActAmount"].ToString());
					}
					if(dt.Rows[n]["ActPoints"]!=null && dt.Rows[n]["ActPoints"].ToString()!="")
					{
						model.ActPoints=int.Parse(dt.Rows[n]["ActPoints"].ToString());
					}
					if(dt.Rows[n]["ActCouponNumbers"]!=null && dt.Rows[n]["ActCouponNumbers"].ToString()!="")
					{
					model.ActCouponNumbers=dt.Rows[n]["ActCouponNumbers"].ToString();
					}
					if(dt.Rows[n]["CopyCardFlag"]!=null && dt.Rows[n]["CopyCardFlag"].ToString()!="")
					{
						model.CopyCardFlag=int.Parse(dt.Rows[n]["CopyCardFlag"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

