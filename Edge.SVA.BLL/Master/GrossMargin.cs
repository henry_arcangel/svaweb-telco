﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 公司表
	/// </summary>
	public partial class GrossMargin
	{
        private readonly IGrossMargin dal = DataAccess.CreateGrossMargin();
        public GrossMargin()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(string GrossMarginCode)
		{
            return dal.Exists(GrossMarginCode);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public int Add(Edge.SVA.Model.GrossMargin model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.GrossMargin model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(string GrossMarginCode)
		{

            return dal.Delete(GrossMarginCode);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool DeleteList(string GrossMarginCodelist)
		{
            return dal.DeleteList(GrossMarginCodelist);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.GrossMargin GetModel(string GrossMarginCode)
		{

            return dal.GetModel(GrossMarginCode);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        public Edge.SVA.Model.GrossMargin GetModelByCache(string GrossMarginCode)
		{

            string CacheKey = "GrossMarginModel-" + GrossMarginCode;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
                    objModel = dal.GetModel(GrossMarginCode);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
            return (Edge.SVA.Model.GrossMargin)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.GrossMargin> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.GrossMargin> DataTableToList(DataTable dt)
		{
            List<Edge.SVA.Model.GrossMargin> modelList = new List<Edge.SVA.Model.GrossMargin>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
                Edge.SVA.Model.GrossMargin model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = new Edge.SVA.Model.GrossMargin();

                    if (dt.Rows[n]["GrossMarginCode"] != null && dt.Rows[n]["GrossMarginCode"].ToString() != "")
					{
                        model.GrossMarginCode = dt.Rows[n]["GrossMarginCode"].ToString();
					}
                    if (dt.Rows[n]["Description1"] != null && dt.Rows[n]["Description1"].ToString() != "")
					{
                        model.Description1 = dt.Rows[n]["Description1"].ToString();
					}
                    if (dt.Rows[n]["Description2"] != null && dt.Rows[n]["Description2"].ToString() != "")
                    {
                        model.Description2 = dt.Rows[n]["Description2"].ToString();
                    }
                    if (dt.Rows[n]["Description3"] != null && dt.Rows[n]["Description3"].ToString() != "")
                    {
                        model.Description3 = dt.Rows[n]["Description3"].ToString();
                    }

                    if (dt.Rows[n]["VenderType"] != null && dt.Rows[n]["VenderType"].ToString() != "")
                    {
                        model.VenderType = int.Parse(dt.Rows[n]["VenderType"].ToString());
                    }
                    if (dt.Rows[n]["VenderID"] != null && dt.Rows[n]["VenderID"].ToString() != "")
                    {
                        model.VenderID = int.Parse(dt.Rows[n]["VenderID"].ToString());
                    }
                    if (dt.Rows[n]["BuyerType"] != null && dt.Rows[n]["BuyerType"].ToString() != "")
                    {
                        model.BuyerType = int.Parse(dt.Rows[n]["BuyerType"].ToString());
                    }
                    if (dt.Rows[n]["BuyerID"] != null && dt.Rows[n]["BuyerID"].ToString() != "")
                    {
                        model.BuyerID = int.Parse(dt.Rows[n]["BuyerID"].ToString());
                    }
                    if (dt.Rows[n]["CardTypeID"] != null && dt.Rows[n]["CardTypeID"].ToString() != "")
                    {
                        model.CardTypeID = int.Parse(dt.Rows[n]["CardTypeID"].ToString());
                    }
                    if (dt.Rows[n]["CardGradeID"] != null && dt.Rows[n]["CardGradeID"].ToString() != "")
                    {
                        model.CardGradeID = int.Parse(dt.Rows[n]["CardGradeID"].ToString());
                    }
                    if (dt.Rows[n]["VAT"] != null && dt.Rows[n]["VAT"].ToString() != "")
                    {
                        model.VAT = decimal.Parse(dt.Rows[n]["VAT"].ToString());
                    }
                    if (dt.Rows[n]["Report"] != null && dt.Rows[n]["Report"].ToString() != "")
                    {
                        model.Report = dt.Rows[n]["Report"].ToString();
                    }
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

		#endregion  Method
	}
}

