﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 面向供应商的订货单
    /// </summary>
    public partial class WalletRule_D
    {
        private readonly IWalletRule_D dal = DataAccess.CreateWalletRule_D();
        public WalletRule_D()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            return dal.Exists(KeyID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.WalletRule_D model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.WalletRule_D model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            return dal.Delete(KeyID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            return dal.DeleteList(KeyIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.WalletRule_D GetModel(int KeyID)
        {

            return dal.GetModel(KeyID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.WalletRule_D GetModelByCache(int KeyID)
        {

            string CacheKey = "WalletRule_DModel-" + KeyID;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(KeyID);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.WalletRule_D)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.WalletRule_D> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.WalletRule_D> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.WalletRule_D> modelList = new List<Edge.SVA.Model.WalletRule_D>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.WalletRule_D model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.WalletRule_D();
                    if (dt.Rows[0]["KeyID"] != null && dt.Rows[0]["KeyID"].ToString() != "")
                    {
                        model.KeyID = int.Parse(dt.Rows[0]["KeyID"].ToString());
                    }
                    if (dt.Rows[0]["WalletRuleCode"] != null && dt.Rows[0]["WalletRuleCode"].ToString() != "")
                    {
                        model.WalletRuleCode = dt.Rows[0]["WalletRuleCode"].ToString();
                    }
                    if (dt.Rows[0]["BrandID"] != null && dt.Rows[0]["BrandID"].ToString() != "")
                    {
                        model.BrandID = int.Parse(dt.Rows[0]["BrandID"].ToString());
                    }
                    if (dt.Rows[0]["CardTypeID"] != null && dt.Rows[0]["CardTypeID"].ToString() != "")
                    {
                        model.CardTypeID = int.Parse(dt.Rows[0]["CardTypeID"].ToString());
                    }
                    if (dt.Rows[0]["CardGradeID"] != null && dt.Rows[0]["CardGradeID"].ToString() != "")
                    {
                        model.CardGradeID = int.Parse(dt.Rows[0]["CardGradeID"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

