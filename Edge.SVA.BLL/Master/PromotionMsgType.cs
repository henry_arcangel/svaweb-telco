﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠消息类型表
	/// </summary>
	public partial class PromotionMsgType
	{
		private readonly IPromotionMsgType dal=DataAccess.CreatePromotionMsgType();
		public PromotionMsgType()
		{}

        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.BrandIDsStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " BrandID in " + str;
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and BrandID in " + str;
                    }
                    else
                    {
                        strWhere = strs[0] + " and BrandID in " + str + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }
        //End
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int PromotionMsgTypeID)
		{
			return dal.Exists(PromotionMsgTypeID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.PromotionMsgType model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.PromotionMsgType model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PromotionMsgTypeID)
		{
			
			return dal.Delete(PromotionMsgTypeID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string PromotionMsgTypeIDlist )
		{
			return dal.DeleteList(PromotionMsgTypeIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.PromotionMsgType GetModel(int PromotionMsgTypeID)
		{
			
			return dal.GetModel(PromotionMsgTypeID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.PromotionMsgType GetModelByCache(int PromotionMsgTypeID)
		{
			
			string CacheKey = "PromotionMsgTypeModel-" + PromotionMsgTypeID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(PromotionMsgTypeID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.PromotionMsgType)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.PromotionMsgType> GetModelList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.PromotionMsgType> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.PromotionMsgType> modelList = new List<Edge.SVA.Model.PromotionMsgType>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.PromotionMsgType model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

