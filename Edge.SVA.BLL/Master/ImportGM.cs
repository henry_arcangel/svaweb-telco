﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 实体卡绑定表
	/// </summary>
	public partial class ImportGM
	{
		private readonly IImportGM dal=DataAccess.CreateImportGM();
        public ImportGM()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.ImportGM model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.ImportGM model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SKU)
        {       
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete(SKU);
        }   

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.ImportGM GetModel(string prodcode)
		{
			//该表无主键信息，请自定义主键/条件字段
            //removed by Jay
           // return dal.GetModel(prodcode);
            return dal.GetModelSKU(Convert.ToInt32(prodcode));
		}

        public Edge.SVA.Model.ImportGM GetModelProd(string prodcode)
        {
            //该表无主键信息，请自定义主键/条件字段

            return dal.GetModel(prodcode);

        }

        /// <summary>
        ///Created By Jay
        /// </summary>
        public Edge.SVA.Model.ImportGM GetModelSKU(int UPC)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.GetModelSKU(UPC);
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        public Edge.SVA.Model.ImportGM GetModelByCache()
		{
			//该表无主键信息，请自定义主键/条件字段
            string CacheKey = "ImportGMModel-";
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
                    objModel = dal.GetModel(CacheKey);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
            return (Edge.SVA.Model.ImportGM)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.ImportGM> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.ImportGM> DataTableToList(DataTable dt)
		{
            List<Edge.SVA.Model.ImportGM> modelList = new List<Edge.SVA.Model.ImportGM>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
                Edge.SVA.Model.ImportGM model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = new Edge.SVA.Model.ImportGM();
                    if (dt.Rows[n]["SKU"] != null && dt.Rows[n]["SKU"].ToString() != "")
					{
                        model.SKU = dt.Rows[n]["SKU"].ToString();
					}
                    if (dt.Rows[n]["SKUDesc"] != null && dt.Rows[n]["SKUDesc"].ToString() != "")
					{
                        model.SKUDesc = dt.Rows[n]["SKUDesc"].ToString();
					}
                    if (dt.Rows[n]["SKUUnitAmount"] != null && dt.Rows[n]["SKUUnitAmount"].ToString() != "")
					{
                        model.SKUUnitAmount = decimal.Parse(dt.Rows[n]["SKUUnitAmount"].ToString());
					}
                    if (dt.Rows[n]["UPC"] != null && dt.Rows[n]["UPC"].ToString() != "")
					{
                        model.UPC = dt.Rows[n]["UPC"].ToString();
					}
                    if (dt.Rows[n]["ProdCode"] != null && dt.Rows[n]["ProdCode"].ToString() != "")
					{
                        model.ProdCode = dt.Rows[n]["ProdCode"].ToString();
					}
                    if (dt.Rows[n]["GMValue"] != null && dt.Rows[n]["GMValue"].ToString() != "")
					{
                        model.GMValue = decimal.Parse(dt.Rows[n]["GMValue"].ToString());
					}
                    if (dt.Rows[n]["CardTypeCode"] != null && dt.Rows[n]["CardTypeCode"].ToString() != "")
					{
                        model.CardTypeCode = dt.Rows[n]["CardTypeCode"].ToString();
					}
                    if (dt.Rows[n]["CardGradeCode"] != null && dt.Rows[n]["CardGradeCode"].ToString() != "")
					{
                        model.CardGradeCode = dt.Rows[n]["CardGradeCode"].ToString();
					}
                    if (dt.Rows[n]["NumberMask"] != null && dt.Rows[n]["NumberMask"].ToString() != "")
					{
                        model.NumberMask = dt.Rows[n]["NumberMask"].ToString();
					}
                    if (dt.Rows[n]["NumberPrefix"] != null && dt.Rows[n]["NumberPrefix"].ToString() != "")
					{
                        model.NumberPrefix = dt.Rows[n]["NumberPrefix"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
					{
                        model.CreatedBy = dt.Rows[n]["CreatedBy"].ToString();
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
                        model.UpdatedBy=dt.Rows[n]["UpdatedBy"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }

        public DataSet GetListViews(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetListViews(PageSize, PageIndex, strWhere, filedOrder);

        }
        public DataSet GetListViews(int PageSize, int PageIndex, string strWhere, string filedOrder,int order)
        {
            return dal.GetListViews(PageSize, PageIndex, strWhere, filedOrder, order);


        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

