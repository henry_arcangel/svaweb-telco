﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// Coupon自动补货规则设置表. 
    ///根据设置
    /// </summary>
    public partial class CouponReplenishRule_H
    {
        private readonly ICouponReplenishRule_H dal = DataAccess.CreateCouponReplenishRule_H();
        public CouponReplenishRule_H()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CouponReplenishCode)
        {
            return dal.Exists(CouponReplenishCode);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.CouponReplenishRule_H model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.CouponReplenishRule_H model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CouponReplenishCode)
        {

            return dal.Delete(CouponReplenishCode);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string CouponReplenishCodelist)
        {
            return dal.DeleteList(CouponReplenishCodelist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.CouponReplenishRule_H GetModel(string CouponReplenishCode)
        {

            return dal.GetModel(CouponReplenishCode);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.CouponReplenishRule_H GetModelByCache(string CouponReplenishCode)
        {

            string CacheKey = "CouponReplenishRule_HModel-" + CouponReplenishCode;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(CouponReplenishCode);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.CouponReplenishRule_H)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.CouponReplenishRule_H> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.CouponReplenishRule_H> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.CouponReplenishRule_H> modelList = new List<Edge.SVA.Model.CouponReplenishRule_H>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.CouponReplenishRule_H model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.CouponReplenishRule_H();
                    if (dt.Rows[n]["CouponReplenishCode"] != null && dt.Rows[n]["CouponReplenishCode"].ToString() != "")
                    {
                        model.CouponReplenishCode = dt.Rows[n]["CouponReplenishCode"].ToString();
                    }
                    if (dt.Rows[n]["Description"] != null && dt.Rows[n]["Description"].ToString() != "")
                    {
                        model.Description = dt.Rows[n]["Description"].ToString();
                    }
                    if (dt.Rows[n]["StartDate"] != null && dt.Rows[n]["StartDate"].ToString() != "")
                    {
                        model.StartDate = DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
                    }
                    if (dt.Rows[n]["EndDate"] != null && dt.Rows[n]["EndDate"].ToString() != "")
                    {
                        model.EndDate = DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
                    }
                    if (dt.Rows[n]["BrandID"] != null && dt.Rows[n]["BrandID"].ToString() != "")
                    {
                        model.BrandID = int.Parse(dt.Rows[n]["BrandID"].ToString());
                    }
                    if (dt.Rows[n]["CouponTypeID"] != null && dt.Rows[n]["CouponTypeID"].ToString() != "")
                    {
                        model.CouponTypeID = int.Parse(dt.Rows[n]["CouponTypeID"].ToString());
                    }
                    if (dt.Rows[n]["StoreTypeID"] != null && dt.Rows[n]["StoreTypeID"].ToString() != "")
                    {
                        model.StoreTypeID = int.Parse(dt.Rows[n]["StoreTypeID"].ToString());
                    }
                    if (dt.Rows[n]["AutoCreateOrder"] != null && dt.Rows[n]["AutoCreateOrder"].ToString() != "")
                    {
                        model.AutoCreateOrder = int.Parse(dt.Rows[n]["AutoCreateOrder"].ToString());
                    }
                    if (dt.Rows[n]["AutoApproveOrder"] != null && dt.Rows[n]["AutoApproveOrder"].ToString() != "")
                    {
                        model.AutoApproveOrder = int.Parse(dt.Rows[n]["AutoApproveOrder"].ToString());
                    }
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
                    if (dt.Rows[n]["DayFlagID"] != null && dt.Rows[n]["DayFlagID"].ToString() != "")
                    {
                        model.DayFlagID = int.Parse(dt.Rows[n]["DayFlagID"].ToString());
                    }
                    if (dt.Rows[n]["WeekFlagID"] != null && dt.Rows[n]["WeekFlagID"].ToString() != "")
                    {
                        model.WeekFlagID = int.Parse(dt.Rows[n]["WeekFlagID"].ToString());
                    }
                    if (dt.Rows[n]["MonthFlagID"] != null && dt.Rows[n]["MonthFlagID"].ToString() != "")
                    {
                        model.MonthFlagID = int.Parse(dt.Rows[n]["MonthFlagID"].ToString());
                    }
                    if (dt.Rows[n]["ActiveTime"] != null && dt.Rows[n]["ActiveTime"].ToString() != "")
                    {
                        model.ActiveTime = DateTime.Parse(dt.Rows[n]["ActiveTime"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

