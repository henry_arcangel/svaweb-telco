﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 店铺类型表。固定数据：
	///1：member card。 2：staff card。 3：store value card。 4：cash card（含有初始金额
	/// </summary>
	public partial class CardTypeNature
	{
		private readonly ICardTypeNature dal=DataAccess.CreateCardTypeNature();
		public CardTypeNature()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CardTypeNatureID)
		{
			return dal.Exists(CardTypeNatureID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CardTypeNature model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardTypeNature model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardTypeNatureID)
		{
			
			return dal.Delete(CardTypeNatureID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CardTypeNatureIDlist )
		{
			return dal.DeleteList(CardTypeNatureIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardTypeNature GetModel(int CardTypeNatureID)
		{
			
			return dal.GetModel(CardTypeNatureID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CardTypeNature GetModelByCache(int CardTypeNatureID)
		{
			
			string CacheKey = "CardTypeNatureModel-" + CardTypeNatureID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CardTypeNatureID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CardTypeNature)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardTypeNature> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardTypeNature> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CardTypeNature> modelList = new List<Edge.SVA.Model.CardTypeNature>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CardTypeNature model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CardTypeNature();
					if(dt.Rows[n]["CardTypeNatureID"]!=null && dt.Rows[n]["CardTypeNatureID"].ToString()!="")
					{
						model.CardTypeNatureID=int.Parse(dt.Rows[n]["CardTypeNatureID"].ToString());
					}
					if(dt.Rows[n]["CardTypeNatureName1"]!=null && dt.Rows[n]["CardTypeNatureName1"].ToString()!="")
					{
					model.CardTypeNatureName1=dt.Rows[n]["CardTypeNatureName1"].ToString();
					}
					if(dt.Rows[n]["CardTypeNatureName2"]!=null && dt.Rows[n]["CardTypeNatureName2"].ToString()!="")
					{
					model.CardTypeNatureName2=dt.Rows[n]["CardTypeNatureName2"].ToString();
					}
					if(dt.Rows[n]["CardTypeNatureName3"]!=null && dt.Rows[n]["CardTypeNatureName3"].ToString()!="")
					{
					model.CardTypeNatureName3=dt.Rows[n]["CardTypeNatureName3"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

