﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class CouponTypeExchangeBinding
    {
        public DataSet FetchFields(string fields, string strWhere, params System.Data.SqlClient.SqlParameter[] parameters)
        {
            return dal.FetchFields(fields, strWhere, parameters);
        }
    }
}
