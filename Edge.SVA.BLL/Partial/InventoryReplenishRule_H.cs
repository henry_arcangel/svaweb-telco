﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class InventoryReplenishRule_H : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateInventoryReplenishRule_H() as IDAL.IBaseDAL; }
        }
    }
}
