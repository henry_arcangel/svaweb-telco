﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
     public partial class  Ord_CardDelivery_D:BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardDelivery_D() as IDAL.IBaseDAL; }
        }

        public Dictionary<int, int> GetCardGradeIndex(string CardDeliveryNumber)
        {
            return dal.GetCardGradeIndex(CardDeliveryNumber); 
        }
    }
}
