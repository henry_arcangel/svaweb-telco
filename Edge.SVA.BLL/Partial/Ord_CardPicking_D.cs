﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardPicking_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardPicking_D() as IDAL.IBaseDAL; }
        }

        public DataSet GetListGroupByCardGrade(string strWhere)
        {
            return dal.GetListGroupByCardGrade(strWhere);
        }

        public Dictionary<int, int> GetCardGradeIndex(string CardPickingNumber)
        {
            return dal.GetCardGradeIndex(CardPickingNumber);
        }

        public bool DeleteByOrder(string CardPickingNumber)
        {
            return dal.DeleteByOrder(CardPickingNumber);
        }
    
    }
}
