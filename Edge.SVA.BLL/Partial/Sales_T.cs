﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL
{
   public partial class Sales_T
    {
        /// <summary>
        /// 删除一条数据
        /// </summary>
       public bool Delete(string TxnNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Sales_T ");
            strSql.Append(" where TxnNo=@TxnNo");
            SqlParameter[] parameters = {
					new SqlParameter("@TxnNo", SqlDbType.VarChar)
			};
            parameters[0].Value = TxnNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
