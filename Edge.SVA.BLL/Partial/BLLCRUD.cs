﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Edge.SVA.IDAL;
using Edge.SVA.DALFactory;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class BLLCRUD
    {
        private static readonly IDALCRUD dal = DataAccess.CreateDALCRUD();
        public DataSet Query(string sql)
        {
            return dal.Query(sql);
        }
        public int ExecuteSql(string sql) 
        {
            return dal.ExecuteSql(sql);
        }
        public int ExecuteSqlList(List<string> list)
        {
            return dal.ExecuteSqlList(list);
        }
    }
}
