﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Ord_OrderToSupplier_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_OrderToSupplier_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByOrder(string orderSupplierNumber)
        {
            return dal.DeleteByOrder(orderSupplierNumber);
        }

        public DataSet GetListGroupByCouponType(string strWhere)
        {
            return dal.GetListGroupByCouponType(strWhere);
        }
    }
}
