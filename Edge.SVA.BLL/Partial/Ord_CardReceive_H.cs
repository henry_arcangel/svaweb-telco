﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardReceive_H : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get
            {
                return DALFactory.DataAccess.CreateOrd_CardReceive_H() as IDAL.IBaseDAL;
            }
        }
    }
}
