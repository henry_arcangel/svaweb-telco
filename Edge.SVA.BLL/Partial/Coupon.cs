﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Coupon
    {
 
        public DataSet GetListWithBatch(int Top, string strWhere, string filedOrder)
        {
            return dal.GetListWithBatch(Top, strWhere, filedOrder);
        }

        public int GetCountWithBatch(string strWhere)
        {
            return dal.GetCountWithBatch(strWhere);
        }

        public DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetPageListWithBatch(pageSize, currentPage, strWhere, filedOrder);
        }

        public bool ExsitCoupon(Model.Ord_ImportCouponUID_H model)
        {
            return dal.ExsitCoupon(model);
        }

        public bool ExsitCoupon(Model.Ord_CouponAdjust_H model)
        {
            return dal.ExsitCoupon(model);
        }

        public bool ExsitCoupon(string couponNumber)
        {
            return dal.ExsitCoupon(couponNumber);
        }
        public bool ValidCouponStauts(Model.Ord_CouponAdjust_H model, params int[] CouponStatus)
        {
            return dal.ValidCouponStauts(model, CouponStatus);
        }

        public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        {
            return dal.GetListForBatchOperation(Top, strWhere, filedOrder);
        }

        public DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            return dal.GetListForTotal(PageSize, PageIndex, strWhere, filedOrder, fields, times);
        }

        public Edge.SVA.Model.Coupon GetModelByUID(string couponUID)
        {
            return dal.GetModelByUID(couponUID);
        }

        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, fields, times);
        }

        public int GetCount(string strWhere, int times)
        {
            return dal.GetCount(strWhere, times);
        }
    }
}
