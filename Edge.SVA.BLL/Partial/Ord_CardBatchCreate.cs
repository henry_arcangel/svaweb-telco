﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardBatchCreate : BaseBLL
    {
        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.CardGradeStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " CardGradeID in " + str;
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and CardGradeID in " + str;
                    }
                    else
                    {
                        strWhere = strs[0] + " and CardGradeID in " + str + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }
        //End


        public string ExportCSV(int batchID, int cardCount)
        {
            return dal.ExportCSV(batchID, cardCount);
        }


        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardBatchCreate() as IDAL.IBaseDAL; }
        }
    }
}
