﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL
{
    public partial class Promotion_Member
    {
        public List<PromotionMemberViewModel> GetNewModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return NewDataTableToList(ds.Tables[0]);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<PromotionMemberViewModel> NewDataTableToList(DataTable dt)
        {
            List<PromotionMemberViewModel> modelList = new List<PromotionMemberViewModel>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                PromotionMemberViewModel model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new PromotionMemberViewModel();
                    if (dt.Rows[n]["KeyID"] != null && dt.Rows[n]["KeyID"].ToString() != "")
                    {
                        model.KeyID = int.Parse(dt.Rows[n]["KeyID"].ToString());
                    }
                    if (dt.Rows[n]["PromotionCode"] != null && dt.Rows[n]["PromotionCode"].ToString() != "")
                    {
                        model.PromotionCode = dt.Rows[n]["PromotionCode"].ToString();
                    }
                    if (dt.Rows[n]["LoyaltyType"] != null && dt.Rows[n]["LoyaltyType"].ToString() != "")
                    {
                        model.LoyaltyType = int.Parse(dt.Rows[n]["LoyaltyType"].ToString());
                    }
                    if (dt.Rows[n]["LoyaltyValue"] != null && dt.Rows[n]["LoyaltyValue"].ToString() != "")
                    {
                        model.LoyaltyValue = int.Parse(dt.Rows[n]["LoyaltyValue"].ToString());
                    }
                    if (dt.Rows[n]["LoyaltyThreshold"] != null && dt.Rows[n]["LoyaltyThreshold"].ToString() != "")
                    {
                        model.LoyaltyThreshold = int.Parse(dt.Rows[n]["LoyaltyThreshold"].ToString());
                    }
                    if (dt.Rows[n]["LoyaltyBirthdayFlag"] != null && dt.Rows[n]["LoyaltyBirthdayFlag"].ToString() != "")
                    {
                        model.LoyaltyBirthdayFlag = int.Parse(dt.Rows[n]["LoyaltyBirthdayFlag"].ToString());
                    }
                    if (dt.Rows[n]["LoyaltyPromoScope"] != null && dt.Rows[n]["LoyaltyPromoScope"].ToString() != "")
                    {
                        model.LoyaltyPromoScope = int.Parse(dt.Rows[n]["LoyaltyPromoScope"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        public bool DeleteData(string PromotionCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Promotion_Member ");
            strSql.Append(" where PromotionCode=@PromotionCode");
            SqlParameter[] parameters = {
					new SqlParameter("@PromotionCode", SqlDbType.VarChar,64)};
            parameters[0].Value = PromotionCode;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
