﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class CardType
    {
        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.CardTypeStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " CardTypeID in " + str;
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and CardTypeID in " + str;
                    }
                    else
                    {
                        strWhere = strs[0] + " and CardTypeID in " + str + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }
        //End

        /// <summary>
        /// 获取总页数不加权限
        /// </summary>
        public int GetCountUnlimited(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

        public List<int> GetCardTypes(int brandID)
        {
            return dal.GetCardTypes(brandID);
        }

        public DataSet GetCardTypeList(int brandID)
        {
            return dal.GetList(" BrandID = " + brandID);
        }

        public DataSet GetListWithoutBrand(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        //Add by Alex 2014-06-09 ++
        public SVA.Model.CardType GetImportCardType(string cardTypeCode)
        {
            return dal.GetImportCardType(cardTypeCode);
        }
        //Add by Alex 2014-06-09 --
    }
}
