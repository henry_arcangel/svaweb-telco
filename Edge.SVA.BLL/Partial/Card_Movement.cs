﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
using System.Data;

namespace Edge.SVA.BLL
{
    /// <summary>
    /// Card操作的流水表。
    ///通过insert触发器
    /// </summary>
    public partial class Card_Movement
    {
        /// <summary>
        /// 获得总条数.Card_Move_Movement和Card内联查询
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public int GetCountWithCard(string strWhere)
        {
            return dal.GetCountWithCard(strWhere);
        }


        /// <summary>
        /// 获得查询分页数据.Card_Move_Movement和Card内联查询
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="currentPage"></param>
        /// <param name="strWhere"></param>
        /// <param name="filedOrder"></param>
        /// <returns></returns>
        public DataSet GetListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetListWithCard(pageSize,currentPage,strWhere,filedOrder);
        }

    }
}
