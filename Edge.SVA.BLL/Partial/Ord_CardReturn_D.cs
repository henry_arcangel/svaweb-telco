﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardReturn_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardReturn_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByOrder(string CardReturnNumber)
        {
            return dal.DeleteByOrder(CardReturnNumber);
        }
    }
}
