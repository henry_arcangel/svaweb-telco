﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardOrderForm_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardOrderForm_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByOrder(string CardOrderFormNumber)
        {
            return dal.DeleteByOrder(CardOrderFormNumber);
        }
   
    }
}
