﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL
{
    public partial class CouponType
    {
        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.BrandIDsStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " BrandID in " + str;
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and BrandID in " + str;
                    }
                    else
                    {
                        strWhere = strs[0] + " and BrandID in " + str + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }
        //End
        public DateTime? GetExpiryDate(Model.CouponType couponType)
        {
            switch (couponType.CouponValidityUnit)
            {
                case 1: return DateTime.Now.AddYears(couponType.CouponValidityDuration) ;   //年
                case 2: return DateTime.Now.AddMonths(couponType.CouponValidityDuration);   //月
                case 3: return DateTime.Now.AddDays(couponType.CouponValidityDuration*7);   //星期
                case 4: return DateTime.Now.AddDays(couponType.CouponValidityDuration);     //日
                case 5: return DateTime.Now;                                                //有效期不变更
            }
            return null;
        }

        public bool isHasCouponTypeCode(string code,int conuponID)
        {
            if (conuponID != 0)
            {
                Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(conuponID);

                if (model == null) return false;

                if (!string.IsNullOrEmpty(model.CouponTypeCode))
                {
                    if (code.ToUpper().Trim() == model.CouponTypeCode.ToUpper().Trim())
                        return false;
                }
            }

            int count = new Edge.SVA.BLL.CouponType().GetCount("CouponTypeCode='" + code + "'");
            return count > 0;
        }

        //private string SessionChangeBrandIDsStr(string strWhere)
        //{
        //    string str = SessionInfo.BrandIDsStr;
        //    if (!String.IsNullOrEmpty(str))
        //    {
        //        if (string.IsNullOrEmpty(strWhere))
        //        {
        //            strWhere = " BrandID in " + str;
        //        }
        //        else
        //        {
        //            string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
        //            if (strs.Length <= 1)
        //            {
        //                strWhere = strWhere + " and BrandID in " + str;
        //            }
        //            else
        //            {
        //                strWhere = strs[0] + " and BrandID in " + str + strs[1];
        //            }
        //        }
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(strWhere))
        //        {
        //            strWhere = " 1!=1 ";
        //        }
        //        else
        //        {
        //            string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
        //            if (strs.Length <= 1)
        //            {
        //                strWhere = strWhere + " and 1!=1 ";
        //            }
        //            else
        //            {
        //                strWhere = strs[0] + " and 1!=1 " + strs[1];
        //            }
        //        }
        //    }
        //    return strWhere;
        //}

        /// <summary>
        /// 获取总页数不加权限
        /// </summary>
        public int GetCountUnlimited(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

        public SVA.Model.CouponType GetImportCouponType(string couponTypeCode)
        {
            return dal.GetImportCouponType(couponTypeCode);
        }

        public DataSet GetCouponTypes(int brandID)
        {
            return dal.GetList(" BrandID = " + brandID);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListWithoutBrand(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetList(strWhere);
        }

        //Add by Robin 2015-01-07 for Get Coupon Nature
        public DataSet GetCouponNatureList(string couponTypeID)
        {
            string sql = "select l.KeyID,l.CouponTypeID,l.CouponNatureID, c.* from CouponNature c inner join CouponNature_List l on c.couponnatureid=l.couponnatureid where l.CouponTypeID=@couponTypeID";
            SqlParameter[] parameters = {
					new SqlParameter("@couponTypeID", SqlDbType.Int)};

            parameters[0].Value = couponTypeID;

            return DbHelperSQL.Query(sql, parameters);
        }
        //End
    }
}
