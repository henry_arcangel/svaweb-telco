﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class WalletRule_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateWalletRule_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByCode(string walletRuleCode)
        {
            return dal.DeleteByCode(walletRuleCode);
        }
    }
}
