﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardReceive_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CardReceive_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByOrder(string cardReceiveNumber)
        {
            return dal.DeleteByOrder(cardReceiveNumber);
        }
    }
}
