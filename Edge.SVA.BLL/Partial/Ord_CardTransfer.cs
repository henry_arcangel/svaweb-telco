﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardTransfer : BaseBLL
    {
        public bool Update(Model.Ord_CardTransfer model, int times)
        {
            return dal.Update(model, times);
        }

        protected override IDAL.IBaseDAL BaseDAL
        {
            get
            {
                return DALFactory.DataAccess.CreateOrd_CardTransfer() as IDAL.IBaseDAL;
            }
        }
    }
}
