﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Edge.SVA.BLL.Domain.DataResources
{
    /// <summary>
    /// mapping 
    /// </summary>
    public class CouponTypeRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly CouponTypeRepostory instance = new CouponTypeRepostory();
        private CouponTypeRepostory()
        {
            LoadDataFromDatabase();
        }
        
        private void LoadDataFromDatabase()
        {
            CouponType bll = new CouponType();
            List<Edge.SVA.Model.CouponType> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.CouponType> dic = new Dictionary<int, Edge.SVA.Model.CouponType>();
            Dictionary<string, Edge.SVA.Model.CouponType> dic1 = new Dictionary<string, Edge.SVA.Model.CouponType>();
            foreach (var item in list)
            {
                dic.Add(item.CouponTypeID, item);
                dic1.Add(item.CouponTypeCode, item);
            }
            idDic = dic;
            codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.CouponType> idDic = new Dictionary<int, Edge.SVA.Model.CouponType>();
        private Dictionary<string, Edge.SVA.Model.CouponType> codeDic = new Dictionary<string, Edge.SVA.Model.CouponType>();
        public static CouponTypeRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.CouponType GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.CouponType();
        }
        public Edge.SVA.Model.CouponType GetModelByCode(string code)
        {
            if (!codeDic.ContainsKey(code))
            {
                Refresh();
            }
            if (codeDic.ContainsKey(code))
            {
                return codeDic[code];
            }
            return new Edge.SVA.Model.CouponType();
        }
    }
}
