﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class BrandRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly BrandRepostory instance = new BrandRepostory();
        private BrandRepostory()
        {
            LoadDataFromDatabase();
        }

        private void LoadDataFromDatabase()
        {
            Brand bll = new Brand();
            List<Edge.SVA.Model.Brand> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.Brand> dic = new Dictionary<int, Edge.SVA.Model.Brand>();
            Dictionary<string, Edge.SVA.Model.Brand> dic1 = new Dictionary<string, Edge.SVA.Model.Brand>();
            foreach (var item in list)
            {
                dic.Add(item.BrandID, item);
                dic1.Add(item.BrandCode, item);
            }
            idDic = dic;
            codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.Brand> idDic = new Dictionary<int, Edge.SVA.Model.Brand>();
        private Dictionary<string, Edge.SVA.Model.Brand> codeDic = new Dictionary<string, Edge.SVA.Model.Brand>();
        public static BrandRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.Brand GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.Brand();
        }
        public Edge.SVA.Model.Brand GetModelByCode(string code)
        {
            if (!codeDic.ContainsKey(code))
            {
                Refresh();
            }
            if (codeDic.ContainsKey(code))
            {
                return codeDic[code];
            }
            return new Edge.SVA.Model.Brand();
        }
    }
}
