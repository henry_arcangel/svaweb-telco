﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class CardGradeRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly CardGradeRepostory instance = new CardGradeRepostory();
        private CardGradeRepostory()
        {
            LoadDataFromDatabase();
        }

        private void LoadDataFromDatabase()
        {
            CardGrade bll = new CardGrade();
            List<Edge.SVA.Model.CardGrade> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.CardGrade> dic = new Dictionary<int, Edge.SVA.Model.CardGrade>();
            Dictionary<string, Edge.SVA.Model.CardGrade> dic1 = new Dictionary<string, Edge.SVA.Model.CardGrade>();
            foreach (var item in list)
            {
                dic.Add(item.CardGradeID, item);
                dic1.Add(item.CardGradeCode, item);
            }
            idDic = dic;
            codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.CardGrade> idDic = new Dictionary<int, Edge.SVA.Model.CardGrade>();
        private Dictionary<string, Edge.SVA.Model.CardGrade> codeDic = new Dictionary<string, Edge.SVA.Model.CardGrade>();
        public static CardGradeRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.CardGrade GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.CardGrade();
        }
        public Edge.SVA.Model.CardGrade GetModelByCode(string code)
        {
            if (!codeDic.ContainsKey(code))
            {
                Refresh();
            }
            if (codeDic.ContainsKey(code))
            {
                return codeDic[code];
            }
            return new Edge.SVA.Model.CardGrade();
        }
    }
}
