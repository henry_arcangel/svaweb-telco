﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Edge.SVA.BLL.Domain.DataResources
{
    /// <summary>
    /// this mapping table CouponNature
    /// </summary>
    public class CouponTypeNatureRepostory
    {
                private static readonly object syncObj = new object();
        private static readonly CouponTypeNatureRepostory instance = new CouponTypeNatureRepostory();
        private CouponTypeNatureRepostory()
        {
            LoadDataFromDatabase();
        }
        
        private void LoadDataFromDatabase()
        {
            CouponTypeNature bll = new CouponTypeNature();
            List<Edge.SVA.Model.CouponTypeNature> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.CouponTypeNature> dic = new Dictionary<int, Edge.SVA.Model.CouponTypeNature>();
            //Dictionary<string, Edge.SVA.Model.CouponTypeNature> dic1 = new Dictionary<string, Edge.SVA.Model.CouponTypeNature>();
            foreach (var item in list)
            {
                dic.Add(item.CouponTypeNatureID, item);
                //dic1.Add(item.CouponTypeNatureCode, item);
            }
            idDic = dic;
            //codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.CouponTypeNature> idDic = new Dictionary<int, Edge.SVA.Model.CouponTypeNature>();
        //private Dictionary<string, Edge.SVA.Model.CouponTypeNature> codeDic = new Dictionary<string, Edge.SVA.Model.CouponTypeNature>();
        public static CouponTypeNatureRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.CouponTypeNature GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.CouponTypeNature();
        }
        //public Edge.SVA.Model.CouponTypeNature GetModelByCode(string code)
        //{
        //    if (codeDic.ContainsKey(code))
        //    {
        //        return codeDic[code];
        //    }
        //    return null;
        //}
    }
}
