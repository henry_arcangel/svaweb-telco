﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class Campaign
    {
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.Campaign model, SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Campaign(");
            strSql.Append("CampaignCode,CampaignName1,CampaignName2,CampaignName3,CampaignDetail1,CampaignDetail3,CampaignDetail2,CampaignType,CampaignPicFile,Status,BrandID,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
            strSql.Append(" values (");
            strSql.Append("@CampaignCode,@CampaignName1,@CampaignName2,@CampaignName3,@CampaignDetail1,@CampaignDetail3,@CampaignDetail2,@CampaignType,@CampaignPicFile,@Status,@BrandID,@StartDate,@EndDate,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512),
					new SqlParameter("@CampaignName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignDetail1", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail3", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail2", SqlDbType.NVarChar),
					new SqlParameter("@CampaignType", SqlDbType.Int,4),
					new SqlParameter("@CampaignPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignCode;
            parameters[1].Value = model.CampaignName1;
            parameters[2].Value = model.CampaignName2;
            parameters[3].Value = model.CampaignName3;
            parameters[4].Value = model.CampaignDetail1;
            parameters[5].Value = model.CampaignDetail3;
            parameters[6].Value = model.CampaignDetail2;
            parameters[7].Value = model.CampaignType;
            parameters[8].Value = model.CampaignPicFile;
            parameters[9].Value = model.Status;
            parameters[10].Value = model.BrandID;
            parameters[11].Value = model.StartDate;
            parameters[12].Value = model.EndDate;
            parameters[13].Value = model.CreatedOn;
            parameters[14].Value = model.CreatedBy;
            parameters[15].Value = model.UpdatedOn;
            parameters[16].Value = model.UpdatedBy;

            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            //添加主表时需要返回主键ID
            object id = SqlHelper.NewExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL

            return Convert.ToInt32(id);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public void Update(Edge.SVA.Model.Campaign model, SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Campaign set ");
            strSql.Append("CampaignName1=@CampaignName1,");
            strSql.Append("CampaignName2=@CampaignName2,");
            strSql.Append("CampaignName3=@CampaignName3,");
            strSql.Append("CampaignDetail1=@CampaignDetail1,");
            strSql.Append("CampaignDetail3=@CampaignDetail3,");
            strSql.Append("CampaignDetail2=@CampaignDetail2,");
            strSql.Append("CampaignType=@CampaignType,");
            strSql.Append("CampaignPicFile=@CampaignPicFile,");
            strSql.Append("Status=@Status,");
            strSql.Append("BrandID=@BrandID,");
            strSql.Append("StartDate=@StartDate,");
            strSql.Append("EndDate=@EndDate,");
            strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("CampaignCode=@CampaignCode");
            strSql.Append(" where CampaignID=@CampaignID");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CampaignDetail1", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail3", SqlDbType.NVarChar),
					new SqlParameter("@CampaignDetail2", SqlDbType.NVarChar),
					new SqlParameter("@CampaignType", SqlDbType.Int,4),
					new SqlParameter("@CampaignPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CampaignCode", SqlDbType.VarChar,512)};
            parameters[0].Value = model.CampaignName1;
            parameters[1].Value = model.CampaignName2;
            parameters[2].Value = model.CampaignName3;
            parameters[3].Value = model.CampaignDetail1;
            parameters[4].Value = model.CampaignDetail3;
            parameters[5].Value = model.CampaignDetail2;
            parameters[6].Value = model.CampaignType;
            parameters[7].Value = model.CampaignPicFile;
            parameters[8].Value = model.Status;
            parameters[9].Value = model.BrandID;
            parameters[10].Value = model.StartDate;
            parameters[11].Value = model.EndDate;
            parameters[12].Value = model.CreatedOn;
            parameters[13].Value = model.CreatedBy;
            parameters[14].Value = model.UpdatedOn;
            parameters[15].Value = model.UpdatedBy;
            parameters[16].Value = model.CampaignID;
            parameters[17].Value = model.CampaignCode;

            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public void Delete(int CampaignID, SqlTransaction trans)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Campaign ");
            strSql.Append(" where CampaignID=@CampaignID");
            SqlParameter[] parameters = { new SqlParameter("@CampaignID", SqlDbType.Int, 4) };
            parameters[0].Value = CampaignID;

            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }
    }
}
