﻿<%@ Page Language="c#" CodeBehind="EditRole.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.Admin.EditRole" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>EditRole</title>
</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：编辑角色信息</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                编辑角色信息
            </th>
        </tr>
        <tr align="left">
            <td width="50%" valign="middle">
                初始权限名称：
                <asp:TextBox ID="TxtNewname" runat="server"></asp:TextBox>&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <b>
                    <asp:Label ID="RoleLabel" runat="server"></asp:Label></b>
            </td>
        </tr>
        <tr>
            <td height="30" valign="middle" colspan="2">
                <b>&nbsp;&nbsp; 权限类别</b>：
                <asp:DropDownList ID="CategoryDownList" runat="server" AutoPostBack="True" Width="245px"
                    BackColor="GhostWhite" OnSelectedIndexChanged="CategoryDownList_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td height="22" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                    <tr>
                        <td valign="top" align="center" width="43%">
                            <asp:ListBox ID="CategoryList" runat="server" Width="100%" Rows="15" BackColor="AliceBlue"
                                Font-Size="9pt" SelectionMode="Multiple"></asp:ListBox>
                        </td>
                        <td align="center" valign="middle" width="14%">
                            <p>
                                <asp:Button ID="AddPermissionButton" runat="server" CssClass="submit" Text="增加权限 >>"
                                    OnClick="AddPermissionButton_Click"></asp:Button></p>
                            <p>
                                <asp:Button ID="RemovePermissionButton" runat="server" CssClass="submit" Text="<< 移除权限"
                                    OnClick="RemovePermissionButton_Click"></asp:Button></p>
                        </td>
                        <td valign="top" align="center" width="43%">
                            <asp:ListBox ID="PermissionList" runat="server" Width="100%" Rows="15" BackColor="AliceBlue"
                                Font-Size="9pt" SelectionMode="Multiple"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                品牌列表
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="cbSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="cbSelectAll_CheckedChanged"
                    Text="全选" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="3" Width="100%">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
    <div align="center">
    </div>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Button ID="BtnUpName" runat="server" Text="保存" OnClick="BtnUpName_Click" CssClass="submit" />
        <asp:Button ID="button2" runat="server" Text="返回" OnClick="button2_Click" CssClass="submit" />
        <asp:Button ID="RemoveRoleButton" runat="server" Text="删除当前角色" OnClick="RemoveRoleButton_Click"
            CssClass="submit"></asp:Button>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
