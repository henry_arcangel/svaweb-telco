﻿<%@ Page Language="c#" CodeBehind="PermissionAdmin.aspx.cs" AutoEventWireup="True"
    Inherits="Edge.Web.Accounts.Admin.PermissionAdmin" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">

    <script type="text/javascript">

        $(function() {
            $(".msgrpt tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgrpt tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：权限管理</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <asp:Label ID="LoginResult" runat="server">权限管理</asp:Label>
            </th>
        </tr>
        <%-- <tr>
            <td valign="middle">
                增加权限类别：
                <asp:TextBox ID="CategoriesName" runat="server" Width="156"></asp:TextBox>&nbsp;<asp:Button
                    ID="BtnAddCategory" runat="server" Text="新增" CssClass="submit" OnClick="BtnAddCategory_Click" />
                <asp:Label ID="lbltip1" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        --%>
        <tr>
            <td valign="middle">
                选择权限类别：
                <asp:DropDownList ID="ClassList" runat="server" Width="156px" AutoPostBack="True"
                    OnSelectedIndexChanged="ClassList_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;
                <asp:Button ID="BtnDelCategory" runat="server" Text="删除" CssClass="submit" OnClick="BtnDelCategory_Click"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td height="28">
                增加新权限：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="PermissionsName" runat="server" Width="156"></asp:TextBox>&nbsp;
                <asp:Button ID="BtnAddPermissions" runat="server" Text="新增" CssClass="submit" OnClick="BtnAddPermissions_Click" />
                <asp:Label ID="lbltip2" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                该类别的权限列表
            </td>
        </tr>
        <tr>
            <td>
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgrpt">
                            <tr>
                                <th width="6%">
                                    <input type="checkbox" onclick="checkAll(this);" />
                                    选择
                                </th>
                                <th width="20%">
                                    权限编码
                                </th>
                                <th width="60%">
                                    权限名称
                                </th>
                                <th width="20%">
                                    操作
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lb_id" runat="server" Text='<%#Eval("PermissionID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <span class="btn_bg">
                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit">编 辑</asp:LinkButton>
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div style="line-height: 30px; height: 30px;">
                    <div class="left">
                        <span class="btn_bg">
                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="return confirm( '确定要删除这些记录吗？ ');"
                                OnClick="btnDelete_Click">删 除</asp:LinkButton>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable" id="TabEdit"
                    runat="server">
                    <tr>
                        <td align="center" colspan="2">
                            <strong>以下只能修改权限新增时的默认名字！</strong> 权限编码<asp:Label ID="lblPermId" runat="server" Font-Bold="True"></asp:Label>的名称修改
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="40%">
                            新权限名称：
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewName" runat="server"></asp:TextBox>
                            <asp:Label ID="lbltip3" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnupSave" runat="server" Text="保存" CssClass="submit" OnClick="btnupSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="submit" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
