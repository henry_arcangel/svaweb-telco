﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Edge.Security.Manager;
using Edge.Common;
using Edge.Web.UI;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.Admin
{
	/// <summary>
	/// UserAdmin 的摘要说明。
	/// </summary>
    public partial class UserAdmin : Edge.Web.UI.ManagePage
	{

        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        public string keywords = "";

		protected void Page_Load(object sender, System.EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            if (!string.IsNullOrEmpty(Request.Params["keywords"]))
            {
                this.keywords = Request.Params["keywords"].Trim();
            }

            if (!Page.IsPostBack)
            {
                RptBind("UserID>0" + CombSqlTxt(this.keywords), "UserID ASC");
            }
		}

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }
            User userAdmin = new User();
            //获得总条数
            this.pcount = userAdmin.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            this.txtKeywords.Text = this.keywords;

            DataSet ds = userAdmin.GetPageList(this.pagesize, this.page, strWhere, orderby);

            Tools.DataTool.AddSex(ds, "SexName", "Sex");
            this.rptList.DataSource = ds.Tables[0];
            this.rptList.DataBind();
        }
        #endregion


        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");

            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and UserName like '%" + _keywords + "%'");
            }
          
            return strTemp.ToString();
        }

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hf_id")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    User currentUser = new User(id);
                    //保存日志
                    // SaveLogs("：" + );
                    Logger.Instance.WriteOperationLog(this.PageName, "Delete User " + currentUser.UserName);
                    currentUser.Delete();
                }
            }

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "UserAdmin.aspx?" + CombUrlTxt(txtKeywords.Text.Trim()) + "page=0", Resources.MessageTips.SUCESS_TITLE);
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserAdmin.aspx?" + CombUrlTxt(txtKeywords.Text.Trim()) + "page=0");
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("../add.aspx");
        }


	}
}
