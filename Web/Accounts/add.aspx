﻿<%@ Page Language="c#" CodeBehind="Add.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Add</title>
    <%--    <script language="javascript" src="../../js/date.js"></script>--%>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                rules: {
                    txtPassword:
                    {
                        required: true
                    },
                    txtPassword1: {
                        required: true
                    }
                },
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
            jQuery.validator.addMethod("password", function (value, element) {
                if (value == null || value.length <= 0) return true;

                var password = $("#txtPassword").val();
                return password == $("#txtPassword1").val();
            }, jQuery.validator.messages.equalTo);
        });
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：增加用户</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                增加用户
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                用户名：
            </td>
            <td width="75%">
                <asp:TextBox ID="txtUserName" TabIndex="1" runat="server" Width="249px" MaxLength="20"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <div align="right">
                    密 码：</div>
            </td>
            <td>
                <asp:TextBox ID="txtPassword" TabIndex="2" runat="server" Width="249px" MaxLength="20"
                    TextMode="Password" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码验证：
            </td>
            <td>
                <asp:TextBox ID="txtPassword1" TabIndex="3" runat="server" Width="249px" MaxLength="20" SkinID="NoClass" CssClass="input password"
                    TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                真实姓名：
            </td>
            <td>
                <asp:TextBox ID="txtTrueName" TabIndex="4" runat="server" Width="249px" MaxLength="20"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                用户性别：
            </td>
            <td>
                <asp:RadioButtonList ID="rdblSex" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Text="男" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="0" Text="女"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Width="200px" CssClass="input digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                电子邮箱：
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="200px" CssClass="input email"></asp:TextBox>
            </td>
        </tr>
        <%--				    <tr>
					    <td align="right">界面风格：
					    </td>
					    <td ><asp:DropDownList id="dropStyle" runat="server" Width="200px">
							    <asp:ListItem Value="1">默认蓝</asp:ListItem>
							    <asp:ListItem Value="2">橄榄绿</asp:ListItem>
							    <asp:ListItem Value="3">深红</asp:ListItem>
							    <asp:ListItem Value="4">深绿</asp:ListItem>
						    </asp:DropDownList></td>
				    </tr>--%>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="history.back()" class="submit" /></div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
