﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<%@ Page Language="c#" CodeBehind="userinfo.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.userinfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>userinfo</title>

    <script src="../js/jquery-1.4.1.min.js" type="text/javascript"></script>

</head>
<body style="padding: 10px;">
    <form id="form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                用户名：
            </td>
            <td width="75%">
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                真实姓名：
            </td>
            <td>
                <asp:Label ID="lblTruename" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                性别：
            </td>
            <td>
                <asp:RadioButtonList ID="rdblSex" runat="server" RepeatDirection="Horizontal" Enabled="False">
                    <asp:ListItem Value="1" Text="男"></asp:ListItem>
                    <asp:ListItem Value="0" Text="女"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:Label ID="lblPhone" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                电子邮箱：
            </td>
            <td>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </td>
        </tr>
        <%--	<tr>
		<td align="right">界面风格：
		</td>
		<td >
			<asp:Label id="lblStyle" runat="server"></asp:Label></td>
	</tr>--%>
        <tr>
            <td align="right">
                Translate__Special_121_Start当前IP：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="lblUserIP" runat="server"></asp:Label>
            </td>
        </tr>
        <%--	<tr>
		<td align="right">当前余额：
		</td>
		<td >
			<asp:Label id="lblModeys" runat="server"></asp:Label>(元)</td>
	</tr>--%>
    </table>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Label ID="RoleList" Visible="False" runat="server"></asp:Label></td></div>
    <uc1:CheckRight ID="CheckRight1" runat="server"></uc1:CheckRight>
    </form>
</body>
</html>
