﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoleLanAdmin.aspx.cs" Inherits="Edge.Web.Accounts.LanAdmin.RoleLanAdmin" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="HEAD1" runat="server">
    <title>Index</title>

    <script src="../../js/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../../js/jquery.pagination.js" type="text/javascript"></script>

    <script src="../../js/function.js" type="text/javascript"></script>

</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">

    <script type="text/javascript">
        function pageselectCallback(page_id, jq) {
            //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgrpt tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgrpt tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <asp:Label ID="Label2" runat="server"><%=this.PageName %></asp:Label>
            </th>
        </tr>
        <tr>
            <td valign="middle" colspan="2">
                选择角色：
                <asp:DropDownList ID="ddlRoleList" runat="server" Width="156px" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlRoleList_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;
                <asp:Button ID="btnDisplayInsertLan" runat="server" Text="新增语言" CssClass="submit"
                    OnClick="btnDisplayInsertLan_Click" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <strong>该角色的语言列表</strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgrpt">
                            <tr>
                                <th width="6%"><input type="checkbox"  onclick="checkAll(this);"/>
                                    选择
                                </th>
                                <th width="20%">
                                    角色编码
                                </th>
                                <th width="30%">
                                    角色名称
                                </th>
                                 <th width="30%">
                                    语言
                                </th>
                                <th width="20%">
                                    操作
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lb_id" runat="server" Text='<%#Eval("RoleID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                            </td>
                               <td align="center">
                                <asp:Label ID="lblLan" runat="server" Visible="false" Text='<%#Eval("Lan")%>'></asp:Label>
                                 <asp:Label ID="lblLanDesc" runat="server" Text='<%#Eval("LanDesc")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <span class="btn_bg">
                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit">编辑</asp:LinkButton>
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
           <tr>
            <td>
                <div class="left">
                     <span class="btn_bg">
                        <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                            OnClick="btnDelete_Click">删 除</asp:LinkButton>
                    </span>
                </div>
            </td>
        </tr>
    </table>
    
    
     <table runat="server" id="modifyTbl" width="100%" border="0" cellspacing="0" cellpadding="0"
        class="msgtable">
        <tr>
            <td align="center" colspan="2">
                <strong>权限编码[</strong><asp:Label ID="lbRoleId" runat="server" Font-Bold="True"></asp:Label><strong>]
                    語言[<asp:Label ID="lblPermLan" runat="server" Font-Bold="True"></asp:Label>
                <asp:Label ID="lblPermLanDesc" runat="server" Font-Bold="True"></asp:Label>]的名称修改</strong>
            </td>
        </tr>
        <tr>
            <td align="right">
                权限名称：
            </td>
            <td>
                <asp:TextBox ID="txtModifyLan" runat="server"></asp:TextBox>
                <asp:Label ID="lbltip3" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnModifyLan" runat="server" Text="保存" CssClass="submit" OnClick="btnModifyLan_Click" />
                <asp:Button ID="btnModifyCancel" runat="server" Text="取消" CssClass="submit" OnClick="btnModifyCancel_Click" />
            </td>
        </tr>
    </table>
    <table runat="server" id="insertTbl" width="100%" border="0" cellspacing="0" cellpadding="0"
        class="msgtable">
        <tr>
            <td align="right">
                名称：
            </td>
            <td>
                <asp:TextBox ID="txtNewLan" runat="server"></asp:TextBox>
                <asp:Label ID="lbltip1" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                语言：
            </td>
            <td>
                <asp:DropDownList ID="ddlLanList" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnInsertLan" runat="server" Text="保存" CssClass="submit" OnClick="btnInsertLan_Click" />
                <asp:Button ID="btnInsertCancel" runat="server" Text="取消" CssClass="submit" OnClick="btnInsertCancel_Click" />
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>