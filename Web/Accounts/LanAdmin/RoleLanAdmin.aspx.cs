﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.LanAdmin
{
    public partial class RoleLanAdmin : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.insertTbl.Visible = false;
                this.modifyTbl.Visible = false;
                LanTools.BindLanList(this.ddlLanList);
                BindRoleList();
                BindLanListByRole();
            }

        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int RoleID = Convert.ToInt32(((Label)e.Item.FindControl("lb_id")).Text);
            string strRoleDesc = ((Label)e.Item.FindControl("lblDescription")).Text;
            string strLanDesc = ((Label)e.Item.FindControl("lblLanDesc")).Text;
            string strLan = ((Label)e.Item.FindControl("lblLan")).Text;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    this.modifyTbl.Visible = true;
                    this.lbRoleId.Text = RoleID.ToString();
                    this.lblPermLan.Text = strLan;
                    this.lblPermLanDesc.Text = strLanDesc;
                    this.txtModifyLan.Text = strRoleDesc;
                    break;

            }
        }

        protected void btnDisplayInsertLan_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = true;
            this.lbltip1.Text = "";
            this.lbltip3.Text = "";
            this.txtNewLan.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnModifyLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtModifyLan.Text.Trim()))
            {
                this.lbltip3.Text = Resources.MessageTips.NameNotEmpty;
            }

            //if (new Edge.Security.Manager.Lan_Accounts_Roles().GetCount(string.Format("Description='{0}' and Lan='{1}'", this.txtNewLan.Text.Trim(), this.ddlLanList.SelectedItem.Value.Trim())) > 0)
            //{
            //    this.lbltip3.Text = Resources.MessageTips.Exists;
            //    return;
            //}

            Edge.Security.Manager.Lan_Accounts_Roles p = new Edge.Security.Manager.Lan_Accounts_Roles();
            Edge.Security.Model.Lan_Accounts_Roles item = new Edge.Security.Model.Lan_Accounts_Roles();
            item.Description = this.txtModifyLan.Text.Trim();
            item.Lan = this.lblPermLan.Text.Trim();
            item.RoleID = int.Parse(this.lbRoleId.Text.Trim());
            p.Update(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Update Role Language: " + item.Description + " and Language: " + item.Lan);

            this.modifyTbl.Visible = false;
            BindLanListByRole();
        }

        protected void btnModifyCancel_Click(object sender, EventArgs e)
        {
            this.modifyTbl.Visible = false;
            this.lbRoleId.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnInsertLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtNewLan.Text.Trim()))
            {
                this.lbltip1.Text = Resources.MessageTips.NameNotEmpty;
            }

            if (new Edge.Security.Manager.Lan_Accounts_Roles().GetCount(string.Format(" Lan='{0}' and RoleID={1}", this.ddlLanList.SelectedItem.Value.Trim(), Tools.ConvertTool.ToInt(this.ddlRoleList.SelectedValue))) > 0)
            {
                this.lbltip1.Text = Resources.MessageTips.Exists;
                return;
            }


            Edge.Security.Manager.Lan_Accounts_Roles bllLan = new Edge.Security.Manager.Lan_Accounts_Roles();
            Edge.Security.Model.Lan_Accounts_Roles item = new Edge.Security.Model.Lan_Accounts_Roles();
            item.Description = this.txtNewLan.Text.Trim();
            item.Lan = this.ddlLanList.SelectedItem.Value;
            item.RoleID = Tools.ConvertTool.ToInt(this.ddlRoleList.SelectedValue);
            bllLan.Add(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Role Language: " + item.Description + " and Language: " + item.Lan);

            this.insertTbl.Visible = false;

            BindLanListByRole();

        }

        protected void btnInsertCancel_Click(object sender, EventArgs e)
        {

            this.insertTbl.Visible = false;
            this.txtModifyLan.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int RoleID = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                string strRoleDesc = ((Label)rptList.Items[i].FindControl("lblDescription")).Text;
                string strLan = ((Label)rptList.Items[i].FindControl("lblLan")).Text;
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    Edge.Security.Manager.Lan_Accounts_Roles p = new Edge.Security.Manager.Lan_Accounts_Roles();
                    Edge.Security.Model.Lan_Accounts_Roles item = new Edge.Security.Model.Lan_Accounts_Roles();
                    item.Description = strRoleDesc;
                    item.Lan = strLan;
                    item.RoleID = RoleID;
                    p.Delete(item);
                    Logger.Instance.WriteOperationLog(this.PageName, "Delete Role Language: " + item.Description + " and Language: " + item.Lan);

                    //保存日志
                    // SaveLogs("" + model.Title); 
                }
            }

            BindLanListByRole();

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "RoleLanAdmin.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
        }

        private void BindRoleList()
        {
            DataSet CategoryList = AccountsTool.GetRoleList(Session["SiteLanguage"].ToString());//todo: 修改成多语言。	
            this.ddlRoleList.DataSource = CategoryList.Tables["Roles"];
            this.ddlRoleList.DataValueField = "RoleID";
            this.ddlRoleList.DataTextField = "Description";
            this.ddlRoleList.DataBind();
        }

        private void BindLanListByRole()
        {
            Edge.Security.Manager.Lan_Accounts_Roles lanBll = new Edge.Security.Manager.Lan_Accounts_Roles();
            int RoleID = int.Parse(this.ddlRoleList.SelectedValue);
            DataSet LanList = lanBll.GetList(" RoleID=" + RoleID);
            LanTools.AddLanDesc(LanList, "LanDesc", "Lan");
            this.rptList.DataSource = LanList;
            this.rptList.DataBind();
        }

        protected void ddlRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLanListByRole();
        }


    }
}
