﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.LanAdmin
{
    public partial class PermissionLanAdmin : Edge.Web.UI.ManagePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                this.insertTbl.Visible=false;
                this.modifyTbl.Visible = false;
               // BindCategoryList();
                BindCategoryTree();
                BindPermissionListByCategory();
                BindLanListByPermission();
                LanTools.BindLanList(this.ddlLanList);
            }
		}


        #region 绑定下拉树
        private void BindCategoryTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.ddlPermissionCategoryList.Items.Clear();
            //加载树
            this.ddlPermissionCategoryList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlPermissionCategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.ddlPermissionCategoryList.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.ddlPermissionCategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion

        private void BindLanListByPermission()
        {
            this.rptList.Controls.Clear();
            if (!string.IsNullOrEmpty(this.ddlPermissionLsit.SelectedValue))
            {
                Edge.Security.Manager.Lan_Accounts_Permissions lanBll = new Edge.Security.Manager.Lan_Accounts_Permissions();
                int PermissionId = int.Parse(this.ddlPermissionLsit.SelectedValue);
                DataSet LanList = lanBll.GetList(" PermissionID=" + PermissionId);
                LanTools.AddLanDesc(LanList, "LanDesc", "Lan");
                this.rptList.DataSource = LanList;
                this.rptList.DataBind();
            }
        }

        private void BindPermissionListByCategory()
        {
            if (!string.IsNullOrEmpty(this.ddlPermissionCategoryList.SelectedValue))
            {
                int CategoryId = int.Parse(this.ddlPermissionCategoryList.SelectedValue);
                DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                this.ddlPermissionLsit.DataSource = PermissionsList.Tables["Categories"];
                this.ddlPermissionLsit.DataValueField = "PermissionID";
                this.ddlPermissionLsit.DataTextField = "Description";
                this.ddlPermissionLsit.DataBind();
            }
        }

        //private void BindCategoryList()
        //{
        //    DataSet CategoryList = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());//todo: 修改成多语言。
        //    this.ddlPermissionCategoryList.DataSource = CategoryList.Tables["Categories"];
        //    this.ddlPermissionCategoryList.DataValueField = "CategoryID";
        //    this.ddlPermissionCategoryList.DataTextField = "Description";
        //    this.ddlPermissionCategoryList.DataBind();
        //}
       
        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int PermissionsID = Convert.ToInt32(((Label)e.Item.FindControl("lb_id")).Text);
            string strPermissionsDesc = ((Label)e.Item.FindControl("lblDescription")).Text;
            string strLan = ((Label)e.Item.FindControl("lblLan")).Text;
            string strLanDesc = ((Label)e.Item.FindControl("lblLanDesc")).Text;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    this.modifyTbl.Visible = true;
                    this.lblPermId.Text = PermissionsID.ToString();
                    this.lblPermLan.Text = strLan;
                    this.lblPermLanDesc.Text = strLanDesc;
                    this.txtModifyLan.Text = strPermissionsDesc;
                    break;

            }
        }

        protected void ddlPermissionLsit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLanListByPermission();
        }

        protected void ddlPermissionCategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPermissionListByCategory();
            BindLanListByPermission();
        }

        protected void btnDisplayInsertLan_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = true;
            this.lbltip1.Text = "";
            this.lbltip3.Text = "";
            this.txtNewLan.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnInsertLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtNewLan.Text.Trim()))
            {
                this.lbltip1.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }

            if (new Edge.Security.Manager.Lan_Accounts_Permissions().GetCount(string.Format(" Lan='{0}' and PermissionID={1}", this.ddlLanList.SelectedItem.Value.Trim(), Tools.ConvertTool.ToInt(this.ddlPermissionLsit.SelectedValue))) > 0)
            {
                this.lbltip1.Text = Resources.MessageTips.Exists;
                return;
            }


            Edge.Security.Manager.Lan_Accounts_Permissions bllLan = new Edge.Security.Manager.Lan_Accounts_Permissions();
            Edge.Security.Model.Lan_Accounts_Permissions item = new Edge.Security.Model.Lan_Accounts_Permissions();
            item.Description = this.txtNewLan.Text.Trim();
            item.Lan = this.ddlLanList.SelectedItem.Value;
            item.PermissionID = Tools.ConvertTool.ToInt(this.ddlPermissionLsit.SelectedValue);
            bllLan.Add(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Permission Language: " + item.Description + " and Language: " + item.Lan);
            this.insertTbl.Visible = false;

            BindLanListByPermission();

        }

        protected void btnInsertCancel_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = false;
            this.txtModifyLan.Text = "";
        }

        protected void btnModifyCancel_Click(object sender, EventArgs e)
        {
            this.modifyTbl.Visible = false;
            this.lblPermId.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnModifyLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtModifyLan.Text.Trim()))
            {
                this.lbltip3.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }

            //if (new Edge.Security.Manager.Lan_Accounts_Permissions().GetCount(string.Format("Description='{0}' and Lan='{1}'", this.txtNewLan.Text.Trim(), this.ddlLanList.SelectedItem.Value.Trim())) > 0)
            //{
            //    this.lbltip3.Text = Resources.MessageTips.Exists;
            //    return;
            //}

            Edge.Security.Manager.Lan_Accounts_Permissions p = new Edge.Security.Manager.Lan_Accounts_Permissions();
            Edge.Security.Model.Lan_Accounts_Permissions item = new Edge.Security.Model.Lan_Accounts_Permissions();
            item.Description = this.txtModifyLan.Text.Trim();
            item.Lan = this.lblPermLan.Text.Trim();
            item.PermissionID = int.Parse(this.lblPermId.Text.Trim());
            p.Update(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Update Permission Language: " + item.Description + " and Language: " + item.Lan);
            this.modifyTbl.Visible = false;
            BindLanListByPermission();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int PermissionsID = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                string strPermissionsDesc = ((Label)rptList.Items[i].FindControl("lblDescription")).Text;
                string strLan = ((Label)rptList.Items[i].FindControl("lblLan")).Text;
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                     Edge.Security.Manager.Lan_Accounts_Permissions p = new  Edge.Security.Manager.Lan_Accounts_Permissions();
                    Edge.Security.Model.Lan_Accounts_Permissions item = new Edge.Security.Model.Lan_Accounts_Permissions();
                    item.Description = strPermissionsDesc;
                    item.Lan = strLan;
                    item.PermissionID = PermissionsID;
                    p.Delete(item);
                    Logger.Instance.WriteOperationLog(this.PageName, "Delete Permission Language: " + item.Description + " and Language: " + item.Lan);

                    //保存日志
                    // SaveLogs("" + model.Title); 
                }
            }

            BindLanListByPermission();

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "PermissionLanAdmin.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
        }

	}
}
