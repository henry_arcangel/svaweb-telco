﻿<%@ Page Language="c#" CodeBehind="userPass.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.userPass" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>userPass</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                rules: {
                    txtOldPassword: "required",
                    txtPassword:
                    {
                        required: true

                    },
                    txtPassword1: {
                        required: true
                    
                    }
                },
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            jQuery.validator.addMethod("password", function (value, element) {
                if (value == null || value.length <= 0) return true;

                var password = $("#txtPassword").val();
                return password == $("#txtPassword1").val();
            }, jQuery.validator.messages.equalTo);
        });
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                用户名：
            </td>
            <td width="75%">
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                原密码：
            </td>
            <td>
                <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" CssClass="input"
                    Text="*"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                新密码：
            </td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                新密码确认：
            </td>
            <td>
                <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" SkinID="NoClass" CssClass="input password" Width="249px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Label ID="lblMsg" runat="server"></asp:Label></div>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" Text="重填" OnClick="btnCancel_Click" CssClass="submit">
        </asp:Button></div>
    <uc1:CheckRight ID="CheckRight1" runat="server"></uc1:CheckRight>
    </form>
</body>
</html>
