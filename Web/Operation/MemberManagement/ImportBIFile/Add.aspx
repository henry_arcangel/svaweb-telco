﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.ImportBIFile.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
        function loading() {
            if (confirm("Are You Sure ?")) {
                window.top.tb_show();
                return true;
            }
            return false;
        }
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                交易编号：
            </td>
            <td width="75%">
                <asp:Label ID="CouponDispenseNumber" runat="server"  MaxLength="512"
                   ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易状态：
            </td>
            <td>
    
               <asp:Label ID="lblApproveStatus" runat="server" ></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
                
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server" 
                   ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn"  runat="server"
                    ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedByName" runat="server"
                    ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="Note" TabIndex="6" runat="server" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                导入文件：
            </td>
            <td>
                <ufb:UploadFileBox ID="ImportFile" runat="server" FileType="files" SubSaveFilePath="Files/BatchImport" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="保 存" OnClick="btnAdd_Click" CssClass="submit"
                        OnClientClick="return loading();"></asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />

    </form>
</body>
</html>
