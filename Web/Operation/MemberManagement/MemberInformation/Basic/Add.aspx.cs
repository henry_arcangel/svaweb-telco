﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3","EducationCode");
                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3","ProfessionCode");
                ds = new Edge.SVA.BLL.Nation().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3","NationCode");
               
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Edge.SVA.Model.Member item = this.GetAddObject();

            if (item != null)
            {
                item.MemberPicture = Session["TempImage"] as byte[];
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = null;
                item.UpdatedOn = null;
            }
            int id = DALTool.Add<Edge.SVA.BLL.Member>(item);
            if (id > 0)
            {
               
               // JscriptPrint(Resources.MessageTips.AddSuccess, "Modify.aspx?id=" + id, "Success");
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
            Session["TempImage"] = null;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Session["TempImage"] = this.MemberPictures.FileBytes;
            Random r = new Random();
            this.Image1.ImageUrl = "~/TempImage.aspx?s=" + r.Next();
            this.Image1.Visible = true;
        }

  
    }
}
