﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others.OnlineShippingAddress
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberAddress, Edge.SVA.Model.MemberAddress>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            Edge.SVA.Model.MemberAddress item = this.GetUpdateObject();
            if (item != null)
            {
                if (this.MemberFirstAddr.SelectedValue == "1")
                {
                    int isHas = new Edge.SVA.BLL.MemberAddress().GetCount("MemberFirstAddr=1 and MemberID=" + item.MemberID);
                    if (isHas > 0)
                    {
                        this.lblMsg.Text = Resources.MessageTips.ExistDefaultAddress;
                        this.lblMsg.Visible = true;
                        return;
                    }
                }

                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;

            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.MemberAddress>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0&id=" + item.MemberID, Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0&id" + item.MemberID, Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
