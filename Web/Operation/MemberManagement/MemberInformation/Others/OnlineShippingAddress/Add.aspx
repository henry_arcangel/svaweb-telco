﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Others.OnlineShippingAddress.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：增加会员收货地址</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                增加会员收货地址
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                国家：
            </td>
            <td width="75%">
                <asp:TextBox ID="AddressCountry" TabIndex="1" runat="server" MaxLength="512" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                省（州）：
            </td>
            <td>
                <asp:TextBox ID="AddressProvince" TabIndex="2" runat="server" MaxLength="512" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                城市：
            </td>
            <td>
                <asp:TextBox ID="AddressCity" TabIndex="3" runat="server" MaxLength="512" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                详细地址：
            </td>
            <td>
                <asp:TextBox ID="AddressDetail" TabIndex="4" runat="server" MaxLength="512" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮编：
            </td>
            <td>
                <asp:TextBox ID="AddressZipCode" TabIndex="5" runat="server" MaxLength="512" CssClass="input digits required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:TextBox ID="Contact" TabIndex="6" runat="server" MaxLength="512" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:TextBox ID="ContactPhone" TabIndex="7" runat="server" MaxLength="512" CssClass="input digits required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否会员默认地址：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberFirstAddr" TabIndex="8" runat="server" CssClass="required"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="0">否</asp:ListItem>
                    <asp:ListItem Value="1">是</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
