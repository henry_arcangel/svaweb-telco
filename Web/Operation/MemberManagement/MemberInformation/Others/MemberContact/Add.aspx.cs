﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others.MemberContact
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberSNSAccount, Edge.SVA.Model.MemberSNSAccount>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindSNSType(SNSTypeID);            
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.MemberSNSAccount item = this.GetAddObject();

            if (item != null)
            {
                item.MemberID = Edge.Web.Tools.ConvertTool.ToInt(Request.Params["memberid"]);
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }

            if (item.MemberID <= 0)
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "#", Resources.MessageTips.FAILED_TITLE);
                return;
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.MemberSNSAccount>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0&id=" + Request.Params["memberid"], Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0&id=" + Request.Params["memberid"], Resources.MessageTips.FAILED_TITLE);
            }

        }
    }
}
