﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponBatchCreate, Edge.SVA.Model.Ord_CouponBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.CouponStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CouponController.CouponStatus.Dormant);
            }
        }

        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(Model.CouponTypeID);
                this.CouponTypeID.Text = model == null ? "" : DALTool.GetStringByCulture(model.CouponTypeName1, model.CouponTypeName2, model.CouponTypeName3);
                this.CouponTypeID.Text = model == null ? "" : ControlTool.GetDropdownListText(this.CouponTypeID.Text, model.CouponTypeCode);

                Edge.SVA.Model.Campaign campaign = model == null || !model.CampaignID.HasValue ? null : new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());
                CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                CampaignID.Text = campaign == null ? "" : ControlTool.GetDropdownListText(CampaignID.Text, campaign.CampaignCode);

                if (Model.ApproveStatus == "A")
                {
                    this.txtBatchCouponID.Text = DALTool.GetBatchCode(Model.BatchCouponID, null);
                    this.btnExport.Visible = true;
                  
                }
                else
                {
                    this.txtBatchCouponID.Text = "";
                    this.btnExport.Visible = false;
                }

                this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.Value);
                this.CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.Value);
                this.ApproveStatus.Text = DALTool.GetApproveStatusString(this.ApproveStatus.Text);
                this.lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                this.ApproveBusDate.Text = Edge.Web.Tools.ConvertTool.ToStringDate(Model.ApproveBusDate.GetValueOrDefault());


                GetCreatedInfo(Model.CouponTypeID);
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            string fileName = "";
            int records = 0;


            Edge.SVA.BLL.Ord_CouponBatchCreate bll = new SVA.BLL.Ord_CouponBatchCreate();
            Edge.SVA.Model.Ord_CouponBatchCreate model = bll.GetModel(this.CouponCreateNumber.Text);
        
            if (model == null)
            {
                JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            try
            {
                fileName = bll.ExportCSV(model);
                if (string.IsNullOrEmpty(fileName))
                {
                    JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                records = model == null ? 0 : model.CouponCount;
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.ExportTool.ExportFile(fileName);

                Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
            }
            
            

        }


        private void GetCreatedInfo(int couponTypeID)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            this.CreatedCoupons.Text = bll.GetCount(string.Format("CouponTypeID = {0}", couponTypeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCoupons = 0;
            string lastCreatedCoupon = "";
            Controllers.CouponController.GetCreatedCouponInfo(couponTypeID, 0, ref remainCoupons, ref lastCreatedCoupon, ref msg);
            this.RemainCoupons.Text = remainCoupons.ToString("N00");
            this.LastCreatedCoupons.Text = lastCreatedCoupon;
        }
    }
}
