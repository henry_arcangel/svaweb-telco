﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.BatchImportCoupons.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right">
                交易编号：
            </td>
            <td>
                <asp:Label ID="ImportCouponNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                交易状态：
            </td>
            <td width="75%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易批核工作日期：
            </td>
            <td>
                <asp:Label ID="ApproveBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedByName" runat="server"></asp:Label>
                <asp:HiddenField ID="CreatedBy" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                批核时间：
            </td>
            <td>
                <asp:Label ID="ApproveOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="ApproveByName" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveBy" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                授权号：
            </td>
            <td>
                <asp:Label ID="ApprovalCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="ImportCouponDesc1" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th>
                        优惠劵类型编号
                    </th>
                    <th>
                        优惠劵类型
                    </th>
                    <th>
                        优惠劵批次编号
                    </th>
                    <th>
                        优惠劵物理编号
                    </th>
                    <th>
                        优惠劵编号
                    </th>
                    <th>
                        面额
                    </th>
                    <th>
                        状态
                    </th>
                    <th>
                        创建日期
                    </th>
                    <th>
                        有效日期
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <%#Eval("CouponTypeCode")%>
                </td>
                <td align="center">
                    <%#Eval("CouponTypeIDName")%>
                </td>
                <td align="center">
                    <%#Eval("BatchID")%>
                </td>
                <td align="center">
                    <%#Eval("CouponUID")%>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    <%#Eval("Denomination", "{0:N02}")%>
                </td>
                <td align="center">
                    <%#Eval("Status") %>
                </td>
                <td align="center">
                    <%#Eval("CreatedDate", "{0:yyyy-MM-dd}")%>
                </td>
                <td align="center">
                    <%#Eval("ExpiryDate", "{0:yyyy-MM-dd}")%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr">
        </div>
        <div style="margin-top: 10px; text-align: center;">
        </div>
        <div align="center">
            <asp:Button ID="btnSave" runat="server" Text="保 存" CssClass="submit" OnClick="btnSave_Click" />
            <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                class="submit" />
        </div>
        <uc2:checkright ID="Checkright1" runat="server" />
    </div>
    </form>
    <script type="text/javascript">
        
         $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__&id=<%=this.id %>"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
