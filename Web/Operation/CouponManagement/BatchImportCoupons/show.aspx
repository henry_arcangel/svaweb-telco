﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="show.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.BatchImportCoupons.show" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right">
                交易编号：
            </td>
            <td>
                <asp:Label ID="ImportCouponNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                交易状态：
            </td>
            <td width="75%">
                <asp:Label ID="ApproveStatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易批核工作日期：
            </td>
            <td>
                <asp:Label ID="ApproveBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核时间：
            </td>
            <td>
                <asp:Label ID="ApproveOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="ApproveBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                授权号：
            </td>
            <td>
                <asp:Label ID="ApprovalCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="ImportCouponDesc1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="10%">
                                    优惠劵类型编号
                                </th>
                                <th width="10%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    优惠劵批次编号
                                </th>
                                <th width="10%">
                                    优惠劵物理编号
                                </th>
                                <th width="10%">
                                    优惠劵编号
                                </th>
                                <th width="6%">
                                    面额
                                </th>
                                <th width="6%">
                                    状态
                                </th>
                                <th width="10%">
                                    创建日期
                                </th>
                                <th width="10%">
                                    有效日期
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#Eval("CouponTypeCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponTypeIDName")%>
                            </td>
                            <td align="center">
                                <%#Eval("BatchID")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponUID")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponNumber")%>
                            </td>
                            <td align="center">
                                <%#Eval("Denomination","{0:N02}")%>
                            </td>
                            <td align="center">
                                <%#Eval("Status") %>
                            </td>
                            <td align="center">
                                <%#Eval("CreatedDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td align="center">
                                <%#Eval("ExpiryDate", "{0:yyyy-MM-dd}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="clear" />
               <%-- <div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                        runat="server" id="dtTotal" visible="false">
                        <tr>
                            <td align="center">
                                汇总：
                            </td>
                            <td align="center">
                                面额汇总：
                                <asp:Label ID="lblTotalDenomination" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>--%>
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="clear" />
                <div align="center" style="width: 100%; text-align: center;">
                    <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="导 出" CssClass="submit" />
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <script type="text/javascript">

        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
