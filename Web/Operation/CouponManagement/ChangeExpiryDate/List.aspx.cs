﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.ChangeExpiryDate
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            if (!this.IsPostBack)
            {
                RptBind("", "CouponNumber");
            }
        }


        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
      
            //获得总条数
            this.pcount = bll.GetCount(strWhere);
     
            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);

            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            Tools.DataTool.AddCouponStatus(ds, "StatusName", "Status");

           

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
    }
}
