﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.ChangeExpiryDate
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.Coupon, Edge.SVA.Model.Coupon>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.Status.Items.Add(new ListItem() { Text = "--------", Value = "" });
                this.Status.Items.Add(new ListItem() { Text = "未被领用", Value = "0" });
                this.Status.Items.Add(new ListItem() { Text = "已被领取", Value = "1" });
                this.Status.Items.Add(new ListItem() { Text = "已被使用", Value = "2" });
                this.Status.Items.Add(new ListItem() { Text = "过期", Value = "3" });
                this.Status.Items.Add(new ListItem() { Text = "作废", Value = "4" });
                this.Status.Items.Add(new ListItem() { Text = "已被绑定", Value = "5" });
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CouponIssueDate.Text = Model.CouponIssueDate.ToString("yyyy-MM-dd");
                this.CouponExpiryDate.Text = Model.CouponExpiryDate.ToString("yyyy-MM-dd");
                this.CouponActiveDate.Text = Model.CouponActiveDate == null ? "" : Model.CouponActiveDate.Value.ToString("yyyy-MM-dd");

                this.CouponTypeID.Items.Add(new ListItem() { Text = DALTool.GetCouponTypeName(this.Model.CouponTypeID, null), Value = Model.CouponTypeID.ToString() });
            }
        }

    }
}
