﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponDelivery
{
    public partial class Void : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];

                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ";");

                string resultMsg = CouponOrderController.BatchVoidCoupon(idList, CouponOrderController.ApproveType.CouponDelivery);

                Logger.Instance.WriteOperationLog(this.PageName, "Void Coupon Order Delivery " + idList.ToString());

                JscriptPrint(resultMsg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
        }
    }
}