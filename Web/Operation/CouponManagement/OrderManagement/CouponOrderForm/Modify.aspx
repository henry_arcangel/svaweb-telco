﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );

            //检查客户类型
            $("input[name='CustomerType']").change(function () {
                checkOrderType();
                //清空记录
                $("#SendAddress,#ContactName,#ContactNumber").val("");
            });

            checkOrderType();
        });
        function checkDetail() {
            $("#StoreID,#ddlBrand,#CustomerID,#SendMethod,#SendAddress,#ContactName,#ContactNumber").removeClass("required");
            $("#brandDetail,#ddlCouponType,#txtOrderCount").addClass("required");
        }
        function checkAdd() {
            $("#SendMethod,#SendAddress,#ContactName,#ContactNumber").addClass("required");
            $("#brandDetail,#ddlCouponType,#txtOrderCount").removeClass("required");

            checkOrderType();
        }

        //检查客户类型
        function checkOrderType() {
            var type = $("input[name='CustomerType']:checked").val();
            if (type == '1') //客户
            {

                $('select#ddlBrand')[0].selectedIndex = 0;
                $('select#StoreID')[0].selectedIndex = 0; 

                $("#CustomerID").addClass("required");
                $("#StoreID,#ddlBrand").removeClass("required");

                $("#StoreID,#ddlBrand").attr("disabled", "disabled");
                $("#CustomerID").removeAttr("disabled");

            }
            else {//店铺
                $('select#CustomerID')[0].selectedIndex = 0; 

                $("#StoreID,#ddlBrand").addClass("required");
                $("#CustomerID").removeClass("required");

                $("#CustomerID").attr("disabled", "disabled");
                $("#StoreID,#ddlBrand").removeAttr("disabled");

            }
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="4" align="left">
                订单信息
            </th>
        </tr>
        <tr>
            <td width="15%" align="right">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CouponOrderFormNumber" runat="server" MaxLength="512"></asp:Label>
            </td>
            <td width="15%" align="right">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td colspan="3">
                <asp:Label ID="CreatedByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                订单内容
            </th>
        </tr>
        <tr>
            <td align="right" colspan="1">
                订单类型：
            </td>
            <td colspan="3">
                <asp:RadioButtonList ID="CustomerType" TabIndex="1" runat="server" RepeatLayout="Table"
                    RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Text="客户订货" Value="1"></asp:ListItem>
                    <asp:ListItem Text="店铺订货" Value="2" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货品牌：
            </td>
            <td>
                <asp:DropDownList ID="ddlBrand" runat="server" TabIndex="2" CssClass="dropdownlist"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="right">
                店铺：
            </td>
            <td>
                <asp:DropDownList ID="StoreID" runat="server" TabIndex="3" CssClass="dropdownlist required"
                    OnSelectedIndexChanged="StoreID_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货客户：
            </td>
            <td>
                <asp:DropDownList ID="CustomerID" runat="server" TabIndex="4" CssClass="dropdownlist"
                    OnSelectedIndexChanged="CustomerID_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="right">
                送货单发送方式：
            </td>
            <td>
                <asp:DropDownList ID="SendMethod" runat="server" CssClass="dropdownlist" TabIndex="5">
                    <asp:ListItem Text="直接交付（打印）" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="SMS" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Email" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Social Network" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货地址：
            </td>
            <td>
                <asp:TextBox ID="SendAddress" runat="server" TabIndex="6" CssClass="input"></asp:TextBox>
            </td>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:TextBox ID="ContactName" runat="server" TabIndex="7" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮件发送：
            </td>
            <td>
                <asp:TextBox ID="Email" runat="server" CssClass="input" TabIndex="8"></asp:TextBox>
            </td>
            <td align="right">
                SMS/MMS 发送：
            </td>
            <td>
                <asp:TextBox ID="SMSMMS" runat="server" CssClass="input" TabIndex="9"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:TextBox ID="ContactNumber" runat="server" CssClass="input" TabIndex="10"></asp:TextBox>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="Remark" runat="server" CssClass="input" TabIndex="11"></asp:TextBox>
            </td>
        </tr>
        <tr id="tranctionDetial">
            <th colspan="2" align="left">
                订单明细：
            </th>
            <th colspan="2" align="right">
                <asp:Button ID="btnAddDetail" runat="server" Text="添 加" OnClick="btnAddDetail_Click"
                    OnClientClick="checkDetail();" CssClass="submit" />
            </th>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:DropDownList ID="brandDetail" runat="server" TabIndex="12" AutoPostBack="true"
                    OnSelectedIndexChanged="brandDetail_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="right">
                优惠劵类型：
            </td>
            <td>
                <asp:DropDownList ID="ddlCouponType" runat="server" TabIndex="13">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货数量：
            </td>
            <td>
                <asp:TextBox ID="txtOrderCount" runat="server" CssClass="input svaQty" TabIndex="15"></asp:TextBox>
                <a name="brandDetail"></a>
            </td>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="15%">
                                    品牌编号
                                </th>
                                <th width="25%">
                                    品牌
                                </th>
                                <th width="15%">
                                    优惠劵类型编号
                                </th>
                                <th width="25%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    订货数量
                                </th>
                                <th width="10%">
                                    &nbsp;
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["BrandCode"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["BrandName"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["CouponTypeCode"]%>
                            </td>
                            <td align="center">
                                <%#((System.Data.DataRow)Container.DataItem)["CouponTypeName"]%>
                            </td>
                            <td align="center">
                                <%#int.Parse(((System.Data.DataRow)Container.DataItem)["CouponQty"].ToString()).ToString("N00")%>
                            </td>
                            <td>
                                <span class="btn_bg">
                                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="cancel" OnClick="btnDelete_Click"
                                        couponid='<%#((System.Data.DataRow)Container.DataItem)["ID"] %>' Text="删 除" />
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提 交" OnClick="btnUpdate_Click" CssClass="submit"
                        OnClientClick="checkAdd();"></asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
