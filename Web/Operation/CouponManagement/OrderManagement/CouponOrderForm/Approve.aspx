﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Approve.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm.Approve" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <th colspan="2" align="left">
                    <%=this.PageName %>
                </th>
            </tr>
            <tr>
                <td width="25%">
                    交易编号:
                </td>
                <td width="75%">
                    <asp:Label ID="TxnNo" runat="server" Text='<%#Eval("TxnNo") %>'></asp:Label>
                </td>
            </tr>
            <tr style="text-align: center; color: Red; font-size: large;">
                <td>
                    <%#Eval("ApprovalMsg")%>
                </td>
                <td>
                    <asp:Label ID="errorMsg" runat="server" Text='<%#Eval("ApproveCode") %>'></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div style="padding-bottom: 10px;">
        <div style="text-align: center;">
            <input type="button" value="关 闭" class="submit" onclick="javascript:window.top.tb_remove();parent.frames['sysMain'].location.href= 'List.aspx' " />
            <asp:Button ID="btnPrint" runat="server" Text="打印" CssClass="submit" OnClientClick="printdiv('div_print')" />
        </div>
    </div>
    <div id="div_print" runat="server">
        <!--startprint-->
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                    <tr>
                        <th colspan="4" align="center">
                            捡货列表
                        </th>
                    </tr>
                    <tr>
                        <td align="right" width="15%">
                            捡货单编号：
                        </td>
                        <td width="35%">
                            <%#Eval("CouponPickingNumber")%>
                        </td>
                        <td align="right">
                            打印时间：
                        </td>
                        <td>
                            <%#Eval("PrintDateTime")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            参考编号：
                        </td>
                        <td>
                            <%#Eval("ReferenceNo")%>
                        </td>
                        <td align="right" width="15%">
                            状态：
                        </td>
                        <td width="35%">
                            <%#Eval("ApproveStatus")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            捡货日期：
                        </td>
                        <td>
                            <%#Eval("PickingDate")%>
                        </td>
                        <td align="right">
                            捡货人：
                        </td>
                        <td>
                            <%#Eval("PickedBy")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                        <tr>
                                            <th width="10%">
                                                优惠劵类型编号
                                            </th>
                                            <th width="10%">
                                                优惠劵类型
                                            </th>
                                            <th width="10%">
                                                订单数量
                                            </th>
                                            <th width="10%">
                                                捡货数量
                                            </th>
                                            <th width="10%">
                                                优惠券起始编号
                                            </th>
                                            <th width="10%">
                                                优惠券结束编号
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblCouponTypeCode" runat="server" Text='<%#Eval("CouponTypeCode")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblCouponType" runat="server" Text='<%#Eval("CouponType")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("CouponQty")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                        </td>
                                        <td align="center">
                                        </td>
                                        <td align="center">
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="center">
                                        </td>
                                        <td align="center">
                                            总计：
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" colspan="3">
                                            <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;"></div>
            </ItemTemplate>
        </asp:Repeater> 
        <!--endprint-->
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
