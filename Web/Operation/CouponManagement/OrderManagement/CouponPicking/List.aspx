﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        function checkSelect(msg, url) {
            var link = url + "?ids=";
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            link += ids;
            link += "&height=460&amp;width=800&amp;TB_iframe=True&amp";

            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.:" + ids)) return false;

            window.top.tb_show("", link, "");
            $("#TB_title", parent.document).hide();

        }

        function hasSelect(msg) {
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.: " + ids)) return false;
            return true;
        }

        function checkPrintSelect(msg, url) {
            var link = url + "?id=";
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()));
                }
            });
            link += ids;
            link += "&height=460&amp;width=800&amp;TB_iframe=True&amp";

            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.:" + ids)) return false;
            window.top.tb_show("", link, "");
            // window.location.href = link;
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
        <HeaderTemplate>
            <table id="msgtablelist" width="100%" border="0" cellspacing="0" cellpadding="0"
                class="msgtablelist">
                <tr>
                    <th width="6%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                    <th width="10%">
                        交易编号
                    </th>
                    <th width="10%">
                        参考编号
                    </th>
                    <th width="5%">
                        交易状态
                    </th>
                    <th width="6%">
                        授权号
                    </th>
                    <th width="8%">
                        交易创建工作日期
                    </th>
                    <th width="8%">
                        交易批核工作日期
                    </th>
                    <th width="8%">
                        交易创建时间
                    </th>
                    <th width="6%">
                        创建人
                    </th>
                    <th width="8%">
                        批核时间
                    </th>
                    <th width="6%">
                        批核人
                    </th>
                    <th width="20%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall"  runat="server" />
                </td>
                <td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("CouponPickingNumber")%>'></asp:Label></a>
                </td>
                <td align="center">
                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Eval("ReferenceNo")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("ApproveStatusName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("ApprovalCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CreatedBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("ApproveBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedByName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveOn" runat="server" Text='<%#Eval("ApproveOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBy" runat="server" Text='<%#Eval("ApproveByName")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg">
                        <asp:LinkButton ID="lkbView" runat="server">查看</asp:LinkButton>
                        <asp:LinkButton ID="lbkEdit" runat="server">捡货回应</asp:LinkButton>
                        <asp:LinkButton ID="lbkReEdit" runat="server">重新捡货</asp:LinkButton>
                    </span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear" style="padding-top: 12px;" />
    <table width="100%">
        <tr>
            <td>
                <div class="left">
                    <span class="btn_bg"><a id="lbtnApprove" runat="server" href="#" onclick="return checkSelect();">
                        批核</a>
                        <asp:LinkButton ID="lbtnVoid" runat="server" OnClick="lbtnVoid_Click">作废</asp:LinkButton>
                        <asp:LinkButton ID="lbtnPrint" runat="server" OnClick="lbtnPrint_Click">打印捡货单</asp:LinkButton>
                        <%--<a id="lbtnPrint" runat="server" href="#" onclick="return checkPrintSelect();">打印捡货单</a>--%>
                    </span>
                </div>
            </td>
            <td class="right">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
