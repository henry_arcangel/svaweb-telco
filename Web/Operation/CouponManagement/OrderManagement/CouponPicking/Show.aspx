﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking.Show" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/BatchAutoComplete.ascx" TagName="batchAutoComplete"
    TagPrefix="bac" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="5" align="left">
                交易信息
            </th>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" width="15%">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CouponPickingNumber" runat="server"></asp:Label>
            </td>
            <td align="right" width="15%">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server" Text="P"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易批核工作日期：
            </td>
            <td>
                <asp:Label ID="ApproveBusDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                批核时间：
            </td>
            <td>
                <asp:Label ID="ApproveOn" runat="server"></asp:Label>
            </td>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="lblApproveBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                授权号：
            </td>
            <td>
                <asp:Label ID="ApprovalCode" runat="server"></asp:Label>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="Remark" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="5" align="left">
                捡货汇总列表
            </th>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Repeater ID="rptTotalList" runat="server" OnItemDataBound="rptTotalList_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="10%">
                                    优惠劵类型编号
                                </th>
                                <th width="10%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    订单数量
                                </th>
                                <th width="6%">
                                    &nbsp;
                                </th>
                                <th width="10%">
                                    捡货数量
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblSeq" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponTypeCode" runat="server" Text='<%#Eval("CouponTypeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponType" runat="server" Text='<%#Eval("CouponType")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td style="text-align: right;">
                                订单汇总：
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                捡货汇总：
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                            </td>
                        </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <th colspan="5" align="left">
                实际捡货结果
            </th>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="10%">
                                    优惠劵类型编号
                                </th>
                                <th width="10%">
                                    优惠劵类型
                                </th>
                                <%--    <th width="10%">
                                    优惠券级别编号
                                </th>
                                <th width="6%">
                                    优惠券级别
                                </th>--%>
                                <th width="10%">
                                    订单数量
                                </th>
                                <th width="10%">
                                    捡货数量
                                </th>
                                <th width="10%">
                                    优惠券起始编号
                                </th>
                                <th width="10%">
                                    优惠券结束编号
                                </th>
                                <th width="10%">
                                    优惠券批次编号
                                </th>
                                <th width="10%">
                                    捡货时间
                                </th>
                                <%--                                         <th width="10%">
                                    
                                </th>--%>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                             <td align="center">
                                <asp:Label ID="lblSeq" runat="server" Text=''></asp:Label>
                                <asp:HiddenField ID="hfCouponTypeID" runat="server" Value='<%#Eval("CouponTypeID") %>' />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponTypeCode" runat="server" Text='<%#Eval("CouponTypeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponType" runat="server" Text='<%#Eval("CouponType")%>'></asp:Label>
                            </td>
                            <%-- <td align="center">
                             <asp:Label ID="lblCouponGradeCode" runat="server" Text='<%#Eval("CouponGradeCode")%>'></asp:Label>   
                            </td>
                            <td align="center">
                            <asp:Label ID="lblCouponGrade" runat="server" Text='<%#Eval("CouponGrade")%>'></asp:Label>  
                            </td>--%>
                            <td align="center">
                                <asp:Label ID="lblOrderQTY" runat="server" Text='<%#Eval("OrderQTY")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickQTY" runat="server" Text='<%#Eval("PickQTY")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblFirstCouponNumber" runat="server" Text='<%#Eval("FirstCouponNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblEndCouponNumber" runat="server" Text='<%#Eval("EndCouponNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponBatchID" runat="server" Text='<%#Eval("BatchCouponCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPickupDateTime" runat="server" Text='<%#Eval("PickupDateTime","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                    runat="server" id="dtTotal">
                    <tr>
                        <td align="center">
                            汇总：
                        </td>
                        <td align="center">
                            订单汇总：
                            <asp:Label ID="lblTotalOrderQTY" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            捡货汇总：
                            <asp:Label ID="lblTotalPickQTY" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="clear" />
                <div align="center" style="width: 100%; text-align: center;">
                    <asp:Button ID="btnPrint" runat="server" Text="打印" CssClass="submit" OnClick="btnPrint_Click" />
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <script type="text/javascript">

        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
