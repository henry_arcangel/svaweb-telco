﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Approve : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                 ViewState["TotalOrderQTY"] = 0;
                 ViewState["TotalPickQTY"] = 0;

                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("ApproveCode", typeof(string));
                dt.Columns.Add("ApprovalMsg", typeof(string));

                DataTable orders = new DataTable();
                orders.Columns.Add("CouponPickingNumber", typeof(string));
                orders.Columns.Add("PrintDateTime", typeof(string));
                orders.Columns.Add("ReferenceNo", typeof(string));
                orders.Columns.Add("ApproveStatus", typeof(string));
                orders.Columns.Add("PickingDate", typeof(string));
                orders.Columns.Add("PickedBy", typeof(string));

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ";");

                bool isSuccess = false;
                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CouponPicking_H mode = new Edge.SVA.BLL.Ord_CouponPicking_H().GetModel(id);

                    DataRow dr = dt.NewRow();
                    dr["TxnNo"] = id;
                    dr["ApproveCode"] = CouponOrderController.ApproveForApproveCode(mode, out isSuccess);
                    if (isSuccess)
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Coupon Order Picking " + mode.CouponPickingNumber + " " + Resources.MessageTips.ApproveCode);

                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                        //打印预览
                        this.div_print.Visible = true;
                        this.btnPrint.Visible = true;

                        DataRow order = orders.NewRow();

                        order["CouponPickingNumber"] = mode.CouponPickingNumber;
                        order["PrintDateTime"] = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                        order["ReferenceNo"] = mode.ReferenceNo;
                        order["ApproveStatus"] = Tools.DALTool.GetOrderPickingApproveStatusString(mode.ApproveStatus);
                        order["PickingDate"] = Tools.ConvertTool.ToStringDate(mode.ApproveOn.GetValueOrDefault());
                        order["PickedBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());

                        orders.Rows.Add(order);

                        this.rptOrders.DataSource = orders;
                        this.rptOrders.DataBind();
                    }
                    else
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Coupon Order Picking " + mode.CouponPickingNumber + " " + Resources.MessageTips.ApproveError);

                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;

                        //打印预览
                        this.div_print.Visible = false;
                        this.btnPrint.Visible = false;
                    }
                    dt.Rows.Add(dr);                 
                }
                this.rptList.DataSource = dt;
                this.rptList.DataBind();

            }
        }


        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            //ViewState["CouponTypeCode"] = null;
            //ViewState["CouponType"] = null;
            //ViewState["OrderQTY"] = null;

            //Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

            //System.Data.DataSet ds = null;

            //ds = bll.GetList(strWhere);

            //Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            //Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");

            //this.rptPrintList.DataSource = ds.Tables[0].DefaultView;
            //this.rptPrintList.DataBind();
        }
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)e.Item.FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");

                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null)
                        {
                            ViewState["CouponType"] = lblCouponType.Text.Trim();
                        }
                        if (lblOrderQTY != null)
                        {
                            ViewState["OrderQTY"] = lblOrderQTY.Text.Trim();
                            //统计数量
                            ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                        }
                        ((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }

                Label lblPickQTY = (Label)e.Item.FindControl("lblPickQTY");
                if (lblPickQTY != null)
                {
                    //统计数量
                    ViewState["TotalPickQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblPickQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()).ToString();
                }
            
            }
            
        }


        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponPicking_D().GetList(string.Format("CouponPickingNumber = '{0}'", drv["CouponPickingNumber"].ToString()) + " order by CouponTypeID,KeyID");

                Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");

                list.DataSource = ds.Tables[0];
                list.DataBind();
            }
        }

        #endregion
    }
}