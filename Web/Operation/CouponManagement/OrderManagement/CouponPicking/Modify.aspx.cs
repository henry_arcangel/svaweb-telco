﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Modify : Tools.BasePage<SVA.BLL.Ord_CouponPicking_H, SVA.Model.Ord_CouponPicking_H>
    {
        private const string fields = "[KeyID],[CouponPickingNumber],[BrandID],[CouponTypeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCouponNumber],[EndCouponNumber],[BatchCouponCode],[PickupDateTime]";

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;
                RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
                RptTotalBind();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetOrderPickingApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }
                else
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }

                string couponTypeList = Controllers.CouponOrderController.GetOrderCouponType(Model.ReferenceNo);
                if (!string.IsNullOrEmpty(couponTypeList))
                {
                    string strWhre = " CouponTypeID in (" + couponTypeList + ")";
                    Edge.Web.Tools.ControlTool.BindCouponType(this.CouponTypeID, strWhre);
                }
                else
                {

                    Edge.Web.Tools.ControlTool.BindCouponType(this.CouponTypeID, " 1>2");
                }
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
        }


        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = e.Item.FindControl("lblCouponType") as Label;
                    Label lblOrderQTY = e.Item.FindControl("lblOrderQTY") as Label;
                    Label lblSeq = e.Item.FindControl("lblSeq") as Label;
                    HiddenField hfCouponTypeID = e.Item.FindControl("hfCouponTypeID") as HiddenField;

                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
                    }
                }


                //删除按钮添加提示
                LinkButton lkbDelete = (LinkButton)e.Item.FindControl("lkbDelete");
                if (lkbDelete != null)
                {
                    lkbDelete.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                }
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Edge.SVA.BLL.Ord_CouponPicking_D bll = new SVA.BLL.Ord_CouponPicking_D();
                Edge.SVA.Model.Ord_CouponPicking_D model = bll.GetModel(Tools.ConvertTool.ToInt(e.CommandArgument.ToString()));
                if (model == null) return;

                if (bll.Delete(Edge.Web.Tools.ConvertTool.ToInt(e.CommandArgument.ToString())))
                {
                    this.RecordCount = -1;
                    RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID", fields);
                }
                else
                {
                    JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
                }

                if (bll.GetCount(string.Format("CouponPickingNumber='{0}' and CouponTypeID = {1}", Request.Params["id"], model.CouponTypeID)) <= 0)
                {
                    model.PickQTY = 0;
                    model.FirstCouponNumber = null;
                    model.EndCouponNumber = null;
                    model.PickupDateTime = null;
                    model.BatchCouponCode = null;

                    if (bll.Add(model) > 0)
                    {
                        this.RecordCount = -1;
                        RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID", fields);
                    }
                    else
                    {
                        JscriptPrintAndFocus(Resources.MessageTips.DeleteFailed, "", Resources.MessageTips.FAILED_TITLE, this.btnAddItem.ClientID);
                    }
                }

                RptTotalBind();
            }

        }

        #endregion

        #region Other Event
        protected void rdb1_CheckedChanged(object sender, EventArgs e)
        {
            this.BatchCouponID.Enable = true;
            this.CouponCount.Enabled = false;
            this.CouponCount.Text = "";
            this.CouponNumberFirst.Enabled = false;
            this.CouponNumberFirst.Text = "";
            this.CouponUID.Enabled = false;
            this.CouponUID.Text = "";
        }

        protected void rdb2_CheckedChanged(object sender, EventArgs e)
        {
            this.BatchCouponID.Enable = false;
            this.BatchCouponID.Text = "";
            this.CouponCount.Enabled = true;
            this.CouponNumberFirst.Enabled = true;
            this.CouponUID.Enabled = false;
            this.CouponUID.Text = "";
        }

        protected void rdb3_CheckedChanged(object sender, EventArgs e)
        {
            this.BatchCouponID.Enable = false;
            this.BatchCouponID.Text = "";
            this.CouponCount.Enabled = false;
            this.CouponCount.Text = "";
            this.CouponNumberFirst.Enabled = false;
            this.CouponNumberFirst.Text = "";
            this.CouponUID.Enabled = true;
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            int userID = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            string couponPickingNumber = string.Empty;
            int couponTypeID = 0;
            int QTY = 0;
            int batchID = 0;
            string couponUID = string.Empty;
            string StartCouponNumber = string.Empty;
            int filterType = 0;

            if (string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            {
                JscriptPrintAndFocus(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE, this.CouponTypeID.ClientID);
                CloseLoading();
                return;
            }

            couponTypeID = ConvertTool.ToInt(this.CouponTypeID.SelectedValue);
            couponPickingNumber = this.CouponPickingNumber.Text.Trim();

            if (rdb1.Checked)
            {
                filterType = 1;
                batchID = this.BatchCouponID.Value;

                if (batchID <= 0)
                {
                    JscriptPrintAndFocus(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE, this.BatchCouponID.ClientID);
                    CloseLoading();
                    return;
                }
            }
            else if (rdb2.Checked)
            {
                filterType = 3;
                StartCouponNumber = this.CouponNumberFirst.Text.Trim();
                QTY = ConvertTool.ToInt(this.CouponCount.Text);

                if (String.IsNullOrEmpty(StartCouponNumber))
                {
                    JscriptPrintAndFocus(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE, this.CouponNumberFirst.ClientID);
                    CloseLoading();
                    return;
                }
            }
            else if (rdb3.Checked)
            {
                filterType = 2;
                couponUID = this.CouponUID.Text.Trim();

                if (String.IsNullOrEmpty(couponUID))
                {
                    JscriptPrintAndFocus(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE, this.CouponUID.ClientID);
                    CloseLoading();
                    return;
                }
            }
         
            
            if (Controllers.CouponOrderController.AddPickupDetailCoupon(userID, couponPickingNumber, couponTypeID, QTY, batchID, couponUID, StartCouponNumber, filterType) == 0)
            {

                string condition = string.Format("CouponPickingNumber = '{0}' and CouponTypeID = {1} and PickQTY = 0", couponPickingNumber, couponTypeID);
                if (new SVA.BLL.Ord_CouponPicking_D().GetCount(condition) == 1)
                {
                    System.Data.DataSet ds = new SVA.BLL.Ord_CouponPicking_D().GetList(condition);
                    new SVA.BLL.Ord_CouponPicking_D().Delete(Tools.ConvertTool.ConverType<int>(ds.Tables[0].Rows[0]["KeyID"].ToString()));
                }
               
                RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
                JscriptPrint(Resources.MessageTips.AddSuccess, "", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
            }
            CloseLoading();

            RptTotalBind();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponPicking_H item = this.GetUpdateObject();

            if (Request.Params["Status"] != null && Request.Params["Status"] == "R")
                item.ApproveStatus = "P";
            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponPicking_H>(item))
            {
                string msg = "";
                if (!Controllers.CouponOrderController.CanApprovePicked(item, out msg))
                {
                    //JscriptPrint(msg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                    JscriptPrint(msg, "", Resources.MessageTips.WARNING_TITLE);
                }
                else
                {
                    JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                }
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int couponTypeID = ConvertTool.ConverType<int>(this.CouponTypeID.SelectedValue);
            this.BatchCouponID.DataSource = Controllers.BatchController.GetInstance().GetBatch(10, couponTypeID);
        }
        #endregion

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;

            int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;

            int count = 0;
            ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
            this.RecordCount = count;

            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();
        }

        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.rptPager.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private Dictionary<int, int> CouponTypeIndex
        {
            get
            {
                if (ViewState["CouponTypeIndex"] == null)
                {
                    ViewState["CouponTypeIndex"] = new SVA.BLL.Ord_CouponPicking_D().GetCouponTypeIndex(Request.Params["id"]);
                }
                return ViewState["CouponTypeIndex"] as Dictionary<int, int>;
            }
        }
       
        #region 捡回汇总列表
        private void RptTotalBind()
        {
            
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

            System.Data.DataSet ds = bll.GetListGroupByCouponType(string.Format("CouponPickingNumber='{0}'", Request.Params["id"].Trim()));

            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddID(ds, "ID", 0, 0);

            this.rptTotalList.DataSource = ds.Tables[0].DefaultView;
            this.rptTotalList.DataBind();
        }

        protected void rptTotalList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.AlternatingItem) || (e.Item.ItemType == ListItemType.Item))
            {
                Label orderQTY = (Label)e.Item.FindControl("lblOrderQTY");
                Label pickQTY = (Label)e.Item.FindControl("lblPickQTY");

                long longOrderQTY = Tools.ConvertTool.ConverType<long>(orderQTY.Text);
                long longPickQTY = Tools.ConvertTool.ConverType<long>(pickQTY.Text);


                if (!Controllers.CouponOrderController.IsMeetPickingByType(longOrderQTY, longPickQTY))
                {
                    pickQTY.CssClass = "noMeetPicked";
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label TotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                Label TotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");

                //统计
                long totalOrderQTY = 0;
                long totalPickQTY = 0;
                Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
                if (TotalOrderQTY != null)
                {
                    TotalOrderQTY.Text = totalOrderQTY.ToString();
                }
                if (TotalPickQTY != null)
                {
                    TotalPickQTY.Text = totalPickQTY.ToString();
                }

                if (!Controllers.CouponOrderController.IsMeetPickingByType(totalOrderQTY, totalPickQTY))
                {
                    TotalPickQTY.CssClass = "noMeetPicked";
                }

            }
        }
        #endregion


    }
}