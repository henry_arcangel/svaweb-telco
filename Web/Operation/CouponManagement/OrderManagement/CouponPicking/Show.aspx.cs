﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_CouponPicking_H, SVA.Model.Ord_CouponPicking_H>
    {

        private const string fields = "[KeyID],[CouponPickingNumber],[BrandID],[CouponTypeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCouponNumber],[EndCouponNumber],[BatchCouponCode],[PickupDateTime]";

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;

                RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
                RptTotalBind();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;

                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetOrderPickingApproveStatusString(this.Model.ApproveStatus);

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                    this.btnPrint.Visible = false;
                }
                else
                {
                    this.btnPrint.Visible = false;
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;

            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //string CouponTypeCode = "";
            //string CouponType = "";
            //string OrderQTY = "";

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)e.Item.FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");
                    Label lblSeq = e.Item.FindControl("lblSeq") as Label;
                    HiddenField hfCouponTypeID = e.Item.FindControl("hfCouponTypeID") as HiddenField;
                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
                    }
                }
               
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Print.aspx?id={0}", Request.Params["id"]));
        }

        

        #endregion


        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.rptPager.PageSize, currentPage);
            }


            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();
        }

        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.rptPager.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private Dictionary<int, int> CouponTypeIndex
        {
            get
            {
                if (ViewState["CouponTypeIndex"] == null)
                {
                    ViewState["CouponTypeIndex"] = new SVA.BLL.Ord_CouponPicking_D().GetCouponTypeIndex(Request.Params["id"]);
                }
                return ViewState["CouponTypeIndex"] as Dictionary<int, int>;
            }
        }
        #region 捡回汇总列表
        private void RptTotalBind()
        {
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

            System.Data.DataSet ds = bll.GetListGroupByCouponType(string.Format("CouponPickingNumber='{0}'", Request.Params["id"].Trim()));

            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddID(ds, "ID", this.rptPager.PageSize, this.rptPager.CurrentPageIndex - 1);
            this.rptTotalList.DataSource = ds.Tables[0].DefaultView;
            this.rptTotalList.DataBind();

            ////统计
            //long totalOrderQTY = 0;
            //long totalPickQTY = 0;
            //Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            //lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            //lblTotalPickQTY.Text = totalPickQTY.ToString();
        }

        protected void rptTotalList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.AlternatingItem)||(e.Item.ItemType == ListItemType.Item))
            {
                Label orderQTY = (Label)e.Item.FindControl("lblOrderQTY");
                Label pickQTY = (Label)e.Item.FindControl("lblPickQTY");

                long longOrderQTY = Tools.ConvertTool.ConverType<long>(orderQTY.Text);
                long longPickQTY = Tools.ConvertTool.ConverType<long>(pickQTY.Text);


                if (!Controllers.CouponOrderController.IsMeetPickingByType(longOrderQTY, longPickQTY))
                {
                    pickQTY.CssClass = "noMeetPicked";
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label TotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                Label TotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");

                //统计
                long totalOrderQTY = 0;
                long totalPickQTY = 0;
                Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
                if (TotalOrderQTY != null)
                {
                    TotalOrderQTY.Text = totalOrderQTY.ToString();
                }
                if (TotalPickQTY != null)
                {
                    TotalPickQTY.Text = totalPickQTY.ToString();
                }

                if (!Controllers.CouponOrderController.IsMeetPickingByType(totalOrderQTY, totalPickQTY))
                {
                    TotalPickQTY.CssClass = "noMeetPicked";
                }

            }
        }
        #endregion
    }
}