﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;


namespace Edge.Web.Operation.CouponManagement.ChangeStatus
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Coupon,Edge.SVA.Model.Coupon>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                this.CouponIssueDate.Text = Model.CouponIssueDate.ToString("yyyy-MM-dd");
                this.CouponExpiryDate.Text = Model.CouponExpiryDate.ToString("yyyy-MM-dd");
                this.CouponActiveDate.Text = Model.CouponActiveDate == null ? "" : Model.CouponActiveDate.Value.ToString("yyyy-MM-dd");

                this.CouponTypeID.Items.Add(new ListItem() { Text = DALTool.GetCouponTypeName(this.Model.CouponTypeID, null), Value = Model.CouponTypeID.ToString() });
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            User currentUser = new Edge.Security.Manager.User(user);

            Edge.SVA.Model.Coupon model = this.GetUpdateObject();
            if (model == null)
            {
                JscriptPrint("更新失败！", "List.aspx?page=0", "Success");
                return;
            }
            model.Status = Edge.Web.Tools.ConvertTool.ToInt(this.Status.Text);

            model.UpdatedBy = currentUser.UserID;
            model.UpdatedOn = DateTime.Now;

            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            if (bll.Update(model))
            {
                JscriptPrint("更新成功！", "List.aspx?page=0", "Success");
            }
            else
            {
                JscriptPrint("更新失败！", "List.aspx?page=0", "Success");
            }
        }
    }
}
