﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeStatus.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：优惠劵查询</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                <tr>
                    <th width="6%">
                        优惠劵号码
                    </th>
                    <th width="6%">
                        优惠劵类型
                    </th>
                    <th width="10%">
                        优惠券发行日期
                    </th>
                    <th width="10%">
                        优惠券过期日期
                    </th>
                    <th width="15%">
                        优惠劵激活日期
                    </th>
                    <th width="10%">
                        优惠劵状态
                    </th>
                    <th width="10%">
                        主卡卡号
                    </th>
                    <th width="6%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("CouponNumber")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("CouponTypeName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName1" runat="server" Text='<%#Eval("CouponIssueDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName2" runat="server" Text='<%#Eval("CouponExpiryDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName3" runat="server" Text='<%#Eval("CouponActiveDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblUpdatedOn" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg"><a href="modify.aspx?id=<%#Eval("CouponNumber") %>">修改</a></span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr" runat="server" visible="false">
        </div>
        <uc2:checkright ID="Checkright1" runat="server" />
    </form>

    <script type="text/javascript">
        
          $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« 上一页",
            next_text: "下一页 »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtable tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtable tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

</body>
</html>
