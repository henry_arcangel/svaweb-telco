﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeManagement.CouponQuery.Show" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right">
                优惠劵编号：
            </td>
            <td>
                <asp:Label ID="CouponNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵物理编号：
            </td>
            <td>
                <asp:Label ID="CouponUID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵类型：
            </td>
            <td>
                <asp:Label ID="CouponTypeID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵批次编号：
            </td>
            <td>
                <asp:Label ID="BatchCouponID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                面额：
            </td>
            <td>
                <asp:Label ID="CouponTypeAmount" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                使用产品：
            </td>
            <td>
                <a class="thickbox btn_bg" href="ShowProduct.aspx?Url=&CouponTypeID=<%=this.couponTypetID%>&view=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                    查看</a>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                使用部门：
            </td>
            <td width="75%">
                <a class="thickbox btn_bg" href="ShowDeparment.aspx?Url=&CouponTypeID=<%=this.couponTypetID%>&view=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                    查看</a>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:Label ID="CouponIssueDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效日期：
            </td>
            <td>
                <asp:Label ID="CouponExpiryDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                优惠劵交易信息：
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="TXNList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                            id="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="6%">
                                    品牌编号
                                </th>
                                <th width="6%">
                                    店铺编号
                                </th>
                                <th width="6%">
                                    服务器编号
                                </th>
                                <th width="6%">
                                    收银机号
                                </th>
                                <th width="15%">
                                    交易类型
                                </th>
                                <th width="10%">
                                    交易编号
                                </th>
                                <th width="10%">
                                    业务日期
                                </th>
                                <th width="10%">
                                    交易日期
                                </th>
                                <th width="6%">
                                    原始金额
                                </th>
                                <th width="6%">
                                    交易金额
                                </th>
                                <th width="6%">
                                    剩余金额
                                </th>
                                <th width="6%">
                                    授权号
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#Eval("IDNumber")%>
                            </td>
                            <td align="center">
                                <%#Eval("BrandCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("StoreCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("ServerCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("RegisterCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("OprIDName")%>
                            </td>
                            <td align="center">
                                <%#Eval("RefTxnNo")%>
                            </td>
                            <td align="center">
                                <%#Eval("BusDate","{0:yyyy-MM-dd}")%>
                            </td>
                            <td align="center">
                                <%#Eval("Txndate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td align="center">
                                <%#Eval("OpenBal", "{0:N02}")%>
                            </td>
                            <td align="center">
                                <%#Eval("Amount", "{0:N02}")%>
                            </td>
                            <td align="center">
                                <%#Eval("CloseBal", "{0:N02}")%>
                            </td>
                            <td align="center">
                                <%#Eval("ApprovalCode")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="right">
                    <webdiyer:AspNetPager ID="TXNListPager" runat="server" CustomInfoTextAlign="Left"
                        FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="TXNListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="True" LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="javascript:history.go(-<%=PageCount %>)"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
