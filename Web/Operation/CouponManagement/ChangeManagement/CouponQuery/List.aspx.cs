﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponQuery
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        private const string fields = "CouponNumber,CouponTypeID,BatchCouponID,Status,CouponAmount,CreatedOn,CouponExpiryDate";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptListPager.PageSize = webset.ContentPageNum;

                ControlTool.BindCouponType(CouponTypeID);
                ControlTool.BindCouponStatus(Status);
                ControlTool.BindProduct(Products);
                ControlTool.BindDeparment(Deparment);

            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.RecordCount = -1;
            this.rptListPager.CurrentPageIndex = 1;
            if (!this.hasInput())
            {
                this.CloseLoading();
                JscriptPrint(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE);
                RptBind(null);
                return;
            }

            string strWhere = GetStrWhere();

            Logger.Instance.WriteOperationLog(this.PageName, "Coupon Enquiry");

            RptBind(strWhere);


        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            RptBind(GetStrWhere());

            this.AnimateRoll("msgtablelist");
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.AddTitle(this.CouponTypeID);

            if (string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            {
                ControlTool.BindProduct(Products);
                ControlTool.BindDeparment(Deparment);
                this.BatchCouponID.DataSource = Controllers.BatchController.GetInstance().GetBatch(10);
                return;
            }
            int couponTypeID = ConvertTool.ConverType<int>(this.CouponTypeID.SelectedValue);

            ControlTool.BindDeparment(Deparment, couponTypeID);
            ControlTool.BindProduct(Products, couponTypeID);

            this.BatchCouponID.DataSource = Controllers.BatchController.GetInstance().GetBatch(10, couponTypeID);
        }

        private void RptBind(string strWhere)
        {
            if (string.IsNullOrEmpty(strWhere))
            {
                this.rptList.DataSource = null; ;
                this.rptList.DataBind();
                this.rptListPager.RecordCount = 0;
                this.CloseLoading();
                return;
            }
            int currentPage = this.rptListPager.CurrentPageIndex < 1 ? 0 : this.rptListPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Coupon coupon = new Edge.SVA.BLL.Coupon();
            DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = coupon.GetListForTotal(this.rptListPager.PageSize, currentPage, strWhere, "CouponNumber", fields, 600);
                this.RecordCount = ds != null && ds.Tables.Count > 1 ? int.TryParse(ds.Tables[1].Rows[0]["Total"].ToString(), out count) ? count : 0 : 0;
            }
            else
            {
                ds = coupon.GetList(this.rptListPager.PageSize, currentPage, strWhere, "CouponNumber", fields, 600);
            }

            this.rptListPager.RecordCount = this.RecordCount < 0 ? 0 : this.RecordCount;

            if (ds == null || ds.Tables[0].DefaultView.Count <= 0)
            {
                JscriptPrintAndClose(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                this.rptList.DataSource = null;
                this.rptList.DataBind();
                this.rptListPager.RecordCount = 0;
                return;
            }

            DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            DataTool.AddCouponStatus(ds, "StatusName", "Status");
            DataTool.AddID(ds, "ID", this.rptListPager.PageSize, currentPage);
            DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
            DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");
            DataTool.AddCouponAmount(ds, "NewCouponAmount", "CouponAmount", "CouponTypeID");
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();

            this.CloseLoading();
        }

        private string GetStrWhere()
        {
            string strWhere = "";

            if (!string.IsNullOrEmpty(this.CouponNumber.Text))
            {
                strWhere += string.Format("CouponNumber = '{0}' and ", this.CouponNumber.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.CouponUID.Text))
            {
                strWhere += string.Format("CouponNumber in (select CouponNumber from CouponUIDMap where CouponUID =  '{0}') and ", this.CouponUID.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            {
                strWhere += string.Format("CouponTypeID = {0} and ", this.CouponTypeID.SelectedValue);
            }
            if (string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            {
                if (!string.IsNullOrEmpty(this.Products.SelectedValue) && !string.IsNullOrEmpty(this.Deparment.SelectedValue))
                {
                    strWhere += string.Format("CouponTypeID in ({0},{1}) and ", this.Products.SelectedValue, this.Deparment.SelectedValue);
                }
                else if (!string.IsNullOrEmpty(this.Products.SelectedValue))
                {
                    strWhere += string.Format("CouponTypeID in ({0}) and ", this.Products.SelectedValue);
                }
                else if (!string.IsNullOrEmpty(this.Deparment.SelectedValue))
                {
                    strWhere += string.Format("CouponTypeID in ({0}) and ", this.Deparment.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(this.BatchCouponID.Text))
            {
                strWhere += string.Format("BatchCouponID = {0} and ", this.BatchCouponID.Value);
            }

            if (!string.IsNullOrEmpty(this.CouponTypeAmount.Text))
            {
                strWhere += string.Format("CouponTypeID in (select CouponTypeID from CouponType where CouponTypeAmount =  '{0}') and ", this.CouponTypeAmount.Text.Replace(",", "").Trim());
            }
            if (!string.IsNullOrEmpty(this.Status.Text))
            {
                strWhere += string.Format("Status = '{0}' and ", this.Status.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.CouponCreatedOn.Text))
            {
                strWhere += string.Format("Convert(char(10),CreatedOn,120) = '{0}' and ", this.CouponCreatedOn.Text);
            }
            if (!string.IsNullOrEmpty(this.CouponExpiryDate.Text))
            {
                strWhere += string.Format("Convert(char(10),CouponExpiryDate,120) = '{0}' and ", this.CouponExpiryDate.Text);
            }
            if (!string.IsNullOrEmpty(strWhere)) strWhere = strWhere.Substring(0, strWhere.Length - 4);

            return strWhere;
        }

        private bool hasInput()
        {
            if (!string.IsNullOrEmpty(this.CouponNumber.Text.Trim())) return true;

            if (!string.IsNullOrEmpty(this.CouponUID.Text.Trim())) return true;

            if (!string.IsNullOrEmpty(this.CouponTypeID.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.BatchCouponID.Text)) return true;

            if (!string.IsNullOrEmpty(this.CouponTypeAmount.Text)) return true;

            if (!string.IsNullOrEmpty(this.Status.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.Products.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.Deparment.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.CouponCreatedOn.Text)) return true;

            if (!string.IsNullOrEmpty(this.CouponExpiryDate.Text)) return true;

            return false;
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                ViewState["RecordCount"] = value;
            }
        }
    }
}
