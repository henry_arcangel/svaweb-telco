﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using System.Text;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponRedeem
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponAdjust_H, Edge.SVA.Model.Ord_CouponAdjust_H>
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "CouponTypeID =-1");
                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
                Edge.Web.Tools.ControlTool.BindBrand(Brand);
                //ControlTool.BindBatchID(BatchCouponID);
                InitData();


                this.CouponStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CouponController.CouponStatus.Redeemed);
            }
        }

        private void InitData()
        {
            this.CouponAdjustNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.OrderCouponRedeemed);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Value);
            this.TxnDate.Text = DALTool.GetSystemDate();

            this.btnAddItem.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            this.btnDeleteItem.Visible = this.btnDeleteAllItem.Visible = this.rptAddList.Items.Count > 0 ? true : false;
            this.lblTxnAmount.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            this.txtActualTxnAmount.Visible = this.rptSearchList.Items.Count > 0 ? true : false; 

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponAdjust_H item = this.GetAddObject();

            if (item != null)
            {
                //item.CouponCreateNumber = DALTool.GetREFNOCode("BTHCOU");
                if (item.CouponAdjustNumber.Equals(string.Empty))
                {
                    JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
                    return;
                }
                item.BrandCode = Tools.DALTool.GetBrandCode(Tools.ConvertTool.ToInt(this.Brand.SelectedValue), null);//Add Brand Code
                item.StoreCode = Tools.DALTool.GetStoreCode(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), null);//Add Store Code
                item.OprID = Convert.ToInt32(Enum.Parse(typeof(CouponController.OprID), CouponController.OprID.Redeemed.ToString()));
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
            }
            //DataTable source = null;
            //if (this.cbSearchAll.Checked)
            //{
            //    source = this.GetSearchDataTable();
            //    if (source == null || source.Rows.Count <= 0)
            //    {
            //        JscriptPrint(Resources.MessageTips.SelectCoupons, "Add.aspx", "Error");
            //        return;
            //    }
            //}
            //else
            //{
                if (ViewState["AddResult"] == null || ((DataTable)ViewState["AddResult"]).Rows.Count <= 0)
                {
                    JscriptPrint(Resources.MessageTips.SelectCoupons, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
            //}

            int count = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CouponAdjust_H>(item);
            if (count > 0)
            {
                //if (this.cbSearchAll.Checked)
                //{

                //    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                //    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                //    database.SetExecuteTimeout(600);

                //    DataTable needInsertDt = database.GetTableSchema("Ord_CouponAdjust_D");
                //    foreach (DataRow drItem in source.Rows)
                //    {
                //        DataRow dr = needInsertDt.NewRow();
                //        dr["CouponAdjustNumber"] = this.CouponAdjustNumber.Text;
                //        dr["CouponNumber"] = drItem["CouponNumber"];
                //        needInsertDt.Rows.Add(dr);
                //    }
                //    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CouponAdjust_D");
                //    if (!es.Success)
                //    {

                //        JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", "Failed");
                //        return;
                //    }
                //}
                //else
                //{

                if (ViewState["AddResult"] != null)
                {
                    DataTable issuedDT = (DataTable)ViewState["AddResult"];
                    //foreach (DataRow row in issuedDT.Rows)
                    //{
                    //    Edge.SVA.Model.Ord_CouponAdjust_D model = new Edge.SVA.Model.Ord_CouponAdjust_D();
                    //    model.CouponNumber = row["CouponNumber"].ToString();
                    //    model.CouponAdjustNumber = this.CouponAdjustNumber.Text;

                    //    int s = new Edge.SVA.BLL.Ord_CouponAdjust_D().Add(model);
                    //}
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(600);

                    DataTable needInsertDt = database.GetTableSchema("Ord_CouponAdjust_D");
                    foreach (DataRow row in issuedDT.Rows)
                    {
                        DataRow dr = needInsertDt.NewRow();
                        dr["CouponAdjustNumber"] = this.CouponAdjustNumber.Text;
                        dr["CouponNumber"] = row["CouponNumber"];
                        dr["CouponAmount"] = row["ActualTxnAmount"];
                        needInsertDt.Rows.Add(dr);
                    }
                    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CouponAdjust_D");
                    if (!es.Success)
                    {
                        JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                        return;
                    }
                    //   ViewState["SelectedCouponIssued"] = ((DataSet)this.rptSearchList.DataSource).Tables[0].Clone();
                }
                // }
                Logger.Instance.WriteOperationLog(this.PageName, "Add Coupon Redeem  " + item.CouponAdjustNumber + " " + Resources.MessageTips.AddSuccess);


                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Coupon Redeem  " + item.CouponAdjustNumber + " " + Resources.MessageTips.AddFailed);

                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }


        private DataTable GetSearchDataTable()
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();

            int top = Edge.Web.Tools.ConvertTool.ToInt(CouponCount.Text.Trim());
            int batchCouponID = BatchCouponID.Value;
            string couponNumber = CouponNumber.Text.Trim();
            string couponTypeID = CouponTypeID.SelectedValue;
            string strWhere = string.Format(" Coupon.Status in ( {0} )", (int)CouponController.CouponStatus.Activated);
            string filedOrder = " Coupon.CouponNumber ASC ";

            if (string.IsNullOrEmpty(couponTypeID))
            {
                couponTypeID = Tools.DALTool.GetCouponTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 2);
            }

            strWhere = GetCouponSearchStrWhere(top, batchCouponID, couponNumber, couponTypeID, this.CouponUID.Text.Trim(), strWhere);

            //Display message
            int count = bll.GetCount(strWhere);

            if (count <= 0)
            {
                this.JscriptPrint(Messages.Manager.MessagesTool.instance.GetMessage("90180"), "", Resources.MessageTips.WARNING_TITLE);
                return null;
            }
            if ((top > webset.MaxSearchNum) || ((count > webset.MaxSearchNum) && top <= 0))
            {
                top = webset.MaxSearchNum;
                this.JscriptPrint(Resources.MessageTips.IsMaxSearchLimit, "", Resources.MessageTips.WARNING_TITLE);
            }

            DataSet ds = bll.GetListForBatchOperation(top, strWhere, filedOrder);

            //Edge.Web.Tools.DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
            //Edge.Web.Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            //Edge.Web.Tools.DataTool.AddCouponStatus(ds, "StatusName", "Status");
            //Tools.DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");
            return ds.Tables[0];
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int top = Edge.Web.Tools.ConvertTool.ToInt(CouponCount.Text.Trim());
            int batchCouponID = BatchCouponID.Value;
            string couponNumber = CouponNumber.Text.Trim();
            string couponTypeID = CouponTypeID.SelectedValue;

            if ((top <= 0) && (batchCouponID <= 0) && string.IsNullOrEmpty(couponNumber) && string.IsNullOrEmpty(couponTypeID) && string.IsNullOrEmpty(this.CouponUID.Text.Trim()))
            {
                JscriptPrintAndClose(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (top > webset.MaxSearchNum)
            {
                this.JscriptPrintAndClose(Resources.MessageTips.IsMaxSearchLimit, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }

            this.rptSearchListPager.CurrentPageIndex = 1;
            // this.page = 0;
            DataTable dt = GetSearchDataTable();
            //Add new column
            if (dt != null && dt.Rows.Count>0)
            {
                Tools.DataTool.AddColumn(dt, "ActualTxnAmount", 0.00);
                Tools.DataTool.AddColumn(dt, "ForfeitAmount", 0.00);
            }
            ViewState["SearchResult"] = dt;
            BindSearchList();
            this.CloseLoading();
        }

        protected void cbSearchAll_CheckedChanged(object sender, EventArgs e)
        {
            //if ((!this.cbSearchAll.Checked) && this.rptSearchList.Items.Count > 0)
            //{
            //    this.btnAddItem.Visible = true;
            //}
            //else
            //{
            //    this.btnAddItem.Visible = false;
            //}
            // this.lbtnDeleteIssued.Visible = !this.cbSearchAll.Checked;
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(this.CouponTypeID.SelectedValue))
            //{

            //    this.BatchCouponID.DataSource = Controllers.BatchController.GetInstance().GetBatch(10);
            //    return;
            //}
            //int couponTypeID = ConvertTool.ConverType<int>(this.CouponTypeID.SelectedValue);


            //this.BatchCouponID.DataSource = Controllers.BatchController.GetInstance().GetBatch(10, couponTypeID);
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            string ids = "";
            decimal inputAmount = 0;
            decimal couponAmount = 0;
            for (int i = 0; i < rptSearchList.Items.Count; i++)
            {
                string couponNumber = ((Label)rptSearchList.Items[i].FindControl("lblCouponNumber")).Text.Trim();
                CheckBox cb = (CheckBox)rptSearchList.Items[i].FindControl("cb_id");
                Label lbl = (Label)rptSearchList.Items[i].FindControl("lblCouponAmount");
                if (cb.Checked)
                {
                    inputAmount = Tools.ConvertTool.ToDecimal(this.txtActualTxnAmount.Text.Trim());
                    couponAmount = Tools.ConvertTool.ToDecimal(lbl.Text.Trim());
                    ids += string.Format("{0},", "'" + couponNumber + "'");
                }
            }
            ids = ids.TrimEnd(',');

            if (string.IsNullOrEmpty(ids))
            {
                JscriptPrint(Resources.MessageTips.NotSelected, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(this.txtActualTxnAmount.Text))
                {
                    JscriptPrint(Resources.MessageTips.EntryActualTxnAmount, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                if (inputAmount <= 0)
                {
                    JscriptPrint(Resources.MessageTips.EntryActualTxnAmount, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                if (inputAmount > couponAmount)
                {
                    JscriptPrint(Resources.MessageTips.EntryActualTxnAmountTooLarge, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }           
            }

            AddItem();

        }

        protected void btnDeleteItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnDeleteAllItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }

        protected void rptSearchListPager_PageChanged(object sender, EventArgs e)
        {
            BindSearchList();
        }

        protected void rptAddListPager_PageChanged(object sender, EventArgs e)
        {
            BindAddList();
        }

        //绑定搜索结果列表
        private void BindSearchList()
        {
            if (ViewState["SearchResult"] != null)
            {
                this.rptSearchListPager.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["SearchResult"];
                this.rptSearchListPager.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.rptSearchListPager.CurrentPageIndex, this.rptSearchListPager.PageSize);

                this.rptSearchList.DataSource = Tools.DALTool.GetCouponViewDataTable(viewDT);
                this.rptSearchList.DataBind();

                // this.rptSearchListPager.CustomInfoHTML = string.Format("Current:{0}/{1} Total:{2} Per page:{3}", new object[] { this.rptSearchListPager.CurrentPageIndex, this.rptSearchListPager.PageCount, this.rptSearchListPager.RecordCount, this.rptSearchListPager.PageSize });

            }
            else
            {
                this.rptSearchListPager.PageSize = webset.ContentPageNum;
                this.rptSearchListPager.RecordCount = 0;
                this.rptSearchList.DataSource = null;
                this.rptSearchList.DataBind();
            }

            this.btnAddItem.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            this.lblTxnAmount.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
            this.txtActualTxnAmount.Visible = this.rptSearchList.Items.Count > 0 ? true : false;
        }

        //绑定待添加结果列表
        private void BindAddList()
        {
            if (ViewState["AddResult"] != null)
            {
                this.rptAddListPager.PageSize = webset.ContentPageNum;
                DataTable dt = (DataTable)ViewState["AddResult"];
                //Summary 
                SummaryAmounts(dt);
                this.rptAddListPager.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.rptAddListPager.CurrentPageIndex, this.rptAddListPager.PageSize);
                this.rptAddList.DataSource = Tools.DALTool.GetCouponViewDataTable(viewDT);
                this.rptAddList.DataBind();


                // this.rptAddListPager.CustomInfoHTML = string.Format("Current:{0}/{1} Total:{2} Per page:{3}", new object[] { this.rptAddListPager.CurrentPageIndex, this.rptAddListPager.PageCount, this.rptAddListPager.RecordCount, this.rptAddListPager.PageSize });
            }
            else
            {
                this.rptAddListPager.PageSize = webset.ContentPageNum;
                this.rptAddListPager.RecordCount = 0;
                this.rptAddList.DataSource = null;
                this.rptAddList.DataBind();
            }

            this.btnDeleteItem.Visible = this.btnDeleteAllItem.Visible = this.rptAddList.Items.Count > 0 ? true : false;
        }

        private void AddItem()
        {
            if (ViewState["SearchResult"] != null)
            {
                if (ViewState["AddResult"] == null)
                {
                    ViewState["AddResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                }
                DataTable addDTView = (DataTable)ViewState["AddResult"];
                if (addDTView.DefaultView.Count >= webset.MaxShowNum)
                {
                    this.JscriptPrint(Resources.MessageTips.IsMaximumLimit, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                DataTable dtSearch = ((DataTable)ViewState["SearchResult"]).Clone();

                if (!cbSearchAll.Checked)
                {
                    string ids = "";
                    for (int i = 0; i < rptSearchList.Items.Count; i++)
                    {
                        string couponNumber = ((Label)rptSearchList.Items[i].FindControl("lblCouponNumber")).Text.Trim();
                        CheckBox cb = (CheckBox)rptSearchList.Items[i].FindControl("cb_id");
                        if (cb.Checked)
                        {
                            ids += string.Format("{0},", "'" + couponNumber + "'");
                        }
                    }
                    ids = ids.TrimEnd(',');

                    if (string.IsNullOrEmpty(ids.Trim()))
                    {
                        this.JscriptPrint(Resources.MessageTips.NotSelected, "", Resources.MessageTips.WARNING_TITLE);
                        return;
                    }

                    ViewState["ids"] = ids;

                    DataTable vsDT = (DataTable)ViewState["SearchResult"];
                    DataView dvSearch = vsDT.DefaultView;
                    dvSearch.RowFilter = "CouponNumber in (" + ids + ")";
                    dtSearch = dvSearch.ToTable();
                    foreach (DataRowView drv in dvSearch)
                    {
                        drv.Delete();
                    }
                    vsDT.AcceptChanges();

                    ViewState["SearchResult"] = vsDT;

                }
                else
                {
                    dtSearch = (DataTable)ViewState["SearchResult"];
                    ViewState["SearchResult"] = ((DataTable)ViewState["SearchResult"]).Clone();
                    cbSearchAll.Checked = false;
                }

                DataTable addDT = (DataTable)ViewState["AddResult"];
                DataTable newSearchDT = Edge.Web.Tools.ConvertTool.CombineTheSameDatatable2(addDT, dtSearch, "CouponNumber");
                //Add ActualTxnAmount
                AddActualTxnAmount(newSearchDT);
                this.txtActualTxnAmount.Text = string.Empty;
                ViewState["AddResult"] = newSearchDT;
                CheckBrandStore(newSearchDT);
                this.rptAddListPager.CurrentPageIndex = 1;
                this.rptSearchListPager.CurrentPageIndex = 1;
                BindAddList();
                BindSearchList();
            }
        }

        private void DeleteItem()
        {
            int checkCount = 0;
            for (int i = 0; i < rptAddList.Items.Count; i++)
            {
                string couponNumber = ((Label)this.rptAddList.Items[i].FindControl("lblCouponNumber")).Text.Trim();
                CheckBox cb = (CheckBox)rptAddList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    checkCount += 1;
                }
            }

            if (checkCount == 0)
            {
                JscriptPrint(Resources.MessageTips.NotSelected, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }


            if (ViewState["AddResult"] != null)
            {
                DataTable addDT = (DataTable)ViewState["AddResult"];

                for (int i = 0; i < rptAddList.Items.Count; i++)
                {
                    string couponNumber = ((Label)this.rptAddList.Items[i].FindControl("lblCouponNumber")).Text.Trim();
                    CheckBox cb = (CheckBox)rptAddList.Items[i].FindControl("cb_id");
                    if (cb.Checked)
                    {
                        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                        {
                            if (addDT.Rows[j]["CouponNumber"].ToString().Trim() == couponNumber)
                            {
                                addDT.Rows.Remove(addDT.Rows[j]);
                            }
                        }
                        addDT.AcceptChanges();
                    }
                }

                ViewState["AddResult"] = addDT;
                CheckBrandStore(addDT);
                this.rptAddListPager.CurrentPageIndex = 1;
                BindAddList();
            }
        }

        private void DeleteAllItem()
        {
            if (ViewState["AddResult"] != null)
            {
                DataTable addDT = ((DataTable)ViewState["AddResult"]).Clone();
                ViewState["AddResult"] = addDT;
                CheckBrandStore(addDT);
                this.rptAddListPager.CurrentPageIndex = 1;
                BindAddList();
            }
        }


        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
            InitCouponTypeByStore();
            InitSearchList();
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitCouponTypeByStore();
            InitSearchList();
        }

        private void CheckBrandStore(DataTable addDT)
        {
            if (addDT.Rows.Count > 0)
            {
                Brand.Enabled = false;
                StoreID.Enabled = false;
            }
            else
            {
                Brand.Enabled = true;
                StoreID.Enabled = true;
            }
        }

        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(StoreID, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }

        private void InitCouponTypeByStore()
        {
            if (!string.IsNullOrEmpty(this.StoreID.SelectedValue))
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "CouponTypeID in (" + Tools.DALTool.GetCouponTypeListByStoreIDBingding(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), 2) + ")");
            else
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, " 1>2");
        }

        private void InitSearchList()
        {
            ViewState["SearchResult"] = null;
            BindSearchList();

        }

        private void SummaryAmounts(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                dtTotal.Visible = true;

                for (int i = 0; i <= table.Rows.Count-1; i++)
                {
                    table.Rows[i]["ForfeitAmount"] = Tools.ConvertTool.ConverType<decimal>(table.Rows[i]["CouponAmount"].ToString()) - Tools.ConvertTool.ToDecimal(table.Rows[i]["ActualTxnAmount"].ToString());

                }
                table.AcceptChanges();

                decimal totalDenomination = Tools.ConvertTool.ToDecimal(table.Compute(" sum(CouponAmount) ", "").ToString());
                decimal totalActualTxnAmount =Tools.ConvertTool.ToDecimal(table.Compute(" sum(ActualTxnAmount) ", "").ToString());
                decimal totalForfeitAmount = Tools.ConvertTool.ToDecimal(table.Compute(" sum(ForfeitAmount) ", "").ToString());
                this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
                this.lblTotalActualTxnAmount.Text = totalActualTxnAmount.ToString("N2");
                this.lblTotalForfeitAmount.Text = totalForfeitAmount.ToString("N2");
            }
            else
            {

                dtTotal.Visible = false;
            }

        }

        private void AddActualTxnAmount(DataTable table)
        {
            if (ViewState["ids"] != null)
            {
                string[] idsList = ViewState["ids"].ToString().Split(',');
                foreach(string strID in idsList)
                {
                    for (int i = 0; i <= table.Rows.Count - 1; i++)
                    {
                        if (strID.Replace("'","").Trim()== table.Rows[i]["CouponNumber"].ToString().Trim())
                        {
                            decimal inputAmount = Tools.ConvertTool.ConverType<decimal>(this.txtActualTxnAmount.Text.Trim());
                           table.Rows[i]["ActualTxnAmount"] = inputAmount;
                        }
                    }
                    table.AcceptChanges();
                }
            }
        }

    }
}
