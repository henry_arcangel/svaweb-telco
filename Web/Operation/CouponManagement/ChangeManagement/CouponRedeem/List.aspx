﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeManagement.CouponRedeem.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

    <script type="text/javascript">
        function checkSelect(msg, url) {
            var link = url + "?ids=";
            var ids = "";
            $("#msgtablelist tr").each(function(trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            link += ids;
            link += "&height=460&amp;width=800&amp;TB_iframe=True&amp";

            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.:" + ids)) return false;

            window.top.tb_show("", link, "");
            $("#TB_title", parent.document).hide();

        }
        function hasSelect(msg) {
            var ids = "";
            $("#msgtablelist tr").each(function (trindex, tritem) {
                if ($(tritem).find("td input").attr("checked")) {
                    ids += ($.trim($(tritem).find("td:eq(1)").text()) + ";");
                }
            });
            if (ids.length <= 0) {
                parent.jAlert("Please Select Item", "Warning Message.");
                return false;
            }
            if (!confirm(msg + "\n TXN NO.: " + ids)) return false;
            return true;
        }
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound">
        <HeaderTemplate>
            <table id="msgtablelist" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="6%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                    <th width="10%">
                        交易编号
                    </th>
                    <th width="6%">
                        交易状态
                    </th>
                    <th width="6%">
                        授权号
                    </th>
                    <th width="10%">
                        交易创建工作日期
                    </th>
                    <th width="10%">
                        交易批核工作日期
                    </th>
                    <th width="12%">
                        交易创建时间
                    </th>
                    <th width="6%">
                        创建人
                    </th>
                    <th width="10%">
                        批核时间
                    </th>
                    <th width="6%">
                        批核人
                    </th>
                    <th width="10%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                </td>
                <td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("CouponAdjustNumber")%>'></asp:Label></a>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveStatus" runat="server" Text='<%#Eval("ApproveStatusName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveCode" runat="server" Text='<%#Eval("ApprovalCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedBusDate" runat="server" Text='<%#Eval("CreatedBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBusDate" runat="server" Text='<%#Eval("ApproveBusDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreateOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreateBy" runat="server" Text='<%#Eval("CreatedName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveOn" runat="server" Text='<%#Eval("ApproveOn","{0:yyyy-MM-dd HH:mm:ss}")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblApproveBy" runat="server" Text='<%#Eval("ApproveName")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg">
                        <asp:LinkButton ID="lkbView" CommandName="V" runat="server">查看</asp:LinkButton>
                        <asp:LinkButton ID="lbkEdit" CommandName="E" runat="server">修改</asp:LinkButton>
                    </span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr">
        </div>
        <div class="left">
            <span class="btn_bg">
                <asp:LinkButton ID="lbtnAdd" runat="server" OnClick="lbtnAdd_Click">添加</asp:LinkButton>
                 <a id="lbtnApprove" runat="server" href="#" onclick="return checkSelect();">批核</a>
                <asp:LinkButton ID="lbtnVoid" runat="server" OnClick="lbtnVoid_Click">作废</asp:LinkButton>
            </span>
        </div>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>

    <script type="text/javascript">
        
         $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

</body>
</html>
