﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponRedeem
{
    public partial class Approve : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("ApproveCode", typeof(string));
                dt.Columns.Add("ApprovalMsg", typeof(string));

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ";");

                bool isSuccess = false;
                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CouponAdjust_H mode = new Edge.SVA.BLL.Ord_CouponAdjust_H().GetModel(id);

                    DataRow dr = dt.NewRow();
                    dr["TxnNo"] = id;
                    dr["ApproveCode"] = CouponController.ApproveCouponForApproveCode(mode, CouponController.OprID.Redeemed,out isSuccess);
                    if (isSuccess)
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Coupon Redeem " + mode.CouponAdjustNumber + " " + Resources.MessageTips.ApproveCode);

                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                    }
                    else
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Coupon Redeem " + mode.CouponAdjustNumber + " " + Resources.MessageTips.ApproveError);

                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                    }
                    dt.Rows.Add(dr);
                }
                this.rptList.DataSource = dt;
                this.rptList.DataBind();

            }
        }
    }
}
