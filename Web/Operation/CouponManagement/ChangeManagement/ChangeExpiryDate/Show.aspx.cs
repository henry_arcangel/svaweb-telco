﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Common;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.ChangeExpiryDate
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponAdjust_H, Edge.SVA.Model.Ord_CouponAdjust_H>
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        public string id = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.id = Request.Params["id"];
            this.pagesize = webset.ContentPageNum;

            if (!this.IsPostBack)
            {
                //Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                //string strWhere = string.Format("CouponAdjustNumber = '{0}'", WebCommon.No_SqlHack(Model.CouponAdjustNumber));

                //RptBind(strWhere);

                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(this.ApproveStatus.Text);

                SVA.Model.Reason reason = new SVA.BLL.Reason().GetModel(Model.ReasonID.GetValueOrDefault());
                this.ReasonID.Text = reason == null ? "" : DALTool.GetStringByCulture(reason.ReasonDesc1, reason.ReasonDesc2, reason.ReasonDesc3);
                this.ReasonID.Text = reason == null ? "" : ControlTool.GetDropdownListText(ReasonID.Text, reason.ReasonCode);

                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                string strWhere = string.Format("Ord_CouponAdjust_D.CouponAdjustNumber = '{0}'", WebCommon.No_SqlHack(Model.CouponAdjustNumber));

                if (Model.ApproveStatus.ToUpper().Trim() == "A") strWhere = string.Format(" Coupon_Movement.RefTxnNo ='{0}' and Coupon_Movement.OprID = '{1}' ", WebCommon.No_SqlHack(Model.CouponAdjustNumber), WebCommon.No_SqlHack(Model.OprID.ToString()));

                RptBind(strWhere, Model.ApproveStatus);


                //汇总金额
                Edge.SVA.BLL.Ord_CouponAdjust_D bll = new SVA.BLL.Ord_CouponAdjust_D();
                this.dtTotal.Visible = true;
                if (Model.ApproveStatus.ToUpper().Trim() == "A")
                {
                    this.lblTotalDenomination.Text = bll.GetAllDenominationWithCoupon_Movement(strWhere).ToString("N02");
                }
                else
                {
                    this.lblTotalDenomination.Text = bll.GetAllDenominationWithCoupon(strWhere).ToString("N02");
                }
            }
        }

        //private void RptBind(string strWhere)
        //{
        //    if (!int.TryParse(Request.Params["page"], out this.page))
        //    {
        //        this.page = 0;
        //    }

        //    Edge.SVA.BLL.Ord_CouponAdjust_D bll = new Edge.SVA.BLL.Ord_CouponAdjust_D();
        //    this.pcount = bll.GetCountWithCoupon(strWhere);
        //    DataSet ds = bll.GetPageListWithCoupon(this.pagesize, this.page, strWhere, "Ord_CouponAdjust_D.CouponNumber");

        //    DataTool.AddCouponStatus(ds, "StatusName", "Status");
        //    DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}

        private void RptBind(string strWhere, string status)
        {
            if (!int.TryParse(Request.Params["page"], out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Ord_CouponAdjust_D bll = new Edge.SVA.BLL.Ord_CouponAdjust_D();

            if (status.ToUpper().Trim() == "A")
            {
                this.pcount = bll.GetCountWithCoupon_Movement(strWhere);

                DataSet ds = bll.GetPageListWithCoupon_Movement(this.pagesize, this.page, strWhere, "Coupon_Movement.CouponNumber");

                DataTool.AddCouponStatus(ds, "StatusName", "Status");
                DataTool.AddCouponStatus(ds, "OrgStatusName", "OrgStatus");
                DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
                DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
                DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");

                this.rptList.DataSource = ds.Tables[0].DefaultView;
                this.rptList.DataBind();
            }
            else
            {
                this.pcount = bll.GetCountWithCoupon(strWhere);

                DataSet ds = bll.GetPageListWithCoupon(this.pagesize, this.page, strWhere, "Ord_CouponAdjust_D.CouponNumber");

                DataTool.AddCouponStatus(ds, "StatusName", "Status");
                DataTool.AddCouponStatus(ds, "OrgStatusName", "Status");
                DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
                DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
                DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");

                this.rptList.DataSource = ds.Tables[0].DefaultView;
                this.rptList.DataBind();

            }
        }
    }
}
