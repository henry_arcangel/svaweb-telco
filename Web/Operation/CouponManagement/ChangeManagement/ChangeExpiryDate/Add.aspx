﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeManagement.ChangeExpiryDate.Add" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/BatchAutoComplete.ascx" TagName="batchAutoComplete"
    TagPrefix="bac" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

        //全选取消按钮函数，调用样式如：
        function checkAll2(chkobj) {
            if ($(chkobj).attr("checked") == true) {
                $(".checkall2 input").attr("checked", true);
                $(".checkall2 input[disabled=disabled]").attr("checked", false);

            } else {
                $(".checkall2 input").attr("checked", false);
            }
        }
        
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="4" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right" width="15%">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CouponAdjustNumber" runat="server"></asp:Label>
            </td>
            <td align="right" width="15%">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="Note" TabIndex="1" runat="server" MaxLength="512" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                修改选项
            </th>
        </tr>
        <tr>
            <td align="right">
                有效日期：
            </td>
            <td>
                <asp:TextBox ID="ActExpireDate" runat="server" MaxLength="512" CssClass="input required"
                    onfocus="WdatePicker()"></asp:TextBox>
                <span class="star">*</span>
            </td>
            <td align="right">
                原因：
            </td>
            <td>
                <asp:DropDownList ID="ReasonID" TabIndex="2" runat="server" CssClass="dropdownlist required cancel">
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <th align="left" colspan="3">
                筛选条件
            </th>
            <th align="right">
                <asp:Button ID="btnSearch" runat="server" Text="搜 索" CssClass="submit cancel" OnClick="btnSearch_Click"
                    OnClientClick="javascript:window.top.tb_show();"></asp:Button>
            </th>
        </tr>
        <tr>
            <td align="right">
                优惠券类型：
            </td>
            <td>
                <asp:DropDownList ID="CouponTypeID" TabIndex="2" runat="server" CssClass="dropdownlist cancel"
                    AutoPostBack="true" OnSelectedIndexChanged="CouponTypeID_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="right">
                优惠劵批次编号：
            </td>
            <td>
                <bac:batchAutoComplete ID="BatchCouponID" runat="server" CoupontTypeClientID="CouponTypeID" />
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠券数量：
            </td>
            <td>
                <asp:TextBox ID="CouponCount" TabIndex="4" runat="server" MaxLength="9" CssClass="input digits"></asp:TextBox>
            </td>
            <td align="right">
                第一张优惠券号码：
            </td>
            <td>
                <asp:TextBox ID="CouponNumber" TabIndex="4" runat="server" MaxLength="20" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵物理编号：
            </td>
            <td>
                <asp:TextBox ID="CouponUID" runat="server" MaxLength="21"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <th align="left" colspan="3">
                优惠券搜索结果列表
            </th>
            <th align="right">
                <asp:CheckBox ID="cbSearchAll" runat="server" Text="添加所有搜索结果" CssClass="cancel" OnCheckedChanged="cbSearchAll_CheckedChanged"
                    onclick="checkAllSearch(this);" />
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptSearchList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    <input type="checkbox" class="checkall" onclick="checkAll(this);" />选择
                                </th>
                                <th width="10%">
                                    优惠券号码
                                </th>
                                <th width="10%">
                                    优惠券物理编号
                                </th>
                                <th width="20%">
                                    优惠券类型
                                </th>
                                <th width="10%">
                                    批次号
                                </th>
                                <th width="10%">
                                    面额
                                </th>
                                <th width="8%">
                                    状态
                                </th>
                                <th width="10%">
                                    创建日期
                                </th>
                                <th width="10%">
                                    有效日期
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponNumber" runat="server" Text='<%#Eval("CouponNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponUID" runat="server" Text='<%#Eval("CouponUID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponType" runat="server" Text='<%#Eval("CouponType")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBatchCouponID" runat="server" Text='<%#Eval("BatchCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponAmount" runat="server" Text='<%#Eval("CouponAmount","{0:0.00}")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd}")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblExpiryDate" runat="server" Text='<%#Eval("CouponExpiryDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div>
                    <div class="left">
                        <asp:Button ID="btnAddItem" runat="server" Text="添加" CssClass="submit cancel" OnClick="btnAddItem_Click" />
                    </div>
                    <div class="right">
                        <webdiyer:AspNetPager ID="rptSearchListPager" runat="server" CustomInfoTextAlign="Left"
                            FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                            LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                            PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" TextAfterPageIndexBox=""
                            TextBeforePageIndexBox="" OnPageChanged="rptSearchListPager_PageChanged" CssClass="asppager"
                            CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                            CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="True" LayoutType="Table">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                待添加优惠券列表
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptAddList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    <input type="checkbox" onclick="checkAll2(this);" />选择
                                </th>
                                <th width="10%">
                                    优惠券号码
                                </th>
                                <th width="10%">
                                    优惠券物理编号
                                </th>
                                <th width="20%">
                                    优惠券类型
                                </th>
                                <th width="10%">
                                    批次号
                                </th>
                                <th width="10%">
                                    面额
                                </th>
                                <th width="8%">
                                    状态
                                </th>
                                <th width="10%">
                                    创建日期
                                </th>
                                <th width="10%">
                                    有效日期
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall2" runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponNumber" runat="server" Text='<%#Eval("CouponNumber")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponUID" runat="server" Text='<%#Eval("CouponUID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponType" runat="server" Text='<%#Eval("CouponType")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBatchCouponID" runat="server" Text='<%#Eval("BatchCouponID","{0:D12}")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCouponAmount" runat="server" Text='<%#Eval("CouponAmount","{0:0.00}")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd}")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblExpiryDate" runat="server" Text='<%#Eval("CouponExpiryDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div>
                    <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist"
                            runat="server" id="dtTotal" visible="false">
                            <tr>
                                <td align="center">
                                    汇总：
                                </td>
                                <td align="center">
                                    面额汇总：
                                    <asp:Label ID="lblTotalDenomination" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="left">
                        <asp:Button ID="btnDeleteItem" runat="server" Text="删除" CssClass="submit cancel "
                            OnClick="btnDeleteItem_Click" />
                        <asp:Button ID="btnDeleteAllItem" runat="server" Text="清空" CssClass="submit cancel "
                            OnClick="btnDeleteAllItem_Click" />
                    </div>
                    <div class="right">
                        <webdiyer:AspNetPager ID="rptAddListPager" runat="server" CustomInfoTextAlign="Left"
                            FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                            LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                            PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" TextAfterPageIndexBox=""
                            TextBeforePageIndexBox="" OnPageChanged="rptAddListPager_PageChanged" CssClass="asppager"
                            CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount%"
                            CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="True" LayoutType="Table">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit"
                        OnClientClick="return confirm( 'Are you sure? ');"></asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
