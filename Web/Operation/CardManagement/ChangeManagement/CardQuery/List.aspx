﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery.List" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/BatchAutoComplete.ascx" TagName="batchAutoComplete"
    TagPrefix="bac" %>
<%@ Register Src="../../../../Controls/CardBatchIDAutoComplete.ascx" TagName="CardBatchIDAutoComplete"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });


        });
        function check() {

            var regAmount = /^(\d{1,2},){0,1}(\d{1,3},){0,1}\d{1,3}(\.\d{1,2})?$/gi;
            var regPoint = /^(\d{1,2},){0,1}(\d{1,3},){0,1}\d{1,3}$/gi;
            var regDigits = /^\d+$/gi;

            var amount = $("#Amount").val();
            var point = $("#Point").val();
            var cardNumber = $("#CardNumber").val();
            var cardUID = $("#CardUID").val();

            if (amount.length > 0 && !regAmount.exec(amount)) return false;
            if (point.length > 0 && !regPoint.exec(point)) return false;
            if (cardNumber.length > 0 && !regDigits.exec(cardNumber)) return false;
            if (cardUID.length > 0 && !regDigits.exec(cardUID)) return false;

            window.top.tb_show();
            return true;
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th align="left">
                <%=this.PageName %>
            </th>
            <th align="right">
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" CssClass="submit"
                    OnClientClick="check();" />
            </th>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:DropDownList ID="CardTypeID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡级别：
            </td>
            <td>
                <asp:DropDownList ID="CardGradeID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CardGradeID_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡批次编号：
            </td>
            <td>
                <uc1:CardBatchIDAutoComplete ID="CardBatchID" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                卡号：
            </td>
            <td>
                <asp:TextBox ID="CardNumber" runat="server" MaxLength="20" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡物理编号：
            </td>
            <td>
                <asp:TextBox ID="CardUID" runat="server" MaxLength="21" CssClass="input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:DropDownList ID="Status" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡初始金额：
            </td>
            <td>
                <asp:TextBox ID="Amount" runat="server" CssClass="input svaAmount"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡初始积分：
            </td>
            <td width="75%">
                <asp:TextBox ID="Point" runat="server" CssClass="input svaPoint"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:TextBox ID="CreatedOn" runat="server" onfocus="WdatePicker()" CssClass="input dateISO"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效日期：
            </td>
            <td>
                <asp:TextBox ID="ExpiryDate" runat="server" onfocus="WdatePicker()" CssClass="input dateISO"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="5%">
                        序号
                    </th>
                    <th width="15%">
                        卡号码
                    </th>
                    <th width="10%">
                        卡物理编号
                    </th>
                    <th width="10%">
                        卡类型
                    </th>
                    <th width="10%">
                        卡级别
                    </th>
                    <th width="10%">
                        批次编号
                    </th>
                    <th width="5%">
                        状态
                    </th>
                    <th width="5%">
                        金额
                    </th>
                    <th width="5%">
                        积分
                    </th>
                    <th width="10%">
                        创建日期
                    </th>
                    <th width="10%">
                        有效日期
                    </th>
                    <th width="10%">
                        &nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <%#Eval("ID")%>
                </td>
                <td align="center">
                    <%#Eval("CardNumber")%>
                </td>
                <td align="center">
                    <%#Eval("VendorCardNumber")%>
                </td>
                <td align="center">
                    <%#Eval("CardTypeName")%>
                </td>
                <td align="center">
                    <%#Eval("CardGradeName")%>
                </td>
                <td align="center">
                    <%#Eval("BatchCode")%>
                </td>
                <td align="center">
                    <%#Eval("StatusName")%>
                </td>
                <td align="center">
                    <%#Eval("TotalAmount", "{0:N02}")%>
                </td>
                <td align="center">
                    <%#Eval("TotalPoints")%>
                </td>
                <td align="center">
                    <%#Eval("CreatedOn", "{0:yyyy-MM-dd}")%>
                </td>
                <td align="center">
                    <%#Eval("CardExpiryDate", "{0:yyyy-MM-dd}")%>
                </td>
                <td align="center">
                    <span class="btn_bg"><a href="Show.aspx?id=<%#Eval("CardNumber") %>&CardTypeID=<%#Eval("CardTypeID") %>&CardTypeName=<%#Eval("CardTypeName") %>&CardGradeName=<%#Eval("CardGradeName") %>&BatchCode=<%#Eval("BatchCode") %>&VendorCardNumber=<%#Eval("VendorCardNumber") %>&StatusName=<%#Eval("StatusName") %>&TotalAmount= <%#Eval("TotalAmount", "{0:N02}")%>&TotalPoints=<%#Eval("TotalPoints") %>&CreatedOn=<%#Eval("CreatedOn", "{0:yyyy-MM-dd}")%>&CardExpiryDate=<%#Eval("CardExpiryDate", "{0:yyyy-MM-dd}")%>&MemberID=<%#Eval("MemberID") %>">
                        详细</a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="clear">
    </div>
    <div class="right">
        <webdiyer:AspNetPager ID="rptListPager" runat="server" CustomInfoTextAlign="Left"
            FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
            LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
            PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
            TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
            CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
            CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
            LayoutType="Table">
        </webdiyer:AspNetPager>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <script type="text/javascript">

        $(function () {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
