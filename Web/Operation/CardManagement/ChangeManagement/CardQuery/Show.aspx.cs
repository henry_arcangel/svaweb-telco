﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery
{
    public partial class Show : Edge.Web.UI.ManagePage
    {

        private const string fields = "ServerCode,StoreID,RegisterCode,OprID,RefTxnNo,BusDate,Txndate,ApprovalCode";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.TXNListPager.PageSize = webset.ContentPageNum;

                SetObject();

                string strWhere = string.Format("CardNumber = '{0}'", Request.Params["id"]);
                RptBind(strWhere, "CardNumber", "");
            }
        }

        protected void TXNListPager_PageChanged(object sender, EventArgs e)
        {
            string strWhere = string.Format("CardNumber = '{0}'", Request.Params["id"]);
            RptBind(strWhere, "KeyID", "");
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby, string fields)
        {
            int currentPage = this.TXNListPager.CurrentPageIndex - 1 < 0 ? 0 : this.TXNListPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Card_Movement bll = new Edge.SVA.BLL.Card_Movement();

            //获得总条数
            this.TXNListPager.RecordCount = bll.GetCount(strWhere);

            DataSet ds = bll.GetList(this.TXNListPager.PageSize, currentPage, strWhere, orderby);

            Tools.DataTool.AddID(ds, "IDNumber", this.TXNListPager.PageSize, currentPage);
            Tools.DataTool.AddTxnTypeName(ds, "OprIDName", "OprID");
            Tools.DataTool.AddColumn(ds, "BrandCode", this.BrandCode);
            Tools.DataTool.AddStoreCode(ds, "StoreCode", "StoreID");

            this.TXNList.DataSource = ds.Tables[0].DefaultView;
            this.TXNList.DataBind();
        }
        #endregion

        private void SetObject()
        {
            this.CardTypeID.Text = Request.Params["CardTypeName"];
            this.CardGradID.Text = Request.Params["CardGradeName"];
            this.CardNumber.Text = Request.Params["id"];
            this.BatchCardID.Text = Request.Params["BatchCode"];
            this.VendorCardNumber.Text = Request.Params["VendorCardNumber"];
            this.Status.Text = Request.Params["StatusName"];
            this.TotalAmount.Text = Request.Params["TotalAmount"];
            this.TotalPoints.Text = Request.Params["TotalPoints"];
            this.CardIssueDate.Text = Request.Params["CreatedOn"];
            this.CardExpiryDate.Text = Request.Params["CardExpiryDate"];
            int memberID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["MemberID"]);
            int cardTypetID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["CardTypeID"]);

            Edge.SVA.Model.CardType cardType = new Edge.SVA.BLL.CardType().GetModel(cardTypetID);
            Edge.SVA.Model.Brand brand = cardType == null ? null : new Edge.SVA.BLL.Brand().GetModel(cardType.BrandID);
            this.BrandCode = brand == null ? null : brand.BrandCode;


            //会员资料
            Edge.SVA.Model.Member member = new Edge.SVA.BLL.Member().GetModel(memberID);
            if (member != null)
            {
                this.MemberEngGivenName.Text = member.MemberEngGivenName;
                this.MemberIdentityRef.Text = member.MemberIdentityRef;
                this.MemberSex.SelectedValue = member.MemberSex.GetValueOrDefault().ToString();
            }
        }

        private string BrandCode
        {
            get
            {
                if (ViewState["BrandCode"] == null) return "";
                return ViewState["BrandCode"].ToString();
            }
            set
            {
                ViewState["BrandCode"] = value;
            }
        }
    }
}
