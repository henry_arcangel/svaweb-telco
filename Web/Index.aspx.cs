﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using System.Configuration;
using System.Web.Security;

namespace Edge.Web
{
    public partial class Index : Edge.Web.UI.ManagePage
    {

        private static string WebVersion = "";

        protected System.Web.UI.WebControls.Label lblMsg;


        AccountsPrincipal user;
        User currentUser;
        public string strWelcome;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["UserInfo"] == null)
                {
                    return;
                }
                //user = new AccountsPrincipal(string.Empty, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。

                currentUser = (Edge.Security.Manager.User)Session["UserInfo"];
                Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
                DataSet ds;
                ds = sm.GetTreeListByLan("", Session["SiteLanguage"].ToString());
                BindTreeView("sysMain", ds.Tables[0]);

                //if (this.TreeView1.Nodes.Count == 0)
                //{
                //    strWelcome += "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;但你没有任何模块的访问权";
                //}
                this.lblAdminName.Text = currentUser.UserName;

                if (string.IsNullOrEmpty(WebVersion))
                {
                    this.lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }                
            }
        }

        //邦定根节点
        public void BindTreeView(string TargetFrame, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + 0);//　选出所有子节点	

            //菜单状态
            string MenuExpanded = ConfigurationManager.AppSettings.Get("MenuExpanded");
           // bool menuExpand = bool.Parse(MenuExpanded);

            TreeView1.Nodes.Clear(); // 清空树
            foreach (DataRow r in drs)
            {
                if (r["KeshiPublic"].ToString().ToLower() == "true")
                {
                    string nodeid = r["NodeID"].ToString();
                    string text = r["Text"].ToString();
                    string parentid = r["ParentID"].ToString();
                    string location = r["Location"].ToString();
                    string url = r["Url"].ToString();
                    string imageurl = r["ImageUrl"].ToString();
                    int permissionid = int.Parse(r["PermissionID"].ToString().Trim());
                    string framename = TargetFrame;

                    //treeview set
                   // this.TreeView1.Font.Name = "宋体";
                    this.TreeView1.Font.Size = FontUnit.Parse("9");

                    //权限控制菜单		
                    if ((permissionid == -1) || (user.HasPermissionID(permissionid)))//绑定用户有权限的和没设权限的（即公开的菜单）
                    {
                        // Microsoft.Web.UI.WebControls.TreeNode rootnode = new Microsoft.Web.UI.WebControls.TreeNode();
                        TreeNode rootnode = new TreeNode();
                        rootnode.Text = text;
                        rootnode.Value = nodeid;
                       // rootnode.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "javascript:;" : url;
                        rootnode.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "AdminCenter.aspx" : url;
                        rootnode.Target = framename;
                        rootnode.Expanded = true;
                        rootnode.ImageUrl = imageurl;

                        TreeView1.Nodes.Add(rootnode);

                        int sonparentid = int.Parse(nodeid);// or =location
                        CreateNode(framename, sonparentid, rootnode, dt);
                    }

                }
            }


            //treeview set
            this.TreeView1.ShowLines = true;
            this.TreeView1.ShowExpandCollapse = true;//是否显示前面的加减号

        }

        //邦定任意节点
        public void CreateNode(string TargetFrame, int parentid, TreeNode parentnode, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);//选出所有子节点			
            foreach (DataRow r in drs)
            {
                if (r["KeshiPublic"].ToString().ToLower() == "true")
                {
                    string nodeid = r["NodeID"].ToString();
                    string text = r["Text"].ToString();
                    string location = r["Location"].ToString();
                    string url = r["Url"].ToString();
                    string imageurl = r["ImageUrl"].ToString();
                    int permissionid = int.Parse(r["PermissionID"].ToString().Trim());
                    string framename = TargetFrame;

                    //权限控制菜单
                    if ((permissionid == -1) || (user.HasPermissionID(permissionid)))
                    {

                        TreeNode node = new TreeNode();
                        node.Text = text;
                        node.Value = nodeid;
                        //node.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "javascript:;" : url;
                        node.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "AdminCenter.aspx" : url;
                        node.Target = TargetFrame;
                        node.ImageUrl = imageurl;
                        node.Expanded = false;
                        //node.Expanded=true;
                        int sonparentid = int.Parse(nodeid);// or =location

                        if (parentnode == null)
                        {
                            TreeView1.Nodes.Clear();
                            parentnode = new TreeNode();
                            TreeView1.Nodes.Add(parentnode);
                        }
                        parentnode.ChildNodes.Add(node);
                        CreateNode(framename, sonparentid, node, dt);
                    }//endif
                }

            }//endforeach		

        }

        protected void lbtnExit_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Clear();
            Response.Redirect("Login.aspx");
            //Response.Write("<script language=javascript>window.close();</script>");
            //Response.End();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Clear();
            Response.Redirect("Login.aspx");
        }		
    }
}
