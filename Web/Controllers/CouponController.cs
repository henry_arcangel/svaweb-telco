﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using Edge.Messages.Manager;
using System.Data;
using System.Data.SqlClient;

namespace Edge.Web.Controllers
{
    /// <summary>
    /// 专用于处理业务逻辑
    /// </summary>
    public class CouponController
    {
        #region Approve Coupon
        /// <summary>
        /// 批核导入优惠劵，若批核成功返回True,否则返回False
        /// </summary>
        /// <param name="model">导入优惠劵Model</param>
        /// <returns></returns>
        private static bool ApproveCoupon(Edge.SVA.Model.Ord_ImportCouponUID_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new Edge.SVA.BLL.Ord_ImportCouponUID_H();

            return bll.Update(model);
        }

        /// <summary>
        /// 批核导入优惠劵，若批核成功返回True,否则返回False
        /// </summary>
        /// <param name="model">导入优惠劵Model</param>
        /// <returns></returns>
        private static bool ApproveCoupon(Edge.SVA.Model.Ord_CouponBatchCreate model)
        {
            if (model == null) return false;
            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponBatchCreate bll = new Edge.SVA.BLL.Ord_CouponBatchCreate();


            return bll.Update(model);


        }

        private static bool ApproveCoupon(Edge.SVA.Model.Ord_CouponAdjust_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H();

            return bll.Update(model);
        }

        public static string ApproveCouponForApproveCode(Edge.SVA.Model.Ord_ImportCouponUID_H model)
        {
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Pending";

            if (!CanApprove(model)) return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90190");

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new Edge.SVA.BLL.Ord_ImportCouponUID_H();

            if (bll.Update(model, 3600))
            {
                model = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(model.ImportCouponNumber);
                return model.ApprovalCode;
            }
            return "";
        }

        public static string ApproveCouponForApproveCode(Edge.SVA.Model.Ord_CouponAdjust_H model, OprID oprID, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) throw new Exception("Approve Transaction  Can't not Empty");

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Pending";

            Edge.SVA.BLL.Coupon couponBll = new Edge.SVA.BLL.Coupon();
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            if (oprID == OprID.Expired)
            {

                string[] list = webset.CouponExpiryDateStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
                if (!couponBll.ValidCouponStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90186");
                }
            }
            else if (oprID == OprID.Issued)
            {
                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
                if (!couponBll.ValidCouponStauts(model, (int)CouponController.CouponStatus.Dormant)) return MessagesTool.instance.GetMessage("90186");
            }
            else if (oprID == OprID.Active)
            {
                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
                if (!couponBll.ValidCouponStauts(model, (int)CouponStatus.Issued)) return MessagesTool.instance.GetMessage("90186");
            }
            else if (oprID == OprID.Void)
            {
                string[] list = webset.CouponVoidStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
                if (!couponBll.ValidCouponStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90186");
                }
            }
            else if (oprID == OprID.Redeemed)
            {
                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");
                if (!couponBll.ValidCouponStauts(model, (int)CouponStatus.Activated)) return MessagesTool.instance.GetMessage("90186");
            }
            else if (oprID == OprID.ChangeStatus)
            {
                string[] list = webset.CouponStatusChangeStatusEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");

                if (!couponBll.ValidCouponStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90190");
                }
            }
            else if (oprID == OprID.ChangeDenomination)
            {
                string[] list = webset.CouponChangeDenominationEnable.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] status = Edge.Utils.Tools.StringHelper.StringToInt(list);

                if (!couponBll.ExsitCoupon(model)) return MessagesTool.instance.GetMessage("90178");

                if (!couponBll.ValidCouponStauts(model, status))
                {
                    return MessagesTool.instance.GetMessage("90186");
                }
            }


            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CouponAdjust_H().GetModel(model.CouponAdjustNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }

        public static string ApproveCouponForApproveCode(Edge.SVA.Model.Ord_CouponBatchCreate model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return MessagesTool.instance.GetMessage("90178");

            string msg = CanApprove(model, out isSuccess);
            if (!isSuccess) return msg;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponBatchCreate bll = new Edge.SVA.BLL.Ord_CouponBatchCreate();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CouponBatchCreate().GetModel(model.CouponCreateNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }

        public static string ApproveCouponForApproveCode(Edge.SVA.Model.Ord_ImportCouponDispense_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return MessagesTool.instance.GetMessage("90178");

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCouponDispense_H bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_ImportCouponDispense_H().GetModel(model.CouponDispenseNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }
        #endregion

        #region Void Coupon

        private static bool VoidCoupon(Edge.SVA.Model.Ord_ImportCouponUID_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new Edge.SVA.BLL.Ord_ImportCouponUID_H();

            return bll.Update(model);
        }

        private static bool VoidCoupon(Edge.SVA.Model.Ord_CouponBatchCreate model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());


            Edge.SVA.BLL.Ord_CouponBatchCreate bll = new Edge.SVA.BLL.Ord_CouponBatchCreate();

            return bll.Update(model);
        }

        private static bool VoidCoupon(Edge.SVA.Model.Ord_CouponAdjust_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H();

            return bll.Update(model);
        }

        private static bool VoidCoupon(Edge.SVA.Model.Ord_ImportCouponDispense_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_ImportCouponDispense_H bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_H();

            return bll.Update(model);
        }
        #endregion

        #region Can Approve

        public static bool CanApprove(Edge.SVA.Model.Ord_ImportCouponUID_H model)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            if (bll.ExsitCoupon(model)) return false;
            return true;
        }

        public static bool CanApprove(Edge.SVA.Model.Ord_CouponAdjust_H model)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            if (bll.ExsitCoupon(model)) return false;
            return true;
        }

        public static string CanApprove(Edge.SVA.Model.Ord_CouponBatchCreate model, out bool isSusscess)
        {
            isSusscess = false;
            SVA.Model.CouponType couponType = new SVA.BLL.CouponType().GetModel(model.CouponTypeID);
            if (couponType == null) return "Not Exist Coupon Type";
            if (couponType.IsImportCouponNumber.GetValueOrDefault() == 1) return "Invalid Coupon Type,only support Coupon Type - Manual";
            SVA.BLL.Coupon bll = new SVA.BLL.Coupon();
            long max = long.Parse(couponType.CouponNumMask.Substring(couponType.CouponNumMask.IndexOf("9")));
            if (couponType.CouponCheckdigit.GetValueOrDefault()) max = max / 10;

            int count = bll.GetCount(string.Format("CouponTypeID = {0}", model.CouponTypeID), 120);

            if ((max - count - model.CouponCount) >= 0)
            {
                isSusscess = true;
                return null;
            }

            return Messages.Manager.MessagesTool.instance.GetMessage("90417");
        }

        #endregion

        public static string BatchVoidCoupon(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.BatchCreate)
                {
                    Edge.SVA.Model.Ord_CouponBatchCreate model = new Edge.SVA.BLL.Ord_CouponBatchCreate().GetModel(id);
                    if (CouponController.VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.ImportCoupon)
                {
                    Edge.SVA.Model.Ord_ImportCouponUID_H model = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(id);
                    if (CouponController.VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.CouponAdjust)
                {
                    Edge.SVA.Model.Ord_CouponAdjust_H model = new Edge.SVA.BLL.Ord_CouponAdjust_H().GetModel(id);
                    if (CouponController.VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.BIImport)
                {
                    Edge.SVA.Model.Ord_ImportCouponDispense_H model = new Edge.SVA.BLL.Ord_ImportCouponDispense_H().GetModel(id);
                    if (CouponController.VoidCoupon(model)) success++;
                }
            }

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }

        public static SVA.Model.CouponType GetImportCouponType(string couponTypeCode, Dictionary<string, SVA.Model.CouponType> cache)
        {
            if (cache != null && cache.ContainsKey(couponTypeCode)) return cache[couponTypeCode];

            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();

            SVA.Model.CouponType couponType = bll.GetImportCouponType(couponTypeCode);
            cache.Add(couponTypeCode, couponType);

            return couponType;
        }

        public static bool IsCouponTypeStore(int couponTypeID, int storeID, Dictionary<int, Edge.SVA.Model.CouponType> cache)
        {
            Edge.SVA.Model.CouponType couponType = CouponController.GetCouponTypeByCache(couponTypeID, cache);

            if (couponType == null) return false;

            if (couponType.IsMemberBind.GetValueOrDefault() == 1) return true;

            if (new Edge.SVA.BLL.CouponTypeStoreCondition_List().GetCount(string.Format("CouponTypeID={0}", couponTypeID)) <= 0) return true;


            return true;
        }

        public static bool IsCouponTypeBrand(int couponTypeID, int storeID, Dictionary<int, Edge.SVA.Model.CouponType> cache)
        {
            Edge.SVA.Model.CouponType couponType = CouponController.GetCouponTypeByCache(couponTypeID, cache);

            if (couponType == null) return false;

            if (couponType.IsMemberBind.GetValueOrDefault() == 1) return true;

            if (new Edge.SVA.BLL.CouponTypeStoreCondition_List().GetCount(string.Format("CouponTypeID={0}", couponTypeID)) <= 0) return true;


            return true;
        }

        public static string GetPreviousCouponStatus(int currentStatus, string couponNumber)
        {
            switch (currentStatus)
            {
                case 0: return "";
                case 1: return DALTool.GetCouponTypeStatusName((int)CouponStatus.Dormant);
                case 2: return DALTool.GetCouponTypeStatusName((int)CouponStatus.Issued);
                case 3: return DALTool.GetCouponTypeStatusName((int)CouponStatus.Activated);
                case 4: return DALTool.GetCouponTypeStatusName(DALTool.GetPreviousCouponStatus(currentStatus, couponNumber));
                case 5: return DALTool.GetCouponTypeStatusName(DALTool.GetPreviousCouponStatus(currentStatus, couponNumber));
            }

            return "";
        }

        private static Edge.SVA.Model.CouponType GetCouponTypeByCache(int couponTypeID, Dictionary<int, Edge.SVA.Model.CouponType> cache)
        {
            Edge.SVA.Model.CouponType couponType = null;
            if (cache != null && cache.Keys.Contains(couponTypeID))
            {
                couponType = cache[couponTypeID];
            }
            else
            {
                couponType = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);
                if (couponType == null) return null;
                cache.Add(couponTypeID, couponType);
            }

            return couponType;
        }

        public static int GetCreatedCouponInfo(int couponTypeID, int createCount, ref long remainCoupons, ref string lastCreatedCoupon, ref string msg)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int,6) , 
                                            new SqlParameter("@TypeID", SqlDbType.Int,50),
                                            new SqlParameter("@GenQty", SqlDbType.Int,6), 
                                            new SqlParameter("@ReturnStartNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnEndNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnDuplicateNumber", SqlDbType.VarChar,64),
                                             new SqlParameter("@RemainQty", SqlDbType.BigInt),
                                            new SqlParameter("@LastNumber", SqlDbType.VarChar,64)
                                          };
            parameters[0].Value = 1;
            parameters[1].Value = couponTypeID;
            parameters[2].Value = createCount;
            parameters[3].Direction = ParameterDirection.Output;
            parameters[4].Direction = ParameterDirection.Output;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;

            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("CheckGenerateNumber", parameters, out count);
            if (parameters[5].Value != null)
            {
                msg = parameters[5].Value.ToString();
            }
            if (parameters[6].Value != null)
            {
                remainCoupons = Edge.Web.Tools.ConvertTool.ToLong(parameters[6].Value.ToString());
            }
            if (parameters[7].Value != null)
            {
                lastCreatedCoupon = parameters[7].Value.ToString();
            }
            return result;

        }

        public enum ApproveType
        {
            BatchCreate,
            ImportCoupon,
            CouponAdjust,
            BIImport

        }

        public enum CouponStatus
        {
            Dormant = 0,
            Issued = 1,
            Activated = 2,
            Redeemed = 3,
            Expired = 4,
            Voided = 5,
            Recycled = 6
        }

        public enum OprID
        {
            Active = 33,
            Redeemed = 34,
            Void = 35,
            Expired = 38,
            Issued = 40,
            ChangeStatus = 41,
            ChangeDenomination = 44,
            Forfeit = 55,
            BindCouponToCard = 32
        }

        public static class CouponRefnoCode
        {
            public static string OrderBatchCreationOfCoupons = "BCC";
            public static string OrderBatchImportCoupons = "BIC";
            public static string OrderCouponActive = "CA";
            public static string OrderCouponChangeExpiryDate = "CED";
            public static string OrderCouponIssued = "CI";
            public static string OrderCouponRedeemed = "CR";
            public static string OrderCouponStatusAdjustmentHighSecurity = "CSA";
            public static string OrderCouponVoid = "CV";
            public static string OrderCouponChangeDenomination = "CD";
            public static string OrderImportBICoupons = "BICOU";
            public static string OrderCardPickup = "CPO";
            public static string OrderCardDelivery = "CDO";
            public static string CardOrderForm = "COF";
            public static string OrderCouponPickup = "COPO";
            public static string OrderCouponDelivery = "CODO";
            public static string CouponOrderForm = "COFO";
        }
    }



}
