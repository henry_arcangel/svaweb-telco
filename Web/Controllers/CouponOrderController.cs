﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.Controllers
{
    public class CouponOrderController
    {
        #region Approve Order

        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CouponOrderForm_H model,out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) throw new Exception("Approve Transaction  Can't not Empty");

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Pending";

            Edge.SVA.BLL.Ord_CouponOrderForm_H bll = new Edge.SVA.BLL.Ord_CouponOrderForm_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CouponOrderForm_H().GetModel(model.CouponOrderFormNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CouponPicking_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) throw new Exception("Approve Transaction  Can't not Empty");

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Picked";

            string msg="";
            if (!CanApprovePicked(model, out msg)) return msg;

            Edge.SVA.BLL.Ord_CouponPicking_H bll = new Edge.SVA.BLL.Ord_CouponPicking_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CouponPicking_H().GetModel(model.CouponPickingNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        public static string ApproveForApproveCode(Edge.SVA.Model.Ord_CouponDelivery_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) throw new Exception("Approve Transaction  Can't not Empty");

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Pending";

            Edge.SVA.BLL.Ord_CouponDelivery_H bll = new Edge.SVA.BLL.Ord_CouponDelivery_H();
            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CouponDelivery_H().GetModel(model.CouponDeliveryNumber);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }
        #endregion

        #region Void Coupon

        private static bool VoidCoupon(Edge.SVA.Model.Ord_CouponOrderForm_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponOrderForm_H bll = new Edge.SVA.BLL.Ord_CouponOrderForm_H();

            return bll.Update(model);
        }

        private static bool VoidCoupon(Edge.SVA.Model.Ord_CouponPicking_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P" && model.ApproveStatus != "R") return false;//R for Pending, P for Picked.

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponPicking_H bll = new Edge.SVA.BLL.Ord_CouponPicking_H();

            return bll.Update(model);
        }

        private static bool VoidCoupon(Edge.SVA.Model.Ord_CouponDelivery_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponDelivery_H bll = new Edge.SVA.BLL.Ord_CouponDelivery_H();

            return bll.Update(model);
        }
        #endregion

        #region Picked Coupon
        private static bool PickedCoupon(Edge.SVA.Model.Ord_CouponPicking_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "R") return false;

            model.ApproveStatus = "P";//Picked
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CouponPicking_H bll = new Edge.SVA.BLL.Ord_CouponPicking_H();

            return bll.Update(model);
        }
        #endregion


        public static string BatchVoidCoupon(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.CouponOrderForm)
                {
                    Edge.SVA.Model.Ord_CouponOrderForm_H model = new Edge.SVA.BLL.Ord_CouponOrderForm_H().GetModel(id);
                    if (VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.CouponPicking)
                {
                    Edge.SVA.Model.Ord_CouponPicking_H model = new Edge.SVA.BLL.Ord_CouponPicking_H().GetModel(id);
                    if (VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.CouponDelivery)
                {
                    Edge.SVA.Model.Ord_CouponDelivery_H model = new Edge.SVA.BLL.Ord_CouponDelivery_H().GetModel(id);
                    if (VoidCoupon(model)) success++;
                }
            }

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }


        public static int AddPickupDetailCoupon(int userID, string couponPickingNumber, int couponTypeID, int QTY, int batchID, string couponUID, string StartCouponNumber, int filterType)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@UserID", SqlDbType.Int) , 
                                            new SqlParameter("@CouponPickingNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@FilterType", SqlDbType.Int), 
                                            new SqlParameter("@CouponTypeID", SqlDbType.Int),
                                            new SqlParameter("@QTY", SqlDbType.Int),
                                            new SqlParameter("@BatchID", SqlDbType.Int),
                                             new SqlParameter("@CouponUID", SqlDbType.VarChar,512),
                                            new SqlParameter("@StartCouponNumber", SqlDbType.VarChar,64)
                                          };
            parameters[0].Value = userID;
            parameters[1].Value = couponPickingNumber;
            parameters[2].Value = filterType;
            parameters[3].Value = couponTypeID;
            parameters[4].Value = QTY;
            parameters[5].Value = batchID;
            parameters[6].Value = couponUID;
            parameters[7].Value = StartCouponNumber;

            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("AddPickupDetail_Coupon", parameters, out count);
            return result;
        }

        public static string GetOrderCouponType(string couponOrderFormNumber)
        {
            string strReturn = "";

            string query = "SELECT [CouponTypeID] FROM [Ord_CouponOrderForm_D] where [CouponOrderFormNumber]='" + couponOrderFormNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CouponTypeID"] + ",";
                }

                strReturn = strReturn.TrimEnd(',');
                return strReturn;
            }

            return strReturn;
        }

        public static long GetPickupOrderTotalQty(string couponOrderFormNumber)
        {
            long lonReturn = 0;

            string query = "SELECT sum(CouponQty) as CouponQty FROM [Ord_CouponOrderForm_D] where [CouponOrderFormNumber]='" + couponOrderFormNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                lonReturn = Tools.ConvertTool.ConverType<long>(ds.Tables[0].Rows[0]["CouponQty"].ToString());

                return lonReturn;
            }
            return lonReturn;
        }

        public static long GetDeliveryOrderTotalQty(string couponPickingNumber)
        {
            long lonReturn = 0;

            string query = "SELECT sum(CouponQty) as CouponQty FROM [Ord_CouponOrderForm_D] where CouponOrderFormNumber=(select top 1 ReferenceNo from Ord_CouponPicking_H where CouponPickingNumber=(select top 1 ReferenceNo from Ord_CouponDelivery_H where CouponDeliveryNumber='" + couponPickingNumber.Trim() + "'))";
            //string query = "SELECT sum(o.CouponQty) as CouponQty FROM [Ord_CouponOrderForm_D] o left join dbo.Ord_CouponPicking_H p on o.CouponOrderFormNumber=p.ReferenceNo where p.CouponPickingNumber='" + couponPickingNumber.Trim() + "'";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                lonReturn = Tools.ConvertTool.ConverType<long>(ds.Tables[0].Rows[0]["CouponQty"].ToString());

                return lonReturn;
            }
            return lonReturn;
        }

        public static bool CanApprovePicked(Edge.SVA.Model.Ord_CouponPicking_H model,out string msg)
        {
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            string allowType = webset.CouponOrderPickingAllowSetting.Trim();

            string strSql = "SELECT CouponTypeID,MAX(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CouponPicking_D] where CouponPickingNumber='" + model.CouponPickingNumber + "' group by CouponTypeID ";
            
            DataSet ds =Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
                //long OrderQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
                //long PickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
                foreach (DataRow row in dt.Rows)
                {
                    long OrderQTY = Tools.ConvertTool.ConverType<long>(row["OrderQTY"].ToString());
                    long PickQTY = Tools.ConvertTool.ConverType<long>(row["PickQTY"].ToString());

                    if (allowType == "1")
                    {
                        if (PickQTY == OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet1;
                            return false;
                        }
                    }
                    else if (allowType == "2")
                    {
                        if (PickQTY <= OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet2;
                            return false;
                        }
                    }
                    else if (allowType == "3")
                    {
                        if (PickQTY >= OrderQTY)
                        {
                            msg = "";
                            return true;
                        }
                        else
                        {
                            msg = Resources.MessageTips.OrderPickSet3;
                            return false;
                        }
                    }
                    else
                    {
                        msg = "";
                        return true;
                    }
                }

                msg = "";
                return true;
            }
            else
            {
                msg = msg = Resources.MessageTips.OrderPickSet4;
                return false;
            }
        
        }


        public static bool IsMeetPickingByType(long OrderQTY, long PickQTY)
        {
            Edge.Security.Model.WebSet webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));

            string allowType = webset.CouponOrderPickingAllowSetting.Trim();

            if (allowType == "1")
            {
                if (PickQTY == OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (allowType == "2")
            {
                if (PickQTY <= OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (allowType == "3")
            {
                if (PickQTY >= OrderQTY)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public static void GetApprovePickedTotal(string couponPickingNumber, out long totalOrderQTY,out long totalPickQTY)
        {
            string strSql = "SELECT CouponTypeID,MAX(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CouponPicking_D] where CouponPickingNumber='" + couponPickingNumber.Trim() + "' group by CouponTypeID ";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
               totalOrderQTY= Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
               totalPickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
            }
            else
            {
                totalPickQTY = 0;
                totalOrderQTY = 0;
            }


        }

        public static void GetApproveDeliveryTotal(string couponDeliveryNumber, out long totalOrderQTY, out long totalPickQTY)
        {
            string strSql = "SELECT CouponTypeID,MAX(OrderQTY) as OrderQTY,Sum(PickQTY) as PickQTY FROM [Ord_CouponDelivery_D] where CouponDeliveryNumber='" + couponDeliveryNumber.Trim() + "' group by CouponTypeID ";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                DataTable dt = ds.Tables[0];
                totalOrderQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(OrderQTY)", "").ToString());
                totalPickQTY = Tools.ConvertTool.ConverType<long>(dt.Compute("sum(PickQTY)", "").ToString());
            }
            else
            {
                totalPickQTY = 0;
                totalOrderQTY = 0;
            }


        }

        public static void GenOrderFormPrintPages(List<string> idList, System.Web.UI.HtmlControls.HtmlGenericControl printDiv)
        {
            foreach (string id in idList)
            {
                SVA.Model.Ord_CouponPicking_H picking = new SVA.BLL.Ord_CouponPicking_H().GetModelByOrderNumber(id);
                if (picking == null) continue;

                #region 构建打印列表
                Table printTable = new Table();
                printTable.CellPadding = 0;  //设置单元格内间距
                printTable.CellSpacing = 0;   //设置单元格之间的距离
                //printTable.BorderWidth = 0;
                printTable.Width = new Unit(100, UnitType.Percentage);
                printTable.CssClass = "msgtable";

                //表头
                TableHeaderRow headerRow = new TableHeaderRow();
                TableHeaderCell headerRowCell = new TableHeaderCell();
                headerRowCell.Text = "捡货列表";
                headerRowCell.ColumnSpan = 4;
                headerRowCell.HorizontalAlign = HorizontalAlign.Center;
                headerRow.Cells.Add(headerRowCell);
                printTable.Rows.Add(headerRow);

                //第一行
                TableRow row1 = new TableRow();
                TableCell row1Cell1 = new TableCell();
                row1Cell1.Text = "捡货单编号：";
                row1Cell1.HorizontalAlign = HorizontalAlign.Right;
                row1Cell1.Width = new Unit(15, UnitType.Percentage);
                row1.Cells.Add(row1Cell1);
                TableCell row1Cell2 = new TableCell();
                row1Cell2.Text = picking.CouponPickingNumber;
                row1Cell2.Width = new Unit(35, UnitType.Percentage);
                row1.Cells.Add(row1Cell2);
                TableCell row1Cell3 = new TableCell();
                row1Cell3.Text = "打印时间：";
                row1Cell3.HorizontalAlign = HorizontalAlign.Right;
                row1Cell3.Width = new Unit(15, UnitType.Percentage);
                row1.Cells.Add(row1Cell3);
                TableCell row1Cell4 = new TableCell();
                row1Cell4.Text = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                row1.Cells.Add(row1Cell4);
                printTable.Rows.Add(row1);

                //第二行
                TableRow row2 = new TableRow();
                TableCell row2Cell1 = new TableCell();
                row2Cell1.Text = "参考编号：";
                row2Cell1.HorizontalAlign = HorizontalAlign.Right;
                row2.Cells.Add(row2Cell1);
                TableCell row2Cell2 = new TableCell();
                row2Cell2.Text = picking.ReferenceNo;
                row2.Cells.Add(row2Cell2);
                TableCell row2Cell3 = new TableCell();
                row2Cell3.Text = "状态：";
                row2Cell3.HorizontalAlign = HorizontalAlign.Right;
                row2.Cells.Add(row2Cell3);
                TableCell row2Cell4 = new TableCell();
                row2Cell4.Text = Tools.DALTool.GetOrderPickingApproveStatusString(picking.ApproveStatus);
                row2.Cells.Add(row2Cell4);
                printTable.Rows.Add(row2);

                //第三行
                TableRow row3 = new TableRow();
                TableCell row3Cell1 = new TableCell();
                row3Cell1.Text = "捡货日期：";
                row3Cell1.HorizontalAlign = HorizontalAlign.Right;
                row3.Cells.Add(row3Cell1);
                TableCell row3Cell2 = new TableCell();
                row3Cell2.Text = Tools.ConvertTool.ToStringDate(picking.ApproveOn.GetValueOrDefault());
                row3.Cells.Add(row3Cell2);
                TableCell row3Cell3 = new TableCell();
                row3Cell3.Text = "捡货人：";
                row3Cell3.HorizontalAlign = HorizontalAlign.Right;
                row3.Cells.Add(row3Cell3);
                TableCell row3Cell4 = new TableCell();
                row3Cell4.Text = Tools.DALTool.GetUserName(picking.ApproveBy.GetValueOrDefault());
                row3.Cells.Add(row3Cell4);
                printTable.Rows.Add(row3);


                #region Picking list table
                //构建表
                Table printPickingTable = new Table();
                printPickingTable.CellPadding = 0;  //设置单元格内间距
                printPickingTable.CellSpacing = 0;   //设置单元格之间的距离
                //printTable.BorderWidth = 0;
                printPickingTable.Width = new Unit(100, UnitType.Percentage);
                printPickingTable.CssClass = "msgtablelist";

                //表头
                TableHeaderRow headerPickingRow = new TableHeaderRow();
                TableHeaderCell headerPickingRowCell1 = new TableHeaderCell();
                headerPickingRowCell1.Text = "优惠劵类型编号";
                headerPickingRow.Cells.Add(headerPickingRowCell1);
                TableHeaderCell headerPickingRowCell2 = new TableHeaderCell();
                headerPickingRowCell2.Text = "优惠劵类型";
                headerPickingRow.Cells.Add(headerPickingRowCell2);
                TableHeaderCell headerPickingRowCell3 = new TableHeaderCell();
                headerPickingRowCell3.Text = "订单数量";
                headerPickingRow.Cells.Add(headerPickingRowCell3);
                TableHeaderCell headerPickingRowCell4 = new TableHeaderCell();
                headerPickingRowCell4.Text = "捡货数量";
                headerPickingRow.Cells.Add(headerPickingRowCell4);
                TableHeaderCell headerPickingRowCell5 = new TableHeaderCell();
                headerPickingRowCell5.Text = "优惠券起始编号";
                headerPickingRow.Cells.Add(headerPickingRowCell5);
                TableHeaderCell headerPickingRowCell6 = new TableHeaderCell();
                headerPickingRowCell6.Text = "优惠券结束编号";
                headerPickingRow.Cells.Add(headerPickingRowCell6);
                printPickingTable.Rows.Add(headerPickingRow);

                System.Data.DataSet ds = new SVA.BLL.Ord_CouponOrderForm_D().GetList(string.Format("CouponOrderFormNumber = '{0}'", id));
                Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    //第二行
                    TableRow printPickingRow = new TableRow();
                    TableCell printPickingRowCell1 = new TableCell();
                    printPickingRowCell1.Text = row["CouponTypeCode"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell1);
                    TableCell printPickingRowCell2 = new TableCell();
                    printPickingRowCell2.Text = row["CouponType"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell2);
                    TableCell printPickingRowCell3 = new TableCell();
                    printPickingRowCell3.Text = row["CouponQty"].ToString();
                    printPickingRow.Cells.Add(printPickingRowCell3);
                    TableCell printPickingRowCell4 = new TableCell();
                    printPickingRowCell4.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell4);
                    TableCell printPickingRowCell5 = new TableCell();
                    printPickingRowCell5.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell5);
                    TableCell printPickingRowCell6 = new TableCell();
                    printPickingRowCell6.Text = "";
                    printPickingRow.Cells.Add(printPickingRowCell6);
                    printPickingTable.Rows.Add(printPickingRow);
                }

                #endregion

                //第四行
                TableRow row4 = new TableRow();
                TableCell row4Cell = new TableCell();
                row4Cell.ColumnSpan = 4;
                row4Cell.Controls.Add(printPickingTable);
                row4.Cells.Add(row4Cell);
                printTable.Rows.Add(row4);

                printDiv.Controls.Add(printTable);
                Panel p = new Panel();
                p.Style.Add("padding-bottom", "10px");
                printDiv.Controls.Add(p);
                #endregion
            }
        }
       
        public enum ApproveType
        {
            CouponOrderForm,
            CouponPicking,
            CouponDelivery

        }

        public static class RefnoCode
        {
            public static string OrderCouponPickup = "COPO";
            public static string OrderCouponDelivery = "CODO";
            public static string OrderCouponForm = "COFO";
        }
    }
}