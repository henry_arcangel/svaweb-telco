﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MemberMasterFile.Profession
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Profession,Edge.SVA.Model.Profession>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);
            }
        }
      
    }
}
