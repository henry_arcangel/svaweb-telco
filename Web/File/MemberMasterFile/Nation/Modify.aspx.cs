﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Nation
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Nation,Edge.SVA.Model.Nation>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

     
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.Nation item = this.GetUpdateObject();

            if (item == null)
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
                return;
            }

            if (Tools.DALTool.isHasNationCode(this.NationCode.Text.Trim(), item.NationID))
            {
                JscriptPrint(Resources.MessageTips.ExistNationCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.NationCode = item.NationCode.ToUpper().Trim();
                item.CountryCode = item.CountryCode.ToUpper().Trim();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Nation>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
