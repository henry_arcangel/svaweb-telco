﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store,Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Show");
            if (!this.IsPostBack)
            {
                //Edge.Web.Tools.ControlTool.BindStoreType(StoreTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                    this.BrandID.Text = DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                }
                else
                {
                    this.lblCardIssuer.Text = "";
                }

                Edge.SVA.Model.StoreType storeType = new Edge.SVA.BLL.StoreType().GetModel(Model.StoreTypeID.GetValueOrDefault());
                string text = storeType == null ? "" : DALTool.GetStringByCulture(storeType.StoreTypeName1, storeType.StoreTypeName2, storeType.StoreTypeName3);
                this.StoreTypeID.Text = storeType == null ? "" : ControlTool.GetDropdownListText(text, storeType.StoreTypeCode);
                

              
            }
        }
    }
}
