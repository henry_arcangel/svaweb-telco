﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

	<script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

	<script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

	<script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>
</head>
<body style="padding:10px;">
    <form id="form1" runat="server">
   <div class="navigation">
      <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom:10px;"></div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                   <tr>
					<th colspan="2" align="left"><%=this.PageName %></th>
                     </tr>
				    <tr>
					    <td width="25%" align="right">描述：</td>
					    <td width="75%">
						    <asp:textbox id="StoreGroupName1" tabIndex="1" runat="server" Width="249px" MaxLength="20" CssClass="input required"></asp:textbox><span class="star">*</span>
						</td>
				    </tr>
				    <tr>
					    <td align="right">
						    <div align="right">其他描述1：</div>
					    </td>
					    <td >
						    <asp:textbox id="StoreGroupName2" tabIndex="2" runat="server" Width="249px" MaxLength="20" CssClass="input "  ></asp:textbox>
						  </td>
				    </tr>
				    <tr>
					    <td align="right">其他描述2：</td>
					    <td>
						    <asp:textbox id="StoreGroupName3" tabIndex="4" runat="server" Width="249px" MaxLength="20" CssClass="input "></asp:textbox>
						</td>
				    </tr>
				    <tr>
					    <td colSpan="2" align="center"><asp:label id="lblMsg" runat="server" ForeColor="Red"></asp:label></td>
				    </tr>
				    <tr>
		           <td colSpan="2" align="center">
			      <div align="center">
			        <asp:button id="btnAdd" runat="server" Text="提交" onclick="btnAdd_Click" CssClass="submit"></asp:button>
				    <input type="button" name="button1"  value="返 回" onclick= "location.href= 'List.aspx' " class="submit"/>
				   </div>
		          </td>
	        </tr>
	    </table>
			<div style="margin-top:10px;text-align:center;"></div>	
            <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
