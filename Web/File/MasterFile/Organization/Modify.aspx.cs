﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Organization
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Organization, Edge.SVA.Model.Organization>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.Organization item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.OrganizationCode = item.OrganizationCode.ToUpper();

            }

            if (Edge.Web.Tools.DALTool.isHasOrganizationCode(item.OrganizationCode, item.OrganizationID))
            {
                JscriptPrint(Resources.MessageTips.ExistOrganizationCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (!Edge.Web.Tools.DALTool.isHasCard(this.CardNumber.Text.Trim()))
            {
                JscriptPrint(Resources.MessageTips.NotExistCard, "", Resources.MessageTips.WARNING_TITLE);
                this.CardNumber.Focus();
                return;
            }

            if (DALTool.Update<Edge.SVA.BLL.Organization>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }


        }
    }
}