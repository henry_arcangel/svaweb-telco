﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.Organization.Modify" %>

<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                捐赠组织编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="OrganizationCode" TabIndex="1" runat="server" CssClass="input required svaCode"
                    MaxLength="20"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                捐赠组织描述：
            </td>
            <td>
                <asp:TextBox ID="OrganizationName1" TabIndex="2" runat="server" CssClass="input required"
                    hinttitle="捐赠组织描述" hintinfo="请输入规范的捐赠组织描述。不能超過512個字符"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="OrganizationName2" TabIndex="3" runat="server" CssClass="input"
                    hinttitle="其他描述1" hintinfo="对捐赠组织的其他描述1,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="OrganizationName3" TabIndex="4" runat="server" CssClass="input"
                    hinttitle="其他描述2" hintinfo="对捐赠组织的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                关联的卡号：
            </td>
            <td>
                <asp:TextBox ID="CardNumber" TabIndex="5" runat="server" CssClass="input required"></asp:TextBox><span
                    class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计获得的积分：
            </td>
            <td>
                <asp:TextBox ID="CumulativePoints" TabIndex="6" runat="server" CssClass="input svaPoint" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计获得的金额：
            </td>
            <td>
                <asp:TextBox ID="CumulativeAmt" TabIndex="7" runat="server" CssClass="input svaAmount" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <%-- <tr>
            <td align="right">
                机构类型：
            </td>
            <td>
                <asp:TextBox ID="OrganizationType" TabIndex="8" runat="server" CssClass="input"></asp:TextBox>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                图片路径：
            </td>
            <td>
                <ufb:UploadFileBox ID="OrganizationPicFile" tabindex="9" runat="server" SubSaveFilePath="Images/Organization"
                    HintTitle="图片" HintInfo="Translate__Special_121_Start捐赠组织的图片。点击按钮进行上传，上传的文件支持JPG,GIF，PNG，BMP,文件大小不能超过10240KBTranslate__Special_121_End" />
            </td>
        </tr>
        <tr>
            <td align="right">
                是否调用第三方接口：
            </td>
            <td>
                <asp:RadioButtonList ID="CallInterface" runat="server" TabIndex="10" RepeatDirection="Horizontal">
                    <asp:ListItem Text="调用" Value="1"></asp:ListItem>
                    <asp:ListItem Text="不调用" Value="0" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2">
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
