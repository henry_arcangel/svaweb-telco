﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.IndustryGroup
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Industry, Edge.SVA.Model.Industry>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasIndustryCode(this.IndustryCode.Text.Trim(), 0))
            {
                this.JscriptPrint(Resources.MessageTips.ExistIndustryCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }


            Edge.SVA.Model.Industry item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.IndustryCode = item.IndustryCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Industry>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
