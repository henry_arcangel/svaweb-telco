﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div>
        <asp:Repeater ID="rptList" runat="server">
            <HeaderTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                    <tr>
                        <th width="10%">
                            <input type="checkbox" onclick="checkAll(this);" />选择
                        </th>
                        <th width="15%">
                            卡类型编号
                        </th>
                        <th width="25%">
                            描述
                        </th>
                        <th width="20%">
                            发行商
                        </th>
                        <th width="20%">
                            品牌
                        </th>
                        <th width="10%">
                            操作
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td align="center">
                        <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" /><asp:HiddenField ID="lb_id"
                            runat="server" Value='<%#Eval("CardTypeID")%>'></asp:HiddenField>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblCardTypeCode" runat="server" Text='<%#Eval("CardTypeCode")%>'></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblCardTypeName1" runat="server" Text='<%#Eval("CardTypeName1")%>'></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblCardIssuerName" runat="server" Text='<%#Eval("CardIssuerName")%>'></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("BrandCode")%>'></asp:Label>
                        -
                        <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                    </td>
                    <td align="center">
                        <span class="btn_bg"><a href="show.aspx?id=<%#Eval("CardTypeID") %>">查看</a> <a href="modify.aspx?id=<%#Eval("CardTypeID") %>&page=<%=page %>">
                            修改</a> </span>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <div class="spClear">
        </div>
        <div style="line-height: 30px; height: 30px;">
            <div id="Pagination" class="right flickr">
            </div>
            <div class="left">
                <span class="btn_bg">
                    <asp:LinkButton ID="lbtnDel" runat="server" OnClick="lbtnDel_Click">删除</asp:LinkButton>
                    <asp:LinkButton ID="lbtnAdd" runat="server" OnClick="lbtnAdd_Click">添加</asp:LinkButton>
                </span>
            </div>
        </div>
    </div>
    </form>
    <uc2:checkright ID="Checkright1" runat="server" />
    <script type="text/javascript">
        
        $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
