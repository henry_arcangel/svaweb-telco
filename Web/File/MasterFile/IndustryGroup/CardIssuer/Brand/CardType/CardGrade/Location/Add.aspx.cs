﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Location
{
    public partial class Add : Edge.Web.UI.ManagePage
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ViewState["cardGradeID"] = Request.Params["CardGradeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString());
                ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
                ViewState["conditionTypeID"] = 2;

                this.txtCount.Text = webset.ContentPageNum.ToString();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            for (int i = 0; i < ckbLocationID.Items.Count; i++)
            {
                if (ckbLocationID.Items[i].Selected)
                {
                    int locationID = Edge.Web.Tools.ConvertTool.ToInt(ckbLocationID.Items[i].Value.ToString());

                    Edge.SVA.Model.CardGradeStoreCondition item = new Edge.SVA.Model.CardGradeStoreCondition();
                    item.CardGradeID = Convert.ToInt32(ViewState["cardGradeID"]);
                    item.StoreConditionType = Convert.ToInt32(ViewState["storeConditionTypeID"]);
                    item.ConditionType = Convert.ToInt32(ViewState["conditionTypeID"]);
                    item.ConditionID = locationID;
                    item.CreatedBy = DALTool.GetCurrentUser().UserID;
                    item.CreatedOn = DateTime.Now;


                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CardGradeID={0} and StoreConditionType={1} and ConditionType={2} and ConditionID={3}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"], ViewState["conditionTypeID"], locationID));
                    if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount(sbWhere.ToString()) <= 0)
                    {
                        DALTool.Add<Edge.SVA.BLL.CardGradeStoreCondition>(item);
                    }
                }
            }
            Response.Redirect(string.Format("List.aspx?page=0&CardGradeID={0}&StoreConditionTypeID={1}&type=2", ViewState["cardGradeID"], ViewState["storeConditionTypeID"]));
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {

            Edge.Web.Tools.ControlTool.BindLocation(ckbLocationID, LocationName.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

        }


        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("List.aspx?page=0&CardGradeID={0}&StoreConditionTypeID={1}&type=2", ViewState["cardGradeID"], ViewState["storeConditionTypeID"]));
        }

    }
}
