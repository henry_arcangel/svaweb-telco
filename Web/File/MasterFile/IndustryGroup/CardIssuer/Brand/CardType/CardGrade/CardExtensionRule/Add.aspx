﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：增加卡有效期规则</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                增加卡有效期规则
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                规则类型：
            </td>
            <td width="75%">
                <asp:DropDownList ID="RuleType" TabIndex="1" runat="server" SkinID="NoClass" CssClass="dropdownlist required">
                    <asp:ListItem Text="----------" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="卡有效期延长规则" Value="0"></asp:ListItem>
                    <asp:ListItem Text="金额有效期规则" Value="1"></asp:ListItem>
                    <asp:ListItem Text="积分有效期规则" Value="2">  </asp:ListItem>
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:DropDownList ID="CardTypeID" TabIndex="2" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡等级：
            </td>
            <td>
                <asp:DropDownList ID="CardGradeID" TabIndex="3" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                规则记录序号：
            </td>
            <td>
                <asp:TextBox ID="ExtensionRuleSeqNo" TabIndex="4" runat="server" CssClass="input required digits"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                最大增加数：
            </td>
            <td>
                <asp:TextBox ID="MaxLimit" TabIndex="5" runat="server" CssClass="input required digits"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                增值金额：
            </td>
            <td>
                <asp:TextBox ID="RuleAmount" TabIndex="6" runat="server" CssClass="input required number"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                延长日期：
            </td>
            <td>
                <asp:TextBox ID="Extension" TabIndex="7" runat="server" CssClass="input required number"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效期延长单位：
            </td>
            <td>
                <asp:DropDownList ID="ExtensionUnit" TabIndex="8" runat="server">
                    <asp:ListItem Text="----------" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="永久" Value="0"></asp:ListItem>
                    <asp:ListItem Text="年" Value="1"></asp:ListItem>
                    <asp:ListItem Text="月" Value="2">  </asp:ListItem>
                    <asp:ListItem Text="星期" Value="3"></asp:ListItem>
                    <asp:ListItem Text="天" Value="4">  </asp:ListItem>
                    <asp:ListItem Text="有效期不变更" Value="5">  </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效期延长方式：
            </td>
            <td>
                <asp:DropDownList ID="ExtendType" TabIndex="9" runat="server">
                    <asp:ListItem Text="----------" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="增值时重置有效期" Value="0"></asp:ListItem>
                    <asp:ListItem Text="增值时重置有效期。" Value="1"></asp:ListItem>
                    <asp:ListItem Text="增值时不变更有效期" Value="2">  </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                开始日期：
            </td>
            <td>
                <asp:TextBox ID="StartDate" TabIndex="10" runat="server" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'EndDate\',{d:0});}'})"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                结束日期：
            </td>
            <td>
                <asp:TextBox ID="EndDate" TabIndex="11" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'StartDate\',{d:0});}'})"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:RadioButtonList ID="Status" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="有效" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="无效" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit cancel" OnClick="btnReturn_Click" />
                    <%-- <input type="button" name="button1"  value="返 回" onclick= "history.back(); " class="submit"/>--%>
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
