﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

            }
        }
      

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasCardGradeCode(this.CardGradeCode.Text.Trim(), 0))
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistCardGradeCode, "", Resources.MessageTips.WARNING_TITLE, CardGradeCode.ClientID);
                return;
            }

            if (Edge.Web.Tools.DALTool.isHasCardGradeRank(this.CardGradeRank.Text.Trim(), Tools.ConvertTool.ToInt(this.CardTypeID.SelectedValue), 0))
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistCardGradeRank, "", Resources.MessageTips.WARNING_TITLE, CardGradeRank.ClientID);
                return;
            }

            Edge.SVA.Model.CardGrade item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CardGradeCode = item.CardGradeCode.ToUpper();
                item.CardGradeDiscCeiling = string.IsNullOrEmpty(this.CardGradeDiscCeiling.Text) ? 0 : decimal.Parse(this.CardGradeDiscCeiling.Text.Trim()) / 100;

                item.CardNumMask = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.Trim() : item.CardNumMask;
                item.CardNumPattern = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.Trim() : item.CardNumPattern;
                item.CardNumMask = item.CardNumMask.ToUpper();

                if (item.IsImportUIDNumber.GetValueOrDefault() == 0)
                {
                    //判断是否重复卡规则
                    if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, item.CardTypeID))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }

                    //判断cardtype是否为空，如果为空，必须填写cardgrade的号码规则
                    if ((Tools.DALTool.isEmptyCardNumMaskWithCardType(item.CardTypeID)) && string.IsNullOrEmpty(this.CardNumMask.Text.Trim()))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90293"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }

                    //if ((this.CardNumMask.Enabled == true) && (this.CardNumPattern.Enabled == true))//如果不是继承
                    //{
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, 0))
                        {
                            this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                            return;
                        }
                    //}
                }
                else if (item.IsImportUIDNumber.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, item.CardTypeID))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }

                    //判断cardtype是否为空，如果为空，必须填写cardgrade的号码规则
                    if ((Tools.DALTool.isEmptyCardNumMaskWithCardType(item.CardTypeID)) && string.IsNullOrEmpty(item.CardNumMask))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90293"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }

                    //if ((this.CardNumMask.Enabled == true) && (this.CardNumPattern.Enabled == true))//如果不是继承
                    //{
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, 0))
                        {
                            this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                            return;
                        }
                    //}
                }
            }

            //Html处理
            item.CardGradeNotes = Edge.Common.Utils.ToHtml(this.CardGradeNotes.Text);
            if (Tools.DALTool.Add<Edge.SVA.BLL.CardGrade>(item) > 0)
            {

                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0" + "&tabs=1", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0" + "&tabs=1", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.CardType item = new Edge.SVA.BLL.CardType().GetModel(Tools.ConvertTool.ToInt(this.CardTypeID.SelectedValue));
            if (item != null)
            {
                this.IsImportUIDNumber.SelectedValue = item.IsImportUIDNumber.GetValueOrDefault().ToString();
                this.IsImportUIDNumber.Enabled = false;

                if (this.IsImportUIDNumber.SelectedValue == "1")//导入
                {
                    this.IsConsecutiveUID.SelectedValue = item.IsConsecutiveUID.GetValueOrDefault().ToString();
                    this.UIDCheckDigit.SelectedValue = item.UIDCheckDigit.GetValueOrDefault().ToString();
                    this.UIDToCardNumber.SelectedValue = item.UIDToCardNumber.GetValueOrDefault().ToString();
                    this.IsConsecutiveUID.Enabled = false;
                    this.UIDCheckDigit.Enabled = false;
                    this.UIDToCardNumber.Enabled = false;

                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        this.CardNumMaskImport.Text = item.CardNumMask;
                        this.CardNumPatternImport.Text = item.CardNumPattern;
                    }

                    if ((string.IsNullOrEmpty(item.CardNumMask)) || (string.IsNullOrEmpty(item.CardNumPattern)))
                    {
                        this.CardNumMaskImport.Enabled = true;
                        this.CardNumPatternImport.Enabled = true;
                    }
                    else
                    {
                        this.CardNumMaskImport.Enabled = false;
                        this.CardNumPatternImport.Enabled = false;
                    }
                }
                else//手动
                {
                    this.CardNumMask.Text = item.CardNumMask;
                    this.CardNumPattern.Text = item.CardNumPattern;
                    this.CardCheckdigit.SelectedValue = item.CardCheckdigit.GetValueOrDefault().ToString();
                    this.CheckDigitModeID.SelectedValue = item.CheckDigitModeID.GetValueOrDefault().ToString();
                    this.CardNumberToUID.SelectedValue = item.CardNumberToUID.GetValueOrDefault().ToString();
                    if ((string.IsNullOrEmpty(this.CardNumMask.Text.Trim())) && (string.IsNullOrEmpty(this.CardNumPattern.Text.Trim())))
                    {
                        this.CardNumMask.Enabled = true;
                        this.CardNumPattern.Enabled = true;
                        this.CardCheckdigit.Enabled = true;
                        this.CheckDigitModeID.Enabled = true;
                        this.CardNumberToUID.Enabled = true;
                    }
                    else
                    {
                        this.CardNumMask.Enabled = false;
                        this.CardNumPattern.Enabled = false;
                        this.CardCheckdigit.Enabled = false;
                        this.CheckDigitModeID.Enabled = false;
                        this.CardNumberToUID.Enabled = false;
                    }
                }
            }
        }
    }
}
