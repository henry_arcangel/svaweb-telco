﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardExtensionRule,Edge.SVA.Model.CardExtensionRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string cardGradeId = Request.Params["CardGradeID"];
                this.CardGradeID.SelectedValue = cardGradeId;
                this.CardTypeID.SelectedValue = new Edge.SVA.BLL.CardGrade().GetModel(int.Parse(cardGradeId)).CardTypeID.ToString();
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            Edge.SVA.Model.CardExtensionRule item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CardExtensionRule>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "../Modify.aspx?id="+ Request.Params["CardGradeID"] + "&tabs=2", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "../Modify.aspx?id=" + Request.Params["CardGradeID"] + "&tabs=2", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + Request.Params["CardGradeID"] + "&tabs=2");
        }
    }
}
