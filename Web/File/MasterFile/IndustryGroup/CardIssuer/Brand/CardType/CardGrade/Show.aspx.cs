﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "Show");
                //Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                //Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                //Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);
                CardExtensionRuleRptBind("CardGradeID=" + Request.Params["id"]);
                CardPointRuleRptBind("CardGradeID=" + Request.Params["id"]);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                Edge.SVA.Model.CardType cardType = new Edge.SVA.BLL.CardType().GetModel(Model.CardTypeID);
                string cardTypeText = cardType == null ? "" : DALTool.GetStringByCulture(cardType.CardTypeName1, cardType.CardTypeName2, cardType.CardTypeName3);
                this.CardTypeID.Text = cardType == null ? "" : ControlTool.GetDropdownListText(cardTypeText, cardType.CardTypeCode);

                Edge.SVA.Model.PasswordRule password = new Edge.SVA.BLL.PasswordRule().GetModel(Model.PasswordRuleID.GetValueOrDefault());
                string passwordText = password == null ? "" : DALTool.GetStringByCulture(password.Name1, password.Name2, password.Name3);
                this.PasswordRuleID.Text = password == null ? "" : ControlTool.GetDropdownListText(passwordText, password.PasswordRuleCode);

                Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(Model.CampaignID.GetValueOrDefault());
                string campaignText = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                this.CampaignID.Text = campaign == null ? "" : ControlTool.GetDropdownListText(campaignText, campaign.CampaignCode);

                this.CardPointToAmountRate.Text = ((int)Model.CardPointToAmountRate.GetValueOrDefault()).ToString("N00");       //todo: 数据库类型不正确
                this.CardGradeDiscCeiling.Text = ((int)(Model.CardGradeDiscCeiling.GetValueOrDefault() * 100)).ToString();

                this.CardCheckdigitView.Text = this.CardCheckdigit.SelectedItem == null ? "" : this.CardCheckdigit.SelectedItem.Text;
               // this.CardCheckdigit.Visible = false;

                this.IsAllowStoreValueView.Text = this.IsAllowStoreValue.SelectedItem == null ? "" : this.IsAllowStoreValue.SelectedItem.Text;
                this.IsAllowStoreValue.Visible = false;

                this.IsAllowConsumptionPointView.Text = this.IsAllowConsumptionPoint.SelectedItem == null ? "" : this.IsAllowConsumptionPoint.SelectedItem.Text;
                this.IsAllowConsumptionPoint.Visible = false;

                this.ActiveResetExpiryDateView.Text = this.ActiveResetExpiryDate.SelectedItem == null ? "" : this.ActiveResetExpiryDate.SelectedItem.Text;
                this.ActiveResetExpiryDate.Visible = false;

                this.ForfeitAmountAfterExpiredView.Text = this.ForfeitAmountAfterExpired.SelectedItem == null ? "" : this.ForfeitAmountAfterExpired.SelectedItem.Text;
                this.ForfeitAmountAfterExpired.Visible = false;

                this.ForfeitPointAfterExpiredView.Text = this.ForfeitPointAfterExpired.SelectedItem == null ? "" : this.ForfeitPointAfterExpired.SelectedItem.Text;
                this.ForfeitPointAfterExpired.Visible = false;

                this.CardGradeUpdMethodView.Text = this.CardGradeUpdMethod.SelectedItem == null ? "" : this.CardGradeUpdMethod.SelectedItem.Text;
                this.CardGradeUpdMethod.Visible = false;

                this.CardValidityUnitView.Text = this.CardValidityUnit.SelectedItem == null ? "" : this.CardValidityUnit.SelectedItem.Text;
                this.CardValidityUnit.Visible = false;

                this.GracePeriodUnitView.Text = this.GracePeriodUnit.SelectedItem == null ? "" : this.GracePeriodUnit.SelectedItem.Text;
                this.GracePeriodUnit.Visible = false;

                this.CardPointTransferView.Text = this.CardPointTransfer.SelectedItem == null ? "" : this.CardPointTransfer.SelectedItem.Text;
                this.CardPointTransfer.Visible = false;

                this.CardAmountTransferView.Text = this.CardAmountTransfer.SelectedItem == null ? "" : this.CardAmountTransfer.SelectedItem.Text;
                this.CardAmountTransfer.Visible = false;


                this.IsImportUIDNumberView.Text = this.IsImportUIDNumber.SelectedItem == null ? "" : this.IsImportUIDNumber.SelectedItem.Text;
                //this.IsImportUIDNumber.Visible = false;

                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.UIDCheckDigit.Visible = false;

                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CheckDigitModeID.Visible = false;


                this.CardNumberToUIDView.Text = this.CardNumberToUID.SelectedItem == null ? "" : this.CardNumberToUID.SelectedItem.Text;
                this.CardNumberToUID.Visible = false;

                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.IsConsecutiveUID.Visible = false;

                this.UIDToCardNumberView.Text = this.UIDToCardNumber.SelectedItem == null ? "" : this.UIDToCardNumber.SelectedItem.Text;
                //this.UIDToCardNumber.Visible = false;

                //string type = this.IsImportUIDNumber.SelectedValue.Trim();
                //if (type == "0")
                //{
                //    this.IsConsecutiveUID.Visible = false;
                //    this.IsConsecutiveUIDView.Visible = false;  
                //    this.UIDCheckDigit.Visible = false;
                //    this.UIDCheckDigitView.Visible = false;
                //    this.UIDToCardNumber.Visible = false;
                //    this.UIDToCardNumberView.Visible = false;
                //}
                //else
                //{
                //    this.CardCheckdigit.Visible = false;
                //    this.CardCheckdigitView.Visible = false;
                //    this.CheckDigitModeID.Visible = false;
                //    this.CheckDigitModeIDView.Visible = false;
                //    this.CardNumberToUID.Visible = false;
                //    this.CardNumberToUIDView.Visible = false;
                //    this.CardNumMask.Visible = false;
                //    this.CardNumPattern.Visible = false;
                //}


                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }
            }
        }

        private void CardExtensionRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            this.rptExtensionRuleList.DataSource = ds.Tables[0].DefaultView;
            this.rptExtensionRuleList.DataBind();
        }

        private void CardPointRuleRptBind(string strWhere)
        {

            Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();


            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            this.rptPointRuleList.DataSource = ds.Tables[0].DefaultView;
            this.rptPointRuleList.DataBind();
        }

        
    }
}
