﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryUiPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetjQueryUiCssPath() %>' />
    <script type="text/javascript">
        $(function () {

            jQuery.validator.addMethod("InitBalanceLess", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var maxId = "#CardGradeMaxAmount";
                if ($(maxId).length <= 0 || $(maxId).val().length <= 0) return true;

                var max = parseFloat($(maxId).val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;
            }, jQuery.validator.messages.InitBalanceLess);


            jQuery.validator.addMethod("CardGradeMaxPoint", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var maxId = "#CardGradeMaxPoint";
                if ($(maxId).length <= 0 || $(maxId).val().length <= 0) return true;

                var max = parseFloat($(maxId).val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;
            }, jQuery.validator.messages.CardGradeMaxPoint);

            jQuery.validator.addMethod("MaxPointPreAdd", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var maxId = "#MaxPointPreAdd";
                if ($(maxId).length <= 0 || $(maxId).val().length <= 0) return true;

                var max = parseFloat($(maxId).val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;
            }, jQuery.validator.messages.tMaxPointPreAdd);

            jQuery.validator.addMethod("MaxAmountPreAdd", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var minId = "#MinAmountPreAdd";
                if ($(minId).length <= 0 || $(minId).val().length <= 0) return true;

                var min = parseFloat($(minId).val().replace(/,/g, ""));
                var max = parseFloat(value.replace(/,/g, ""));

                return max >= min;
            }, jQuery.validator.messages.MaxAmountPreAdd);

            jQuery.validator.addMethod("MinAmountPreAdd", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var maxId = "#MaxAmountPreAdd";
                if ($(maxId).length <= 0 || $(maxId).val().length <= 0) return true;

                var max = parseFloat($(maxId).val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;
            }, jQuery.validator.messages.MinAmountPreAdd);

            jQuery.validator.addMethod("MinAmountPreTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#MaxAmountPreTransfer").val().length <= 0) return true;

                var max = parseFloat($("#MaxAmountPreTransfer").val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.MinAmountPreTransfer);

            jQuery.validator.addMethod("MaxAmountPreTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#MinAmountPreTransfer").val().length <= 0) return true;

                var min = parseFloat($("#MinAmountPreTransfer").val().replace(/,/g, ""));
                var max = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.MinAmountPreTransfer);

            jQuery.validator.addMethod("DayMaxAmountTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#DayMaxAmountTransfer").val().length <= 0) return true;

                var max = parseFloat($("#DayMaxAmountTransfer").val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.DayMaxAmountTransfer);

            jQuery.validator.addMethod("MinPointPreTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#MaxPointPreTransfer").val().length <= 0) return true;

                var max = parseFloat($("#MaxPointPreTransfer").val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.MinPointPreTransfer);

            jQuery.validator.addMethod("MaxPointPreTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#MinPointPreTransfer").val().length <= 0) return true;

                var min = parseFloat($("#MinPointPreTransfer").val().replace(/,/g, ""));
                var max = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.MinPointPreTransfer);

            jQuery.validator.addMethod("DayMaxPointTransfer", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#DayMaxPointTransfer").val().length <= 0) return true;

                var max = parseFloat($("#DayMaxPointTransfer").val().replace(/,/g, ""));
                var min = parseFloat(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.DayMaxPointTransfer);

            $(".NumMaskValidImport").keypress(function (event) {
                var keyCode = event.which;
                if (keyCode == 65 || (keyCode == 57) || (keyCode == 97))
                    return true;
                else
                    return false;
            }).focus(function () {
                this.style.imeMode = 'disabled';
            });
            jQuery.validator.addMethod("NumMaskValidImport", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var reg = /^A*9+$/gi;
                var isNumber = /^9+/gi;

                if (/9+/gi.exec(value) && /9+/gi.exec(value).toString().length > 18) {
                    return false;
                }
                if (isNumber.exec(value)) {
                    $(".NumPatternValidImport").val("");
                    $(".NumPatternValidImport").attr('disabled', true);
                    $(".NumPatternValidImport").removeClass("required");

                }
                else {
                    $(".NumPatternValidImport").removeAttr('disabled');
                    $(".NumPatternValidImport").addClass("required");
                }
                return reg.exec(value);
            }, jQuery.validator.messages.NumMaskValid);

            jQuery.validator.addMethod("NumPatternValidImport", function (value, element) {
                var reg = /^A+/gi;
                var mask = $(".NumMaskValidImport").val();
                if (mask.length <= 0) return true;

                var result = reg.exec(mask);
                if (result == null || result.length <= 0) return true;

                return result.toString().length == value.length;

            }, jQuery.validator.messages.NumPatternValid);
            //表单验证JS
            $("#form1").validate({

                errorElement: "span",
                success: function (label) {
                    label.text(" ").addClass("success");
                }
            });

            //是否可以设置界限值
            $("#CardGradeUpdMethod,#CardValidityUnit,#CardPointTransfer,#CardAmountTransfer").change(function () {
                setControls();
            });

            setControls();
            tabs(1);

            $("#IsAllowStoreValue").click(function () {
                checkIsAllowStoreValue();
            });

            $("#IsAllowConsumptionPoint").click(function () {
                checkIsAllowConsumptionPoint();
            });

            checkIsAllowStoreValue();
            checkIsAllowConsumptionPoint();

            $("input[name='IsImportUIDNumber']").change(function () {
                checkCreateCardType();
            });

            checkCreateCardType();

            var cd = new Object();
            $("#CardNumberToUID option").each(function () {
                if ($(this).val() == "2") cd.Delete = $("#CardNumberToUID").find("option[value='2']").text();
                if ($(this).val() == "3") cd.Add = $("#CardNumberToUID").find("option[value='3']").text();
            });
            $("input[name='CardCheckdigit']").change(function () {
                checkDigit(cd);
            });
            $("input[name='UIDCheckDigit']").change(function () {
                checkDigitUID(cd);
            });

            $("#UIDToCardNumber").change(function () {
                checkBinding(cd);
            });
            checkBinding(cd);
        });

        function setControls() {
            var CardGradeUpdMethod = $("#CardGradeUpdMethod").val();
            if (CardGradeUpdMethod == '0') {
                $("#CardGradeUpdThreshold").removeClass("required");
                $("#CardGradeUpdThreshold").attr("disabled", "disabled");
                $("#CardGradeUpdThreshold").next(".star").hide();
                $("#CardGradeUpdThreshold").val("");

            }
            else {
                $("#CardGradeUpdThreshold").addClass("required");
                $("#CardGradeUpdThreshold").removeAttr("disabled");
                $("#CardGradeUpdThreshold").next(".star").show();
            }

            var CardValidityUnit = $("#CardValidityUnit").val();
            if ((CardValidityUnit == '0') || (CardValidityUnit == '5')) {
                $("#CardValidityDuration").removeClass("required");
                $("#CardValidityDuration").attr("disabled", "disabled");
                $("#CardValidityDuration").next(".star").hide();
            }
            else {
                $("#CardValidityDuration").addClass("required");
                $("#CardValidityDuration").removeAttr("disabled");
                $("#CardValidityDuration").next(".star").show()
            }

            var CardAmountTransfer = $("#CardPointTransfer").val();
            if (CardAmountTransfer == '0') {
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").removeClass("required");
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").attr("disabled", "disabled");
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").next(".star").hide();
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").val("");
            }
            else {
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").addClass("required");
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").removeAttr("disabled");
                $("#MinPointPreTransfer,#MaxPointPreTransfer,#DayMaxPointTransfer").next(".star").show();
            }

            var CardAmountTransfer = $("#CardAmountTransfer").val();
            if (CardAmountTransfer == '0') {
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").removeClass("required");
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").attr("disabled", "disabled");
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").next(".star").hide();
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").val("");
            }
            else {
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").addClass("required");
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").removeAttr("disabled");
                $("#MinAmountPreTransfer,#MaxAmountPreTransfer,#DayMaxAmountTransfer").next(".star").show();
            }

        }


        //是否允许充值
        function checkIsAllowStoreValue() {
            var method = $("input[name='IsAllowStoreValue']:checked").val();
            if (method == '0') {
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").val("");
                if ($("#IsAllowStoreValue").attr("disabled")) { //判断可用
                    $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").removeAttr('disabled');
                }
                else {
                    $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").attr('disabled', ' true');
                }
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").removeClass("required");
                $(".storeValue").hide();
            }
            else {
                if ($("#IsAllowStoreValue").attr("disabled")) { //判断可用
                    $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").attr('disabled', ' true');
                }
                else {
                    $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").removeAttr('disabled');
                }
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").addClass("required");
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").each(function () {
                    if ($(this).val().length <= 0) $(this).val("0.00");
                });

                $(".storeValue").show();
            }
        }

        //是否允许积分
        function checkIsAllowConsumptionPoint() {
            var method = $("input[name='IsAllowConsumptionPoint']:checked").val();
            if (method == '0') {
                $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").val("");
                if ($("#IsAllowConsumptionPoint").attr("disabled")) { //判断可用
                    $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").removeAttr('disabled');
                }
                else {
                    $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").attr('disabled', ' true');
                }
                $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").removeClass("required svaPoint");
                $(".consumptionPoint").hide();
            }
            else {
                if ($("#IsAllowConsumptionPoint").attr("disabled")) { //判断可用
                    $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").attr('disabled', ' true');
                }
                else {
                    $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").removeAttr('disabled');
                }
                $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").addClass("required svaPoint");
                $("#CardGradeMaxPoint,#MinPointPreAdd,#MaxPointPreAdd").each(function () {
                    if ($(this).val().length <= 0) $(this).val("0");
                });
                $(".consumptionPoint").show();
            }
        }

        //卡创建类型
        function checkCreateCardType() {
            var isImportCard = $("input[name='IsImportUIDNumber']:checked").val();
            if (isImportCard == '0') //手动创建
            {
                $("#CardCheckdigit,#CheckDigitModeID,#CardNumberToUID,#CardNumMask,#CardNumPattern").addClass("required");
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCardNumber,#CardNumMaskImport,#CardNumPatternImport").removeClass("required");

                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();

                if ($("input[name='CardCheckdigit']:checked").val() == "0") $("#CheckDigitModeTR").hide();
                else $("#CheckDigitModeTR").show();
                checkBinding();

            }
            else {//导入
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCardNumber").addClass("required");
                $("#CardCheckdigit,#CheckDigitModeID,#CardNumberToUID,#CardNumMask,#CardNumPattern").removeClass("required");

                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();
                checkBinding();
            }
        }

        //Manual CheckDigit
        function checkDigit(cd) {
            var method = $("input[name='CardCheckdigit']:checked").val();
            if (method == "1") {
                if ($("#CardNumberToUID option[value='3']").length > 0) {
                    $("#CardNumberToUID option[value='3']").remove();
                }
                $("#CheckDigitModeTR").show();
                if ($("#CardNumberToUID option[value='2']").length <= 0) $("#CardNumberToUID").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#CardNumberToUID option[value='2']").length > 0) {
                    $("#CardNumberToUID option[value='2']").remove();
                }
                $("#CheckDigitModeTR").hide();
                if ($("#CardNumberToUID option[value='3']").length <= 0) $("#CardNumberToUID").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        //Import CheckDigit
        function checkDigitUID(cd) {
            var method = $("input[name='UIDCheckDigit']:checked").val();
            if (method == "1") {
                if ($("#UIDToCardNumber option[value='3']")) {
                    $("#UIDToCardNumber option[value='3']").remove();
                }
                if ($("#UIDToCardNumber option[value='2']").length <= 0) $("#UIDToCardNumber").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#UIDToCardNumber option[value='2']")) {
                    $("#UIDToCardNumber option[value='2']").remove();
                }
                if ($("#UIDToCardNumber option[value='3']").length <= 0) $("#UIDToCardNumber").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        function checkBinding(cd) {
            var isImport = $("input[name='IsImportUIDNumber']:checked").val();
            var selected = $("#UIDToCardNumber").val();
            if (isImport == "0") {
                $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
                checkDigit(cd);
            }
            else {
                if (selected == "0") {
                    $("#CardNumMaskImport,#CardNumPatternImport").addClass("required");
                    $("#CardNumMaskImportTR,#CardNumPatternImportTR").show();
                    $(".ImportRolesNumber").show();

                }
                else {
                    $("#CardNumMaskImport,#CardnNumPatternImport").removeClass("required");
                    $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
                    $(".ImportRolesNumber").hide();
                }
                checkDigitUID(cd);
            }
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="tabs">
        <ul>
            <li onclick="tabs(1);"><a><span>基本信息</span></a></li>
        </ul>
    </div>
    <div class="fragment">
        <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    基本信息
                </th>
            </tr>
            <tr>
                <td width="25%" align="right">
                    卡级别编号：
                </td>
                <td width="75%">
                    <asp:TextBox ID="CardGradeCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode"
                        hinttitle="卡级别编号" hintinfo="Translate__Special_121_StartKey identifier of Card Grade. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span
                            class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    描述：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required"
                        hinttitle="描述" hintinfo="请输入规范的卡級別名称。不能超過512個字符"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述1：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input "
                        hinttitle="其他描述1" hintinfo="對卡級別的描述,不能超過512個字符"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述2：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input "
                        hinttitle="其他描述2" hintinfo="對卡級別的另一個描述,不能超過512個字符"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡类型：
                </td>
                <td>
                    <asp:DropDownList ID="CardTypeID" TabIndex="5" runat="server" CssClass="dropdownlist required"
                        AutoPostBack="True" OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged">
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    条款条例：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeNotes" TabIndex="6" runat="server" TextMode="MultiLine"
                        CssClass="input required" Height="100" hinttitle="条款条例" hintinfo="请输入条框条例的描述，不能超过512位"></asp:TextBox><span
                            class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡图片：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CardGradePicFile" TabIndex="7" runat="server" SubSaveFilePath="Images/CardGrade"
                        FileType="pictures" CssClass="required" HintTitle="卡图片" HintInfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡设计模板：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CardGradeLayoutFile" TabIndex="8" runat="server" SubSaveFilePath="Images/CardGrade"
                        FileType="files" CssClass="required" HintTitle="卡设计模板" HintInfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡结算单模板：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CardGradeStatementFile" TabIndex="9" runat="server" SubSaveFilePath="Images/CardGrade"
                        FileType="files" CssClass="required" HintTitle="卡结算单模板" HintInfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    密码规则：
                </td>
                <td>
                    <asp:DropDownList ID="PasswordRuleID" runat="server" TabIndex="9" CssClass="dropdownlist required"
                        SkinID="NoClass">
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    活动：
                </td>
                <td>
                    <asp:DropDownList ID="CampaignID" TabIndex="9" runat="server" CssClass="dropdownlist "
                        SkinID="NoClass">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡级别等级：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeRank" TabIndex="9" runat="server" MaxLength="2" CssClass="input required digits"
                        hinttitle="卡級別序號" hintinfo="作为卡之间级别高低的定义。数字越大，级别越高。"></asp:TextBox> <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    卡号规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    卡号码是否导入：
                </td>
                <td>
                    <asp:RadioButtonList ID="IsImportUIDNumber" runat="server" CssClass="required" TabIndex="9"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <th colspan="2" align="left">
                    手动创建编号规则
                </th>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号编码规则：
                </td>
                <td>
                    <asp:TextBox ID="CardNumMask" TabIndex="9" runat="server" MaxLength="20" CssClass="input required inputUpper NumMaskValid"
                        onpaste="return false" oncontextmenu="return false;" hinttitle="卡号编码规则" hintinfo="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号前缀号码：
                </td>
                <td>
                    <asp:TextBox ID="CardNumPattern" TabIndex="9" runat="server" CssClass="input required digits NumPatternValid"
                        hinttitle="卡号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号是否添加校验位：
                </td>
                <td>
                    <asp:RadioButtonList ID="CardCheckdigit" runat="server" CssClass="required" TabIndex="9"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="CheckDigitModeTR" class="ManualRoles">
                <td align="right">
                    校验位计算方法：
                </td>
                <td>
                    <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="10" CssClass="dropdownlist"
                        SkinID="NoClass">
                        <asp:ListItem Value="1" Text="EAN13"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:DropDownList ID="CardNumberToUID" runat="server" TabIndex="11" CssClass="dropdownlist"
                        SkinID="NoClass">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <th colspan="2" align="left">
                    导入物理编号规则
                </th>
            </tr>
            <tr class="ImportRoles" id="CardNumMaskImportTR">
                <td align="right">
                    卡号编码规则：
                </td>
                <td>
                    <asp:TextBox ID="CardNumMaskImport" TabIndex="8" runat="server" CssClass="input inputUpper NumMaskValidImport" MaxLength="20"
                        onpaste="return false" oncontextmenu="return false;" hinttitle="卡号编码规则" hintinfo="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999Translate__Special_121_End"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ImportRoles" id="CardNumPatternImportTR">
                <td align="right">
                    卡号前缀号码：
                </td>
                <td>
                    <asp:TextBox ID="CardNumPatternImport" TabIndex="9" runat="server" CssClass="input digits NumPatternValidImport"
                        hinttitle="卡号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="10"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="10"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:DropDownList ID="UIDToCardNumber" runat="server" TabIndex="11" CssClass="dropdownlist"
                        SkinID="NoClass">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    使用规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    发行品牌/店铺：
                </td>
                <td>
                    <%--<asp:TextBox ID="EarnStoreCondition" TabIndex="18" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
                    <%--           <a id="store1" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=0&type=2&storetype=1&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        添加</a>--%>
                    <span style="font-style: italic;">请新增成功卡级别后添加该项。</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    使用品牌/店铺：
                </td>
                <td>
                    <%--<asp:TextBox ID="ConsumeStoreCondition" TabIndex="19" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
                    <%--          <a id="store2" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=0&type=2&storetype=2&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        添加</a>--%>
                    <span style="font-style: italic;">请新增成功卡级别后添加该项。</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否允许储值：
                </td>
                <td>
                    <asp:RadioButtonList ID="IsAllowStoreValue" TabIndex="10" runat="server" CssClass="required"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="是"></asp:ListItem>
                        <asp:ListItem Value="0" Selected="True" Text="否"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否允许积分：
                </td>
                <td>
                    <asp:RadioButtonList ID="IsAllowConsumptionPoint" TabIndex="11" runat="server" CssClass="required"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1" Text="是"></asp:ListItem>
                        <asp:ListItem Value="0" Text="否"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    初始金额：
                </td>
                <td>
                    <asp:TextBox ID="CardTypeInitAmount" TabIndex="11" runat="server" MaxLength="20"
                        Text="0.00" CssClass="input required svaAmount InitBalanceLess" hinttitle="初始金额"
                        hintinfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    初始积分：
                </td>
                <td>
                    <asp:TextBox ID="CardTypeInitPoints" TabIndex="12" runat="server" MaxLength="20"
                        Text="0" CssClass="input required svaPoint CardGradeMaxPoint" hinttitle="初始积分" hintinfo="请输入正整數，不允许输入小数"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡最大現金賬套金額：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeMaxAmount" TabIndex="13" runat="server" MaxLength="512"
                        Text="0.00" CssClass="input svaAmount" hinttitle="卡最大現金賬套金額" hintinfo="请输入正數，保留兩位小數"></asp:TextBox><span
                            id="star1" class="star storeValue">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡最大积分帐套积分：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeMaxPoint" TabIndex="13" runat="server" MaxLength="512"
                        Text="0" CssClass="input svaPoint" hinttitle="卡最大积分帐套积分" hintinfo="必須输入數字，並且是整數"></asp:TextBox><span
                            class="star consumptionPoint">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最低充值现金帐套金额：
                </td>
                <td>
                    <asp:TextBox ID="MinAmountPreAdd" TabIndex="14" runat="server" Text="0.00" CssClass="input svaAmount InitBalanceLess MinAmountPreAdd"
                        hinttitle="最低充值现金帐套金额" hintinfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star storeValue">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最低充值积分帐套积分：
                </td>
                <td>
                    <asp:TextBox ID="MinPointPreAdd" TabIndex="14" runat="server" Text="0" CssClass="input MaxPointPreAdd CardGradeMaxPoint svaPoint "
                        hinttitle="最低充值积分帐套积分" hintinfo="必須输入數字，並且是整數"></asp:TextBox><span class="star consumptionPoint">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最高充值现金帐套金额：
                </td>
                <td>
                    <asp:TextBox ID="MaxAmountPreAdd" TabIndex="15" runat="server" Text="0.00" CssClass="input svaAmount InitBalanceLess MaxAmountPreAdd"
                        hinttitle="最高充值现金帐套金额" hintinfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star storeValue">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最高充值积分帐套积分：
                </td>
                <td>
                    <asp:TextBox ID="MaxPointPreAdd" TabIndex="14" runat="server" Text="0" CssClass="input CardGradeMaxPoint svaPoint "
                        hinttitle="最高充值积分帐套积分" hintinfo="必須输入數字，並且是整數"></asp:TextBox><span class="star consumptionPoint">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    折扣最高值：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeDiscCeiling" TabIndex="16" runat="server" MaxLength="20"
                        Text="0" CssClass="input required svaDiscount" HintTitle="折扣最高值" HintInfo="请输入0~100的整數，不允許輸入小數"></asp:TextBox><span
                            class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小消费积分（每次）：
                </td>
                <td>
                    <asp:TextBox ID="CardConsumeBasePoint" TabIndex="17" runat="server" MaxLength="20"
                        Text="0" CssClass="input required svaPoint" HintTitle="消费积分的最低值" HintInfo="必須输入數字，並且是整數"></asp:TextBox><span
                            class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小剩余积分：
                </td>
                <td>
                    <asp:TextBox ID="MinBalancePoint" TabIndex="18" runat="server" MaxLength="20" CssClass="input required svaPoint"
                        Text="0" HintTitle="最小剩余积分" HintInfo="请输入正整數，不允许输入小数"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小消费金额（每次）：
                </td>
                <td>
                    <asp:TextBox ID="MinConsumeAmount" TabIndex="18" runat="server" MaxLength="20" CssClass="input required svaAmount"
                        Text="0.00" HintTitle="最小消费金额(每一次)" HintInfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小剩余金额：
                </td>
                <td>
                    <asp:TextBox ID="MinBalanceAmount" TabIndex="18" runat="server" MaxLength="20" CssClass="input required svaAmount"
                        Text="0.00" HintTitle="最小剩余金额" HintInfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    过期后是否清空帐套金额：
                </td>
                <td>
                    <asp:RadioButtonList ID="ForfeitAmountAfterExpired" TabIndex="18" runat="server"
                        CssClass="required" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">不清零</asp:ListItem>
                        <asp:ListItem Value="1">清零</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    过期后是否清空帐套积分：
                </td>
                <td>
                    <asp:RadioButtonList ID="ForfeitPointAfterExpired" TabIndex="18" runat="server" CssClass="required"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">不清零</asp:ListItem>
                        <asp:ListItem Value="1">清零</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    升级规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    升级条件：
                </td>
                <td>
                    <asp:DropDownList ID="CardGradeUpdMethod" TabIndex="19" runat="server" CssClass="required">
                        <asp:ListItem Selected="True" Value="0" title="无">无</asp:ListItem>
                        <asp:ListItem Value="3" title="单笔消费">单笔消费</asp:ListItem>
                        <asp:ListItem Value="1" title="年累积购买值大于">年累积购买值大于</asp:ListItem>
                        <asp:ListItem Value="2" title="年累计积分值大于">年累计积分值大于</asp:ListItem>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    界限值：
                </td>
                <td>
                    <asp:TextBox ID="CardGradeUpdThreshold" TabIndex="20" runat="server" CssClass="input svaAmount"
                        HintTitle="界限值" HintInfo="请输入正數，最大兩位小數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效期规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    <div align="right">
                        有效期长度：</div>
                </td>
                <td>
                    <asp:TextBox ID="CardValidityDuration" TabIndex="21" runat="server" CssClass="input digits"
                        MaxLength="3" hinttitle="有效期长度" hintinfo="必須输入數字，並且是整數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    有效期长度单位：
                </td>
                <td>
                    <asp:DropDownList ID="CardValidityUnit" TabIndex="22" runat="server" CssClass="required">
                        <%--<asp:ListItem Value="0" Selected="True" title="永久">永久</asp:ListItem>--%>
                        <asp:ListItem Value="1" title="年">年</asp:ListItem>
                        <asp:ListItem Value="2" title="月">月</asp:ListItem>
                        <asp:ListItem Value="3" title="星期">星期</asp:ListItem>
                        <asp:ListItem Value="4" title="天">天</asp:ListItem>
                        <%--<asp:ListItem Value="5" title="有效期不变更">有效期不变更</asp:ListItem>--%>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    激活是否重置有效期：
                </td>
                <td>
                    <asp:RadioButtonList ID="ActiveResetExpiryDate" runat="server" TabIndex="23" CssClass="required"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <div align="right">
                        宽限期值：</div>
                </td>
                <td>
                    <asp:TextBox ID="GracePeriodValue" TabIndex="23" runat="server" CssClass="input required digits"
                        MaxLength="3" hinttitle="宽限期值" hintinfo="必須输入數字，並且是整數">0</asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    宽限期单位：
                </td>
                <td>
                    <asp:DropDownList ID="GracePeriodUnit" TabIndex="23" runat="server" CssClass="required">
                        <%--<asp:ListItem Value="0" Selected="True" title="永久">永久</asp:ListItem>--%>
                        <asp:ListItem Value="1" title="年">年</asp:ListItem>
                        <asp:ListItem Value="2" title="月">月</asp:ListItem>
                        <asp:ListItem Value="3" title="星期">星期</asp:ListItem>
                        <asp:ListItem Value="4" title="天">天</asp:ListItem>
                        <%--<asp:ListItem Value="5" title="有效期不变更">有效期不变更</asp:ListItem>--%>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    转赠/转换规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    积分转换现金规则：
                </td>
                <td>
                    <asp:TextBox ID="CardPointToAmountRate" TabIndex="24" runat="server" CssClass="input required svaPoint"
                        Text="0" HintTitle="积分转换现金规则" HintInfo="必須输入數字，並且是整數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    现金转换积分规则：
                </td>
                <td>
                    <asp:TextBox ID="CardAmountToPointRate" TabIndex="25" runat="server" CssClass="input required svaAmount "
                        Text="0" HintTitle="现金转换积分规则" HintInfo="请输入正數，保留兩位小數"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    积分转换规则（转出）：
                </td>
                <td>
                    <asp:DropDownList ID="CardPointTransfer" TabIndex="26" runat="server" CssClass="required">
                        <asp:ListItem Selected="True" Value="0" title="不允许">不允许</asp:ListItem>
                        <asp:ListItem Value="1" title="允许同卡级别">允许同卡级别</asp:ListItem>
                        <asp:ListItem Value="2" title="允许同卡类型">允许同卡类型</asp:ListItem>
                        <asp:ListItem Value="3" title="允许同品牌">允许同品牌</asp:ListItem>
                        <asp:ListItem Value="4" title="允许同发行商">允许同发行商</asp:ListItem>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最小转换积分：
                </td>
                <td>
                    <asp:TextBox ID="MinPointPreTransfer" TabIndex="27" runat="server" CssClass="input svaPoint MinPointPreTransfer DayMaxPointTransfer"
                        HintTitle="每笔交易最小转换积分" HintInfo="必須输入數字，並且是整數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最大转换积分：
                </td>
                <td>
                    <asp:TextBox ID="MaxPointPreTransfer" TabIndex="28" runat="server" CssClass="input svaPoint MaxPointPreTransfer DayMaxPointTransfer"
                        HintTitle="每笔交易最大转换积分" HintInfo="必須输入數字，並且是整數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每天最大转赠积分：
                </td>
                <td>
                    <asp:TextBox ID="DayMaxPointTransfer" TabIndex="29" runat="server" CssClass="input svaPoint"
                        HintTitle="每天最大转赠积分" HintInfo="必須输入數字，並且是整數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    现金转换规则：
                </td>
                <td>
                    <asp:DropDownList ID="CardAmountTransfer" TabIndex="30" runat="server" CssClass="required">
                        <asp:ListItem Selected="True" Value="0" title="不允许">不允许</asp:ListItem>
                        <asp:ListItem Value="1" title="允许同卡级别">允许同卡级别</asp:ListItem>
                        <asp:ListItem Value="2" title="允许同卡类型">允许同卡类型</asp:ListItem>
                        <asp:ListItem Value="3" title="允许同品牌">允许同品牌</asp:ListItem>
                        <asp:ListItem Value="4" title="允许同发行商">允许同发行商</asp:ListItem>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最小转换金额：
                </td>
                <td>
                    <asp:TextBox ID="MinAmountPreTransfer" TabIndex="31" runat="server" CssClass="input svaAmount MinAmountPreTransfer DayMaxAmountTransfer"
                        HintTitle="每笔交易最小转换金额" HintInfo="请输入正數，保留兩位小數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最大转换金额：
                </td>
                <td>
                    <asp:TextBox ID="MaxAmountPreTransfer" TabIndex="32" runat="server" CssClass="input svaAmount MaxAmountPreTransfer DayMaxAmountTransfer"
                        HintTitle="每笔交易最大转换金额" HintInfo="请输入正數，保留兩位小數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每天最大转赠金额：
                </td>
                <td>
                    <asp:TextBox ID="DayMaxAmountTransfer" TabIndex="33" runat="server" CssClass="input svaAmount"
                        HintTitle="每天最大转赠金额" HintInfo="请输入正數，保留兩位小數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <asp:Button ID="btnAdd" runat="server" Text="提 交" OnClick="btnAdd_Click" CssClass="submit">
                        </asp:Button>
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="showMessage" colspan="2">
                    *为必填项
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
