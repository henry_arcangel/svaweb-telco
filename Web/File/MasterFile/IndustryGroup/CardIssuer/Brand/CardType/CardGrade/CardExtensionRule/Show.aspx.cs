﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardExtensionRule,Edge.SVA.Model.CardExtensionRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "Show");
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());
            }
        }
    }
}
