﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：增加积分规则</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                增加积分规则
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                规则编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="PointRuleSeqNo" TabIndex="1" runat="server" SkinID="NoClass" CssClass="input required digits"
                    Width="249px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:DropDownList ID="CardTypeID" TabIndex="2" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡等级：
            </td>
            <td>
                <asp:DropDownList ID="CardGradeID" TabIndex="3" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                计算方式设置：
            </td>
            <td>
                <asp:TextBox ID="PointRuleOper" TabIndex="4" runat="server" CssClass="input required digits"
                    Text="0" HintTitle="计算方式设置" HintInfo="Translate__Special_121_Start注意：0：每当消费PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分。  如：PointRuleAmount=100，PointRulePoints=10， 消费2000， 就可以获得 （2000 /100 ）* 10 = 2001：当消费金额大于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分2：当消费金额小于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                设定的积分规则金额：
            </td>
            <td>
                <asp:TextBox ID="PointRuleAmount" TabIndex="5" runat="server" CssClass="input required number"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                设定的积分规则积分：
            </td>
            <td>
                <asp:TextBox ID="PointRulePoints" TabIndex="6" runat="server" CssClass="input required digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                开始日期：
            </td>
            <td>
                <asp:TextBox ID="StartDate" TabIndex="10" runat="server" onClick="WdatePicker()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                结束日期：
            </td>
            <td>
                <asp:TextBox ID="EndDate" TabIndex="11" runat="server" onClick="WdatePicker()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit cancel" OnClick="btnReturn_Click" />
                    <%--<input type="button" name="button1"  value="返 回" onclick= "history.back(); " class="submit"/>--%>
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
