﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.Show" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <script type="text/javascript">
        $(function () {

            checkIsImportCardNumber();
        });


        //是否导入优惠券
        function checkIsImportCardNumber() {
            var method = $("input[name='IsImportUIDNumber']:checked").val();
            if (method == '1') { //导入
                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();
                if ($("#UIDToCardNumber").val() != "0") $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
            }
            else {//手动

                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();

                checkCheckDigitMode();
            }

            $("#IsImportUIDNumber,#UIDToCardNumber,#CardCheckdigit").hide();
        }


        // //卡创建类型
        function checkCheckDigitMode() {
            var isCardCheckdigit = $("input[name='CardCheckdigit']:checked").val();
            if (isCardCheckdigit == '0') //没有Check
            {
                $("#msgtable tr[class=CheckDigitMode]").hide();
            }
            else {
                $("#msgtable tr[class=CheckDigitMode]").show();
            }
        }

        // //卡创建类型
        function checkCheckDigitMode() {
            var isCardCheckdigit = $("input[name='CardCheckdigit']:checked").val();

            if (isCardCheckdigit == '0') //没有Check
            {
                $("#CheckDigitModeTR").hide();
            }
            else {
                $("#CheckDigitModeTR").show();
            }
        }

    </script>
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                基本资料
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡类型编号：
            </td>
            <td width="75%">
                <asp:Label ID="CardTypeCode" TabIndex="2" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                描述：
            </td>
            <td>
                <asp:Label ID="CardTypeName1" TabIndex="3" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:Label ID="CardTypeName2" TabIndex="4" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:Label ID="CardTypeName3" TabIndex="5" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:Label ID="BrandID" TabIndex="10" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                币种：
            </td>
            <td>
                <asp:Label ID="CurrencyID" TabIndex="11" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                卡号规则
            </th>
        </tr>
        <tr>
            <td align="right">
                卡号码是否导入：
            </td>
            <td>
                <asp:Label ID="IsImportUIDNumberView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="IsImportUIDNumber" runat="server" CssClass="required" TabIndex="7"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ManualRoles">
            <th colspan="2" align="left">
                手动创建编号规则
            </th>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号编码规则：
            </td>
            <td>
                <asp:Label ID="CardNumMask" TabIndex="6" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号前缀号码：
            </td>
            <td>
                <asp:Label ID="CardNumPattern" TabIndex="7" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号是否添加校验位：
            </td>
            <td>
                <asp:Label ID="CardCheckdigitView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CardCheckdigit" runat="server" TabIndex="13" Enabled="false"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="CheckDigitModeTR" class="ManualRoles">
            <td align="right">
                校验位计算方法：
            </td>
            <td>
                <asp:Label ID="CheckDigitModeIDView" runat="server"></asp:Label>
                <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass" Enabled="false">
                    <asp:ListItem Value="1" Text="EAN13"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="CardNumberToUIDView" runat="server"></asp:Label>
                <asp:DropDownList ID="CardNumberToUID" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass" Enabled="false">
                    <asp:ListItem Value="1">全部复制</asp:ListItem>
                    <asp:ListItem Value="0">绑定</asp:ListItem>
                    <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                    <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <th colspan="2" align="left">
                导入物理编号规则
            </th>
        </tr>
        <tr class="ImportRoles" id="CardNumMaskImportTR">
            <td align="right">
                卡号编码规则：
            </td>
            <td>
                <asp:Label ID="CardNumMaskImport" TabIndex="6" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="ImportRoles" id="CardNumPatternImportTR">
            <td align="right">
                卡号前缀号码：
            </td>
            <td>
                <asp:Label ID="CardNumPatternImport" TabIndex="7" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="IsConsecutiveUIDView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="11"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="UIDCheckDigitView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="11"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="UIDToCardNumberView" runat="server"></asp:Label>
                <asp:DropDownList ID="UIDToCardNumber" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass" Enabled="false">
                    <asp:ListItem Value="1">全部复制</asp:ListItem>
                    <asp:ListItem Value="0">绑定</asp:ListItem>
                    <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                    <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                使用规则
            </th>
        </tr>
        <tr>
            <td align="right">
                是否必须指定会员：
            </td>
            <td>
                <asp:Label ID="CardMustHasOwnerView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CardMustHasOwner" runat="server" TabIndex="12" Enabled="false"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                现金帐套是否有多重有效期：
            </td>
            <td>
                <asp:Label ID="CashExpiredateView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CashExpiredate" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                积分帐套是否有多重有效期：
            </td>
            <td>
                <asp:Label ID="PointExpiredateView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="PointExpiredate" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否需要写卡：
            </td>
            <td>
                <asp:Label ID="IsPhysicalCardView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="IsPhysicalCard" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left" class="style1">
                有效性
            </th>
        </tr>
        <tr>
            <td align="right">
                卡类型启用日期：
            </td>
            <td>
                <asp:Label ID="CardTypeStartDate" runat="server" TabIndex="18"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡类型结束日期：
            </td>
            <td>
                <asp:Label ID="CardTypeEndDate" runat="server" TabIndex="19"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                其他信息
            </th>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                卡级别列表
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptCardGradeList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="20%">
                                    卡级别编号
                                </th>
                                <th width="20%">
                                    卡级别等级
                                </th>
                                <th width="60%">
                                    卡级别描述
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblCardGraddCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardgradeRank" runat="server" Text='<%#Eval("CardgradeRank")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGradeName1" runat="server" Text='<%#Eval("CardGradeName1")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div>
                    <div class="right">
                        <webdiyer:AspNetPager ID="rptCardGradeListPager" runat="server" CustomInfoTextAlign="Left"
                            FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                            LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                            PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" TextAfterPageIndexBox=""
                            TextBeforePageIndexBox="" OnPageChanged="rptCardGradeListPager_PageChanged" CssClass="asppager"
                            CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                            CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                            LayoutType="Table">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
