﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.Modify" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        $(function () {

            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
            $(".NumMaskValidImport,.NumMaskValidManual").keypress(function (event) {
                var keyCode = event.which;
                if (keyCode == 65 || (keyCode == 57) || (keyCode == 97))
                    return true;
                else
                    return false;
            }).focus(function () {
                this.style.imeMode = 'disabled';
            });
            jQuery.validator.addMethod("NumMaskValidImport", function (value, element) {
                var pattern = $(".NumPatternValidImport").val();
                if (pattern.length <= 0 && value.length <= 0) {
                    $(".NumMaskValidImport,.NumPatternValidImport").removeClass("required");
                    return true;
                }
                else if (pattern.length > 0 && value.length <= 0) {
                    return false;
                }
                else if (value.length > 0 && /9+/gi.exec(value) && /9+/gi.exec(value).toString().length > 18) {
                    return false;
                }

                if (/^9+/gi.exec(value)) {
                    $(".NumPatternValidImport").val("");
                    $(".NumPatternValidImport").attr('disabled', true);
                    $(".NumPatternValidImport").removeClass("required");
                }
                else {
                    $(".NumPatternValidImport").removeAttr('disabled');
                    $(".NumPatternValidImport").addClass("required");
                }
                return /^A*9+$/gi.exec(value);
            }, jQuery.validator.messages.NumMaskValid);

            jQuery.validator.addMethod("NumPatternValidImport", function (value, element) {

                var mask = $(".NumMaskValidImport").val();
                if (mask.length <= 0 && value.length <= 0) {
                    $(".NumMaskValidImport,.NumPatternValidImport").removeClass("required");
                    return true;
                }
                else if (mask.length > 0 && /^9+/gi.exec(mask)) {
                    return true;
                }
                else if (mask.length > 0 && value.length <= 0) {
                    return false;
                }

                var result = /^A+/gi.exec(mask);
                if (result == null || result.length <= 0) return true;

                return result.toString().length == value.length;

            }, jQuery.validator.messages.NumPatternValid);

            jQuery.validator.addMethod("NumMaskValidManual", function (value, element) {
                var pattern = $(".NumPatternValidManual").val();
                if (pattern.length <= 0 && value.length <= 0) {
                    $(".NumMaskValidManual,.NumPatternValidManual").removeClass("required");
                    return true;
                }
                else if (pattern.length > 0 && value.length <= 0) {
                    return false;
                }
                else if (value.length > 0 && /9+/gi.exec(value) && /9+/gi.exec(value).toString().length > 18) {
                    return false;
                }

                if (/^9+/gi.exec(value)) {
                    $(".NumPatternValidManual").val("");
                    $(".NumPatternValidManual").attr('disabled', true);
                    $(".NumPatternValidManual").removeClass("required");
                }
                else {
                    $(".NumPatternValidManual").removeAttr('disabled');
                    $(".NumPatternValidManual").addClass("required");
                }
                return /^A*9+$/gi.exec(value);
            }, jQuery.validator.messages.NumMaskValid);

            jQuery.validator.addMethod("NumPatternValidManual", function (value, element) {
                var mask = $(".NumMaskValidManual").val();
                if (mask.length <= 0 && value.length <= 0) {
                    $(".NumMaskValidManual,.NumPatternValidManual").removeClass("required");
                    return true;
                }
                else if (mask.length > 0 && /^9+/gi.exec(mask)) {
                    return true;
                }
                else if (mask.length > 0 && value.length <= 0) {
                    return false;
                }
                var result = /^A+/gi.exec(mask);
                if (result == null || result.length <= 0) return true;

                return result.toString().length == value.length;

            }, jQuery.validator.messages.NumPatternValid);

            $("input[name=IsImportUIDNumber]").change(function () {
                checkCreateCardType();
            });
            checkCreateCardType();

            var cd = new Object();
            $("#CardNumberToUID option").each(function () {
                if ($(this).val() == "2") cd.Delete = $("#CardNumberToUID").find("option[value='2']").text();
                if ($(this).val() == "3") cd.Add = $("#CardNumberToUID").find("option[value='3']").text();
            });
            $("input[name='CardCheckdigit']").change(function () {
                checkDigit(cd);
            });
            $("input[name='UIDCheckDigit']").change(function () {
                checkDigitUID(cd);
            });
            checkDigit(cd);
            checkDigitUID(cd);

            $("#UIDToCardNumber").change(function () {
                checkBinding();
            });
            checkBinding();
        });

        //卡创建类型
        function checkCreateCardType() {

            var isImportCard = $("input[name='IsImportUIDNumber']:checked").val();
            if (isImportCard == '0') //手动创建
            {
                $("#CardCheckdigit,#CheckDigitModeID,#CardNumberToUID").addClass("required");
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCardNumber,#CardNumPatternImport,#CardNumMaskImport").removeClass("required");
                $("#CardNumPatternImport,#CardNumMaskImport").val("");

                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();

                if ($("input[name='CardCheckdigit']:checked").val() == "1") $("#CheckDigitModeTR").show();
                else $("#CheckDigitModeTR").hide();

            
            }
            else {//导入

                $("#CardCheckdigit,#CheckDigitModeID,#CardNumberToUID,#CardNumPattern,#CardNumMask").removeClass("required");
                $("#CardNumPattern,#CardNumMask").val("");
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCardNumber").addClass("required");

                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();

                checkBinding();
            }
        }
        //Manual CheckDigit
        function checkDigit(cd) {
            var method = $("input[name='CardCheckdigit']:checked").val();
            if (method == "1") {
                if ($("#CardNumberToUID option[value='3']").length > 0) {
                    $("#CardNumberToUID option[value='3']").remove();
                }
                $("#CheckDigitModeTR").show();
                if ($("#CardNumberToUID option[value='2']").length <= 0) $("#CardNumberToUID").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#CardNumberToUID option[value='2']").length > 0) {
                    $("#CardNumberToUID option[value='2']").remove();
                }
                $("#CheckDigitModeTR").hide();
                if ($("#CardNumberToUID option[value='3']").length <= 0) $("#CardNumberToUID").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        //Import CheckDigit
        function checkDigitUID(cd) {
            var method = $("input[name='UIDCheckDigit']:checked").val();
            if (method == "1") {
                if ($("#UIDToCardNumber option[value='3']")) {
                    $("#UIDToCardNumber option[value='3']").remove();
                }
                if ($("#UIDToCardNumber option[value='2']").length <= 0) $("#UIDToCardNumber").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#UIDToCardNumber option[value='2']")) {
                    $("#UIDToCardNumber option[value='2']").remove();
                }
                if ($("#UIDToCardNumber option[value='3']").length <= 0) $("#UIDToCardNumber").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        function checkBinding() {
            var isImport = $("input[name='IsImportUIDNumber']:checked").val();
            var selected = $("#UIDToCardNumber").val();
            if (isImport == "0") {
                $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
            }
            else if (selected == "0") {
                $("#CardNumMaskImportTR,#CardNumPatternImportTR").show();
            }
            else {
                $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
            }
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                基本资料
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                卡类型编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="CardTypeCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode"
                    hinttitle="卡类型编号" hintinfo="Translate__Special_121_StartKey identifier of Card Type. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span
                        class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                描述：
            </td>
            <td>
                <asp:TextBox ID="CardTypeName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required"
                    hinttitle="描述" hintinfo="请输入规范的卡類型名称。不能超過512個字符"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="CardTypeName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input "
                    hinttitle="其他描述1" hintinfo="對卡類型的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="CardTypeName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input "
                    hinttitle="其他描述2" hintinfo="對卡類型的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:DropDownList ID="BrandID" TabIndex="5" runat="server" Enabled="false" CssClass="dropdownlist required">
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                币种：
            </td>
            <td>
                <asp:DropDownList ID="CurrencyID" TabIndex="7" runat="server" CssClass="dropdownlist required">
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                卡号规则
            </th>
        </tr>
        <tr>
            <td align="right">
                卡号码是否导入：
            </td>
            <td>
                <asp:RadioButtonList ID="IsImportUIDNumber" runat="server" CssClass="required" TabIndex="7"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ManualRoles">
            <th colspan="2" align="left">
                手动创建编号规则
            </th>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号编码规则：
            </td>
            <td>
                <asp:TextBox ID="CardNumMask" TabIndex="8" runat="server" CssClass="input inputUpper NumMaskValidManual"
                    onpaste="return false" oncontextmenu="return false;" hinttitle="卡号编码规则" hintinfo="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号前缀号码：
            </td>
            <td>
                <asp:TextBox ID="CardNumPattern" TabIndex="9" runat="server" CssClass="input digits NumPatternValidManual"
                    hinttitle="卡号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                卡号是否添加校验位：
            </td>
            <td>
                <asp:RadioButtonList ID="CardCheckdigit" runat="server" CssClass="required" TabIndex="10"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="CheckDigitModeTR" class="ManualRoles">
            <td align="right">
                校验位计算方法：
            </td>
            <td>
                <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass">
                    <asp:ListItem Value="1" Text="EAN13"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="ManualRoles">
            <td align="right">
                Translate__Special_121_Start是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:DropDownList ID="CardNumberToUID" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass">
                    <asp:ListItem Value="1">全部复制</asp:ListItem>
                    <asp:ListItem Value="0">绑定</asp:ListItem>
                    <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                    <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <th colspan="2" align="left">
                导入物理编号规则
            </th>
        </tr>
        <tr class="ImportRoles" id="CardNumMaskImportTR">
            <td align="right">
                卡号编码规则：
            </td>
            <td>
                <asp:TextBox ID="CardNumMaskImport" TabIndex="8" runat="server" CssClass="input inputUpper NumMaskValidImport" MaxLength="20"
                    onpaste="return false" oncontextmenu="return false;" hinttitle="卡号编码规则" hintinfo="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr class="ImportRoles" id="CardNumPatternImportTR">
            <td align="right">
                卡号前缀号码：
            </td>
            <td>
                <asp:TextBox ID="CardNumPatternImport" TabIndex="9" runat="server" CssClass="input digits NumPatternValidImport"
                    hinttitle="卡号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="11"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="11"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="ImportRoles">
            <td align="right">
                Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End
            </td>
            <td>
                <asp:DropDownList ID="UIDToCardNumber" runat="server" TabIndex="11" CssClass="dropdownlist"
                    SkinID="NoClass">
                    <asp:ListItem Value="1">全部复制</asp:ListItem>
                    <asp:ListItem Value="0">绑定</asp:ListItem>
                    <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                    <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                使用规则
            </th>
        </tr>
        <tr>
            <td align="right">
                是否必须指定会员：
            </td>
            <td>
                <asp:RadioButtonList ID="CardMustHasOwner" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                现金帐套是否有多重有效期：
            </td>
            <td>
                <asp:RadioButtonList ID="CashExpiredate" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                积分帐套是否有多重有效期：
            </td>
            <td>
                <asp:RadioButtonList ID="PointExpiredate" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                    <asp:ListItem Value="0">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否需要写卡：
            </td>
            <td>
                <asp:RadioButtonList ID="IsPhysicalCard" runat="server" CssClass="required" TabIndex="13"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                有效性
            </th>
        </tr>
        <tr>
            <td align="right">
                卡类型启用日期：
            </td>
            <td>
                <asp:TextBox ID="CardTypeStartDate" runat="server" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'CardTypeEndDate\',{d:0});}'})"
                    TabIndex="14" CssClass="input required dateISO" hinttitle="卡类型启用日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox><span
                        class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡类型结束日期：
            </td>
            <td>
                <asp:TextBox ID="CardTypeEndDate" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'CardTypeStartDate\',{d:0});}'})"
                    TabIndex="15" CssClass="input required dateISO" hinttitle="卡类型结束日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox><span
                        class="star">*</span>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                卡级别列表
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Repeater ID="rptCardGradeList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="20%">
                                    卡级别编号
                                </th>
                                <th width="20%">
                                    卡级别等级
                                </th>
                                <th width="60%">
                                    卡级别描述
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblCardGraddCode" runat="server" Text='<%#Eval("CardGradeCode")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardgradeRank" runat="server" Text='<%#Eval("CardgradeRank")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCardGradeName1" runat="server" Text='<%#Eval("CardGradeName1")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div>
                    <div class="right">
                        <webdiyer:AspNetPager ID="rptCardGradeListPager" runat="server" CustomInfoTextAlign="Left"
                            FirstPageText="First" HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                            LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                            PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" TextAfterPageIndexBox=""
                            TextBeforePageIndexBox="" OnPageChanged="rptCardGradeListPager_PageChanged" CssClass="asppager"
                            CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                            CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                            LayoutType="Table">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdbg" align="center" valign="bottom">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提 交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2">
                *为必填项
            </td>
        </tr>
    </table>
    </form>
    <uc2:checkright ID="Checkright1" runat="server" />
</body>
</html>
