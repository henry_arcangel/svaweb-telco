﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class BrandLocationStore : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["type"] = Request.Params["type"];//CouponType =1,CardGrade=2;
                ViewState["storetype"] = Request.Params["storetype"];//1：发布店铺。2：使用店铺
                ViewState["id"] = Request.Params["id"];//CardGradeID or CouponTypeID

                InitCKBList();
                InitCheckedList();

                string view = Request.Params["view"] == null ? "" : "1";
                if (view == "1")
                {
                    DisabledCheckedList(this.ckbBrandList);
                    DisabledCheckedList(this.ckbStoreList);
                }

            } 
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ViewState["type"].ToString() == "1")
            {
                List<Edge.SVA.Model.CouponTypeStoreCondition> earnStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();
                List<Edge.SVA.Model.CouponTypeStoreCondition> consumeStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

                if (ViewState["storetype"].ToString() == "1")
                {
                    StringBuilder earnStoreNameSB = new StringBuilder();

                    earnStoreCheckedList.AddRange(GetCouponTypeCheckedList(this.ckbBrandList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 1, 1, earnStoreNameSB));
                    // checkedList.AddRange(GetCouponTypeCheckedList(this.ckbLocationList, (int)ViewState["id"], 2, 1,earnStoreNameSB));
                    earnStoreCheckedList.AddRange(GetCouponTypeCheckedList(this.ckbStoreList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 3, 1, earnStoreNameSB));

                    Session["CouponTypeEarnStoreCheckedList"] = earnStoreCheckedList;

                   // this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() {$('#EarnStoreCondition', parent.frames[0].document).val('" + earnStoreNameSB.ToString() + "'); window.parent.tb_remove(); });</script>");

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() { window.parent.tb_remove(); });</script>");

                }
                else if (ViewState["storetype"].ToString() == "2")
                {

                    StringBuilder consumeStoreNameSB = new StringBuilder();

                    consumeStoreCheckedList.AddRange(GetCouponTypeCheckedList(this.ckbBrandList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 1, 2, consumeStoreNameSB));
                    // checkedList.AddRange(GetCouponTypeCheckedList(this.ckbLocationList, (int)ViewState["id"], 2, 2,earnStoreNameSB));
                    consumeStoreCheckedList.AddRange(GetCouponTypeCheckedList(this.ckbStoreList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 3, 2, consumeStoreNameSB));

                    Session["CouponTypeConsumeStoreCheckedList"] = consumeStoreCheckedList;

                    //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() {$('#ConsumeStoreCondition', parent.frames[0].document).val('" + consumeStoreNameSB.ToString() + "'); window.parent.tb_remove(); });</script>");

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() { window.parent.tb_remove(); });</script>");

                }
               
            }
            else if (ViewState["type"].ToString() == "2")
            {
                List<Edge.SVA.Model.CardGradeStoreCondition> earnStoreCheckedList = new List<Edge.SVA.Model.CardGradeStoreCondition>();
                List<Edge.SVA.Model.CardGradeStoreCondition> consumeStoreCheckedList = new List<Edge.SVA.Model.CardGradeStoreCondition>();

                if (ViewState["storetype"].ToString() == "1")
                {
                    StringBuilder earnStoreNameSB = new StringBuilder();

                    earnStoreCheckedList.AddRange(GetCardGradeCheckedList(this.ckbBrandList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 1, 1, earnStoreNameSB));
                    // checkedList.AddRange(GetCouponTypeCheckedList(this.ckbLocationList, (int)ViewState["id"], 2, 1,earnStoreNameSB));
                    earnStoreCheckedList.AddRange(GetCardGradeCheckedList(this.ckbStoreList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 3, 1, earnStoreNameSB));

                    Session["CardGradeEarnStoreCheckedList"] = earnStoreCheckedList;

                    // this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() {$('#EarnStoreCondition', parent.frames[0].document).val('" + earnStoreNameSB.ToString() + "'); window.parent.tb_remove(); });</script>");

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() { window.parent.tb_remove(); });</script>");

                }
                else if (ViewState["storetype"].ToString() == "2")
                {

                    StringBuilder consumeStoreNameSB = new StringBuilder();

                    consumeStoreCheckedList.AddRange(GetCardGradeCheckedList(this.ckbBrandList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 1, 2, consumeStoreNameSB));
                    // checkedList.AddRange(GetCouponTypeCheckedList(this.ckbLocationList, (int)ViewState["id"], 2, 2,earnStoreNameSB));
                    consumeStoreCheckedList.AddRange(GetCardGradeCheckedList(this.ckbStoreList, Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString()), 3, 2, consumeStoreNameSB));

                    Session["CardGradeConsumeStoreCheckedList"] = consumeStoreCheckedList;

                    //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() {$('#ConsumeStoreCondition', parent.frames[0].document).val('" + consumeStoreNameSB.ToString() + "'); window.parent.tb_remove(); });</script>");

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script type='text/javascript'> $(function() { window.parent.tb_remove(); });</script>");

                }
            }


        }


        private void InitCKBList()
        {
            Edge.Web.Tools.ControlTool.BindBrand(this.ckbBrandList);
         //   Edge.Web.Tools.ControlTool.BindLocation(this.ckbLocationList);
            Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreList);

        }

        private void InitCheckedList()
        {
            if ((ViewState["type"] != null) && (ViewState["id"] != null) &&  (ViewState["storetype"]!=null))
            {
                int type =Edge.Web.Tools.ConvertTool.ToInt(ViewState["type"].ToString());
                int id = Edge.Web.Tools.ConvertTool.ToInt(ViewState["id"].ToString());
                int storetype=Edge.Web.Tools.ConvertTool.ToInt(ViewState["storetype"].ToString());

                if (id == 0)
                {
                    if (ViewState["type"].ToString() == "1")
                    {
                        if ((ViewState["storetype"].ToString() == "1") && (Session["CouponTypeEarnStoreCheckedList"]!=null))
                        {
                            List<Edge.SVA.Model.CouponTypeStoreCondition> checkedList = (List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeEarnStoreCheckedList"];

                            SetCouponTypeCheckedList(checkedList);
                        }
                        else if ((ViewState["storetype"].ToString() == "2") && (Session["CouponTypeConsumeStoreCheckedList"]!=null))
                        {
                            List<Edge.SVA.Model.CouponTypeStoreCondition> checkedList = (List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeConsumeStoreCheckedList"];

                            SetCouponTypeCheckedList(checkedList);
                        }    
                    }
                    else if (ViewState["type"].ToString() == "2")
                    {
                        if ((ViewState["storetype"].ToString() == "1") && (Session["CardGradeEarnStoreCheckedList"] != null))
                        {
                            List<Edge.SVA.Model.CardGradeStoreCondition> checkedList = (List<Edge.SVA.Model.CardGradeStoreCondition>)Session["CardGradeEarnStoreCheckedList"];

                            SetCardGradeCheckedList(checkedList);
                        }
                        else if ((ViewState["storetype"].ToString() == "2") && (Session["CardGradeConsumeStoreCheckedList"] != null))
                        {
                            List<Edge.SVA.Model.CardGradeStoreCondition> checkedList = (List<Edge.SVA.Model.CardGradeStoreCondition>)Session["CardGradeConsumeStoreCheckedList"];

                            SetCardGradeCheckedList(checkedList);
                        }    


                    }
                }
                else
                {
                    if (ViewState["type"].ToString() == "1")
                    {
                        List<Edge.SVA.Model.CouponTypeStoreCondition> checkedList = new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + id + " and StoreConditionType=" + storetype);

                        foreach (Edge.SVA.Model.CouponTypeStoreCondition item in checkedList)
                        {
                            if (item.ConditionType == 1)//1：brand。2：Location。3：store
                            {
                                SetCheckedItem(this.ckbBrandList, item.ConditionID.ToString().Trim());
                            }
                            else if (item.ConditionType == 2)
                            {
                                // SetCheckedItem(this.ckbLocationList, item.ConditionID.ToString().Trim());
                            }
                            else if (item.ConditionType == 3)
                            {
                                SetCheckedItem(this.ckbStoreList, item.ConditionID.ToString().Trim());
                            }
                        }
                    }
                    else if (ViewState["type"].ToString() == "2")
                    {
                        List<Edge.SVA.Model.CardGradeStoreCondition> checkedList = new Edge.SVA.BLL.CardGradeStoreCondition().GetModelList("CardGradeID=" + id + " and StoreConditionType=" + storetype);

                        foreach (Edge.SVA.Model.CardGradeStoreCondition item in checkedList)
                        {
                            if (item.ConditionType == 1)//1：brand。2：Location。3：store
                            {
                                SetCheckedItem(this.ckbBrandList, item.ConditionID.ToString().Trim());
                            }
                            else if (item.ConditionType == 2)
                            {
                                // SetCheckedItem(this.ckbLocationList, item.ConditionID.ToString().Trim());
                            }
                            else if (item.ConditionType == 3)
                            {
                                SetCheckedItem(this.ckbStoreList, item.ConditionID.ToString().Trim());
                            }
                        }


                    }
                }
            }

        }

        private void SetCouponTypeCheckedList(List<Edge.SVA.Model.CouponTypeStoreCondition> checkedList)
        {
            foreach (Edge.SVA.Model.CouponTypeStoreCondition item in checkedList)
            {
                if (item.ConditionType == 1)//1：brand。2：Location。3：store
                {
                    SetCheckedItem(this.ckbBrandList, item.ConditionID.ToString().Trim());
                }
                else if (item.ConditionType == 2)
                {
                    // SetCheckedItem(this.ckbLocationList, item.ConditionID.ToString().Trim());
                }
                else if (item.ConditionType == 3)
                {
                    SetCheckedItem(this.ckbStoreList, item.ConditionID.ToString().Trim());
                }
            }
        }


        private void SetCardGradeCheckedList(List<Edge.SVA.Model.CardGradeStoreCondition> checkedList)
        {
            foreach (Edge.SVA.Model.CardGradeStoreCondition item in checkedList)
            {
                if (item.ConditionType == 1)//1：brand。2：Location。3：store
                {
                    SetCheckedItem(this.ckbBrandList, item.ConditionID.ToString().Trim());
                }
                else if (item.ConditionType == 2)
                {
                    // SetCheckedItem(this.ckbLocationList, item.ConditionID.ToString().Trim());
                }
                else if (item.ConditionType == 3)
                {
                    SetCheckedItem(this.ckbStoreList, item.ConditionID.ToString().Trim());
                }
            }
        }

        private void SetCheckedItem(CheckBoxList ckbList, string conditionID)
        {
            for (int i = 0; i < ckbList.Items.Count; i++)
            {
                if (ckbList.Items[i].Value.Trim() == conditionID)
                {
                    ckbList.Items[i].Selected = true;
                }
            }
        }

        private List<Edge.SVA.Model.CouponTypeStoreCondition> GetCouponTypeCheckedList(CheckBoxList ckbList,int couponTypeID, int conditionType, int storetype,StringBuilder storeNameList)
        {
            List<Edge.SVA.Model.CouponTypeStoreCondition> checkedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();
            for (int i = 0; i < ckbList.Items.Count; i++)
            {
                if (ckbList.Items[i].Selected)
                {
                    Edge.SVA.Model.CouponTypeStoreCondition item = new Edge.SVA.Model.CouponTypeStoreCondition();
                    item.ConditionID = int.Parse(ckbList.Items[i].Value);
                    item.ConditionType = conditionType;
                    item.CouponTypeID = couponTypeID;
                    item.StoreConditionType = storetype;

                    checkedList.Add(item);
                    if (storeNameList.Length == 0)
                    {
                        storeNameList.Append(ckbList.Items[i].Text);
                    }
                    else
                    {
                        storeNameList.Append(","+ckbList.Items[i].Text);
                    }
                }
            }

            return checkedList;
        }

        private List<Edge.SVA.Model.CardGradeStoreCondition> GetCardGradeCheckedList(CheckBoxList ckbList, int couponTypeID, int conditionType, int storetype, StringBuilder storeNameList)
        {
            List<Edge.SVA.Model.CardGradeStoreCondition> checkedList = new List<Edge.SVA.Model.CardGradeStoreCondition>();
            for (int i = 0; i < ckbList.Items.Count; i++)
            {
                if (ckbList.Items[i].Selected)
                {
                    Edge.SVA.Model.CardGradeStoreCondition item = new Edge.SVA.Model.CardGradeStoreCondition();
                    item.ConditionID = int.Parse(ckbList.Items[i].Value);
                    item.ConditionType = conditionType;
                    item.CardGradeID = couponTypeID;
                    item.StoreConditionType = storetype;

                    checkedList.Add(item);
                    if (storeNameList.Length == 0)
                    {
                        storeNameList.Append(ckbList.Items[i].Text);
                    }
                    else
                    {
                        storeNameList.Append("," + ckbList.Items[i].Text);
                    }
                }
            }

            return checkedList;
        }


        private void DisabledCheckedList(CheckBoxList ckbList)
        {

            for (int i = 0; i < ckbList.Items.Count; i++)
            {

                ckbList.Items[i].Enabled = false;

            }
        }

    }
}
