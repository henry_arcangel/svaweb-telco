﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            //是否可以设置界限值
            $("#CardGradeUpdMethod").click(function() {
                var method = $("input[name='CardGradeUpdMethod']:checked").val();
                if ((method == '1') || (method == '2')) {
                    $("#CardGradeUpdThreshold").addClass("input required digit");
                }
                else {
                    $("#CardGradeUpdThreshold").removeClass("input required digit");
                }
            });
        });
        
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right">
                品牌编号：
            </td>
            <td>
                <asp:TextBox ID="BrandCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode" hinttitle="品牌编号" hintinfo="Translate__Special_121_StartKey identifier of brand.1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                描述：
            </td>
            <td width="75%">
                <asp:TextBox ID="BrandName1" TabIndex="2" runat="server" CssClass="input required" hinttitle="描述" hintinfo="请输入规范的品牌名称。不能超過512個字符"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="BrandName2" TabIndex="3" runat="server" CssClass="input " hinttitle="其他描述1" hintinfo="對品牌的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="BrandName3" TabIndex="4" runat="server" CssClass="input " hinttitle="其他描述2" hintinfo="對品牌的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                发行商：
            </td>
            <td>
                <asp:DropDownList ID="CardIssuerID" runat="server"  Enabled="false" SkinID="NoClass"
                    CssClass="dropdownlist required">
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                行业：
            </td>
            <td>
                <asp:DropDownList ID="IndustryID" runat="server" TabIndex="5" SkinID="NoClass" CssClass="dropdownlist required">
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                小图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="BrandPicSFile" runat="server" SubSaveFilePath="Images/Brand"  hinttitle="小图片" hintinfo="Translate__Special_121_Start品牌標誌的圖標(小圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End" />
            </td>
        </tr>
        <tr>
            <td align="right">
                中图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="BrandPicMFile" runat="server" SubSaveFilePath="Images/Brand" hinttitle="中图片" hintinfo="Translate__Special_121_Start品牌標誌的圖標(中圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End"  />
            </td>
        </tr>
        <tr>
            <td align="right">
                大图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="BrandPicGFile" runat="server" SubSaveFilePath="Images/Brand" hinttitle="大图片" hintinfo="Translate__Special_121_Start品牌標誌的圖標(大圖)。點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <span class="uploading">正在上传，请稍候...</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提 交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2" >
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
