﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {

            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
            $(".NumMaskValidImport").keypress(function (event) {
                var keyCode = event.which;

                if (keyCode == 65 || (keyCode == 57) || (keyCode == 97))
                    return true;
                else
                    return false;
            }).focus(function () {
                this.style.imeMode = 'disabled';
            });
            jQuery.validator.addMethod("NumMaskValidImport", function (value, element) {
                if (value == null || value.length <= 0) return true;
                var reg = /^A*9+$/gi;
                var isNumber = /^9+/gi;

                if (/9+/gi.exec(value) && /9+/gi.exec(value).toString().length > 18) {
                    return false;
                }
                if (isNumber.exec(value)) {
                    $(".NumPatternValidImport").val("");
                    $(".NumPatternValidImport").attr('disabled', true);
                    $(".NumPatternValidImport").removeClass("required");

                }
                else {
                    $(".NumPatternValidImport").removeAttr('disabled');
                    $(".NumPatternValidImport").addClass("required");
                }
                return reg.exec(value);
            }, jQuery.validator.messages.NumMaskValid);

            jQuery.validator.addMethod("NumPatternValidImport", function (value, element) {
                var reg = /^A+/gi;
                var mask = $(".NumMaskValidImport").val();
                if (mask.length <= 0) return true;

                var result = reg.exec(mask);
                if (result == null || result.length <= 0) return true;

                return result.toString().length == value.length;

            }, jQuery.validator.messages.NumPatternValid);

            $("#IsImportCouponNumber").click(function () {
                checkIsImportCouponNumber();
            });

            $("#CoupontypeFixedAmount").click(function () {
                checkIsFixedAmount();
            });

            checkIsImportCouponNumber();
            checkIsFixedAmount();

            $("#CouponCheckdigit").click(function () {
                checkCheckDigitMode();
            });

            $("#CouponValidityUnit").click(function () {
                checkCouponSpecifyExpiryDate();
            });
            checkCouponSpecifyExpiryDate();

            var cd = new Object();
            $("#CouponNumberToUID option").each(function () {
                if ($(this).val() == "2") cd.Delete = $("#CouponNumberToUID").find("option[value='2']").text();
                if ($(this).val() == "3") cd.Add = $("#CouponNumberToUID").find("option[value='3']").text();
            });
            $("input[name='CouponCheckdigit']").change(function () {
                checkDigit(cd);
            });
            $("input[name='UIDCheckDigit']").change(function () {
                checkDigitUID(cd);
            });
            checkDigit(cd);
            checkDigitUID(cd);

            $("#UIDToCouponNumber").change(function () {
                checkBinding();
            });
            checkBinding();
        });
        //是否会员优惠券
        function checkIsMemberBind() {
            var method = $("input[name='IsMemberBind']:checked").val();
            if (method == '1') {
                $("#issueBrand,#issueLocation,#issueStore,#useBrand,#useLocation,#useStore").hide();

            }
            else {
                $("#issueBrand,#issueLocation,#issueStore,#useBrand,#useLocation,#useStore").show();

            }
        }

        //是否导入优惠券
        function checkIsImportCouponNumber() {
            var method = $("input[name='IsImportCouponNumber']:checked").val();
            if (method == '1') { //导入

                $("#CouponNumMask,#CouponNumPattern,#CouponCheckdigit,#CheckDigitModeID,#CouponNumberToUID").removeClass("required");
                $("#CouponNumMask,#CouponNumPattern,#CouponCheckdigit,#CheckDigitModeID,#CouponNumberToUID").next(".star").hide();
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCouponNumber").addClass("required");
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCouponNumber").next(".star").show();
                $("#CouponNumMask,#CouponNumPattern").val("");
                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();
                checkBinding();
            }
            else {//手动
                $("#CouponNumMask,#CouponNumPattern,#CouponCheckdigit,#CheckDigitModeID,#CouponNumberToUID").addClass("required");
                $("#CouponNumMask,#CouponNumPattern,#CouponCheckdigit,#CheckDigitModeID,#CouponNumberToUID").next(".star").show();
                $("#CouponNumMaskImport,#CouponNumPatternImport,#IsConsecutiveUID,#UIDCheckDigit,#UIDToCouponNumber").removeClass("required");
                $("#IsConsecutiveUID,#UIDCheckDigit,#UIDToCouponNumber").next(".star").hide();
                $("#CouponNumMaskImport,#CouponNumPatternImport").val("");
                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();
                checkBinding();


                if ($("input[name='CouponCheckdigit']:checked").val() == 'False') //没有Check
                {
                    $("#CheckDigitModeTR").hide();
                }
                else {
                    $("#CheckDigitModeTR").show();
                }
            }
        }


        //是否固定面额
        function checkIsFixedAmount() {
            var method = $("input[name='CoupontypeFixedAmount']:checked").val();
            if (method == '0') {
                $("#CouponTypeAmount").val("");
                $("#CouponTypeAmount").attr('disabled', ' true');
                $("#CouponTypeAmount").removeClass("required number");
                $("#star").hide();
            }
            else {
                $("#CouponTypeAmount").removeAttr('disabled');
                $("#CouponTypeAmount").addClass("required number");
                $("#star").show();
            }
        }


        // CheckDigitMode
        function checkCheckDigitMode() {
            var isCouponCheckdigit = $("input[name='CouponCheckdigit']:checked").val();

            if (isCouponCheckdigit == 'False') //没有Check
            {
                $("#CheckDigitModeTR").hide();
            }
            else {
                $("#CheckDigitModeTR").show();
            }
        }

        //是否固定有效期
        function checkCouponSpecifyExpiryDate() {
            var isCouponSpecifyExpiryDate = $("#CouponValidityUnit").val();

            if (isCouponSpecifyExpiryDate == '6') //是固定有效期
            {
                $("#CouponValidityDuration").removeClass("required");
                $("#CouponValidityDuration").attr("disabled", "disabled");
                $("#CouponValidityDuration").next(".star").hide();
                $("#CouponValidityDuration").val("");

                $("#CouponSpecifyExpiryDate").addClass("required");
                $("#CouponSpecifyExpiryDate").removeAttr("disabled");
                $("#CouponSpecifyExpiryDate").next(".star").show()
            }
            else {
                $("#CouponValidityDuration").addClass("required");
                $("#CouponValidityDuration").removeAttr("disabled");
                $("#CouponValidityDuration").next(".star").show()

                $("#CouponSpecifyExpiryDate").removeClass("required");
                $("#CouponSpecifyExpiryDate").attr("disabled", "disabled");
                $("#CouponSpecifyExpiryDate").next(".star").hide();
                $("#CouponSpecifyExpiryDate").val("");
            }
        }
        //Manual CheckDigit
        function checkDigit(cd) {
            var method = $("input[name='CouponCheckdigit']:checked").val();
            if (method == "True") {
                if ($("#CouponNumberToUID option[value='3']").length > 0) {
                    $("#CouponNumberToUID option[value='3']").remove();
                }
                $("#CheckDigitModeTR").show();
                if ($("#CouponNumberToUID option[value='2']").length <= 0) $("#CouponNumberToUID").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#CouponNumberToUID option[value='2']").length > 0) {
                    $("#CouponNumberToUID option[value='2']").remove();
                }
                $("#CheckDigitModeTR").hide();
                if ($("#CouponNumberToUID option[value='3']").length <= 0) $("#CouponNumberToUID").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        //Import CheckDigit
        function checkDigitUID(cd) {
            var method = $("input[name='UIDCheckDigit']:checked").val();
            if (method == "1") {
                if ($("#UIDToCouponNumber option[value='3']")) {
                    $("#UIDToCouponNumber option[value='3']").remove();
                }
                if ($("#UIDToCouponNumber option[value='2']").length <= 0) $("#UIDToCouponNumber").append("<option value='2'>" + cd.Delete + "</option>");
            }
            else {
                if ($("#UIDToCouponNumber option[value='2']")) {
                    $("#UIDToCouponNumber option[value='2']").remove();
                }
                if ($("#UIDToCouponNumber option[value='3']").length <= 0) $("#UIDToCouponNumber").append("<option value='3'>" + cd.Add + "</option>");
            }
        }
        //Import Binding
        function checkBinding() {
            var isImport = $("input[name='IsImportCouponNumber']:checked").val();
            var selected = $("#UIDToCouponNumber").val();
            if (isImport == "0") {
                $("#CouponNumMaskImportTR,#CouponNumPatternImportTR").hide();
            }
            else if (selected == "0") {
                $("#CouponNumMaskImport,#CouponNumPatternImport").addClass("required");
                $("#CouponNumMaskImportTR,#CouponNumPatternImportTR").show();
                $(".ImportRolesNumber").show();

            }
            else {
                $("#CouponNumMaskImport,#CouponNumPatternImport").removeClass("required");
                $("#CouponNumMaskImportTR,#CouponNumPatternImportTR").hide();
                $(".ImportRolesNumber").hide();
            }
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <script type="text/javascript">
         $(function() {
              $(".liTabs").click(function() {parent.jAlert("<%=Edge.Messages.Manager.MessagesTool.instance.GetMessage("10028")%>", "<%=Resources.MessageTips.WARNING_TITLE%>");});
             });
    </script>
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="tabs">
        <ul>
            <li onclick="tabs(1);"><a><span>基本信息</span></a></li>
            <li class="liTabs"><a><span>消费金额转换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>积分兑换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>现金兑换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>优惠券兑换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>组合兑换规则</span></a></li>
        </ul>
    </div>
    <div class="fragment">
        <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    基本信息
                </th>
            </tr>
            <tr>
                <td width="25%" align="right">
                    优惠劵类型编号：
                </td>
                <td width="75%">
                    <asp:TextBox ID="CouponTypeCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode"
                        hinttitle="优惠劵类型编号" hintinfo="Translate__Special_121_StartKey identifier of Coupon Type. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span
                            class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    描述：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required"
                        hinttitle="描述" hintinfo="请输入规范的優惠券類型的名称。不能超過512個字符"></asp:TextBox><span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述1：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input "
                        hinttitle="其他描述1" hintinfo="對優惠券類型的描述,不能超過512個字符"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述2：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input "
                        hinttitle="其他描述2" hintinfo="對優惠券類型的另一個描述,不能超過512個字符"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    品牌：
                </td>
                <td>
                    <asp:DropDownList ID="BrandID" TabIndex="7" runat="server" CssClass="dropdownlist required"
                        Enabled="false" AutoPostBack="true" SkinID="NoClass">
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    活动：
                </td>
                <td>
                    <asp:DropDownList ID="CampaignID" TabIndex="8" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否固定面额：
                </td>
                <td>
                    <asp:RadioButtonList ID="CoupontypeFixedAmount" runat="server" TabIndex="9" RepeatDirection="Horizontal"
                        hinttitle="是否固定面额" hintinfo="Translate__Special_121_Start選擇“Yes”,请輸入固定的面额；Translate__Special_121_End">
                        <asp:ListItem Text="是的" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="不是" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    面额：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeAmount" TabIndex="10" runat="server" CssClass="input svaAmount"
                        hinttitle="面额" hintinfo="请输入正數，最大兩位小數"></asp:TextBox><span id="star" class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    币种：
                </td>
                <td>
                    <asp:DropDownList ID="CurrencyID" TabIndex="11" runat="server" CssClass="dropdownlist required"
                        SkinID="NoClass">
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
              <tr>
                <td align="right">
                    销售货号匹配清单：
                </td>
                <td>
                    <a id="A3" class="thickbox btn_bg" href="SaleProduct/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        编辑销售货号</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    指定限购商品：
                </td>
                <td>
                    <a id="A1" class="thickbox btn_bg" href="Product/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        编辑限购商品</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    指定限购部门：
                </td>
                <td>
                    <a id="A2" class="thickbox btn_bg" href="Department/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        编辑限购部门</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券布局：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CouponTypeLayoutFile" runat="server" TabIndex="12" SubSaveFilePath="Images/CouponType"
                        FileType="files" HintTitle="优惠券布局" HintInfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券图片：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CouponTypePicFile" runat="server" TabIndex="13" SubSaveFilePath="Images/CouponType"
                        HintTitle="优惠券图片" HintInfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否会员优惠券：
                </td>
                <td>
                    <asp:RadioButtonList ID="IsMemberBind" runat="server" TabIndex="14" RepeatDirection="Horizontal">
                        <asp:ListItem Text="是的" Value="1"></asp:ListItem>
                        <asp:ListItem Text="不是" Value="0" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    发行品牌/店铺：
                </td>
                <td>
                    <a id="issueBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a> <a id="issueStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                            店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    使用品牌/店铺：
                </td>
                <td>
                    <a id="useBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a> <a id="useStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                            店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    密码规则：
                </td>
                <td>
                    <asp:DropDownList ID="PasswordRuleID" runat="server" TabIndex="15" CssClass="dropdownlist required">
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券面额清空控制：
                </td>
                <td>
                    <asp:DropDownList ID="CouponforfeitControl" runat="server" TabIndex="15" CssClass="dropdownlist required">
                        <asp:ListItem Value="0">直接清空</asp:ListItem>
                        <asp:ListItem Value="1">当日清空</asp:ListItem>
                        <asp:ListItem Value="2">不清空</asp:ListItem>
                    </asp:DropDownList>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    优惠券号码规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠券号码是否导入：
                </td>
                <td>
                    <asp:RadioButtonList ID="IsImportCouponNumber" runat="server" TabIndex="15" RepeatDirection="Horizontal">
                        <asp:ListItem Text="是" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="否" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <th colspan="2" align="left">
                    手动创建编号规则
                </th>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    优惠券号码编号规则：
                </td>
                <td>
                    <asp:TextBox ID="CouponNumMask" TabIndex="16" runat="server" MaxLength="20" CssClass="input inputUpper NumMaskValid"
                        onpaste="return false" oncontextmenu="return false;" hinttitle="优惠券号码编号规则" hintinfo="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    优惠券号码编号前缀号码：
                </td>
                <td>
                    <asp:TextBox ID="CouponNumPattern" TabIndex="17" runat="server" CssClass="input digits NumPatternValid"
                        hinttitle="优惠券号码编号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    是否包含校验位：
                </td>
                <td>
                    <asp:RadioButtonList ID="CouponCheckdigit" runat="server" TabIndex="18" RepeatDirection="Horizontal">
                        <asp:ListItem Text="是" Value="True"></asp:ListItem>
                        <asp:ListItem Text="否" Value="False" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="CheckDigitModeTR" class="ManualRoles">
                <td align="right">
                    校验位计算方法：
                </td>
                <td>
                    <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="18" CssClass="dropdownlist">
                        <asp:ListItem Value="1">EAN13</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    Translate__Special_121_Start是否优惠券号码复制到优惠券物理编号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:DropDownList ID="CouponNumberToUID" runat="server" TabIndex="18" CssClass="dropdownlist"
                        SkinID="NoClass">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <th colspan="2" align="left">
                    导入物理编号规则
                </th>
            </tr>
            <tr class="ImportRoles" id="CouponNumMaskImportTR">
                <td align="right">
                    优惠券号码编号规则：
                </td>
                <td>
                    <asp:TextBox ID="CouponNumMaskImport" TabIndex="16" runat="server" MaxLength="20"
                        CssClass="input inputUpper NumMaskValidImport" onpaste="return false" oncontextmenu="return false;"
                        hinttitle="优惠券号码编号规则" hintinfo="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ImportRoles" id="CouponNumPatternImportTR">
                <td align="right">
                    优惠券号码编号前缀号码：
                </td>
                <td>
                    <asp:TextBox ID="CouponNumPatternImport" TabIndex="17" runat="server" CssClass="input digits NumPatternValidImport"
                        hinttitle="优惠券号码编号前缀号码" hintinfo="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="18"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="18"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start是否优惠券物理编号复制到优惠券号码：（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:DropDownList ID="UIDToCouponNumber" runat="server" TabIndex="18" CssClass="dropdownlist"
                        SkinID="NoClass">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效期规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    延长有效期值：
                </td>
                <td>
                    <asp:TextBox ID="CouponValidityDuration" TabIndex="18" MaxLength="3" runat="server"
                        CssClass="input digits" hinttitle="延长有效期值" hintinfo="必須输入數字，並且是整數"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    延长有效期的单位：
                </td>
                <td>
                    <asp:DropDownList ID="CouponValidityUnit" runat="server" TabIndex="21" CssClass="dropdownlist">
                        <asp:ListItem Value="1" Selected="True">年</asp:ListItem>
                        <asp:ListItem Value="2">月</asp:ListItem>
                        <asp:ListItem Value="3">星期</asp:ListItem>
                        <asp:ListItem Value="4">天</asp:ListItem>
                        <asp:ListItem Value="6">指定失效日期</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券固定过期日期：
                </td>
                <td>
                    <asp:TextBox ID="CouponSpecifyExpiryDate" TabIndex="19" runat="server" onfocus="WdatePicker()"
                        CssClass="input dateISO" hinttitle="优惠券固定过期日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    激活是否重置有效期：
                </td>
                <td>
                    <asp:RadioButtonList ID="ActiveResetExpiryDate" runat="server" TabIndex="21" CssClass="required"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    优惠券转增规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠劵是否可以转赠：
                </td>
                <td>
                    <asp:DropDownList ID="CouponTypeTransfer" runat="server" TabIndex="22" CssClass="dropdownlist">
                        <asp:ListItem Value="0">不允许</asp:ListItem>
                        <asp:ListItem Value="1">同品牌转赠</asp:ListItem>
                        <asp:ListItem Value="2">同卡级别转赠</asp:ListItem>
                        <asp:ListItem Value="3">同卡类型转赠</asp:ListItem>
                        <asp:ListItem Value="4">任意转赠</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效性资料
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠劵类型启用日期：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeStartDate" TabIndex="21" runat="server" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'CouponTypeEndDate\',{d:0});}'})"
                        CssClass="input dateISO" hinttitle="优惠劵类型启用日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠劵类型结束日期：
                </td>
                <td>
                    <asp:TextBox ID="CouponTypeEndDate" TabIndex="22" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'CouponTypeStartDate\',{d:0});}'})"
                        CssClass="input dateISO" hinttitle="优惠劵类型结束日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <asp:Button ID="btnUpdate" runat="server" Text="提 交" OnClick="btnUpdate_Click" CssClass="submit">
                        </asp:Button>
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" /></div>
                </td>
            </tr>
            <tr>
                <td class="showMessage" colspan="2">
                    *为必填项
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    消费金额转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnConsumeAmountList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                    <asp:HiddenField ID="lb_id" runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangeConsumeAmount" runat="server" Text='<%#Eval("ExchangeConsumeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnConsumeAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                        查看</a></span> <span class="btn_bg"><a href="EarnCouponRule/EarnConsumeAmount/Modify.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                            修改</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <div style="line-height: 30px; height: 30px;">
                        <div id="Div1" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnConsumeAmountDel" runat="server" OnClick="lbtnEarnConsumeAmountDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnConsumeAmountAdd" runat="server" OnClick="lbtnEarnConsumeAmountAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    积分转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnPointList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        积分
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                    <asp:HiddenField ID="lb_id" runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangePoint" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnPoint/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                        查看</a></span> <span class="btn_bg"><a href="EarnCouponRule/EarnPoint/Modify.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                            修改</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <div style="line-height: 30px; height: 30px;">
                        <div id="Div2" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnPointDel" runat="server" OnClick="lbtnEarnPointDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnPointAdd" runat="server" OnClick="lbtnEarnPointAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    现金转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnAmountList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        兑换金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                    <asp:HiddenField ID="lb_id" runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                                </td>
                                <%--                                <td align="center">
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("KeyID")%>' Visible="false"></asp:Label><a
                                        href="EarnCouponRule/EarnAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>"><asp:Label ID="lblPointRuleID"
                                            runat="server" Text='<%#Eval("KeyID")%>'></asp:Label></a>
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangeAmount" runat="server" Text='<%#Eval("ExchangeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                        查看</a></span> <span class="btn_bg"><a href="EarnCouponRule/EarnAmount/Modify.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=modify">
                                            修改</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <div style="line-height: 30px; height: 30px;">
                        <div id="Div3" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnAmountDel" runat="server" OnClick="lbtnEarnAmountDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnAmountAdd" runat="server" OnClick="lbtnEarnAmountAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    优惠券兑换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnCouponList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        数量
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                    <asp:HiddenField ID="lb_id" runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblPointRuleID" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeCouponCount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnCoupon/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=modify">
                                        查看</a></span> <span class="btn_bg"><a href="EarnCouponRule/EarnCoupon/Modify.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=modify">
                                            修改</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <div style="line-height: 30px; height: 30px;">
                        <div id="Div4" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnCouponDel" runat="server" OnClick="lbtnEarnCouponDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnCouponAdd" runat="server" OnClick="lbtnEarnCouponAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    组合转换规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnAmountPointList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        积分
                                    </th>
                                    <th width="10%">
                                        金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                    <asp:HiddenField ID="lb_id" runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblPointRuleID" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangeAmount" runat="server" Text='<%#Eval("ExchangeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatusName" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnAmountPoint/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=modify">
                                        查看</a></span> <span class="btn_bg"><a href="EarnCouponRule/EarnAmountPoint/Modify.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=modify">
                                            修改</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <div style="line-height: 30px; height: 30px;">
                        <div id="Div5" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnAmountPointDel" runat="server" OnClick="lbtnEarnAmountPointDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnAmountPointAdd" runat="server" OnClick="lbtnEarnAmountPointAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
