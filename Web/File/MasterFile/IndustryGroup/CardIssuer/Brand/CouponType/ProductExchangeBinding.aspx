﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductExchangeBinding.aspx.cs"
    Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.ProductExchangeBinding" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
         
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：使用产品</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                添加使用产品：
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                产品：
            </td>
            <td width="75%">
                <asp:TextBox ID="ProdCode" runat="server" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:DropDownList ID="BrandID" runat="server" CssClass="dropdownlist required">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="确定" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    &nbsp;</div>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                已添加使用产品：
            </th>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div style="padding-bottom: 10px;">
                </div>
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    <input type="checkbox" onclick="checkAll(this);" />选择
                                </th>
                                <%--                 <th width="8%">
                                    优惠券类型
                                </th>--%>
                                <th width="10%">
                                    品牌
                                </th>
                                <th width="10%">
                                    产品
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" /><asp:HiddenField ID="lb_id"
                                    runat="server" Value='<%#Eval("KeyID")%>'></asp:HiddenField>
                            </td>
                            <%--                    <td align="center">
                                <asp:Label ID="lblCouponTypeID" runat="server" Text='<%#Eval("CouponTypeID")%>'></asp:Label>
                            </td>--%>
                            <td align="center">
                                <asp:Label ID="lblBrandName" runat="server" Text='<%#Eval("BrandName")%>'></asp:Label>
                                <asp:HiddenField ID="lblBrandID" runat="server" Value='<%#Eval("BrandID")%>'></asp:HiddenField>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblPordCode" runat="server" Text='<%#Eval("ProdCode")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
                <div style="line-height: 30px; height: 30px;">
                    <div id="Pagination" class="right flickr">
                    </div>
                    <div class="left">
                        <span class="btn_bg">
                            <asp:LinkButton ID="lbtnDel" runat="server" OnClick="lbtnDel_Click" CssClass="cancel">删除</asp:LinkButton>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
