﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType,Edge.SVA.Model.CouponType>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            int tabNum = string.IsNullOrEmpty(Request.Params["tabs"]) ? 1 : int.Parse(Request.Params["tabs"]);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "tabs", "<script type='text/javascript'> $(function() {tabs(" + tabNum + "); });</script>");

            if (!IsPostBack)
            {
                this.lbtnEarnAmountDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                this.lbtnEarnAmountPointDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                this.lbtnEarnConsumeAmountDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                this.lbtnEarnCouponDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                this.lbtnEarnPointDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                
               
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);

                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                EarnCouponRuleRptBind("CouponTypeID=" + Request.Params["id"]);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                string brandID = brand.BrandID.ToString();
                this.BrandID.Items.Add(new ListItem() { Text = brandText, Value = brandID });

                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID, Model.BrandID);

                this.CampaignID.SelectedValue = Model.CampaignID.GetValueOrDefault().ToString();

                string msg = "";
                if (Model.IsImportCouponNumber.GetValueOrDefault() == 1 && Model.UIDToCouponNumber.GetValueOrDefault() == 0)
                {
                    this.CouponNumMaskImport.Text = Model.CouponNumMask;
                    this.CouponNumPatternImport.Text = Model.CouponNumPattern;
                }
                

                if ((DALTool.isCouponTypeCreateCoupon(Model.CouponTypeID, ref msg)))//已创建优惠券，不能修改
                {
                    if (Model.IsImportCouponNumber.GetValueOrDefault() == 0)            //手动      
                    {
                        if (!string.IsNullOrEmpty(CouponNumMask.Text.Trim()))//号码规则不为空
                        {
                            this.CouponNumMask.Enabled = false;
                            this.CouponNumPattern.Enabled = false;
                            this.CouponCheckdigit.Enabled = false;
                            this.CheckDigitModeID.Enabled = false;
                            this.CouponNumberToUID.Enabled = false;
                            this.IsImportCouponNumber.Enabled = false;
                        }
                    }
                    else                                                                //导入
                    {
                        this.UIDCheckDigit.Enabled = false;
                        this.IsConsecutiveUID.Enabled = false;
                        this.UIDToCouponNumber.Enabled = false;
                        this.IsImportCouponNumber.Enabled = false;
                    }

                    //除了号码规则外，其他规则都不能修改
                    CouponTypeCode.Enabled = false;
                    CoupontypeFixedAmount.Enabled = false;
                    CouponTypeAmount.Enabled = false;
                    IsMemberBind.Enabled = false;
                    CouponValidityDuration.Enabled = false;
                    CouponValidityUnit.Enabled = false;
                    ActiveResetExpiryDate.Enabled = false;
                    CouponTypeTransfer.Enabled = false;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.CouponType item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.CouponTypeCode = item.CouponTypeCode.ToUpper();
                item.CouponNumMask = item.CouponNumMask.ToUpper();

                if (item.IsImportCouponNumber != null)
                {
                    if (item.IsImportCouponNumber.GetValueOrDefault() == 1)
                    {
                        item.CouponNumMask = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumMaskImport.Text.ToUpper().Trim() : "";
                        item.CouponNumPattern = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumPatternImport.Text.ToUpper().Trim() : "";
                        item.CouponCheckdigit = null;
                        item.CheckDigitModeID = null;
                        item.CouponNumberToUID = null;
                    }
                    else
                    {
                        item.UIDCheckDigit = null;
                        item.IsConsecutiveUID = null;
                        item.UIDToCouponNumber = null;
                    }
                }

                if (new Edge.SVA.BLL.CouponType().isHasCouponTypeCode(item.CouponTypeCode, item.CouponTypeID))
                {
                    JscriptPrintAndFocus(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE, this.CouponTypeCode.ClientID);
                    return;
                }
                if (this.IsImportCouponNumber.SelectedValue.Trim() == "0")
                {
                    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), item.CouponTypeID))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMask.ClientID);
                        return;
                    }
                }
                if (item.IsImportCouponNumber.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 0)
                {
                    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), 0))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMaskImport.ClientID);
                        return;
                    }
                }

                if (!Tools.DALTool.CheckNumberMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 2, item.CouponTypeID))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMask.ClientID);
                    return;
                }
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CouponType>(item))
            {
                #region 店铺/区域/品牌
                //List<Edge.SVA.Model.CouponTypeStoreCondition> allStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

                //if (Session["CouponTypeEarnStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeEarnStoreCheckedList"]);
                //}
                //else
                //{
                //    allStoreCheckedList.AddRange(new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + item.CouponTypeID + " and StoreConditionType=" + 1));
                //}

                //if (Session["CouponTypeConsumeStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeConsumeStoreCheckedList"]);
                //}
                //else
                //{
                //    allStoreCheckedList.AddRange(new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + item.CouponTypeID + " and StoreConditionType=" + 2));
                //}

                ////为每个新的条件赋值CouponTypeID
                //foreach (Edge.SVA.Model.CouponTypeStoreCondition newItem in allStoreCheckedList)
                //{
                //    newItem.CouponTypeID = item.CouponTypeID;
                //}

                //int add = new Edge.SVA.BLL.CouponTypeStoreCondition().UpdateList(allStoreCheckedList, item.CouponTypeID,Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
                //if (add > 0)
                //{
                //    JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //}
                //else
                //{
                //    JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);

                //}
                #endregion

                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }

        private bool IsRightCouponNumPattern()
        {
            if (string.IsNullOrEmpty(CouponNumPattern.Text.Trim())) return true;

            string strCouponNumMask = CouponNumMask.Text.Trim().ToUpper();
            string strNewCouponNumMask = "";
            foreach (char c in strCouponNumMask)
            {
                if (c == 'A')
                {
                    strNewCouponNumMask = strNewCouponNumMask + c.ToString();
                }
                else
                {
                    break;
                }
            }

            int intNewCouponNumMask = strNewCouponNumMask.Length;
            int intCouponNumPattern = CouponNumPattern.Text.Trim().Length;

            return intNewCouponNumMask == intCouponNumPattern;
        }

        private void EarnCouponRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new Edge.SVA.BLL.EarnCouponRule();
            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            Edge.Web.Tools.DataTool.AddExchangeType(ds, "ExchangeTypeName", "ExchangeType");
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");  

            DataView consumeAmountDV = ds.Tables[0].DefaultView;
            consumeAmountDV.RowFilter = "ExchangeType='5'";
            this.lbtnEarnConsumeAmountDel.Enabled=consumeAmountDV.Count > 0 ? true :false;
            this.rptEarnConsumeAmountList.DataSource = consumeAmountDV;
            this.rptEarnConsumeAmountList.DataBind();

            DataView pointDV = ds.Tables[0].DefaultView;
            pointDV.RowFilter = "ExchangeType='2'";
            this.lbtnEarnPointDel.Enabled = pointDV.Count > 0 ? true : false;
            this.rptEarnPointList.DataSource = pointDV;
            this.rptEarnPointList.DataBind();

            DataView amountDV = ds.Tables[0].DefaultView;
            amountDV.RowFilter = "ExchangeType='1'";
            this.lbtnEarnAmountDel.Enabled = amountDV.Count > 0 ? true : false;
            this.rptEarnAmountList.DataSource = amountDV;
            this.rptEarnAmountList.DataBind();

            DataView couponDV = ds.Tables[0].DefaultView;
            couponDV.RowFilter = "ExchangeType='4'";
            this.lbtnEarnCouponDel.Enabled = couponDV.Count > 0 ? true : false;
            this.rptEarnCouponList.DataSource = couponDV;
            this.rptEarnCouponList.DataBind();

            DataView amountPointDV = ds.Tables[0].DefaultView;
            amountPointDV.RowFilter = "ExchangeType='3'";
            this.lbtnEarnAmountPointDel.Enabled = amountPointDV.Count > 0 ? true : false;
            this.rptEarnAmountPointList.DataSource = amountPointDV;
            this.rptEarnAmountPointList.DataBind();
        }

        protected void lbtnEarnConsumeAmountDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptEarnConsumeAmountList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptEarnConsumeAmountList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptEarnConsumeAmountList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("EarnCouponRule/EarnConsumeAmount/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
          
        }

        protected void lbtnEarnConsumeAmountAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("EarnCouponRule/EarnConsumeAmount/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=5");
        }

        protected void lbtnEarnPointDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptEarnPointList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptEarnPointList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptEarnPointList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("EarnCouponRule/EarnPoint/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
        }

        protected void lbtnEarnPointAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("EarnCouponRule/EarnPoint/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=2");
        }

        protected void lbtnEarnAmountDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptEarnAmountList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptEarnAmountList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptEarnAmountList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("EarnCouponRule/EarnAmount/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
            //JscriptPrint(Resources.MessageTips.DeleteSuccess, "Modify.aspx?id=" + Request.Params["id"] + "&tabs=4", Resources.MessageTips.SUCESS_TITLE);
        }

        protected void lbtnEarnAmountAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("EarnCouponRule/EarnAmount/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=1");
        }

        protected void lbtnEarnCouponDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptEarnCouponList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptEarnCouponList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptEarnCouponList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("EarnCouponRule/EarnCoupon/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
            //JscriptPrint(Resources.MessageTips.DeleteSuccess, "Modify.aspx?id=" + Request.Params["id"] + "&tabs=5", Resources.MessageTips.SUCESS_TITLE);
        }

        protected void lbtnEarnCouponAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("EarnCouponRule/EarnCoupon/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=4");
        }

        protected void lbtnEarnAmountPointDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptEarnAmountPointList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptEarnAmountPointList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptEarnAmountPointList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("EarnCouponRule/EarnAmountPoint/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
            //JscriptPrint(Resources.MessageTips.DeleteSuccess, "Modify.aspx?id=" + Request.Params["id"] + "&tabs=6", Resources.MessageTips.SUCESS_TITLE);
        }

        protected void lbtnEarnAmountPointAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("EarnCouponRule/EarnAmountPoint/Add.aspx?CouponTypeID=" + Request.Params["id"] + "&ExchangeType=3");
        }



    }
}
