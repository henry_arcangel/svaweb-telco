﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Location
{
    public partial class Delete : Edge.Web.UI.ManagePage
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];

                ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
                ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());

                if (string.IsNullOrEmpty(ids))
                {
                    Response.Redirect(string.Format("List.aspx?page=0&CouponTypeID={0}&StoreConditionTypeID={1}&type=2", ViewState["couponTypeID"], ViewState["storeConditionTypeID"]));
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Delete " + ids);
                foreach (string id in ids.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {

                    if (string.IsNullOrEmpty(id)) continue;
                    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.CouponTypeStoreCondition>(Tools.ConvertTool.ToInt(id));
                }
                Response.Redirect(string.Format("List.aspx?page=0&CouponTypeID={0}&StoreConditionTypeID={1}&type=2", ViewState["couponTypeID"], ViewState["storeConditionTypeID"]));

            }
        }
    }
}
