﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmountPoint
{
    public partial class Delete : Edge.Web.UI.ManagePage
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "../../Modify.aspx?id=" + Request.Params["id"] + "&tabs=6", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Delete " + ids);
                foreach (string id in ids.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (string.IsNullOrEmpty(id)) continue;
                    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.EarnCouponRule>(Tools.ConvertTool.ToInt(id));
                }
                JscriptPrint(Resources.MessageTips.DeleteSuccess, "../../Modify.aspx?id=" + Request.Params["id"] + "&tabs=6", Resources.MessageTips.SUCESS_TITLE);
            }
        }
    }
}
