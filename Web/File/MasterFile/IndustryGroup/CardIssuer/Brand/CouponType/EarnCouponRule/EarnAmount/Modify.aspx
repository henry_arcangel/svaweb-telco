﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmount.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：修改现金兑换优惠券规则</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                修改现金兑换优惠券规则
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                优惠券类型：
            </td>
            <td width="75%">
                <asp:DropDownList ID="CouponTypeID" TabIndex="1" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                兑换类型：
            </td>
            <td>
                <asp:DropDownList ID="ExchangeType" TabIndex="2" runat="server" Enabled="false" SkinID="NoClass"
                    CssClass="dropdownlist required">
                    <asp:ListItem Text="----------" Value=""></asp:ListItem>
                    <asp:ListItem Text="金额兑换" Value="1"></asp:ListItem>
                    <asp:ListItem Text="积分兑换" Value="2" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="金额+积分兑换" Value="3">  </asp:ListItem>
                    <asp:ListItem Text="优惠券兑换" Value="4">  </asp:ListItem>
                    <asp:ListItem Text="消费金额兑换" Value="5">  </asp:ListItem>
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <%--        <tr>
            <td align="right">
                兑换规则排序：
            </td>
            <td>
                <asp:TextBox ID="ExchangeRank" TabIndex="3" runat="server" CssClass="input required digits"
                    Text="0"></asp:TextBox>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                兑换需要的现金：
            </td>
            <td>
                <asp:TextBox ID="ExchangeAmount" TabIndex="3" runat="server" CssClass="input required number"
                    Text="0"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                活动启用日期：
            </td>
            <td>
                <asp:TextBox ID="StartDate" TabIndex="4" runat="server" MaxLength="512" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'EndDate\',{d:0});}'})"
                    CssClass="input required dateISO"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                活动结束日期：
            </td>
            <td>
                <asp:TextBox ID="EndDate" TabIndex="5" runat="server" MaxLength="512" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'StartDate\',{d:0});}'})"
                    CssClass="input required dateISO"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:RadioButtonList ID="Status" runat="server" RepeatDirection="Horizontal" TabIndex="20">
                    <asp:ListItem Selected="True" Value="1">有效</asp:ListItem>
                    <asp:ListItem Value="0">无效</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit cancel" OnClick="btnReturn_Click" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
