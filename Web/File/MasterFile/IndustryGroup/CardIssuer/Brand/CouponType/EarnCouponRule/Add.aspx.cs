﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeIDLimit);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeIDLimit);
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID);
                Edge.Web.Tools.ControlTool.BindCouponType(ExchangeCouponTypeID);
            }
        }


        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string CouponTypeID = Request.Params["CouponTypeID"];
                this.CouponTypeID.SelectedValue = CouponTypeID;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            Edge.SVA.Model.EarnCouponRule item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.EarnCouponRule>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=2", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=2", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=2");
        }
    }
}
