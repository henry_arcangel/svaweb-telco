﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeIDLimit);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeIDLimit);
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID);
                Edge.Web.Tools.ControlTool.BindCouponType(ExchangeCouponTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string CouponTypeID = Request.Params["id"];
                this.CouponTypeID.SelectedValue = CouponTypeID;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.EarnCouponRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.EarnCouponRule>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "../Modify.aspx?id=" + item.CouponTypeID + "&tabs=2", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "../Modify.aspx?id=" + item.CouponTypeID + "&tabs=2", Resources.MessageTips.FAILED_TITLE);
            }

        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + this.CouponTypeID.SelectedValue + "&tabs=2");
        }
    }
}
