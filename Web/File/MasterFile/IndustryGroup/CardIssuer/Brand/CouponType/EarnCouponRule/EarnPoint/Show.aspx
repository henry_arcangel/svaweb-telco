﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnPoint.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：显示积分兑换优惠券规则 </b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                显示积分兑换优惠券规则
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                优惠券类型：
            </td>
            <td width="75%">
                <asp:DropDownList ID="CouponTypeID" runat="server" CssClass="dropdownlist" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                兑换类型：
            </td>
            <td>
                <asp:DropDownList ID="ExchangeType" runat="server" Enabled="false" SkinID="NoClass"
                    CssClass="dropdownlist">
                    <asp:ListItem Text="----------" Value=""></asp:ListItem>
                    <asp:ListItem Text="金额兑换" Value="1"></asp:ListItem>
                    <asp:ListItem Text="积分兑换" Value="2" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="金额+积分兑换" Value="3">  </asp:ListItem>
                    <asp:ListItem Text="优惠券兑换" Value="4">  </asp:ListItem>
                    <asp:ListItem Text="消费金额兑换" Value="5">  </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
<%--        <tr>
            <td align="right">
                兑换规则排序：
            </td>
            <td>
                <asp:Label ID="ExchangeRank" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                兑换需要的积分：
            </td>
            <td>
                <asp:Label ID="ExchangePoint" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                规则起始日期：
            </td>
            <td>
                <asp:Label ID="StartDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                规则失效日期：
            </td>
            <td>
                <asp:Label ID="EndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:RadioButtonList ID="Status" runat="server" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Selected="True" Value="1">有效</asp:ListItem>
                    <asp:ListItem Value="0">无效</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <%-- <input type="button" name="button1"  value="返 回" onclick= "history.back(); " class="submit"/>--%>
                    <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit cancel" OnClick="btnReturn_Click" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
