﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmount
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "Show");
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            string type =Request.Params["type"].ToLower();
            switch (type)
            {
                case "show": Response.Redirect("../../Show.aspx?id=" + this.CouponTypeID.SelectedValue + "&tabs=4"); break;
                case "modify": Response.Redirect("../../Modify.aspx?id=" + this.CouponTypeID.SelectedValue + "&tabs=4"); break;
                default: Response.Redirect("../../Show.aspx?id=" + this.CouponTypeID.SelectedValue + "&tabs=4"); break;
            }
          
        }
    }
}
