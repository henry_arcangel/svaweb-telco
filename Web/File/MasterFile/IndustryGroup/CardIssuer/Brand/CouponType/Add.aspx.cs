﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using System.Web.Services;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType, Edge.SVA.Model.CouponType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindBrand(this.BrandID);
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            logger.WriteOperationLog(this.PageName, "Add");
            Edge.SVA.Model.CouponType item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CouponTypeCode = item.CouponTypeCode.ToUpper();
                item.CouponNumMask = item.CouponNumMask.ToUpper();

                try
                {
                    if (item.IsImportCouponNumber.GetValueOrDefault() == 1)     //导入
                    {
                        item.CouponNumMask = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumMaskImport.Text.ToUpper().Trim() : "";
                        item.CouponNumPattern = item.UIDToCouponNumber.GetValueOrDefault() == 0 ? this.CouponNumPatternImport.Text.ToUpper().Trim() : "";
                        item.CouponCheckdigit = null;
                        item.CheckDigitModeID = null;
                        item.CouponNumberToUID = null;

                        if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                        if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCouponNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                        if (item.UIDToCouponNumber.GetValueOrDefault() == 0 && (string.IsNullOrEmpty(this.CouponNumMaskImport.Text.Trim()) || string.IsNullOrEmpty(this.CouponNumPatternImport.Text.Trim())))
                            throw new Exception("Binding Coupon Number Mask Rule or Coupon Number Prefix ");
                    }
                    else                                                        //手动
                    {
                        item.UIDCheckDigit = null;
                        item.IsConsecutiveUID = null;
                        item.UIDToCouponNumber = null;

                        if (item.CouponCheckdigit.GetValueOrDefault() && item.CouponNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                        if (!item.CouponCheckdigit.GetValueOrDefault() && item.CouponNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                    }
                }
                catch (Exception ex)
                {
                    JscriptPrint(ex.Message, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                if (new Edge.SVA.BLL.CouponType().isHasCouponTypeCode(item.CouponTypeCode, 0))
                {
                    JscriptPrintAndFocus(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE, this.CouponTypeCode.ClientID);
                    return;
                }
                if (this.IsImportCouponNumber.SelectedValue.Trim() == "0")
                {
                    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(), 0))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMask.ClientID);
                        return;
                    }
                }

                if (item.IsImportCouponNumber.GetValueOrDefault() == 1 && item.UIDToCouponNumber.GetValueOrDefault() == 0)
                {
                    if (Tools.DALTool.isHasCouponTypeCouponNumMask(this.CouponNumMaskImport.Text.Trim(), this.CouponNumPatternImport.Text.Trim(), 0))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMaskImport.ClientID);
                        return;
                    }
                }

                if (!Tools.DALTool.CheckNumberMask(this.CouponNumMask.Text.Trim(), this.CouponNumPattern.Text.Trim(),2,0))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90394"), "", Resources.MessageTips.WARNING_TITLE, this.CouponNumMask.ClientID);
                    return;
                }

            }

            if (Tools.DALTool.Add<Edge.SVA.BLL.CouponType>(item) > 0)
            {
                #region 新增使用产品，使用部门

                //List<Edge.SVA.Model.CouponTypeExchangeBinding> newCouponTypeExchangeBindingList = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
                //if (Session["DepartmentCouponTypeExchangeBinding"] != null)
                //{
                //    List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];
                //    foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                //    {

                //        newCouponTypeExchangeBindingList.Add(model);
                //    }
                //}

                //if (Session["ProductCouponTypeExchangeBinding"] != null)
                //{
                //    List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];

                //    foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                //    {

                //        newCouponTypeExchangeBindingList.Add(model);
                //    }
                //}

                //Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                //foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in newCouponTypeExchangeBindingList)
                //{
                //    model.CouponTypeID = id;
                //    bll.Add(model);
                //}
                #endregion

                #region 新增使用店铺/区域/品牌
                //List<Edge.SVA.Model.CouponTypeStoreCondition> allStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

                //if (Session["CouponTypeEarnStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeEarnStoreCheckedList"]);
                //}

                //if (Session["CouponTypeConsumeStoreCheckedList"] != null)
                //{
                //    allStoreCheckedList.AddRange((List<Edge.SVA.Model.CouponTypeStoreCondition>)Session["CouponTypeConsumeStoreCheckedList"]);
                //}

                //if (allStoreCheckedList.Count > 0)
                //{
                //    //为每个新的条件赋值CouponTypeID 和时间
                //    foreach (Edge.SVA.Model.CouponTypeStoreCondition newItem in allStoreCheckedList)
                //    {
                //        newItem.CouponTypeID = id;
                //        newItem.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //        newItem.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //        newItem.CreatedOn = System.DateTime.Now;
                //        newItem.UpdatedOn = System.DateTime.Now;
                //    }

                //    int add = new Edge.SVA.BLL.CouponTypeStoreCondition().AddList(allStoreCheckedList);
                //    if (add > 0)
                //    {

                //        JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //    }
                //    else
                //    {
                //        new Edge.SVA.BLL.CouponType().Delete(id);

                //        JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);

                //    }
                //}
                //else
                //{
                //    JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                //}
                #endregion

                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        private bool IsRightCouponNumPattern()
        {
            if (string.IsNullOrEmpty(CouponNumPattern.Text.Trim())) return true;

            string strCouponNumMask = CouponNumMask.Text.Trim().ToUpper();
            string strNewCouponNumMask = "";
            foreach (char c in strCouponNumMask)
            {
                if (c == 'A')
                {
                    strNewCouponNumMask = strNewCouponNumMask + c.ToString();
                }
                else
                {
                    break;
                }
            }

            int intNewCouponNumMask = strNewCouponNumMask.Length;
            int intCouponNumPattern = CouponNumPattern.Text.Trim().Length;

            return intNewCouponNumMask == intCouponNumPattern;
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.BrandID.SelectedValue))
            {
                Tools.ControlTool.BindCampaign(CampaignID);
            }
            else
            {
                Tools.ControlTool.BindCampaign(CampaignID, Tools.ConvertTool.ToInt(this.BrandID.SelectedValue));
            }
        }
    }
}
