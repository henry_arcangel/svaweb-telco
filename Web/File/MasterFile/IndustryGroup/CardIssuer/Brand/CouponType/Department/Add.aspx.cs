﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Department
{
    public partial class Add : Edge.Web.UI.ManagePage
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());

              //  this.txtCount.Text = webset.ContentPageNum.ToString();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int couponTypeID = Edge.Web.Tools.ConvertTool.ToInt(ViewState["couponTypeID"].ToString());
            logger.WriteOperationLog(this.PageName, "Add couponTypeID " + couponTypeID.ToString());
            if (couponTypeID > 0)
            {

                this.lblMsg.Visible = false;

                Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                model.BindingType = 2;
                model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                model.DepartCode = DepartCode.Text.Trim();
                model.CouponTypeID = couponTypeID;

                Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();

                if (bllExchangeBinding.GetCount(string.Format("BindingType=2 and BrandID={0} and DepartCode='{1}' and CouponTypeID={2} ", Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue), DepartCode.Text.Trim(), couponTypeID)) > 0)
                {
                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.Exists;
                    return;
                }


                if (bllExchangeBinding.Add(model) > 0)
                {
                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.AddSuccess;

                    Response.Redirect(string.Format("List.aspx?page=0&CouponTypeID={0}&type=2", ViewState["couponTypeID"]));

                    // AddModelToSession(model);
                    // BindList();
                }
                else
                {
                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.AddFailed;
                }
            }
            else
            {
                this.lblMsg.Visible = true;
                this.lblMsg.Text = Resources.MessageTips.AddFailed;
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {

            //Edge.Web.Tools.ControlTool.BindLocation(ckbLocationID, LocationName.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("List.aspx?page=0&CouponTypeID={0}&type=2", ViewState["couponTypeID"]));
        }

    }
}
