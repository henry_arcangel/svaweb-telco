﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.SaleProduct
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        public int couponTypeID;
        public int type;
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            ViewState["couponTypeID"] = this.couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
            ViewState["type"] = this.type = Request.Params["type"] == null ? 1 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["type"].ToString());

            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");
                this.lbtnDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";

                int couponTypeID = Convert.ToInt32(ViewState["couponTypeID"]);
                if (couponTypeID > 0)
                {
                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CouponTypeID={0}", couponTypeID));
                    sbWhere.Append(" and BindingType=1 and DepartCode is null ");

                    RptBind(sbWhere.ToString(), "KeyID");
                }


                if (ViewState["type"].ToString() == "1")
                {
                    this.lbtnDel.Visible = false;
                    this.lbtnAdd.Visible = false;
                }
                else
                {
                    this.lbtnDel.Visible = true;
                    this.lbtnAdd.Visible = true;
                }
            }
        }



        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();

            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }
            //获得总条数
            this.pcount = bll.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ViewState["type"].ToString() == "1")
                {
                    (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
                }
            }
            //else if (e.Item.ItemType == ListItemType.Header)
            //{
            //    if (ViewState["type"].ToString() == "1")
            //    {
            //        (e.Item.FindControl("checkBox") as CheckBox).Enabled = false;
            //    }
            //}
        }

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }

            if (string.IsNullOrEmpty(ids))
            {
                JscriptPrint(Resources.MessageTips.NotSelected, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            Response.Redirect(string.Format("Delete.aspx?CouponTypeID={0}&ids={1}", ViewState["couponTypeID"], ids));
        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("add.aspx?CouponTypeID={0}", ViewState["couponTypeID"]));
        }
    }
}