﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrandLocationStore.aspx.cs"
    Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.BrandLocationStore" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            //$("#TB_title", parent.document).hide();
        });
         
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：品牌/区域/店铺</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left" >
                品牌
            </th>
        </tr>
        <tr>
            <td width="25%" align="right" >
                指定品牌：
            </td>
            <td width="75%" >
                <asp:CheckBoxList ID="ckbBrandList" runat="server" RepeatColumns="2" 
                    BorderStyle="None" >
                </asp:CheckBoxList>
            </td>
            <%--</tr>
             <tr>
            <th colspan="2" align="left">区域</th>
        </tr>
        <tr>
            <td align="right">指定区域：</td>
            <td>
                <asp:CheckBoxList ID="ckbLocationList" runat="server">
                </asp:CheckBoxList>
            </td>
        </tr>--%>
        </tr>
            <tr>
                <th colspan="2" align="left">
                    店铺
                </th>
            </tr>
            <tr>
                <td align="right">
                    指定店铺：
                </td>
                <td>
                    <asp:CheckBoxList ID="ckbStoreList" runat="server" RepeatColumns="2" 
                        BorderStyle="None">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <asp:Button ID="btnAdd" runat="server" Text="确定" OnClick="btnAdd_Click" CssClass="submit">
                        </asp:Button>
                        &nbsp;</div>
                </td>
            </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
