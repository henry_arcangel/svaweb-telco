﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign,Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Tools.ControlTool.BindBrand(BrandID);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Campaign item = this.GetUpdateObject();

            if (item == null)
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page="+page.ToString(), Resources.MessageTips.FAILED_TITLE);
                return;
            }

            if (Tools.DALTool.isHasCampaignCode(this.CampaignCode.Text.Trim(), item.CampaignID))
            {
                JscriptPrint(Resources.MessageTips.ExistCampaignCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CampaignCode = item.CampaignCode.ToUpper();

            if (DALTool.Update<Edge.SVA.BLL.Campaign>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page="+page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page="+page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
