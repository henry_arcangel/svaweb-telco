﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
          <tr>
            <td align="right">
                活动编号：
            </td>
            <td>
                <asp:Label ID="CampaignCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                描述：
            </td>
            <td>
                <asp:Label ID="CampaignName1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:Label ID="CampaignName2" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:Label ID="CampaignName3" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                详细描述：
            </td>
            <td>
                <asp:Label ID="CampaignDetail1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他详细描述1：
            </td>
            <td>
                <asp:Label ID="CampaignDetail2" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他详细描述2：
            </td>
            <td>
                <asp:Label ID="CampaignDetail3" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:Label ID="BrandID" runat="server"></asp:Label>
                <%--<asp:DropDownList ID="BrandID" runat="server" Enabled="false">
                </asp:DropDownList>--%>
            </td>
        </tr>
<%--        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="CampaignPicFile" runat="server" FileType="preview" />
            </td>
        </tr>
        <tr>
            <td align="right">
                活动启用日期：
            </td>
            <td>
                <asp:Label ID="StartDate" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                活动结束日期：
            </td>
            <td>
                <asp:Label ID="EndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="javascript:history.back();"
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
