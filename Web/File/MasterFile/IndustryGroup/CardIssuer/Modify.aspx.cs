﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardIssuer, Edge.SVA.Model.CardIssuer>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    DataSet ds = new Edge.SVA.BLL.Industry().GetAllList();
            //    Edge.Web.Tools.ControlTool.BindDataSet(IndustryID, ds, "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3");
            //}
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.CardIssuer item = this.GetUpdateObject();
            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;

            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardIssuer>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", "Success");
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", "Failed");
            }
        }
    }
}
