﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.NumberRulesSetting
{
    public partial class Delete : Edge.Web.UI.ManagePage
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Delete");
                foreach (string id in ids.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (string.IsNullOrEmpty(id)) continue;
                    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.REFNO>(id);
                }
                JscriptPrint(Resources.MessageTips.DeleteSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
        }
    }
}
