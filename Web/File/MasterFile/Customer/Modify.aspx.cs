﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Customer
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Customer, Edge.SVA.Model.Customer>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName,"Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Customer item = this.GetUpdateObject();

            if (Tools.DALTool.isHasCustomerCode(this.CustomerCode.Text.Trim(), item.CustomerID))
            {
                JscriptPrint(Resources.MessageTips.ExistCustomerCode, "", Resources.MessageTips.WARNING_TITLE);
                this.CustomerCode.Focus();
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CustomerCode = item.CustomerCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Customer>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
