﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.StoreNature
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreType,Edge.SVA.Model.StoreType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            if (Tools.DALTool.isHasStoreTypeCode(this.StoreTypeCode.Text.Trim(), 0))
            {
                this.JscriptPrint(Resources.MessageTips.ExistStoreGroupCode, "", Resources.MessageTips.WARNING_TITLE);
                this.StoreTypeCode.Focus();
                return;
            }

            Edge.SVA.Model.StoreType item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.StoreTypeCode = item.StoreTypeCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.StoreType>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + item.StoreTypeCode);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + item == null ? "No Data" : item.StoreTypeCode);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
         
        }
    }
}
