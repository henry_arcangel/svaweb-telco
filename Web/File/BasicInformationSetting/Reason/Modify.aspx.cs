﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.Reason
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Reason,Edge.SVA.Model.Reason>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);
            Edge.SVA.Model.Reason item = this.GetUpdateObject();

            if (Tools.DALTool.isHasReasonCode(this.ReasonCode.Text.Trim(), item.ReasonID))
            {
                this.JscriptPrint(Resources.MessageTips.ExistReasonCode, "", Resources.MessageTips.WARNING_TITLE);
                this.ReasonCode.Focus();
                return;
            }

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.ReasonCode = item.ReasonCode.ToUpper();
            }
            if (DALTool.Update<Edge.SVA.BLL.Reason>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + item.ReasonCode);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Update\t Code:" + item == null ? "No Data" : item.ReasonCode);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

        }
    }
}
