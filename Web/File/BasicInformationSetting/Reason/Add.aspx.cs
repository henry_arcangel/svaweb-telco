﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.Reason
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Reason,Edge.SVA.Model.Reason>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            if (Tools.DALTool.isHasReasonCode(this.ReasonCode.Text.Trim(), 0))
            {
                this.JscriptPrint(Resources.MessageTips.ExistReasonCode, "", Resources.MessageTips.WARNING_TITLE);
                this.ReasonCode.Focus();
                return;
            }

            Edge.SVA.Model.Reason item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.ReasonCode = item.ReasonCode.ToUpper();
            }
       
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Reason>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + item.ReasonCode);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Reason Add\t Code:" + item == null ? "No Data" : item.ReasonCode);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }

        }
    }
}
