﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.PasswordRulesSetting.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript">
        $(function () {
            jQuery.validator.addMethod("PWDMinLength", function (value, element) {


                if (value == null || value.length <= 0) return true;
                if ($("#PWDMaxLength").val().length <= 0) return true;

                var max = parseInt($("#PWDMaxLength").val().replace(/,/g, ""));
                var min = parseInt(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.PWDMinLength);

            jQuery.validator.addMethod("PWDMaxLength", function (value, element) {

                if (value == null || value.length <= 0) return true;
                if ($("#PWDMinLength").val().length <= 0) return true;

                var min = parseInt($("#PWDMinLength").val().replace(/,/g, ""));
                var max = parseInt(value.replace(/,/g, ""));

                return max >= min;

            }, jQuery.validator.messages.PWDMaxLength);

            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            $("#PWDStructure").change(function () {
                checkStruct();
            });

            checkStruct();
        });

        function checkStruct() {
            var struct = $("#PWDStructure").val();
            if (struct == undefined || struct == "0") {
                $("#PWDMinLength,#PWDMaxLength").val("0");
                $("#PWDMinLength,#PWDMaxLength").attr('disabled', 'true');
            }
            else {
                $("#PWDMinLength,#PWDMaxLength").removeAttr('disabled');
            }
        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                密码编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="PasswordRuleCode" TabIndex="1" runat="server" CssClass="input required svaCode"
                    MaxLength="20" hinttitle="密码编号" hintinfo="Translate__Special_121_StartKey identifier of Password Rule Setting.1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                描述：
            </td>
            <td width="75%">
                <asp:TextBox ID="Name1" TabIndex="2" runat="server" CssClass="input required" hinttitle="描述"
                    hintinfo="请输入规范的密碼規則名称。不能超過512個字符"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                其他描述1：
            </td>
            <td width="75%">
                <asp:TextBox ID="Name2" TabIndex="3" runat="server" CssClass="input" hinttitle="其他描述1"
                    hintinfo="對密碼規則的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                其他描述2：
            </td>
            <td width="75%">
                <asp:TextBox ID="Name3" TabIndex="4" runat="server" CssClass="input" hinttitle="其他描述2"
                    hintinfo="對密碼規則的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码构成：
            </td>
            <td>
                <asp:DropDownList ID="PWDStructure" runat="server" SkinID="NoClass" CssClass="dropdownlist required">
                    <asp:ListItem Value="0" title="No Requirement">No Requirement</asp:ListItem>
                    <asp:ListItem Value="1" title="Numbers">Numbers </asp:ListItem>
                    <asp:ListItem Value="2" title="Alphabets">Alphabets </asp:ListItem>
                    <asp:ListItem Value="3" title="Numbers + Alphabets">Numbers + Alphabets </asp:ListItem>
                    <asp:ListItem Value="4" title="Numbers +Alphabets +Symbols">Numbers +Alphabets +Symbols</asp:ListItem>
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                密码最小长度：
            </td>
            <td width="75%">
                <asp:TextBox ID="PWDMinLength" TabIndex="5" runat="server" CssClass="input required digits PWDMinLength"
                    hinttitle="密码最小长度" hintinfo="只能輸入正整數"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码最大长度：
            </td>
            <td>
                <asp:TextBox ID="PWDMaxLength" TabIndex="6" runat="server" CssClass="input required digits PWDMaxLength"
                    hinttitle="密码最大长度" hintinfo="只能輸入正整數"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码安全级别：
            </td>
            <td>
                <asp:TextBox ID="PWDSecurityLevel" TabIndex="7" runat="server" CssClass="input required digits"
                    hinttitle="密码安全级别" hintinfo="只能输入整数，0为最低，数字越大，级别越高"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数：
            </td>
            <td>
                <asp:TextBox ID="PWDValidDays" TabIndex="8" runat="server" CssClass="input required digits"
                    hinttitle="密码有效天数" hintinfo="只能輸入正整數"></asp:TextBox>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数的单位：
            </td>
            <td>
                <asp:DropDownList ID="PWDValidDaysUnit" TabIndex="9" runat="server" CssClass="dropdownlist required"
                    SkinID="NoClass">
                    <asp:ListItem Text="--------" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="年" Value="1" title="年"></asp:ListItem>
                    <asp:ListItem Text="月" Value="2" title="月"></asp:ListItem>
                    <asp:ListItem Text="星期" Value="3" title="星期"></asp:ListItem>
                    <asp:ListItem Text="天" Value="4" title="天"></asp:ListItem>
                </asp:DropDownList>
                <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否会员密码规则：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberPWDRule" runat="server" RepeatDirection="Horizontal"
                    TabIndex="10">
                    <asp:ListItem Text="是" Value="1"></asp:ListItem>
                    <asp:ListItem Text="否" Value="0" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提 交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2">
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
