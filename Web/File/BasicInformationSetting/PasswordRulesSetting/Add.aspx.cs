﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Add");
            if (Tools.DALTool.isHasPasswordRuleCode(this.PasswordRuleCode.Text.Trim(), 0))
            {
                this.JscriptPrintAndFocus(Resources.MessageTips.ExistPasswordSettingCode, "", Resources.MessageTips.WARNING_TITLE, this.PasswordRuleCode.ClientID);
                return;
            }
            if (this.MemberPWDRule.SelectedValue == "1")
            {
                System.Data.DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("MemberPWDRule = 1");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    JscriptPrint(Resources.MessageTips.ExistMemberRule, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
            }

            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            if (bll.GetCount(string.Format("PWDSecurityLevel = {0}",Tools.ConvertTool.ConverType<int>(this.PWDSecurityLevel.Text))) > 0)
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistPasswordSecurity, "", Resources.MessageTips.WARNING_TITLE, this.PWDSecurityLevel.ClientID);
                return;
            }

            Edge.SVA.Model.PasswordRule item = this.GetAddObject();
            
            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.PasswordRuleCode = item.PasswordRuleCode.ToUpper();
                item.PWDMinLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMinLength.GetValueOrDefault();
                item.PWDMaxLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMaxLength.GetValueOrDefault();

            }
            if (DALTool.Add<Edge.SVA.BLL.PasswordRule>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + item.PasswordRuleCode);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Add\t Code:" + item == null ? "No Data" : item.PasswordRuleCode);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
