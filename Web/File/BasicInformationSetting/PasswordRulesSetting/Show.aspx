﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.PasswordRulesSetting.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                描述：
            </td>
            <td width="75%">
                <asp:Label ID="Name1" TabIndex="1" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                其他描述1：
            </td>
            <td width="75%">
                <asp:Label ID="Name2" TabIndex="1" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                其他描述2：
            </td>
            <td width="75%">
                <asp:Label ID="Name3" TabIndex="1" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                密码最小长度：
            </td >
            <td width="75%">
                <asp:Label ID="PWDMinLength" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码最小长度：
            </td>
            <td>
                <asp:Label ID="PWDMaxLength" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码安全级别：
            </td>
            <td>
                <asp:Label ID="PWDSecurityLevel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码构成：
            </td>
            <td>
             <asp:Label ID="PWDStructureView" runat="server"></asp:Label>
             <asp:DropDownList  ID="PWDStructure" runat="server" SkinID="NoClass" CssClass="dropdownlist " Enabled="false">
                        <asp:ListItem Value="0">No Requirement</asp:ListItem>
                        <asp:ListItem Value="1">Numbers </asp:ListItem>
                        <asp:ListItem Value="2">Alphabets </asp:ListItem>
                        <asp:ListItem Value="3">Numbers + Alphabets </asp:ListItem>
                        <asp:ListItem Value="4">Numbers +Alphabets +Symbols</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数：
            </td>
            <td>
                <asp:Label ID="PWDValidDays" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数的单位：
            </td>
            <td>
                <asp:Label ID="PWDValidDaysUnitView" runat="server"></asp:Label>
                <asp:DropDownList ID="PWDValidDaysUnit" TabIndex="3" runat="server" CssClass="dropdownlist required"
                    Enabled="false" SkinID="NoClass">
                    <asp:ListItem Text="--------" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="永久" Value="0"></asp:ListItem>
                    <asp:ListItem Text="年" Value="1"></asp:ListItem>
                    <asp:ListItem Text="月" Value="2"></asp:ListItem>
                    <asp:ListItem Text="星期" Value="3"></asp:ListItem>
                    <asp:ListItem Text="天" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
      <tr>
            <td align="right">
                是否会员密码规则：
            </td>
            <td>
                <asp:Label ID="MemberPWDRuleView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="MemberPWDRule" runat="server" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="是" Value="1"></asp:ListItem>
                    <asp:ListItem Text="否" Value="0" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
