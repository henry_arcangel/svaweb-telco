﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                this.PWDStructureView.Text = this.PWDStructure.SelectedItem == null ? "" : this.PWDStructure.SelectedItem.Text;
                this.PWDStructure.Visible = false;

                this.PWDValidDaysUnitView.Text = this.PWDValidDaysUnit.SelectedItem == null ? "" : this.PWDValidDaysUnit.SelectedItem.Text;
                this.PWDValidDaysUnit.Visible = false;

                this.MemberPWDRuleView.Text = this.MemberPWDRule.SelectedItem == null ? "" : this.MemberPWDRule.SelectedItem.Text;
                this.MemberPWDRule.Visible = false;
            }
        }
    }
}
