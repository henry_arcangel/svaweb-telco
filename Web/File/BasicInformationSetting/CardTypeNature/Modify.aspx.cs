﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.CardTypeNature
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardTypeNature, Edge.SVA.Model.CardTypeNature>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
      
            Edge.SVA.Model.CardTypeNature item = this.GetUpdateObject();
            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }
        
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardTypeNature>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
