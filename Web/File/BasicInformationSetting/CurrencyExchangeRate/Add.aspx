﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                币种编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="CurrencyCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode" hinttitle="币种编号" hintinfo="Translate__Special_121_StartKey identifier of Currency.1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span
                    class="star">*</span>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                描述：
            </td>
            <td width="75%">
                <asp:TextBox ID="CurrencyName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required" hinttitle="描述" hintinfo="请输入规范的幣種名称。不能超過512個字符"></asp:TextBox><span
                    class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="CurrencyName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input" hinttitle="其他描述1" hintinfo="對幣種的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="CurrencyName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input" hinttitle="其他描述2" hintinfo="對幣種的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <%--<tr>
            <td align="right">货币编码：</td>
            <td>
                <asp:textbox id="CurrencyCode" tabIndex="4" runat="server" MaxLength="512" CssClass="input required"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td align="right">与本币汇率：</td>
            <td>
                <asp:textbox id="CurrencyRate" tabIndex="5" runat="server" Width="249px" MaxLength="20" CssClass="input required digits" ></asp:textbox>
            </td>
        </tr>
        <tr>
            <td align="right">是否本币：</td>
            <td>
               <asp:RadioButtonList ID="IsLocalCurrency" tabIndex="6" runat="server" RepeatDirection ="Horizontal">
                        <asp:ListItem Text="是" Value ="1"></asp:ListItem>
                        <asp:ListItem Text="否" Value ="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>--%>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提 交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2">
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
