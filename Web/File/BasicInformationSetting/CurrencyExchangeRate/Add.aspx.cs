﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Currency, Edge.SVA.Model.Currency>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Add ");
            //if (!Controllers.RegexController.GetInstance().IsSvaCode(this.CurrencyCode.Text.Trim()))
            //{
            //    this.JscriptPrintAndFocus(Resources.MessageTips.CodeFormatError, "", Resources.MessageTips.WARNING_TITLE, this.CurrencyCode.ClientID);
            //    return;
            //}

            if (Tools.DALTool.isHasCurrencyCode(this.CurrencyCode.Text.Trim(), 0))
            {
                this.JscriptPrintAndFocus(Resources.MessageTips.ExistCurrencyCode, "", Resources.MessageTips.WARNING_TITLE, this.CurrencyCode.ClientID);
                return;
            }

            Edge.SVA.Model.Currency item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;

                item.CurrencyCode = item.CurrencyCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Currency>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Currency Success Code:" + item.CurrencyCode);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Currency Failed Code:" + item == null ? "No Data" : item.CurrencyCode);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }

        }
    }
}
