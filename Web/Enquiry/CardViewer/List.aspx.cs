﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Enquiry.CardViewer
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = 10;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Edge.SVA.BLL.ReceiveTxn rt = new Edge.SVA.BLL.ReceiveTxn();
            string cardNo = Common.PageValidate.SqlTextClear(this.txtCardNo.Text);
            DataSet ds = rt.GetList(10, string.Format("CardNumber = '{0}'", cardNo), "TxnDate desc");

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                JscriptPrint(Resources.MessageTips.NoData, "List.aspx", Resources.MessageTips.WARNING_TITLE);
                return;
            }

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        
    }
}
