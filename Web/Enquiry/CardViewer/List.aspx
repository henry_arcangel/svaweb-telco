﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Enquiry.CardViewer.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：卡记录查询</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr style="text-align: center;">
                <td align="right">
                    卡号
                </td>
                <td width="210px">
                    <asp:TextBox ID="txtCardNo" runat="server" Width="300px" CssClass="input required digits"></asp:TextBox>
                </td>
                <td align="left">
                    <asp:Button ID="btnSearch" runat="server" Text="搜索" CssClass="submit" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="6%">
                        编号
                    </th>
                    <th width="6%">
                        店铺编号
                    </th>
                    <th width="10%">
                        服务器编号
                    </th>
                    <th width="10%">
                        交易单号
                    </th>
                    <th width="15%">
                        交易日期
                    </th>
                    <th width="10%">
                        操作金额
                    </th>
                    <th width="10%">
                        操作积分
                    </th>
                    <th width="6%">
                        状态
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <a href="show.aspx?id=<%#Eval("KeyID") %>">
                        <asp:Label ID="lb_id" runat="server" Text='<%#Eval("KeyID")%>'></asp:Label></a>
                </td>
                <td align="center">
                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("StoreCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName1" runat="server" Text='<%#Eval("ServerCode")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName2" runat="server" Text='<%#Eval("TxnNo")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblIndustryName3" runat="server" Text='<%#Eval("BusDate")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblUpdatedOn" runat="server" Text='<%#Eval("Points")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr" runat="server" visible="false">
        </div>
        <uc2:checkright ID="Checkright1" runat="server" />
    </form>

    <script type="text/javascript">
        
        $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

</body>
</html>
