﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Tools
{
    /// <summary>
    /// 封装控件相关操作
    /// </summary>
    public class ControlTool
    {

        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {

                    ListItem li =new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = dr[key].ToString().Trim() };
                    li.Attributes["title"] = dr[key].ToString().Trim();
                    ddl.Items.Add(li);
                }
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        /// <summary>
        /// 根据当前语言绑定数据源到DropdownList
        /// </summary>
        /// <param name="ddl">要绑定的DropdownList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">DropdownList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(DropDownList ddl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            ddl.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(),dr[code].ToString().Trim());
                    ListItem li = new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = text };
                    li.Attributes.Add("title",text);

                    ddl.Items.Add(li);
                }
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        public static string GetDropdownListText(string text, string code)
        {
            return string.Format("{0} - {1}", code, text);
        }

        public static void AddTitle(DropDownList ddl)
        {
            if (ddl == null) return;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                ddl.Items[i].Attributes.Add("title", ddl.Items[i].Text);
            }
        }

        public static void BindDictionaryValueKey(DropDownList ddl, Dictionary<string, string> source)
        {
            ddl.Items.Clear();
            foreach (KeyValuePair<string, string> keyValue in source)
            {
                ListItem li = new ListItem() { Value = keyValue.Value, Text = keyValue.Key };
                li.Attributes["title"] = keyValue.Key;
                ddl.Items.Add(li);
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.

        }
        /// <summary>
        /// 根据当前语言绑定数据源到CheckBoxList
        /// </summary>
        /// <param name="ddl">要绑定的CheckBoxList</param>
        /// <param name="source">数据源</param>
        /// <param name="keyValue">CheckBoxList Value的值</param>
        /// <param name="keyText1">英文-> 数据库名字1</param>
        /// <param name="keyText2">简体中文-> 数据库名字2</param>
        /// <param name="keyText3">繁体中文-> 数据库名字3</param>
        public static void BindDataSet(CheckBoxList ckbList, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, string code)
        {
            ckbList.Items.Clear();

            if (source != null && source.Tables != null && source.Tables.Count > 0)
            {
                string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    string text = GetDropdownListText(dr[key].ToString().Trim(), dr[code].ToString().Trim());
                    ckbList.Items.Add(new ListItem() { Value = dr[keyValue].ToString().Trim(), Text = text });
                }
            }
        }

        public static void BindDataSetCheckboxList(CheckBoxList cbl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3)
        {
            if (source == null || source.Tables == null || source.Tables.Count <= 0) return;
            cbl.Items.Clear();
            string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);
            cbl.DataSource = source.Tables[0].DefaultView;
            cbl.DataTextField = key;
            cbl.DataValueField = keyValue;
            cbl.DataBind();
        }

        public static void BindDataSetCheckboxList(CheckBoxList cbl, DataSet source, string keyValue, string keyText1, string keyText2, string keyText3, List<string> keyValueList)
        {
            if (source == null || source.Tables == null || source.Tables.Count <= 0) return;
            cbl.Items.Clear();

            string key = DALTool.GetStringByCulture(keyText1, keyText2, keyText3);

            foreach (DataRow dr in source.Tables[0].Rows)
            {
                dr["BrandName1"] = dr["BrandCode"] + "-" + dr["BrandName1"];
                dr["BrandName2"] = dr["BrandCode"] + "-" + dr["BrandName2"];
                dr["BrandName3"] = dr["BrandCode"] + "-" + dr["BrandName3"];
            }
           
            cbl.DataSource = source.Tables[0].DefaultView;
            cbl.DataTextField = key;
            cbl.DataValueField = keyValue;
            cbl.DataBind();

            foreach (ListItem item in cbl.Items)
            {
                if (keyValueList.Exists(i => i.Equals(item.Value)))
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }

        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardIssuer(DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardIssuer().GetAllList();
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardIssuerID", "CardIssuerName1", "CardIssuerName2", "CardIssuerName3");
        }


        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponStatus(DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CouponController.CouponStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new ListItem() { Text = DALTool.GetCouponTypeStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });

        }


        /// <summary>
        /// 绑定所有发行商
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardStatus(DropDownList ddl)
        {

            ddl.Items.Clear();


            System.Collections.IEnumerator enumertor = Enum.GetValues(typeof(CardController.CardStatus)).GetEnumerator();

            while (enumertor.MoveNext())
            {
                object value = enumertor.Current;

                ddl.Items.Add(new ListItem() { Text = DALTool.GetCardStatusName((int)value), Value = ((int)value).ToString() });
            }


            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });

        }

        /// <summary>
        /// 绑定所有品牌
        /// </summary>
        /// <param name="ddl">品牌DropdownList</param>
        public static void BindBrand(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Brand().GetList("1 = 1 order by BrandCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3","BrandCode");

        }

        /// <summary>
        /// 绑定所有行业
        /// </summary>
        /// <param name="ddl">行业DropdownList</param>
        public static void BindIndustry(DropDownList ddl)
        {

            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Industry().GetList("1 = 1 order by IndustryCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3","IndustryCode");
        }

        /// <summary>
        /// 绑定所有币种
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCurrency(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Currency().GetList("1 = 1 order by CurrencyCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CurrencyID", "CurrencyName1", "CurrencyName2", "CurrencyName3", "CurrencyCode");
        }

        /// <summary>
        /// 绑定所有活动
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCampaign(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList("1 = 1 order by CampaignCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }

        /// <summary>
        /// 绑定所有活动
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCampaign(DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Campaign().GetList(string.Format(" BrandID = {0} order by CampaignCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CampaignID", "CampaignName1", "CampaignName2", "CampaignName3", "CampaignCode");
        }

        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("1 = 1 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }


        /// <summary>
        /// 绑定所有卡类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardTypeForManual(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardType().GetList("IsImportUIDNumber = 0 order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");
        }

        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList("1 = 1 order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }


        /// <summary>
        /// 绑定所有卡级别
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCardGrade(DropDownList ddl, int cardTypeID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(string.Format("CardTypeID={0} order by CardGradeCode", cardTypeID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList("1 = 1 order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl, string strWhere)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(strWhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3","CouponTypeCode");
        }

        /// <summary>
        /// 绑定所有优惠券类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindCouponType(DropDownList ddl, int brandID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponType().GetList(string.Format(" BrandID = {0} order by CouponTypeCode",brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");
        }

        public static void BindCouponGrade(DropDownList ddl)
        {
            ddl.Items.Clear();
            ControlTool.BindDataSet(ddl, null, null, null, null, null);
        }


        /// <summary>
        /// 绑定所有联系方式类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindSNSType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.SNSType().GetAllList();
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "SNSTypeID", "SNSTypeName1", "SNSTypeName2", "SNSTypeName3");
        }

        /// <summary>
        /// 绑定店铺类型
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindStoreType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.StoreType().GetList("1 = 1 order by StoreTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "StoreTypeID", "StoreTypeName1", "StoreTypeName2", "StoreTypeName3","StoreTypeCode");
        }


        /// <summary>
        /// 绑定原因
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindReasonType(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Reason().GetList("1 = 1 order by ReasonCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "ReasonID", "ReasonDesc1", "ReasonDesc2", "ReasonDesc3", "ReasonCode");
        }


        /// <summary>
        /// 绑定语言列表
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindLanguageMap(DropDownList ddl)
        {
            DataSet ds = new Edge.SVA.BLL.LanguageMap().GetAllList();
            ddl.Items.Clear();

            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ddl.DataTextField = "LanguageDesc";
                ddl.DataValueField = "KeyID";
                ddl.DataSource = ds;
                ddl.DataBind();
            }

            ddl.Items.Insert(0, new ListItem() { Text = "----------", Value = "" });//Add by Nathan for All dropdownlist default value.
        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList("1 = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreCodeWithBrand(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithBrand(DropDownList dll, int brandID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList(string.Format(" BrandID = {0} order by StoreCode", brandID));
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStore(CheckBoxList ckbList, int storeTypeID, int brandID,string storeCode, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" 1=1 ");
            if (storeTypeID > 0)
            {
                sbWhere.Append(string.Format(" and StoreTypeID = {0}", storeTypeID));
            }
            if (brandID > 0)
            {
                sbWhere.Append(string.Format(" and BrandID = {0}", brandID));
            }
            if (!string.IsNullOrEmpty(name))
            {
                sbWhere.Append(string.Format(" and (StoreName1 like '%{0}%' or StoreName2 like '%{0}%' or StoreName3 like '%{0}%')", name));
            }
            if (!string.IsNullOrEmpty(storeCode))
            {
                sbWhere.Append(string.Format(" and StoreCode = '{0}'", storeCode));
            }

            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Store().GetList(top, sbWhere.ToString(), "StoreCode");
            }
            else
            {

                ds = new Edge.SVA.BLL.Store().GetList(sbWhere.ToString());
            }

            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        /// <summary>
        /// 绑定所有品牌
        /// </summary>
        /// <param name="ddl">品牌CheckBoxList</param>
        public static void BindBrand(CheckBoxList ckbList,string brandCode, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" 1=1 ");
            if (!string.IsNullOrEmpty(name))
            {
                    sbWhere.Append(string.Format(" and (BrandName1 like '%{0}%' or BrandName2 like '%{0}%' or BrandName3 like '%{0}%')", name));
            }
            if (!string.IsNullOrEmpty(brandCode))
            {
                sbWhere.Append(string.Format(" and BrandCode = '{0}'", brandCode));
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.Brand().GetList(top, sbWhere.ToString(), "BrandID");
            }
            else
            {

                ds = new Edge.SVA.BLL.Brand().GetList(sbWhere.ToString());
            }
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "BrandID", "BrandName1", "BrandName2", "BrandName3", "BrandCode");

        }

        /// <summary>
        /// 绑定所有区域
        /// </summary>
        /// <param name="ddl">区域CheckBoxList</param>
        public static void BindLocation(CheckBoxList ckbList, string name, int top)
        {
            ckbList.Items.Clear();
            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(name))
            {
                if (sbWhere.Length > 0)
                {
                    sbWhere.Append(string.Format(" and (StoreGroupName1 like '%{0}%' or StoreGroupName2 like '%{0}%' or StoreGroupName3 like '%{0}%')", name));
                }
                else
                {
                    sbWhere.Append(string.Format(" (StoreGroupName1 like '%{0}%' or StoreGroupName2 like '%{0}%' or StoreGroupName3 like '%{0}%')", name));
                }
            }
            DataSet ds = new DataSet();
            if (top > 0)
            {
                ds = new Edge.SVA.BLL.StoreGroup().GetList(top, sbWhere.ToString(), "StoreGroupID");
            }
            else
            {

                ds = new Edge.SVA.BLL.StoreGroup().GetList(sbWhere.ToString());
            }
            Edge.Web.Tools.ControlTool.BindDataSet(ckbList, ds, "StoreGroupID", "StoreGroupName1", "StoreGroupName2", "StoreGroupName3", "StoreGroupCode");
        }

        /// <summary>
        /// 绑定所有店铺
        /// </summary>
        /// <param name="ddl">店铺CheckBoxList</param>
        public static void BindStoreWithStoreCode(DropDownList dll)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Store().GetList("1 = 1 order by StoreCode");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");

        }

        public static void BindPasswordRule(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("1 = 1 order by PasswordRuleCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "PasswordRuleID", "Name1", "Name2", "Name3", "PasswordRuleCode");
        }

        public static void BindProduct(DropDownList ddl)
        {
            ddl.Items.Clear();
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
            DataSet ds = bll.FetchFields("CouponTypeID,BrandID,ProdCode", "BindingType = 2 and  ProdCode is not null and DepartCode is null", null);
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CouponTypeIDS", "");

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string key = string.Format("{0} - {1}", dr["BrandCode"].ToString().Trim(), dr["ProdCode"].ToString().Trim());
                string couponTypeID = dr["CouponTypeID"].ToString().Trim();
                if (dic.ContainsKey(key))
                {
                    dic[key] += string.Format(",{0}", couponTypeID);
                    continue;
                }
                dic.Add(key, dr["CouponTypeID"].ToString().Trim());
            }

            BindDictionaryValueKey(ddl, dic);

        }

        public static void BindDeparment(DropDownList ddl)
        {
            
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
            DataSet ds = bll.FetchFields("CouponTypeID,BrandID,DepartCode", "BindingType = 2 and  ProdCode is null and DepartCode is not null", null);
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string key = string.Format("{0} - {1}", dr["BrandCode"].ToString().Trim(), dr["DepartCode"].ToString().Trim());
                string couponTypeID = dr["CouponTypeID"].ToString().Trim();
                if (dic.ContainsKey(key))
                {
                    dic[key] += string.Format(",{0}", couponTypeID);
                    continue;
                }
                dic.Add(key, dr["CouponTypeID"].ToString().Trim());
            }

            BindDictionaryValueKey(ddl, dic);

           
        }

        public static void BindProduct(DropDownList dll, int couponTypeID)
        {
            dll.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is not null and DepartCode is null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Edge.Web.Tools.ControlTool.BindDataSet(dll, ds, "CouponTypeID", "ProdCode", "ProdCode", "ProdCode", "BrandCode");
        }

        public static void BindDeparment(DropDownList ddl, int couponTypeID)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.CouponTypeExchangeBinding().GetList("BindingType = 2 and  ProdCode is null and DepartCode is not null and CouponTypeID = " + couponTypeID.ToString());
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CouponTypeID", "DepartCode", "DepartCode", "DepartCode", "BrandCode");
        }

        public static void BindCustomer(DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Customer().GetList("1 = 1 order by CustomerCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "CustomerID", "CustomerDesc1", "CustomerDesc2", "CustomerDesc3", "CustomerCode");
        }
    }
}
