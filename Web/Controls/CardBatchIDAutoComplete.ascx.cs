﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;

namespace Edge.Web.Controls
{
    public partial class CardBatchIDAutoComplete : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.DataSource = Edge.Web.Controllers.CardBatchController.GetInstance().GetBatch(10); 
            }
        }

        [Browsable(true)]
        public string Text
        {
            set { this.BatchCouponID.Text = value; }
            get { return this.BatchCouponID.Text; }
        }

        public int Value
        {
            get 
            {
                Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
                DataSet ds = bll.GetList(string.Format("[BatchCardCode] = '{0}'", Edge.Common.WebCommon.No_SqlHack(this.Text)));
                if (ds == null || ds.Tables.Count <= 0) return -1;
                if (ds.Tables.Count != 1) return -1;

                List<Edge.SVA.Model.BatchCard> models = bll.DataTableToList(ds.Tables[0]);

                if (models.Count <= 0) return -1;

                return models[0].BatchCardID;
            }
            set { this.BatchCouponIDValue.Value = value.ToString() ; }
        }

        [Browsable(true)]
        public bool Enable
        {
            set { this.BatchCouponID.Enabled = value; }
            get { return this.BatchCouponID.Enabled; }
        }

        [Browsable(true)]
        public string CoupontTypeClientID
        {
            get;
            set;
        }

        [Browsable(true)]
        public int TabIndex
        {
            set
            {
                this.BatchCouponID.Attributes["TabIndex"] = value.ToString();
            }
        }

        public Dictionary<int, string> DataSource
        {
            set
            {
                this.BatchCouponID.Text = "";
                this.NewBatch.DataSource = value;
                this.NewBatch.DataBind();
            }
        }
    }
}