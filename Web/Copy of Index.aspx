﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Edge.Web.Index" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href='<%#GetJSThickBoxCssPath() %>' />

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQeruyAlertsPath() %>'></script>

    <link media="screen" href='<%#GetjQeruyAlertsStylePath() %>' type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">

    <script type="text/javascript">
        $(window).resize(function() {
            myWindowResize();
        })

        $(window).ready(function() {
            myWindowResize();
        })

        function myWindowResize() {
            var divheight;
            divheight = $(window).height() - $("#mainTop1").height() - $("#mainTop2").height() - $("#mainFooter").height();
            $("#mainLeft").height(divheight);
            $("#sysMain").height(divheight);
        }
    </script>

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background: #EBF5FC;">
        <tbody>
            <tr>
                <td id="mainTop1" height="70" colspan="3" style="background: url(images/head_bg.gif);">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="24%" height="70">
                                <img src="images/head_logo.png" alt="Logo" width="233" height="50" />
                            </td>
                            <td width="76%" valign="bottom">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="mainTop2" height="30" colspan="3" style="padding: 0px 10px; font-size: 12px;
                    background: url(images/navsub_bg.gif) repeat-x;">
                    <div style="float: right; line-height: 20px;">
                        <%-- <a href="AdminCenter.aspx" target="sysMain">管理中心</a> | <a target="_blank" href="../">
                            预览网站</a> |--%>
                        <asp:LinkButton ID="lbtnExit" runat="server" OnClick="lbtnExit_Click">安全退出</asp:LinkButton>
                    </div>
                    <div style="padding-left: 20px; line-height: 20px; background: url(images/siteico.gif) 0px 0px no-repeat;">
                        当前登录用户： <font color="#FF0000">
                            <asp:Label ID="lblAdminName" runat="server" Text="Label"></asp:Label></font>您好，欢迎光临。
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top" id="mainLeft" style="background: #FFF;">
                    <div style="text-align: left; width: 185px; height: 100%; overflow: scroll; font-size: 12px;">
                        <asp:TreeView ID="TreeView1" runat="server">
                        </asp:TreeView>
                    </div>
                </td>
                <td valign="middle" style="width: 8px; background: url(images/main_cen_bg.gif) repeat-x;">
                    <div id="sysBar" style="cursor: pointer;">
                        <img id="barImg" src="images/butClose.gif" alt="Close/Open left menu" />
                    </div>
                </td>
                <td id="mainRight" style="width: 100%" valign="top">
                    <iframe frameborder="0" id="sysMain" name="sysMain" scrolling="yes" src="AdminCenter.aspx"
                        style="height: 100%; visibility: inherit; width: 100%; z-index: 1;"></iframe>
                </td>
            </tr>
            <tr>
                <td id="mainFooter" height="28" colspan="3" bgcolor="#EBF5FC" style="padding: 0px 10px;
                    font-size: 10px; color: #2C89AD; background: url(images/foot_bg.gif) repeat-x;">
                </td>
            </tr>
        </tbody>
    </table>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
