﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web
{
    public partial class TempImage : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (Request.Params.HasKeys() && Request.Params.GetKey(0).ToLower() == "url")
                {
                    string url = Request.QueryString["url"];
                    if (!url.StartsWith("~"))
                    {
                        url = url.Insert(0, "~");
                    }
                    if (!System.IO.File.Exists(Server.MapPath(url)))
                    {
                        this.ErrorMessage.Text = Resources.MessageTips.FileNotExist;

                        this.img.Visible = false;
                    }
                    else
                    {
                        this.img.ImageUrl = url;
                    }
                }
                else if (Session["TempImage"] != null)
                {
                    byte[] bytes = Session["TempImage"] as byte[];

                    if (bytes != null && bytes.Length >= 1)
                    {
                        Response.BinaryWrite(bytes);
                    }
                }
                 
            }
        }
    }
}
