﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;

namespace Edge.Web.SysManage
{
	/// <summary>
	/// LogIndex 的摘要说明。
	/// </summary>
    public partial class LogIndex : Edge.Web.UI.ManagePage
	{

        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

		protected void Page_Load(object sender, System.EventArgs e)
		{
            this.pagesize = webset.ContentPageNum;

            if (!Page.IsPostBack)
            {

                RptBind("ID>0", "ID ASC");

            }

            //if (!Page.IsPostBack)
            //{
			
                //int pageIndex=1;
                //if(Request.Params["page"]!=null && Request.Params["page"].ToString()!="")
                //{
                //    Session["pagelog"]=Convert.ToInt32(Request.Params["page"]);
                //    pageIndex=Convert.ToInt32(Request.Params["page"]);
                //}
                //else
                //{
                //    if(Session["pagelog"]!=null && Session["pagelog"].ToString()!="")
                //    {
                //        pageIndex=Convert.ToInt32(Session["pagelog"]);
                //    }
                //    else
                //    {
                //        pageIndex=1;
                //        Session["pagelog"]=1;
                //    }
                //}

                //dataBind(pageIndex);
			//}
		}
        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();


            //获得总条数
            this.pcount = sm.GetLogCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = sm.GetLogPageList(this.pagesize, this.page, strWhere, orderby);
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion



        //private void dataBind(int pageIndex)
        //{
        //    pageIndex--;
        //    Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
        //    DataSet ds=new DataSet(); 
        //    ds=sm.GetLogs("");
        //    grid.DataSource=ds.Tables[0].DefaultView;
        //    int record_Count=ds.Tables[0].Rows.Count;
        //    int page_Size=grid.PageSize;
        //    int totalPages = int.Parse(Math.Ceiling((double)record_Count/page_Size).ToString());
        //    if(totalPages>0)
        //    {
        //        if ((pageIndex+1)>totalPages) 
        //            pageIndex = totalPages-1;				
        //    }
        //    else
        //    {
        //        pageIndex=0;
        //    }
        //    grid.CurrentPageIndex=pageIndex;
        //    grid.DataBind();			
        //    int page_Count=grid.PageCount;			
        //    int page_Current=pageIndex+1;
        //    //Page021.Page_Count=page_Count;		
        //    //Page021.Page_Size=page_Size;			
        //    //Page021.Page_Current=page_Current;
        //}
		protected string FormatString(string str) 
		{ 
//			str=str.Replace(" ","&nbsp;&nbsp;"); 
//			str=str.Replace("<","&lt;"); 
//			str=str.Replace(">","&gt;"); 
//			str=str.Replace('\n'.ToString(),"<br>"); 
			if(str.Length>16)
			{
				str=str.Substring(0,16)+"...";
			}			
			return str; 
		} 

		protected void Confirm_Click(object sender, System.EventArgs e)
		{
           


            //string dgIDs = "";
            //bool BxsChkd = false; 
            //foreach (DataGridItem item in grid.Items) 
            //{
            //    CheckBox deleteChkBxItem = (CheckBox) item.FindControl ("DeleteThis");
            //    if (deleteChkBxItem.Checked) 
            //    {					
            //        BxsChkd = true;					
            //        dgIDs += item.Cells[0].Text + ",";
            //    }
            //}						
            //if(BxsChkd)
            //{ 
            //    dgIDs=dgIDs.Substring(0,dgIDs.LastIndexOf(","));
            //    Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
            //    sm.DeleteLog(dgIDs);
            //    Response.Redirect("logindex.aspx");
            //}			
		}

		protected void btnDelAll_Click(object sender, System.EventArgs e)
		{
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
            sm.DeleteLog("");
            Response.Redirect("logindex.aspx");
		}

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
                    //保存日志
                    // SaveLogs("");
                    sm.DeleteLog(id.ToString());
                }
            }

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "logindex.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
        }
	}
}
